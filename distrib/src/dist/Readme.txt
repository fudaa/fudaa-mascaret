*** FUDAA-MASCARET 3.6 ***


Fudaa Mascaret : Installation
-----------------------------
* Before installing and executing Fudaa-Mascaret, you must have Java Runtime Environment (JRE) installed.

  See http://www.java.com if necessary.
  
* To begin the installation procedure:
   - uninstall all previous versions of Fudaa-Mascaret
   - click on the executable file "Fudaa_Mascaret_Setup.jar"

Information - Support
---------------------
For any problem or question, please contact us at: ot-consultancy@opentelemac.org
