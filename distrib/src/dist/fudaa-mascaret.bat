@echo off
@rem Lancement de Fudaa-Mascaret

@rem A modifier suivant l'installation du JDK.
set JAVA="%{JAVA_HOME}"

@rem A modifier suivant la version de Fudaa-Mascaret
set JAR=fudaa-mascaret.jar

%JAVA%\bin\javaw -Xms256m -Xmx2048m -cp "%{INSTALL_PATH}\%JAR%" org.fudaa.fudaa.mascaret.Mascaret "--no_corba" %1 %2 %3 %4 %5 %6 %7 %8 %9
