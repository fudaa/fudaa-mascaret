#!/bin/sh
# Lancement de Fudaa-Mascaret

# A modifier suivant l'installation du JDK.
JAVA=%{JAVA_HOME}

# A modifier suivant la version de Fudaa-Mascaret
JAR=fudaa-mascaret.jar

$JAVA/bin/java -cp "%{INSTALL_PATH}/$JAR" org.fudaa.fudaa.mascaret.Mascaret "--no_corba" $1 $2 $3 $4 $5 $6 $7 $8 $9
