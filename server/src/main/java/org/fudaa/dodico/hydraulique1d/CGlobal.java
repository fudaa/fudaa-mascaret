/*
 * @file         CGlobal.java
 * @creation     2000-08-16
 * @modification $Date: 2006-09-08 08:59:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IZone;
/**
 * Contient des m�thodes static g�n�rales pour comparer des doubles � une precision pr�s.
 * @version      $Revision: 1.8 $ $Date: 2006-09-08 08:59:07 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public final class CGlobal {
  public final static long PRECISION= 10000L;
  public static long arrondiLong(double nb) {
    return Math.round(nb * PRECISION);
  }
  public static double arrondi(double nb) {
    return ((double)Math.round(nb * PRECISION)) / PRECISION;
  }
  public static boolean egale(double nb1, double nb2) {
    return (Math.round(nb1 * PRECISION) == Math.round(nb2 * PRECISION));
  }
  public static int compare(double nb1, double nb2) {
    return (Math.round(nb1 * PRECISION) - Math.round(nb2 * PRECISION)) < 0
      ? -1
      : (Math.round(nb1 * PRECISION) - Math.round(nb2 * PRECISION)) > 0
      ? 1
      : 0;
  }
  public static boolean appartient(double nb, double borne1, double borne2) {
    double tmp;
    if (borne1 > borne2) {
      tmp= borne1;
      borne1= borne2;
      borne2= tmp;
    }
    return ((Math.round(nb * PRECISION) - Math.round(borne1 * PRECISION)) >= 0)
      && ((Math.round(nb * PRECISION) - Math.round(borne2 * PRECISION)) <= 0);
  }
  public static boolean appartient(double nb, IZone zone) {
    return appartient(nb, zone.abscisseDebut(), zone.abscisseFin());
  }
   public final static boolean AVEC_QUALITE_DEAU= true;
   public final static boolean AVEC_CALAGE_AUTO= true;
}
