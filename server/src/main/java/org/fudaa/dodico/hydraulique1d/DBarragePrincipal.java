/**
 * @file         DBarragePrincipal.java
 * @creation     2000-08-09
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IBarragePrincipal;
import org.fudaa.dodico.corba.hydraulique1d.IBarragePrincipalOperations;
import org.fudaa.dodico.corba.hydraulique1d.ISite;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "barrage principal".
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DBarragePrincipal
  extends DHydraulique1d
  implements IBarragePrincipal,IBarragePrincipalOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IBarragePrincipal) {
      IBarragePrincipal q= (IBarragePrincipal)_o;
      site((ISite)q.site().creeClone());
      ruptureProgressive(q.ruptureProgressive());
      cotePlanEau(q.cotePlanEau());
    }
  }
  @Override
  final public IObjet creeClone() {
    IBarragePrincipal p=
      UsineLib.findUsine().creeHydraulique1dBarragePrincipal();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "barragePrincipal";
    if (site_ != null)
      s += " (" + site_.toString() + ")";
    return s;
  }
  /*** IBarragePrincipal ***/
  // constructeurs
  public DBarragePrincipal() {
    super();
    site_= UsineLib.findUsine().creeHydraulique1dSite();
    ruptureProgressive_= false;
    cotePlanEau_= 0.;
  }
  @Override
  public void dispose() {
    site_= null;
    ruptureProgressive_= false;
    cotePlanEau_= 0.;
    super.dispose();
  }
  // Attributs
  private ISite site_;
  @Override
  public ISite site() {
    return site_;
  }
  @Override
  public void site(ISite s) {
    site_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "site");
  }
  private boolean ruptureProgressive_;
  @Override
  public boolean ruptureProgressive() {
    return ruptureProgressive_;
  }
  @Override
  public void ruptureProgressive(boolean s) {
    ruptureProgressive_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "ruptureProgressive");
  }
  private double cotePlanEau_;
  @Override
  public double cotePlanEau() {
    return cotePlanEau_;
  }
  @Override
  public void cotePlanEau(double s) {
    cotePlanEau_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "cotePlanEau");
  }
}
