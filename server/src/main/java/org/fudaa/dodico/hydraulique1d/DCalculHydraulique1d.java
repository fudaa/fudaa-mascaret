/*
 * @file         DCalculHydraulique1d.java
 * @creation     2000-08-10
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.calcul.ICalcul;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1dOperations;
import org.fudaa.dodico.corba.hydraulique1d.IEtude1d;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.DService;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "calcul hydraulique1d".
 * Associe une étude 1D et un calcul.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DCalculHydraulique1d
  extends DService
  implements ICalculHydraulique1d,ICalculHydraulique1dOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ICalculHydraulique1d) {
      ICalculHydraulique1d q= (ICalculHydraulique1d)_o;
      calculCode((ICalcul)q.calculCode().creeClone());
      etude((IEtude1d)q.etude().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ICalculHydraulique1d p=
      UsineLib.findUsine().creeHydraulique1dCalculHydraulique1d();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "calculHydraulique1d";
    return s;
  }
  // IService
  @Override
  public String description() {
    return "Hydraulique1d, serveur de calcul metier: " + super.description();
  }
  /*** ICalculHydraulique1d ***/
  // constructeurs
  public DCalculHydraulique1d() {
    super();
  }
  @Override
  public void dispose() {
    super.dispose();
  }
  // Attributs
  private ICalcul calculCode_;
  @Override
  public ICalcul calculCode() {
    return calculCode_;
  }
  @Override
  public void calculCode(ICalcul s) {
    if(calculCode_==s) return;
    calculCode_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "calculCode");
  }
  private IEtude1d etude_;
  @Override
  public IEtude1d etude() {
    return etude_;
  }
  @Override
  public void etude(IEtude1d s) {
    if(etude_==s) return;
    etude_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "etude");
  }
}
