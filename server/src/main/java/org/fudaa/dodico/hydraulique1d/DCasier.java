/*
 * @file         DCasier.java
 * @creation
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.ICasier;
import org.fudaa.dodico.corba.hydraulique1d.ICasierOperations;
import org.fudaa.dodico.corba.hydraulique1d.casier.IGeometrieCasier;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "casier".
 * Contient une cote, un nom et la g�om�trie du casier.
 * Une loi de type hydrogramme peut y �tre rattach�.
 * @version      $Revision: 1.10 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DCasier extends DHydraulique1d implements ICasier,ICasierOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ICasier) {
      ICasier q= (ICasier)_o;
      if (q.geometrie() != null)
        geometrie((IGeometrieCasier)q.geometrie().creeClone());
      loiRattachee((ILoiHydrogramme)q.loiRattachee());
      if (q.nom() != null) {
        nom(new String(q.nom()));
      }
      coteInitiale(q.coteInitiale());
      numero(q.numero());
    }
  }
  @Override
  final public IObjet creeClone() {
    ICasier p= UsineLib.findUsine().creeHydraulique1dCasier();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    return nom_;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Casier"+numero_+" '" + nom_ + "'";
    res[1]= "";
    res[1] += "Zinit : " + coteInitiale_;
    if (geometrie_ != null) {
      res[1] += " " + geometrie_.getInfos()[0];
    }
    if (loiRattachee_ != null) {
      res[1] += " loi : " + loiRattachee_.nom();
    } else {
      res[1] += " sans d�bit d'apport";
    }
    return res;
  }
  /*** ICasier ***/
  // constructeurs
  public DCasier() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    nom_= null;
    coteInitiale_= 0.;
    geometrie_= UsineLib.findUsine().creeHydraulique1dNuagePointsCasier();
    loiRattachee_= null;
    numero_= 0;
  }
  @Override
  public void dispose() {
    id_= 0;
    nom_= null;
    coteInitiale_= 0.;
    geometrie_= null;
    loiRattachee_= null;
    numero_= 0;
    super.dispose();
  }
  // attributs
  private int id_;
  private String nom_;
  @Override
  public String nom() {
    return nom_;
  }
  @Override
  public void nom(String s) {
    if (nom_ != null) {
      if (nom_.equals(s))
        return;
    }
    nom_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nom");
  }
  private double coteInitiale_;
  @Override
  public double coteInitiale() {
    return coteInitiale_;
  }
  @Override
  public void coteInitiale(double s) {
    if (coteInitiale_==s) return;
    coteInitiale_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteInitiale");
  }
  private ILoiHydrogramme loiRattachee_;
  @Override
  public ILoiHydrogramme loiRattachee() {
    return loiRattachee_;
  }
  @Override
  public void loiRattachee(ILoiHydrogramme s) {
    if (loiRattachee_==s) return;
    loiRattachee_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loiRattachee");
  }
  private IGeometrieCasier geometrie_;
  @Override
  public IGeometrieCasier geometrie() {
    return geometrie_;
  }
  @Override
  public void geometrie(IGeometrieCasier s) {
    if (geometrie_==s) return;
    geometrie_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "geometrie");
  }
  private int numero_;
  @Override
  public int numero() {
    return numero_;
  }
  @Override
  public void numero(int s) {
    if (numero_ == s)
      return;
    if (nom_ == null)
      nom("casier"+s);
    else if (nom_.equalsIgnoreCase("casier"+numero_))
      nom("casier"+s);
    numero_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numero");
  }
  // les m�thodes
  @Override
  public boolean isNuagePoints() {
    return geometrie_.isNuagePoints();
  }
  @Override
  public void toNuagePoints() {
    if (isNuagePoints())
      return;
    IGeometrieCasier g=
      UsineLib.findUsine().creeHydraulique1dNuagePointsCasier();
    geometrie(g);
  }
  @Override
  public boolean isPlanimetrage() {
    return geometrie_.isPlanimetrage();
  }
  @Override
  public void toPlanimetrage() {
    if (isPlanimetrage())
      return;
    IGeometrieCasier g=
      UsineLib.findUsine().creeHydraulique1dPlanimetrageCasier();
    geometrie(g);
  }
}
