/**
 * @creation     2000-08-09
 * @modification $Date: 2006-03-02 14:34:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.IConditionsInitiales;
import org.fudaa.dodico.corba.hydraulique1d.IConditionsInitialesOperations;
import org.fudaa.dodico.corba.hydraulique1d.ILigneEauInitiale;
import org.fudaa.dodico.corba.hydraulique1d.IParametresReprise;
import org.fudaa.dodico.corba.hydraulique1d.IParametresRepriseHelper;
import org.fudaa.dodico.corba.hydraulique1d.IZone;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "conditions initiales".
 * Peut �tre compos� d'une ligne d'eau initiale.
*  En noyau transcritique, elle peut �tre compos� d'une reprise de calcul ou d'une zone s�che.
 * @version      $Revision: 1.12 $ $Date: 2006-03-02 14:34:13 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DConditionsInitiales
  extends DHydraulique1d
  implements IConditionsInitiales,IConditionsInitialesOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IConditionsInitiales) {
      IConditionsInitiales c= (IConditionsInitiales)_o;
      paramsReprise(IParametresRepriseHelper.narrow(c.paramsReprise().creeClone()));
      ligneEauInitiale((ILigneEauInitiale)c.ligneEauInitiale().creeClone());

      if (c.zonesSeches() != null) {
        IZone[] izs= new IZone[c.zonesSeches().length];
        for (int i= 0; i < izs.length; i++)
          izs[i]= (IZone)c.zonesSeches()[i].creeClone();
        zonesSeches(izs);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IConditionsInitiales c=
      UsineLib.findUsine().creeHydraulique1dConditionsInitiales();
    c.initialise(tie());
    return c;
  }
  @Override
  final public String toString() {
    String s= "conditionsInitiales";
    return s;
  }
  /*** IConditionsInitiales ***/
  // constructeurs
  public DConditionsInitiales() {
    super();
    paramsReprise_= null;
    ligneEauInitiale_= UsineLib.findUsine().creeHydraulique1dLigneEauInitiale();
    zonesSeches_= new IZone[0];

  }
  @Override
  public void dispose() {
    paramsReprise_= null;
    ligneEauInitiale_= null;
    zonesSeches_= null;
    super.dispose();
  }
  // attributs
  private IParametresReprise paramsReprise_;
  @Override
  public IParametresReprise paramsReprise() {
    return paramsReprise_;
  }
  @Override
  public void paramsReprise(IParametresReprise paramsReprise) {
    if (paramsReprise_==paramsReprise) return;
    paramsReprise_= paramsReprise;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "paramsReprise");
  }
  private ILigneEauInitiale ligneEauInitiale_;
  @Override
  public ILigneEauInitiale ligneEauInitiale() {
    return ligneEauInitiale_;
  }
  @Override
  public void ligneEauInitiale(ILigneEauInitiale ligneEauInitiale) {
    if (ligneEauInitiale_==ligneEauInitiale) return;
    ligneEauInitiale_= ligneEauInitiale;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "ligneEauInitiale");
  }
  private IZone[] zonesSeches_;
  @Override
  public IZone[] zonesSeches() {
    return zonesSeches_;
  }
  @Override
  public void zonesSeches(IZone[] zonesSeches) {
    if (egale(zonesSeches_, zonesSeches)) return;
    zonesSeches_= zonesSeches;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zonesSeches");
  }
  private final static boolean egale(IZone[] p, IZone[] p2) {
    if (p == p2)
      return true;
    if (p == null || p2 == null)
      return false;

    int length = p.length;
    if (p2.length != length)
      return false;

    for (int i = 0; i < length; i++)
      if (p[i] != p2[i])
        return false;

    return true;
  }



  // methodes
  @Override
  public void creeParamsReprise() {
    paramsReprise_= UsineLib.findUsine().creeHydraulique1dParametresReprise();
  }
  @Override
  public void creeLigneInitiale() {
    ligneEauInitiale_= UsineLib.findUsine().creeHydraulique1dLigneEauInitiale();
  }
  @Override
  public IZone creeZoneSeche() {
    IZone zone= UsineLib.findUsine().creeHydraulique1dZone();
    IZone zones[]= new IZone[zonesSeches_.length + 1];
    for (int i= 0; i < zonesSeches_.length; i++)
      zones[i]= zonesSeches_[i];
    zones[zones.length - 1]= zone;
    zonesSeches(zones);
    return zone;
  }
  private void supprimeZoneSeche(IZone zone) {
    Vector newZones= new Vector(zonesSeches_.length - 1);
    for (int i= 0; i < zonesSeches_.length; i++) {
      if (zonesSeches_[i] == zone) {
        UsineLib.findUsine().supprimeHydraulique1dZone(zonesSeches_[i]);
      } else
        newZones.add(zonesSeches_[i]);
    }
    IZone[] zonesSeches= new IZone[newZones.size()];
    for (int i= 0; i < zonesSeches.length; i++) {
      zonesSeches[i]= (IZone)newZones.get(i);
    }
    zonesSeches(zonesSeches);
  }
  @Override
  public void supprimeZonesSeches(IZone[] zones) {
    //System.out.println("supprimeZonesSeches(IZone[] zones) zones.length="+zones.length);
    for (int i= 0; i < zones.length; i++) {
      supprimeZoneSeche(zones[i]);
    }
  }
  @Override
  public void supprimeZonesSechesAvecBief(IBief bief) {
    if (zonesSeches_ == null) return;

    Vector newZones= new Vector();
    for (int i= 0; i < zonesSeches_.length; i++) {
      if (zonesSeches_[i].biefRattache() == bief) {
        UsineLib.findUsine().supprimeHydraulique1dZone(zonesSeches_[i]);
      } else
        newZones.add(zonesSeches_[i]);
    }

    if (newZones.size() == zonesSeches_.length)
      // aucune  suppression n'a �t� faite.
      return;

    IZone[] zonesSeches= new IZone[newZones.size()];
    for (int i= 0; i < zonesSeches.length; i++) {
      zonesSeches[i]= (IZone)newZones.get(i);
    }
    zonesSeches(zonesSeches);
  }


}
