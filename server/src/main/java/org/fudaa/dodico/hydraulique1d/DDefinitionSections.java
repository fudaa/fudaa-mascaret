/**
 * @file         DDefinitionSections.java
 * @creation     2001-07-17
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSections;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsOperations;
import org.fudaa.dodico.corba.objet.IObjet;
/**
 * Implémentation de l'objet métier "définitions des sections de calculs".
 * @version      $Revision: 1.7 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public abstract class DDefinitionSections
  extends DHydraulique1d
  implements IDefinitionSections,IDefinitionSectionsOperations {
  @Override
  public void initialise(IObjet _o) {
  }
  @Override
  public String toString() {
    String s= "definitionSections ?";
    return s;
  }
  // constructeurs
  public DDefinitionSections() {
    super();
  }
  @Override
  public void dispose() {
    super.dispose();
  }
}
