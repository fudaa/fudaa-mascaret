/**
 * @file         DDefinitionSectionsParSections.java
 * @creation     2000-08-09
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSections;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSectionsOperations;
import org.fudaa.dodico.corba.hydraulique1d.ISite;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "d�finitions des sections de calculs sections par sections".
 * Contient un tableau de sites (association une abscisse et d'un bief).
 * @version      $Revision: 1.9 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DDefinitionSectionsParSections
  extends DDefinitionSections
  implements IDefinitionSectionsParSections,IDefinitionSectionsParSectionsOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IDefinitionSectionsParSections) {
      IDefinitionSectionsParSections q= (IDefinitionSectionsParSections)_o;
      if (q.unitaires() != null) {
        ISite[] iu= new ISite[q.unitaires().length];
        for (int i= 0; i < iu.length; i++)
          iu[i]= (ISite)q.unitaires()[i].creeClone();
        unitaires(iu);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IDefinitionSectionsParSections p=
      UsineLib.findUsine().creeHydraulique1dDefinitionSectionsParSections();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionsSections";
    return s;
  }
  /*** IDefinitionSectionsParSections ***/
  // constructeurs
  public DDefinitionSectionsParSections() {
    super();
    unitaires_= new ISite[0];
  }
  @Override
  public void dispose() {
    unitaires_= null;
    super.dispose();
  }
  // Attributs
  private ISite[] unitaires_;
  @Override
  public ISite[] unitaires() {
    return unitaires_;
  }
  @Override
  public void unitaires(ISite[] s) {
    if (egale(unitaires_, s)) return;
    unitaires_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "unitaires");
  }
  // methodes
  @Override
  public ISite creeSection(int indice) {
    ISite unitaire= UsineLib.findUsine().creeHydraulique1dSite();
    ISite[] us= new ISite[unitaires_.length + 1];
    for (int i= 0; i < indice; i++)
      us[i]= unitaires_[i];
    us[indice]= unitaire;
    for (int i= indice + 1; i < us.length; i++)
      us[i]= unitaires_[i - 1];
    unitaires(us);
    return unitaire;
  }
  @Override
  public void creeSectionALaFin(IBief biefRattache) {
    ISite unitaire= UsineLib.findUsine().creeHydraulique1dSite();
    unitaire.biefRattache(biefRattache);
    ISite[] us= new ISite[unitaires_.length + 1];
    for (int i= 0; i < unitaires_.length; i++)
      us[i]= unitaires_[i];
    us[unitaires_.length]= unitaire;
    unitaires(us);
  }
  @Override
  public void supprimeSections(ISite[] sections) {
    Vector newus= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      boolean trouve= false;
      for (int j= 0; j < sections.length; j++) {
        if (unitaires_[i] == sections[j])
          trouve= true;
      }
      if (!trouve)
        newus.add(unitaires_[i]);
    }
    ISite[] us= new ISite[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (ISite)newus.get(i);
    unitaires(us);
  }
  @Override
  public void supprimeSectionMaillageAvecBief(IBief bief) {
    if (unitaires_ == null) return;
    Vector newus= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      if (unitaires_[i].biefRattache() == bief) {
        UsineLib.findUsine().supprimeHydraulique1dSite(unitaires_[i]);
      } else {
        newus.add(unitaires_[i]);
      }
    }

    if (newus.size() == unitaires_.length)
      // aucune suppression n'a �t� faite.
      return;

    ISite[] us= new ISite[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (ISite)newus.get(i);
    unitaires(us);
  }
}
