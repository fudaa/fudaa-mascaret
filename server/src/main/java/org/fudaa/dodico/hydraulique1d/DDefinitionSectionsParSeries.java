/**
 * @file         DDefinitionSectionsParSeries.java
 * @creation     2000-08-09
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSeries;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSeriesOperations;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSeriesUnitaire;
import org.fudaa.dodico.corba.hydraulique1d.IProfil;
import org.fudaa.dodico.corba.hydraulique1d.IReseau;
import org.fudaa.dodico.corba.hydraulique1d.IZone;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "d�finitions des sections de calculs par s�ries de zones".
 * Contient un tableau d'�l�ment associant une zone (un bief et 2 abscisses) et un pas d'espace.
 * @version      $Revision: 1.9 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DDefinitionSectionsParSeries
  extends DDefinitionSections
  implements IDefinitionSectionsParSeries,IDefinitionSectionsParSeriesOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IDefinitionSectionsParSeries) {
      IDefinitionSectionsParSeries q= (IDefinitionSectionsParSeries)_o;
      surProfils(q.surProfils());
      if (q.unitaires() != null) {
        IDefinitionSectionsParSeriesUnitaire[] iu=
          new IDefinitionSectionsParSeriesUnitaire[q.unitaires().length];
        for (int i= 0; i < iu.length; i++)
          iu[i]=
            (IDefinitionSectionsParSeriesUnitaire)q.unitaires()[i].creeClone();
        unitaires(iu);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IDefinitionSectionsParSeries p=
      UsineLib.findUsine().creeHydraulique1dDefinitionSectionsParSeries();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionsSeries";
    return s;
  }
  /*** IDefinitionSectionsParSeries ***/
  // constructeurs
  public DDefinitionSectionsParSeries() {
    super();
    surProfils_= true;
    unitaires_= new IDefinitionSectionsParSeriesUnitaire[0];
  }
  @Override
  public void dispose() {
    surProfils_= true;
    unitaires_= null;
    super.dispose();
  }
  // Attributs
  private boolean surProfils_;
  @Override
  public boolean surProfils() {
    return surProfils_;
  }
  @Override
  public void surProfils(boolean s) {
    if (surProfils_==s) return;
    surProfils_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "surProfils");
  }
  private IDefinitionSectionsParSeriesUnitaire[] unitaires_;
  @Override
  public IDefinitionSectionsParSeriesUnitaire[] unitaires() {
    return unitaires_;
  }
  @Override
  public void unitaires(IDefinitionSectionsParSeriesUnitaire[] s) {
    if (egale(unitaires_, s)) return;
    unitaires_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "unitaires");
  }
  // methodes
  @Override
  public IDefinitionSectionsParSeriesUnitaire creeSerie(int indice) {
    IDefinitionSectionsParSeriesUnitaire unitaire=
      UsineLib
        .findUsine()
        .creeHydraulique1dDefinitionSectionsParSeriesUnitaire();
    IDefinitionSectionsParSeriesUnitaire[] us=
      new IDefinitionSectionsParSeriesUnitaire[unitaires_.length + 1];
    for (int i= 0; i < indice; i++)
      us[i]= unitaires_[i];
    us[indice]= unitaire;
    for (int i= indice + 1; i < us.length; i++)
      us[i]= unitaires_[i - 1];
    unitaires(us);
    return unitaire;
  }
  @Override
  public void creeSerieALaFin(IBief biefRattache) {
    IDefinitionSectionsParSeriesUnitaire unitaire=
      UsineLib
        .findUsine()
        .creeHydraulique1dDefinitionSectionsParSeriesUnitaire();
    unitaire.zone().biefRattache(biefRattache);
    if ((biefRattache.extrAmont() != null)
      && (biefRattache.extrAmont().profilRattache() != null)) {
      unitaire.zone().abscisseDebut(
        biefRattache.extrAmont().profilRattache().abscisse());
    }
    if ((biefRattache.extrAval() != null)
      && (biefRattache.extrAval().profilRattache() != null)) {
      unitaire.zone().abscisseFin(
        biefRattache.extrAval().profilRattache().abscisse());
    }
    IDefinitionSectionsParSeriesUnitaire[] us=
      new IDefinitionSectionsParSeriesUnitaire[unitaires_.length + 1];
    for (int i= 0; i < unitaires_.length; i++)
      us[i]= unitaires_[i];
    us[unitaires_.length]= unitaire;
    unitaires(us);
  }
  @Override
  public void supprimeSeries(IDefinitionSectionsParSeriesUnitaire[] series) {
    Vector newus= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      boolean trouve= false;
      for (int j= 0; j < series.length; j++) {
        if (unitaires_[i] == series[j])
          trouve= true;
      }
      if (!trouve)
        newus.add(unitaires_[i]);
    }
    IDefinitionSectionsParSeriesUnitaire[] us=
      new IDefinitionSectionsParSeriesUnitaire[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (IDefinitionSectionsParSeriesUnitaire)newus.get(i);
    unitaires(us);
  }
  @Override
  public void supprimeSectionMaillageAvecBief(IBief bief) {
    if (unitaires_ == null) return;

    Vector newus= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      if (unitaires_[i].zone() != null) {
        if (unitaires_[i].zone().biefRattache() == bief) {
          UsineLib.findUsine().supprimeHydraulique1dZone(unitaires_[i].zone());
          UsineLib.findUsine().
              supprimeHydraulique1dDefinitionSectionsParSeriesUnitaire(
              unitaires_[i]);
        }
        else {
          newus.add(unitaires_[i]);
        }
      }
    }
    IDefinitionSectionsParSeriesUnitaire[] us=
      new IDefinitionSectionsParSeriesUnitaire[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (IDefinitionSectionsParSeriesUnitaire)newus.get(i);
    unitaires(us);
  }
  @Override
  public IDefinitionSectionsParSeriesUnitaire[] verifieContraintesProfil(IReseau reseau) {
    IDefinitionSectionsParSeriesUnitaire[] errs=
      new IDefinitionSectionsParSeriesUnitaire[0];
    if ((unitaires_ == null))
      return errs;
    Vector erreurs= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      IProfil b1=
        reseau.getProfilAbscisse(unitaires_[i].zone().abscisseDebut());
      IProfil b2= reseau.getProfilAbscisse(unitaires_[i].zone().abscisseFin());
      if (b1 == b2)
        erreurs.add(unitaires_[i]);
    }
    errs= new IDefinitionSectionsParSeriesUnitaire[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++)
      errs[i]= (IDefinitionSectionsParSeriesUnitaire)erreurs.get(i);
    return errs;
  }
  @Override
  public IDefinitionSectionsParSeriesUnitaire[] verifieContraintesBief(IReseau reseau) {
    IDefinitionSectionsParSeriesUnitaire[] errs=
      new IDefinitionSectionsParSeriesUnitaire[0];
    if ((unitaires_ == null))
      return errs;
    Vector erreurs= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      IBief b1=
        reseau.getBiefContenantAbscisse(unitaires_[i].zone().abscisseDebut());
      IBief b2=
        reseau.getBiefContenantAbscisse(unitaires_[i].zone().abscisseFin());
      if (b1 != b2)
        erreurs.add(unitaires_[i]);
    }
    errs= new IDefinitionSectionsParSeriesUnitaire[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++)
      errs[i]= (IDefinitionSectionsParSeriesUnitaire)erreurs.get(i);
    return errs;
  }
  @Override
  public IDefinitionSectionsParSeriesUnitaire[] verifieContraintesChevauchement(IReseau reseau) {
    IDefinitionSectionsParSeriesUnitaire[] errs=
      new IDefinitionSectionsParSeriesUnitaire[0];
    if ((unitaires_ == null))
      return errs;
    Vector erreurs= new Vector();
    IProfil bmax= null;
    for (int i= 0; i < unitaires_.length; i++) {
      IProfil b1=
        reseau.getProfilAbscisse(unitaires_[i].zone().abscisseDebut());
      IProfil b2= reseau.getProfilAbscisse(unitaires_[i].zone().abscisseFin());
      if ((bmax != null) && (b1.abscisse() < bmax.abscisse())) {
        erreurs.add(unitaires_[i - 1]);
        erreurs.add(unitaires_[i]);
      }
      bmax= b2;
    }
    errs= new IDefinitionSectionsParSeriesUnitaire[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++)
      errs[i]= (IDefinitionSectionsParSeriesUnitaire)erreurs.get(i);
    return errs;
  }
  @Override
  public void remplitAvecProfils(IReseau reseau) {
    if (unitaires_ == null)
      return;
    IDefinitionSectionsParSeriesUnitaire[] nouvu= null;
    Vector vu= new Vector();
    IBief[] biefs= reseau.biefs();
    int nb= 0;
    supprimeSeries(unitaires_);
    for (int b= 0; b < biefs.length; b++) {
      IProfil[] profils= biefs[b].profils();
      for (int i= 0; i < profils.length - 1; i++) {
        IDefinitionSectionsParSeriesUnitaire u= creeSerie(nb++);
        IZone zone= u.zone();
        zone.abscisseDebut(profils[i].abscisse());
        zone.abscisseFin(profils[i + 1].abscisse());
        u.pas(zone.abscisseFin() - zone.abscisseDebut());
        vu.add(u);
      }
    }
    nouvu= new IDefinitionSectionsParSeriesUnitaire[vu.size()];
    for (int i= 0; i < nouvu.length; i++) {
      nouvu[i]= (IDefinitionSectionsParSeriesUnitaire)vu.get(i);
    }
    unitaires(nouvu);
  }
}
