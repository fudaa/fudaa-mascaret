/*
 * @file         DDefinitionSectionsParSeriesUnitaire.java
 * @creation     2000-08-09
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSeriesUnitaire;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSeriesUnitaireOperations;
import org.fudaa.dodico.corba.hydraulique1d.IZone;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "�l�ment d'une d�finition des sections de calculs par zone".
 * Association d'une zone (un bief et 2 abscisses) et un pas d'espace.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DDefinitionSectionsParSeriesUnitaire
  extends DHydraulique1d
  implements IDefinitionSectionsParSeriesUnitaire,IDefinitionSectionsParSeriesUnitaireOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IDefinitionSectionsParSeriesUnitaire) {
      IDefinitionSectionsParSeriesUnitaire q=
        (IDefinitionSectionsParSeriesUnitaire)_o;
      zone((IZone)q.zone().creeClone());
      pas(q.pas());
    }
  }
  @Override
  final public IObjet creeClone() {
    IDefinitionSectionsParSeriesUnitaire p=
      UsineLib
        .findUsine()
        .creeHydraulique1dDefinitionSectionsParSeriesUnitaire();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionSerieUnitaire(pas " + pas_;
    if (zone_ != null)
      s += ",zone " + zone_.toString();
    s += ")";
    return s;
  }
  /*** IDefinitionSectionsParSeriesUnitaire ***/
  // constructeurs
  public DDefinitionSectionsParSeriesUnitaire() {
    super();
    zone_= UsineLib.findUsine().creeHydraulique1dZone();
    pas_= 1.;
  }
  @Override
  public void dispose() {
    zone_= null;
    pas_= 0.;
    super.dispose();
  }
  // Attributs
  private IZone zone_;
  @Override
  public IZone zone() {
    return zone_;
  }
  @Override
  public void zone(IZone s) {
    if (zone_==s) return;
    zone_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zone");
  }
  private double pas_;
  @Override
  public double pas() {
    return pas_;
  }
  @Override
  public void pas(double s) {
    if (pas_==s) return;
    pas_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pas");
  }
  // methodes
  @Override
  public int nbSections() {
    double res= ((zone_.abscisseFin() - zone_.abscisseDebut()) / pas_) - 1;
    return (int)Math.round(res);
  }
  @Override
  public void nbSections(int nb) {
    double pas= (zone_.abscisseFin() - zone_.abscisseDebut()) / (nb + 1);
    pas(pas);
  }
}
