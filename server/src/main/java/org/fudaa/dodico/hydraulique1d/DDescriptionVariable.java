/**
 * @file         DDescriptionVariable.java
 * @creation     2000-08-10
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IDescriptionVariable;
import org.fudaa.dodico.corba.hydraulique1d.IDescriptionVariableOperations;
import org.fudaa.dodico.corba.hydraulique1d.LTypeNombre;
import org.fudaa.dodico.corba.hydraulique1d.LUnite;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "Description d'une variable résultat".
 * @version      $Revision: 1.9 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DDescriptionVariable
  extends DHydraulique1d
  implements IDescriptionVariable,IDescriptionVariableOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IDescriptionVariable) {
      IDescriptionVariable q= (IDescriptionVariable)_o;
      nom(q.nom());
      description(q.description());
      unite(q.unite());
      type(q.type());
      nbDecimales(q.nbDecimales());
    }
  }
  @Override
  final public IObjet creeClone() {
    IDescriptionVariable p=
      UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "variable " + nom_;
    return s;
  }
  /*** IDescriptionVariable ***/
  // constructeurs
  public DDescriptionVariable() {
    super();
    nom_= "var";
    description_= "Variable";
    unite_= LUnite.RIEN;
    type_= LTypeNombre.ENTIER;
    nbDecimales_= 0;
  }
  @Override
  public void dispose() {
    nom_= null;
    description_= null;
    unite_= null;
    type_= null;
    nbDecimales_= 0;
    super.dispose();
  }
  // Attributs
  private String nom_;
  @Override
  public String nom() {
    return nom_;
  }
  @Override
  public void nom(String s) {
    nom_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nom");
  }
  private String description_;
  @Override
  public String description() {
    return description_;
  }
  @Override
  public void description(String s) {
    if (description_.equals(s)) return;
    description_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "description");
  }
  private LUnite unite_;
  @Override
  public LUnite unite() {
    return unite_;
  }
  @Override
  public void unite(LUnite s) {
    if (unite_.value()==s.value()) return;
    unite_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "unite");
  }
  private LTypeNombre type_;
  @Override
  public LTypeNombre type() {
    return type_;
  }
  @Override
  public void type(LTypeNombre s) {
    if (type_.value()==s.value()) return;
    type_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "type");
  }
  private int nbDecimales_;
  @Override
  public int nbDecimales() {
    return nbDecimales_;
  }
  @Override
  public void nbDecimales(int s) {
    if (nbDecimales_==s) return;
    nbDecimales_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nbDecimales");
  }
}
