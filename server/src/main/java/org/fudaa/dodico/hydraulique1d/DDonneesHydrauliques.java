/**
 * @file         DDonneesHydrauliques.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.fudaa.dodico.corba.evenement.IObjetEvent;
import org.fudaa.dodico.corba.evenement.IObjetEventListenerSupport;
import org.fudaa.dodico.corba.evenement.IObjetEventSender;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.IConditionsInitiales;
import org.fudaa.dodico.corba.hydraulique1d.IDonneesHydrauliques;
import org.fudaa.dodico.corba.hydraulique1d.IDonneesHydrauliquesOperations;
import org.fudaa.dodico.corba.hydraulique1d.ILaisse;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiGeometrique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimniHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiOuvertureVanne;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiRegulation;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiSeuil;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.evenement.DObjetEventListenerSupport;
import org.fudaa.dodico.evenement.ObjetEventListener;
import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTracer;
/**
 * Impl�mentation de l'objet m�tier "donn�es hydrauliques" de l'�tude.
 * Contients les conditions initiales, les lois hydrauliques, les laisses de crues.
 * @version      $Revision: 1.18 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DDonneesHydrauliques
  extends DHydraulique1d
  implements IDonneesHydrauliques,IDonneesHydrauliquesOperations, ObjetEventListener {
  transient private DObjetEventListenerSupport evtSupport_;
  /*** IObjet ***/
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IDonneesHydrauliques) {
      IDonneesHydrauliques c= (IDonneesHydrauliques)_o;
      conditionsInitiales(
        (IConditionsInitiales)c.conditionsInitiales().creeClone());
      if (c.lois() != null) {
        ILoiHydraulique[] il= new ILoiHydraulique[c.lois().length];
        for (int i= 0; i < il.length; i++)
          il[i]= (ILoiHydraulique)c.lois()[i].creeClone();
        lois(il);
      }
      if (c.laisses() != null) {
        ILaisse[] il= new ILaisse[c.laisses().length];
        for (int i= 0; i < il.length; i++)
          il[i]= (ILaisse)c.laisses()[i].creeClone();
        laisses(il);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IDonneesHydrauliques c=
      UsineLib.findUsine().creeHydraulique1dDonneesHydrauliques();
    c.initialise(tie());
    return c;
  }
  @Override
  final public String toString() {
    String s= "donneesHydrauliques";
    return s;
  }
  /*** IDonneesHydrauliques ***/
  // constructeurs
  public DDonneesHydrauliques() {
    super();
    lois_= new ILoiHydraulique[0];
    conditionsInitiales_=
      UsineLib.findUsine().creeHydraulique1dConditionsInitiales();
    laisses_= new ILaisse[0];
    evtSupport_= DObjetEventListenerSupport.createEventSupport();
    evtSupport_.clientListener(this);
    ((IObjetEventSender)UsineLib.findUsine()).addObjetEventListener(
      (IObjetEventListenerSupport)evtSupport_.tie());
  }
  @Override
  public void dispose() {
    ((IObjetEventSender)UsineLib.findUsine()).removeObjetEventListener(
      (IObjetEventListenerSupport)evtSupport_.tie());
    evtSupport_.clientListener(null);
    evtSupport_= null;
    lois_= null;
    conditionsInitiales_= null;
    laisses_= null;
    super.dispose();
  }
  @Override
  public void objetCree(IObjetEvent e) {}
  @Override
  public void objetSupprime(IObjetEvent e) {
    IObjet src=e.getSource();
    if (src instanceof IBief) {
      supprimePointsLigneEauInitAvecBief((IBief)src);
      supprimeZonesSechesAvecBief((IBief)src);
    }
  }
  @Override
  public void objetModifie(IObjetEvent e) {
    if ((e.getSource() instanceof IBief)&&("indice".equals(e.getChamp()))){
      System.out.println("MODICATION DE L'INDICE DU BIEF");
      System.out.println("DDonneesHydrauliques objetModifie(IObjetEvent e=)");
      System.out.println("\t e.getSource="+e.getSource().getClass().getName());
      System.out.println("\t e.getChamp="+e.getChamp());
      System.out.println("\t e.getMessage="+e.getMessage());
      System.out.println("\t e.isConsomme="+e.isConsomme());
      int nouveauNumero = ((IBief)e.getSource()).indice()+1;
      StringTokenizer st = new StringTokenizer(e.getMessage(),"()");
      if (st.countTokens() >=2) {
//        String nouveauMessage = st.nextToken().trim();
        st.nextToken();
        int ancienNumero = Integer.parseInt(st.nextToken());
        miseAJourNumeroBiefPointsLigneEauInit(nouveauNumero, ancienNumero);
//        if (ok) e.setMessage(nouveauMessage);
      }
    }
  }
  // attributs
  private IConditionsInitiales conditionsInitiales_;
  @Override
  public IConditionsInitiales conditionsInitiales() {
    return conditionsInitiales_;
  }
  @Override
  public void conditionsInitiales(IConditionsInitiales conditionsInitiales) {
    if (conditionsInitiales_==conditionsInitiales) return;
    conditionsInitiales_= conditionsInitiales;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "conditionsInitiales");
  }
  private ILoiHydraulique[] lois_;
  @Override
  public ILoiHydraulique[] lois() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    return lois_;
  }
  @Override
  public void lois(ILoiHydraulique[] lois) {
    if (lois == null) {
      lois_= new ILoiHydraulique[0];
    }
    if (egale(lois_, lois)) return;
    lois_= lois;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "lois");
  }
  private ILaisse[] laisses_;
  @Override
  public ILaisse[] laisses() {
    return laisses_;
  }
  @Override
  public void laisses(ILaisse[] laisses) {
    if (egale(laisses_, laisses)) return;
    laisses_= laisses;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "laisses");
  }
  // methodes
  @Override
  public void ajouteLoi(ILoiHydraulique loi) {
    if (loi == null)
      return;
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    if (isContient(loi)) return;
    List listLoi = new ArrayList(Arrays.asList(lois_));
    listLoi.add(loi);
    lois_= (ILoiHydraulique[])listLoi.toArray(new ILoiHydraulique[0]);
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "lois");
    initIndiceLois();
  }

  private final boolean isContient(ILoiHydraulique loi) {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    if (loi!=null) {
      for (int i = 0; i < lois_.length; i++) {
        if (loi.numero() == lois_[i].numero())
          return true;
      }
    }
    return false;
  }

  @Override
  public void supprimeLois(ILoiHydraulique[] _lois) {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
      return;
    }
    if (_lois == null) return;
    if (_lois.length==0) return;
    Vector newlois= new Vector();
    

  /*  for (int i= 0; i < lois_.length; i++) {
      for (int j= 0; j < _lois.length; j++) {
        if (lois_[i] == _lois[j]) {
          DLoiHydraulique.supprimeLoiHydraulique(lois_[i]);
          
        } else
          newlois.add(lois_[i]);
      }
    }*/
    

    boolean LoiTrouvee =false;
    for (int i= 0; i < lois_.length; i++) {
      for (int j= 0; j < _lois.length; j++) {
        if (lois_[i] == _lois[j]) {
          DLoiHydraulique.supprimeLoiHydraulique(lois_[i]);
          LoiTrouvee = true;
        } 
        if (LoiTrouvee) break;
      }
      if (!LoiTrouvee) newlois.add(lois_[i]);
      LoiTrouvee = false;
   }


    lois_= new ILoiHydraulique[newlois.size()];
    for (int i= 0; i < lois_.length; i++)
      lois_[i]= (ILoiHydraulique)newlois.get(i);
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "lois");
    initIndiceLois();
  }
  
  
  
  @Override
  public ILaisse creeLaisse(IBief biefRattache) {
    ILaisse laisse= UsineLib.findUsine().creeHydraulique1dLaisse();
    laisse.site().biefRattache(biefRattache);
    ILaisse laisses[]= new ILaisse[laisses_.length + 1];
    for (int i= 0; i < laisses_.length; i++)
      laisses[i]= laisses_[i];
    laisses[laisses.length - 1]= laisse;
    laisses(laisses);
    return laisse;
  }
  @Override
  public void supprimeLaisses(ILaisse[] laisses) {
    if (laisses_ == null)
      return;
    for (int i= 0; i < laisses.length; i++) {
      supprimeLaisse(laisses[i]);
    }
  }
  protected void supprimeLaisse(ILaisse laisse) {
    if (laisses_ == null)
      return;
    Vector newlaiss= new Vector();
    for (int i= 0; i < laisses_.length; i++) {
      if (laisses_[i] == laisse) {
        UsineLib.findUsine().supprimeHydraulique1dLaisse(laisse);
      } else
        newlaiss.add(laisses_[i]);
    }
    ILaisse[] nlaisses= new ILaisse[newlaiss.size()];
    for (int i= 0; i < nlaisses.length; i++)
      nlaisses[i]= (ILaisse)newlaiss.get(i);
    laisses(nlaisses);
  }
  @Override
  public void supprimeZonesSechesAvecBief(IBief bief) {
    if (conditionsInitiales() != null) {
      conditionsInitiales().supprimeZonesSechesAvecBief(bief);
    }
  }
  @Override
  public void supprimePointsLigneEauInitAvecBief(IBief bief) {
    System.out.println("DDonneesHydrauliques supprimePointsLigneEauInitAvecBief(..)");
    System.out.println("bief.indice="+bief.indice());
    if (conditionsInitiales() != null) {
      if (conditionsInitiales().ligneEauInitiale() != null) {
        conditionsInitiales().ligneEauInitiale().supprimePointsBiefNumero(bief.indice()+1);
      }
    }
  }
  @Override
  public void miseAJourNumeroBiefPointsLigneEauInit(int nouveau, int ancien) {
    if (conditionsInitiales() != null) {
      if (conditionsInitiales().ligneEauInitiale() != null) {
        conditionsInitiales().ligneEauInitiale().miseAJourNumeroBiefPointsLigneEauInit(nouveau, ancien);
      }
    }
  }
  @Override
  public ILoiGeometrique creeLoiGeometrique() {
    ILoiGeometrique loi= UsineLib.findUsine().creeHydraulique1dLoiGeometrique();
    ajouteLoi(loi);
    return loi;
  }
  @Override
  public ILoiHydrogramme creeLoiHydrogramme() {
    ILoiHydrogramme loi= UsineLib.findUsine().creeHydraulique1dLoiHydrogramme();
    ajouteLoi(loi);
    return loi;
  }
  @Override
  public ILoiLimnigramme creeLoiLimnigramme() {
    ILoiLimnigramme loi= UsineLib.findUsine().creeHydraulique1dLoiLimnigramme();
    ajouteLoi(loi);
    return loi;
  }
  @Override
  public ILoiLimniHydrogramme creeLoiLimniHydrogramme() {
    ILoiLimniHydrogramme loi=
      UsineLib.findUsine().creeHydraulique1dLoiLimniHydrogramme();
    ajouteLoi(loi);
    return loi;
  }
  @Override
  public ILoiOuvertureVanne creeLoiOuvertureVanne() {
    ILoiOuvertureVanne loi=
      UsineLib.findUsine().creeHydraulique1dLoiOuvertureVanne();
    ajouteLoi(loi);
    return loi;
  }
  @Override
  public ILoiRegulation creeLoiRegulation() {
    ILoiRegulation loi= UsineLib.findUsine().creeHydraulique1dLoiRegulation();
    ajouteLoi(loi);
    return loi;
  }
  @Override
  public ILoiSeuil creeLoiSeuil() {
    ILoiSeuil loi= UsineLib.findUsine().creeHydraulique1dLoiSeuil();
    ajouteLoi(loi);
    return loi;
  }
  @Override
  public ILoiTarage creeLoiTarage() {
    ILoiTarage loi= UsineLib.findUsine().creeHydraulique1dLoiTarage();
    ajouteLoi(loi);
    return loi;
  }
  @Override
  public ILoiTracer creeLoiTracer(int nbColonne) {
  ILoiTracer loi= UsineLib.findUsine().creeHydraulique1dLoiTracer();
  loi.setTailleTableau(nbColonne);
  ajouteLoi(loi);
  return loi;
}

  @Override
  public ILoiHydraulique getLoi(int numero) {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    ILoiHydraulique loi= null;
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i].numero() == numero) {
        loi= lois_[i];
        break;
      }
    }
    return loi;
  }
  
  //Cette fonction renvoie la position de la loi dans le tableau une fois les lois tracer retir�es
  @Override
  public int getIndiceLoi(ILoiHydraulique loi) {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
	 ILoiHydraulique[] loisSaufTracer;
	 loisSaufTracer= getToutesLoisSaufTracer();
	 
    for (int i= 0; i < loisSaufTracer.length; i++) {
      if (loisSaufTracer[i] == loi) {
        return i;
      }
    }
    return -1;
  }
  
  //Cette fonction renvoie la position de la loi dans le tableau des lois tracer 
  @Override
  public int getIndiceLoiTracer(ILoiTracer loi) {
	    if (lois_ == null) {
	    	 lois_= new ILoiHydraulique[0];
	    }
		 ILoiTracer[] loisTracer;
	    loisTracer= getLoisTracer();
	    	
	    for (int i= 0; i < loisTracer.length; i++) {
	      if (loisTracer[i] == loi) {
	        return i;
	      }
	    }
	    return -1;
	  }
  
  @Override
  public void initIndiceLois() {

   ArrayList vLois= new ArrayList();
   ArrayList vLoisGeo= new ArrayList();

   if (lois_ == null) {
     lois_= new ILoiHydraulique[0];
   }
   for (int i= 0; i < lois_.length; i++) {
     if (lois_[i] instanceof ILoiGeometrique)
         vLoisGeo.add(lois_[i]);
     else
         vLois.add(lois_[i]);

   }

   vLois.addAll(vLoisGeo);
   lois_= (ILoiHydraulique[])vLois.toArray(new ILoiHydraulique[0]);


    for (int i= 0; i < lois_.length; i++) {
      lois_[i].indice(i);
    }

  }
  @Override
  public ILoiHydraulique[]  getToutesLoisSaufGeometrique() {
    ArrayList vLois= new ArrayList();

    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    for (int i= 0; i < lois_.length; i++) {
      if (!(lois_[i] instanceof ILoiGeometrique))
          vLois.add(lois_[i]);

    }
    ILoiHydraulique[] tLois= (ILoiHydraulique[])vLois.toArray(new ILoiHydraulique[0]);

    return tLois;
  }

  @Override
  public ILoiHydraulique[]  getToutesLoisSaufTracer() {
  Vector vLois= new Vector();
  if (lois_ == null) {
    lois_= new ILoiHydraulique[0];
  }
  for (int i= 0; i < lois_.length; i++) {
    if (!(lois_[i] instanceof ILoiTracer))
      vLois.addElement(lois_[i]);
  }
  ILoiHydraulique[] tLois= new ILoiHydraulique[vLois.size()];
  for (int i= 0; i < tLois.length; i++) {
    tLois[i]= (ILoiHydraulique)vLois.elementAt(i);
  }
  return tLois;
}
  @Override
    public ILoiHydraulique[]  getToutesLoisSaufTracerEtGeometrique() {
        ILoiHydraulique[] loiSaufGeo = getToutesLoisSaufGeometrique();
        Vector vLois = new Vector();
        if (loiSaufGeo == null) {
            loiSaufGeo = new ILoiHydraulique[0];
        }
        for (int i = 0; i < loiSaufGeo.length; i++) {
            if (!(loiSaufGeo[i] instanceof ILoiTracer))
                vLois.addElement(loiSaufGeo[i]);
        }
        ILoiHydraulique[] tLois = new ILoiHydraulique[vLois.size()];
        for (int i = 0; i < tLois.length; i++) {
            tLois[i] = (ILoiHydraulique) vLois.elementAt(i);
        }
        return tLois;
    }

  @Override
  public ILoiHydrogramme[] getLoisHydrogramme() {
    Vector vLois= new Vector();
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i] instanceof ILoiHydrogramme)
        vLois.addElement(lois_[i]);
    }
    ILoiHydrogramme[] tLois= new ILoiHydrogramme[vLois.size()];
    for (int i= 0; i < tLois.length; i++) {
      tLois[i]= (ILoiHydrogramme)vLois.elementAt(i);
    }
    return tLois;
  }
  @Override
  public ILoiLimniHydrogramme[] getLoisLimniHydrogramme() {
    Vector vLois= new Vector();
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i] instanceof ILoiLimniHydrogramme)
        vLois.addElement(lois_[i]);
    }
    ILoiLimniHydrogramme[] tLois= new ILoiLimniHydrogramme[vLois.size()];
    for (int i= 0; i < tLois.length; i++) {
      tLois[i]= (ILoiLimniHydrogramme)vLois.elementAt(i);
    }
    return tLois;
  }
  @Override
  public ILoiRegulation[] getLoisRegulation() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    Vector vLois= new Vector();
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i] instanceof ILoiRegulation)
        vLois.addElement(lois_[i]);
    }
    ILoiRegulation[] tLois= new ILoiRegulation[vLois.size()];
    for (int i= 0; i < tLois.length; i++) {
      tLois[i]= (ILoiRegulation)vLois.elementAt(i);
    }
    return tLois;
  }
  @Override
  public ILoiGeometrique[] getLoisGeometrique() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    Vector vLois= new Vector();
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i] instanceof ILoiGeometrique)
        vLois.addElement(lois_[i]);
    }
    ILoiGeometrique[] tLois= new ILoiGeometrique[vLois.size()];
    for (int i= 0; i < tLois.length; i++) {
      tLois[i]= (ILoiGeometrique)vLois.elementAt(i);
    }
    return tLois;
  }
  @Override
  public ILoiLimnigramme[] getLoisLimnigramme() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    Vector vLois= new Vector();
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i] instanceof ILoiLimnigramme)
        vLois.addElement(lois_[i]);
    }
    ILoiLimnigramme[] tLois= new ILoiLimnigramme[vLois.size()];
    for (int i= 0; i < tLois.length; i++) {
      tLois[i]= (ILoiLimnigramme)vLois.elementAt(i);
    }
    return tLois;
  }
  @Override
  public ILoiOuvertureVanne[] getLoisOuvertureVanne() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    Vector vLois= new Vector();
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i] instanceof ILoiOuvertureVanne) {
        vLois.addElement(lois_[i]);
      }
    }
    ILoiOuvertureVanne[] tLois= new ILoiOuvertureVanne[vLois.size()];
    for (int i= 0; i < tLois.length; i++) {
      tLois[i]= (ILoiOuvertureVanne)vLois.elementAt(i);
    }
    return tLois;
  }
  @Override
  public ILoiTarage[] getLoisTarage() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    Vector vLois= new Vector();
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i] instanceof ILoiTarage)
        vLois.addElement(lois_[i]);
    }
    ILoiTarage[] tLois= new ILoiTarage[vLois.size()];
    for (int i= 0; i < tLois.length; i++) {
      tLois[i]= (ILoiTarage)vLois.elementAt(i);
    }
    return tLois;
  }
  @Override
  public ILoiTracer[] getLoisTracer() {
  if (lois_ == null) {
    lois_= new ILoiHydraulique[0];
  }
  Vector vLois= new Vector();
  for (int i= 0; i < lois_.length; i++) {
    if (lois_[i] instanceof ILoiTracer)
      vLois.addElement(lois_[i]);
  }
  ILoiTracer[] tLois= new ILoiTracer[vLois.size()];
  for (int i= 0; i < tLois.length; i++) {
    tLois[i]= (ILoiTracer)vLois.elementAt(i);
  }
  return tLois;
}

  @Override
  public ILoiSeuil[] getLoisSeuil() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    Vector vLois= new Vector();
    for (int i= 0; i < lois_.length; i++) {
      if (lois_[i] instanceof ILoiSeuil)
        vLois.addElement(lois_[i]);
    }
    ILoiSeuil[] tLois= new ILoiSeuil[vLois.size()];
    for (int i= 0; i < tLois.length; i++) {
      tLois[i]= (ILoiSeuil)vLois.elementAt(i);
    }
    return tLois;
  }
  @Override
  public double getTempsInitial() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    double res= Double.POSITIVE_INFINITY;
    boolean found= false;
    for (int i= 0; i < lois_.length; i++) {
      double[] t= new double[0];
      if (lois_[i] instanceof ILoiLimniHydrogramme)
        t= ((ILoiLimniHydrogramme)lois_[i]).t();
      else if (lois_[i] instanceof ILoiHydrogramme)
        t= ((ILoiHydrogramme)lois_[i]).t();
      else if (lois_[i] instanceof ILoiLimnigramme)
        t= ((ILoiLimnigramme)lois_[i]).t();
      for (int p= 0; p < t.length; p++) {
        if (t[p] < res) {
          res= t[p];
          found= true;
        }
      }
    }
    if (!found)
      res= 1.;
    return res;
  }
  @Override
  public double getTempsFinal() {
    if (lois_ == null) {
      lois_= new ILoiHydraulique[0];
    }
    double res= Double.NEGATIVE_INFINITY;
    boolean found= false;
    for (int i= 0; i < lois_.length; i++) {
      double[] t= new double[0];
      if (lois_[i] instanceof ILoiLimniHydrogramme)
        t= ((ILoiLimniHydrogramme)lois_[i]).t();
      else if (lois_[i] instanceof ILoiHydrogramme)
        t= ((ILoiHydrogramme)lois_[i]).t();
      else if (lois_[i] instanceof ILoiLimnigramme)
        t= ((ILoiLimnigramme)lois_[i]).t();
      for (int p= 0; p < t.length; p++) {
        if (t[p] > res) {
          res= t[p];
          found= true;
        }
      }
    }
    if (!found)
      res= -1.;
    return res;
  }
  @Override
  public boolean verifiePermanent(ILoiHydraulique l) {
    if (l instanceof ILoiHydrogramme)
      return ((ILoiHydrogramme)l).verifiePermanent();
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent(ILoiHydraulique l) {
    if (l instanceof ILoiHydrogramme)
      return ((ILoiHydrogramme)l).verifieTempsNonPermanent();
    return true;
  }
}
