/**
 * @file         DEtude1d.java
 * @creation     2000-07-27
 * @modification $Date: 2006-03-02 14:34:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
// import fr.dyade.koala.xml.koml.*;// import JSX.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import org.fudaa.dodico.boony.BoonyXgzDeserializer;
import org.fudaa.dodico.boony.BoonyXgzSerializer;
import org.fudaa.dodico.boony.BoonyXmlDeserializer;
import org.fudaa.dodico.boony.BoonyXmlSerializer;
import org.fudaa.dodico.corba.hydraulique1d.*;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.*;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresQualiteDEauHelper;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresQualiteDEau;

/**
 * Impl�mentation de l'objet m�tier g�n�ral "�tude 1D".
 * Contient entre autre les donn�es hydrauliques, les param�tres g�n�raux,
 * les param�tres temporels, le r�seau hydraulique, les r�sultats g�n�raux
 * d'une �tude.
 * @version      $Revision: 1.15 $ $Date: 2006-03-02 14:34:13 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DEtude1d extends DHydraulique1d implements IEtude1dOperations,IEtude1d {
  // IMPORTANT: NE PAS OUBLIER D'INCREMENTER CE NUMERO A CHAQUE MODIF DU FORMAT
  public final static String FORMAT_VERSION= "0.4";
  public final static String FORMAT_VERSION_WITH_NEW_READER= "0.4";
  public final static String FORMAT_VERSION_WITH_OLD_READER= "0.3";
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IEtude1d) {
      IEtude1d q= (IEtude1d)_o;
      description(q.description());
      donneesHydro((IDonneesHydrauliques)q.donneesHydro().creeClone());
      paramTemps(IParametresTemporelsHelper.narrow(q.paramTemps().creeClone()));
      paramGeneraux(IParametresGenerauxHelper.narrow(q.paramGeneraux().creeClone()));
      paramResultats(IParametresResultatsHelper.narrow(q.paramResultats().creeClone()));
      reseau((IReseau)q.reseau().creeClone());
      resultatsGeneraux(IResultatsGenerauxHelper.narrow(q.resultatsGeneraux().creeClone()));
      etatMenu(q.etatMenu());
      versionFormat(q.versionFormat());
      identifieurs(q.identifieurs());
      if (q.qualiteDEau() != null)
          qualiteDEau(IParametresQualiteDEauHelper.narrow(q.qualiteDEau().creeClone()));
      if (q.calageAuto() != null)
        calageAuto(ICalageAutoHelper.narrow(q.calageAuto().creeClone()));
    }
  }

  @Override
  final public IObjet creeClone() {
    IEtude1d p= UsineLib.findUsine().creeHydraulique1dEtude1d();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "etude1d";
    return s;
  }
  /*** IEtude1d ***/
  // constructeurs
  public DEtude1d() {
    super();
    description_= "Etude hydraulique1d";
    donneesHydro_= UsineLib.findUsine().creeHydraulique1dDonneesHydrauliques();
    paramTemps_= UsineLib.findUsine().creeHydraulique1dParametresTemporels();
    paramGeneraux_= UsineLib.findUsine().creeHydraulique1dParametresGeneraux();
    paramResultats_= UsineLib.findUsine().creeHydraulique1dParametresResultats();
    reseau_= UsineLib.findUsine().creeHydraulique1dReseau();
    resultatsGeneraux_=
      UsineLib.findUsine().creeHydraulique1dResultatsGeneraux();
    calageAuto_=UsineLib.findUsine().creeHydraulique1dCalageAuto();
    etatMenu_= 0;
    versionFormat_= FORMAT_VERSION;
    identifieurs_= new SIdentifieur[0];
    qualiteDEau_ = UsineLib.findUsine().creeHydraulique1dParametresQualiteDEau();

  }
  @Override
  public void dispose() {
    description_= null;
    if (donneesHydro_ != null) {
      donneesHydro_.dispose();
      donneesHydro_= null;
    }
    if (paramTemps_ != null) {
      paramTemps_.dispose();
      paramTemps_= null;
    }
    if (paramGeneraux_ != null) {
      paramGeneraux_.dispose();
      paramGeneraux_= null;
    }
    if (paramResultats_ != null) {
      paramResultats_.dispose();
      paramResultats_= null;
    }
    if (reseau_ != null) {
      reseau_.dispose();
      reseau_= null;
    }
    if (resultatsGeneraux_ != null) {
      resultatsGeneraux_.dispose();
      resultatsGeneraux_= null;
    }
    if (qualiteDEau_ != null) {
        qualiteDEau_.dispose();
        qualiteDEau_=null;
    }
    if (calageAuto_ != null) {
        calageAuto_.dispose();
        calageAuto_ = null;
    }
    etatMenu_= 0;
    versionFormat_= null;
    identifieurs_= null;
    Identifieur.IDENTIFIEUR.table_= new Hashtable();
    super.dispose();
    System.gc();
  }

  // Attributs
  private String description_;
  @Override
  public String description() {
    return description_;
  }
  @Override
  public void description(String s) {
    if (description_.equals(s)) return;
    description_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "description");
  }
  private IDonneesHydrauliques donneesHydro_;
  @Override
  public IDonneesHydrauliques donneesHydro() {
    return donneesHydro_;
  }
  @Override
  public void donneesHydro(IDonneesHydrauliques s) {
    if (donneesHydro_==s) return;
    donneesHydro_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "donneesHydro");
  }
  private IParametresTemporels paramTemps_;
  @Override
  public IParametresTemporels paramTemps() {
    return paramTemps_;
  }
  @Override
  public void paramTemps(IParametresTemporels s) {
    if (paramTemps_==s) return;
    paramTemps_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "paramTemps");
  }
  private IParametresGeneraux paramGeneraux_;
  @Override
  public IParametresGeneraux paramGeneraux() {
    return paramGeneraux_;
  }
  @Override
  public void paramGeneraux(IParametresGeneraux s) {
    if (paramGeneraux_==s) return;
    paramGeneraux_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "paramGeneraux");
  }
  private IParametresResultats paramResultats_;
  @Override
  public IParametresResultats paramResultats() {
    return paramResultats_;
  }
  @Override
  public void paramResultats(IParametresResultats s) {
    if (paramResultats_==s) return;
    paramResultats_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "paramResultats");
  }
  private IReseau reseau_;
  @Override
  public IReseau reseau() {
    return reseau_;
  }
  @Override
  public void reseau(IReseau s) {
    if (reseau_==s) return;
    reseau_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "reseau");
  }
  private IResultatsGeneraux resultatsGeneraux_;
  @Override
  public IResultatsGeneraux resultatsGeneraux() {
    return resultatsGeneraux_;
  }
  @Override
  public void resultatsGeneraux(IResultatsGeneraux s) {
    if (resultatsGeneraux_==s) return;
    resultatsGeneraux_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "resultatsGeneraux");
  }
  private int etatMenu_;
  @Override
  public int etatMenu() {
    return etatMenu_;
  }
  @Override
  public void etatMenu(int s) {
    if (etatMenu_==s) return;
    etatMenu_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "etatMenu");
  }
  private String versionFormat_;
  @Override
  public String versionFormat() {
    return versionFormat_;
  }
  @Override
  public void versionFormat(String s) {
    if (versionFormat_.equals(s)) return;
    versionFormat_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "versionFormat");
  }
  private SIdentifieur[] identifieurs_;
  @Override
  public SIdentifieur[] identifieurs() {
    return identifieurs_;
  }
  @Override
  public void identifieurs(SIdentifieur[] s) {
    identifieurs_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "identifieurs");
  }

  private ICalageAuto calageAuto_;
  @Override
  public ICalageAuto calageAuto() {
    return calageAuto_;
  }
  @Override
  public void calageAuto(ICalageAuto _calageAuto) {
    if (calageAuto_==_calageAuto) return;
    calageAuto_=_calageAuto;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "calageAuto");
  }

  private IParametresQualiteDEau qualiteDEau_;
  @Override
   public IParametresQualiteDEau qualiteDEau() {
       return qualiteDEau_;
   }

  @Override
   public void qualiteDEau(IParametresQualiteDEau s) {
       if (qualiteDEau_ == s)
           return;
       qualiteDEau_ = s;
       UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                             "qualiteDEau");
   }




  // methodes
  @Override
  public byte[] toByteArray() {
    ByteArrayOutputStream baos= new ByteArrayOutputStream(65536);
    //    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
      BoonyXmlSerializer fluxEcrit= new BoonyXgzSerializer();
      fluxEcrit.open(baos);
      //      Object[] objets = new Object[3];
      //      objets[0] = versionFormat();
      SIdentifieur[] ids=
        new SIdentifieur[Identifieur.IDENTIFIEUR.table_.size()];
      //      Object[] identifieur = new Object[Identifieur.IDENTIFIEUR.table_.size()];
      Enumeration cles= Identifieur.IDENTIFIEUR.table_.keys();
      int i= 0;
      while (cles.hasMoreElements()) {
        SIdentifieur id= new SIdentifieur();
        id.nom= (String)cles.nextElement();
        id.numero=
          ((Integer)Identifieur.IDENTIFIEUR.table_.get(id.nom)).intValue();
        ids[i]= id;
        /*        Object[] item = new Object[2];
                item[0] = cles.nextElement();
                item[1] = Identifieur.IDENTIFIEUR.table_.get(item[0]);
                identifieur[i] = item;*/
        i++;
      }
      identifieurs(ids);
      /*      objets[1] = identifieur;
            objets[2] = this;
            fluxEcrit.write(objets);*/
      fluxEcrit.write(this);
      fluxEcrit.close(); // classique et koml
    } catch (Exception ex) {
      System.err.println("$$$ " + ex);
      ex.printStackTrace();
    }
    return baos.toByteArray();
  }
  @Override
  public IEtude1d initFromByteArray(byte[] buffer, boolean compresse,String _version) {
    IEtude1d etude= null;
    try {
      ByteArrayInputStream bais= new ByteArrayInputStream(buffer);
      BoonyXmlDeserializer fluxLu;
      if (compresse)
        fluxLu= new BoonyXgzDeserializer();
      else
        fluxLu= new BoonyXmlDeserializer();
      fluxLu.open(bais);
      //      Object[] objets = (Object[]) fluxLu.read();
      if(FORMAT_VERSION_WITH_OLD_READER.compareTo(_version)>=0){
        System.out.println("READ OLD");
        etude= IEtude1dHelper.narrow((org.omg.CORBA.Object)fluxLu.readOld());
      }
      else {
        etude = IEtude1dHelper.narrow( (org.omg.CORBA.Object) fluxLu.read());
      }
      if(etude==null) return etude;
      //      String version = ((String) objets[0]).trim();
      String version= etude.versionFormat().trim();
      if (FORMAT_VERSION.compareTo(version) > 0) {
        //pour prendre en compte les nouveaux lecteurs/ecriveurs d'objets m�tiers
        if(version.equals(FORMAT_VERSION_WITH_OLD_READER) && FORMAT_VERSION.equals(FORMAT_VERSION_WITH_NEW_READER)){
         etude.versionFormat(FORMAT_VERSION);
        }
        else{
        System.err.println("le format de fichier est d�suet : ");
        System.err.println(" Version fichier Etude hydraulique1d : " + version);
        System.err.println(
          " Version actuel  Etude hydraulique1d : " + FORMAT_VERSION);
        }
      } else if (FORMAT_VERSION.compareTo(version) > 0) {
        System.err.println(
          "le fichier est incorrect (n� version impossible) : ");
        System.err.println(" Version fichier Etude hydraulique1d : " + version);
        System.err.println(
          " Version actuel  Etude hydraulique1d : " + FORMAT_VERSION);
        System.err.println("Contacter le service de maintenance.");
      } else {
        //        Object[] identifieur = (Object[]) objets[1];
        SIdentifieur[] ids= etude.identifieurs();
        Hashtable table= new Hashtable();
        //        for (int i=0; i<identifieur.length ; i++) {
        for (int i= 0; i < ids.length; i++) {
          //          Object[] item = (Object[])identifieur[i];
          //          table.put(item[0], item[1]);
          table.put(ids[i].nom, new Integer(ids[i].numero));
        }
        Identifieur.IDENTIFIEUR.table_= table;
        //        etude = (DEtude1d) objets[2];
      }
      fluxLu.close();
    } catch (Exception ex) {
      System.err.println("$$$ " + ex);
      ex.printStackTrace();
    }
    if (etude != null) {
      etude.reseau().initIndiceNumero();
      etude.donneesHydro().initIndiceLois();
    }
    return etude;
  }
}
