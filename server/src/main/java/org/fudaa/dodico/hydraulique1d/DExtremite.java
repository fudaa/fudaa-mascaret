/**
 * @file         DExtremite.java
 * @creation     2000-07-24
 * @modification $Date: 2006-06-16 12:30:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.geometrie.SPoint2D;
import org.fudaa.dodico.corba.hydraulique1d.IExtremite;
import org.fudaa.dodico.corba.hydraulique1d.IExtremiteOperations;
import org.fudaa.dodico.corba.hydraulique1d.ILimite;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.ILimiteQualiteDEau;
import org.fudaa.dodico.corba.hydraulique1d.INoeud;
import org.fudaa.dodico.corba.hydraulique1d.IProfil;
import org.fudaa.dodico.corba.hydraulique1d.LLimiteCalcule;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "Extremite d'un bief".
 * On peut y associer une condition limite (ILimite), un noeud ou un profil.
 * De plus pour le noyau transcritique, on utilise 3 points (1, 2 et milieu) et un angle.
 * @version      $Revision: 1.14 $ $Date: 2006-06-16 12:30:16 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public class DExtremite
  extends DHydraulique1d
  implements IExtremite,IExtremiteOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IExtremite) {
      IExtremite q= (IExtremite)_o;
      profilRattache((IProfil)q.profilRattache());
      noeudRattache((INoeud)q.noeudRattache());
      conditionLimite((ILimite)q.conditionLimite());
      conditionLimiteQualiteDEau((ILimiteQualiteDEau)q.conditionLimiteQualiteDEau());
      point1(q.point1());
      point2(q.point2());
      pointMilieu(q.pointMilieu());
      angle(q.angle());
    }
  }
  @Override
  final public IObjet creeClone() {
    IExtremite p= UsineLib.findUsine().creeHydraulique1dExtremite();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "extrémité " + numero_;
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Extremité bief";
    if (conditionLimite_ == null) {
      res[0]= "Extremité bief "+numero_;
      res[1]= "";
    } else {
      res[0]= "Extremité libre "+numero_;
      res[1]= "";
      if (conditionLimite_.loi() != null) {
        res[1]=
          "loi : "
            + conditionLimite_.loi().typeLoi()
            + " "
            + conditionLimite_.loi().nom();
      } else {
        if (conditionLimite_.typeLimiteCalcule().value()
          == LLimiteCalcule._EVACUATION_LIBRE)
          res[1]= "Evacuation libre";
        else if (
          conditionLimite_.typeLimiteCalcule().value()
            == LLimiteCalcule._HAUTEUR_NORMALE)
          res[1]= "Hauteur normale";
      }
    }
    return res;
  }
  /*** IExtremite ***/
  // constructeurs
  public DExtremite() {
    super();
    numero_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    profilRattache_= null;
    noeudRattache_= null;
    conditionLimite_= null;
    creeLimiteQualiteDEau();
    // conditionLimiteQualiteDEau_= null;
    point1_= null;
    point2_= null;
    pointMilieu_= null;
    angle_= 0.;
  }
  @Override
  public void dispose() {
    profilRattache_= null;
    noeudRattache_= null;
    conditionLimite_= null;
    conditionLimiteQualiteDEau_=null;
    point1_= null;
    point2_= null;
    pointMilieu_= null;
    angle_= 0.;
    super.dispose();
  }
  // attributs
  private int numero_;
  @Override
  public int numero() {
    return numero_;
  }
  @Override
  public void numero(int s) {
    if (numero_==s) return;
    numero_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numero");
  }
  private IProfil profilRattache_;
  @Override
  public IProfil profilRattache() {
    return profilRattache_;
  }
  @Override
  public void profilRattache(IProfil s) {
    if (profilRattache_==s) return;
    profilRattache_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "profilRattache");
  }
  private INoeud noeudRattache_;
  @Override
  public INoeud noeudRattache() {
    return noeudRattache_;
  }
  @Override
  public void noeudRattache(INoeud s) {
    if (noeudRattache_==s) return;
    noeudRattache_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "noeudRattache");
  }
  private ILimite conditionLimite_;
  @Override
  public ILimite conditionLimite() {
    return conditionLimite_;
  }
  @Override
  public void conditionLimite(ILimite s) {
    if (conditionLimite_==s) return;
    if (s != null)
      s.nom("limite" + numero());
    conditionLimite_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "conditionLimite");
  }
  private ILimiteQualiteDEau conditionLimiteQualiteDEau_;
  @Override
  public ILimiteQualiteDEau conditionLimiteQualiteDEau() {
      return conditionLimiteQualiteDEau_;
  }

  @Override
  public void conditionLimiteQualiteDEau(ILimiteQualiteDEau s) {
      if (conditionLimiteQualiteDEau_ == s)
          return;
      if (s != null)
      conditionLimiteQualiteDEau_ = s;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                            "conditionLimiteQualiteDEau");
  }

  private SPoint2D point1_;
  @Override
  public SPoint2D point1() {
    return point1_;
  }
  @Override
  public void point1(SPoint2D s) {
    if (point1_==s) return;
    if ((point1_ != null)&&(s != null)) {
      if ( (point1_.x == s.x) && (point1_.y == s.y))
        return;
    }
    point1_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "point1");
  }
  private SPoint2D point2_;
  @Override
  public SPoint2D point2() {
    return point2_;
  }
  @Override
  public void point2(SPoint2D s) {
    if (point2_==s) return;
    if ((point2_ != null)&&(s != null)) {
      if ( (point2_.x == s.x) && (point2_.y == s.y))
        return;
    }
    point2_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "point2");
  }
  private SPoint2D pointMilieu_;
  @Override
  public SPoint2D pointMilieu() {
    return pointMilieu_;
  }
  @Override
  public void pointMilieu(SPoint2D s) {
    if (pointMilieu_==s) return;
    if ((pointMilieu_ != null)&&(s != null)) {
      if ( (pointMilieu_.x == s.x) && (pointMilieu_.y == s.y))
        return;
    }
    pointMilieu_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pointMilieu");
  }
  private double angle_;
  @Override
  public double angle() {
    return angle_;
  }
  @Override
  public void angle(double s) {
    if (angle_==s) return;
    angle_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "angle");
  }
  // methode
  @Override
  public void creePoints() {
    point1(new SPoint2D(0, 0));
    point2(new SPoint2D(0, 0));
    pointMilieu(new SPoint2D(0, 0));
  }
  @Override
  public ILimite creeLimite() {
    ILimite limite= UsineLib.findUsine().creeHydraulique1dLimite();
    conditionLimite(limite);
    return conditionLimite_;
  }

  @Override
  public ILimiteQualiteDEau creeLimiteQualiteDEau() {
  ILimiteQualiteDEau limiteQualiteDEau= UsineLib.findUsine().creeHydraulique1dLimiteQualiteDEau();
  conditionLimiteQualiteDEau(limiteQualiteDEau);
  return conditionLimiteQualiteDEau_;
}



  @Override
  public INoeud creeNoeud() {
    INoeud noeud= UsineLib.findUsine().creeHydraulique1dNoeud();
    noeud.ajouteExtremite((IExtremite)tie());
    noeudRattache(noeud);
    return noeudRattache_;
  }
}
