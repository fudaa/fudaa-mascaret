/**
 * @file         DHydraulique1d.java
 * @creation     2001-07-16
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IHydraulique1dOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.DObjet;
/**
 * Classe de base abstraite des objets m�tier hydraulique1d.
 *
 * @version      $Revision: 1.6 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public abstract class DHydraulique1d
  extends DObjet
  implements IHydraulique1d,IHydraulique1dOperations {
  protected DHydraulique1d() {
    super();
  }
  @Override
  public void dispose() {
    super.dispose();
  }
  @Override
  public void initialise(IObjet _o) {}

  /**
   * Implementation par d�faut de la m�thode d�fini dans l'IDL.
   * @return String[]
   */
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= enChaine();
    res[1]= "";
    return res;
  }
  protected final static boolean egale(IHydraulique1d[] p, IHydraulique1d[] p2) {
    if (p == p2)
      return true;
    if (p == null || p2 == null)
      return false;

    int length = p.length;
    if (p2.length != length)
      return false;

    for (int i = 0; i < length; i++)
      if (p[i] != p2[i])
        return false;

    return true;
  }
}
