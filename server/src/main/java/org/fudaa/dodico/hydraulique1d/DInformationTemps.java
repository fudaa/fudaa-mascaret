/**
 * @file         DInformationTemps.java
 * @creation     2000-08-10
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IInformationTemps;
import org.fudaa.dodico.corba.hydraulique1d.IInformationTempsOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "Information sur le Temps".
 * Pas utilisé dans Fudaa-Mascaret.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DInformationTemps
  extends DHydraulique1d
  implements IInformationTemps,IInformationTempsOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IInformationTemps) {
      IInformationTemps q= (IInformationTemps)_o;
      info(q.info());
      numeroPas(q.numeroPas());
      t(q.t());
      dt(q.dt());
      dtlevy(q.dtlevy());
      jour(q.jour());
      heure(q.heure());
      minute(q.minute());
      seconde(q.seconde());
    }
  }
  @Override
  final public IObjet creeClone() {
    IInformationTemps p=
      UsineLib.findUsine().creeHydraulique1dInformationTemps();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "infoTemps(pas " + numeroPas_ + ",t " + t_ + ")";
    return s;
  }
  /*** IInformationTemps ***/
  // constructeurs
  public DInformationTemps() {
    super();
    info_= "";
    numeroPas_= -1;
    t_= 0.;
    dt_= 0.;
    dtlevy_= 0.;
    jour_= 0;
    heure_= 0;
    minute_= 0;
    seconde_= 0;
  }
  @Override
  public void dispose() {
    info_= null;
    numeroPas_= -1;
    t_= 0.;
    dt_= 0.;
    dtlevy_= 0.;
    jour_= 0;
    heure_= 0;
    minute_= 0;
    seconde_= 0;
    super.dispose();
  }
  // Attributs
  private String info_;
  @Override
  public String info() {
    return info_;
  }
  @Override
  public void info(String s) {
    if (info_.equals(s)) return;
    info_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "info");
  }
  private int numeroPas_;
  @Override
  public int numeroPas() {
    return numeroPas_;
  }
  @Override
  public void numeroPas(int s) {
    if (numeroPas_==s) return;
    numeroPas_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numeroPas");
  }
  private double t_;
  @Override
  public double t() {
    return t_;
  }
  @Override
  public void t(double s) {
    if (t_==s) return;
    t_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "t");
  }
  private double dt_;
  @Override
  public double dt() {
    return dt_;
  }
  @Override
  public void dt(double s) {
    if (dt_==s) return;
    dt_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "dt");
  }
  private double dtlevy_;
  @Override
  public double dtlevy() {
    return dtlevy_;
  }
  @Override
  public void dtlevy(double s) {
    if (dtlevy_==s) return;
    dtlevy_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "dtlevy");
  }
  private int jour_;
  @Override
  public int jour() {
    return jour_;
  }
  @Override
  public void jour(int s) {
    if (jour_==s) return;
    jour_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "jour");
  }
  private int heure_;
  @Override
  public int heure() {
    return heure_;
  }
  @Override
  public void heure(int s) {
    if (heure_==s) return;
    heure_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "heure");
  }
  private int minute_;
  @Override
  public int minute() {
    return minute_;
  }
  @Override
  public void minute(int s) {
    if (minute_==s) return;
    minute_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "minute");
  }
  private int seconde_;
  @Override
  public int seconde() {
    return seconde_;
  }
  @Override
  public void seconde(int s) {
    if (seconde_==s) return;
    seconde_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "seconde");
  }
}
