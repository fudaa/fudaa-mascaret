/**
 * @file         DLaisse.java
 * @creation     2000-12-05
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.ILaisse;
import org.fudaa.dodico.corba.hydraulique1d.ILaisseOperations;
import org.fudaa.dodico.corba.hydraulique1d.ISite;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "Un élément d'une laisse de crue".
 * Associe une cote, un site (une abscisse sur un bief) et un nom.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DLaisse extends DHydraulique1d implements ILaisse,ILaisseOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ILaisse) {
      ILaisse q= (ILaisse)_o;
      nom(q.nom());
      site(q.site());
      cote(q.cote());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILaisse p= UsineLib.findUsine().creeHydraulique1dLaisse();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "laisse " + nom_;
    if (site_ != null)
      s += " (" + site_.toString() + ")";
    return s;
  }
  /*** ILaisse ***/
  // constructeurs
  public DLaisse() {
    super();
    site_= UsineLib.findUsine().creeHydraulique1dSite();
    nom_= "Laisse";
    cote_= 0.;
  }
  @Override
  public void dispose() {
    site_= null;
    nom_= null;
    cote_= 0.;
    super.dispose();
  }
  // attributs
  private String nom_;
  @Override
  public String nom() {
    return nom_;
  }
  @Override
  public void nom(String s) {
    if (nom_.equals(s)) return;
    nom_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nom");
  }
  private ISite site_;
  @Override
  public ISite site() {
    return site_;
  }
  @Override
  public void site(ISite s) {
    if (site_==s) return;
    site_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "site");
  }
  private double cote_;
  @Override
  public double cote() {
    return cote_;
  }
  @Override
  public void cote(double s) {
    if (cote_==s) return;
    cote_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "cote");
  }
}
