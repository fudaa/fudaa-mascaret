/*
 * @file         DLiaison.java
 * @creation
 * @modification $Date: 2006-12-12 08:56:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ICasier;
import org.fudaa.dodico.corba.hydraulique1d.ILiaison;
import org.fudaa.dodico.corba.hydraulique1d.ILiaisonOperations;
import org.fudaa.dodico.corba.hydraulique1d.LSensDebitLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ICaracteristiqueLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.IChenalLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.IOrificeLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ISeuilLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ISiphonLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ITopologieLiaison;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "Liaison casier".
 * Associe des caractéristiques d'une liaison et une topologie (relation avec les autres éléments du réseau).
 * @version      $Revision: 1.12 $ $Date: 2006-12-12 08:56:30 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DLiaison extends DHydraulique1d implements ILiaison,ILiaisonOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ILiaison) {
      ILiaison q= (ILiaison)_o;
      if (q.topologie() != null)
        topologie((ITopologieLiaison)q.topologie().creeClone());
      if (q.caracteristiques() != null)
        caracteristiques(
          (ICaracteristiqueLiaison)q.caracteristiques().creeClone());
      numero(q.numero());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILiaison p= UsineLib.findUsine().creeHydraulique1dLiaison();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    return " liaison id :"+id_+" numero :" + numero_+" instance "+super.toString();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Liaison"+numero_;
    res[1]= "";
    if (topologie_ != null) {
      res[1]= topologie_.getInfos()[1] + " ";
    }
    if (caracteristiques_ != null) {
      res[0] += " " + caracteristiques_.getInfos()[0];
      res[1] += caracteristiques_.getInfos()[1];
    }
    return res;
  }
  /*** ILiaison ***/
  // constructeurs
  public DLiaison() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    //System.err.println("Constructeur liaison !"+this.toString());
    topologie_= null;
    caracteristiques_= UsineLib.findUsine().creeHydraulique1dSeuilLiaison();
  }
  @Override
  public void dispose() {
    id_= 0;
    topologie_= null;
    caracteristiques_= null;
    super.dispose();
  }
  // attributs
  private int id_;
  private int numero_;
  @Override
  public int numero() {
    return numero_;
  }
  @Override
  public void numero(int s) {
    if (numero_ == s) return;
    numero_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numero");
  }
  private ITopologieLiaison topologie_;
  @Override
  public ITopologieLiaison topologie() {
    return topologie_;
  }
  @Override
  public void topologie(ITopologieLiaison s) {
    if (topologie_==s) return;
    topologie_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "topologie");
  }
  private ICaracteristiqueLiaison caracteristiques_;
  @Override
  public ICaracteristiqueLiaison caracteristiques() {
    return caracteristiques_;
  }
  @Override
  public void caracteristiques(ICaracteristiqueLiaison s) {
    if (caracteristiques_==s) return;
    caracteristiques_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "caracteristiques");
  }
  // méthodes
  @Override
  public boolean isRiviereCasier() {
    if (topologie_ == null) {
      ITopologieLiaison g=
        UsineLib.findUsine().creeHydraulique1dTopologieRiviereCasier();
      topologie(g);
      return true;
    }
    return topologie_.isRiviereCasier();
  }
  @Override
  public void toRiviereCasier() {
    if (isRiviereCasier())
      return;
    ITopologieLiaison g=
      UsineLib.findUsine().creeHydraulique1dTopologieRiviereCasier();
    topologie(g);
  }
  @Override
  public boolean isCasierCasier() {
    if (topologie_ == null) {
      ITopologieLiaison g=
        UsineLib.findUsine().creeHydraulique1dTopologieCasierCasier();
      topologie(g);
      return true;
    }
    return topologie_.isCasierCasier();
  }
  @Override
  public void toCasierCasier() {
    if (isCasierCasier())
      return;
    ITopologieLiaison g=
      UsineLib.findUsine().creeHydraulique1dTopologieCasierCasier();
    topologie(g);
  }
  @Override
  public double getAbscisse() {
    return topologie_.getAbscisse();
  }
  @Override
  public void setAbscisse(double abscisse) {
    topologie_.setAbscisse(abscisse);
  }
  @Override
  public ICasier getCasierRattache() {
    return topologie_.getCasierRattache();
  }
  @Override
  public void setCasierRattache(ICasier casierRattache) {
    topologie_.setCasierRattache(casierRattache);
  }
  @Override
  public IBief getBiefRattache() {
    return topologie_.getBiefRattache();
  }
  @Override
  public void setBiefRattache(IBief biefRattache) {
    topologie_.setBiefRattache(biefRattache);
  }
  @Override
  public ICasier getCasierAmontRattache() {
    return topologie_.getCasierAmontRattache();
  }
  @Override
  public void setCasierAmontRattache(ICasier casierRattache) {
    topologie_.setCasierAmontRattache(casierRattache);
  }
  @Override
  public ICasier getCasierAvalRattache() {
    return topologie_.getCasierAvalRattache();
  }
  @Override
  public void setCasierAvalRattache(ICasier casierRattache) {
    topologie_.setCasierAvalRattache(casierRattache);
  }
  @Override
  public boolean isSeuil() {
    return caracteristiques_.isSeuil();
  }
  @Override
  public void toSeuil() {
    if (isSeuil())
      return;
    double z = getCote();
    supprimeCaracteristique();
    ICaracteristiqueLiaison carac=
      UsineLib.findUsine().creeHydraulique1dSeuilLiaison();
    carac.setCote(z);
    caracteristiques(carac);
  }
  @Override
  public boolean isChenal() {
    return caracteristiques_.isChenal();
  }
  @Override
  public void toChenal() {
    if (isChenal())
      return;
    double z = getCote();
    supprimeCaracteristique();
    ICaracteristiqueLiaison carac=
      UsineLib.findUsine().creeHydraulique1dChenalLiaison();
    carac.setCote(z);
    caracteristiques(carac);
  }
  @Override
  public boolean isSiphon() {
    return caracteristiques_.isSiphon();
  }
  @Override
  public void toSiphon() {
    if (isSiphon())
      return;
    double z = getCote();
    supprimeCaracteristique();
    ICaracteristiqueLiaison carac=
      UsineLib.findUsine().creeHydraulique1dSiphonLiaison();
    carac.setCote(z);
    caracteristiques(carac);
  }
  @Override
  public boolean isOrifice() {
    return caracteristiques_.isOrifice();
  }
  @Override
  public void toOrifice() {
    if (isOrifice())
      return;
    double z = getCote();
    supprimeCaracteristique();
    ICaracteristiqueLiaison carac=
      UsineLib.findUsine().creeHydraulique1dOrificeLiaison();
    carac.setCote(z);
    caracteristiques(carac);
  }
  @Override
  public double getCote() {
    return caracteristiques_.getCote();
  }
  @Override
  public void setCote(double cote) {
    caracteristiques_.setCote(cote);
  }
  @Override
  public double getCoefQOrifice() {
    return caracteristiques_.getCoefQOrifice();
  }
  @Override
  public void setCoefQOrifice(double coefQOrifice) {
    caracteristiques_.setCoefQOrifice(coefQOrifice);
  }
  @Override
  public double getLargeur() {
    return caracteristiques_.getLargeur();
  }
  @Override
  public void setLargeur(double largeur) {
    caracteristiques_.setLargeur(largeur);
  }
  @Override
  public double getLongueur() {
    return caracteristiques_.getLongueur();
  }
  @Override
  public void setLongueur(double longueur) {
    caracteristiques_.setLongueur(longueur);
  }
  @Override
  public double getCoefQ() {
    return caracteristiques_.getCoefQ();
  }
  @Override
  public void setCoefQ(double coefQ) {
    caracteristiques_.setCoefQ(coefQ);
  }
  @Override
  public double getSection() {
    return caracteristiques_.getSection();
  }
  @Override
  public void setSection(double section) {
    caracteristiques_.setSection(section);
  }
  @Override
  public double getCoefActivation() {
    return caracteristiques_.getCoefActivation();
  }
  @Override
  public void setCoefActivation(double coefActivation) {
    caracteristiques_.setCoefActivation(coefActivation);
  }
  @Override
  public double getRugosite() {
    return caracteristiques_.getRugosite();
  }
  @Override
  public void setRugosite(double rugosite) {
    caracteristiques_.setRugosite(rugosite);
  }
  @Override
  public double getCoefPerteCharge() {
    return caracteristiques_.getCoefPerteCharge();
  }
  @Override
  public void setCoefPerteCharge(double coefPerteCharge) {
    caracteristiques_.setCoefPerteCharge(coefPerteCharge);
  }
  @Override
  public LSensDebitLiaison getSensDebit() {
    return caracteristiques_.getSensDebit();
  }
  @Override
  public void setSensDebit(LSensDebitLiaison sensDebit) {
    caracteristiques_.setSensDebit(sensDebit);
  }
  private void supprimeCaracteristique() {
    if (caracteristiques_ == null) return;
    if (caracteristiques_.isChenal()) {
      IChenalLiaison li = (IChenalLiaison)caracteristiques_;
      UsineLib.findUsine().supprimeHydraulique1dChenalLiaison(li);
    } else if (caracteristiques_.isOrifice()) {
      IOrificeLiaison li = (IOrificeLiaison)caracteristiques_;
      UsineLib.findUsine().supprimeHydraulique1dOrificeLiaison(li);
    } else if (caracteristiques_.isSeuil()) {
      ISeuilLiaison li = (ISeuilLiaison) caracteristiques_;
      UsineLib.findUsine().supprimeHydraulique1dSeuilLiaison(li);
    } else if (caracteristiques_.isSiphon()) {
      ISiphonLiaison li = (ISiphonLiaison) caracteristiques_;
      UsineLib.findUsine().supprimeHydraulique1dSiphonLiaison(li);
    }

  }
}
