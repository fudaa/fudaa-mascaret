/**
 * @file         DLigneEauInitiale.java
 * @creation     2000-08-09
 * @modification $Date: 2005-09-01 17:12:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.ILigneEauInitiale;
import org.fudaa.dodico.corba.hydraulique1d.ILigneEauInitialeOperations;
import org.fudaa.dodico.corba.hydraulique1d.ILigneEauPoint;
import org.fudaa.dodico.corba.hydraulique1d.LFormatFichier;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.corba.usine.IUsine;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "ligne d'eau initiale".
 * Contient un tableau de points de type ILigneEauPoint.
 * @version      $Revision: 1.11 $ $Date: 2005-09-01 17:12:46 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DLigneEauInitiale
  extends DHydraulique1d
  implements ILigneEauInitiale,ILigneEauInitialeOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ILigneEauInitiale) {
      ILigneEauInitiale l= (ILigneEauInitiale)_o;
      nom(l.nom());
      format(l.format());
      if (l.points() != null) {
        ILigneEauPoint[] ip= new ILigneEauPoint[l.points().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (ILigneEauPoint)l.points()[i].creeClone();
        points(ip);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    ILigneEauInitiale l=
      UsineLib.findUsine().creeHydraulique1dLigneEauInitiale();
    l.initialise(tie());
    return l;
  }
  @Override
  final public String toString() {
    String s= "ligneEauInitiale";
    return s;
  }
  /*** ILigneEauInitiale ***/
  // constructeurs
  public DLigneEauInitiale() {
    super();
    nom_= "";
    //    lig.format(LFormatFichier.LIDO_PERMANENT); le format est null (rentr� au clavier par d�faut)
    format_= null;
    points_= new ILigneEauPoint[0];
  }
  @Override
  public void dispose() {
    nom_= null;
    format_= null;
    points_= null;
    super.dispose();
  }
  // attributs
  private String nom_;
  @Override
  public String nom() {
    return nom_;
  }
  @Override
  public void nom(String nom) {
    if (nom_.equals(nom)) return;
    nom_= nom;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nom");
  }
  private ILigneEauPoint[] points_;
  @Override
  public ILigneEauPoint[] points() {
    return points_;
  }
  @Override
  public void points(ILigneEauPoint[] points) {
    if (egale(points_, points)) return;
    points_= points;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "points");
  }
  private LFormatFichier format_;
  @Override
  public LFormatFichier format() {
    return format_;
  }
  @Override
  public void format(LFormatFichier format) {
    if (format_.value()==format.value()) return;
    format_= format;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "format");
  }
  // methodes
  @Override
  public int[][] delimitationsSectionsBiefs() {
    int[][] delim= null;
    int biefprec= 0, sect1= 0;
    Vector vdelim= new Vector();
    if ((points_ != null) && (points_.length > 0)) {
      biefprec= points_[0].numeroBief();
      sect1= 0;
    }
    for (int i= 1; i < points_.length; i++) {
      int b= points_[i].numeroBief();
      if (biefprec == -1) {
        if (b == -1) {
          continue;
        } else {
          sect1= i - 1;
          biefprec= b;
          continue;
        }
      }
      if ((b != biefprec) || (i == points_.length - 1)) {
        int[] del= new int[2];
        del[0]= sect1 + 1;
        if (i == points_.length - 1)
          del[1]= points_.length;
        else
          del[1]= i;
        vdelim.add(del);
        biefprec= b;
        sect1= i - 1;
      }
    }
    delim= new int[vdelim.size()][2];
    for (int i= 0; i < delim.length; i++)
      delim[i]= (int[])vdelim.get(i);
    return delim;
  }
  @Override
  public void creePointALaFin() {
    ILigneEauPoint point= UsineLib.findUsine().creeHydraulique1dLigneEauPoint();
    ILigneEauPoint[] us= new ILigneEauPoint[points_.length + 1];
    for (int i= 0; i < points_.length; i++)
      us[i]= points_[i];
    us[points_.length]= point;
    points(us);
  }
  @Override
  public ILigneEauPoint[] creePoints(int nombre) {
    ILigneEauPoint[] res= new ILigneEauPoint[nombre];
    IUsine usine= UsineLib.findUsine();
    for (int i= 0; i < nombre; i++)
      res[i]= usine.creeHydraulique1dLigneEauPoint();
    return res;
  }
  @Override
  public void supprimePoints(ILigneEauPoint[] points) {
    Vector newus= new Vector();
    for (int i= 0; i < points_.length; i++) {
      boolean trouve= false;
      for (int j= 0; j < points.length; j++) {
        if (points_[i] == points[j])
          trouve= true;
      }
      if (!trouve)
        newus.add(points_[i]);
    }
    ILigneEauPoint[] us= new ILigneEauPoint[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (ILigneEauPoint)newus.get(i);
    points(us);
  }
  @Override
  public void supprimePoint(ILigneEauPoint point) {
    UsineLib.findUsine().supprimeHydraulique1dLigneEauPoint(point);
  }
  @Override
  public void supprimePointsBiefNumero(int numeroBief) {
    System.out.println("DLigneEauInitiale supprimePointsBiefNumero("+numeroBief+")");
    System.out.println("Avant : points_.length="+points_.length);
    for (int i = 0; i < points_.length; i++) {
      System.out.println(points_[i].numeroBief()+" "+points_[i].abscisse());
    }
    Vector newus= new Vector();
    for (int i= 0; i < points_.length; i++) {
      if (numeroBief == points_[i].numeroBief()) {
        supprimePoint(points_[i]);
      } else {
        newus.add(points_[i]);
      }
    }
    if (newus.size() == points_.length) {
      // aucune suppression n'a �t� faite.
      return;
    }
    ILigneEauPoint[] us= new ILigneEauPoint[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (ILigneEauPoint)newus.get(i);
    points(us);
    System.out.println("Apres : points_.length="+points_.length);
    for (int i = 0; i < points_.length; i++) {
      System.out.println(points_[i].numeroBief()+" "+points_[i].abscisse());
    }
  }
  @Override
  public void miseAJourNumeroBiefPointsLigneEauInit(int nouveauNumero, int ancienNumero) {
    System.out.println("DLigneEauInitiale miseAJourNumeroBiefPointsLigneEauInit(nouveau="+nouveauNumero+", ancien="+ancienNumero+")");
    System.out.println("Avant : points_.length="+points_.length);
//    if (points_.length == 0) return false;
    for (int i = 0; i < points_.length; i++) {
      System.out.println(points_[i].numeroBief()+" "+points_[i].abscisse());
    }
    for (int i= 0; i < points_.length; i++) {
      if (ancienNumero == points_[i].numeroBief()) {
        points_[i].numeroBief(nouveauNumero);
      }
    }
    System.out.println("Apres :");
    for (int i = 0; i < points_.length; i++) {
      System.out.println(points_[i].numeroBief()+" "+points_[i].abscisse());
    }
//    return true;

  }

  @Override
  public boolean existePointsBiefNumero(int numeroBief) {
    for (int i = 0; i < points_.length; i++) {
      if (points_[i].numeroBief() == numeroBief) {
        return true;
      }
    }
    return false;
  }
  @Override
  public double  amontPointsBiefNumero(int numeroBief) {
    double amont = Double.MAX_VALUE;
    for (int i = 0; i < points_.length; i++) {
      if (points_[i].numeroBief() == numeroBief) {
        amont = Math.min(amont, points_[i].abscisse());
      }
    }
    return amont;
  }
  @Override
  public double  avalPointsBiefNumero(int numeroBief) {
    double aval = Double.NEGATIVE_INFINITY;
    for (int i = 0; i < points_.length; i++) {
      if (points_[i].numeroBief() == numeroBief) {
        aval = Math.max(aval, points_[i].abscisse());
      }
    }
    return aval;
  }

}
