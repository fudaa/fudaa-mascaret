/**
 * @file         DLigneEauPoint.java
 * @creation     2000-08-09
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.ILigneEauPoint;
import org.fudaa.dodico.corba.hydraulique1d.ILigneEauPointOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "point d'une ligne d'eau initiale".
 * Contient une abscisse, un num�ro de bief, une cote, un d�bit,
 * 2 coef. de frottement (mineur et majeur).
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DLigneEauPoint
  extends DHydraulique1d
  implements ILigneEauPoint,ILigneEauPointOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ILigneEauPoint) {
      ILigneEauPoint l= (ILigneEauPoint)_o;
      numeroBief(l.numeroBief());
      abscisse(l.abscisse());
      cote(l.cote());
      debit(l.debit());
      coefFrottementMin(l.coefFrottementMin());
      coefFrottementMaj(l.coefFrottementMaj());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILigneEauPoint l= UsineLib.findUsine().creeHydraulique1dLigneEauPoint();
    l.initialise(tie());
    return l;
  }
  @Override
  final public String toString() {
    String s= "ligneEauPoint (b";
    s += numeroBief_;
    s += "," + abscisse_ + ")";
    return s;
  }
  /*** ILigneEauPoint ***/
  // constructeurs
  public DLigneEauPoint() {
    super();
    numeroBief_= 1;
    abscisse_= 0.;
    cote_= 0.;
    debit_= 0.;
    coefFrottementMin_= 20.;
    coefFrottementMaj_= 15.;
  }
  @Override
  public void dispose() {
    numeroBief_= -1;
    abscisse_= 0.;
    cote_= 0.;
    debit_= 0.;
    coefFrottementMin_= 0.;
    coefFrottementMaj_= 0.;
    super.dispose();
  }
  // attributs
  private int numeroBief_;
  @Override
  public int numeroBief() {
    return numeroBief_;
  }
  @Override
  public void numeroBief(int numeroBief) {
    if (numeroBief_==numeroBief) return;
    numeroBief_= numeroBief;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numeroBief");
  }
  private double abscisse_;
  @Override
  public double abscisse() {
    return abscisse_;
  }
  @Override
  public void abscisse(double abscisse) {
    if (abscisse_==abscisse) return;
    abscisse_= abscisse;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");
  }
  private double cote_;
  @Override
  public double cote() {
    return cote_;
  }
  @Override
  public void cote(double cote) {
    if (cote_==cote) return;
    cote_= cote;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "cote");
  }
  private double debit_;
  @Override
  public double debit() {
    return debit_;
  }
  @Override
  public void debit(double debit) {
    if (debit_==debit) return;
    debit_= debit;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "debit");
  }
  private double coefFrottementMin_;
  @Override
  public double coefFrottementMin() {
    return coefFrottementMin_;
  }
  @Override
  public void coefFrottementMin(double coefFrottementMin) {
    if (coefFrottementMin_==coefFrottementMin) return;
    coefFrottementMin_= coefFrottementMin;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "coefFrottementMin");
  }
  private double coefFrottementMaj_;
  @Override
  public double coefFrottementMaj() {
    return coefFrottementMaj_;
  }
  @Override
  public void coefFrottementMaj(double coefFrottementMaj) {
    if (coefFrottementMaj_==coefFrottementMaj) return;
    coefFrottementMaj_= coefFrottementMaj;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "coefFrottementMaj");
  }
}
