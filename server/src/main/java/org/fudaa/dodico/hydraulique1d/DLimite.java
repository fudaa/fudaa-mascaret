/**
 * @file         DLimite.java
 * @creation     2000-08-09
 * @modification $Date: 2005-11-22 10:14:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.ILimite;
import org.fudaa.dodico.corba.hydraulique1d.ILimiteOperations;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.LLimiteCalcule;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.corba.hydraulique1d.LCondLimiteImposee;
/**
 * Implémentation de l'objet métier d'une "condition limite d'un bief".
 * Contient un nom et un type (Par loi, évacuation Libre ou hauteur normale)
 * Si le type est par loi, on y rattche une loi hydraulique.
 * @version      $Revision: 1.9 $ $Date: 2005-11-22 10:14:56 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DLimite extends DHydraulique1d implements ILimiteOperations,ILimite {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ILimite) {
      ILimite q= (ILimite)_o;
      if (q.loi() != null)
        loi((ILoiHydraulique)q.loi().creeClone());
      nom(q.nom());
      typeLimiteCalcule(q.typeLimiteCalcule());
      condLimiteImposee(q.condLimiteImposee());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILimite l= UsineLib.findUsine().creeHydraulique1dLimite();
    l.initialise(tie());
    return l;
  }
  @Override
  final public String toString() {
    String s= "limite " + nom_;
    if (loi_ != null)
      s += "(loi " + loi_.toString() + ")";
    return s;
  }
  /*** ILimite ***/
  // constructeurs
  public DLimite() {
    super();
    nom_= "limite";
    typeLimiteCalcule_= LLimiteCalcule.EVACUATION_LIBRE;
    loi_= null;
    condLimiteImposee_=LCondLimiteImposee.DEBIT;
  }
  @Override
  public void dispose() {
    nom_= null;
    typeLimiteCalcule_= null;
    loi_= null;
    condLimiteImposee_=null;
    super.dispose();
  }
  // attributs
  private String nom_;
  @Override
  public String nom() {
    return nom_;
  }
  @Override
  public void nom(String nom) {
    if (nom_.equals(nom)) return;
    nom_= nom;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nom");
  }
  private LLimiteCalcule typeLimiteCalcule_;
  @Override
  public LLimiteCalcule typeLimiteCalcule() {
    return typeLimiteCalcule_;
  }
  @Override
  public void typeLimiteCalcule(LLimiteCalcule typeLimiteCalcule) {
    if (typeLimiteCalcule_.value() == typeLimiteCalcule.value()) return;
    typeLimiteCalcule_= typeLimiteCalcule;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "typeLimiteCalcule");
  }
  private ILoiHydraulique loi_;
  @Override
  public ILoiHydraulique loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiHydraulique loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // methodes
  @Override
  public ILoiTarage creeLoiTarage() {
    ILoiTarage tar= UsineLib.findUsine().creeHydraulique1dLoiTarage();
    loi(tar);
    return tar;
  }
  @Override
  public ILoiHydrogramme creeLoiHydrogramme() {
    ILoiHydrogramme tar= UsineLib.findUsine().creeHydraulique1dLoiHydrogramme();
    loi(tar);
    return tar;
  }
  @Override
  public ILoiLimnigramme creeLoiLimnigramme() {
    ILoiLimnigramme tar= UsineLib.findUsine().creeHydraulique1dLoiLimnigramme();
    loi(tar);
    return tar;
  }

  private LCondLimiteImposee condLimiteImposee_;
  @Override
  public LCondLimiteImposee condLimiteImposee() {
      return condLimiteImposee_;
  }

  @Override
  public void condLimiteImposee(LCondLimiteImposee newCondLimiteImposee) {

      if (condLimiteImposee_.value() == newCondLimiteImposee.value()) return;
      condLimiteImposee_=newCondLimiteImposee;
      UsineLib.findUsine().fireObjetModifie(
            toString(),
            tie(),
            "condLimiteImposee");


  }
}
