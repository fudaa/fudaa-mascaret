/**
 * @file         DLoiHydraulique.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Comparator;

import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydrauliqueOperations;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiGeometrique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimniHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiOuvertureVanne;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiRegulation;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiSeuil;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTracer;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation abstraite de l'objet m�tier "loi hydraulique" des donn�es hydrauliques.
 * Associe un nom et un num�ro.
 * @see org.fudaa.dodico.corba.hydraulique1d.loi.
 * @version      $Revision: 1.13 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public abstract class DLoiHydraulique
  extends DHydraulique1d
  implements ILoiHydraulique,ILoiHydrauliqueOperations, Comparator {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ILoiHydraulique) {
      ILoiHydraulique q= (ILoiHydraulique)_o;
      nom(q.nom());
    }
  }
  @Override
  public String toString() {
    return nom() + " numero " + (indice()+1);
  }

  /**
   * Compares its two arguments for order.  Returns a negative integer,
   * zero, or a positive integer as the first argument is less than, equal
   * to, or greater than the second.<p>
   *
   * The implementor must ensure that <tt>sgn(compare(x, y)) ==
   * -sgn(compare(y, x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
   * implies that <tt>compare(x, y)</tt> must throw an exception if and only
   * if <tt>compare(y, x)</tt> throws an exception.)<p>
   *
   * The implementor must also ensure that the relation is transitive:
   * <tt>((compare(x, y)&gt;0) &amp;&amp; (compare(y, z)&gt;0))</tt> implies
   * <tt>compare(x, z)&gt;0</tt>.<p>
   *
   * Finally, the implementer must ensure that <tt>compare(x, y)==0</tt>
   * implies that <tt>sgn(compare(x, z))==sgn(compare(y, z))</tt> for all
   * <tt>z</tt>.<p>
   *
   * It is generally the case, but <i>not</i> strictly required that
   * <tt>(compare(x, y)==0) == (x.equals(y))</tt>.  Generally speaking,
   * any comparator that violates this condition should clearly indicate
   * this fact.  The recommended language is "Note: this comparator
   * imposes orderings that are inconsistent with equals."
   *
   * @param o1 the first object to be compared.
   * @param o2 the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the
   * 	       first argument is less than, equal to, or greater than the
   *	       second.
   */
  @Override
  public int compare(Object o1, Object o2) {
    if ((o1 instanceof ILoiHydraulique) && (o1 instanceof ILoiHydraulique)) {
      ILoiHydraulique loi1= (ILoiHydraulique)o1;
      ILoiHydraulique loi2= (ILoiHydraulique)o2;
      return loi1.numero() - loi2.numero();
    } else
      return 0;
  }
  /*** ILoiHydraulique ***/
  // constructeurs
  public DLoiHydraulique() {
    super();
    nom_= toString();
    numero_= Identifieur.IDENTIFIEUR.identificateurLibre("loi");
    indice_=0;
  }
  @Override
  public void dispose() {
    nom_= null;
    numero_=0;
    indice_=0;
    super.dispose();
  }
  // attributs
  /**
   * numero_ est �gale � un identifiant immuable de la loi.
   * Il est donc invariable lors de suppression d'autres lois contrairement � l'indice.
   * Il est utilis� pour des comparaisons de lois.
   * Pour l'utilisateur, cette attribut n'existe pas.
   */
  protected int numero_;
  /**
   * numero est �gale � un identifiant immuable de la loi.
   * Il est donc invariable lors de suppression d'autres lois contrairement � l'indice.
   * Il est utilis� pour des comparaisons de lois.
   * Pour l'utilisateur, cette attribut n'existe pas.
   * @return int le num�ro identifieur de cette loi.
   */
  @Override
  public int numero() {
    return numero_;
  }
  @Override
  public void numero(int numero) {
    if (numero_==numero) return;
    numero_= numero;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numero");
  }
  /**
   * indice_ correspond � l'indice de cette loi dans le tableau de lois de {@link DDonneesHydrauliques}.
   * Il est variable lors de suppression d'autres lois.
   * Pour l'utilisateur, indice + 1 est �gale au num�ro de la loi.
   */
  protected int indice_;

  /**
   * indice correspond � l'indice de cette loi dans le tableau de lois de {@link DDonneesHydrauliques}.
   * Il est variable lors de suppression d'autres lois.
   * Pour l'utilisateur, indice + 1 est �gale au num�ro de la loi.
   * @return int l'indice de cette loi dans le tableau de lois de {@link DDonneesHydrauliques}.
   */
  @Override
  public int indice() {
    return indice_;
  }

  /**
   * indice permet de modifier l'indice de cette loi.
   * Modifie �ventuellement le nom de la loi.
   * @param indice int le nouveau indice.
   */
  @Override
  public void indice(int indice) {
	  
	  
    // si le nom contient un marqueur "9999999999" lors de la premi�re instanciation.
    // on remplace le marqueur par la valeur de l'indice + 1.
    if (nom_ != null) {
      int indexe = nom_.indexOf("9999999999");
      if (indexe != -1) {
        String debutNom = nom_.substring(0,indexe);
        String finNom = nom_.substring(indexe+10,nom_.length());
        nom(debutNom+(indice+1)+finNom);
      }
    }

    if (indice_==indice) return;

    // Si l'ancien indice (indice_) est diff�rent du nouveau (indice)
    // Si en plus le nom commence par "loi " suivi de l'ancien num�ro variable (indice_+1)
    // on remplace ce num�ro par le nouveau (indice+1).
    if (nom_ != null) {
    	String chaineIndice = "" + (indice_ + 1);
        int indexe = nom_.indexOf(chaineIndice);

        if (indexe != -1 && nom_.substring(0,indexe).equals("loi ")) {

          String debutNom = nom_.substring(0, indexe);
          String finNom = nom_.substring(indexe+chaineIndice.length(),nom_.length());
          nom(debutNom + (indice + 1)+finNom);
      }
        
        //Pour traiter les anciens fichier dans lesquels le numero est � la fin du nom
        if (nom_.endsWith(""+(indice_+1))) {
              int indexe2 = nom_.indexOf("" + (indice_ + 1));
              if (indexe2 != -1){
                String debutNom = nom_.substring(0, indexe2);
                nom(debutNom + (indice + 1));
              }
            }
        
    }
    indice_= indice;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "indice");
  }
  protected String nom_;
  @Override
  public String nom() {
    return nom_;
  }
  @Override
  public void nom(String nom) {
    if (nom_.equals(nom)) return;
    nom_= nom;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nom");
  }
  // methodes
  @Override
  public void creePoint(int i) {}
  @Override
  public void supprimePoints(int[] i) {}
  @Override
  public String typeLoi() {
    return null;
  }
  @Override
  public int nbPoints() {
    return 0;
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {}
  @Override
  public double getValeur(int ligne, int colonne) {
    return Double.NaN;
  }
  @Override
  public void setPoints(double[][] points) {}
  @Override
  public double[][] pointsToDoubleArray() {
    return null;
  }
  public static void supprimeLoiHydraulique(ILoiHydraulique loi) {
    if (loi instanceof ILoiGeometrique)
      UsineLib.findUsine().supprimeHydraulique1dLoiGeometrique(
        (ILoiGeometrique)loi);
    else if (loi instanceof ILoiLimniHydrogramme)
      UsineLib.findUsine().supprimeHydraulique1dLoiLimniHydrogramme(
        (ILoiLimniHydrogramme)loi);
    else if (loi instanceof ILoiHydrogramme)
      UsineLib.findUsine().supprimeHydraulique1dLoiHydrogramme(
        (ILoiHydrogramme)loi);
    else if (loi instanceof ILoiLimnigramme)
      UsineLib.findUsine().supprimeHydraulique1dLoiLimnigramme(
        (ILoiLimnigramme)loi);
    else if (loi instanceof ILoiOuvertureVanne)
      UsineLib.findUsine().supprimeHydraulique1dLoiOuvertureVanne(
        (ILoiOuvertureVanne)loi);
    else if (loi instanceof ILoiRegulation)
      UsineLib.findUsine().supprimeHydraulique1dLoiRegulation(
        (ILoiRegulation)loi);
    else if (loi instanceof ILoiSeuil)
      UsineLib.findUsine().supprimeHydraulique1dLoiSeuil((ILoiSeuil)loi);
    else if (loi instanceof ILoiTarage)
      UsineLib.findUsine().supprimeHydraulique1dLoiTarage((ILoiTarage)loi);
    else if (loi instanceof ILoiTracer)
      UsineLib.findUsine().supprimeHydraulique1dLoiTracer((ILoiTracer)loi);
    else {
      try {
        throw new RuntimeException(
          "DLoiHydraulique: aucun destructeur pour "
            + loi.getClass().getName());
      } catch (RuntimeException e) {
        e.printStackTrace();
      }
    }
  }

}
