/**
 * @file         DMaillage.java
 * @creation     2000-08-09
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSections;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSections;
import org.fudaa.dodico.corba.hydraulique1d.IDefinitionSectionsParSeries;
import org.fudaa.dodico.corba.hydraulique1d.IMaillage;
import org.fudaa.dodico.corba.hydraulique1d.IMaillageOperations;
import org.fudaa.dodico.corba.hydraulique1d.LMethodeMaillage;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "maillage".
 * Contient un indicateur de la m�thode de maillage et de la d�finition des sections de calcul.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DMaillage extends DHydraulique1d implements IMaillageOperations,IMaillage {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IMaillage) {
      IMaillage q= (IMaillage)_o;
      methode(q.methode());
      if (q.sections() != null)
        sections((IDefinitionSections)q.sections().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    IMaillage p= UsineLib.findUsine().creeHydraulique1dMaillage();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "maillage";
    return s;
  }
  /*** IMaillage ***/
  // constructeurs
  public DMaillage() {
    super();
    methode_= LMethodeMaillage.SECTIONS_SUR_PROFILS;
    sections_= null;
  }
  @Override
  public void dispose() {
    methode_= null;
    sections_= null;
    super.dispose();
  }
  // Attributs
  private LMethodeMaillage methode_;
  @Override
  public LMethodeMaillage methode() {
    return methode_;
  }
  @Override
  public void methode(LMethodeMaillage s) {
    if (methode_.value()==s.value()) return;
    methode_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "methode");
  }
  private IDefinitionSections sections_;
  @Override
  public IDefinitionSections sections() {
    return sections_;
  }
  @Override
  public void sections(IDefinitionSections s) {
    if (sections_==s) return;
    sections_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "sections");
  }
  // Methodes
  @Override
  public void creeSectionsParSeries() {
    if (sections() instanceof IDefinitionSectionsParSections) {
      UsineLib.findUsine().supprimeHydraulique1dDefinitionSectionsParSections(
        (IDefinitionSectionsParSections)sections());
    } else if (sections() instanceof IDefinitionSectionsParSeries) {
      UsineLib.findUsine().supprimeHydraulique1dDefinitionSectionsParSeries(
        (IDefinitionSectionsParSeries)sections());
    }
    sections(
      UsineLib.findUsine().creeHydraulique1dDefinitionSectionsParSeries());
  }
  @Override
  public void creeSectionsParSections() {
    if (sections() instanceof IDefinitionSectionsParSections) {
      UsineLib.findUsine().supprimeHydraulique1dDefinitionSectionsParSections(
        (IDefinitionSectionsParSections)sections());
    } else if (sections() instanceof IDefinitionSectionsParSeries) {
      UsineLib.findUsine().supprimeHydraulique1dDefinitionSectionsParSeries(
        (IDefinitionSectionsParSeries)sections());
    }
    sections(
      UsineLib.findUsine().creeHydraulique1dDefinitionSectionsParSections());
  }
}
