/**
 * @file         DNoeud.java
 * @creation     2000-07-24
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.IExtremite;
import org.fudaa.dodico.corba.hydraulique1d.INoeud;
import org.fudaa.dodico.corba.hydraulique1d.INoeudOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier d'un "noeud" du r�seau hydraulique repr�sentant un confluent.
 * Contient un tableau d'extremit� et un nom.
 * @version      $Revision: 1.10 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public class DNoeud extends DHydraulique1d implements INoeudOperations,INoeud {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof INoeud) {
      INoeud q= (INoeud)_o;
      if (q.extremites() != null) {
        IExtremite[] ip= new IExtremite[q.extremites().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (IExtremite)q.extremites()[i].creeClone();
        extremites(ip);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    INoeud p= UsineLib.findUsine().creeHydraulique1dNoeud();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "noeud " + numero_;
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Confluent "+numero_;
    int nbExtrem= extremites_.length;
    if (nbExtrem == 0)
      res[1]= "pas de connexion";
    else {
      res[1]= "";
      for (int i= 0; i < extremites_.length; i++) {
        if (i == 0)
          res[1]= res[1] + "extremit� n� : " + extremites_[i].numero();
        else
          res[1]= res[1] + ", " + extremites_[i].numero();
      }
    }
    return res;
  }
  /*** INoeud ***/
  // constructeurs
  public DNoeud() {
    super();
    numero_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    extremites_= new IExtremite[0];
  }
  @Override
  public void dispose() {
    extremites_= null;
  }
  // attributs
  private int numero_;
  @Override
  public int numero() {
    return numero_;
  }
  @Override
  public void numero(int s) {
    if (numero_==s) return;
    numero_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numero");
  }
  private IExtremite[] extremites_;
  @Override
  public IExtremite[] extremites() {
	Arrays.sort(extremites_, new ComparateurExtremite());
    return extremites_;
  }
  @Override
  public void extremites(IExtremite[] s) {
    if (egale(extremites_,s)) return;
    extremites_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "extremites");
  }
  // methode
  @Override
  public void ajouteExtremite(IExtremite extrem) {
    if (extrem == null)
      return;
    Vector vExtrem= new Vector(extremites_.length, 1);
    for (int i= 0; i < extremites_.length; i++)
      vExtrem.addElement(extremites_[i]);
    if (vExtrem.contains(extrem))
      return;
    vExtrem.addElement(extrem);
    IExtremite[] extremites= new IExtremite[vExtrem.size()];
    for (int i= 0; i < extremites.length; i++)
      extremites[i]= (IExtremite)vExtrem.get(i);
    extremites(extremites);
    // System.err.println(""+extremites_.length+" extremites");
  }
  @Override
  public void supprimeExtremite(IExtremite extrem) {
    Vector nouv= new Vector();
    for (int i= 0; i < extremites_.length; i++) {
      if (extremites_[i] != extrem)
        nouv.add(extremites_[i]);
    }
    IExtremite[] extrs= new IExtremite[nouv.size()];
    for (int i= 0; i < extrs.length; i++)
      extrs[i]= (IExtremite)nouv.get(i);
    extremites(extrs);
  }
  @Override
  public void setExtremite(int indice, IExtremite extrem) {
    if (indice < extremites_.length)
      extremites_[indice]= extrem;
    else
      ajouteExtremite(extrem);
  }
}
class ComparateurExtremite implements Comparator {
	  /**
	   * Compares its two arguments for order.  Returns a negative integer,
	   * zero, or a positive integer as the first argument is less than, equal
	   * to, or greater than the second.<p>
	   *
	   * The implementor must ensure that <tt>sgn(compare(x, y)) ==
	   * -sgn(compare(y, x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
	   * implies that <tt>compare(x, y)</tt> must throw an exception if and only
	   * if <tt>compare(y, x)</tt> throws an exception.)<p>
	   *
	   * The implementor must also ensure that the relation is transitive:
	   * <tt>((compare(x, y)&gt;0) &amp;&amp; (compare(y, z)&gt;0))</tt> implies
	   * <tt>compare(x, z)&gt;0</tt>.<p>
	   *
	   * Finally, the implementer must ensure that <tt>compare(x, y)==0</tt>
	   * implies that <tt>sgn(compare(x, z))==sgn(compare(y, z))</tt> for all
	   * <tt>z</tt>.<p>
	   *
	   * It is generally the case, but <i>not</i> strictly required that
	   * <tt>(compare(x, y)==0) == (x.equals(y))</tt>.  Generally speaking,
	   * any comparator that violates this condition should clearly indicate
	   * this fact.  The recommended language is "Note: this comparator
	   * imposes orderings that are inconsistent with equals."
	   *
	   * @param o1 the first object to be compared.
	   * @param o2 the second object to be compared.
	   * @return a negative integer, zero, or a positive integer as the
	   * 	       first argument is less than, equal to, or greater than the
	   *	       second.
	   * @throws ClassCastException if the arguments' types prevent them from
	   * 	       being compared by this Comparator.
	   */
  @Override
	  public int compare(Object o1, Object o2) {
	    if ( (o1 instanceof IExtremite) && (o1 instanceof IExtremite)) {
	      IExtremite ex1 = (IExtremite) o1;
	      IExtremite ex2 = (IExtremite) o2;
	      return ex1.numero() - ex2.numero();
	    }
	    else
	      return 0;
	  }
	}
