/**
 * @file         DOptionsListing.java
 * @creation     2000-08-10
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IOptionsListing;
import org.fudaa.dodico.corba.hydraulique1d.IOptionsListingOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "option" d'impression sur le fichier "listing".
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DOptionsListing
  extends DHydraulique1d
  implements IOptionsListing,IOptionsListingOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IOptionsListing) {
      IOptionsListing q= (IOptionsListing)_o;
      geometrie(q.geometrie());
      planimetrage(q.planimetrage());
      reseau(q.reseau());
      loisHydrauliques(q.loisHydrauliques());
      ligneEauInitiale(q.ligneEauInitiale());
      calcul(q.calcul());
    }
  }
  @Override
  final public IObjet creeClone() {
    IOptionsListing p= UsineLib.findUsine().creeHydraulique1dOptionsListing();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "optionsListing";
    return s;
  }
  /*** IOptionsListing ***/
  // constructeurs
  public DOptionsListing() {
    super();
    geometrie_= false;
    planimetrage_= false;
    reseau_= false;
    loisHydrauliques_= false;
    ligneEauInitiale_= false;
    calcul_= true;
  }
  @Override
  public void dispose() {
    geometrie_= false;
    planimetrage_= false;
    reseau_= false;
    loisHydrauliques_= false;
    ligneEauInitiale_= false;
    calcul_= true;
    super.dispose();
  }
  // Attributs
  private boolean geometrie_;
  @Override
  public boolean geometrie() {
    return geometrie_;
  }
  @Override
  public void geometrie(boolean s) {
    if (geometrie_==s) return;
    geometrie_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "geometrie");
  }
  private boolean planimetrage_;
  @Override
  public boolean planimetrage() {
    return planimetrage_;
  }
  @Override
  public void planimetrage(boolean s) {
    if (planimetrage_==s) return;
    planimetrage_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "planimetrage");
  }
  private boolean reseau_;
  @Override
  public boolean reseau() {
    return reseau_;
  }
  @Override
  public void reseau(boolean s) {
    if (reseau_==s) return;
    reseau_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "reseau");
  }
  private boolean loisHydrauliques_;
  @Override
  public boolean loisHydrauliques() {
    return loisHydrauliques_;
  }
  @Override
  public void loisHydrauliques(boolean s) {
    if (loisHydrauliques_==s) return;
    loisHydrauliques_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loisHydrauliques");
  }
  private boolean ligneEauInitiale_;
  @Override
  public boolean ligneEauInitiale() {
    return ligneEauInitiale_;
  }
  @Override
  public void ligneEauInitiale(boolean s) {
    if (ligneEauInitiale_==s) return;
    ligneEauInitiale_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "ligneEauInitiale");
  }
  private boolean calcul_;
  @Override
  public boolean calcul() {
    return calcul_;
  }
  @Override
  public void calcul(boolean s) {
    if (calcul_==s) return;
    calcul_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "calcul");
  }
}
