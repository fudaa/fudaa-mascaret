package org.fudaa.dodico.hydraulique1d;

import org.fudaa.dodico.corba.hydraulique1d.IOptionsListingTracer;
import org.fudaa.dodico.corba.hydraulique1d.IOptionsListingTracerOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;

/*
 * @file         DOptionsListingTracer.java
 * @creation     2006-02-28
 * @modification $Date: 2006-03-02 14:34:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class DOptionsListingTracer   extends DHydraulique1d
  implements IOptionsListingTracer,IOptionsListingTracerOperations {
  @Override
    public void initialise(IObjet _o) {
      super.initialise(_o);
      if (_o instanceof DOptionsListingTracer) {
          IOptionsListingTracer q= (IOptionsListingTracer)_o;
          concentInit(q.concentInit());
          loiTracer(q.loiTracer());
          concentrations(q.concentrations());
          bilanTracer(q.bilanTracer());

      }
    }
  @Override
    final public IObjet creeClone() {
      IOptionsListingTracer p= UsineLib.findUsine().creeHydraulique1dOptionsListingTracer();
      p.initialise(tie());
      return p;
    }
  @Override
    final public String toString() {
      String s= "optionsListing tracer";
      return s;
    }

  @Override
    public void dispose() {
        concentInit_= false;
        loiTracer_= false;
        concentrations_= false;
        bilanTracer_= false;
        super.dispose();

    }

    public DOptionsListingTracer() {
         super();
        concentInit_= false;
        loiTracer_ = false;
        concentrations_ = true;
        bilanTracer_ = true;

    }

    private boolean concentInit_;
  @Override
    public boolean concentInit() {
      return concentInit_;
    }
  @Override
    public void concentInit(boolean newConcentInit) {
        if (concentInit_==newConcentInit) return;
        concentInit_ = newConcentInit;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "concentInit");
    }

    private boolean loiTracer_;
  @Override
    public boolean loiTracer() {
        return loiTracer_;
    }
  @Override
    public void loiTracer(boolean newLoiTracer) {
        if (loiTracer_==newLoiTracer) return;
       loiTracer_ = newLoiTracer;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "loiTracer");


    }

    private boolean concentrations_;
  @Override
    public boolean concentrations() {
        return concentrations_;
    }
  @Override
    public void concentrations(boolean newConcentrations) {
        if (concentrations_==newConcentrations) return;
        concentrations_ = newConcentrations;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "concentrations");


    }

    private boolean bilanTracer_;
  @Override
    public boolean bilanTracer() {
        return bilanTracer_;
    }

  @Override
    public void bilanTracer(boolean newBilanTracer) {
        if (bilanTracer_==newBilanTracer) return;
        bilanTracer_ = newBilanTracer;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "bilanTracer");


    }
}
