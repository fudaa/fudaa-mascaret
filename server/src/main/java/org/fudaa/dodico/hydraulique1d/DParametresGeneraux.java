/*
 * @file         DParametresGeneraux.java
 * @creation     2000-07-27
 * @modification $Date: 2007-03-28 15:35:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.evenement.IObjetEvent;
import org.fudaa.dodico.corba.evenement.IObjetEventListenerSupport;
import org.fudaa.dodico.corba.evenement.IObjetEventSender;
import org.fudaa.dodico.corba.hydraulique1d.IBarragePrincipal;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.IMaillage;
import org.fudaa.dodico.corba.hydraulique1d.IParametresGeneraux;
import org.fudaa.dodico.corba.hydraulique1d.IParametresGenerauxCasier;
import org.fudaa.dodico.corba.hydraulique1d.IParametresGenerauxCasierHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresGenerauxHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresGenerauxOperations;
import org.fudaa.dodico.corba.hydraulique1d.IReseau;
import org.fudaa.dodico.corba.hydraulique1d.IZone;
import org.fudaa.dodico.corba.hydraulique1d.LRegime;
import org.fudaa.dodico.corba.hydraulique1d.LTypeCompositionLits;
import org.fudaa.dodico.corba.hydraulique1d.LTypeFrottement;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.evenement.DObjetEventListenerSupport;
import org.fudaa.dodico.evenement.ObjetEventListener;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier des "parametres g�n�raux" de l'�tude.
 * Peut contenir, entre autre, le barrage principal d'une onde de submersion,
*  les param�tres g�n�raux d'une �tude avec des casiers et le maillage.
 *
 * @version      $Revision: 1.19 $ $Date: 2007-03-28 15:35:28 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public class DParametresGeneraux
  extends DHydraulique1d
  implements IParametresGeneraux,IParametresGenerauxOperations, ObjetEventListener {
  transient private DObjetEventListenerSupport evtSupport_;
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IParametresGeneraux) {
      IParametresGeneraux q= IParametresGenerauxHelper.narrow(_o);
      regime(q.regime());
      noyauV5P2(q.noyauV5P2());
      ondeSubmersion(q.ondeSubmersion());
      froudeLimConditionLimite(q.froudeLimConditionLimite());
      if (q.barragePrincipal() != null)
        barragePrincipal((IBarragePrincipal)q.barragePrincipal().creeClone());
      compositionLits(q.compositionLits());
      frottementsParois(q.frottementsParois());
      traitementImpliciteFrottements(q.traitementImpliciteFrottements());
      typeFrottement(q.typeFrottement());
      perteChargeConfluents(q.perteChargeConfluents());
      elevationCoteArriveeFront(q.elevationCoteArriveeFront());
      interpolationLineaireCoefFrottement(
        q.interpolationLineaireCoefFrottement());
      debordProgressifLitMajeur(q.debordProgressifLitMajeur());
      debordProgressifZoneStockage(q.debordProgressifZoneStockage());
      implicitationNoyauTrans(q.implicitationNoyauTrans());
      optimisationNoyauTrans(q.optimisationNoyauTrans());
      perteChargeAutoElargissement(q.perteChargeAutoElargissement());
      hauteurEauMinimal(q.hauteurEauMinimal());
      if (q.maillage() != null)
        maillage((IMaillage)q.maillage().creeClone());
      if (q.zoneEtude() != null)
        zoneEtude((IZone)q.zoneEtude().creeClone());
      if (q.parametresCasier() != null)
        parametresCasier(
          IParametresGenerauxCasierHelper.narrow(q.parametresCasier().creeClone()));

    }
  }
  @Override
  final public IObjet creeClone() {
    IParametresGeneraux p=
      UsineLib.findUsine().creeHydraulique1dParametresGeneraux();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "parametresGeneraux";
    return s;
  }
  @Override
  public void objetCree(IObjetEvent e) {}
  @Override
  public void objetSupprime(IObjetEvent e) {
        IObjet src=e.getSource();
/*        System.out.println("DParametresGeneraux objetSupprime(IObjetEvent e=)");
        System.out.println("\t e.getSource="+e.getSource().getClass().getName());
        System.out.println("\t e.getChamp="+e.getChamp());
        System.out.println("\t e.getMessage="+e.getMessage());*/
    if (src instanceof IBief) {
      supprimeSectionMaillageAvecBief((IBief)src);
    }
  }
  @Override
  public void objetModifie(IObjetEvent e) {}
  /*** IParametresGeneraux ***/
  // constructeurs
  public DParametresGeneraux() {
    super();
    regime_= LRegime.FLUVIAL_PERMANENT;
    noyauV5P2_=false;
    ondeSubmersion_= false;
    froudeLimConditionLimite_= 1000;
    barragePrincipal_= null;
    compositionLits_= LTypeCompositionLits.MINEUR_MAJEUR;
    frottementsParois_= false;
    profilsAbscAbsolu_= false;
    traitementImpliciteFrottements_= false;
    typeFrottement_= LTypeFrottement.STRICKLER;
    perteChargeConfluents_= false;
    elevationCoteArriveeFront_= 0.05;
    interpolationLineaireCoefFrottement_= false;
    debordProgressifLitMajeur_= false;
    debordProgressifZoneStockage_= false;
    implicitationNoyauTrans_= false;
    optimisationNoyauTrans_ = false;
    perteChargeAutoElargissement_= true;
    hauteurEauMinimal_= 0.005;
    maillage_= UsineLib.findUsine().creeHydraulique1dMaillage();
    zoneEtude_= UsineLib.findUsine().creeHydraulique1dZone();
    evtSupport_= DObjetEventListenerSupport.createEventSupport();
    parametresCasier_ = UsineLib.findUsine().creeHydraulique1dParametresGenerauxCasier();
    //evtSupport_= new DObjetEventListenerSupport();
    evtSupport_.clientListener(this);
    ((IObjetEventSender)UsineLib.findUsine()).addObjetEventListener(
      (IObjetEventListenerSupport)evtSupport_.tie());
  }
  @Override
  public void dispose() {
    ((IObjetEventSender)UsineLib.findUsine()).removeObjetEventListener(
      (IObjetEventListenerSupport)evtSupport_.tie());
    evtSupport_.clientListener(null);
    evtSupport_= null;
    regime_= null;
    noyauV5P2_=false;
    ondeSubmersion_= false;
    froudeLimConditionLimite_= 0.;
    barragePrincipal_= null;
    compositionLits_= null;
    frottementsParois_= false;
    traitementImpliciteFrottements_= true;
    typeFrottement_= null;
    perteChargeConfluents_= false;
    elevationCoteArriveeFront_= 0.;
    interpolationLineaireCoefFrottement_= true;
    debordProgressifLitMajeur_= false;
    debordProgressifZoneStockage_= false;
    implicitationNoyauTrans_= false;
    optimisationNoyauTrans_ = false;
    perteChargeAutoElargissement_= false;
    hauteurEauMinimal_= 0.;
    maillage_= null;
    zoneEtude_= null;
    parametresCasier_= null;
    super.dispose();
  }
  // Attributs
  private boolean noyauV5P2_;
  @Override
  public boolean noyauV5P2() {
    return noyauV5P2_;
  }
  @Override
  public void noyauV5P2(boolean s) {
    if (noyauV5P2_==s) return;
    noyauV5P2_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "noyauV5P2");
  }
  private boolean ondeSubmersion_;
  @Override
  public boolean ondeSubmersion() {
    return ondeSubmersion_;
  }
  @Override
  public void ondeSubmersion(boolean s) {
    if (ondeSubmersion_==s) return;
    ondeSubmersion_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "ondeSubmersion");
  }
  private LRegime regime_;
  @Override
  public LRegime regime() {
    return regime_;
  }
  @Override
  public void regime(LRegime s) {
    if (regime_.value()==s.value()) return;
    regime_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "regime");
  }
  private double froudeLimConditionLimite_;
  @Override
  public double froudeLimConditionLimite() {
    return froudeLimConditionLimite_;
  }
  @Override
  public void froudeLimConditionLimite(double s) {
    if (froudeLimConditionLimite_==s) return;
    froudeLimConditionLimite_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "froudeLimConditionLimite");
  }
  private IBarragePrincipal barragePrincipal_;
  @Override
  public IBarragePrincipal barragePrincipal() {
    return barragePrincipal_;
  }
  @Override
  public void barragePrincipal(IBarragePrincipal s) {
    if (barragePrincipal_==s) return;
    barragePrincipal_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "barragePrincipal");
  }
  private LTypeCompositionLits compositionLits_;
  @Override
  public LTypeCompositionLits compositionLits() {
    return compositionLits_;
  }
  @Override
  public void compositionLits(LTypeCompositionLits s) {
    if (compositionLits_.value()==s.value()) return;
    compositionLits_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "compositionLits");
  }
  private boolean frottementsParois_;
  @Override
  public boolean frottementsParois() {
    return frottementsParois_;
  }
  @Override
  public void frottementsParois(boolean s) {
    if (frottementsParois_==s) return;
    frottementsParois_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "frottementsParois");
  }
  private boolean profilsAbscAbsolu_;
  @Override
  public boolean profilsAbscAbsolu() {
    return profilsAbscAbsolu_;
  }
  @Override
  public void profilsAbscAbsolu(boolean s) {
    if (profilsAbscAbsolu_==s) return;
    profilsAbscAbsolu_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "profilsAbscAbsolu");
  }
  private boolean traitementImpliciteFrottements_;
  @Override
  public boolean traitementImpliciteFrottements() {
    return traitementImpliciteFrottements_;
  }
  @Override
  public void traitementImpliciteFrottements(boolean s) {
    if (traitementImpliciteFrottements_==s) return;
    traitementImpliciteFrottements_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "traitementImpliciteFrottements");
  }
  private LTypeFrottement typeFrottement_;
  @Override
  public LTypeFrottement typeFrottement() {
    return typeFrottement_;
  }
  @Override
  public void typeFrottement(LTypeFrottement s) {
    if (typeFrottement_.value()==s.value()) return;
    typeFrottement_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "typeFrottement");
  }
  private boolean perteChargeConfluents_;
  @Override
  public boolean perteChargeConfluents() {
    return perteChargeConfluents_;
  }
  @Override
  public void perteChargeConfluents(boolean s) {
    if (perteChargeConfluents_==s) return;
    perteChargeConfluents_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "perteChargeConfluents");
  }
  private double elevationCoteArriveeFront_;
  @Override
  public double elevationCoteArriveeFront() {
    return elevationCoteArriveeFront_;
  }
  @Override
  public void elevationCoteArriveeFront(double s) {
    if (elevationCoteArriveeFront_==s) return;
    elevationCoteArriveeFront_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "elevationCoteArriveeFront");
  }
  private boolean interpolationLineaireCoefFrottement_;
  @Override
  public boolean interpolationLineaireCoefFrottement() {
    return interpolationLineaireCoefFrottement_;
  }
  @Override
  public void interpolationLineaireCoefFrottement(boolean s) {
    if (interpolationLineaireCoefFrottement_==s) return;
    interpolationLineaireCoefFrottement_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "interpolationLineaireCoefFrottement");
  }
  private boolean debordProgressifLitMajeur_;
  @Override
  public boolean debordProgressifLitMajeur() {
    return debordProgressifLitMajeur_;
  }
  @Override
  public void debordProgressifLitMajeur(boolean s) {
    if (debordProgressifLitMajeur_==s) return;
    debordProgressifLitMajeur_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "debordProgressifLitMajeur");
  }
  private boolean debordProgressifZoneStockage_;
  @Override
  public boolean debordProgressifZoneStockage() {
    return debordProgressifZoneStockage_;
  }
  @Override
  public void debordProgressifZoneStockage(boolean s) {
    if (debordProgressifZoneStockage_==s) return;
    debordProgressifZoneStockage_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "debordProgressifZoneStockage");
  }
  private boolean implicitationNoyauTrans_;
  @Override
  public boolean implicitationNoyauTrans() {
    return implicitationNoyauTrans_;
  }
  @Override
  public void implicitationNoyauTrans(boolean implicitationNoyauTrans) {
    if (implicitationNoyauTrans_==implicitationNoyauTrans) return;
    implicitationNoyauTrans_= implicitationNoyauTrans;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "implicitationNoyauTrans");
  }


    private boolean optimisationNoyauTrans_;
  @Override
  public boolean optimisationNoyauTrans() {
return optimisationNoyauTrans_;
}
  @Override
  public void optimisationNoyauTrans(boolean optimisationNoyauTrans) {
if (optimisationNoyauTrans_==optimisationNoyauTrans) return;
optimisationNoyauTrans_= optimisationNoyauTrans;
UsineLib.findUsine().fireObjetModifie(
  toString(),
  tie(),
  "optimisationNoyauTrans");
}



  private boolean perteChargeAutoElargissement_;
  @Override
  public boolean perteChargeAutoElargissement() {
    return perteChargeAutoElargissement_;
  }
  @Override
  public void perteChargeAutoElargissement(boolean perteChargeAutoElargissement) {
    if (perteChargeAutoElargissement_==perteChargeAutoElargissement) return;
    perteChargeAutoElargissement_= perteChargeAutoElargissement;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "perteChargeAutoElargissement");
  }
  private double hauteurEauMinimal_;
  @Override
  public double hauteurEauMinimal() {
    return hauteurEauMinimal_;
  }
  @Override
  public void hauteurEauMinimal(double hauteurEauMinimal) {
    if (hauteurEauMinimal_==hauteurEauMinimal) return;
    hauteurEauMinimal_= hauteurEauMinimal;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "hauteurEauMinimal");
  }
  private IMaillage maillage_;
  @Override
  public IMaillage maillage() {
    return maillage_;
  }
  @Override
  public void maillage(IMaillage s) {
    if (maillage_==s) return;
    maillage_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "maillage");
  }
  private IZone zoneEtude_;
  @Override
  public IZone zoneEtude() {
    return zoneEtude_;
  }
  @Override
  public void zoneEtude(IZone s) {
    if (zoneEtude_==s) return;
    zoneEtude_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zoneEtude");
  }
  private IParametresGenerauxCasier parametresCasier_;
  @Override
  public IParametresGenerauxCasier parametresCasier() {
    return parametresCasier_;
  }
  @Override
  public void parametresCasier(IParametresGenerauxCasier s) {
    if (parametresCasier_==s) return;
    parametresCasier_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "parametresCasier");
  }



  // methodes
  @Override
  public void recalculeZoneEtude(IReseau reseau) {
    double xori= reseau.getXOrigine();
    double xfin= reseau.getXFin();
    if (xori < Double.MAX_VALUE)
      zoneEtude_.abscisseDebut(xori);
    if (xfin > Double.NEGATIVE_INFINITY)
      zoneEtude_.abscisseFin(xfin);
    zoneEtude(zoneEtude_);
  }
  @Override
  public IBarragePrincipal creeBarragePrincipal() {
    barragePrincipal_= UsineLib.findUsine().creeHydraulique1dBarragePrincipal();
    return barragePrincipal_;
  }
  @Override
  public IMaillage creeMaillage() {
    return UsineLib.findUsine().creeHydraulique1dMaillage();
  }
  @Override
  public void supprimeSectionMaillageAvecBief(IBief bief) {
    System.out.println("supprimeSectionMaillageAvecBief(IBief bief)");
    if (maillage() != null) {
      if (maillage().sections() != null) {
        maillage().sections().supprimeSectionMaillageAvecBief(bief);
      }
    }
  }
}
