/**
 * @creation     2003-05-30
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IParametresGenerauxCasier;
import org.fudaa.dodico.corba.hydraulique1d.IParametresGenerauxCasierHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresGenerauxCasierOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier des "param�tres g�n�raux d'une �tude avec des casiers".
 * @version      $Revision: 1.9 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DParametresGenerauxCasier
  extends DHydraulique1d
  implements IParametresGenerauxCasier,IParametresGenerauxCasierOperations {
  /*** IObjet ***/
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IParametresGenerauxCasier) {
      IParametresGenerauxCasier q= IParametresGenerauxCasierHelper.narrow(_o);
      coefImplicitationCasier(q.coefImplicitationCasier());
      coefImplicitationCouplage(q.coefImplicitationCouplage());
      nbMaxIterationCouplage(q.nbMaxIterationCouplage());
      activation(q.activation());
    }
  }
  @Override
  public IObjet creeClone() {
    IParametresGenerauxCasier p=
      UsineLib.findUsine().creeHydraulique1dParametresGenerauxCasier();
    p.initialise(tie());
    return p;
  }
  @Override
  public String toString() {
    String s= "ParametresGenerauxCasier";
    s += ": "
      + coefImplicitationCasier_
      + ","
      + coefImplicitationCouplage_
      + ","
      + nbMaxIterationCouplage_;
    return s;
  }
  /*** IParametresGenerauxCasier ***/
  // constructeurs
  public DParametresGenerauxCasier() {
    super();
    coefImplicitationCasier_= 0.5;
    coefImplicitationCouplage_= 0.5;
    nbMaxIterationCouplage_= 1;
    activation_= true;
  }
  // destructeur
  @Override
  public void dispose() {
    coefImplicitationCasier_= 0.;
    coefImplicitationCouplage_= 0;
    nbMaxIterationCouplage_= 0;
    activation_= false;
    super.dispose();
  }
  // attributs
  private double coefImplicitationCasier_;
  @Override
  public double coefImplicitationCasier() {
    return coefImplicitationCasier_;
  }
  @Override
  public void coefImplicitationCasier(double t) {
    if (coefImplicitationCasier_==t) return;
    coefImplicitationCasier_= t;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "coefImplicitationCasier");
  }
  private double coefImplicitationCouplage_;
  @Override
  public double coefImplicitationCouplage() {
    return coefImplicitationCouplage_;
  }
  @Override
  public void coefImplicitationCouplage(double t) {
    if (coefImplicitationCouplage_==t) return;
    coefImplicitationCouplage_= t;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "coefImplicitationCouplage");
  }
  private int nbMaxIterationCouplage_;
  @Override
  public int nbMaxIterationCouplage() {
    return nbMaxIterationCouplage_;
  }
  @Override
  public void nbMaxIterationCouplage(int t) {
    if (nbMaxIterationCouplage_==t) return;
    nbMaxIterationCouplage_= t;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "nbMaxIterationCouplage");
  }
  private boolean activation_;
  @Override
  public boolean activation() {
    return activation_;
  }
  @Override
  public void activation(boolean t) {
    if (activation_==t) return;
    activation_= t;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "activation");
  }
}
