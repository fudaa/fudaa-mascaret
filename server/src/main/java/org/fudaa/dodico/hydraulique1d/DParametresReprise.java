/**
 * @creation     2000-08-09
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IParametresReprise;
import org.fudaa.dodico.corba.hydraulique1d.IParametresRepriseHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresRepriseOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier des "param�tres de reprise d'un calcul".
 * Le contenu du fichier est stock� dans un tableau d'octets.
 * @version      $Revision: 1.10 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DParametresReprise
  extends DHydraulique1d
  implements IParametresReprise,IParametresRepriseOperations {
  @Override
  public void initialise(IObjet _o) {
    //DEBUG: a distance c'est toujours faux
    if (_o instanceof IParametresReprise) {
      IParametresReprise p= IParametresRepriseHelper.narrow(_o);
      fichier(p.fichier());
      contenu(p.contenu());
      tFinal(p.tFinal());
    }
  }
  @Override
  final public IObjet creeClone() {
    IParametresReprise p=
      UsineLib.findUsine().creeHydraulique1dParametresReprise();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "param�tresReprise";
    return s;
  }
  /*** IParametresReprise ***/
  // constructeurs
  public DParametresReprise() {
    super();
    fichier_= "";
    contenu_= new byte[0];
    tFinal_= 0.;
  }
  @Override
  public void dispose() {
    fichier_= null;
    contenu_= null;
    tFinal_= 0.;
    super.dispose();
  }
  // attributs
  private String fichier_;
  @Override
  public String fichier() {
    return fichier_;
  }
  @Override
  public void fichier(String fichier) {
    if (fichier_.equals(fichier)) return;
    fichier_= fichier;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "fichier");
  }
  private byte[] contenu_;
  @Override
  public byte[] contenu() {
    return contenu_;
  }
  @Override
  public void contenu(byte[] contenu) {
    contenu_= contenu;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "contenu");
  }
  private double tFinal_;
  @Override
  public double tFinal() {
    return tFinal_;
  }
  @Override
  public void tFinal(double tFinal) {
    if (tFinal_==tFinal) return;
    tFinal_= tFinal;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "tFinal");
  }
}
