/**
 * @file         DParametresResultats.java
 * @creation     2000-08-10
 * @modification $Date: 2006-09-12 08:33:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.IDescriptionVariable;
import org.fudaa.dodico.corba.hydraulique1d.IOptionsListing;
import org.fudaa.dodico.corba.hydraulique1d.IOptionsListingTracer;
import org.fudaa.dodico.corba.hydraulique1d.IParametresResultats;
import org.fudaa.dodico.corba.hydraulique1d.IParametresResultatsHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresResultatsOperations;
import org.fudaa.dodico.corba.hydraulique1d.IParametresStockage;
import org.fudaa.dodico.corba.hydraulique1d.IParametresStockageHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTailleMaxFichier;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTailleMaxFichierHelper;
import org.fudaa.dodico.corba.hydraulique1d.LTypeNombre;
import org.fudaa.dodico.corba.hydraulique1d.LUnite;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier des "param�tres des fichiers r�sultats" de l'�tude.
 * Contients entre autres un tableau des description des variables dans le fichier r�sultat,
 * les options d'impression dans le fichier listing, les tailles maximum des fichiers autoris�s
 * par Fudaa-Mascaret et les param�tres de stockages dans les fichiers r�sultats et listing
 * (p�riode, d�but).
 * @version      $Revision: 1.16 $ $Date: 2006-09-12 08:33:58 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public class DParametresResultats
  extends DHydraulique1d
  implements IParametresResultats,IParametresResultatsOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IParametresResultats) {
      IParametresResultats q= IParametresResultatsHelper.narrow(_o);
      if (q.optionsListing() != null)
        optionsListing((IOptionsListing)q.optionsListing().creeClone());
    if (q.optionsListingTracer() != null)
        optionsListingTracer((IOptionsListingTracer)q.optionsListingTracer().creeClone());
      if (q.paramStockage() != null)
        paramStockage(IParametresStockageHelper.narrow(q.paramStockage().creeClone()));
      if (q.paramTailleMaxFichier() != null)
        paramTailleMaxFichier(
          IParametresTailleMaxFichierHelper.narrow(q.paramTailleMaxFichier().creeClone()));
      postRubens(q.postRubens());
      postOpthyca(q.postOpthyca());
      postOpthycaQualiteDEau(q.postOpthycaQualiteDEau());
      decalage(q.decalage());
      if (q.variables() != null) {
        IDescriptionVariable[] ip=
          new IDescriptionVariable[q.variables().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (IDescriptionVariable)q.variables()[i].creeClone();
        variables(ip);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IParametresResultats p=
      UsineLib.findUsine().creeHydraulique1dParametresResultats();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "parametresResultats";
    return s;
  }
  /*** IParametresResultats ***/
  // constructeurs
  public DParametresResultats() {
    super();
    optionsListing_= UsineLib.findUsine().creeHydraulique1dOptionsListing();
    optionsListingTracer_= UsineLib.findUsine().creeHydraulique1dOptionsListingTracer();
    paramStockage_= UsineLib.findUsine().creeHydraulique1dParametresStockage();
    paramStockage_= UsineLib.findUsine().creeHydraulique1dParametresStockage();
    paramTailleMaxFichier_=
      UsineLib.findUsine().creeHydraulique1dParametresTailleMaxFichier();
    postRubens_= true;
    postOpthyca_= false;
    postOpthycaQualiteDEau_=true;
    decalage_= 1.;
    variables_= new IDescriptionVariable[0];
    ajouteVariable(
      "Y",
      "Hauteur d'eau maximale",
      LUnite.M,
      LTypeNombre.REEL,
      3);
    ajouteVariable(
      "VMIN",
      "Vitesse mineur",
      LUnite.M_PAR_S,
      LTypeNombre.REEL,
      4);
    ajouteVariable("ZREF", "Cote du fond", LUnite.M, LTypeNombre.REEL, 4);
    ajouteVariable("Z", "Cote de l'eau", LUnite.M, LTypeNombre.REEL, 3);
    ajouteVariable(
      "QMIN",
      "D�bit mineur",
      LUnite.M3_PAR_S,
      LTypeNombre.REEL,
      3);
    ajouteVariable(
      "QMAJ",
      "D�bit majeur",
      LUnite.M3_PAR_S,
      LTypeNombre.REEL,
      3);
  }
  @Override
  public void dispose() {
    optionsListing_= null;
    optionsListingTracer_=null;
    paramStockage_= null;
    postRubens_= false;
    postOpthyca_= false;
    postOpthycaQualiteDEau_=false;
    decalage_= 0.;
    variables_= null;
    super.dispose();
  }
  // Attributs
  private IOptionsListing optionsListing_;
  @Override
  public IOptionsListing optionsListing() {
    return optionsListing_;
  }
  @Override
  public void optionsListing(IOptionsListing s) {
    if (optionsListing_==s) return;
    optionsListing_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "optionsListing");
  }

  private IOptionsListingTracer optionsListingTracer_;
  @Override
  public IOptionsListingTracer optionsListingTracer() {
      return optionsListingTracer_;
  }

  @Override
  public void optionsListingTracer(IOptionsListingTracer s) {
      if (optionsListingTracer_ == s)
          return;
      optionsListingTracer_ = s;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(),"optionsListingTracer");
  }

  private IParametresStockage paramStockage_;
  @Override
  public IParametresStockage paramStockage() {
    return paramStockage_;
  }
  @Override
  public void paramStockage(IParametresStockage s) {
    if (paramStockage_==s) return;
    paramStockage_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "paramStockage");
  }
  private IParametresTailleMaxFichier paramTailleMaxFichier_;
  @Override
  public IParametresTailleMaxFichier paramTailleMaxFichier() {
    return paramTailleMaxFichier_;
  }
  @Override
  public void paramTailleMaxFichier(IParametresTailleMaxFichier t) {
    if (paramTailleMaxFichier_==t) return;
    paramTailleMaxFichier_= t;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "paramTailleMaxFichier");
  }

  private boolean postRubens_;
  @Override
  public boolean postRubens() {
    return postRubens_;
  }
  @Override
  public void postRubens(boolean s) {
    if (postRubens_==s) return;
    postRubens_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "postRubens");
  }
  private boolean postOpthyca_;
  @Override
  public boolean postOpthyca() {
    return postOpthyca_;
  }
  @Override
  public void postOpthyca(boolean s) {
    if (postOpthyca_==s) return;
    postOpthyca_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "postOpthyca");
  }
  private boolean postOpthycaQualiteDEau_;
  @Override
  public boolean postOpthycaQualiteDEau() {
      return postOpthycaQualiteDEau_;
  }

  @Override
  public void postOpthycaQualiteDEau(boolean s) {
      if (postOpthycaQualiteDEau_ == s)
          return;
      postOpthycaQualiteDEau_ = s;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                            "postOpthycaQualiteDEau");
  }

  private double decalage_;
  @Override
  public double decalage() {
      return decalage_;
  }
  @Override
  public void decalage(double s) {
    if (decalage_==s) return;
    decalage_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "decalage");
  }
  private IDescriptionVariable[] variables_;
  @Override
  public IDescriptionVariable[] variables() {
    return variables_;
  }
  @Override
  public void variables(IDescriptionVariable[] s) {
    if (egale(variables_,s)) return;
    variables_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "variables");
  }
  // M�thodes
  @Override
  public boolean existeVariable(String nomVariable) {
    boolean existe= false;
    for (int i= 0; i < variables_.length; i++) {
      if (variables_[i].nom().equals(nomVariable))
        return true;
    }
    return existe;
  }
  @Override
  public void ajouteVariable(
    String nom,
    String description,
    LUnite unite,
    LTypeNombre type,
    int nbDecimal) {
    if (existeVariable(nom)) return;
    IDescriptionVariable descrip=
      UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    descrip.nom(nom);
    descrip.description(description);
    descrip.unite(unite);
    descrip.type(type);
    descrip.nbDecimales(nbDecimal);
    List listVar = new ArrayList(Arrays.asList(variables_));
    listVar.add(descrip);
    variables_= (IDescriptionVariable[])listVar.toArray(new IDescriptionVariable[0]);
   }
  @Override
  public void supprimeVariable(String nomVariable) {
    Vector newVars= new Vector();
    for (int i= 0; i < variables_.length; i++) {
      if (variables_[i].nom().equals(nomVariable)) {
        UsineLib.findUsine().supprimeHydraulique1dDescriptionVariable(
          variables_[i]);
      } else
        newVars.add(variables_[i]);
    }
    variables_= new IDescriptionVariable[newVars.size()];
    for (int i= 0; i < variables_.length; i++)
      variables_[i]= (IDescriptionVariable)newVars.get(i);
  }

}
