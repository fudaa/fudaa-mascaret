/**
 * @file         DParametresStockage.java
 * @creation     2000-08-10
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.IParametresStockage;
import org.fudaa.dodico.corba.hydraulique1d.IParametresStockageHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresStockageOperations;
import org.fudaa.dodico.corba.hydraulique1d.ISite;
import org.fudaa.dodico.corba.hydraulique1d.LOptionStockage;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier des "param�tres de stockage" dans les fichiers listing et r�sultats.
 * Pr�cise notamment les p�riodes et le d�but d'�criture dans les fichiers listing et r�sultats.
 * @version      $Revision: 1.9 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 **/
public class DParametresStockage
  extends DHydraulique1d
  implements IParametresStockage,IParametresStockageOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IParametresStockage) {
      IParametresStockage q= IParametresStockageHelper.narrow(_o);
      option(q.option());
      premierPasTempsStocke(q.premierPasTempsStocke());
      periodeListing(q.periodeListing());
      periodeResultat(q.periodeResultat());
      pasTempsImpression(q.pasTempsImpression());
      if (q.sites() != null) {
        ISite[] ip= new ISite[q.sites().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (ISite)q.sites()[i].creeClone();
        sites(ip);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IParametresStockage p=
      UsineLib.findUsine().creeHydraulique1dParametresStockage();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "parametresStockage";
    return s;
  }
  /*** IParametresStockage ***/
  // constructeurs
  public DParametresStockage() {
    super();
    option_= LOptionStockage.TOUTES_SECTIONS;
    premierPasTempsStocke_= 1;
    periodeListing_= 1;
    periodeResultat_= 1;
    pasTempsImpression_= 0.;
    sites_= new ISite[0];
  }
  @Override
  public void dispose() {
    option_= null;
    premierPasTempsStocke_= 0;
    periodeListing_= 0;
    periodeResultat_= 0;
    pasTempsImpression_= 0.;
    sites_= null;
    super.dispose();
  }
  // Attributs
  private LOptionStockage option_;
  @Override
  public LOptionStockage option() {
    return option_;
  }
  @Override
  public void option(LOptionStockage s) {
    if (option_.value()==s.value()) return;
    option_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "option");
  }
  private int premierPasTempsStocke_;
  @Override
  public int premierPasTempsStocke() {
    return premierPasTempsStocke_;
  }
  @Override
  public void premierPasTempsStocke(int s) {
    if (premierPasTempsStocke_==s) return;
    premierPasTempsStocke_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "premierPasTempsStocke");
  }
  private int periodeListing_;
  @Override
  public int periodeListing() {
    return periodeListing_;
  }
  @Override
  public void periodeListing(int s) {
    if (periodeListing_==s) return;
    periodeListing_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "periodeListing");
  }
  private int periodeResultat_;
  @Override
  public int periodeResultat() {
    return periodeResultat_;
  }
  @Override
  public void periodeResultat(int s) {
    if (periodeResultat_==s) return;
    periodeResultat_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "periodeResultat");
  }
  private double pasTempsImpression_;
  @Override
  public double pasTempsImpression() {
    return pasTempsImpression_;
  }
  @Override
  public void pasTempsImpression(double s) {
    if (pasTempsImpression_==s) return;
    pasTempsImpression_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "pasTempsImpression");
  }
  private ISite[] sites_;
  @Override
  public ISite[] sites() {
    return sites_;
  }
  @Override
  public void sites(ISite[] s) {
    if (sites_==s) return;
    sites_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "sites");
  }
  // methodes
  @Override
  public void creeSite(IBief biefRattache) {
    ISite site= UsineLib.findUsine().creeHydraulique1dSite();
    site.biefRattache(biefRattache);
    ISite[] us= new ISite[sites_.length + 1];
    for (int i= 0; i < sites_.length; i++)
      us[i]= sites_[i];
    us[sites_.length]= site;
    sites(us);
  }
  @Override
  public void supprimeSites(ISite[] sites) {
    Vector newus= new Vector();
    for (int i= 0; i < sites_.length; i++) {
      boolean trouve= false;
      for (int j= 0; j < sites.length; j++) {
        if (sites_[i] == sites[j])
          trouve= true;
      }
      if (!trouve)
        newus.add(sites_[i]);
    }
    ISite[] us= new ISite[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (ISite)newus.get(i);
    sites(us);
  }
}
