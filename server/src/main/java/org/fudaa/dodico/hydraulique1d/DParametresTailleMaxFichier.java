/**
 * @creation     2001-10-01
 * @modification $Date: 2006-04-06 12:22:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTailleMaxFichier;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTailleMaxFichierHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTailleMaxFichierOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier des "param�tres des tailles maximum des fichiers" g�r� par Fudaa-Mascaret.
 * @version      $Revision: 1.13 $ $Date: 2006-04-06 12:22:51 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DParametresTailleMaxFichier
  extends DHydraulique1d
  implements IParametresTailleMaxFichier,IParametresTailleMaxFichierOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IParametresTailleMaxFichier) {
      IParametresTailleMaxFichier q= IParametresTailleMaxFichierHelper.narrow(_o);
      maxListingCode(q.maxListingCode());
      maxListingDamocles(q.maxListingDamocles());
      maxListingCalage(q.maxListingCalage());
      maxListingTracer(q.maxListingTracer());
      maxResultatOpthyca(q.maxResultatOpthyca());
      maxResultatRubens(q.maxResultatRubens());
      maxResultatOpthycaTracer(q.maxResultatOpthycaTracer());
      maxResultatRubensTracer(q.maxResultatRubensTracer());
      maxResultatReprise(q.maxResultatReprise());
    }
  }
  @Override
  final public IObjet creeClone() {
    IParametresTailleMaxFichier p=
      UsineLib.findUsine().creeHydraulique1dParametresTailleMaxFichier();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "parametresTailleMaxFichier";
    return s;
  }
  /*** IParametresTemporels ***/
  // constructeurs
  public DParametresTailleMaxFichier() {
    super();
    maxListingCode_= 500.;
    maxListingDamocles_= 10.;
    maxListingCalage_=1000;
    maxListingTracer_=5000;
    maxResultatOpthyca_= 40000.;
    maxResultatRubens_= 20000.;
    maxResultatOpthycaTracer_= 40000.;
    maxResultatRubensTracer_= 20000.;
    maxResultatReprise_= 1000.;
  }
  @Override
  public void dispose() {
    maxListingCode_= 0.;
    maxListingDamocles_= 0.;
    maxListingCalage_=0;
    maxListingTracer_=0;
    maxResultatOpthyca_= 0.;
    maxResultatRubens_= 0.;
    maxResultatOpthycaTracer_= 0.;
    maxResultatRubensTracer_= 0.;
    maxResultatReprise_= 0.;
    super.dispose();
  }
  // Attributs
  private double maxListingCode_;
  @Override
  public double maxListingCode() {
    return maxListingCode_;
  }
  @Override
  public void maxListingCode(double s) {
    if (maxListingCode_==s) return;
    maxListingCode_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "maxListingCode");
  }
  private double maxListingDamocles_;
  @Override
  public double maxListingDamocles() {
    return maxListingDamocles_;
  }
  @Override
  public void maxListingDamocles(double s) {
    if (maxListingDamocles_==s) return;
    maxListingDamocles_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "maxListingDamocles");
  }
  private double maxResultatOpthyca_;
  @Override
  public double maxResultatOpthyca() {
    return maxResultatOpthyca_;
  }
  @Override
  public void maxResultatOpthyca(double s) {
    if (maxResultatOpthyca_==s) return;
    maxResultatOpthyca_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "maxResultatOpthyca");
  }
  private double maxResultatRubens_;
  @Override
  public double maxResultatRubens() {
    return maxResultatRubens_;
  }
  @Override
  public void maxResultatRubens(double s) {
    if (maxResultatRubens_==s) return;
    maxResultatRubens_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "maxResultatRubens");
  }
  private double maxResultatOpthycaTracer_;
  @Override
  public double maxResultatOpthycaTracer() {
    return maxResultatOpthycaTracer_;
  }
  @Override
  public void maxResultatOpthycaTracer(double s) {
    if (maxResultatOpthycaTracer_==s) return;
    maxResultatOpthycaTracer_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "maxResultatOpthycaTracer");
  }
  private double maxResultatRubensTracer_;
  @Override
  public double maxResultatRubensTracer() {
    return maxResultatRubensTracer_;
  }
  @Override
  public void maxResultatRubensTracer(double s) {
    if (maxResultatRubensTracer_==s) return;
    maxResultatRubens_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "maxResultatRubensTracer");
  }

  private double maxResultatReprise_;
  @Override
  public double maxResultatReprise() {
    return maxResultatReprise_;
  }
  @Override
  public void maxResultatReprise(double s) {
    if (maxResultatReprise_==s) return;
    maxResultatReprise_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "maxResultatReprise");
  }

  private double maxListingCalage_;
  @Override
  public double maxListingCalage() {
    return maxListingCalage_;
  }

  @Override
  public void maxListingCalage(double s) {
    if (maxListingCalage_==s) return;
    maxListingCalage_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "maxListingCalage");
  }

  private double maxListingTracer_;
  @Override
  public double maxListingTracer() {
      return maxListingTracer_;
  }

  @Override
  public void maxListingTracer(double s) {
      if (maxListingTracer_ == s)
          return;
      maxListingTracer_ = s;
      UsineLib.findUsine().fireObjetModifie(
              toString(),
              tie(),
              "maxListingTracer");
}


}
