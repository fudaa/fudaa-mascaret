/**
 * @creation     2000-07-27
 * @modification $Date: 2005-11-22 10:14:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTemporels;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTemporelsHelper;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTemporelsOperations;
import org.fudaa.dodico.corba.hydraulique1d.LCritereArret;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier des "param�tres temporelles" de l'�tude.
 * @version      $Revision: 1.10 $ $Date: 2005-11-22 10:14:56 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DParametresTemporels
  extends DHydraulique1d
  implements IParametresTemporels,IParametresTemporelsOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IParametresTemporels) {
      IParametresTemporels q= IParametresTemporelsHelper.narrow(_o);
      tempsInitial(q.tempsInitial());
      tempsFinal(q.tempsFinal());
      pasTemps(q.pasTemps());
      pasTempsImpression(q.pasTempsImpression());
      critereArret(q.critereArret());
      nbPasTemps(q.nbPasTemps());
      pasTempsVariable(q.pasTempsVariable());
      nbCourant(q.nbCourant());
      coteMax(q.coteMax());
      biefControle(q.biefControle());
      abscisseControle(q.abscisseControle());
    }
  }
  @Override
  final public IObjet creeClone() {
    IParametresTemporels p=
      UsineLib.findUsine().creeHydraulique1dParametresTemporels();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "parametresTemporels";
    return s;
  }
  /*** IParametresTemporels ***/
  // constructeurs
  public DParametresTemporels() {
    super();
    tempsInitial_= 0.;
    tempsFinal_= 1.;
    pasTemps_= 1.;
    critereArret_= LCritereArret.NB_PAS_TEMPS;
    nbPasTemps_= 1;
    pasTempsVariable_= false;
    nbCourant_= 0.8;
    coteMax_=0;
    biefControle_=1;
    abscisseControle_=0;
  }
  @Override
  public void dispose() {
    tempsInitial_= 0.;
    tempsFinal_= 0.;
    pasTemps_= 0.;
    critereArret_= null;
    nbPasTemps_= 0;
    pasTempsVariable_= false;
    nbCourant_= 0.;
    coteMax_=0;
    biefControle_=0;
    abscisseControle_=0;
    super.dispose();
  }
  // Attributs
  private double tempsInitial_;
  @Override
  public double tempsInitial() {
    return tempsInitial_;
  }
  @Override
  public void tempsInitial(double s) {
    if (tempsInitial_==s) return;
    tempsInitial_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "tempsInitial");
  }
  private double tempsFinal_;
  @Override
  public double tempsFinal() {
    return tempsFinal_;
  }
  @Override
  public void tempsFinal(double s) {
    if (tempsFinal_==s) return;
    tempsFinal_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "tempsFinal");
  }
  private double pasTemps_;
  @Override
  public double pasTemps() {
    return pasTemps_;
  }
  @Override
  public void pasTemps(double s) {
    if (pasTemps_==s) return;
    pasTemps_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pasTemps");
  }
  private double pasTempsImpression_;
  @Override
  public double pasTempsImpression() {
    return pasTempsImpression_;
  }
  @Override
  public void pasTempsImpression(double s) {
    if (pasTempsImpression_==s) return;
    pasTempsImpression_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "pasTempsImpression");
  }
  private LCritereArret critereArret_;
  @Override
  public LCritereArret critereArret() {
    return critereArret_;
  }
  @Override
  public void critereArret(LCritereArret s) {
    if (critereArret_.value()==s.value()) return;
    critereArret_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "critereArret");
  }
  private int nbPasTemps_;
  @Override
  public int nbPasTemps() {
    return nbPasTemps_;
  }
  @Override
  public void nbPasTemps(int s) {
    if (nbPasTemps_==s) return;
    nbPasTemps_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nbPasTemps");
  }
  private boolean pasTempsVariable_;
  @Override
  public boolean pasTempsVariable() {
    return pasTempsVariable_;
  }
  @Override
  public void pasTempsVariable(boolean s) {
    if (pasTempsVariable_==s) return;
    pasTempsVariable_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pasTempsVariable");
  }

  private double nbCourant_;
  @Override
  public double nbCourant() {
    return nbCourant_;
  }
  @Override
  public void nbCourant(double s) {
    if (nbCourant_==s) return;
    nbCourant_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nbCourant");
  }


  private double coteMax_;
  @Override
    public void coteMax(double coteMax) {
        if (coteMax_==coteMax) return;
        coteMax_ = coteMax;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteMax");
    }
  @Override
  public double coteMax() {
    return coteMax_;
  }

  private int biefControle_;
  @Override
  public void biefControle(int biefControle) {
    if (biefControle_==biefControle) return;
       biefControle_ = biefControle;
       UsineLib.findUsine().fireObjetModifie(toString(), tie(), "biefControle");
  }
  @Override
  public int biefControle() {
     return biefControle_;
  }

  private double abscisseControle_;
  @Override
   public void abscisseControle(double abscisseControle) {
        if (abscisseControle_==abscisseControle) return;
       abscisseControle_ = abscisseControle;
       UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisseControle");
   }
  @Override
   public double abscisseControle() {
    return abscisseControle_;
   }
}
