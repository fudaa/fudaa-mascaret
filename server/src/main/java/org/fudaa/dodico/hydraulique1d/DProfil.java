/**
 * @file         DProfil.java
 * @creation     2000-08-09
 * @modification $Date: 2005-09-01 17:12:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Comparator;

import org.fudaa.dodico.corba.geometrie.SPoint2D;
import org.fudaa.dodico.corba.hydraulique1d.IProfil;
import org.fudaa.dodico.corba.hydraulique1d.IProfilOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier des "profils" d'un bief.
 * G�re un tableau des points (X,Y) du profil, une abscisse curviligne du bief, un nom,
 * les limites pour le lit majeur droit et gauche et pour le lit mineur droit et gauche.
 * @version      $Revision: 1.10 $ $Date: 2005-09-01 17:12:46 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DProfil extends DHydraulique1d implements IProfil,IProfilOperations {
  public final static ProfilComparator COMPARATOR= new ProfilComparator();
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IProfil) {
      IProfil q= (IProfil)_o;
      nom(q.nom());
      abscisse(q.abscisse());
      points((SPoint2D[])q.points().clone());
      indiceLitMinDr(q.indiceLitMinDr());
      indiceLitMinGa(q.indiceLitMinGa());
      indiceLitMajDr(q.indiceLitMajDr());
      indiceLitMajGa(q.indiceLitMajGa());
    }
  }
  @Override
  final public IObjet creeClone() {
    IProfil p= UsineLib.findUsine().creeHydraulique1dProfil();
    p.abscisse(abscisse_+0.01);
    p.nom(nom_ + "bis");
    SPoint2D[] copie= new SPoint2D[points_.length];
    for (int i= 0; i < copie.length; i++) {
      SPoint2D pt= points_[i];
      copie[i]= new SPoint2D(pt.x, pt.y);
    }
    p.points(copie);
    p.indiceLitMajGa(indiceLitMajGa_);
    p.indiceLitMinGa(indiceLitMinGa_);
    p.indiceLitMinDr(indiceLitMinDr_);
    p.indiceLitMajDr(indiceLitMajDr_);
    return p;
  }
  @Override
  final public String toString() {
    String s= "profil " + numero_;
    return s;
  }
  /*** IProfil ***/
  // constructeurs
  public DProfil() {
    super();
    numero_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    nom_= "Profil " + numero_;
    abscisse_= 0.;
    points_= new SPoint2D[0];
    indiceLitMinDr_= 0;
    indiceLitMinGa_= 0;
    indiceLitMajDr_= 0;
    indiceLitMajGa_= 0;
  }
  @Override
  public void dispose() {
    nom_= null;
    abscisse_= 0.;
    points_= null;
    indiceLitMinDr_= -1;
    indiceLitMinGa_= -1;
    indiceLitMajDr_= -1;
    indiceLitMajGa_= -1;
    super.dispose();
  }
  // attributs
  private int numero_;
  @Override
  public int numero() {
    return numero_;
  }
  @Override
  public void numero(int s) {
    if (numero_==s) return;
    numero_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numero");
  }
  private String nom_;
  @Override
  public String nom() {
    return nom_;
  }
  @Override
  public void nom(String s) {
    if (nom_.equals(s)) return;
    nom_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nom");
  }
  private double abscisse_;
  @Override
  public double abscisse() {
    return abscisse_;
  }
  @Override
  public void abscisse(double s) {
    if (abscisse_==s) return;
    abscisse_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");
  }
  private SPoint2D[] points_;
  @Override
  public SPoint2D[] points() {
    return points_;
  }
  @Override
  public void points(SPoint2D[] s) {
    if (egale(points_, s)) return;
    points_= s;
    if (s != null)
      if (s.length <= 1)
        indiceLitMinDr(0);
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "points");
  }
  private int indiceLitMinDr_;
  @Override
  public int indiceLitMinDr() {
    if (points_.length == 0)
      indiceLitMinDr(-1);
    if ((indiceLitMinDr_ == -1) && (points_.length != 0))
      indiceLitMinDr(0);
    return indiceLitMinDr_;
  }
  @Override
  public void indiceLitMinDr(int s) {
    if (points_.length == 0) {
      indiceLitMinDr_= -1;
      indiceLitMajDr_= -1;
      indiceLitMinGa_= -1;
      indiceLitMajGa_= -1;
    } else if (points_.length == 1) {
      indiceLitMinDr_= 0;
      indiceLitMajDr_= 0;
      indiceLitMinGa_= 0;
      indiceLitMajGa_= 0;
    } else {
      if (s >= points_.length)
        s= points_.length - 1;
      if (s < 0)
        s= 0;
      if (s < indiceLitMinGa_)
        indiceLitMinGa(s);
      if (s < indiceLitMajGa_)
        indiceLitMajGa(s);
      if (s > indiceLitMajDr_)
        indiceLitMajDr(s);
      if (indiceLitMinDr_==s) return;
      indiceLitMinDr_= s;
    }
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "indiceLitMinDr");
  }
  private int indiceLitMinGa_;
  @Override
  public int indiceLitMinGa() {
    if (points_.length == 0)
      indiceLitMinGa(-1);
    if ((indiceLitMinGa_ == -1) && (points_.length != 0))
      indiceLitMinGa(0);
    return indiceLitMinGa_;
  }
  @Override
  public void indiceLitMinGa(int s) {
    if (points_.length == 0) {
      indiceLitMinDr_= -1;
      indiceLitMajDr_= -1;
      indiceLitMinGa_= -1;
      indiceLitMajGa_= -1;
    } else if (points_.length == 1) {
      indiceLitMinDr_= 0;
      indiceLitMajDr_= 0;
      indiceLitMinGa_= 0;
      indiceLitMajGa_= 0;
    } else {
      if (s >= points_.length)
        s= points_.length - 1;
      if (s < 0)
        s= 0;
      if (s > indiceLitMinDr_)
        indiceLitMinDr(s);
      if (s > indiceLitMajDr_)
        indiceLitMajDr(s);
      if (s < indiceLitMajGa_)
        indiceLitMajGa(s);
      if (indiceLitMinGa_==s) return;
      indiceLitMinGa_= s;
    }
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "indiceLitMinGa");
  }
  private int indiceLitMajDr_;
  @Override
  public int indiceLitMajDr() {
    if (points_.length == 0)
      indiceLitMajDr(-1);
    if ((indiceLitMajDr_ == -1) && (points_.length != 0))
      indiceLitMajDr(0);
    return indiceLitMajDr_;
  }
  @Override
  public void indiceLitMajDr(int s) {
    if (points_.length == 0) {
      indiceLitMinDr_= -1;
      indiceLitMajDr_= -1;
      indiceLitMinGa_= -1;
      indiceLitMajGa_= -1;
    } else if (points_.length == 1) {
      indiceLitMinDr_= 0;
      indiceLitMajDr_= 0;
      indiceLitMinGa_= 0;
      indiceLitMajGa_= 0;
    } else {
      if (s >= points_.length)
        s= points_.length - 1;
      if (s < 0)
        s= 0;
      if (s < indiceLitMinGa_)
        indiceLitMinGa(s);
      if (s < indiceLitMajGa_)
        indiceLitMajGa(s);
      if (s < indiceLitMinDr_)
        indiceLitMinDr(s);
      if (indiceLitMajDr_==s) return;
      indiceLitMajDr_= s;
    }
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "indiceLitMajDr");
  }
  private int indiceLitMajGa_;
  @Override
  public int indiceLitMajGa() {
    if (points_.length == 0)
      indiceLitMajGa(-1);
    if ((indiceLitMajGa_ == -1) && (points_.length != 0))
      indiceLitMajGa(0);
    return indiceLitMajGa_;
  }
  @Override
  public void indiceLitMajGa(int s) {
    if (points_.length == 0) {
      indiceLitMinDr_= -1;
      indiceLitMajDr_= -1;
      indiceLitMinGa_= -1;
      indiceLitMajGa_= -1;
    } else if (points_.length == 1) {
      indiceLitMinDr_= 0;
      indiceLitMajDr_= 0;
      indiceLitMinGa_= 0;
      indiceLitMajGa_= 0;
    } else {
      if (s >= points_.length)
        s= points_.length - 1;
      if (s < 0)
        s= 0;

      if (s > indiceLitMinGa_)
        indiceLitMinGa(s);
      if (s > indiceLitMajDr_)
        indiceLitMajDr(s);
      if (s > indiceLitMinDr_)
        indiceLitMinDr(s);

      if (indiceLitMajGa_==s) return;
      indiceLitMajGa_= s;
    }
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "indiceLitMajGa");
  }
  // methodes
  @Override
  public void indicModifPoint(int indice) {
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "points" + indice);
  }
  @Override
  public void coteMinMax(
    org.omg.CORBA.DoubleHolder coteMin,
    org.omg.CORBA.DoubleHolder coteMax) {
    double zMin= Double.MAX_VALUE;
    double zMax= Double.NEGATIVE_INFINITY;
    for (int i= 0; i < points_.length; i++) {
      zMin= Math.min(zMin, points_[i].y);
      zMax= Math.max(zMax, points_[i].y);
    }
    coteMin.value= zMin;
    coteMax.value= zMax;
  }
  @Override
  public double getFond() {
    double profLitI= Double.MAX_VALUE;
    for (int z= 0; z < points_.length; z++) {
      if (points_[z].y < profLitI)
        profLitI= points_[z].y;
    }
    return profLitI;
  }
  @Override
  public void creePoint(int indice) {
    SPoint2D[] newpts= new SPoint2D[points_.length + 1];
    if (indice < 0)
      indice= 0;
    if (indice >= points_.length)
      indice= points_.length;
    for (int i= 0; i < indice; i++)
      newpts[i]= points_[i];
    SPoint2D ptCree= new SPoint2D();
    try {
      ptCree.x= points_[indice - 1].x;
      ptCree.y= points_[indice - 1].y;
      ptCree.x= (points_[indice - 1].x + points_[indice].x) / 2;
      ptCree.y= (points_[indice - 1].y + points_[indice].y) / 2;
    } catch (ArrayIndexOutOfBoundsException ex) {}
    newpts[indice]= ptCree;
    //System.err.println("Point "+(indice)+" ins�r�");
    for (int i= indice + 1; i < newpts.length; i++)
      newpts[i]= points_[i - 1];
    points(newpts);
    if (points_.length == 0) {
      indiceLitMinGa(-1);
      indiceLitMinDr(-1);
      indiceLitMajGa(-1);
      indiceLitMajDr(-1);
    } else if (points_.length == 1) {
      indiceLitMinGa(0);
      indiceLitMinDr(0);
      indiceLitMajGa(0);
      indiceLitMajDr(0);
    } else {
      if (indice <= indiceLitMinDr_) {
        indiceLitMinDr(indiceLitMinDr_ + 1);
      }
      if (indice <= indiceLitMajDr_) {
        indiceLitMajDr(indiceLitMajDr_ + 1);
      }
      if (indice <= indiceLitMinGa_) {
        indiceLitMinGa(indiceLitMinGa_ + 1);
      }
      if (indice <= indiceLitMajGa_) {
        indiceLitMajGa(indiceLitMajGa_ + 1);
      }
    }
  }
  @Override
  public void supprimePoint(int indice) {
    if ((indice < 0) || (indice >= points_.length))
      return;
    SPoint2D[] newpts= new SPoint2D[points_.length - 1];
    for (int i= 0; i < indice; i++)
      newpts[i]= points_[i];
    //System.err.println("Point "+indice+" supprim�");
    for (int i= indice + 1; i < points_.length; i++)
      newpts[i - 1]= points_[i];
    points(newpts);
    if (points_.length == 0) { // dernier point enleve
      indiceLitMinGa(-1);
      indiceLitMinDr(-1);
      indiceLitMajGa(-1);
      indiceLitMajDr(-1);
      //System.err.println("Dernier point supprim�");
    } else if (points_.length == 1) {
      indiceLitMinGa(0);
      indiceLitMinDr(0);
      indiceLitMajGa(0);
      indiceLitMajDr(0);
    } else {
      if (indice <= indiceLitMinDr()) {
        indiceLitMinDr(indiceLitMinDr_ - 1);
      }
      if (indice <= indiceLitMajDr()) {
        indiceLitMajDr(indiceLitMajDr_ - 1);
      }
      if (indice <= indiceLitMinGa()) {
        indiceLitMinGa(indiceLitMinGa_ - 1);
      }
      if (indice <= indiceLitMajGa()) {
        indiceLitMajGa(indiceLitMajGa_ - 1);
      }
    }
  }
  @Override
  public String description() {
    return nom() + " X=" + abscisse();
  }
  @Override
  public boolean hasZoneStockage() {
    if ((indiceLitMajGa() == 0) || (indiceLitMajGa() == points().length - 1)) {
      if ((indiceLitMajDr() == 0)
        || (indiceLitMajDr() == points().length - 1)) {} else
        return true;
    } else
      return true;
    return false;
  }
  private final static boolean egale(SPoint2D[] p, SPoint2D[] p2) {
    if (p==p2) return true;
    if (p==null || p2==null) return false;

    int length = p.length;
    if (p2.length != length) return false;

    for (int i=0; i<length; i++)
        if (!((p[i].x==p2[i].x)&&(p[i].y==p2[i].y)))
            return false;

    return true;
  }
}
class ProfilComparator implements Comparator {
  ProfilComparator() {}
  @Override
  public int compare(Object o1, Object o2) {
    double xo1= ((IProfil)o1).abscisse();
    double xo2= ((IProfil)o2).abscisse();
    return xo1 < xo2 ? -1 : xo1 == xo2 ? 0 : 1;
  }
  @Override
  public boolean equals(Object obj) {
    return false;
  }
}
