/**
 * @file         DReseau.java
 * @creation     2000-07-24
 * @modification $Date: 2007-03-23 13:41:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Arrays;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.IBarragePrincipal;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ICasier;
import org.fudaa.dodico.corba.hydraulique1d.IExtremite;
import org.fudaa.dodico.corba.hydraulique1d.ILiaison;
import org.fudaa.dodico.corba.hydraulique1d.ILimite;
import org.fudaa.dodico.corba.hydraulique1d.INoeud;
import org.fudaa.dodico.corba.hydraulique1d.IProfil;
import org.fudaa.dodico.corba.hydraulique1d.IReseau;
import org.fudaa.dodico.corba.hydraulique1d.IReseauOperations;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsBief;
import org.fudaa.dodico.corba.hydraulique1d.ISingularite;
import org.fudaa.dodico.corba.hydraulique1d.IZoneFrottement;
import org.fudaa.dodico.corba.hydraulique1d.IZonePlanimetrage;
import org.fudaa.dodico.corba.hydraulique1d.SIndiceZoneStockage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IApport;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISource;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoir;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IPerteCharge;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuil;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilDenoye;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilGeometrique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLimniAmont;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLoi;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilNoye;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAmont;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAval;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTranscritique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilVanne;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier du "r�seau" hydraulique de l'�tude.
 * G�re les �l�ments du r�seau � savoirs : les biefs, les casiers, les liaisons avec les casiers,
 * ainsi que les noeuds (confluents) et les singularit�s non rattach�s aux biefs.
 * @version      $Revision: 1.30 $ $Date: 2007-03-23 13:41:24 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DReseau extends DHydraulique1d implements IReseau,IReseauOperations {
  private transient Vector nouveauxNoeud_=new Vector();
  private transient Vector nouvellesSing_=new Vector();
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IReseau) {
      IReseau q= (IReseau)_o;
      if (q.biefs() != null) {
        IBief[] ip= new IBief[q.biefs().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (IBief)q.biefs()[i].creeClone();
        biefs(ip);
      }
      if (q.casiers() != null) {
        ICasier[] ip= new ICasier[q.casiers().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (ICasier)q.casiers()[i].creeClone();
        casiers(ip);
      }
      if (q.liaisons() != null) {
        ILiaison[] ip= new ILiaison[q.liaisons().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (ILiaison)q.liaisons()[i].creeClone();
        liaisons(ip);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IReseau r= UsineLib.findUsine().creeHydraulique1dReseau();
    r.initialise(tie());
    return r;
  }
  @Override
  final public String toString() {
    String s= "r�seau";
    return s;
  }
  /*** IReseau ***/
  // constructeurs
  public DReseau() {
    super();
    biefs_= new IBief[0];
    casiers_= new ICasier[0];
    liaisons_= new ILiaison[0];
    nbPasPlanimetrageImpose_=false;
    nbPasPlanimetrage_ = 50;
  }
  @Override
  public void dispose() {
    biefs_= null;
    casiers_= null;
    casiers_= null;
    nbPasPlanimetrageImpose_=false;
    nbPasPlanimetrage_ = 0;
    super.dispose();
  }
  // attributs
  private IBief[] biefs_;
  @Override
  public IBief[] biefs() {
    return biefs_;
  }
  @Override
  public void biefs(IBief[] s) {
    if (egale(biefs_, s)) return;
    biefs_= s;
    trieBiefs();
    initIndiceBief();
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "biefs");
  }
  private ICasier[] casiers_;
  @Override
  public ICasier[] casiers() {
    return casiers_;
  }
  @Override
  public void casiers(ICasier[] s) {
    if (egale(casiers_, s)) return;
    casiers_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "casiers");
  }
  private ILiaison[] liaisons_;
  @Override
  public ILiaison[] liaisons() {
    return liaisons_;
  }
  @Override
  public void liaisons(ILiaison[] s) {
    if (egale(liaisons_, s)) return;
    liaisons_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "liaisons");
  }


  private boolean nbPasPlanimetrageImpose_;
  @Override
  public boolean nbPasPlanimetrageImpose() {
      return nbPasPlanimetrageImpose_;
  }
  @Override
  public void nbPasPlanimetrageImpose(boolean nbPasPlanimetrageImpose) {
      if (nbPasPlanimetrageImpose == nbPasPlanimetrageImpose_) return;
      nbPasPlanimetrageImpose_ = nbPasPlanimetrageImpose;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nbPasPlanimetrageImpose");
  }
  private int nbPasPlanimetrage_;
  @Override
  public int nbPasPlanimetrage() {
      return nbPasPlanimetrage_;
  }
  @Override
  public void nbPasPlanimetrage(int nbPasPlanimetrage) {
      if (nbPasPlanimetrage == nbPasPlanimetrage_) return;
      nbPasPlanimetrage_ = nbPasPlanimetrage;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nbPasPlanimetrage");
  }
  // methodes
  @Override
  public int getIndiceCasier(ICasier casier) {
    for (int i= 0; i < casiers_.length; i++) {
      if (casier == casiers_[i])
        return i;
    }
    return -1;
  }
  @Override
  public int getIndiceLiaison(ILiaison liaison) {
    for (int i= 0; i < liaisons_.length; i++) {
      if (liaison == liaisons_[i])
        return i;
    }
    return -1;
  }
  @Override
  public ICasier ajouterCasier() {
    ICasier casier= UsineLib.findUsine().creeHydraulique1dCasier();
    ICasier casiers[]= new ICasier[casiers_.length + 1];
    for (int i= 0; i < casiers_.length; i++)
      casiers[i]= casiers_[i];
    casiers[casiers.length - 1]= casier;
    casiers(casiers);
    initNumeroCasier();
    return casier;
  }
  @Override
  public void supprimeCasiers(ICasier[] _casiers) {
    if (casiers_ == null)
      return;
    if (casiers_.length == 0)
        return;
    Vector newcasiers= new Vector();

    boolean casierTrouvee =false;
    for (int i= 0; i < casiers_.length; i++) {
      for (int j= 0; j < _casiers.length; j++) {
        if (casiers_[i] == _casiers[j]) {
        	UsineLib.findUsine().supprimeHydraulique1dCasier(casiers_[i]);
          casierTrouvee = true;
        }
        if (casierTrouvee) break;
      }
      if (!casierTrouvee) newcasiers.add(casiers_[i]);
      casierTrouvee = false;
   }


    casiers_= new ICasier[newcasiers.size()];
    for (int i= 0; i < casiers_.length; i++)
      casiers_[i]= (ICasier)newcasiers.get(i);
    initNumeroCasier();
  }
  @Override
  public ILiaison ajouterLiaison() {
	  System.err.println("on est dans ajouterLiaison");
    ILiaison liaison= UsineLib.findUsine().creeHydraulique1dLiaison();
    ILiaison liaisons[]= new ILiaison[liaisons_.length + 1];
    for (int i= 0; i < liaisons_.length; i++)
      liaisons[i]= liaisons_[i];
    liaisons[liaisons.length - 1]= liaison;
    liaisons(liaisons);
    initNumeroLiaison();
    return liaison;
  }
  @Override
  public void supprimeLiaisons(ILiaison[] _liaisons) {
    if (liaisons_ == null)
      return;
    if (liaisons_.length == 0)
        return;
    Vector newliaisons= new Vector();

    boolean liaisonTrouvee =false;
    for (int i= 0; i < liaisons_.length; i++) {
      for (int j= 0; j < _liaisons.length; j++) {
        if (liaisons_[i] == _liaisons[j]) {
        	UsineLib.findUsine().supprimeHydraulique1dLiaison(liaisons_[i]);
          liaisonTrouvee = true;
        }
        if (liaisonTrouvee) break;
      }
      if (!liaisonTrouvee) newliaisons.add(liaisons_[i]);
      liaisonTrouvee = false;
   }

    liaisons_= new ILiaison[newliaisons.size()];
    for (int i= 0; i < liaisons_.length; i++)
      liaisons_[i]= (ILiaison)newliaisons.get(i);
    initNumeroLiaison();
  }
  @Override
  public void initIndiceNumero() {
    initIndiceBief();
    initNumeroCasier();
    initNumeroLiaison();
    initNumeroNoeud();
    initNumeroSing();
  }
  @Override
  public void initIndiceBief() {
    int numeroProfil=1;
    for (int i= 0; i < biefs_.length; i++) {
      IBief b = biefs_[i];
      b.indice(i);
      IProfil[] profs = b.profils();
      if(profs != null) {
        for (int j = 0; j < profs.length; j++) {
          IProfil p = profs[j];
          p.numero(numeroProfil);
          numeroProfil++;
        }
      }
    }

  }
  @Override
  public void initNumeroCasier() {
    for (int i= 0; i < casiers_.length; i++) {
      casiers_[i].numero(i + 1);
    }
  }
  @Override
  public void initNumeroLiaison() {
    for (int i= 0; i < liaisons_.length; i++) {
      liaisons_[i].numero(i + 1);
    }
  }

  public void initNumeroNoeud() {
    INoeud[] noeudsConnectes = noeudsConnectesBiefs();
    for (int i = 0; i < noeudsConnectes.length; i++) {
      noeudsConnectes[i].numero(i+1);
    }
    int num = noeudsConnectes.length+1;
    int nbNouveauxNoeuds = nouveauxNoeud_.size();
    for (int i = 0; i < nbNouveauxNoeuds; i++) {
      INoeud inoeud = (INoeud)nouveauxNoeud_.get(i);
      if (!isContient(noeudsConnectes, inoeud)) {
        inoeud.numero(num);
        num++;
      }
    }
  }

  public void initNumeroSing() {
    ISingularite[] singularitesConnectees = singularites();
    for (int i = 0; i < singularitesConnectees.length; i++) {
      singularitesConnectees[i].numero(i+1);
    }
    int num = singularitesConnectees.length+1;
    int nbNouvellesSing = nouvellesSing_.size();
    for (int i = 0; i < nbNouvellesSing; i++) {
      ISingularite ising = (ISingularite)nouvellesSing_.get(i);
      if (!isContient(singularitesConnectees, ising)) {
        ising.numero(num);
        num++;
      }
    }
  }

  @Override
  public IBief creeBief() {
    IBief bief= UsineLib.findUsine().creeHydraulique1dBief();
    IBief biefs[]= new IBief[biefs_.length + 1];
    for (int i= 0; i < biefs_.length; i++)
      biefs[i]= biefs_[i];
    biefs[biefs.length - 1]= bief;
    biefs(biefs);
    initIndiceBief();
    return bief;
  }
  @Override
  public INoeud creeNoeud() {
 /*   if ((biefs_ == null) || (biefs_.length == 0)) {
      //erreur
      System.err.println("DReseau#creeNoeud: aucun bief d�fini");
      return null;
    }
    INoeud noeud= null;
    IExtremite e= null;
    for (int i= 0; i < biefs_.length; i++) {
      IExtremite e1= biefs_[i].extrAmont();
      IExtremite e2= biefs_[i].extrAval();
      if (e1.noeudRattache() == null) {
        e= e1;
        break;
      }
      if (e2.noeudRattache() == null) {
        e= e2;
        break;
      }
    }
    if (e != null)
      noeud= e.creeNoeud();*/
    INoeud inoeud = UsineLib.findUsine().creeHydraulique1dNoeud();
    nouveauxNoeud_.add(inoeud);
    initNumeroNoeud();
    return inoeud;
  }
  @Override
  public IApport creeApport() {
    IApport ising = UsineLib.findUsine().creeHydraulique1dApport();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISource creeSource() {
  ISource ising = UsineLib.findUsine().creeHydraulique1dSource();
  nouvellesSing_.add(ising);
  initNumeroSing();
  return ising;
}

  @Override
  public IPerteCharge creePerteCharge() {
    IPerteCharge ising = UsineLib.findUsine().creeHydraulique1dPerteCharge();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuil creeSeuil() {
    ISeuil ising = UsineLib.findUsine().creeHydraulique1dSeuilLoi();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilTranscritique creeSeuilTranscritique() {
    ISeuilTranscritique ising = UsineLib.findUsine().creeHydraulique1dSeuilTranscritique();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilNoye creeSeuilNoye() {
    ISeuilNoye ising = UsineLib.findUsine().creeHydraulique1dSeuilNoye();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilDenoye creeSeuilDenoye() {
    ISeuilDenoye ising = UsineLib.findUsine().creeHydraulique1dSeuilDenoye();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilLimniAmont creeSeuilLimniAmont() {
    ISeuilLimniAmont ising = UsineLib.findUsine().creeHydraulique1dSeuilLimniAmont();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilTarageAmont creeSeuilTarageAmont() {
    ISeuilTarageAmont ising = UsineLib.findUsine().creeHydraulique1dSeuilTarageAmont();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilTarageAval creeSeuilTarageAval() {
    ISeuilTarageAval ising = UsineLib.findUsine().creeHydraulique1dSeuilTarageAval();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilLoi creeSeuilLoi() {
    ISeuilLoi ising = UsineLib.findUsine().creeHydraulique1dSeuilLoi();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilGeometrique creeSeuilGeometrique() {
    ISeuilGeometrique ising = UsineLib.findUsine().creeHydraulique1dSeuilGeometrique();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public ISeuilVanne creeSeuilVanne() {
    ISeuilVanne ising = UsineLib.findUsine().creeHydraulique1dSeuilVanne();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public IDeversoir creeDeversoir() {
    IDeversoir ising = UsineLib.findUsine().creeHydraulique1dDeversoirComportementZCoefQ();
    nouvellesSing_.add(ising);
    initNumeroSing();
    return ising;
  }
  @Override
  public IBarragePrincipal creeBarragePrincipal() {
    return UsineLib.findUsine().creeHydraulique1dBarragePrincipal();
  }
  @Override
  public void creeResultatsReseau() {
    IBief[] biefs= biefs();
    for (int i= 0; i < biefs.length; i++) {
      IResultatsBief rb= UsineLib.findUsine().creeHydraulique1dResultatsBief();
      biefs[i].resultatsBief(rb);
    }
  }
  @Override
  public void supprimeBiefs(IBief[] _biefs) {
    if (biefs_ == null)
      return;
    if (biefs_.length == 0)
        return;
    Vector newbiefs= new Vector();

    boolean biefTrouvee =false;
    for (int i= 0; i < biefs_.length; i++) {
      for (int j= 0; j < _biefs.length; j++) {
        if (biefs_[i] == _biefs[j]) {
            supprimeZonesPlanimAvecBief(biefs_[i]);
            supprimeZonesFrottementAvecBief(biefs_[i]);
        	UsineLib.findUsine().supprimeHydraulique1dBief(biefs_[i]);
          biefTrouvee = true;
        }
        if (biefTrouvee) break;
      }
      if (!biefTrouvee) newbiefs.add(biefs_[i]);
      biefTrouvee = false;
   }

    biefs_= new IBief[newbiefs.size()];
    for (int i= 0; i < biefs_.length; i++)
      biefs_[i]= (IBief)newbiefs.get(i);
    trieBiefs();
    initIndiceBief();
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "biefs");
  }
  private void supprimeZonesPlanimAvecBief(IBief bief) {
    IZonePlanimetrage[] zones= bief.zonesPlanimetrage();
    int taille = zones.length;
    for (int i = 0; i < taille; i++) {
      bief.supprimeZonePlanimetrage(zones[i]);
    }
  }
  private void supprimeZonesFrottementAvecBief(IBief bief) {
    IZoneFrottement[] zones= bief.zonesFrottement();
    int taille = zones.length;
    for (int i = 0; i < taille; i++) {
      bief.supprimeZoneFrottement(zones[i]);
    }
  }
  @Override
  public void supprimeSingularites(ISingularite[] sings) {
    boolean existeSuppression = false;
    for (int i = 0; i < sings.length; i++) {
      boolean supression = supprimeSingularite(sings[i]);
      if (supression && !existeSuppression) {
        existeSuppression = true;
      }
    }
    if (existeSuppression) {
      initNumeroSing();
    }
  }
  private boolean supprimeSingularite(ISingularite sing) {
    if (sing == null) return false;
    for (int i= 0; i < biefs_.length; i++) {
      ISingularite[] biefsings= biefs_[i].singularites();
      for (int j= 0; j < biefsings.length; j++) {
        if (biefsings[j] == sing) {
          biefs_[i].supprimeSingularite(biefsings[j]);
          nouvellesSing_.remove(sing);
          return true;
        }
      }
    }
    DSingularite.supprimeSingularite(sing);
    return nouvellesSing_.remove(sing);
  }
  @Override
  public void supprimeProfils(IProfil[] profils) {
    for (int i= 0; i < biefs_.length; i++) {
      IProfil[] biefprofs= biefs_[i].profils();
      for (int j= 0; j < biefprofs.length; j++) {
        for (int k= 0; k < profils.length; k++) {
          if (biefprofs[j].numero() == profils[k].numero()) {
            biefs_[i].supprimeProfils(new IProfil[] { biefprofs[j] });
          }
        }
      }
    }
  }
  @Override
  public void supprimeExtremite(IExtremite extremite) {
    if (extremite != null) {
      UsineLib.findUsine().supprimeHydraulique1dExtremite(extremite);
    }
  }
  @Override
  public void supprimeNoeud(INoeud noeud) {
    if (noeud != null) {
      INoeud n= null;
      for (int i= 0; i < biefs_.length; i++) {
        IBief b= biefs_[i];
        n= b.extrAmont().noeudRattache();
        if (n == noeud) {
          b.extrAmont().noeudRattache(null);
        }
        n= b.extrAval().noeudRattache();
        if (n == noeud) {
          b.extrAval().noeudRattache(null);
        }
      }
      nouveauxNoeud_.remove(noeud);
      UsineLib.findUsine().supprimeHydraulique1dNoeud(noeud);
      initNumeroNoeud();
    }
  }
/*  public ILimite creeLimite(boolean presenceQualiteDEau) {
    if ((biefs_ == null) || (biefs_.length == 0)) {
      //erreur
      System.err.println("DReseau#creeLimite: aucun bief d�fini");
      return null;
    }
    ILimite limite= null;
    IExtremite e= null;
    for (int i= 0; i < biefs_.length; i++) {
      IExtremite e1= biefs_[i].extrAmont();
      IExtremite e2= biefs_[i].extrAval();
      if (e1.conditionLimite() == null) {
        e= e1;
        break;
      }
      if (e2.conditionLimite() == null) {
        e= e2;
        break;
      }
    }
    if (e != null)
      limite= e.creeLimite();
      if (presenceQualiteDEau) limiteQualiteDEau = e.creeLimiteQualiteDEau();
    return limite;
  }*/

  @Override
  public void supprimeLimites(ILimite[] limites) {
    for (int i= 0; i < biefs_.length; i++) {
      ILimite[] bieflims= biefs_[i].limites();
      for (int j= 0; j < bieflims.length; j++) {
        for (int k= 0; k < limites.length; k++) {
          if (bieflims[j] == limites[k]) {
            biefs_[i].supprimeLimite(bieflims[j]);
          }
        }
      }
    }
  }


  @Override
  public void supprimeZonesFrottement(IZoneFrottement[] zones) {
    for (int i= 0; i < biefs_.length; i++) {
      IZoneFrottement[] biefzones= biefs_[i].zonesFrottement();
      for (int j= 0; j < biefzones.length; j++) {
        for (int k= 0; k < zones.length; k++) {
          if (biefzones[j] == zones[k]) {
            biefs_[i].supprimeZoneFrottement(biefzones[j]);
          }
        }
      }
    }
  }
  @Override
  public void supprimeZonesPlanimetrage(IZonePlanimetrage[] zones) {
    for (int i= 0; i < biefs_.length; i++) {
      IZonePlanimetrage[] biefzones= biefs_[i].zonesPlanimetrage();
      for (int j= 0; j < biefzones.length; j++) {
        for (int k= 0; k < zones.length; k++) {
          if (biefzones[j] == zones[k]) {
            biefs_[i].supprimeZonePlanimetrage(biefzones[j]);
          }
        }
      }
    }
  }
  @Override
  public IZonePlanimetrage creeZonePlanimetrage(
    int profilDebut,
    int profilFin,
    double taillePas) {
    IBief bief= getBiefContenantProfilNumero(profilDebut);
    if (getBiefContenantProfilNumero(profilFin) != bief) {
      System.err.println(
        "DReseau: erreur: zone planimetrage definie sur plus d'un bief!");
      return null;
    }
    IZonePlanimetrage zone= bief.creeZonePlanimetrage();
    zone.abscisseDebut(getProfilNumero(profilDebut).abscisse());
    zone.abscisseFin(getProfilNumero(profilFin).abscisse());
    zone.taillePas(taillePas);
    return zone;
  }
  @Override
  public SIndiceZoneStockage[] getIndicesZonesStockages() {
    Vector vZones= new Vector();
    for (int i= 0; i < biefs_.length; i++) {
      IBief b= biefs_[i];
      for (int j= 0; j < b.profils().length; j++) {
        IProfil p= b.profils()[j];
        if (p.hasZoneStockage()) {
          vZones.add(
            new SIndiceZoneStockage(
              i,
              j,
              p.indiceLitMajDr(),
              p.indiceLitMajGa()));
        }
      }
    }
    SIndiceZoneStockage[] zones= new SIndiceZoneStockage[vZones.size()];
    for (int i= 0; i < zones.length; i++) {
      zones[i]= (SIndiceZoneStockage)vZones.get(i);
    }
    return zones;
  }
  @Override
  public void setIndicesZonesStockage(SIndiceZoneStockage[] indicesZonesStockages) {
    // suppression des zones de stockages existante
    for (int i= 0; i < biefs_.length; i++) {
      IProfil[] profs= biefs_[i].profilsAvecZonesStockage();
      if (profs != null) {
        for (int j= 0; j < profs.length; j++) {
          IProfil p= profs[j];
          p.indiceLitMajGa(0);
          p.indiceLitMajDr(p.points().length - 1);
        }
      }
    }
    // ajout des zones sp�cifi� par indicesZonesStockages
    for (int i= 0; i < indicesZonesStockages.length; i++) {
      SIndiceZoneStockage z= indicesZonesStockages[i];
      IProfil p= biefs_[z.indiceBief].profils()[z.indiceProfil];
      p.indiceLitMajDr(z.indiceLitMajDroit);
      p.indiceLitMajGa(z.indiceLitMajGauche);
    }
  }
  @Override
  public double getXOrigine() {
    double res= Double.MAX_VALUE;
    for (int i= 0; i < biefs_.length; i++) {
      double rb= biefs_[i].getXOrigine();
      if (rb < res)
        res= rb;
    }
    return res;
  }
  @Override
  public double getXFin() {
    double res= Double.NEGATIVE_INFINITY;
    for (int i= 0; i < biefs_.length; i++) {
      double rb= biefs_[i].getXFin();
      if (rb > res)
        res= rb;
    }
    return res;
  }
  @Override
  public IExtremite[] extremites() {
    Vector extremites= new Vector();
    for (int i= 0; i < biefs_.length; i++) {
      IBief b= biefs_[i];
      if (!extremites.contains(b.extrAmont()))
        extremites.addElement(b.extrAmont());
      if (!extremites.contains(b.extrAval()))
        extremites.addElement(b.extrAval());
    }
    IExtremite[] extremitesNew= new IExtremite[extremites.size()];
    for (int i= 0; i < extremitesNew.length; i++)
      extremitesNew[i]= (IExtremite)extremites.get(i);
    return extremitesNew;
  }
  @Override
  public ILimite[] limites() {
    Vector limites= new Vector();
    IExtremite[] extremites= extremites();
    for (int i= 0; i < extremites.length; i++) {
      ILimite cond= ((IExtremite)extremites[i]).conditionLimite();
      if ((cond != null) && (!limites.contains(cond)))
        limites.add(cond);
    }
    ILimite[] res= new ILimite[limites.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (ILimite)limites.get(i);
    return res;
  }
  @Override
  public IExtremite[] extremitesLibres() {
    Vector extremitesLibre= new Vector();
    for (int i= 0; i < biefs_.length; i++) {
      IBief b= (IBief)biefs_[i];
      if ((b.extrAmont().noeudRattache() == null)
        && (!extremitesLibre.contains(b.extrAmont()))) {
        extremitesLibre.addElement(b.extrAmont());
      }
      if ((b.extrAval().noeudRattache() == null)
        && (!extremitesLibre.contains(b.extrAval()))) {
        extremitesLibre.addElement(b.extrAval());
      }
    }
    IExtremite[] res= new IExtremite[extremitesLibre.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (IExtremite)extremitesLibre.get(i);
    return res;
  }
  @Override
  public IExtremite getExtremiteNumero(int numero) {
    IExtremite res= null;
    for (int i= 0; i < biefs_.length; i++) {
      IExtremite e= biefs_[i].extrAmont();
      if (e.numero() == numero) {
        res= e;
        break;
      }
      e= biefs_[i].extrAval();
      if (e.numero() == numero) {
        res= e;
        break;
      }
    }
    return res;
  }
  @Override
  public IExtremite getExtremiteContenantLimite(ILimite limite) {
    IExtremite res= null;
    for (int i= 0; i < biefs_.length; i++) {
      IExtremite e= biefs_[i].extrAmont();
      if (e.conditionLimite() == limite) {
        res= e;
        break;
      }
      e= biefs_[i].extrAval();
      if (e.conditionLimite() == limite) {
        res= e;
        break;
      }
    }
    return res;
  }
  @Override
  public INoeud[] noeudsConnectesBiefs() {
    Vector noeuds= new Vector();
    INoeud n= null;
    for (int i= 0; i < biefs_.length; i++) {
      IBief b= biefs_[i];
      n= b.extrAmont().noeudRattache();
      if ((n != null) && (!noeuds.contains(n)))
        noeuds.addElement(n);
      n= b.extrAval().noeudRattache();
      if ((n != null) && (!noeuds.contains(n)))
        noeuds.addElement(n);
    }
    INoeud[] res= new INoeud[noeuds.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (INoeud)noeuds.get(i);
    return res;
  }
  @Override
  public INoeud getNoeudNumero(int numero) {
    INoeud[] noeuds= noeudsConnectesBiefs();
    for (int i= 0; i < noeuds.length; i++) {
      if (noeuds[i].numero() == numero)
        return noeuds[i];
    }
    return null;
  }
  @Override
  public int getNumeroApparitionProfil(IProfil profil) {
    int numero= 1;
    for (int i= 0; i < biefs_.length; i++) {
      for (int j= 0; j < biefs_[i].profils().length; j++) {
        if (biefs_[i].profils()[j] == profil) {
          return numero;
        }
        numero++;
      }
    }
    return 0;
  }
  @Override
  public IProfil[] profils() {
    int nb= nbProfils();
    IProfil[] res= new IProfil[nb];
    int nbPrec= 0;
    for (int b= 0; b < biefs_.length; b++) {
      IProfil[] ps= biefs_[b].profils();
      for (int i= 0; i < ps.length; i++)
        res[nbPrec + i]= ps[i];
      nbPrec += ps.length;
    }
    return res;
  }
  @Override
  public ISingularite[] singularites() {
    Vector resV= new Vector();
    for (int b= 0; b < biefs_.length; b++) {
      ISingularite[] ps= biefs_[b].singularites();
      for (int i= 0; i < ps.length; i++)
        resV.add(ps[i]);
    }
    return (ISingularite[])resV.toArray(new ISingularite[0]);
  }
  @Override
  public ISingularite getSingulariteId(int id) {
    ISingularite[] singularites= singularites();
    for (int i= 0; i < singularites.length; i++) {
      if (singularites[i].id() == id)
        return singularites[i];
    }
    return null;
  }

  @Override
  public IZonePlanimetrage[] zonesPlanimetrage() {
    Vector resV= new Vector();
    for (int b= 0; b < biefs_.length; b++) {
      IZonePlanimetrage[] ps= biefs_[b].zonesPlanimetrage();
      if (ps != null) {
        for (int i = 0; i < ps.length; i++)
          resV.add(ps[i]);
      }
    }
    IZonePlanimetrage[] res= new IZonePlanimetrage[resV.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (IZonePlanimetrage)resV.get(i);
    return res;
  }
  @Override
  public IZoneFrottement[] zonesFrottement() {
    Vector resV= new Vector();
    for (int b= 0; b < biefs_.length; b++) {
      IZoneFrottement[] ps= biefs_[b].zonesFrottement();
      for (int i= 0; i < ps.length; i++)
        resV.add(ps[i]);
    }
    IZoneFrottement[] res= new IZoneFrottement[resV.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (IZoneFrottement)resV.get(i);
    return res;
  }
  @Override
  public IProfil getProfilNumero(int numero) {
    IProfil[] profils= profils();
    for (int i= 0; i < profils.length; i++) {
      if (profils[i].numero() == numero)
        return profils[i];
    }
    return null;
  }
  @Override
  public IProfil getProfilAbscisse(double abscisseProfil) {
    IProfil[] profils= profils();
    for (int i= 0; i < profils.length; i++) {
      if (CGlobal.egale(profils[i].abscisse(), abscisseProfil))
        return profils[i];
    }
    return null;
  }
  @Override
  public int getIndiceProfilNumero(int numero) {
    IProfil[] profils= profils();
    for (int i= 0; i < profils.length; i++) {
      if (profils[i].numero() == numero)
        return i;
    }
    return -1;
  }
  @Override
  public int getIndiceProfilAbscisse(double abscisseProfil) {
    IProfil[] profils= profils();
    for (int i= 0; i < profils.length; i++) {
      if (CGlobal.egale(profils[i].abscisse(), abscisseProfil))
        return i;
    }
    return -1;
  }
  @Override
  public int getIndiceBief(IBief bief) {
    for (int i= 0; i < biefs_.length; i++) {
      if (bief == biefs_[i])
        return i;
    }
    return -1;
  }
  @Override
  public boolean hasSingularites() {
    boolean res= false;
    for (int i= 0; i < biefs_.length; i++) {
      res= res || (biefs_[i].seuils().length > 0);
      if (res)
        break;
    }
    return res;
  }
  @Override
  public boolean hasZonesStockage() {
    boolean res= false;
    for (int i= 0; i < biefs_.length; i++) {
      res= res || (biefs_[i].profilsAvecZonesStockage().length > 0);
      if (res)
        break;
    }
    return res;
  }
  @Override
  public IBief getBiefContenantProfilNumero(int numeroProfil) {
    for (int i= 0; i < biefs_.length; i++) {
      if (biefs_[i].contientProfilNumero(numeroProfil))
        return biefs_[i];
    }
    return null;
  }
  @Override
  public IBief getBiefContenantProfilAbscisse(double abscisseProfil) {
    for (int i= 0; i < biefs_.length; i++) {
      if (biefs_[i].contientProfilAbscisse(abscisseProfil))
        return biefs_[i];
    }
    return null;
  }
  @Override
  public IBief getBiefContenantAbscisse(double abscisse) {
    for (int i= 0; i < biefs_.length; i++) {
      if (biefs_[i].contientAbscisse(abscisse))
        return biefs_[i];
    }
    return null;
  }
  @Override
  public IBief getBiefContenantSingularite(ISingularite sing) {
    for (int i= 0; i < biefs_.length; i++) {
      if (biefs_[i].contientSingularite(sing))
        return biefs_[i];
    }
    return null;
  }
  @Override
  public IZoneFrottement getFrottementContenantProfilNumero(int numeroProfil) {
    IBief bief= getBiefContenantProfilNumero(numeroProfil);
    if (bief == null)
      return null;
    IZoneFrottement[] zones= bief.zonesFrottement();
    for (int i= 0; i < zones.length; i++) {
      if (zones[i]
        .contientAbscisse(
          bief.profils()[bief.getIndiceProfilNumero(numeroProfil)].abscisse()))
        return zones[i];
    }
    return null;
  }
  @Override
  public int nbProfils() {
    int nb= 0;
    for (int i= 0; i < biefs_.length; i++) {
      nb += biefs_[i].profils().length;
    }
    return nb;
  }
  @Override
  public ISeuil[] seuils() {
    ISeuil[] s= new ISeuil[0];
    for (int i= 0; i < biefs_.length; i++) {
      ISeuil[] sb= biefs_[i].seuils();
      ISeuil[] tmp= s;
      s= new ISeuil[tmp.length + sb.length];
      for (int j= 0; j < tmp.length; j++)
        s[j]= tmp[j];
      for (int j= 0; j < sb.length; j++)
        s[tmp.length + j]= sb[j];
    }
    return s;
  }
  @Override
  public IPerteCharge[] pertesCharges() {
    IPerteCharge[] s= new IPerteCharge[0];
    for (int i= 0; i < biefs_.length; i++) {
      IPerteCharge[] sb= biefs_[i].pertesCharges();
      IPerteCharge[] tmp= s;
      s= new IPerteCharge[tmp.length + sb.length];
      for (int j= 0; j < tmp.length; j++)
        s[j]= tmp[j];
      for (int j= 0; j < sb.length; j++)
        s[tmp.length + j]= sb[j];
    }
    return s;
  }
  @Override
  public IApport[] apports() {
    IApport[] s= new IApport[0];
    for (int i= 0; i < biefs_.length; i++) {
      IApport[] sb= biefs_[i].apports();
      IApport[] tmp= s;
      s= new IApport[tmp.length + sb.length];
      for (int j= 0; j < tmp.length; j++)
        s[j]= tmp[j];
      for (int j= 0; j < sb.length; j++)
        s[tmp.length + j]= sb[j];
    }
    return s;
  }
  @Override
  public ISource[] sources() {
  ISource[] s= new ISource[0];
  for (int i= 0; i < biefs_.length; i++) {
    ISource[] sb= biefs_[i].sources();
    ISource[] tmp= s;
    s= new ISource[tmp.length + sb.length];
    for (int j= 0; j < tmp.length; j++)
      s[j]= tmp[j];
    for (int j= 0; j < sb.length; j++)
      s[tmp.length + j]= sb[j];
  }
  return s;
}

  @Override
  public IDeversoir[] deversoirs() {
    IDeversoir[] s= new IDeversoir[0];
    for (int i= 0; i < biefs_.length; i++) {
      IDeversoir[] sb= biefs_[i].deversoirs();
      IDeversoir[] tmp= s;
      s= new IDeversoir[tmp.length + sb.length];
      for (int j= 0; j < tmp.length; j++)
        s[j]= tmp[j];
      for (int j= 0; j < sb.length; j++)
        s[tmp.length + j]= sb[j];
    }
    return s;
  }
  @Override
  public int nbPasTempsResultats() {
    int nb= 0;
    for (int i= 0; i < biefs_.length; i++) {
      if (biefs_[i].resultatsBief() != null)
        nb= Math.max(nb, biefs_[i].resultatsBief().pasTemps().length);
    }
    return nb;
  }
  @Override
  public IBief getBiefNumero(int numeroBief) {
    for (int i= 0; i < biefs_.length; i++) {
      if (biefs_[i].numero() == numeroBief)
        return biefs_[i];
    }
    return null;
  }

  /**
   * Retoune l'estimation du nombre de planim�trage.
   * @return Le max de l'estimation de chaque bief et ne pouvant d�passer 250
   */
  @Override
  public int getNbPasPlanimetrage() {
    int nbPas= 0;
    for (int i= 0; i < biefs_.length; i++) {
      nbPas= Math.max(nbPas, biefs_[i].getNbPasPlanimetrage());
    }
    return Math.min(nbPas, 250);
  }
  @Override
  public ISingularite[] getSingularitesHorsBief() {
    ISingularite[] sings= singularites();
    Vector resV= new Vector();
    for (int i= 0; i < sings.length; i++) {
      IBief bief= getBiefContenantAbscisse(sings[i].abscisse());
      if (bief == null) {
        System.err.println("Singularit� " + sings[i].numero() + " hors-bief");
        resV.add(sings[i]);
      }
    }
    ISingularite[] res= new ISingularite[resV.size()];
    for (int i= 0; i < res.length; i++) {
      res[i]= (ISingularite)resV.get(i);
    }
    return res;
  }
  @Override
  public double interpoleFond(double abscisse) {
    IBief bief= getBiefContenantAbscisse(abscisse);
    return bief.interpoleFond(abscisse);
  }

  /**
   * @todo
   * @param numeroBief int
   * @param abscisseRelative double
   * @param distanceEntreBief double
   * @return double
   */
  @Override
  public double getAbscisseAbsolue (int numeroBief, double abscisseRelative, double distanceEntreBief) {
    System.out.println("numeroBief="+numeroBief);
    System.out.println("abscisseRelative="+abscisseRelative);
    int indice = numeroBief -1;
    double abscisseAbsolueDebut = 0;
    double abscisseRelativeDebut = Double.NaN;
    for (int i = 0; i < biefs_.length; i++) {
      if (indice == biefs_[i].indice()) {
        abscisseRelativeDebut = biefs_[i].getXOrigine();
        break;
      }
      abscisseAbsolueDebut = abscisseAbsolueDebut + biefs_[i].getLongueur()+ distanceEntreBief;
    }
    System.out.println("abscisseAbsolueDebut="+abscisseAbsolueDebut);
    System.out.println("abscisseRelativeDebut="+abscisseRelativeDebut);
    return abscisseRelative-abscisseRelativeDebut+abscisseAbsolueDebut;
  }
  /**
   * Retourne l'abscisse relative � partir de l'abscisse absolue.
   * @param numeroBief int
   * @param abscisseAbsolue double
   * @param abscisseAbsolueDebutBief double
   * @return double
   */
  @Override
  public double getAbscisseRelative (int numeroBief, double abscisseAbsolue, double abscisseAbsolueDebutBief) {
    int indice = numeroBief -1;
    double abscisseRelativeDebut = Double.NaN;
    for (int i = 0; i < biefs_.length; i++) {
      if (indice == biefs_[i].indice()) {
        abscisseRelativeDebut = biefs_[i].getXOrigine();
      }
    }
    return abscisseAbsolue-abscisseAbsolueDebutBief+abscisseRelativeDebut;
  }
  private void trieBiefs() {
    if (biefs_ == null)
      return;
    Arrays.sort(biefs_, DBief.COMPARATOR);
  }
  private void afficheBiefs() {
    if (biefs_ == null) {
      System.out.println("biefs_ == null");
      return;
    }
    for (int i = 0; i < biefs_.length; i++) {
      IBief b = biefs_[i];
      System.out.println("biefs_["+i+"]= indice="+b.indice()+" num="+b.numero());

    }

  }
  private final static boolean isContient(Object[] objets, Object objet) {
    for (int i = 0; i < objets.length; i++) {
      if (objets[i] == objet) {
        return true;
      }
    }
    return false;
  }

}
