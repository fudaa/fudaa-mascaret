/**
 * @file         DResultatsBief.java
 * @creation     2000-08-10
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsBief;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsBiefHelper;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsBiefOperations;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsBiefPasTemps;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsBiefPasTempsHelper;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "résultats du bief".
 * Pas utilisé dans Fudaa-Mascaret.
 * @version      $Id: DResultatsBief.java,v 1.9 2005-06-29 18:07:57 jm_lacombe Exp $
 * @author       Axel von Arnim
 */
public class DResultatsBief
  extends DHydraulique1d
  implements IResultatsBief,IResultatsBiefOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IResultatsBief) {
      IResultatsBief q= IResultatsBiefHelper.narrow(_o);
      if (q.pasTemps() != null) {
        IResultatsBiefPasTemps[] ip=
          new IResultatsBiefPasTemps[q.pasTemps().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= IResultatsBiefPasTempsHelper.narrow(q.pasTemps()[i].creeClone());
        pasTemps(ip);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IResultatsBief p= UsineLib.findUsine().creeHydraulique1dResultatsBief();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "resultatsBief";
    return s;
  }
  /*** IResultatsBief ***/
  // constructeurs
  public DResultatsBief() {
    super();
    pasTemps_= new IResultatsBiefPasTemps[0];
  }
  @Override
  public void dispose() {
    pasTemps_= null;
    super.dispose();
  }
  // Attributs
  private IResultatsBiefPasTemps[] pasTemps_;
  @Override
  public IResultatsBiefPasTemps[] pasTemps() {
    return pasTemps_;
  }
  @Override
  public void pasTemps(IResultatsBiefPasTemps[] s) {
    if (egale(pasTemps_, s)) return;
    pasTemps_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pasTemps");
  }
}
