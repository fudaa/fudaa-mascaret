/**
 * @file         DResultatsGeneraux.java
 * @creation     2000-08-10
 * @modification $Date: 2006-10-04 15:16:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */

package org.fudaa.dodico.hydraulique1d;

import org.fudaa.dodico.corba.hydraulique1d.IParametresReprise;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsGeneraux;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsGenerauxHelper;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsGenerauxOperations;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatial;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Impl�mentation de l'objet m�tier "r�sultats g�n�raux" de l'�tude.
 * Contient tous les r�sultats d'une �tude.
 * Les fichiers listing et messages sont stock�s dans des tableaux d'octets.
 * Les r�sultats Rubens sont �galement stock�s dans des tableaux d'octets, ils existent
 * encore pour des raisons de compatibilit� anscendante lors de la d�s�rialisation
 * de vieux fichiers '.masc'. Maintenant, quelquesoit le format de fichier d'origine,
 * on stocke les r�sultats dans des objets IResultatsTemporelSpatial.
 *
 * @version      $Revision: 1.19 $ $Date: 2006-10-04 15:16:30 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */

public class DResultatsGeneraux
  extends DHydraulique1d
  implements IResultatsGeneraux,IResultatsGenerauxOperations {

  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IResultatsGeneraux) {
      IResultatsGeneraux q= IResultatsGenerauxHelper.narrow(_o);
      listing(q.listing());
      resultatReprise(q.resultatReprise());
      listingDamocles(q.listingDamocles());
      listingCasier(q.listingCasier());
      listingLiaison(q.listingLiaison());
      listingTracer(q.listingTracer());
      messagesEcran(q.messagesEcran());
      messagesEcranErreur(q.messagesEcranErreur());
      resultatsRubens(q.resultatsRubens());
      resultatsRubensCasier(q.resultatsRubensCasier());
      resultatsRubensLiaison(q.resultatsRubensLiaison());
      resultatsTemporelSpatial(q.resultatsTemporelSpatial());
      resultatsTemporelCasier(q.resultatsTemporelCasier());
      resultatsTemporelLiaison(q.resultatsTemporelLiaison());
      resultatsTemporelTracer(q.resultatsTemporelTracer());
      //resultatsRubensTracer(q.resultatsRubensTracer());
    }
  }
  @Override
  final public IObjet creeClone() {
    IResultatsGeneraux p=
      UsineLib.findUsine().creeHydraulique1dResultatsGeneraux();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "resultatsGeneraux";
    return s;
  }
  /*** IResultatsGeneraux ***/
  // constructeurs
  public DResultatsGeneraux() {
    super();
    listing_= null;
    resultatReprise_= UsineLib.findUsine().creeHydraulique1dParametresReprise();
    messagesEcran_= null;
    messagesEcranErreur_= null;
    listingDamocles_= null;
    listingCasier_= null;
    listingLiaison_= null;
    listingTracer_= null;
    resultatsRubens_= null;
    resultatsRubensCasier_= null;
    resultatsRubensLiaison_= null;
    resultatsTemporelSpatial_=
      UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatial();
    resultatsTemporelCasier_=
      UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatial();
    resultatsTemporelLiaison_=
      UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatial();
    resultatsTemporelTracer_=
      UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatial();
    //resultatsRubensTracer_= null;
  }
  @Override
  public void dispose() {
    listing_= null;
    listingCasier_= null;
    listingLiaison_= null;
    listingTracer_= null;
    if (resultatReprise_ != null) {
      resultatReprise_.dispose();
      resultatReprise_= null;
    }
    messagesEcran_= null;
    messagesEcranErreur_= null;
    listingDamocles_= null;
    if (resultatsTemporelSpatial_ != null) {
      resultatsTemporelSpatial_.dispose();
      resultatsTemporelSpatial_= null;
    }
    if (resultatsTemporelCasier_ != null) {
      resultatsTemporelCasier_.dispose();
      resultatsTemporelCasier_= null;
    }
    if (resultatsTemporelLiaison_ != null) {
      resultatsTemporelLiaison_.dispose();
      resultatsTemporelLiaison_= null;
    }
    if (resultatsTemporelTracer_ != null) {
        resultatsTemporelTracer_.dispose();
        resultatsTemporelTracer_ = null;
    }
    resultatsRubens_= null;
    resultatsRubensCasier_= null;
    resultatsRubensLiaison_= null;
    //resultatsRubensTracer_=null;

    super.dispose();
  }
  // Attributs
  private byte[] listingCasier_;
  @Override
  public byte[] listingCasier() {
    return listingCasier_;
  }
  @Override
  public void listingCasier(byte[] s) {
    listingCasier_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "listingCasier");
  }
  private byte[] listingLiaison_;
  @Override
  public byte[] listingLiaison() {
    return listingLiaison_;
  }
  @Override
  public void listingLiaison(byte[] s) {
    listingLiaison_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "listingLiaison");
  }
  private byte[] listingTracer_;
  @Override
  public byte[] listingTracer() {
    return listingTracer_;
  }
  @Override
  public void listingTracer(byte[] s) {
    listingTracer_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "listingTracer");
  }

  private byte[] listing_;
  @Override
  public byte[] listing() {
    return listing_;
  }
  @Override
  public void listing(byte[] s) {
    listing_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "listing");
  }
  private IParametresReprise resultatReprise_;
  @Override
  public IParametresReprise resultatReprise() {
    return resultatReprise_;
  }
  @Override
  public void resultatReprise(IParametresReprise resultatReprise) {
    if (resultatReprise_==resultatReprise) return;
    resultatReprise_= resultatReprise;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultatReprise");
  }
  private byte[] listingDamocles_;
  @Override
  public byte[] listingDamocles() {
    return listingDamocles_;
  }
  @Override
  public void listingDamocles(byte[] s) {
    listingDamocles_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "listingDamocles");
  }
  private byte[] messagesEcran_;
  @Override
  public byte[] messagesEcran() {
    return messagesEcran_;
  }
  @Override
  public void messagesEcran(byte[] s) {
    messagesEcran_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "messagesEcran");
  }
  private byte[] messagesEcranErreur_;
  @Override
  public byte[] messagesEcranErreur() {
    return messagesEcranErreur_;
  }
  @Override
  public void messagesEcranErreur(byte[] s) {
    messagesEcranErreur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "messagesEcranErreur");
  }
  private byte[] resultatsRubens_;
  @Override
  public byte[] resultatsRubens() {
    return resultatsRubens_;
  }
  @Override
  public void resultatsRubens(byte[] s) {
    resultatsRubens_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultatsRubens");
  }
  private byte[] resultatsRubensCasier_;
  @Override
  public byte[] resultatsRubensCasier() {
    return resultatsRubensCasier_;
  }
  @Override
  public void resultatsRubensCasier(byte[] s) {
    resultatsRubensCasier_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "resultatsRubensCasier");
  }
  /*private byte[] resultatsRubensTracer_;
  public byte[] resultatsRubensTracer() {
    return resultatsRubensTracer_;
  }
  public void resultatsRubensTracer(byte[] s) {
    resultatsRubensTracer_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "resultatsRubensTracer");
  }*/
  private byte[] resultatsRubensLiaison_;
  @Override
  public byte[] resultatsRubensLiaison() {
    return resultatsRubensLiaison_;
  }
  @Override
  public void resultatsRubensLiaison(byte[] s) {
    resultatsRubensLiaison_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "resultatsRubensLiaison");
  }
 
  private IResultatsTemporelSpatial resultatsTemporelSpatial_;
  @Override
  public IResultatsTemporelSpatial resultatsTemporelSpatial() {
    return resultatsTemporelSpatial_;
  }
  @Override
  public void resultatsTemporelSpatial(IResultatsTemporelSpatial r) {
    if (resultatsTemporelSpatial_==r) return;
    resultatsTemporelSpatial_= r;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "resultatsTemporelSpatial");
  }
  private IResultatsTemporelSpatial resultatsTemporelCasier_;
  @Override
  public IResultatsTemporelSpatial resultatsTemporelCasier() {
    if (resultatsTemporelCasier_ != null) {
      resultatsTemporelCasier_.resultatsCasier(true);
      return resultatsTemporelCasier_;
    } else {
      return null;
    }
  }
  @Override
  public void resultatsTemporelCasier(IResultatsTemporelSpatial r) {
    if (resultatsTemporelCasier_==r) return;
    resultatsTemporelCasier_= r;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "resultatsTemporelCasier");
  }
  private IResultatsTemporelSpatial resultatsTemporelLiaison_;
  @Override
  public IResultatsTemporelSpatial resultatsTemporelLiaison() {
    if (resultatsTemporelLiaison_ != null) {
      resultatsTemporelLiaison_.resultatsLiaison(true);
      return resultatsTemporelLiaison_;
    } else {
      return null;
    }
  }
  @Override
  public void resultatsTemporelLiaison(IResultatsTemporelSpatial r) {
    if (resultatsTemporelLiaison_==r) return;
    resultatsTemporelLiaison_= r;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "resultatsTemporelLiaison");
  }

  private IResultatsTemporelSpatial resultatsTemporelTracer_;
  @Override
  public IResultatsTemporelSpatial resultatsTemporelTracer() {
      if (resultatsTemporelTracer_ != null) {
          resultatsTemporelTracer_.resultatsTracer(true);
          return resultatsTemporelTracer_;
      } else {
          return null;
      }
  }

  @Override
  public void resultatsTemporelTracer(IResultatsTemporelSpatial r) {
      if (resultatsTemporelTracer_ == r)
          return;
      resultatsTemporelTracer_ = r;
      UsineLib.findUsine().fireObjetModifie(
              toString(),
              tie(),
              "resultatsTemporelTracer");
  }


}
