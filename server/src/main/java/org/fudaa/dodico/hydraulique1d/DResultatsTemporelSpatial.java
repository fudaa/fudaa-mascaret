/**
 * @file         DResultatsTemporelSpatial.java
 * @creation     2001-10-01
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.corba.hydraulique1d.IDescriptionVariable;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatial;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatialBief;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatialBiefHelper;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatialHelper;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatialOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation des objets m�tiers "r�sultats temporels/iteratifs et spatial" des r�sultats g�n�raux.
 * Contients la descriptions des variables, les valeurs des diff�rents pas, les valeurs
 * des variables par biefs et des indicateurs pr�cisant si c'est un r�sultat casier, liaison ou biefs.
 * @version      $Revision: 1.17 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DResultatsTemporelSpatial
  extends DHydraulique1d
  implements IResultatsTemporelSpatial,IResultatsTemporelSpatialOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IResultatsTemporelSpatial) {
      IResultatsTemporelSpatial q= IResultatsTemporelSpatialHelper.narrow(_o);
      if (q.resultatsBiefs() != null) {
        IResultatsTemporelSpatialBief[] ib=
          new IResultatsTemporelSpatialBief[q.resultatsBiefs().length];
        for (int i= 0; i < ib.length; i++)
          ib[i]=
            IResultatsTemporelSpatialBiefHelper.narrow(q.resultatsBiefs()[i].creeClone());
        resultatsBiefs(ib);
      }
      if (q.valPas() != null) {
        valPas((double[])q.valPas().clone());
      }
      if (q.descriptionVariables() != null) {
        IDescriptionVariable[] ip=
          new IDescriptionVariable[q.descriptionVariables().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (IDescriptionVariable)q.descriptionVariables()[i].creeClone();
        descriptionVariables(ip);
      }
      resultatsCasier(q.resultatsCasier());
      resultatsLiaison(q.resultatsLiaison());
      resultatsPermanent(q.resultatsPermanent());
      resultatsTracer(q.resultatsTracer());
      resultatsCalageAuto(q.resultatsCalageAuto());
      setTemporel(q.isTemporel());
    }
  }
  @Override
  final public IObjet creeClone() {
    IResultatsTemporelSpatial p=
      UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatial();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "resultatsTemporelSpatial";
    return s;
  }
  /*** IResultatsGeneraux ***/
  // constructeurs
  public DResultatsTemporelSpatial() {
    super();
    pasTemps_= new double[0];
    resultatsBiefs_= new IResultatsTemporelSpatialBief[0];
    descriptionVariables_= new IDescriptionVariable[0];
    resultatsCasier_ = false;
    resultatsLiaison_ = false;
    resultatsPermanent_ = false;
    resultatsTracer_ = false;
    resultatsCalageAuto_=false;
    btemporel_=true;
  }
  @Override
  public void dispose() {
    pasTemps_= null;
    if (resultatsBiefs_ != null) {
      for (int i= 0; i < resultatsBiefs_.length; i++) {
        resultatsBiefs_[i].dispose();
      }
      resultatsBiefs_= null;
    }
    if (descriptionVariables_ != null) {
      for (int i= 0; i < descriptionVariables_.length; i++) {
        descriptionVariables_[i].dispose();
      }
      descriptionVariables_= null;
    }
    super.dispose();
  }

  // Attributs
  private IDescriptionVariable[] descriptionVariables_;
  @Override
  public IDescriptionVariable[] descriptionVariables() {
    return descriptionVariables_;
  }
  @Override
  public void descriptionVariables(IDescriptionVariable[] s) {
    if (egale(descriptionVariables_, s)) return;
    descriptionVariables_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "descriptionVariables");
  }
  private IResultatsTemporelSpatialBief[] resultatsBiefs_;
  @Override
  public IResultatsTemporelSpatialBief[] resultatsBiefs() {
    return resultatsBiefs_;
  }
  @Override
  public void resultatsBiefs(IResultatsTemporelSpatialBief[] resultatsBiefs) {
    if (egale(resultatsBiefs_, resultatsBiefs)) return;
    resultatsBiefs_= resultatsBiefs;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultatsBiefs");
  }
  private double[] pasTemps_;

  /**
   *  @deprecated Remplac� par valPas()
   */
  @Override
  public double[] pasTemps() {
    return valPas();
  }

  /**
   * @deprecated Remplac� par valPas(double[] _pas)
   */
  @Override
  public void pasTemps(double[] _pas) {
    valPas(_pas);
  }

  private boolean resultatsLiaison_;
  @Override
  public boolean resultatsLiaison() {
    return resultatsLiaison_;
  }
  @Override
  public void resultatsLiaison(boolean resultatsLiaison) {
    if (resultatsLiaison_ == resultatsLiaison) return;
    resultatsLiaison_ = resultatsLiaison;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultatsLiaison");
  }

  private boolean resultatsCasier_;
  @Override
  public boolean resultatsCasier() {
    return resultatsCasier_;
  }
  @Override
  public void resultatsCasier(boolean resultatsCasier) {
    if (resultatsCasier_ == resultatsCasier) return;
    resultatsCasier_ = resultatsCasier;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultatsCasier");
  }

  private boolean resultatsPermanent_;
  @Override
  public boolean resultatsPermanent() {
    return resultatsPermanent_;
  }
  @Override
  public void resultatsPermanent(boolean resultatsPermanent) {
    if (resultatsPermanent_ == resultatsPermanent) return;
    resultatsPermanent_ = resultatsPermanent;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultatsPermanent");
  }


  private boolean resultatsTracer_;
  @Override
  public boolean resultatsTracer() {
    return resultatsTracer_;
  }
  @Override
  public void resultatsTracer(boolean resultatsTracer) {
    if (resultatsTracer_ == resultatsTracer) return;
    resultatsTracer_ = resultatsTracer;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultatsTracer");
  }


  private boolean resultatsCalageAuto_;
  @Override
  public boolean resultatsCalageAuto() {
    return resultatsCalageAuto_;
  }
  @Override
  public void resultatsCalageAuto(boolean resultatsCalageAuto) {
    if (resultatsCalageAuto_ == resultatsCalageAuto) return;
    resultatsCalageAuto_ = resultatsCalageAuto;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultatsCalageAuto");
  }



  /**
   * Retourne les valeurs des pas. Les valeurs des pas peuvent �tre des n� d'it�ration ou des
   * valeurs de pas de temps.
   * @return Les valeurs de pas.
   */
  @Override
  public double[] valPas() {
    return pasTemps_;
  }

  /**
   * Affecte les valeurs des pas. Les valeurs des pas peuvent �tre des n� d'it�ration ou des
   * valeurs de pas de temps.
   * @param _pas Les valeurs de pas.
   */
  @Override
  public void valPas(double[] _pas) {
    if (Arrays.equals(pasTemps_, _pas)) return;
    pasTemps_= _pas;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "valPas");
  }

  private boolean btemporel_;
  /**
   * Retourne la nature des pas (Temporels ou it�ratifs).
   * @return true : Temporels, false : It�ratifs
   */
  @Override
  public boolean isTemporel() {
    return btemporel_;
  }

  /**
   * D�fini la nature des pas (Temporels ou it�ratifs).
   * @param _b true : Temporels, false : It�ratifs
   */
  @Override
  public void setTemporel(boolean _b) {
    btemporel_=_b;
  }

  /**
   * end l'indice de colonne correspondant � ce nom de variable.
   * @param _c  : nom de la variable
   */
  @Override
  public int getIndiceVariable(String _c) {
     for (int i = 0; i < descriptionVariables_.length; i++) {
         if (descriptionVariables_[i].nom().equals(_c)) return i;
             }
             return -1;
         }


  
  /**
  * Fusionne 2 IResultatsTemporelSpatial.
  * @param IResultatsTemporelSpatial a et b
  */
  static public IResultatsTemporelSpatial fusionnerIResultatsTemporelSpatial(IResultatsTemporelSpatial a, IResultatsTemporelSpatial b) throws Exception {

        if ((a==null) && (b==null)){
            return null;
        }else if (a.descriptionVariables()==null){
            return b;
        }else if (b.descriptionVariables()==null){
            return a;
        }

        IResultatsTemporelSpatial ires = UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatial();

        //descriptionVariables
        int descriptionVariablesLenghtA = a.descriptionVariables().length;
        int descriptionVariablesLenghtB = b.descriptionVariables().length;
        int descriptionVariablesLenght = descriptionVariablesLenghtA+descriptionVariablesLenghtB;
        IDescriptionVariable[] descrVar = new IDescriptionVariable[descriptionVariablesLenght];
        for (int i = 0; i < descriptionVariablesLenghtA; i++) {
            descrVar[i]=a.descriptionVariables()[i];
        }
        for (int i = 0; i < descriptionVariablesLenghtB; i++) {
            descrVar[descriptionVariablesLenghtA+i]=b.descriptionVariables()[i];
        }
        ires.descriptionVariables(descrVar);

        //pasTemps
        if (!Arrays.equals(a.pasTemps(),b.pasTemps())) {
            throw new Exception("Les 2 r�sultats n'ont pas les m�mes pas de temps ! Fusion impossible");
        }
        ires.pasTemps(a.pasTemps());

        //valPas
        if (!Arrays.equals(a.valPas(),b.valPas())) {
            throw new Exception(
                    "Les 2 r�sultats n'ont pas les m�mes pas de temps ! Fusion impossible");
        }
        ires.valPas(a.valPas());


        //resultatsBiefs

        if (a.resultatsBiefs().length!=b.resultatsBiefs().length) {
           throw new Exception(
                   "Les 2 r�sultats n'ont pas le m�me nombre de bief ! Fusion impossible");
       }
       IResultatsTemporelSpatialBief[] newResultatsBiefs =new IResultatsTemporelSpatialBief[a.resultatsBiefs().length];
       for (int i = 0; i < a.resultatsBiefs().length; i++) {
           IResultatsTemporelSpatialBief newBief = UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatialBief();
           IResultatsTemporelSpatialBief aBief = a.resultatsBiefs()[i];
           IResultatsTemporelSpatialBief bBief = b.resultatsBiefs()[i];
           
           if (!CtuluLibArray.equals(aBief.abscissesSections(),
                              bBief.abscissesSections(),0.5)) {
               throw new Exception(
                       "Les 2 r�sultats n'ont pas les m�mes  sections de calcul pour le bief  " +
                       (i + 1) + " ! Fusion impossible");
           }
           
           //On compare la precision (taille en String)pour conserver les section les plus precises
           if (CtuluLibArray.precision(aBief.abscissesSections())>CtuluLibArray.precision(bBief.abscissesSections()))
        		   newBief.abscissesSections(aBief.abscissesSections());
           else    newBief.abscissesSections(bBief.abscissesSections());
           double[][][] aBiefTab = aBief.valeursVariables();
           double[][][] bBiefTab = bBief.valeursVariables();
           double[][][] newBiefTab = new double[aBiefTab.length+bBiefTab.length][aBiefTab[0].length][aBiefTab[0][0].length];
               for (int tps = 0; tps < aBiefTab[0].length; tps++) {
                   for (int absc = 0; absc < aBiefTab[0][0].length; absc++) {
                       for (int var = 0; var < aBiefTab.length; var++) {
                       newBiefTab[var][tps][absc]=aBiefTab[var][tps][absc];
                   }
                   for (int var = 0; var < bBiefTab.length; var++) {
                       newBiefTab[aBiefTab.length+var][tps][absc]=bBiefTab[var][tps][absc];
                   }
               }

           }
           newBief.valeursVariables(newBiefTab);
           newResultatsBiefs[i]=newBief;
       }
        ires.resultatsBiefs(newResultatsBiefs);

        if (a.resultatsCasier() || b.resultatsCasier()) ires.resultatsCasier(true);
        else ires.resultatsCasier(false);
        if (a.resultatsLiaison() || b.resultatsLiaison()) ires.resultatsLiaison(true);
        else ires.resultatsLiaison(false);
        if (a.resultatsPermanent() || b.resultatsPermanent()) ires.resultatsPermanent(true);
        else ires.resultatsPermanent(false);
        if (a.resultatsTracer() || b.resultatsTracer()) ires.resultatsTracer(true);
        else ires.resultatsTracer(false);
        if (a.resultatsCalageAuto() || b.resultatsCalageAuto()) ires.resultatsCalageAuto(true);
        else ires.resultatsCalageAuto(false);


         return ires;

  }

}
