/**
 * @file         DResultatsTemporelSpatialBief.java
 * @creation     2001-10-01
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatialBief;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatialBiefHelper;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatialBiefOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation des objets métiers "résultats temporels et spatial d'un bief" des résultats temporels et spatial.
 * Contients les valeurs des différentes variables pour un bief ou un casier ou une liaison suivant l'indicateur
 * de l'objet contenaire IResultatsTemporelSpatial.
 * @version      $Revision: 1.11 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DResultatsTemporelSpatialBief
  extends DHydraulique1d
  implements IResultatsTemporelSpatialBief,IResultatsTemporelSpatialBiefOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IResultatsTemporelSpatialBief) {
      IResultatsTemporelSpatialBief q= IResultatsTemporelSpatialBiefHelper.narrow(_o);
      if (q.abscissesSections() != null) {
        abscissesSections((double[])q.abscissesSections().clone());
      }
      if (q.valeursVariables() != null) {
        valeursVariables((double[][][])q.valeursVariables().clone());
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IResultatsTemporelSpatialBief p=
      UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatialBief();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "resultatsTemporelSpatialBief";
    return s;
  }
  /*** IResultatsGeneraux ***/
  // constructeurs
  public DResultatsTemporelSpatialBief() {
    super();
    abscissesSections_= new double[0];
    valeursVariables_= new double[0][0][0];
  }
  @Override
  public void dispose() {
    abscissesSections_= null;
    valeursVariables_= null;
    super.dispose();
  }
  // Attributs
  private double[] abscissesSections_;
  @Override
  public double[] abscissesSections() {
    return abscissesSections_;
  }
  @Override
  public void abscissesSections(double[] abscissesSections) {
    abscissesSections_= abscissesSections;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "abscissesSections");
  }
  private double[][][] valeursVariables_;

  /**
   * Retourne les valeurs des variables.
   * @return valeursVariables[iVariable][iPasTemps][iSectionCalcul]
   */
  @Override
  public double[][][] valeursVariables() {
    return valeursVariables_;
  }
  /**
   * Initialise les valeurs des variables.
   * @param valeursVariables valeursVariables[iVariable][iPasTemps][iSectionCalcul]
   */
  @Override
  public void valeursVariables(double[][][] valeursVariables) {
    valeursVariables_= valeursVariables;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "valeursVariables");
  }
}
