/**
 * @file         DSectionCalculee.java
 * @creation     2000-08-10
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculee;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculeeOperations;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculeePasTemps;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation des objets m�tiers "sections calcul�es" d'un bief.
 * Pas utilis� dans Fudaa-Mascaret.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DSectionCalculee
  extends DHydraulique1d
  implements ISectionCalculee,ISectionCalculeeOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISectionCalculee) {
      ISectionCalculee q= (ISectionCalculee)_o;
      abscisse(q.abscisse());
      if (q.pasTemps() != null) {
        ISectionCalculeePasTemps[] ip=
          new ISectionCalculeePasTemps[q.pasTemps().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (ISectionCalculeePasTemps)q.pasTemps()[i].creeClone();
        pasTemps(ip);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    ISectionCalculee p= UsineLib.findUsine().creeHydraulique1dSectionCalculee();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionCalculee abs " + abscisse_;
    return s;
  }
  /*** ISectionCalculee ***/
  // constructeurs
  public DSectionCalculee() {
    super();
    abscisse_= 0.;
    pasTemps_= new ISectionCalculeePasTemps[0];
  }
  @Override
  public void dispose() {
    abscisse_= 0.;
    pasTemps_= null;
    super.dispose();
  }
  // Attributs
  private double abscisse_;
  @Override
  public double abscisse() {
    return abscisse_;
  }
  @Override
  public void abscisse(double s) {
    if (abscisse_==s) return;
    abscisse_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");
  }
  private ISectionCalculeePasTemps[] pasTemps_;
  @Override
  public ISectionCalculeePasTemps[] pasTemps() {
    return pasTemps_;
  }
  @Override
  public void pasTemps(ISectionCalculeePasTemps[] s) {
    if (egale(pasTemps_,s)) return;
    pasTemps_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pasTemps");
  }
}
