/**
 * @file         DSectionCalculeePasTemps.java
 * @creation     2000-08-10
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Arrays;

import org.fudaa.dodico.corba.hydraulique1d.IInformationTemps;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculeePasTemps;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculeePasTempsOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation des objets m�tiers "sections calcul�es des pas de temps"
 * des sections calcul�es d'un bief.
 * Pas utilis� dans Fudaa-Mascaret.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DSectionCalculeePasTemps
  extends DHydraulique1d
  implements ISectionCalculeePasTemps,ISectionCalculeePasTempsOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISectionCalculeePasTemps) {
      ISectionCalculeePasTemps q= (ISectionCalculeePasTemps)_o;
      if (q.infoTemps() != null)
        infoTemps((IInformationTemps)q.infoTemps().creeClone());
      if (q.valeurs() != null)
        valeurs((double[])q.valeurs().clone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISectionCalculeePasTemps p=
      UsineLib.findUsine().creeHydraulique1dSectionCalculeePasTemps();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionCalculee ";
    if (infoTemps_ != null)
      s += "pas " + infoTemps_.toString();
    else
      s += "?";
    return s;
  }
  /*** ISectionCalculeePasTemps ***/
  // constructeurs
  public DSectionCalculeePasTemps() {
    super();
    infoTemps_= null;
    valeurs_= new double[0];
  }
  @Override
  public void dispose() {
    infoTemps_= null;
    valeurs_= null;
    super.dispose();
  }
  // Attributs
  private IInformationTemps infoTemps_;
  @Override
  public IInformationTemps infoTemps() {
    return infoTemps_;
  }
  @Override
  public void infoTemps(IInformationTemps s) {
    if (infoTemps_==s) return;
    infoTemps_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "infoTemps");
  }
  private double[] valeurs_;
  @Override
  public double[] valeurs() {
    return valeurs_;
  }
  @Override
  public void valeurs(double[] s) {
    if (Arrays.equals(valeurs_, s)) return;
    valeurs_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "valeurs");
  }
}
