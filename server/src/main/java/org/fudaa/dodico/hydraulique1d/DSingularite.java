/**
 * @file         DSingularite.java
 * @creation     2000-08-10
 * @modification $Date: 2006-06-14 12:53:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.ISingularite;
import org.fudaa.dodico.corba.hydraulique1d.ISingulariteOperations;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IApport;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IBarrage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirComportementLoi;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirComportementZCoefQ;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IPerteCharge;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilDenoye;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilGeometrique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLimniAmont;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLoi;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilNoye;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAmont;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAval;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilVanne;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation des objets m�tiers "singularit�s" des biefs ou du r�seau hydraulique.
 * Abstraction de la notion de singularit�.
 * @see org.fudaa.dodico.hydraulique1d.singularite
 * @version      $Revision: 1.11 $ $Date: 2006-06-14 12:53:22 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public abstract class DSingularite
  extends DHydraulique1d
  implements ISingularite,ISingulariteOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ISingularite) {
      ISingularite q= (ISingularite)_o;
      numero(q.numero());
      nom(q.nom());
      abscisse(q.abscisse());
    }
  }
  @Override
  public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "singularit� " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  /*** ISingularite ***/
  // constructeurs
  public DSingularite() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre("singularite");
    numero_=id_;
    nom_= "singularite " + numero_;
    abscisse_= 0.;
  }
  @Override
  public void dispose() {
    id_=0;
    numero_=0;
    nom_= null;
    abscisse_= 0.;
    super.dispose();
  }
  // Attributs
  protected int id_;
  @Override
  public int id() {
    return id_;
  }

  @Override
  public void id(int s) {
    id_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "id");
  }
  protected int numero_;
  @Override
  public int numero() {
    return numero_;
  }
  @Override
  public void numero(int s) {
    if (numero_!= s){

        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numero");
        //Pour les anciennes versions (nom= Seuil 5)
        if (nom_.endsWith(" "+numero_)) nom(nom_.replaceAll(" "+numero_," "+s));
        //Pour les nouvelles versions (nom= Seuil-Singularit� n�5)
        nom(nom_.replaceAll("-Singularit� n�"+numero_,"-Singularit� n�"+s));
        numero_ = s;
    }
  }
  protected String nom_;
  @Override
  public String nom() {
    return nom_;
  }
  @Override
  public void nom(String s) {
    if (nom_.equals(s)) return;
    nom_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nom");
  }
  protected double abscisse_;
  @Override
  public double abscisse() {
    return abscisse_;
  }
  @Override
  public void abscisse(double s) {
    if (abscisse_==s) return;
    abscisse_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return null;
  }
  public static void supprimeSingularite(ISingularite loi) {
    System.out.println("supprimeSingularite="+loi.getClass());
    if (loi instanceof IApport)
      UsineLib.findUsine().supprimeHydraulique1dApport((IApport)loi);
    else if (loi instanceof IPerteCharge)
      UsineLib.findUsine().supprimeHydraulique1dPerteCharge((IPerteCharge)loi);
    else if (loi instanceof IBarrage)
      UsineLib.findUsine().supprimeHydraulique1dBarrage((IBarrage)loi);
    else if (loi instanceof ISeuilGeometrique)
      UsineLib.findUsine().supprimeHydraulique1dSeuilGeometrique(
        (ISeuilGeometrique)loi);
    else if (loi instanceof ISeuilLoi)
      UsineLib.findUsine().supprimeHydraulique1dSeuilLoi((ISeuilLoi)loi);
    else if (loi instanceof ISeuilLimniAmont)
      UsineLib.findUsine().supprimeHydraulique1dSeuilLimniAmont(
        (ISeuilLimniAmont)loi);
    else if (loi instanceof ISeuilNoye)
      UsineLib.findUsine().supprimeHydraulique1dSeuilNoye((ISeuilNoye)loi);
    else if (loi instanceof ISeuilDenoye)
      UsineLib.findUsine().supprimeHydraulique1dSeuilDenoye((ISeuilDenoye)loi);
    else if (loi instanceof ISeuilTarageAmont)
      UsineLib.findUsine().supprimeHydraulique1dSeuilTarageAmont(
        (ISeuilTarageAmont)loi);
    else if (loi instanceof ISeuilTarageAval)
      UsineLib.findUsine().supprimeHydraulique1dSeuilTarageAval(
        (ISeuilTarageAval)loi);
    else if (loi instanceof ISeuilVanne)
      UsineLib.findUsine().supprimeHydraulique1dSeuilVanne((ISeuilVanne)loi);
    else if (loi instanceof IDeversoirComportementLoi)
      UsineLib.findUsine().supprimeHydraulique1dDeversoirComportementLoi(
        (IDeversoirComportementLoi)loi);
    else if (loi instanceof IDeversoirComportementZCoefQ)
      UsineLib.findUsine().supprimeHydraulique1dDeversoirComportementZCoefQ(
        (IDeversoirComportementZCoefQ)loi);
    else {
      try {
        throw new RuntimeException(
          "DSingularite: aucun destructeur pour " + loi.getClass().getName());
      } catch (RuntimeException e) {
        e.printStackTrace();
      }
    }
  }
}
