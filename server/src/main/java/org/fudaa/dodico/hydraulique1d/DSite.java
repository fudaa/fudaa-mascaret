/**
 * @file         DSite.java
 * @creation     2000-08-09
 * @modification $Date: 2006-04-10 15:41:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ISite;
import org.fudaa.dodico.corba.hydraulique1d.ISiteOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation des objets métiers "sites" des paramètres de stockage ou
 * des laisses de crue ou du barrage principal ou des définitions de sections.
 * Associe une abscisse et une référence vers un bief.
 * @version      $Revision: 1.10 $ $Date: 2006-04-10 15:41:25 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DSite extends DHydraulique1d implements ISite,ISiteOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ISite) {
      ISite q= (ISite)_o;
      abscisse(q.abscisse());
      if (q.biefRattache() != null)
        biefRattache(q.biefRattache());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISite p= UsineLib.findUsine().creeHydraulique1dSite();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    String s= "site (b";
    if (biefRattache_ != null)
      s += (biefRattache_.indice()+1);
    else
      s += "?";
    s += "," + abscisse_ + ")";
    return s;
  }
  /*** ISite ***/
  // constructeurs
  public DSite() {
    super();
    abscisse_= 0.;
    biefRattache_= null;
  }
  @Override
  public void dispose() {
    abscisse_= 0.;
    biefRattache_= null;
    super.dispose();
  }
  // attributs
  private double abscisse_;
  @Override
  public double abscisse() {
    return abscisse_;
  }
  @Override
  public void abscisse(double t) {
    if (abscisse_==t) return;
    abscisse_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");
  }
  private IBief biefRattache_;
  @Override
  public IBief biefRattache() {
    return biefRattache_;
  }
  @Override
  public void biefRattache(IBief t) {
    if (biefRattache_==t) return;
    biefRattache_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "biefRattache");
  }
}
