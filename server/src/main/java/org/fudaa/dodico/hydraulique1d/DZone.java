/**
 * @file         DZone.java
 * @creation     2000-08-09
 * @modification $Date: 2006-04-10 15:41:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Comparator;

import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.IZone;
import org.fudaa.dodico.corba.hydraulique1d.IZoneOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation des objets métiers "zones" des conditions initiales ou
 * des définitions de sections.
 * Associe une abscisse et une référence vers un bief.
 * @version      $Revision: 1.9 $ $Date: 2006-04-10 15:41:25 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class DZone extends DHydraulique1d implements IZone,IZoneOperations {
  public final static ZoneComparator COMPARATOR= new ZoneComparator();
  /*** IObjet ***/
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IZone) {
      IZone q= (IZone)_o;
      abscisseDebut(q.abscisseDebut());
      abscisseFin(q.abscisseFin());
      if (q.biefRattache() != null)
        biefRattache((IBief)q.biefRattache().creeClone());
    }
  }
  @Override
  public IObjet creeClone() {
    IZone p= UsineLib.findUsine().creeHydraulique1dZone();
    p.initialise(tie());
    return p;
  }
  @Override
  public String toString() {
    String s= "zone (b";
    if (biefRattache_ != null)
      s += (biefRattache_.indice()+1);
    else
      s += "?";
    s += "," + abscisseDebut_ + "-" + abscisseFin_ + ")";
    return s;
  }
  /*** IZone ***/
  // constructeurs
  public DZone() {
    super();
    abscisseDebut_= 0.;
    abscisseFin_= 0.;
    biefRattache_= null;
  }
  // destructeur
  @Override
  public void dispose() {
    abscisseDebut_= 0.;
    abscisseFin_= 0.;
    biefRattache_= null;
    super.dispose();
  }
  // attributs
  private double abscisseDebut_;
  @Override
  public double abscisseDebut() {
    return abscisseDebut_;
  }
  @Override
  public void abscisseDebut(double t) {
    if (abscisseDebut_==t) return;
    abscisseDebut_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisseDebut");
  }
  private double abscisseFin_;
  @Override
  public double abscisseFin() {
    return abscisseFin_;
  }
  @Override
  public void abscisseFin(double t) {
    if (abscisseFin_==t) return;
    abscisseFin_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisseFin");
  }
  private IBief biefRattache_;
  @Override
  public IBief biefRattache() {
    return biefRattache_;
  }
  @Override
  public void biefRattache(IBief t) {
    if (biefRattache_==t) return;
    biefRattache_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "biefRattache");
  }
  // methodes
  @Override
  public boolean contientAbscisse(double abscisse) {
    return CGlobal.appartient(abscisse, abscisseDebut_, abscisseFin_);
  }
  @Override
  public boolean appartientA(IZone[] zones) {
    for (int i= 0; i < zones.length; i++) {
      if (zones[i] == this)
        return true;
    }
    return false;
  }
}
class ZoneComparator implements Comparator {
  ZoneComparator() {}
  @Override
  public int compare(Object o1, Object o2) {
    double xo1= ((IZone)o1).abscisseDebut();
    double xo2= ((IZone)o2).abscisseDebut();
    return xo1 < xo2 ? -1 : xo1 == xo2 ? 0 : 1;
  }
  @Override
  public boolean equals(Object obj) {
    return false;
  }
}
