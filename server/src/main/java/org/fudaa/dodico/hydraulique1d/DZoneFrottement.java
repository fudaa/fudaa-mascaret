/**
 * @file         DZoneFrottement.java
 * @creation     2000-08-09
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IZoneFrottement;
import org.fudaa.dodico.corba.hydraulique1d.IZoneFrottementOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;

/**
*  Impl�mentation des objets m�tiers "zones de frottement".
 * Ajoute des coefficients Majeur et Mineur � une Zone.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */

public class DZoneFrottement
  extends DZone
  implements IZoneFrottement,IZoneFrottementOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IZoneFrottement) {
      IZoneFrottement q= (IZoneFrottement)_o;
      super.initialise(q);
      coefMajeur(q.coefMajeur());
      coefMineur(q.coefMineur());
    }
  }
  @Override
  final public IObjet creeClone() {
    IZoneFrottement p= UsineLib.findUsine().creeHydraulique1dZoneFrottement();
    p.initialise(tie());
    return p;
  }
  /*** IZoneFrottement ***/
  // constructeurs
  public DZoneFrottement() {
    super();
    coefMajeur_= 30.;
    coefMineur_= 40.;
  }
  @Override
  public void dispose() {
    coefMajeur_= 0.;
    coefMineur_= 0.;
    super.dispose();
  }
  // attributs
  private double coefMajeur_;
  @Override
  public double coefMajeur() {
    return coefMajeur_;
  }
  @Override
  public void coefMajeur(double t) {
    if (coefMajeur_==t) return;
    coefMajeur_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefMajeur");
  }
  private double coefMineur_;
  @Override
  public double coefMineur() {
    return coefMineur_;
  }
  @Override
  public void coefMineur(double t) {
    if (coefMineur_==t) return;
    coefMineur_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefMineur");
  }
}
