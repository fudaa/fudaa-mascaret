/*
 * @file         DZonePlanimetrage.java
 * @creation     2000-12-07
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IZonePlanimetrage;
import org.fudaa.dodico.corba.hydraulique1d.IZonePlanimetrageOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation des objets m�tiers "zones de planim�trage".
 * Ajoute une taille de Pas � une Zone.
 * @version      $Revision: 1.8 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class DZonePlanimetrage
  extends DZone
  implements IZonePlanimetrage,IZonePlanimetrageOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof IZonePlanimetrage) {
      IZonePlanimetrage q= (IZonePlanimetrage)_o;
      super.initialise(q);
      taillePas(q.taillePas());
    }
  }
  @Override
  final public IObjet creeClone() {
    IZonePlanimetrage p=
      UsineLib.findUsine().creeHydraulique1dZonePlanimetrage();
    p.initialise(tie());
    return p;
  }
  /*** IZonePlanimetrage ***/
  // constructeurs
  public DZonePlanimetrage() {
    super();
    taillePas_= 1.;
  }
  @Override
  public void dispose() {
    taillePas_= 0.;
    super.dispose();
  }
  // attributs
  private double taillePas_;
  @Override
  public double taillePas() {
    return taillePas_;
  }
  @Override
  public void taillePas(double t) {
    if (taillePas_==t) return;
    taillePas_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "taillePas");
  }
}
