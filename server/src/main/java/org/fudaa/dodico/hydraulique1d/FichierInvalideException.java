/*
 * @file         FichierInvalideException.java
 * @creation     2004-02-23
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
/**
 * Impl�mentation de l'exception "FichierInvalideException" g�n�r�e lorsqu'un probl�me est rencontr� en acc�dant � un fichier.
 * @version      $Revision: 1.2 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public class FichierInvalideException extends Exception {
  private int numeroLigne_=-1;
  private String ligne_=null;
  public FichierInvalideException() {
    this(-1, null);
  }
  public FichierInvalideException(int numeroLigne) {
    this(numeroLigne, null);
  }
  public FichierInvalideException(String ligne) {
    super();
    ligne_ = ligne;
  }
  public FichierInvalideException(String ligne, String message) {
    super(message);
    ligne_ = ligne;
  }
  public FichierInvalideException(int numeroLigne, String ligne, String message) {
    super(message);
    numeroLigne_ = numeroLigne;
    ligne_ = ligne;
  }
  public FichierInvalideException(int numeroLigne, String ligne) {
    super();
    numeroLigne_ = numeroLigne;
    ligne_ = ligne;
  }
  public String getLigne() {
    return ligne_;
  }
  public int getNumeroLigne() {
    return numeroLigne_;
  }
}
