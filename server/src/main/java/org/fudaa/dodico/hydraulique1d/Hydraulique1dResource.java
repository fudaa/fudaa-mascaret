package org.fudaa.dodico.hydraulique1d;

/*
 * @file         Hydraulique1dResource.java
 * @creation     2011-02-28
 * @modification $Date: $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2011 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */

import org.fudaa.dodico.commun.DodicoResource;
import com.memoire.bu.BuResource;

/**
 * Classe fille de DodicoResource.
 * Elle cr�e entre autre un champ ��static�� ��HYDRAULIQUE1D�� de sa propre instance.
 * En pratique, elle est utilis�e pour la traduction en anglais.
 * @version      $Revision: $ $Date:  $ by $Author:  $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dResource extends DodicoResource {
 public final static Hydraulique1dResource HYDRAULIQUE1D = new Hydraulique1dResource(DodicoResource.DODICO);

 protected Hydraulique1dResource(final BuResource _parent) {
   super(_parent);
 }
}
