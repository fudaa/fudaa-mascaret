/*
 * @file         Identifieur.java
 * @creation     27/00/00
 * @modification $Date: 2005-06-29 18:07:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d;
import java.util.Hashtable;
/**
 * Implemente un objet "Identifieur" qui permet d'attribuer un num�ro unique aux objets de l'etudes.
 * On utilise ensuite une "hasch table" pour stocker le numero et pour pouvoir cr�er un num�ro diff�rent.
 * @version      $Revision: 1.5 $ $Date: 2005-06-29 18:07:57 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class Identifieur {
  public static final Identifieur IDENTIFIEUR= new Identifieur();
  Hashtable table_;
  // constructeurs
  public Identifieur() {
    table_= new Hashtable();
  }
  // methodes
  public int identificateurLibre(String classe) {
    Object id= table_.get(classe);
    if (id == null)
      id= new Integer(1);
    else
      id= new Integer((((Integer)id).intValue()) + 1);
    table_.put(classe, id);
    return ((Integer)id).intValue();
  }
}
