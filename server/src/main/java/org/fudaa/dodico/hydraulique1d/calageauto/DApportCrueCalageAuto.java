/*
 * @file         DCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.calageauto;

import org.fudaa.dodico.corba.hydraulique1d.calageauto.IApportCrueCalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.IApportCrueCalageAutoOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Implémentation de l'objet apport pour une crue de calage "IApportCrueCalageAuto".
 * @version      $Revision: 1.4 $ $Date: 2006-09-12 08:35:02 $ by $Author: opasteur $
 * @author       Bertrand Marchand
 */
public class DApportCrueCalageAuto extends DHydraulique1d implements IApportCrueCalageAuto,IApportCrueCalageAutoOperations {
  private double abscisse_;
  private double debit_;

  // Constructeur.
  public DApportCrueCalageAuto() {
    super();
    abscisse_=0;
    debit_=0;
  }

  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IApportCrueCalageAuto) {
      IApportCrueCalageAuto q=(IApportCrueCalageAuto)_o;
      abscisse(q.abscisse());
      debit(q.debit());
    }
  }

  @Override
  final public IObjet creeClone() {
    IApportCrueCalageAuto p= UsineLib.findUsine().creeHydraulique1dApportCrueCalageAuto();
    p.initialise(tie());
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Apport";
    res[1]=
      super.getInfos()[1]
        + " abscisse : "
        + abscisse_
        + " debit : "
        + debit_;

    return res;
  }

  @Override
  public void dispose() {
    abscisse_=0;
    debit_=0;
    super.dispose();
  }

  //---  Interface IApportCrueCalageAuto {  ------------------------------------

  @Override
  public double abscisse() {
    return abscisse_;
  }

  @Override
  public void abscisse(double _abscisse) {
    if (abscisse_==_abscisse) return;
    abscisse_=_abscisse;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");
  }

  @Override
  public double debit() {
    return debit_;
  }

  @Override
  public void debit(double _debit) {
    if (debit_==_debit) return;
    debit_=_debit;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "debit");
  }

  //---  } Interface IApportCrueCalageAuto  ------------------------------------
}
