/*
 * @file         DCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.calageauto;

import org.fudaa.dodico.corba.hydraulique1d.IZoneFrottement;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.ICalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.ICalageAutoOperations;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.ICrueCalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.IParametresCalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.IResultatsCalageAuto;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Implémentation de l'objet racine du calage automatique "ICalageAuto".
 * @version      $Revision: 1.4 $ $Date: 2006-09-12 08:35:02 $ by $Author: opasteur $
 * @author       Bertrand Marchand
 */
public class DCalageAuto extends DHydraulique1d implements ICalageAuto,ICalageAutoOperations {
  private IParametresCalageAuto params_;
  private IZoneFrottement[]     zonesFrot_;
  private ICrueCalageAuto[]     crues_;
  private IResultatsCalageAuto  resultats_;

  // Constructeur.
  public DCalageAuto() {
    super();
    params_=UsineLib.findUsine().creeHydraulique1dParametresCalageAuto();
    zonesFrot_=new IZoneFrottement[0];
    crues_=new ICrueCalageAuto[0];
    resultats_=UsineLib.findUsine().creeHydraulique1dResultatsCalageAuto();
  }

  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ICalageAuto) {
      ICalageAuto q= (ICalageAuto)_o;
      parametres(q.parametres());
      zonesFrottement(q.zonesFrottement());
      crues(q.crues());
      resultats(q.resultats());
    }
  }

  @Override
  final public IObjet creeClone() {
    ICalageAuto p=UsineLib.findUsine().creeHydraulique1dCalageAuto();
    p.initialise(tie());
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "CalageAuto";
    res[1]=super.getInfos()[1];

    return res;
  }

  @Override
  public void dispose() {
    if (params_!=null) params_.dispose();
    zonesFrot_=null;
    crues_=null;
    if (resultats_!=null) resultats_.dispose();
    super.dispose();
  }

  //---  Interface ICalageAuto {  ----------------------------------------------

  @Override
  public IParametresCalageAuto parametres() {
    return params_;
  }

  @Override
  public void parametres(IParametresCalageAuto _params) {
    if (params_==_params) return;
    params_=_params;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "params");
  }

  @Override
  public IZoneFrottement[] zonesFrottement() {
    return zonesFrot_;
  }

  @Override
  public void zonesFrottement(IZoneFrottement[] _zones) {
    if (zonesFrot_==_zones) return;
    zonesFrot_=_zones;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zonesFrottement");
  }

  @Override
  public ICrueCalageAuto[] crues() {
    return crues_;
  }

  @Override
  public void crues(ICrueCalageAuto[] _crues) {
    if (crues_==_crues) return;
    crues_=_crues;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "crues");
  }

  @Override
  public IResultatsCalageAuto resultats() {
    return resultats_;
  }

  @Override
  public void resultats(IResultatsCalageAuto _resultats) {
    if (resultats_==_resultats) return;
    resultats_=_resultats;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultats");
  }

  //---  } Interface ICalageAuto  ----------------------------------------------
}
