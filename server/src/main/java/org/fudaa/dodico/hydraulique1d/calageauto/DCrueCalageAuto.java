/*
 * @file         DCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.calageauto;

import org.fudaa.dodico.corba.hydraulique1d.calageauto.IApportCrueCalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.ICrueCalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.ICrueCalageAutoOperations;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.IMesureCrueCalageAuto;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Implémentation de l'objet "ICrueCalageAuto" contenant la description d'une crue pour le calage automatique.
 * @version      $Revision: 1.4 $ $Date: 2006-09-12 08:35:02 $ by $Author: opasteur $
 * @author       Bertrand Marchand
 */
public class DCrueCalageAuto extends DHydraulique1d implements ICrueCalageAuto,ICrueCalageAutoOperations {
  private double                   debitAmont_;
  private double                   coteAval_;
  private IApportCrueCalageAuto [] apports_;
  private IMesureCrueCalageAuto [] mesures_;

  // Constructeur.
  public DCrueCalageAuto() {
    super();
    debitAmont_=0;
    coteAval_=0;
    apports_=new IApportCrueCalageAuto[0];
    mesures_=new IMesureCrueCalageAuto[0];
  }

  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ICrueCalageAuto) {
      ICrueCalageAuto q=(ICrueCalageAuto)_o;
      debitAmont(q.debitAmont());
      coteAval(q.coteAval());
      apports(q.apports());
      mesures(q.mesures());
    }
  }

  @Override
  final public IObjet creeClone() {
    ICrueCalageAuto p= UsineLib.findUsine().creeHydraulique1dCrueCalageAuto();
    p.initialise(tie());
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Crue";
    res[1]=
      super.getInfos()[1]
        + " debitAmont : "
        + debitAmont_
        + " coteAval : "
        + coteAval_
        + " nbApports : "
        + apports_.length
        + " nbMesures : "
        + mesures_.length;

    return res;
  }

  @Override
  public void dispose() {
    debitAmont_=0;
    coteAval_=0;
    apports_=null;
    mesures_=null;
    super.dispose();
  }

  //---  Interface ICrueCalageAuto {  ------------------------------------------

  @Override
  public double debitAmont() {
    return debitAmont_;
  }

  @Override
  public void debitAmont(double _d) {
    if (debitAmont_==_d) return;
    debitAmont_=_d;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "debitAmont");
  }

  @Override
  public double coteAval() {
    return coteAval_;
  }

  @Override
  public void coteAval(double _cote) {
    if (coteAval_==_cote) return;
    coteAval_=_cote;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteAval");
  }

  @Override
  public IApportCrueCalageAuto[] apports() {
    return apports_;
  }

  @Override
  public void apports(IApportCrueCalageAuto[] _apports) {
    if (apports_==_apports) return;
    apports_=_apports;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "apports");
  }

  @Override
  public IMesureCrueCalageAuto[] mesures() {
    return mesures_;
  }

  @Override
  public void mesures(IMesureCrueCalageAuto[] _mesures) {
    if (mesures_==_mesures) return;
    mesures_=_mesures;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "mesures");
  }

  //---  } Interface ICrueCalageAuto  ------------------------------------------
}
