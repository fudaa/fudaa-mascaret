/*
 * @file         DCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.calageauto;

import org.fudaa.dodico.corba.hydraulique1d.calageauto.IMesureCrueCalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.IMesureCrueCalageAutoOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Implémentation de l'objet mesure pour une crue de calage "IMesureCrueCalageAuto".
 * @version      $Revision: 1.4 $ $Date: 2006-09-12 08:35:02 $ by $Author: opasteur $
 * @author       Bertrand Marchand
 */
public class DMesureCrueCalageAuto extends DHydraulique1d implements IMesureCrueCalageAuto,IMesureCrueCalageAutoOperations {
  private double abscisse_;
  private double cote_;
  private double coefficient_;

  // Constructeur.
  public DMesureCrueCalageAuto() {
    super();
    abscisse_=0;
    cote_=0;
    coefficient_=1.0;
  }

  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IMesureCrueCalageAuto) {
      IMesureCrueCalageAuto q=(IMesureCrueCalageAuto)_o;
      abscisse(q.abscisse());
      cote(q.cote());
      coefficient(q.coefficient());
    }
  }

  @Override
  final public IObjet creeClone() {
    IMesureCrueCalageAuto p= UsineLib.findUsine().creeHydraulique1dMesureCrueCalageAuto();
    p.initialise(tie());
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Mesure";
    res[1]=
      super.getInfos()[1]
        + " abscisse : "
        + abscisse_
        + " cote : "
        + cote_
        + " coefficient : "
        + coefficient_;

    return res;
  }

  @Override
  public void dispose() {
    abscisse_=0;
    cote_=0;
    coefficient_=0;
    super.dispose();
  }

  //---  Interface IMesureCrueCalageAuto {  ------------------------------------

  @Override
  public double abscisse() {
    return abscisse_;
  }

  @Override
  public void abscisse(double _abscisse) {
    if (abscisse_==_abscisse) return;
    abscisse_=_abscisse;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");
  }

  @Override
  public double cote() {
    return cote_;
  }

  @Override
  public void cote(double _cote) {
    if (cote_==_cote) return;
    cote_=_cote;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "cote");
  }

  @Override
  public double coefficient() {
    return coefficient_;
  }

  @Override
  public void coefficient(double _coefficient) {
    if (coefficient_==_coefficient) return;
    coefficient_=_coefficient;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefficient");
  }

  //---  } Interface IMesureCrueCalageAuto  ------------------------------------
}
