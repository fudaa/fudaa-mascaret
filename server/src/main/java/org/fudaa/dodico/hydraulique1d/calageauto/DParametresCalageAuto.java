/*
 * @file         DCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.calageauto;

import org.fudaa.dodico.corba.hydraulique1d.calageauto.IParametresCalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.IParametresCalageAutoOperations;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.LMethodeOpt;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.LTypeLit;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Implémentation de l'objet "IParametresCalageAuto" contenant les parametres pour le lancement du calage automatique.
 * @version      $Revision: 1.4 $ $Date: 2006-09-12 08:35:02 $ by $Author: opasteur $
 * @author       Bertrand Marchand
 */
public class DParametresCalageAuto extends DHydraulique1d implements IParametresCalageAuto,IParametresCalageAutoOperations {
  private double      pasGradient_;
  private int         nbMaxIterations_;
  private LTypeLit    typeLit_;
  private double      precision_;
  private double      roInit_;
  private LMethodeOpt methodeOpt_;

  // Constructeur.
  public DParametresCalageAuto() {
    super();
    pasGradient_=1;
    nbMaxIterations_=100;
    typeLit_=LTypeLit.MINEUR;
    precision_=0.001;
    roInit_=1;
    methodeOpt_=LMethodeOpt.DESCENTE_OPTIMALE;
  }

  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IParametresCalageAuto) {
      IParametresCalageAuto q=(IParametresCalageAuto)_o;
      pasGradient(q.pasGradient());
      nbMaxIterations(q.nbMaxIterations());
      typeLit(q.typeLit());
      precision(q.precision());
      roInit(roInit());
      methodeOpt(methodeOpt());
    }
  }

  @Override
  final public IObjet creeClone() {
    IParametresCalageAuto p= UsineLib.findUsine().creeHydraulique1dParametresCalageAuto();
    p.initialise(tie());
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Parametres";
    res[1]=
      super.getInfos()[1]
        + " pasGradient : "
        + pasGradient_
        + " nbMaxIterations : "
        + nbMaxIterations_
        + " typeLit : "
        + (typeLit_.equals(LTypeLit.MINEUR) ? "mineur":"majeur")
        + " precision : "
        + precision_
        + " roInit : "
        + roInit_
        + "methodeOpt : "
        + (methodeOpt_.equals(LMethodeOpt.DESCENTE_OPTIMALE) ? "Descente optimale":
           methodeOpt_.equals(LMethodeOpt.CASIER_NEWTON) ? "Casier newton":"Algo genetique");

    return res;
  }

  @Override
  public void dispose() {
    pasGradient_=0;
    nbMaxIterations_=0;
    typeLit_=null;
    precision_=0;
    roInit_=0;
    methodeOpt_=null;
    super.dispose();
  }

  //---  Interface IParametresCalageAuto {  ------------------------------------

  @Override
  public double pasGradient() {
    return pasGradient_;
  }

  @Override
  public void pasGradient(double _pas) {
    if (pasGradient_==_pas) return;
    pasGradient_=_pas;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pasGradient");
  }

  @Override
  public int nbMaxIterations() {
    return nbMaxIterations_;
  }

  @Override
  public void nbMaxIterations(int _nb) {
    if (nbMaxIterations_==_nb) return;
    nbMaxIterations_=_nb;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "nbMaxIterations");
  }

  @Override
  public LTypeLit typeLit() {
    return typeLit_;
  }

  @Override
  public void typeLit(LTypeLit _type) {
    if (typeLit_.equals(_type)) return;
    typeLit_=_type;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "typeLit");
  }

  @Override
  public double precision() {
    return precision_;
  }

  @Override
  public void precision(double _prec) {
    if (precision_==_prec) return;
    precision_=_prec;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "precision");
  }

  @Override
  public double roInit() {
    return roInit_;
  }

  @Override
  public void roInit(double _ro) {
    if (roInit_==_ro) return;
    roInit_=_ro;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "roInit");
  }

  @Override
  public LMethodeOpt methodeOpt() {
    return methodeOpt_;
  }

  @Override
  public void methodeOpt(LMethodeOpt _mth) {
    if (methodeOpt_.equals(_mth)) return;
    methodeOpt_=_mth;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "methodeOpt");
  }

  //---  } Interface IParametresCalageAuto  ------------------------------------
}
