/*
 * @file         DCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.calageauto;

import org.fudaa.dodico.corba.hydraulique1d.IResultatsTemporelSpatial;
import org.fudaa.dodico.corba.hydraulique1d.IZoneFrottement;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.IResultatsCalageAuto;
import org.fudaa.dodico.corba.hydraulique1d.calageauto.IResultatsCalageAutoOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Implémentation de l'objet "IResultatsCalageAuto" contenant les résultats du calage automatique.
 * @version      $Revision: 1.4 $ $Date: 2006-09-12 08:35:02 $ by $Author: opasteur $
 * @author       Bertrand Marchand
 */
public class DResultatsCalageAuto extends DHydraulique1d implements IResultatsCalageAuto,IResultatsCalageAutoOperations {
  private byte[]                    listingMascaret_;
  private byte[]                    listingDamocles_;
  private byte[]                    listingCalage_;
  private byte[]                    messagesEcran_;
  private byte[]                    messagesEcranErreur_;
  private IResultatsTemporelSpatial resultats_;
  private IZoneFrottement[]         zonesCalees_;

  // Constructeur.
  public DResultatsCalageAuto() {
    super();
    listingMascaret_=new byte[0];
    listingDamocles_=new byte[0];
    listingCalage_=new byte[0];
    messagesEcran_=new byte[0];
    messagesEcranErreur_=new byte[0];
    resultats_=UsineLib.findUsine().creeHydraulique1dResultatsTemporelSpatial();
    zonesCalees_=new IZoneFrottement[0];
  }

  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IResultatsCalageAuto) {
      IResultatsCalageAuto q=(IResultatsCalageAuto)_o;
      listingMascaret(q.listingMascaret());
      listingDamocles(q.listingDamocles());
      listingCalage(q.listingCalage());
      messagesEcran(q.messagesEcran());
      messagesEcranErreur(q.messagesEcranErreur());
      resultats(resultats());
      zonesFrottementCalees(zonesFrottementCalees());
    }
  }

  @Override
  final public IObjet creeClone() {
    IResultatsCalageAuto p= UsineLib.findUsine().creeHydraulique1dResultatsCalageAuto();
    p.initialise(tie());
    return p;
  }

  @Override
  final public String toString() {  // Utile lors les déclenchement de notification sur modification d'attribut.
    return "resultatsCalage";
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= toString();
    res[1]=
      super.getInfos()[1];

    return res;
  }

  @Override
  public void dispose() {
    listingMascaret_=null;
    listingDamocles_=null;
    listingCalage_=null;
    messagesEcran_=null;
    messagesEcranErreur_=null;
    if (resultats_!=null) { resultats_.dispose(); resultats_=null; }
    zonesCalees_=null;
    super.dispose();
  }

  //---  Interface IResultatsCalageAuto {  ------------------------------------

  @Override
  public byte[] listingMascaret() {
    return listingMascaret_;
  }

  @Override
  public void listingMascaret(byte[] _lis) {
    if (listingMascaret_==_lis) return;
    listingMascaret_=_lis;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "listingMascaret");
  }

  @Override
  public byte[] listingDamocles() {
    return listingDamocles_;
  }

  @Override
  public void listingDamocles(byte[] _lis) {
    if (listingDamocles_==_lis) return;
    listingDamocles_=_lis;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "listingDamocles");
  }

  @Override
  public byte[] listingCalage() {
    return listingCalage_;
  }

  @Override
  public void listingCalage(byte[] _lis) {
    if (listingCalage_==_lis) return;
    listingCalage_=_lis;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "listingCalage");
  }

  @Override
  public byte[] messagesEcran() {
    return messagesEcran_;
  }

  @Override
  public void messagesEcran(byte[] _lis) {
    if (messagesEcran_==_lis) return;
    messagesEcran_=_lis;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "messagesEcran");
  }

  @Override
  public byte[] messagesEcranErreur() {
    return messagesEcranErreur_;
  }

  @Override
  public void messagesEcranErreur(byte[] _lis) {
    if (messagesEcranErreur_==_lis) return;
    messagesEcranErreur_=_lis;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "messagesEcranErreur");
  }

  @Override
  public IResultatsTemporelSpatial resultats() {
    return resultats_;
  }

  @Override
  public void resultats(IResultatsTemporelSpatial _res) {
    if (resultats_==_res) return;
    resultats_=_res;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "resultats");
  }

  @Override
  public IZoneFrottement[] zonesFrottementCalees() {
    return zonesCalees_;
  }

  @Override
  public void zonesFrottementCalees(IZoneFrottement[] _zones) {
    if (zonesCalees_==_zones) return;
    zonesCalees_=_zones;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zonesFrottementCalees");
  }

  //---  } Interface IResultatsCalageAuto  ------------------------------------
}
