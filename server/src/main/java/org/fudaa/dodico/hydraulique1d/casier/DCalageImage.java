/**
 * @file         DCalageImage.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.geometrie.SPoint2D;
import org.fudaa.dodico.corba.hydraulique1d.casier.ICalageImage;
import org.fudaa.dodico.corba.hydraulique1d.casier.ICalageImageOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier "calage image" pour aider � d�finir la g�om�trie d'un casier.
 * Pas r�ellement utilis�.
 * @version      $Revision: 1.9 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DCalageImage
  extends DHydraulique1d
  implements ICalageImage,ICalageImageOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ICalageImage) {
      ICalageImage q= (ICalageImage)_o;
      pt1Pixel(clone(q.pt1Pixel()));
      pt2Pixel(clone(q.pt2Pixel()));
      pt3Pixel(clone(q.pt3Pixel()));
      pt1Utilisateur(clone(q.pt1Utilisateur()));
      pt2Utilisateur(clone(q.pt2Utilisateur()));
      pt3Utilisateur(clone(q.pt3Utilisateur()));
    }
  }
  @Override
  final public IObjet creeClone() {
    ICalageImage p= UsineLib.findUsine().creeHydraulique1dCalageImage();
    p.initialise(tie());
    return p;
  }
  /*** ILiaison ***/
  // constructeurs
  public DCalageImage() {
    super();
    pt1Pixel_.x= 0;
    pt1Pixel_.y= 0;
    pt1Utilisateur_.x= 0;
    pt1Utilisateur_.y= 0;
    pt2Pixel_.x= 10;
    pt2Pixel_.y= 0;
    pt2Utilisateur_.x= 10;
    pt2Utilisateur_.y= 0;
    pt3Pixel_.x= 0;
    pt3Pixel_.y= 10;
    pt3Utilisateur_.x= 0;
    pt3Utilisateur_.y= 10;
  }
  @Override
  public void dispose() {
    pt1Pixel_.x= 0;
    pt1Pixel_.y= 0;
    pt1Utilisateur_.x= 0;
    pt1Utilisateur_.y= 0;
    pt2Pixel_.x= 0;
    pt2Pixel_.y= 0;
    pt2Utilisateur_.x= 0;
    pt2Utilisateur_.y= 0;
    pt3Pixel_.x= 0;
    pt3Pixel_.y= 0;
    pt3Utilisateur_.x= 0;
    pt3Utilisateur_.y= 0;
    super.dispose();
  }
  /*** ICalageImage ***/
  // attributs
  private SPoint2D pt1Pixel_;
  @Override
  public SPoint2D pt1Pixel() {
    return pt1Pixel_;
  }
  @Override
  public void pt1Pixel(SPoint2D s) {
    if ((pt1Pixel_ != null)&&(s != null)) {
      if ( (s.x == pt1Pixel_.x) && (s.y == pt1Pixel_.y))
        return;
    }
    pt1Pixel_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pt1Pixel");
  }
  private SPoint2D pt1Utilisateur_;
  @Override
  public SPoint2D pt1Utilisateur() {
    return pt1Utilisateur_;
  }
  @Override
  public void pt1Utilisateur(SPoint2D s) {
    if ((pt1Utilisateur_ != null)&&(s != null)) {
      if ( (s.x == pt1Utilisateur_.x) && (s.y == pt1Utilisateur_.y))
        return;
    }
    pt1Utilisateur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pt1Utilisateur");
  }
  private SPoint2D pt2Pixel_;
  @Override
  public SPoint2D pt2Pixel() {
    return pt2Pixel_;
  }
  @Override
  public void pt2Pixel(SPoint2D s) {
    if ((pt2Pixel_ != null)&&(s != null)) {
      if ( (s.x == pt2Pixel_.x) && (s.y == pt2Pixel_.y))
        return;
    }
    pt2Pixel_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pt2Pixel");
  }
  private SPoint2D pt2Utilisateur_;
  @Override
  public SPoint2D pt2Utilisateur() {
    return pt2Utilisateur_;
  }
  @Override
  public void pt2Utilisateur(SPoint2D s) {
    if ((pt2Utilisateur_ != null)&&(s != null)) {
      if ( (s.x == pt2Utilisateur_.x) && (s.y == pt2Utilisateur_.y))
        return;
    }
    pt2Utilisateur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pt2Utilisateur");
  }
  private SPoint2D pt3Pixel_;
  @Override
  public SPoint2D pt3Pixel() {
    return pt3Pixel_;
  }
  @Override
  public void pt3Pixel(SPoint2D s) {
    if ((pt3Pixel_ != null)&&(s != null)) {
      if ( (s.x == pt3Pixel_.x) && (s.y == pt3Pixel_.y))
        return;
    }
    pt3Pixel_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pt3Pixel");
  }
  private SPoint2D pt3Utilisateur_;
  @Override
  public SPoint2D pt3Utilisateur() {
    return pt3Utilisateur_;
  }
  @Override
  public void pt3Utilisateur(SPoint2D s) {
    if ((pt3Utilisateur_ != null)&&(s != null)) {
      if ( (s.x == pt3Utilisateur_.x) && (s.y == pt3Utilisateur_.y))
        return;
    }
    pt3Utilisateur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pt3Utilisateur");
  }
  private static SPoint2D clone(SPoint2D pt) {
    SPoint2D ptClone= new SPoint2D();
    ptClone.x= pt.x;
    ptClone.y= pt.y;
    return ptClone;
  }
}
