/**
 * @file         DCaracteristiqueLiaison.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.hydraulique1d.LSensDebitLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ICaracteristiqueLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ICaracteristiqueLiaisonOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.hydraulique1d.Identifieur;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "caractéristique d'une liaison".
 * Abstraction des différents type de liaison (seuil, chenal, siphon, orifice).
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public abstract class DCaracteristiqueLiaison
  extends DHydraulique1d
  implements ICaracteristiqueLiaison,ICaracteristiqueLiaisonOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ICaracteristiqueLiaison) {
      ICaracteristiqueLiaison q= (ICaracteristiqueLiaison)_o;
      cote(q.cote());
      id(q.id());
    }
  }
  @Override
  final public String toString() {
    return "CaracteristiqueLiaison" + id_;
  }
  public DCaracteristiqueLiaison() {
    super();
    cote_=1;
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(
        "org.fudaa.dodico.hydraulique1d.casier.DCaracteristiqueLiaison");
  }
  @Override
  public void dispose() {
    cote_= 0;
    id_=0;
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Caractéristiques liaison";
    res[1]= "cote : " + cote_;
    return res;
  }
  /*** ICaracteristiqueLiaison ***/
  // attributs
  protected int id_;
  @Override
  public int id() {
    return id_;
  }
  @Override
  public void id(int s) {
    if (id_==s) return;
    id_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "id");
  }
  protected double cote_;
  @Override
  public double cote() {
    return cote_;
  }
  @Override
  public void cote(double s) {
    if (cote_==s) return;
    cote_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "cote");
  }
  // méthodes
  @Override
  public boolean isSeuil() {
    return false;
  }
  @Override
  public boolean isChenal() {
    return false;
  }
  @Override
  public boolean isSiphon() {
    return false;
  }
  @Override
  public boolean isOrifice() {
    return false;
  }
  @Override
  public double getCote() {
    return cote();
  }
  @Override
  public void setCote(double cote) {
    cote(cote);
  }
  @Override
  public double getLargeur() {
    throw new IllegalArgumentException("methode invalide : getLargeur()");
  }
  @Override
  public void setLargeur(double largeur) {
    throw new IllegalArgumentException("methode invalide : setLargeur()");
  }
  @Override
  public double getLongueur() {
    throw new IllegalArgumentException("methode invalide : getLongueur()");
  }
  @Override
  public void setLongueur(double longueur) {
    throw new IllegalArgumentException("methode invalide : setLongueur()");
  }
  @Override
  public double getCoefQ() {
    throw new IllegalArgumentException("methode invalide : getCoefQ()");
  }
  @Override
  public void setCoefQ(double coefQ) {
    throw new IllegalArgumentException("methode invalide : setCoefQ()");
  }
  @Override
  public double getSection() {
    throw new IllegalArgumentException("methode invalide : getSection()");
  }
  @Override
  public void setSection(double section) {
    throw new IllegalArgumentException("methode invalide : setSection()");
  }
  @Override
  public double getCoefActivation() {
    throw new IllegalArgumentException("methode invalide : getCoefActivation()");
  }
  @Override
  public void setCoefActivation(double coefActivation) {
    throw new IllegalArgumentException("methode invalide : setCoefActivation()");
  }
  @Override
  public double getRugosite() {
    throw new IllegalArgumentException("methode invalide : getRugosite()");
  }
  @Override
  public void setRugosite(double rugosite) {
    throw new IllegalArgumentException("methode invalide : setRugosite()");
  }
  @Override
  public double getCoefPerteCharge() {
    throw new IllegalArgumentException("methode invalide : getCoefPerteCharge()");
  }
  @Override
  public void setCoefPerteCharge(double coefPerteCharge) {
    throw new IllegalArgumentException("methode invalide : setCoefPerteCharge()");
  }
  @Override
  public double getCoefQOrifice() {
    throw new IllegalArgumentException("methode invalide : getCoefQOrifice()");
  }
  @Override
  public void setCoefQOrifice(double coteMin) {
    throw new IllegalArgumentException("methode invalide : setCoefQOrifice()");
  }
  @Override
  public LSensDebitLiaison getSensDebit() {
    throw new IllegalArgumentException("methode invalide : getSensDebit()");
  }
  @Override
  public void setSensDebit(LSensDebitLiaison sensDebit) {
    throw new IllegalArgumentException("methode invalide : setSensDebit()");
  }
}
