/**
 * @file         DSeuilLiaison.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.hydraulique1d.casier.IChenalLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.IChenalLiaisonOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier caractéristique d'une "liaison chenal".
 * Associe une largeur, une longueur et une rugosité.
 * @version      $Revision: 1.10 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DChenalLiaison
  extends DCaracteristiqueLiaison
  implements IChenalLiaison,IChenalLiaisonOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IChenalLiaison) {
      IChenalLiaison q= (IChenalLiaison)_o;
      largeur(q.largeur());
      longueur(q.longueur());
      rugosite(q.rugosite());
    }
  }
  @Override
  final public IObjet creeClone() {
    IChenalLiaison p= UsineLib.findUsine().creeHydraulique1dChenalLiaison();
    p.initialise(tie());
    return p;
  }
  /*** ILiaison ***/
  // constructeurs
  public DChenalLiaison() {
    super();
    largeur_= 1;
    longueur_= 10;
    rugosite_= 30;
  }
  @Override
  public void dispose() {
    id_= 0;
    largeur_= 0;
    longueur_= 0;
    rugosite_= 0;
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Chenal";
    res[1]=
      super.getInfos()[1]
        + " larg. : "
        + largeur_
        + " long. : "
        + longueur_
        + " rugosité : "
        + rugosite_;
    return res;
  }
  /*** ISeuilLiaison ***/
  // attributs
  private double largeur_;
  @Override
  public double largeur() {
    return largeur_;
  }
  @Override
  public void largeur(double s) {
    if (largeur_==s) return;
    largeur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "largeur");
  }
  private double longueur_;
  @Override
  public double longueur() {
    return longueur_;
  }
  @Override
  public void longueur(double s) {
    if (longueur_==s) return;
    longueur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "longueur");
  }
  private double rugosite_;
  @Override
  public double rugosite() {
    return rugosite_;
  }
  @Override
  public void rugosite(double s) {
    if (rugosite_==s) return;
    rugosite_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "rugosite");
  }
  // méthodes
  @Override
  public boolean isChenal() {
    return true;
  }
  @Override
  public double getLargeur() {
    return largeur();
  }
  @Override
  public void setLargeur(double largeur) {
    largeur(largeur);
  }
  @Override
  public double getLongueur() {
    return longueur();
  }
  @Override
  public void setLongueur(double longueur) {
    longueur(longueur);
  }
  @Override
  public double getRugosite() {
    return rugosite();
  }
  @Override
  public void setRugosite(double rugosite) {
    rugosite(rugosite);
  }
  @Override
  public double getCoteMoyenneFond() {
    return cote();
  }
  @Override
  public void setCoteMoyenneFond(double coteMoyenneFond) {
    cote(coteMoyenneFond);
  }
}
