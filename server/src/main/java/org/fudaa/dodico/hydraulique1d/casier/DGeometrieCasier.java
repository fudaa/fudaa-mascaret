/**
 * @file         DGeometrieCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.geometrie.SPoint;
import org.fudaa.dodico.corba.geometrie.SPoint2D;
import org.fudaa.dodico.corba.hydraulique1d.casier.IGeometrieCasier;
import org.fudaa.dodico.corba.hydraulique1d.casier.IGeometrieCasierOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.hydraulique1d.Identifieur;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier "géométrie d'un casier".
 * Abstraction des différentes géométrie (planimétrage ou nuage de point).
 * @version      $Revision: 1.9 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public abstract class DGeometrieCasier
  extends DHydraulique1d
  implements IGeometrieCasier,IGeometrieCasierOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IGeometrieCasier) {
      IGeometrieCasier q= (IGeometrieCasier)_o;
      pasPlanimetrage(q.pasPlanimetrage());
    }
  }
  @Override
  final public String toString() {
    String s= "Geometrie casier" + id_;
    return s;
  }
  /*** IGeometrieCasier ***/
  // constructeurs
  public DGeometrieCasier() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    pasPlanimetrage_= 1;
  }
  @Override
  public void dispose() {
    id_= 0;
    pasPlanimetrage_= 0;
    super.dispose();
  }
  // attributs
  private int id_;
  private double pasPlanimetrage_;
  @Override
  public double pasPlanimetrage() {
    return pasPlanimetrage_;
  }
  @Override
  public void pasPlanimetrage(double pasPlanimetrage) {
    if (pasPlanimetrage_==pasPlanimetrage) return;
    pasPlanimetrage_= pasPlanimetrage;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pasPlanimetrage");
  }
  // les méthodes
  @Override
  public boolean isSurfaceDependCote() {
    return false;
  }
  @Override
  public void setSurfaceDependCote(boolean surfaceDependCote) {
    throw new IllegalArgumentException("methode invalide : setSurfaceDependCote()");
  }
  @Override
  public double getPasPlanimetrage() {
    return pasPlanimetrage();
  }
  @Override
  public void setPasPlanimetrage(double pasPlanimetrage) {
    pasPlanimetrage(pasPlanimetrage);
  }
  @Override
  public SPoint[] getPointsFrontiere() {
    throw new IllegalArgumentException("methode invalide : getPointsFrontiere()");
  }
  @Override
  public void setPointsFrontiere(SPoint[] points) {
    throw new IllegalArgumentException("methode invalide : setPointsFrontiere()");
  }
  @Override
  public SPoint[] getPointsInterieur() {
    throw new IllegalArgumentException("methode invalide : getPointsInterieur()");
  }
  @Override
  public void setPointsInterieur(SPoint[] points) {
    throw new IllegalArgumentException("methode invalide : setPointsInterieur()");
  }
  @Override
  public int getNbCotePlanimetrage() {
    throw new IllegalArgumentException("methode invalide : getNbCotePlanimetrage()");
  }
  @Override
  public boolean isNuagePoints() {
    return false;
  }
  @Override
  public boolean isPlanimetrage() {
    return false;
  }
  @Override
  public SPoint2D[] getPointsPlanimetrage() {
    throw new IllegalArgumentException("methode invalide : getPointsPlanimetrage()");
  }
  @Override
  public void setPointsPlanimetrage(SPoint2D[] points) {
    throw new IllegalArgumentException("methode invalide : setPointsPlanimetrage()");
  }
}
