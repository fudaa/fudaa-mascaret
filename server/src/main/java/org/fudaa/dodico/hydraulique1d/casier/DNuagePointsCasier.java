/**
 * @file         DNuagePointsCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import java.util.Arrays;

import org.fudaa.dodico.corba.geometrie.SPoint;
import org.fudaa.dodico.corba.hydraulique1d.casier.ICalageImage;
import org.fudaa.dodico.corba.hydraulique1d.casier.INuagePointsCasier;
import org.fudaa.dodico.corba.hydraulique1d.casier.INuagePointsCasierOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier g�om�trie d'un casier d�crit par des "nuages de points 3D".
 * Associe les nuages de points fronti�res et int�rieurs largeur, un indicateur
 * de d�pendance de la surface vis a vis de la cote.
 * L'image et le calage de l'image ne sont pas encore utilis�s.
 * @version      $Revision: 1.10 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DNuagePointsCasier
  extends DGeometrieCasier
  implements INuagePointsCasier,INuagePointsCasierOperations {
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Semis de points";
    res[1]= "";
    return res;
  }
  /*** INuagePointsCasier ***/
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof INuagePointsCasier) {
      INuagePointsCasier q= (INuagePointsCasier)_o;
      surfaceDependCote(q.surfaceDependCote());
      pointsFrontiere((SPoint[])q.pointsFrontiere().clone());
      pointsInterieur((SPoint[])q.pointsInterieur().clone());
      image((byte[])q.image().clone());
      if (q.calage() != null)
        calage((ICalageImage)q.calage().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    INuagePointsCasier p=
      UsineLib.findUsine().creeHydraulique1dNuagePointsCasier();
    p.initialise(tie());
    return p;
  }
  // constructeurs
  public DNuagePointsCasier() {
    super();
    surfaceDependCote_= false;
    pointsFrontiere_= new SPoint[0];
    pointsInterieur_= new SPoint[0];
    image_= new byte[0];
    calage_= null;
  }
  @Override
  public void dispose() {
    surfaceDependCote_= false;
    pointsFrontiere_= null;
    pointsInterieur_= null;
    image_= null;
    calage_= null;
    super.dispose();
  }
  // attributs
  private boolean surfaceDependCote_;
  @Override
  public boolean surfaceDependCote() {
    return surfaceDependCote_;
  }
  @Override
  public void surfaceDependCote(boolean surfaceDependCote) {
    if (surfaceDependCote_==surfaceDependCote) return;
    surfaceDependCote_= surfaceDependCote;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "surfaceDependCote");
  }
  private SPoint[] pointsFrontiere_;
  @Override
  public SPoint[] pointsFrontiere() {
    return pointsFrontiere_;
  }
  @Override
  public void pointsFrontiere(SPoint[] points) {
    if (egale(pointsFrontiere_,points)) return;
    pointsFrontiere_= points;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pointsFrontiere");
  }
  private SPoint[] pointsInterieur_;
  @Override
  public SPoint[] pointsInterieur() {
    return pointsInterieur_;
  }
  @Override
  public void pointsInterieur(SPoint[] points) {
    if (egale(pointsInterieur_,points)) return;
    pointsInterieur_= points;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "pointsInterieur");
  }
  private byte[] image_;
  @Override
  public byte[] image() {
    return image_;
  }
  @Override
  public void image(byte[] image) {
    if (Arrays.equals(image_, image)) return;
    image_= image;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "image");
  }
  private ICalageImage calage_;
  @Override
  public ICalageImage calage() {
    return calage_;
  }
  @Override
  public void calage(ICalageImage s) {
    if (calage_== s) return;
    calage_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "calage");
  }
  private final static boolean egale(SPoint[] p, SPoint[] p2) {
    if (p==p2) return true;
    if (p==null || p2==null) return false;

    int length = p.length;
    if (p2.length != length) return false;

    for (int i=0; i<length; i++)
        if (!((p[i].x==p2[i].x)&&(p[i].y==p2[i].y)&&(p[i].z==p2[i].z)))
            return false;

    return true;
  }
  // les m�thodes
  @Override
  public boolean isSurfaceDependCote() {
    return surfaceDependCote_;
  }
  @Override
  public void setSurfaceDependCote(boolean surfaceDependCote) {
    surfaceDependCote(surfaceDependCote);
  }
  @Override
  public double getPasPlanimetrage() {
    return pasPlanimetrage();
  }
  @Override
  public void setPasPlanimetrage(double pasPlanimetrage) {
    pasPlanimetrage(pasPlanimetrage);
  }
  @Override
  public SPoint[] getPointsFrontiere() {
    return pointsFrontiere_;
  }
  @Override
  public void setPointsFrontiere(SPoint[] points) {
    pointsFrontiere(points);
  }
  @Override
  public SPoint[] getPointsInterieur() {
    return pointsInterieur_;
  }
  @Override
  public void setPointsInterieur(SPoint[] points) {
    pointsInterieur(points);
  }
  @Override
  public int getNbCotePlanimetrage() {
    double min1= coteMin(pointsFrontiere_);
    double min2= coteMin(pointsInterieur_);
    double min= Math.min(min1, min2);
    double max1= coteMax(pointsFrontiere_);
    double max2= coteMax(pointsInterieur_);
    double max= Math.max(max1, max2);
    double diff= max - min;
    return (int) (diff / pasPlanimetrage());
  }
  @Override
  public boolean isNuagePoints() {
    return true;
  }
  private final double coteMin(SPoint[] pts) {
    double res= Double.MAX_VALUE;
    for (int i= 0; i < pts.length; i++) {
      res= Math.min(res, pts[i].z);
    }
    return res;
  }
  private final double coteMax(SPoint[] pts) {
    double res= Double.NEGATIVE_INFINITY;
    for (int i= 0; i < pts.length; i++) {
      res= Math.max(res, pts[i].z);
    }
    return res;
  }
}
