/**
 * @file         DOrificeLiaison.java
 * @creation     2003-10-20
 * @modification $Date: 2006-11-29 15:11:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.hydraulique1d.LSensDebitLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.IOrificeLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.IOrificeLiaisonOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier caract�ristique d'une "liaison orifice".
 * Associe une largeur, une section, un sens et 2 coefficients de d�bit.
 * @version      $Revision: 1.12 $ $Date: 2006-11-29 15:11:38 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DOrificeLiaison
  extends DCaracteristiqueLiaison
  implements IOrificeLiaison,IOrificeLiaisonOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IOrificeLiaison) {
      IOrificeLiaison q= (IOrificeLiaison)_o;
      coefQSeuil(q.coefQSeuil());
      coefQOrifice(q.coefQOrifice());
      largeur(q.largeur());
      section(q.section());
      sensDebit(LSensDebitLiaison.from_int(q.sensDebit().value()));
    }
  }
  @Override
  final public IObjet creeClone() {
    IOrificeLiaison p= UsineLib.findUsine().creeHydraulique1dOrificeLiaison();
    p.initialise(tie());
    return p;
  }
  @Override
  public String[] getInfos() {
    String sensDebit = "les 2";
    if (sensDebit_.value() == LSensDebitLiaison._BIEF_VERS_CASIER_OU_VERS_AMONT) {
      sensDebit = "Fin->Origine";
    } else if (sensDebit_.value() == LSensDebitLiaison._CASIER_VERS_BIEF_OU_VERS_AVAL) {
      sensDebit = "Origine->Fin";
    }
    String[] res= new String[2];
    res[0]= "Orifice";
    res[1]=
      super.getInfos()[1]
        + " larg. : "
        + largeur_
        + " coef. Q seuil : "
        + coefQSeuil_
        + " coef. Q orifice : "
        + coefQOrifice_
        + " sens Q : "
        + sensDebit;
    return res;
  }
  /*** ILiaison ***/
  // constructeurs
  public DOrificeLiaison() {
    super();
    largeur_= 1;
    coefQSeuil_= 0.40;
    section_= 1;
    coefQOrifice_= 0.5;
    sensDebit_= LSensDebitLiaison.DEUX_SENS;
  }
  @Override
  public void dispose() {
    id_= 0;
    largeur_= 0;
    coefQSeuil_= 0;
    section_= 0;
    coefQOrifice_= 0;
    sensDebit_= null;
    super.dispose();
  }
  /*** ISeuilLiaison ***/
  // attributs
  private double largeur_;
  @Override
  public double largeur() {
    return largeur_;
  }
  @Override
  public void largeur(double s) {
    if (largeur_==s) return;
    largeur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "largeur");
  }
  private double coefQSeuil_;
  @Override
  public double coefQSeuil() {
    return coefQSeuil_;
  }
  @Override
  public void coefQSeuil(double s) {
    if (coefQSeuil_==s) return;
    coefQSeuil_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefQSeuil");
  }
  private double section_;
  @Override
  public double section() {
    return section_;
  }
  @Override
  public void section(double s) {
    if (section_==s) return;
    section_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "section");
  }
  private double coefQOrifice_;
  @Override
  public double coefQOrifice() {
    return coefQOrifice_;
  }
  @Override
  public void coefQOrifice(double s) {
    if (coefQOrifice_==s) return;
    coefQOrifice_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefQOrifice");
  }
  private LSensDebitLiaison sensDebit_;
  @Override
  public LSensDebitLiaison sensDebit() {
    return sensDebit_;
  }
  @Override
  public void sensDebit(LSensDebitLiaison s) {
    /*System.out.println("DOrificeLiaison sensDebit(LSensDebitLiaison s)");
    System.out.println("s.value()="+s.value());
    System.out.println("sensDebit_.value()="+sensDebit_.value());
    System.out.println("(sensDebit_==s) ="+(sensDebit_==s));*/
    if (sensDebit_==s) return;
    //System.out.println("Affectation sensDebit_= s;");
    sensDebit_= s;
    //System.out.println("CDodico.findUsine().fireObjetModifie");
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "sensDebit");
  }
  // m�thodes
  @Override
  public boolean isOrifice() {
    return true;
  }
  @Override
  public double getLargeur() {
    return largeur();
  }
  @Override
  public void setLargeur(double largeur) {
    largeur(largeur);
  }
  @Override
  public double getCoefQ() {
    return coefQSeuil();
  }
  @Override
  public void setCoefQ(double coefQ) {
    coefQSeuil(coefQ);
  }
  @Override
  public double getSection() {
    return section();
  }
  @Override
  public void setSection(double section) {
    section(section);
  }
  @Override
  public double getCoefQOrifice() {
    return coefQOrifice();
  }
  @Override
  public void setCoefQOrifice(double coefQOrifice) {
    coefQOrifice(coefQOrifice);
  }
  @Override
  public LSensDebitLiaison getSensDebit() {
    return sensDebit();
  }
  @Override
  public void setSensDebit(LSensDebitLiaison sensDebit) {
    sensDebit(sensDebit);
  }
  @Override
  public double getCoteMoyenneRadier() {
    return cote();
  }
  @Override
  public void setCoteMoyenneRadier(double coteMoyenneRadier) {
    cote(coteMoyenneRadier);
  }
}
