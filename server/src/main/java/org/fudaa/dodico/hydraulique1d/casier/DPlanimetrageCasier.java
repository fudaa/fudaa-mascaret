/**
 * @file         DPlanimetrageCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.geometrie.SPoint2D;
import org.fudaa.dodico.corba.hydraulique1d.casier.IPlanimetrageCasier;
import org.fudaa.dodico.corba.hydraulique1d.casier.IPlanimetrageCasierOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier géométrie d'un casier décrit par un "planimétrage".
 * Associe les points 2D du planimétrage et la cote du fond.
 * @version      $Revision: 1.10 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DPlanimetrageCasier
  extends DGeometrieCasier
  implements IPlanimetrageCasier,IPlanimetrageCasierOperations {
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Zfond : "+coteInitiale_+" Planimétrie ";
    res[1]= "";
    return res;
  }
  /*** IPlanimetrageCasier ***/
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IPlanimetrageCasier) {
      IPlanimetrageCasier q= (IPlanimetrageCasier)_o;
      coteInitiale(q.coteInitiale());
      points((SPoint2D[])q.points().clone());
    }
  }
  @Override
  final public IObjet creeClone() {
    IPlanimetrageCasier p=
      UsineLib.findUsine().creeHydraulique1dPlanimetrageCasier();
    p.initialise(tie());
    return p;
  }
  // constructeurs
  public DPlanimetrageCasier() {
    super();
    coteInitiale_= 0;
    points_= new SPoint2D[0];
  }
  @Override
  public void dispose() {
    coteInitiale_= -1;
    points_= null;
    super.dispose();
  }
  // attributs
  private double coteInitiale_;
  @Override
  public double coteInitiale() {
    return coteInitiale_;
  }
  @Override
  public void coteInitiale(double coteInitiale) {
    if (coteInitiale_==coteInitiale) return;
    coteInitiale_= coteInitiale;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteInitiale");
  }
  private SPoint2D[] points_;
  @Override
  public SPoint2D[] points() {
    return points_;
  }
  @Override
  public void points(SPoint2D[] points) {
    if (egale(points_, points)) return;
    points_= points;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "points");
  }
  private final static boolean egale(SPoint2D[] p, SPoint2D[] p2) {
    if (p==p2) return true;
    if (p==null || p2==null) return false;

    int length = p.length;
    if (p2.length != length) return false;

    for (int i=0; i<length; i++)
        if (!((p[i].x==p2[i].x)&&(p[i].y==p2[i].y)))
            return false;

    return true;
  }
  // les méthodes
  @Override
  public SPoint2D[] getPointsPlanimetrage() {
    return points_;
  }
  @Override
  public void setPointsPlanimetrage(SPoint2D[] points) {
    points(points);
  }
  @Override
  public boolean isPlanimetrage() {
    return true;
  }
}
