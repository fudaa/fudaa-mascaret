/**
 * @file         DSeuilLiaison.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.hydraulique1d.casier.ISeuilLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ISeuilLiaisonOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier caract�ristique d'une "liaison seuil".
 * Associe une largeur, un coefficient de d�bit et un coefficient d'activation.
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DSeuilLiaison
  extends DCaracteristiqueLiaison
  implements ISeuilLiaison,ISeuilLiaisonOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilLiaison) {
      ISeuilLiaison q= (ISeuilLiaison)_o;
      largeur(q.largeur());
      coefQ(q.coefQ());
      coefActivation(q.coefActivation());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilLiaison p= UsineLib.findUsine().creeHydraulique1dSeuilLiaison();
    p.initialise(tie());
    return p;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil";
    res[1]=
      super.getInfos()[1]
        + " larg. : "
        + largeur_
        + " coef. Q : "
        + coefQ_
        + " coef. Activation : "
        + coefActivation_;
    return res;
  }
  /*** ILiaison ***/
  // constructeurs
  public DSeuilLiaison() {
    super();
    largeur_= 1;
    coefQ_= 0.40;
    coefActivation_= 0.50;
  }
  @Override
  public void dispose() {
    id_= 0;
    largeur_= 0;
    coefQ_= 0;
    coefActivation_= 0;
    super.dispose();
  }
  /*** ISeuilLiaison ***/
  // attributs
  private double largeur_;
  @Override
  public double largeur() {
    return largeur_;
  }
  @Override
  public void largeur(double s) {
    if (largeur_==s) return;
    largeur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "largeur");
  }
  private double coefQ_;
  @Override
  public double coefQ() {
    return coefQ_;
  }
  @Override
  public void coefQ(double s) {
    if (coefQ_==s) return;
    coefQ_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefQ");
  }
  private double coefActivation_;
  @Override
  public double coefActivation() {
    return coefActivation_;
  }
  @Override
  public void coefActivation(double s) {
    if (coefActivation_==s) return;
    coefActivation_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefActivation");
  }
  // m�thodes
  @Override
  public boolean isSeuil() {
    return true;
  }
  @Override
  public double getLargeur() {
    return largeur();
  }
  @Override
  public void setLargeur(double largeur) {
    largeur(largeur);
  }
  @Override
  public double getCoefQ() {
    return coefQ();
  }
  @Override
  public void setCoefQ(double coefQ) {
    coefQ(coefQ);
  }
  @Override
  public double getCoefActivation() {
    return coefActivation();
  }
  @Override
  public void setCoefActivation(double coefActivation) {
    coefActivation(coefActivation);
  }
  @Override
  public double getCoteMoyenneCrete() {
    return cote();
  }
  @Override
  public void setCoteMoyenneCrete(double coteMoyenneFond) {
    cote(coteMoyenneFond);
  }
}
