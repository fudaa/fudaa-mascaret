/**
 * @file         DSiphonLiaison.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.hydraulique1d.casier.ISiphonLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ISiphonLiaisonOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier caractéristique d'une "liaison siphon".
 * Associe une longueur, une section et un coefficient de perte de charge.
 * @version      $Revision: 1.10 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DSiphonLiaison
  extends DCaracteristiqueLiaison
  implements ISiphonLiaison,ISiphonLiaisonOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISiphonLiaison) {
      ISiphonLiaison q= (ISiphonLiaison)_o;
      longueur(q.longueur());
      section(q.section());
      coefPerteCharge(q.coefPerteCharge());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISiphonLiaison p= UsineLib.findUsine().creeHydraulique1dSiphonLiaison();
    p.initialise(tie());
    return p;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Siphon";
    res[1]=
      super.getInfos()[1]
        + " long. : "
        + longueur_
        + " sec. : "
        + section_
        + " perte charge : "
        + coefPerteCharge_;
    return res;
  }
  /*** ILiaison ***/
  // constructeurs
  public DSiphonLiaison() {
    super();
    longueur_= 1;
    section_= 10;
    coefPerteCharge_= 0.40;
  }
  @Override
  public void dispose() {
    id_= 0;
    longueur_= 0;
    section_= 0;
    coefPerteCharge_= 0;
    super.dispose();
  }
  /*** ISiphonLiaison ***/
  // attributs
  private double longueur_;
  @Override
  public double longueur() {
    return longueur_;
  }
  @Override
  public void longueur(double s) {
    if (longueur_==s) return;
    longueur_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "longueur");
  }
  private double section_;
  @Override
  public double section() {
    return section_;
  }
  @Override
  public void section(double s) {
    if (section_==s) return;
    section_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "section");
  }
  private double coefPerteCharge_;
  @Override
  public double coefPerteCharge() {
    return coefPerteCharge_;
  }
  @Override
  public void coefPerteCharge(double s) {
    if (coefPerteCharge_==s) return;
    coefPerteCharge_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefPerteCharge");
  }
  // méthodes
  @Override
  public boolean isSiphon() {
    return true;
  }
  @Override
  public double getLongueur() {
    return longueur();
  }
  @Override
  public void setLongueur(double longueur) {
    longueur(longueur);
  }
  @Override
  public double getSection() {
    return section();
  }
  @Override
  public void setSection(double section) {
    section(section);
  }
  @Override
  public double getCoefPerteCharge() {
    return coefPerteCharge();
  }
  @Override
  public void setCoefPerteCharge(double coefPerteCharge) {
    coefPerteCharge(coefPerteCharge);
  }
  @Override
  public double getCoteMoyenneFond() {
    return cote();
  }
  @Override
  public void setCoteMoyenneFond(double coteMoyenneFond) {
    cote(coteMoyenneFond);
  }
}
