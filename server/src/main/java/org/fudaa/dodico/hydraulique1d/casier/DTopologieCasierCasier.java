/**
 * @file         DTopologieCasierCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.hydraulique1d.ICasier;
import org.fudaa.dodico.corba.hydraulique1d.casier.ITopologieCasierCasier;
import org.fudaa.dodico.corba.hydraulique1d.casier.ITopologieCasierCasierOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier topologie d'une liaison reliant 2 casiers.
 * Associe 2 r�f�rences vers le casier amont et le casier aval.
 * @version      $Revision: 1.9 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DTopologieCasierCasier
  extends DTopologieLiaison
  implements ITopologieCasierCasier,ITopologieCasierCasierOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ITopologieCasierCasier) {
      ITopologieCasierCasier q= (ITopologieCasierCasier)_o;
      casierAmontRattache(q.casierAmontRattache());
      casierAvalRattache(q.casierAvalRattache());
    }
  }
  @Override
  final public IObjet creeClone() {
    ITopologieCasierCasier p=
      UsineLib.findUsine().creeHydraulique1dTopologieCasierCasier();
    p.initialise(tie());
    return p;
  }
  public DTopologieCasierCasier() {
    super();
    casierAmontRattache_= null;
    casierAvalRattache_= null;
  }
  @Override
  public void dispose() {
    casierAmontRattache_= null;
    casierAvalRattache_= null;
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Casier-Casier";
    res[1]= "";
    return res;
  }
  /*** ITopologieCasierCasier ***/
  // attributs
  private ICasier casierAmontRattache_;
  @Override
  public ICasier casierAmontRattache() {
    return casierAmontRattache_;
  }
  @Override
  public void casierAmontRattache(ICasier s) {
    if (casierAmontRattache_ == s) return;
    casierAmontRattache_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "casierAmontRattache");
  }
  private ICasier casierAvalRattache_;
  @Override
  public ICasier casierAvalRattache() {
    return casierAvalRattache_;
  }
  @Override
  public void casierAvalRattache(ICasier s) {
    if (casierAvalRattache_== s) return;
    casierAvalRattache_= s;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "casierAvalRattache");
  }
  // m�thodes
  @Override
  public boolean isCasierCasier() {
    return true;
  }
  @Override
  public ICasier getCasierAmontRattache() {
    return casierAmontRattache();
  }
  @Override
  public void setCasierAmontRattache(ICasier casierRattache) {
    casierAmontRattache(casierRattache);
  }
  @Override
  public ICasier getCasierAvalRattache() {
    return casierAvalRattache();
  }
  @Override
  public void setCasierAvalRattache(ICasier casierRattache) {
    casierAvalRattache(casierRattache);
  }
}
