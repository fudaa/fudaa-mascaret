/**
 * @file         DTopologieLiaison.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ICasier;
import org.fudaa.dodico.corba.hydraulique1d.casier.ITopologieLiaison;
import org.fudaa.dodico.corba.hydraulique1d.casier.ITopologieLiaisonOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.hydraulique1d.Identifieur;
/**
 * Impl�mentation de l'objet m�tier "topologie d'une liaison".
 * Abstraction des diff�rents type de topologie (casier-casier, bief-casier).
 * @version      $Revision: 1.6 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public abstract class DTopologieLiaison
  extends DHydraulique1d
  implements ITopologieLiaison,ITopologieLiaisonOperations {
  @Override
  public void initialise(IObjet _o) {
    if (_o instanceof ITopologieLiaison) {
      ITopologieLiaison q= (ITopologieLiaison)_o;
    }
  }
  @Override
  final public String toString() {
    return "TopologieLiaison" + id_;
  }
  public DTopologieLiaison() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
  }
  @Override
  public void dispose() {
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Topologie liaison";
    res[1]= "";
    return res;
  }
  /*** ITopologieLiaison ***/
  // attributs
  protected int id_;
  // m�thodes
  @Override
  public boolean isRiviereCasier() {
    return false;
  }
  @Override
  public boolean isCasierCasier() {
    return false;
  }
  @Override
  public double getAbscisse() {
    throw new IllegalArgumentException("methode invalide : getAbscisse()");
  }
  @Override
  public void setAbscisse(double abscisse) {
    throw new IllegalArgumentException("methode invalide : setAbscisse()");
  }
  @Override
  public ICasier getCasierRattache() {
    throw new IllegalArgumentException("methode invalide : getCasierRattache()");
  }
  @Override
  public void setCasierRattache(ICasier casierRattache) {
    throw new IllegalArgumentException("methode invalide : setCasierRattache()");
  }
  @Override
  public IBief getBiefRattache() {
    throw new IllegalArgumentException("methode invalide : getBiefRattache()");
  }
  @Override
  public void setBiefRattache(IBief biefRattache) {
    throw new IllegalArgumentException("methode invalide : setBiefRattache()");
  }
  @Override
  public ICasier getCasierAmontRattache() {
    throw new IllegalArgumentException("methode invalide : getCasierAmontRattache()");
  }
  @Override
  public void setCasierAmontRattache(ICasier casierRattache) {
    throw new IllegalArgumentException("methode invalide : setCasierAmontRattache()");
  }
  @Override
  public ICasier getCasierAvalRattache() {
    throw new IllegalArgumentException("methode invalide : getCasierAvalRattache()");
  }
  @Override
  public void setCasierAvalRattache(ICasier casierRattache) {
    throw new IllegalArgumentException("methode invalide : setCasierAvalRattache()");
  }
}
