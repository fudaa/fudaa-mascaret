/**
 * @file         DTopologieRiviereCasier.java
 * @creation
 * @modification $Date: 2006-09-12 08:35:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.casier;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ICasier;
import org.fudaa.dodico.corba.hydraulique1d.casier.ITopologieRiviereCasier;
import org.fudaa.dodico.corba.hydraulique1d.casier.ITopologieRiviereCasierOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier topologie d'une liaison reliant 1 casier avec une liaison.
 * Associe une r�f�rence vers un bief (et une abscisse) et une r�f�rence vers un casier.
 * @version      $Revision: 1.9 $ $Date: 2006-09-12 08:35:01 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class DTopologieRiviereCasier
  extends DTopologieLiaison
  implements ITopologieRiviereCasier,ITopologieRiviereCasierOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ITopologieRiviereCasier) {
      ITopologieRiviereCasier q= (ITopologieRiviereCasier)_o;
      abscisse(q.abscisse());
      biefRattache(q.biefRattache());
      casierRattache(q.casierRattache());
    }
  }
  @Override
  final public IObjet creeClone() {
    ITopologieRiviereCasier p=
      UsineLib.findUsine().creeHydraulique1dTopologieRiviereCasier();
    p.initialise(tie());
    return p;
  }
  public DTopologieRiviereCasier() {
    super();
    abscisse_= 0;
    casierRattache_= null;
    biefRattache_= null;
  }
  @Override
  public void dispose() {
    abscisse_= 0;
    casierRattache_= null;
    biefRattache_= null;
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Rivi�re-Casier";
    res[1]= "abscisse : " + abscisse_;
    return res;
  }
  /*** ITopologieRiviereCasier ***/
  // attributs
  private double abscisse_;
  @Override
  public double abscisse() {
    return abscisse_;
  }
  @Override
  public void abscisse(double s) {
    if (abscisse_==s) return;
    abscisse_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");
  }
  private ICasier casierRattache_;
  @Override
  public ICasier casierRattache() {
    return casierRattache_;
  }
  @Override
  public void casierRattache(ICasier s) {
    if ((casierRattache_!=null)&&(s!=null)) {
      if (casierRattache_==s) return;
    }
    casierRattache_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "casierRattache");
  }
  private IBief biefRattache_;
  @Override
  public IBief biefRattache() {
    return biefRattache_;
  }
  @Override
  public void biefRattache(IBief s) {
    if (biefRattache_==s) return;
    biefRattache_= s;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "biefRattache");
  }
  // m�thodes
  @Override
  public boolean isRiviereCasier() {
    return true;
  }
  @Override
  public double getAbscisse() {
    return abscisse();
  }
  @Override
  public void setAbscisse(double abscisse) {
    abscisse(abscisse);
  }
  @Override
  public ICasier getCasierRattache() {
    return casierRattache();
  }
  @Override
  public void setCasierRattache(ICasier casierRattache) {
    casierRattache(casierRattache);
  }
  @Override
  public IBief getBiefRattache() {
    return biefRattache();
  }
  @Override
  public void setBiefRattache(IBief biefRattache) {
    biefRattache(biefRattache);
  }
}
