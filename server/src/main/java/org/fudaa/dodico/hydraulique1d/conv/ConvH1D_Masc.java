/*
 * @file         ConvH1D_Masc.java
 * @creation     2004-03-01
 * @modification $Date: 2007-11-20 11:43:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.conv;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.fudaa.dodico.corba.mascaret.SGeoCasier;
import org.fudaa.dodico.corba.mascaret.SGeoCasiers;
import org.fudaa.dodico.corba.mascaret.SLoiHydraulique;
import org.fudaa.dodico.corba.mascaret.SLoiTracer;
import org.fudaa.dodico.corba.mascaret.SNoeud;
import org.fudaa.dodico.corba.mascaret.SParamMeteoTracer;
import org.fudaa.dodico.corba.mascaret.SParamPhysTracer;
import org.fudaa.dodico.corba.mascaret.SParametresApporDeversoirs;
import org.fudaa.dodico.corba.mascaret.SParametresApportCasier;
import org.fudaa.dodico.corba.mascaret.SParametresBarrPrincip;
import org.fudaa.dodico.corba.mascaret.SParametresBiblio;
import org.fudaa.dodico.corba.mascaret.SParametresBief;
import org.fudaa.dodico.corba.mascaret.SParametresBranches;
import org.fudaa.dodico.corba.mascaret.SParametresCAS;
import org.fudaa.dodico.corba.mascaret.SParametresCalage;
import org.fudaa.dodico.corba.mascaret.SParametresCalageAuto;
import org.fudaa.dodico.corba.mascaret.SParametresCasier;
import org.fudaa.dodico.corba.mascaret.SParametresConcInitTracer;
import org.fudaa.dodico.corba.mascaret.SParametresCondInit;
import org.fudaa.dodico.corba.mascaret.SParametresCondLimTracer;
import org.fudaa.dodico.corba.mascaret.SParametresConfluent;
import org.fudaa.dodico.corba.mascaret.SParametresConfluents;
import org.fudaa.dodico.corba.mascaret.SParametresConvecDiffu;
import org.fudaa.dodico.corba.mascaret.SParametresCrueCalageAuto;
import org.fudaa.dodico.corba.mascaret.SParametresCruesCalageAuto;
import org.fudaa.dodico.corba.mascaret.SParametresDebordProgr;
import org.fudaa.dodico.corba.mascaret.SParametresDeversLateraux;
import org.fudaa.dodico.corba.mascaret.SParametresDeversoirsV5P2;
import org.fudaa.dodico.corba.mascaret.SParametresDonneesLoi;
import org.fudaa.dodico.corba.mascaret.SParametresExtrLibres;
import org.fudaa.dodico.corba.mascaret.SParametresFichReprise;
import org.fudaa.dodico.corba.mascaret.SParametresFichResCasier;
import org.fudaa.dodico.corba.mascaret.SParametresFrottement;
import org.fudaa.dodico.corba.mascaret.SParametresGEO;
import org.fudaa.dodico.corba.mascaret.SParametresGen;
import org.fudaa.dodico.corba.mascaret.SParametresGeoReseau;
import org.fudaa.dodico.corba.mascaret.SParametresGeom;
import org.fudaa.dodico.corba.mascaret.SParametresImpress;
import org.fudaa.dodico.corba.mascaret.SParametresImpressResult;
import org.fudaa.dodico.corba.mascaret.SParametresImpressResultTracer;
import org.fudaa.dodico.corba.mascaret.SParametresLiaisons;
import org.fudaa.dodico.corba.mascaret.SParametresLigEau;
import org.fudaa.dodico.corba.mascaret.SParametresListing;
import org.fudaa.dodico.corba.mascaret.SParametresLoi;
import org.fudaa.dodico.corba.mascaret.SParametresLoiTracer;
import org.fudaa.dodico.corba.mascaret.SParametresLoisHydrau;
import org.fudaa.dodico.corba.mascaret.SParametresLoisTracer;
import org.fudaa.dodico.corba.mascaret.SParametresMaillage;
import org.fudaa.dodico.corba.mascaret.SParametresMaillageClavier;
import org.fudaa.dodico.corba.mascaret.SParametresModelPhy;
import org.fudaa.dodico.corba.mascaret.SParametresNoeuds;
import org.fudaa.dodico.corba.mascaret.SParametresNum;
import org.fudaa.dodico.corba.mascaret.SParametresNumCasier;
import org.fudaa.dodico.corba.mascaret.SParametresNumQualiteEau;
import org.fudaa.dodico.corba.mascaret.SParametresParamsCalageAuto;
import org.fudaa.dodico.corba.mascaret.SParametresPasStock;
import org.fudaa.dodico.corba.mascaret.SParametresPerteCharge;
import org.fudaa.dodico.corba.mascaret.SParametresPlanim;
import org.fudaa.dodico.corba.mascaret.SParametresPlanimMaillage;
import org.fudaa.dodico.corba.mascaret.SParametresProfil;
import org.fudaa.dodico.corba.mascaret.SParametresPt;
import org.fudaa.dodico.corba.mascaret.SParametresQApport;
import org.fudaa.dodico.corba.mascaret.SParametresREP;
import org.fudaa.dodico.corba.mascaret.SParametresReprEtude;
import org.fudaa.dodico.corba.mascaret.SParametresResult;
import org.fudaa.dodico.corba.mascaret.SParametresRubens;
import org.fudaa.dodico.corba.mascaret.SParametresSeuil;
import org.fudaa.dodico.corba.mascaret.SParametresSingularite;
import org.fudaa.dodico.corba.mascaret.SParametresSourcesTraceurs;
import org.fudaa.dodico.corba.mascaret.SParametresStockage;
import org.fudaa.dodico.corba.mascaret.SParametresTemp;
import org.fudaa.dodico.corba.mascaret.SParametresTraceur;
import org.fudaa.dodico.corba.mascaret.SParametresVarCalc;
import org.fudaa.dodico.corba.mascaret.SParametresVarStock;
import org.fudaa.dodico.corba.mascaret.SParametresZoneSeche;
import org.fudaa.dodico.corba.mascaret.SParametresZoneStockage;
import org.fudaa.dodico.corba.mascaret.SParametresZonesCalageAuto;
import org.fudaa.dodico.corba.mascaret.SPtCasier;
import org.fudaa.dodico.corba.mascaret.SResultatBief;
import org.fudaa.dodico.corba.mascaret.SResultatPasTemps;
import org.fudaa.dodico.corba.mascaret.SResultatSection;
import org.fudaa.dodico.corba.mascaret.SResultatsLIS;
import org.fudaa.dodico.corba.mascaret.SResultatsOPT;
import org.fudaa.dodico.corba.mascaret.SResultatsRUB;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatialBief;
import org.fudaa.dodico.corba.mascaret.SResultatsVariable;
import org.fudaa.dodico.hydraulique1d.Hydraulique1dResource;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.EnumMetierMethodeOpt;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.EnumMetierTypeLit;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierApportCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierMesureCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierParametresCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierCondLimiteImposee;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierCritereArret;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierLimiteCalcule;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierMethodeMaillage;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeFrottement;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeNombre;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierUnite;
import org.fudaa.dodico.hydraulique1d.metier.MetierBarragePrincipal;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierConditionsInitiales;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSections;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeries;
import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauPoint;
import org.fudaa.dodico.hydraulique1d.metier.MetierLimite;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierMaillage;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGenerauxCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresReprise;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresResultats;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresTemporels;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatialBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierSite;
import org.fudaa.dodico.hydraulique1d.metier.MetierZone;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;
import org.fudaa.dodico.hydraulique1d.metier.MetierZonePlanimetrage;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierGeometrieCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierPlanimetrageCasier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimniHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimnigramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiOuvertureVanne;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiRegulation;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierModeleQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierOptionConvec;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierOptionDiffus;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierTypeCondLimiteTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierConcentrationInitiale;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierLimiteQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierOptionTraceur;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParamPhysTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierTypeSource;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierBarrage;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementZCoefQ;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilDenoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLimniAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilNoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAval;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilVanne;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSource;
import org.fudaa.dodico.mascaret.DescriptionVariables;

/**
 * Classe qui contient les m�thodes statiques permettant de convertir le mod�le m�tier hydraulique1D (les param�tres) en mod�le mascaret.
 *
 * @version $Revision: 1.2 $ $Date: 2007-11-20 11:43:06 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class ConvH1D_Masc {

  public static final double RIEN = Double.NaN;
  public static final int IRIEN = Integer.MIN_VALUE;
  static MetierLoiHydraulique[] LoisSupplementaires_ = new MetierLoiHydraulique[0];

  final static SParametresCAS convertirParametresCas(boolean _calage, MetierEtude1d etude, int numConnexion, boolean xcas) {
    String nomFichierSansExtension = "mascaret" + numConnexion;
    return convertirParametresCas(_calage, etude, nomFichierSansExtension, xcas);
  }

  public static final String getS(final String _s) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(_s);
  }

  public static final SParametresCAS convertirParametresCas(
          boolean _calage,
          MetierEtude1d etude,
          String nomFichierSansExtensionInit, boolean xcas) {
    try {
      //on commence par trier les lois en placant les lois geometriques (profil de crete) � la fin
      etude.donneesHydro().initIndiceLois();
      SParametresCAS paramCas = new SParametresCAS();
      boolean presenceCasier = etude.reseau().casiers().length > 0;
      if (etude.paramGeneraux().parametresCasier() != null) {
        presenceCasier = presenceCasier && etude.paramGeneraux().
                parametresCasier().activation();
      }
      String nomFichierSansExtension = nomFichierSansExtensionInit;
      if (xcas) {
        File file = new File(nomFichierSansExtension);
        if (file.isAbsolute()) {
          nomFichierSansExtension = file.getName();
        }
      }
      try {
        paramCas.parametresGen
                = convertirParametresGen(
                        etude.paramGeneraux().regime(),
                        presenceCasier,
                        nomFichierSansExtension,
                        etude.paramGeneraux().noyauV5P2(), xcas);
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres g�n�raux") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresModelPhy
                = convertirParametresModelPhy(etude.paramGeneraux());
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres de la mod�lisation physique") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresNum
                = convertirParametresNum(etude.paramGeneraux(), presenceCasier);
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres num�riques") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresTemp = convertirParametresTemp(etude.paramTemps());
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres temporels") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresGeoReseau
                = convertirParametresGeoReseau(
                        etude.reseau(),
                        etude.donneesHydro(),
                        etude.paramGeneraux().profilsAbscAbsolu(),
                        nomFichierSansExtension);
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres de la g�om�trie et du r�seau") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresConfluents
                = convertirParametresConfluents(etude.reseau().noeudsConnectesBiefs());
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres des confluents") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresPlanimMaillage
                = convertirParametresPlanimMaillage(
                        etude.reseau(),
                        etude.paramGeneraux().maillage());
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres du planim�trage et du maillage") + "\n"
                + ex.getLocalizedMessage());
      }
      try { //les singularit�s doivent etre converties avant les lois hydrauliques car il faut creer une loi hydrogramme pour les seuils transcritiques
        paramCas.parametresSingularite
                = convertirParametresSingularite(
                        etude.reseau(),
                        etude.paramGeneraux(),
                        etude.donneesHydro(),
                        etude.paramTemps());

      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(
                getS("Conversion des param�tres des singularit�es") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresCasier
                = convertirParametresCasier(etude.reseau(), nomFichierSansExtension);
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres casier") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresApporDeversoirs
                = convertirParametresApporDeversoirs(etude.paramGeneraux().noyauV5P2(), etude.reseau(),
                        etude.donneesHydro());
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres des apports et d�versoirs") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresCalage
                = convertirParametresCalage(
                        etude.reseau(),
                        etude.paramGeneraux().typeFrottement(),etude.paramGeneraux().typeCoefficient());
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres de calage") + "\n"
                + ex.getLocalizedMessage());
      }
      // Parametres calage auto.
      try {
        paramCas.parametresCalageAuto = convertirParametresCalageAuto(
                _calage,
                nomFichierSansExtension,
                etude.calageAuto().parametres(),
                etude.calageAuto().zonesFrottement(),
                etude.calageAuto().crues()
        );
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres de calage automatique") + "\n"
                + ex.getLocalizedMessage());
      }
      //Parametres qualit� d'eau.
      try {
        paramCas.parametresTracer = convertirParametresTracer(
                nomFichierSansExtension,
                etude.qualiteDEau(),
                etude.paramResultats(),
                etude.reseau(),
                etude.donneesHydro()
        );
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres de la qualit� d'eau") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresLoisHydrau
                = convertirParametresLoisHydrau(etude.donneesHydro().getToutesLoisSaufTracerEtGeometrique(),
                        nomFichierSansExtension);
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres des lois hydrauliques") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresCondInit
                = convertirParametresCondInit(
                        etude.donneesHydro().conditionsInitiales(),
                        nomFichierSansExtension,
                        etude.reseau());
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres de la condition initiale") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresImpressResult
                = convertirParametresImpressResult(
                        etude.paramResultats(),
                        etude.description(),
                        nomFichierSansExtension,
                        etude.reseau(),
                        presenceCasier);
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres d'impression et de r�sultats") + "\n"
                + ex.getLocalizedMessage());
      }
      try {
        paramCas.parametresVarCalc = convertirParametresVarCalc();
        paramCas.parametresVarStock = convertirParametresVarStock(
                etude.paramResultats().variables());
      } catch (Throwable ex) {
        ex.printStackTrace();
        throw new RuntimeException(getS("Conversion des param�tres des variables � calculer ou de stockage") + "\n"
                + ex.getLocalizedMessage());
      }
      return paramCas;
    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new RuntimeException(getS("Probl�me lors de la conversion en fichier cas") + "\n"
              + ex.getLocalizedMessage());
    }
  }

  private static final SParametresVarStock convertirParametresVarStock(MetierDescriptionVariable[] varRes) {
    boolean[] varStock = new boolean[42];
    for (int i = 0; i < 42; i++) {
      varStock[i] = false;
    }
    for (int i = 0; i < varRes.length; i++) {
      int indiceVar = DescriptionVariables.getIndiceStock(varRes[i].nom());
      if ((indiceVar >= 0) && (indiceVar < varStock.length)) {
        varStock[indiceVar] = true;
      }
    }
    return new SParametresVarStock(varStock);
  }

  private final static SParametresGen convertirParametresGen(
          EnumMetierRegime regime,
          boolean presenceCasier,
          String nomFichierSansExtensionInit,
          boolean isNoyauV5P2, boolean xcas) {
    SParametresGen paramMas = new SParametresGen();
    String nomFichierSansExtension = nomFichierSansExtensionInit;
    // VERSION DU CODE
    // A partir de la version 7.0 de Mascaret,
    // le mot cl� "VERSION DU CODE" vaut 3 (avant la 7.0, il vaut 2)
    if (isNoyauV5P2) {
      paramMas.versionCode = 2;
    } else {
      paramMas.versionCode = 3;
    }
    // noyau code : 1->SARAP, 2->REZO, 3->MASCARET
    paramMas.code = regime.value() + 1;
    // nom du fichier dictionnaire
    paramMas.dictionaire = "dico.txt";
    // nom de fichier du programme principale
    paramMas.progPrincipal = "princi.f";
    // nom du fichier cas (des mots cles)
    paramMas.fichMotsCles = nomFichierSansExtension + (xcas ? ".xcas" : ".cas");
    // sauvegarde du modele
    paramMas.sauveModele = false;
    // nom du fichier de sauvegarde du mod�le (temporaire)
    paramMas.fichSauvModele = nomFichierSansExtension + ".tmp";
    // calcul pour validation du code
    paramMas.validationCode = false;
    // type de calcul de validation effectue
    paramMas.typeValidation = 1;
    // presence de casiers
    paramMas.presenceCasiers = presenceCasier;
    // bibliotheques utilis�es
    String os = System.getProperty("os.name");
    if (os.startsWith("Windows")) {
      paramMas.bibliotheques
              = new SParametresBiblio("mascaretV5P1.a damoV3P0.a");
    } else {
      paramMas.bibliotheques = null;
    }
    return paramMas;
  }

  private final static SParametresModelPhy convertirParametresModelPhy(MetierParametresGeneraux iparamGen) {
    SParametresModelPhy paramMas = new SParametresModelPhy();
    // pertes de charge automatique aux confluents
    paramMas.perteChargeConf = iparamGen.perteChargeConfluents();
    // composition des lits : 1->DEBORD, 2->FOND/BERGE
    paramMas.compositionLits = iparamGen.compositionLits().value() + 1;
    // conservation du frottement sur les parois verticales
    paramMas.conservFrotVertical = iparamGen.frottementsParois();
    // elevation de cote arrivee du front
    paramMas.elevCoteArrivFront = iparamGen.elevationCoteArriveeFront();
    // interpolation lin�aire des strickler
    paramMas.interpolLinStrickler
            = iparamGen.interpolationLineaireCoefFrottement();
    // les parametres du debordement progressif
    paramMas.debordement
            = new SParametresDebordProgr(
                    iparamGen.debordProgressifLitMajeur(),
                    iparamGen.debordProgressifZoneStockage());
    return paramMas;
  }

  private final static SParametresNum convertirParametresNum(
          MetierParametresGeneraux iparamGen,
          boolean presenceCasier) {
    SParametresNum paramMas = new SParametresNum();
    // calcul d'une onde de submersion => "BARRAGE PRINCIPAL"
    paramMas.calcOndeSubmersion = iparamGen.ondeSubmersion();
    // Froude limite pour les conditions limites si code Mascaret
    paramMas.froudeLimCondLim = iparamGen.froudeLimConditionLimite();
    // traitement implicite du frottement
    paramMas.traitImplicitFrot = iparamGen.traitementImpliciteFrottements();
    // hauteur d'eau minimale
    paramMas.hauteurEauMini = iparamGen.hauteurEauMinimal();
    // implicitation du noyau transcritique
    paramMas.implicitNoyauTrans = iparamGen.implicitationNoyauTrans();
    // optimisation du noyau transcritique
    paramMas.optimisNoyauTrans = iparamGen.optimisationNoyauTrans();
    // perte de charge automatique en cas d'�largissement
    paramMas.perteChargeAutoElargissement = iparamGen.perteChargeAutoElargissement();
    paramMas.termesNonHydrostatiques = iparamGen.termesNonHydrostatiques();
    paramMas.apportDebit = iparamGen.apportDebit() ? 1 : 0;
    paramMas.attenuationConvection = iparamGen.attenuationConvection();
    return paramMas;
  }

  private final static SParametresTemp convertirParametresTemp(MetierParametresTemporels iparamTemp) {
    SParametresTemp paramMas = new SParametresTemp();
    // pas de temps
    paramMas.pasTemps = iparamTemp.pasTemps();
    // temps initial
    paramMas.tempsInit = iparamTemp.tempsInitial();
    // crit�re d'arret du calcul :1-> temps max, 2-> nb temps max, 3->Cote max
    paramMas.critereArret = iparamTemp.critereArret().value() + 1;
    // nombre de pas de temps Si critereArret==2
    paramMas.nbPasTemps = iparamTemp.nbPasTemps();
    // temps maximum Si critereArret==1
    paramMas.tempsMax = iparamTemp.tempsFinal();
    // cote max de controle Si critereArret==3
    paramMas.coteMax = iparamTemp.coteMax();
    // abscisse  du point de controle
    paramMas.abscisseControle = iparamTemp.abscisseControle();
    // numero de bief du point de controle
    paramMas.biefControle = iparamTemp.biefControle();
    // pas de temps variable suivant nombre de courant
    paramMas.pasTempsVar = iparamTemp.pasTempsVariable();
    // nombre de courant souhait�
    paramMas.nbCourant = iparamTemp.nbCourant();
    return paramMas;
  }

  private final static SParametresGeoReseau convertirParametresGeoReseau(
          MetierReseau ireseau,
          MetierDonneesHydrauliques idonnesHydrau,
          boolean profilAbsAbsolu,
          String nomFichierSansExtension) {
    SParametresGeoReseau paramMas
            = new SParametresGeoReseau(
                    new SParametresGeom(),
                    new SParametresBranches(),
                    new SParametresNoeuds(),
                    new SParametresExtrLibres());
    paramMas.geometrie.format = 2; //2->mascaretV5
    paramMas.geometrie.profilsAbscAbsolu = profilAbsAbsolu;
    paramMas.geometrie.fichier = nomFichierSansExtension + ".geo";
    //nom du fichier geometrie
    paramMas.branches.nb = ireseau.biefs().length;
    paramMas.branches.numeros = new int[paramMas.branches.nb];
    paramMas.branches.abscDebut = new double[paramMas.branches.nb];
    paramMas.branches.abscFin = new double[paramMas.branches.nb];
    paramMas.branches.numExtremDebut = new int[paramMas.branches.nb];
    paramMas.branches.numExtremFin = new int[paramMas.branches.nb];
    for (int i = 0; i < ireseau.biefs().length; i++) {
      paramMas.branches.numeros[i] = i + 1;
      paramMas.branches.abscDebut[i]
              = ireseau.biefs()[i].extrAmont().profilRattache().abscisse();
      paramMas.branches.abscFin[i]
              = ireseau.biefs()[i].extrAval().profilRattache().abscisse();
      paramMas.branches.numExtremDebut[i]
              = ireseau.biefs()[i].extrAmont().numero();
      paramMas.branches.numExtremFin[i] = ireseau.biefs()[i].extrAval().numero();
    }
    MetierNoeud[] inoeudsConnectesBiefs = ireseau.noeudsConnectesBiefs();
    paramMas.noeuds.nb = inoeudsConnectesBiefs.length;
    paramMas.noeuds.noeuds = new SNoeud[paramMas.noeuds.nb];
    for (int i = 0; i < inoeudsConnectesBiefs.length; i++) {
      paramMas.noeuds.noeuds[i] = new SNoeud();
      paramMas.noeuds.noeuds[i].num = new int[5];
      for (int j = 0; j < inoeudsConnectesBiefs[i].extremites().length; j++) {
        paramMas.noeuds.noeuds[i].num[j]
                = inoeudsConnectesBiefs[i].extremites()[j].numero();
      }
    }
    MetierExtremite[] extremLibreHydr = ireseau.extremitesLibres();
    paramMas.extrLibres.nb = extremLibreHydr.length;
    paramMas.extrLibres.num = new int[extremLibreHydr.length];
    paramMas.extrLibres.numExtrem = new int[extremLibreHydr.length];
    paramMas.extrLibres.Nom = new String[extremLibreHydr.length];
    paramMas.extrLibres.typeCond = new int[extremLibreHydr.length];
    paramMas.extrLibres.numLoi = new int[extremLibreHydr.length];
    for (int i = 0; i < extremLibreHydr.length; i++) {
      paramMas.extrLibres.num[i] = i + 1;
      paramMas.extrLibres.numExtrem[i] = extremLibreHydr[i].numero();
      paramMas.extrLibres.Nom[i] = extremLibreHydr[i].conditionLimite().nom();
      paramMas.extrLibres.typeCond[i]
              = getType(extremLibreHydr[i].conditionLimite());
      if (extremLibreHydr[i].conditionLimite().loi() != null) {
        paramMas.extrLibres.numLoi[i]
                = idonnesHydrau.getIndiceLoi(
                        extremLibreHydr[i].conditionLimite().loi())
                + 1;
      } else {
        paramMas.extrLibres.numLoi[i] = 1;
      }
    }
    return paramMas;
  }

  /**
   * @param limite DLimite
   * @return int
   */
  private final static int getType(MetierLimite limite) {
    if (limite.loi() == null) { // limite libre ou hauteur normale
      if (limite.typeLimiteCalcule().value()
              == EnumMetierLimiteCalcule._EVACUATION_LIBRE) {
        return 6;
      } else {
        return 7; // hauteur normale
      }
    }
    if (limite.loi() instanceof MetierLoiHydrogramme) {
      return 1; // debit impose
    }
    if (limite.loi() instanceof MetierLoiLimniHydrogramme) {
      if (limite.condLimiteImposee().value() == EnumMetierCondLimiteImposee._COTE) {
        return 2; // cote impose
      } else if (limite.condLimiteImposee().value() == EnumMetierCondLimiteImposee._DEBIT) {
        return 1; // debit impose
      } else {
        return 8; //cote et d�bit impose
      }
    }
    if (limite.loi() instanceof MetierLoiLimnigramme) {
      return 2; // cote impose
    }
    if (limite.loi() instanceof MetierLoiTarage) {
      MetierLoiTarage l = (MetierLoiTarage) limite.loi();
      if (l.amont()) {
        return 3; // Z(Q)
      } else {
        return 4; // Q(Z)
      }
    }
    if (limite.loi() instanceof MetierLoiRegulation) {
      return 5; // 5-> regulation : Zav(Qam)
    }
    return 0; // normalement impossible
  }

  private final static SParametresConfluents convertirParametresConfluents(MetierNoeud[] noeuds) {
    SParametresConfluents paramMas = new SParametresConfluents();
    paramMas.nbConfluents = noeuds.length;
    paramMas.confluents = new SParametresConfluent[noeuds.length];
    for (int i = 0; i < noeuds.length; i++) {
      paramMas.confluents[i] = new SParametresConfluent();
      paramMas.confluents[i].nom = "Noeud" + noeuds[i].numero();
      paramMas.confluents[i].nbAffluent = noeuds[i].extremites().length;
      paramMas.confluents[i].abscisses
              = new double[noeuds[i].extremites().length];
      paramMas.confluents[i].ordonnees
              = new double[noeuds[i].extremites().length];
      paramMas.confluents[i].angles = new double[noeuds[i].extremites().length];
      for (int j = 0; j < noeuds[i].extremites().length; j++) {
        if (noeuds[i].extremites()[j].pointMilieu() != null) {
          paramMas.confluents[i].abscisses[j]
                  = noeuds[i].extremites()[j].pointMilieu().x;
          paramMas.confluents[i].ordonnees[j]
                  = noeuds[i].extremites()[j].pointMilieu().y;
          paramMas.confluents[i].angles[j] = noeuds[i].extremites()[j].angle();
        }
      }
    }
    return paramMas;
  }

  /**
   * Conversion des donn�es de la qualit� d'eau.
   *
   * @return La structure qualit� d'eau.
   */
  private final static SParametresTraceur convertirParametresTracer(
          String _nomFichierSansExtension,
          MetierParametresQualiteDEau _params,
          MetierParametresResultats _res,
          MetierReseau _ireseau,
          MetierDonneesHydrauliques _idonnesHydrau
  ) {
    if (!_params.parametresModeleQualiteEau().presenceTraceurs()) {
      return null; // Structure nulle si pas de qualit� d'eau.
    }
    if (_params.parametresModeleQualiteEau().nbTraceur() == 0) {
      return null; // Structure nulle si pas de traceur.
    }
    SParametresTraceur r = new SParametresTraceur();

    //Params g�n�raux
    r.presenceTraceurs = _params.parametresModeleQualiteEau().presenceTraceurs();
    int nbTraceurs = _params.parametresModeleQualiteEau().nbTraceur();
    r.nbTraceur = nbTraceurs;

    //Params Diff-Conv
    SParametresConvecDiffu paramsDiffCon = new SParametresConvecDiffu();

    paramsDiffCon.convectionTraceurs = new boolean[nbTraceurs];
    paramsDiffCon.diffusionTraceurs = new boolean[nbTraceurs];
    for (int i = 0; i < nbTraceurs; i++) {
      MetierOptionTraceur o = _params.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers()[i];
      paramsDiffCon.convectionTraceurs[i] = o.convectionDuTraceur();
      paramsDiffCon.diffusionTraceurs[i] = o.diffusionDuTraceur();
    }

    //par defaut
    paramsDiffCon.ordreSchemaConvec = IRIEN;
    paramsDiffCon.paramW = RIEN;
    paramsDiffCon.LimitPente = false;

    if (_params.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionConvection().equals(EnumMetierOptionConvec.HYP1FA_NON_CONS)) {
      paramsDiffCon.optionConvection = 2;
    } else if (_params.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionConvection().equals(EnumMetierOptionConvec.HYP1FA_CONS)) {
      paramsDiffCon.optionConvection = 3;
    } else {//Volumes finis
      paramsDiffCon.optionConvection = 4;
      paramsDiffCon.ordreSchemaConvec = _params.parametresGenerauxQualiteDEau().parametresConvecDiffu().ordreSchemaConvec();

      if (_params.parametresGenerauxQualiteDEau().parametresConvecDiffu().ordreSchemaConvec() == 2) {
        paramsDiffCon.paramW = _params.parametresGenerauxQualiteDEau().parametresConvecDiffu().paramW();
      }

      if (_params.parametresGenerauxQualiteDEau().parametresConvecDiffu().ordreSchemaConvec() != 1) {
        paramsDiffCon.LimitPente = _params.parametresGenerauxQualiteDEau().parametresConvecDiffu().LimitPente();
      }
    }
    /**
     * OPTION DE CALCUL DE LA DISPERSION POUR LES TRACEURS les choix sont : 1 : Coefficient de diffusion constant 2 : Elder (1959) 3 : Fisher (1975) 4
     * : Liu (1977) 5 : Iwasa et Aya (1991) 6 : McQuivey et Keefer (1974) 7 : Kashefipur et Falconer (2002 ) 8 : Magazine et al. (1988) 9 : Koussis et
     * Rodriguez-Mirasol (1998) 10: Seo et Cheong (1998) 11: Deng et al. (2001)
     */
    EnumMetierOptionDiffus optionDiffusion = _params.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion();
    if (optionDiffusion.equals(EnumMetierOptionDiffus.K_C1U_C2)) { // Coefficient de diffusion constant
      paramsDiffCon.optionCalculDiffusion = 1;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.ELDER)) {
      paramsDiffCon.optionCalculDiffusion = 2;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.FISHER)) {
      paramsDiffCon.optionCalculDiffusion = 3;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.LIU)) {
      paramsDiffCon.optionCalculDiffusion = 4;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.ISAWA_AGA)) {
      paramsDiffCon.optionCalculDiffusion = 5;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.MC_QUIVEY_KEEFER)) {
      paramsDiffCon.optionCalculDiffusion = 6;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.KASHEFIPUR_FALCONER)) {
      paramsDiffCon.optionCalculDiffusion = 7;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.MAGAZINE_AL)) {
      paramsDiffCon.optionCalculDiffusion = 8;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.KOUSSIS_RODRIGUEZ)) {
      paramsDiffCon.optionCalculDiffusion = 9;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.SEO_CHEONG)) {
      paramsDiffCon.optionCalculDiffusion = 10;
    } else if (optionDiffusion.equals(EnumMetierOptionDiffus.DENG_AL)) {
      paramsDiffCon.optionCalculDiffusion = 11;
    }

    paramsDiffCon.coeffDiffusion1 = _params.parametresGenerauxQualiteDEau().parametresConvecDiffu().coeffDiffusion1();
    paramsDiffCon.coeffDiffusion2 = _params.parametresGenerauxQualiteDEau().parametresConvecDiffu().coeffDiffusion2();

    r.parametresConvecDiffu = paramsDiffCon;

    //Params NumQualiteEau
    SParametresNumQualiteEau paramsNumQualiteEau = new SParametresNumQualiteEau();

    if (_params.parametresModeleQualiteEau().modeleQualiteEau().equals(EnumMetierModeleQualiteDEau.O2)) {
      paramsNumQualiteEau.modeleQualiteEau = 2;
    } else if (_params.parametresModeleQualiteEau().modeleQualiteEau().equals(EnumMetierModeleQualiteDEau.BIOMASS)) {
      paramsNumQualiteEau.modeleQualiteEau = 3;
    } else if (_params.parametresModeleQualiteEau().modeleQualiteEau().equals(EnumMetierModeleQualiteDEau.EUTRO)) {
      paramsNumQualiteEau.modeleQualiteEau = 4;
    } else if (_params.parametresModeleQualiteEau().modeleQualiteEau().equals(EnumMetierModeleQualiteDEau.MICROPOL)) {
      paramsNumQualiteEau.modeleQualiteEau = 5;
    } else if (_params.parametresModeleQualiteEau().modeleQualiteEau().equals(EnumMetierModeleQualiteDEau.THERMIC)) {
      paramsNumQualiteEau.modeleQualiteEau = 6;
    } else { //EnumMetierModeleQualiteDEau.TRANSPORT_PUR
      paramsNumQualiteEau.modeleQualiteEau = 1;
    }
    paramsNumQualiteEau.frequenceCouplHydroTracer = _params.parametresModeleQualiteEau().frequenceCouplHydroTracer();
    if (_params.parametresGenerauxQualiteDEau().paramMeteoTracer() != null) {
      paramsNumQualiteEau.fichMeteoTracer = _nomFichierSansExtension + ".met";
    }
    if (_params.parametresGenerauxQualiteDEau().paramsPhysTracer() != null) {
      paramsNumQualiteEau.fichParamPhysiqueTracer = _nomFichierSansExtension + ".phy";
    }

    r.parametresNumQualiteEau = paramsNumQualiteEau;

    //Params ImpressResultTracer
    SParametresImpressResultTracer paramsImpressResultTracer = new SParametresImpressResultTracer();
    paramsImpressResultTracer.fichListTracer = _nomFichierSansExtension + ".tra_lis";

    if (_res.postOpthycaQualiteDEau()) {
      paramsImpressResultTracer.formatFichResultat = 2;
      paramsImpressResultTracer.fichResultTracer = _nomFichierSansExtension + ".tra_opt";
    } else {//Rubens
      paramsImpressResultTracer.formatFichResultat = 1;
      paramsImpressResultTracer.fichResultTracer = _nomFichierSansExtension + ".tra_rub";
    }
    paramsImpressResultTracer.concentInit = _res.optionsListingTracer().concentInit();
    paramsImpressResultTracer.loiTracer = _res.optionsListingTracer().loiTracer();
    paramsImpressResultTracer.bilanTracer = _res.optionsListingTracer().bilanTracer();
    paramsImpressResultTracer.concentrations = _res.optionsListingTracer().concentrations();

    r.parametresImpressTracer = paramsImpressResultTracer;

    //Params CondLimTracer
    SParametresCondLimTracer paramsCondLimTracer = new SParametresCondLimTracer();
    MetierExtremite[] _extermites = _ireseau.extremitesLibres();
    int nbExtremiteLibreQE = _extermites.length;
    paramsCondLimTracer.typeCondLimTracer = new int[nbExtremiteLibreQE];
    paramsCondLimTracer.numLoiCondLimTracer = new int[nbExtremiteLibreQE];
    for (int i = 0; i < nbExtremiteLibreQE; i++) {
      MetierLimiteQualiteDEau l = _extermites[i].conditionLimiteQualiteDEau();
      //typeCondLimiteTracer
      if (l.typeCondLimiteTracer().equals(EnumMetierTypeCondLimiteTracer.NEUMANN)) {
        paramsCondLimTracer.typeCondLimTracer[i] = 1;
      } else {
        paramsCondLimTracer.typeCondLimTracer[i] = 2;
      }
      //numLoiCondLimTracer
      if (l.loi() != null) {
        paramsCondLimTracer.numLoiCondLimTracer[i]
                = _idonnesHydrau.getIndiceLoiTracer((MetierLoiTracer) l.loi()) + 1;
      }
    }
    r.parametresCondLimTracer = paramsCondLimTracer;

    //Params ConcInitTracer
    SParametresConcInitTracer paramsConcInitTracer = new SParametresConcInitTracer();

    paramsConcInitTracer.modeEntree = 1;

    if (_params.concentrationsInitiales() != null && _params.concentrationsInitiales().length > 0) {

      paramsConcInitTracer.presenceConcInit = true;
      paramsConcInitTracer.fichConcInit = _nomFichierSansExtension + ".conc";

      //en cas d'entr�e par clavier
		  /*paramsConcInitTracer.nbPts = _params.concentrationsInitiales().length;
       int ntPtsConc = _params.concentrationsInitiales().length;
       paramsConcInitTracer.branche = new int[ntPtsConc];
       paramsConcInitTracer.abscisse = new double[ntPtsConc];
       paramsConcInitTracer.concentrations = new SParametresConcentrations[ntPtsConc];

       for (int i = 0; i < _params.concentrationsInitiales().length; i++) {
       MetierConcentrationInitiale conc = _params.concentrationsInitiales()[i];
       paramsConcInitTracer.branche[i] = conc.numeroBief();
       paramsConcInitTracer.abscisse[i] = conc.abscisse();
       paramsConcInitTracer.concentrations[i] = new SParametresConcentrations();
       paramsConcInitTracer.concentrations[i].concentrations = conc.concentrations();
       }
       */
    } else { //Pas de concentrations initiales
      paramsConcInitTracer.presenceConcInit = false;
    }
    r.parametresConcInitTracer = paramsConcInitTracer;

    //Params SourcesTraceurs
    SParametresSourcesTraceurs paramsSourcesTraceurs = new SParametresSourcesTraceurs();
    ArrayList _sources = new ArrayList();
    int nbSources = _ireseau.sources().length;

    paramsSourcesTraceurs.nbSources = nbSources;
    paramsSourcesTraceurs.noms = new String[nbSources];
    paramsSourcesTraceurs.typeSources = new int[nbSources];
    paramsSourcesTraceurs.numBranche = new int[nbSources];
    paramsSourcesTraceurs.abscisses = new double[nbSources];
    paramsSourcesTraceurs.longueurs = new double[nbSources];
    paramsSourcesTraceurs.numLoi = new int[nbSources];

    //Construction d'un tableau contenant le numero de bief pour chaque source
    ArrayList numBrancheQSource = new ArrayList();
    for (int i = 0; i < _ireseau.biefs().length; i++) {
      MetierSource[] sourcesHydr = _ireseau.biefs()[i].sources();
      for (int j = 0; j < sourcesHydr.length; j++) {
        numBrancheQSource.add(
                new Integer(_ireseau.getIndiceBief(_ireseau.biefs()[i]) + 1));
        _sources.add(sourcesHydr[j]);
      }
    }

    for (int i = 0; i < nbSources; i++) {
      MetierSource s = (MetierSource) _sources.get(i);
      paramsSourcesTraceurs.noms[i] = s.nom();
      if (s.type().equals(EnumMetierTypeSource.FLUX_UNITE_TEMPS)) {
        paramsSourcesTraceurs.typeSources[i] = 3;
      } else if (s.type().equals(EnumMetierTypeSource.SURFACIQUE)) {
        paramsSourcesTraceurs.typeSources[i] = 2;
      } else {//volumique
        paramsSourcesTraceurs.typeSources[i] = 1;
      }
      paramsSourcesTraceurs.numBranche[i] = ((Integer) numBrancheQSource.get(i)).intValue();;
      paramsSourcesTraceurs.abscisses[i] = s.abscisse();
      paramsSourcesTraceurs.longueurs[i] = s.longueur();
      paramsSourcesTraceurs.numLoi[i] = _idonnesHydrau.getIndiceLoiTracer(s.loi()) + 1;

    }

    r.parametresSourcesTraceurs = paramsSourcesTraceurs;

    //Params LoisTracer
    SParametresLoisTracer paramsLoisTracer = new SParametresLoisTracer();
    MetierLoiTracer[] loisTraceur = _idonnesHydrau.getLoisTracer();
    int nbLoisTracer = loisTraceur.length;

    paramsLoisTracer.nbLoisTracer = nbLoisTracer;
    paramsLoisTracer.loisTracer = new SParametresLoiTracer[nbLoisTracer];

    for (int i = 0; i < loisTraceur.length; i++) {
      MetierLoiTracer l = (MetierLoiTracer) loisTraceur[i];
      paramsLoisTracer.loisTracer[i] = new SParametresLoiTracer();
      paramsLoisTracer.loisTracer[i].nom = l.nom().trim().replace(' ', '_');
      //entr�e par fichier
      paramsLoisTracer.loisTracer[i].modeEntree = 1;
      paramsLoisTracer.loisTracer[i].fichier = _nomFichierSansExtension + "_tracer" + i + ".loi";
      paramsLoisTracer.loisTracer[i].nbPoints = IRIEN;
      paramsLoisTracer.loisTracer[i].uniteTps = IRIEN;

    }

    r.parametresLoisTracer = paramsLoisTracer;

    return r;
  }

  /**
   * Conversion des donn�es calage automatique.
   *
   * @param _calage La conversion s'effectue pour un mode calage.
   * @return La structure calage automatique.
   */
  private final static SParametresCalageAuto convertirParametresCalageAuto(
          boolean _calage, String _nomFichierSansExtension,
          MetierParametresCalageAuto _params, MetierZoneFrottement[] _zones,
          MetierCrueCalageAuto[] _crues) {

    if (!_calage) {
      return null; // Structure nulle si pas de calage.
    }
    SParametresCalageAuto r = new SParametresCalageAuto();

    // Param�tres g�n�raux
    SParametresParamsCalageAuto pars = new SParametresParamsCalageAuto();
    /*
    if (_params.methodeOpt().equals(EnumMetierMethodeOpt.DESCENTE_OPTIMALE)) {
      pars.methOptimisation = 1;
    } else if (_params.methodeOpt().equals(EnumMetierMethodeOpt.CASIER_NEWTON)) {
      pars.methOptimisation = 2;
    } else {
      pars.methOptimisation = 3;
    }*/
    pars.modeCalageAuto = _calage;
    pars.nbMaxIterations = _params.nbMaxIterations();
    pars.nomFichListing = _nomFichierSansExtension + ".cal_lis";
    pars.nomFichResult = _nomFichierSansExtension + ".cal_opt";
    //pars.pasGradient = _params.pasGradient();
    pars.precision = _params.precision();
    //pars.roInit = _params.roInit();
    if (_params.typeLit().equals(EnumMetierTypeLit.MINEUR)) {
    	pars.typeLit = 1;
    } else
    	if (_params.typeLit().equals(EnumMetierTypeLit.MAJEUR)) {
    		pars.typeLit = 2;
    	}
    	else {
    		pars.typeLit = 3;
    	}
    r.parametres = pars;

    // Zones de frottement initiaux
    SParametresZonesCalageAuto zones = new SParametresZonesCalageAuto();
    zones.nbZones = _zones.length;
    zones.absDebZone = new double[_zones.length];
    zones.absFinZone = new double[_zones.length];
    zones.coefLitMin = new double[_zones.length];
    zones.coefLitMaj = new double[_zones.length];
   
    //TOBEREPLACE    
    zones.coefLitMajBinf = new double[_zones.length];
    zones.coefLitMajBsup = new double[_zones.length];
    zones.coefLitMinBinf = new double[_zones.length];
    zones.coefLitMinBsup = new double[_zones.length];

    
    for (int i = 0; i < _zones.length; i++) {
      zones.absDebZone[i] = _zones[i].abscisseDebut();
      zones.absFinZone[i] = _zones[i].abscisseFin();
      //-- ahadoux - if manning, on exporte l'inverse des coefficients saisis --//
      if(_params.typeCoefficient() != null && _params.typeCoefficient().value() == EnumMetierTypeCoefficient._MANNING) {    	 
    	  if(_zones[i].coefMineur() != 0)
    		  zones.coefLitMin[i] = 1/_zones[i].coefMineur();
    	  if(_zones[i].coefMajeur() != 0)
    		  zones.coefLitMaj[i] = 1/_zones[i].coefMajeur();
    	  //TOBEREPLACE    	  
    	  if(_zones[i].coefLitMajBinf() != 0)
    		  zones.coefLitMajBinf[i] = 1/_zones[i].coefLitMajBinf();
    	  if(_zones[i].coefLitMajBsup() != 0)
    		  zones.coefLitMajBsup[i] = 1/_zones[i].coefLitMajBsup();
    	  if(_zones[i].coefLitMinBinf() != 0)
    		  zones.coefLitMinBinf[i] = 1/_zones[i].coefLitMinBinf();
    	  if(_zones[i].coefLitMinBsup() != 0)
    		  zones.coefLitMinBsup[i] = 1/_zones[i].coefLitMinBsup();    
    		  	  
      }else {
	      zones.coefLitMin[i] = _zones[i].coefMineur();
	      zones.coefLitMaj[i] = _zones[i].coefMajeur();	  
	    //TOBEREPLACE
	      
	      zones.coefLitMajBinf[i] = _zones[i].coefLitMajBinf();
	      zones.coefLitMajBsup[i] = _zones[i].coefLitMajBsup();
	      zones.coefLitMinBinf[i] = _zones[i].coefLitMinBinf();
	      zones.coefLitMinBsup[i] = _zones[i].coefLitMinBsup();
	      
      }
    }
    r.zones = zones;

    // Crues
    SParametresCruesCalageAuto crues = new SParametresCruesCalageAuto();
    crues.nbCrues = _crues.length;
    crues.crues = new SParametresCrueCalageAuto[_crues.length];
    for (int i = 0; i < _crues.length; i++) {
      SParametresCrueCalageAuto crue = new SParametresCrueCalageAuto();
      crue.coteAval = _crues[i].coteAval();
      crue.debitAmont = _crues[i].debitAmont();

      MetierApportCrueCalageAuto[] apports = _crues[i].apports();
      crue.nbApports = apports.length;
      crue.absApports = new double[apports.length];
      crue.debitApports = new double[apports.length];
      for (int j = 0; j < apports.length; j++) {
        crue.absApports[j] = apports[j].abscisse();
        crue.debitApports[j] = apports[j].debit();
      }

      MetierMesureCrueCalageAuto[] mesures = _crues[i].mesures();
      crue.nbMesures = mesures.length;
      crue.absMesures = new double[mesures.length];
      crue.coteMesures = new double[mesures.length];
      crue.pondMesures = new double[mesures.length];
      for (int j = 0; j < mesures.length; j++) {
        crue.absMesures[j] = mesures[j].abscisse();
        crue.coteMesures[j] = mesures[j].cote();
        crue.pondMesures[j] = mesures[j].coefficient();
      }

      crues.crues[i] = crue;
    }
    r.crues = crues;

    return r;
  }

  private final static SParametresPlanimMaillage convertirParametresPlanimMaillage(
          MetierReseau ireseau,
          MetierMaillage imaillage) {
    return new SParametresPlanimMaillage(
            getMethodeMaillage(imaillage),
            getPlanimMas(ireseau),
            getMaillageMas(imaillage, ireseau));
  }

  private final static int getMethodeMaillage(MetierMaillage maillage) {
    // methode de calcul du maillage : 1-> aux profils, 2-> par serie,
    // 3-> section par section, 4->repris de la ligne d'eau initiales,
    // 5-> par serie dont les profils
    int methode = maillage.methode().value();
    switch (methode) {
      case EnumMetierMethodeMaillage._SECTIONS_SUR_PROFILS:
        return 1;
      case EnumMetierMethodeMaillage._SECTIONS_UTILISATEUR:
        return 3;
      case EnumMetierMethodeMaillage._SECTIONS_LIGNE_EAU_INITIALE:
        return 4;
      case EnumMetierMethodeMaillage._SECTIONS_PAR_SERIES:
        MetierDefinitionSectionsParSeries def
                = (MetierDefinitionSectionsParSeries) maillage.sections();
        if (def.surProfils()) {
          return 5;
        } else {
          return 2;
        }
    }
    return 0; // ne doit jamais arriver
  }

  private final static SParametresMaillage getMaillageMas(
          MetierMaillage maillage,
          MetierReseau ireseau) {
    MetierDefinitionSectionsParSeries def;
    SParametresMaillageClavier maillClavMas = new SParametresMaillageClavier();
    int methode = getMethodeMaillage(maillage);
    switch (methode) {
      case 3: // section par section
        MetierDefinitionSectionsParSections defSection
                = (MetierDefinitionSectionsParSections) maillage.sections();
        maillClavMas.nbSections = defSection.unitaires().length;
        maillClavMas.branchesSection = new int[maillClavMas.nbSections];
        maillClavMas.absSection = new double[maillClavMas.nbSections];
        for (int i = 0; i < maillClavMas.nbSections; i++) {
          maillClavMas.absSection[i] = defSection.unitaires()[i].abscisse();
          maillClavMas.branchesSection[i]
                  = ireseau.getIndiceBief(defSection.unitaires()[i].biefRattache()) + 1;
        }
        break;
      case 2: // par serie
        def = (MetierDefinitionSectionsParSeries) maillage.sections();
        maillClavMas.nbZones = def.unitaires().length;
        maillClavMas.numBrancheZone = new int[maillClavMas.nbZones];
        maillClavMas.absDebutZone = new double[maillClavMas.nbZones];
        maillClavMas.absFinZone = new double[maillClavMas.nbZones];
        maillClavMas.nbSectionZone = new int[maillClavMas.nbZones];
        for (int i = 0; i < maillClavMas.nbZones; i++) {
          maillClavMas.numBrancheZone[i]
                  = ireseau.getIndiceBief(def.unitaires()[i].zone().biefRattache()) + 1;
          maillClavMas.absDebutZone[i]
                  = def.unitaires()[i].zone().abscisseDebut();
          maillClavMas.absFinZone[i] = def.unitaires()[i].zone().abscisseFin();
          int nbSection = (int) Math.round((maillClavMas.absFinZone[i] - maillClavMas.absDebutZone[i]) / def.unitaires()[i].pas());
          maillClavMas.nbSectionZone[i] = nbSection - 1;
          //         (int) ((maillClavMas.absFinZone[i] - maillClavMas.absDebutZone[i])
          //           / def.unitaires()[i].pas());
        }
        break;
      case 5: // par serie dont les profils
        def = (MetierDefinitionSectionsParSeries) maillage.sections();
        maillClavMas.nbPlages = def.unitaires().length;
        maillClavMas.num1erProfPlage = new int[maillClavMas.nbPlages];
        maillClavMas.numDerProfPlage = new int[maillClavMas.nbPlages];
        maillClavMas.pasEspacePlage = new double[maillClavMas.nbPlages];
        for (int i = 0; i < maillClavMas.nbPlages; i++) {
          double xDeb = def.unitaires()[i].zone().abscisseDebut();
          MetierBief bief = def.unitaires()[i].zone().biefRattache();
          MetierProfil profDebutPlage
                  = bief.profils()[Math.abs(bief.getIndiceProfilAbscisse(xDeb))];
          //def.unitaires()[i].zone().biefRattache().getIndiceProfilAbscisse(xDeb)
          maillClavMas.num1erProfPlage[i]
                  = ireseau.getNumeroApparitionProfil(profDebutPlage);
          //maillClavMas.num1erProfPlage[i]=def.unitaires()[i].zone().biefRattache().getIndiceProfilAbscisse(xDeb);
          double xFin = def.unitaires()[i].zone().abscisseFin();
          bief = def.unitaires()[i].zone().biefRattache();
          MetierProfil profFinPlage
                  = bief.profils()[Math.abs(bief.getIndiceProfilAbscisse(xFin))];
          maillClavMas.numDerProfPlage[i]
                  = ireseau.getNumeroApparitionProfil(profFinPlage);
          //maillClavMas.numDerProfPlage[i]=def.unitaires()[i].zone().biefRattache().getIndiceProfilAbscisse(xFin);
          maillClavMas.pasEspacePlage[i] = def.unitaires()[i].pas();
        }
        break;
    }
    SParametresMaillage maillMas
            = new SParametresMaillage(2, null, false, null, maillClavMas);
    return maillMas;
  }

  private final static SParametresPlanim getPlanimMas(MetierReseau reseau) {
    SParametresPlanim planim = new SParametresPlanim();
    Vector num1erProf = new Vector();
    Vector numDerProf = new Vector();
    Vector valeursPas = new Vector();
    MetierBief[] biefs = reseau.biefs();
    for (int i = 0; i < biefs.length; i++) {
      MetierZonePlanimetrage[] zonesPlanim = biefs[i].zonesPlanimetrage();
      if (zonesPlanim.length == 0) {
        continue;
      }
      for (int j = 0; j < zonesPlanim.length; j++) {
        MetierZonePlanimetrage p = zonesPlanim[j];
        double taille = p.taillePas();
        double abscDeb = p.abscisseDebut();
        double abscFin = p.abscisseFin();
        int indProfDeb = getIndiceAvalProfil(p, abscDeb);
        int indProfFin = getIndiceAvalProfil(p, abscFin);
        if (j > 0) {
          MetierZonePlanimetrage pPrecedent = zonesPlanim[j - 1];
          if (indProfDeb == getIndiceAvalProfil(pPrecedent, pPrecedent.abscisseFin())) {
            if (indProfFin > indProfDeb) {
              indProfDeb++;
            }
          }
        }
        valeursPas.add(new Double(taille));
        num1erProf.add(
                new Integer(
                        reseau.getNumeroApparitionProfil(
                                p.biefRattache().profils()[indProfDeb])));
        numDerProf.add(
                new Integer(
                        reseau.getNumeroApparitionProfil(
                                p.biefRattache().profils()[indProfFin])));
      }
    }
    planim.nbZones = num1erProf.size();
    planim.num1erProf = new int[planim.nbZones];
    planim.numDerProf = new int[planim.nbZones];
    planim.valeursPas = new double[planim.nbZones];
    for (int i = 0; i < planim.nbZones; i++) {
      planim.num1erProf[i] = ((Integer) num1erProf.get(i)).intValue();
      planim.numDerProf[i] = ((Integer) numDerProf.get(i)).intValue();
      planim.valeursPas[i] = ((Double) valeursPas.get(i)).doubleValue();
    }
    if (reseau.nbPasPlanimetrageImpose()) {
      planim.nbPas = reseau.nbPasPlanimetrage();
    } else {
      planim.nbPas = reseau.getNbPasPlanimetrage();
    }
    return planim;
  }

  private static int getIndiceAvalProfil(MetierZonePlanimetrage p, double abscisse) {
    int indiceProfil = p.biefRattache().getIndiceProfilAbscisse(abscisse);
    if (indiceProfil < 0) {
      indiceProfil = -indiceProfil;
    }
    return indiceProfil;

  }

  private final static SParametresSingularite convertirParametresSingularite(
          MetierReseau ireseau,
          MetierParametresGeneraux iparamGen,
          MetierDonneesHydrauliques idonneesHydrau,
          MetierParametresTemporels iparamsTemps) {
    SParametresSingularite singulariteMas = new SParametresSingularite();

    //initialisation de temps init et max pour creer les lois hydrogrammes des seuils transcritiques
    double tempsInit = iparamsTemps.tempsInitial();
    double tempsMax;
    if (iparamsTemps.critereArret().value() == EnumMetierCritereArret._TEMPS_MAX) {
      tempsMax = iparamsTemps.tempsFinal();
    } else if (iparamsTemps.critereArret().value() == EnumMetierCritereArret._NB_PAS_TEMPS) {
      tempsMax = iparamsTemps.pasTemps() * iparamsTemps.nbPasTemps();
    } else /*cote max*/ {
      tempsMax = 10 * 366 * 24 * 3600 + tempsInit;
    }

    int nbLoi = idonneesHydrau.getToutesLoisSaufGeometrique().length;

    ArrayList listeLoisSupplementaires = new ArrayList();
    Vector numBranchePerteCharge = new Vector();
    Vector abscissePerteCharge = new Vector();
    Vector coefficientPerteCharge = new Vector();
    Vector seuils = new Vector();
    Vector numBrancheSeuil = new Vector();
    for (int i = 0; i < ireseau.biefs().length; i++) {
      MetierPerteCharge[] pertesCharges = ireseau.biefs()[i].pertesCharges();
      for (int j = 0; j < pertesCharges.length; j++) {
        numBranchePerteCharge.addElement(
                new Integer(ireseau.getIndiceBief(ireseau.biefs()[i]) + 1));
        abscissePerteCharge.addElement(new Double(pertesCharges[j].abscisse()));
        coefficientPerteCharge.addElement(
                new Double(pertesCharges[j].coefficient()));
      }
      MetierSeuil[] seuilsHydr = ireseau.biefs()[i].seuils();
      for (int j = 0; j < seuilsHydr.length; j++) {
        seuils.addElement(seuilsHydr[j]);
        numBrancheSeuil.addElement(
                new Integer(ireseau.getIndiceBief(ireseau.biefs()[i]) + 1));
      }
    }
    if (numBranchePerteCharge.size() > 0) {
      singulariteMas.pertesCharges = new SParametresPerteCharge();
      singulariteMas.pertesCharges.nbPerteCharge = numBranchePerteCharge.size();
      int[] numBrancheMas = new int[numBranchePerteCharge.size()];
      double[] abscissesMas = new double[abscissePerteCharge.size()];
      double[] coefficientsMas = new double[coefficientPerteCharge.size()];
      for (int i = 0; i < numBranchePerteCharge.size(); i++) {
        numBrancheMas[i] = ((Integer) numBranchePerteCharge.get(i)).intValue();
        abscissesMas[i] = ((Double) abscissePerteCharge.get(i)).doubleValue();
        coefficientsMas[i]
                = ((Double) coefficientPerteCharge.get(i)).doubleValue();
      }
      singulariteMas.pertesCharges.numBranche = numBrancheMas;
      singulariteMas.pertesCharges.abscisses = abscissesMas;
      singulariteMas.pertesCharges.coefficients = coefficientsMas;
    }
    if (seuils.size() > 0) {
      singulariteMas.nbSeuils = seuils.size();
      singulariteMas.seuils = new SParametresSeuil[singulariteMas.nbSeuils];
      MetierSeuil s;
      for (int i = 0; i < singulariteMas.nbSeuils; i++) {
        SParametresSeuil sMas
                = new SParametresSeuil(null, 0, 0, 0, RIEN, RIEN, RIEN, RIEN, RIEN, IRIEN, IRIEN,
                        null, null, SParametresSeuil.PARAMETRES_SEUIL_DEFAULT_VALUE, RIEN);
        s = (MetierSeuil) seuils.elementAt(i);
        sMas.nom = s.nom();
        sMas.abscisse = s.abscisse();
        sMas.coteRupture = s.coteRupture();
        sMas.numBranche = ((Integer) numBrancheSeuil.elementAt(i)).intValue();
        if (s instanceof MetierSeuilVanne) {
          MetierSeuilVanne v = (MetierSeuilVanne) s;
          sMas.type = 8; // type du seuil : vanne
          sMas.numLoi = idonneesHydrau.getIndiceLoi(v.loi()) + 1;
          sMas.largVanne = v.largeur();
        } else if (s instanceof MetierSeuilNoye) {
          MetierSeuilNoye n = (MetierSeuilNoye) s;
          sMas.type = 1; // type du seuil : Zam=f(Zav,Q)
          sMas.coteCrete = n.coteCrete();
          sMas.numLoi = idonneesHydrau.getIndiceLoi(n.loi()) + 1;
        } else if (s instanceof MetierSeuilTarageAval) {
          MetierSeuilTarageAval t = (MetierSeuilTarageAval) s;
          sMas.type = 7; // type du seuil : Q=f(Zav)
          sMas.coteCrete = t.coteCrete();
          sMas.numLoi = idonneesHydrau.getIndiceLoi(t.loi()) + 1;
        } else if (s instanceof MetierSeuilTarageAmont) {
          MetierSeuilTarageAmont t = (MetierSeuilTarageAmont) s;
          sMas.type = 6; // type du seuil : Q=f(Zam)
          sMas.numLoi = idonneesHydrau.getIndiceLoi(t.loi()) + 1;
          sMas.coteCrete = t.coteCrete();
          if (iparamGen.regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
            sMas.gradient = t.gradient();
          }
        } else if (s instanceof MetierSeuilDenoye) {
          MetierSeuilDenoye d = (MetierSeuilDenoye) s;
          sMas.type = 2;
          /*if (d.loi().amont()) {
           //  Z=f(Q)
           sMas.type= 2;
           } else {
           // Q=f(Z)
           sMas.type = 6;
           }*/
          sMas.coteCrete = d.coteCrete();
          sMas.numLoi = idonneesHydrau.getIndiceLoi(d.loi()) + 1;
          /*if (iparamGen.regime().value()==EnumMetierRegime._TRANSCRITIQUE) {
           sMas.gradient = d.gradient();
           }*/
        } else if (s instanceof MetierSeuilLimniAmont) {
          MetierSeuilLimniAmont l = (MetierSeuilLimniAmont) s;
          sMas.type = 5; // type du seuil : Zam=f(t)
          sMas.coteCrete = l.coteCrete();
          sMas.numLoi = idonneesHydrau.getIndiceLoi(l.loi()) + 1;
        } else if (s instanceof MetierSeuilGeometrique) {
          MetierSeuilGeometrique g = (MetierSeuilGeometrique) s;
          sMas.type = 3; // type du seuil : Profil de crete
          sMas.coteCreteMoy = g.coteMoyenneCrete();
          sMas.coeffDebit = g.coefQ();
          sMas.abscTravCrete = g.loi().d();
          sMas.cotesCrete = g.loi().z();
          sMas.nbPtLoiSeuil = g.loi().z().length;
          /* } else if (s instanceof MetierSeuilTranscritique) {//Uniquement pour d'ancien mod�le
           MetierSeuilTranscritique l= (MetierSeuilTranscritique)s;
           sMas.type= 4; // type du seuil : Zam=f(Cote Crete , Coeff Debit)
           sMas.coteCrete= l.coteCrete();
           sMas.epaisseur= l.epaisseur().value() + 1;
           sMas.coeffDebit= l.coefQ();
           if (iparamGen.regime().value()==EnumMetierRegime._TRANSCRITIQUE) {
           sMas.gradient = l.gradient();
           listeLoisSupplementaires.add(creerDLoiHydrogramme(l,tempsInit, tempsMax,nbLoi));
           sMas.numLoi = nbLoi+1; //idonneesHydrau.getIndiceLoi(l.loi()) + 1;
           nbLoi++;
           }*/
        } else if (s instanceof MetierSeuilLoi) {
          MetierSeuilLoi l = (MetierSeuilLoi) s;
          sMas.type = 4; // type du seuil : Zam=f(Cote Crete , Coeff Debit)
          sMas.coteCrete = l.coteCrete();
          sMas.coeffDebit = l.coefQ();
          sMas.epaisseur = l.epaisseur().value() + 1;
          if (iparamGen.regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
            sMas.gradient = l.gradient();
          }
          if (Math.abs(l.debit()) > 0) {
            listeLoisSupplementaires.add(creerDLoiHydrogramme(l, tempsInit,
                    tempsMax, nbLoi));
            sMas.numLoi = nbLoi + 1; //idonneesHydrau.getIndiceLoi(l.loi()) + 1;
            nbLoi++;
          }
        } else if (s instanceof MetierBarrage) {
          MetierBarrage b = (MetierBarrage) s;
          sMas.coteCrete = b.coteCrete();
        }
        singulariteMas.seuils[i] = sMas;
      }
    }
    if (iparamGen.ondeSubmersion()) {
      MetierBarragePrincipal barragePrincipal = iparamGen.barragePrincipal();
      if (barragePrincipal != null) {
        singulariteMas.barragePrincipal = new SParametresBarrPrincip();
        if (barragePrincipal.ruptureProgressive()) {
          singulariteMas.barragePrincipal.typeRupture = 1;
        } // 1->rupture progressive
        else {
          singulariteMas.barragePrincipal.typeRupture = 2;
          // 2->rupture instantanee
          if (barragePrincipal.site() != null) {
            if (barragePrincipal.site().biefRattache() == null) {
              singulariteMas.barragePrincipal.numBranche = 1;
            } else {
              singulariteMas.barragePrincipal.numBranche
                      = ireseau.getIndiceBief(barragePrincipal.site().biefRattache())
                      + 1;
            }
            singulariteMas.barragePrincipal.abscisse
                    = barragePrincipal.site().abscisse();
            singulariteMas.barragePrincipal.coteCrete
                    = barragePrincipal.cotePlanEau();
          }
        }
      }
    }
    LoisSupplementaires_ = (MetierLoiHydraulique[]) listeLoisSupplementaires.toArray(new MetierLoiHydraulique[0]);
    return singulariteMas;
  }

  private static final SParametresCasier convertirParametresCasier(
          MetierReseau ireseau,
          String nomFichierSansExtension) {
    int nbCasier = ireseau.casiers().length;
    int nbLiaison = ireseau.liaisons().length;
    if ((nbCasier == 0) || (nbLiaison == 0)) {
      return null;
    }
    ireseau.initNumeroCasier();
    SParametresCasier paramMas = new SParametresCasier();
    paramMas.nbCasiers = nbCasier;
    paramMas.fichierGeomCasiers = nomFichierSansExtension + ".casier";
    paramMas.optionPlanimetrage = initIRien(nbCasier);
    paramMas.optionCalcul = initIRien(nbCasier);
    paramMas.cotesInitiale = initRien(nbCasier);
    paramMas.pasPlanimetrage = initRien(nbCasier);
    paramMas.nbCotesPlanimetrage = initIRien(nbCasier);
    MetierCasier[] icasiers = ireseau.casiers();
    for (int i = 0; i < nbCasier; i++) {
      MetierCasier icasier = icasiers[i];
      paramMas.cotesInitiale[i] = icasier.coteInitiale();
      if (icasier.isPlanimetrage()) {
        paramMas.optionPlanimetrage[i] = 1; // planim�trage manuel
      } else {
        paramMas.optionPlanimetrage[i] = 2; // planim�trage automatique
        // option de calcul
        if (icasier.geometrie().isSurfaceDependCote()) {
          paramMas.optionCalcul[i] = 2; // surface d�pendant de la cote
        } else {
          paramMas.optionCalcul[i] = 1; // Surface constante
        }
        //pas de planim�trage
        paramMas.pasPlanimetrage[i] = icasier.geometrie().getPasPlanimetrage();
        //nb cote de planim�trage
        paramMas.nbCotesPlanimetrage[i]
                = icasier.geometrie().getNbCotePlanimetrage();
      }
    }
    if (isContientRien(paramMas.optionCalcul)) {
      paramMas.optionCalcul = null;
    }
    if (isContientRien(paramMas.pasPlanimetrage)) {
      paramMas.pasPlanimetrage = null;
    }
    if (isContientRien(paramMas.nbCotesPlanimetrage)) {
      paramMas.nbCotesPlanimetrage = null;
    }
    /**
     * temporaire avant changement du code pour supporter plusieurs options
     */
    int option = paramMas.optionPlanimetrage[0];

    // v�rification que tous les casiers ont la m�me option de planim�trage
    for (int i = 0; i < paramMas.optionPlanimetrage.length; i++) {
      if (paramMas.optionPlanimetrage[i] != option) {
        throw new RuntimeException("\t" + getS("La repr�sentation de la g�om�trie des casiers ")
                + getS("n'est pas de m�me type pour tous les casiers.") + "\n"
                + "\t" + getS("Le noyau de calcul ne supporte pas encore cette possibilit�."));
      }
    }

    paramMas.optionCalcul = new int[1];
    paramMas.optionCalcul[0] = option;
    /**
     * Fin code temporaire avant changement du code
     */
    paramMas.liaisons = new SParametresLiaisons();
    paramMas.liaisons.nbLiaisons = nbLiaison;
    paramMas.liaisons.types = new int[nbLiaison];
    paramMas.liaisons.nature = new int[nbLiaison];
    paramMas.liaisons.cote = new double[nbLiaison];
    paramMas.liaisons.largeur = new double[nbLiaison];
    paramMas.liaisons.longueur = new double[nbLiaison];
    paramMas.liaisons.rugosite = new double[nbLiaison];
    paramMas.liaisons.section = new double[nbLiaison];
    paramMas.liaisons.coefPerteCharge = new double[nbLiaison];
    paramMas.liaisons.coefDebitSeuil = new double[nbLiaison];
    paramMas.liaisons.coefActivation = new double[nbLiaison];
    paramMas.liaisons.coefDebitOrifice = new double[nbLiaison];
    paramMas.liaisons.typeOrifice = new int[nbLiaison];
    paramMas.liaisons.abscBief = new double[nbLiaison];
    paramMas.liaisons.numCasierOrigine = new int[nbLiaison];
    paramMas.liaisons.numCasierFin = new int[nbLiaison];
    paramMas.liaisons.numBiefAssocie = new int[nbLiaison];
    MetierLiaison[] iliaisons = ireseau.liaisons();
    for (int i = 0; i < nbLiaison; i++) {
      MetierLiaison iliaison = iliaisons[i];
      // type de la liaison
      if (iliaison.isSeuil()) {
        paramMas.liaisons.types[i] = 1;
      }
      if (iliaison.isChenal()) {
        paramMas.liaisons.types[i] = 2;
      }
      if (iliaison.isSiphon()) {
        paramMas.liaisons.types[i] = 3;
      }
      if (iliaison.isOrifice()) {
        paramMas.liaisons.types[i] = 4;
      }
      // nature de la liaison
      if (iliaison.isRiviereCasier()) {
        paramMas.liaisons.nature[i] = 1;
      }
      if (iliaison.isCasierCasier()) {
        paramMas.liaisons.nature[i] = 2;
      }
      // cote de la liaison
      paramMas.liaisons.cote[i] = iliaison.getCote();
      // largeur de la liaison pour les liaisons de type chenal ou seuil ou orifice, -1 sinon
      paramMas.liaisons.largeur[i] = -1.;
      if (iliaison.isSeuil() || iliaison.isChenal() || iliaison.isOrifice()) {
        paramMas.liaisons.largeur[i] = iliaison.getLargeur();
      }
      // longueur de la liaison pour les liaisons de type chenal ou siphon, -1 sinon
      paramMas.liaisons.longueur[i] = -1.;
      if (iliaison.isSiphon() || iliaison.isChenal()) {
        paramMas.liaisons.longueur[i] = iliaison.getLongueur();
      }
      // rugosit� de la liaison pour les liaisons de type chenal, -1 sinon
      paramMas.liaisons.rugosite[i] = -1.;
      if (iliaison.isChenal()) {
        paramMas.liaisons.rugosite[i] = iliaison.getRugosite();
      }
      // section de la liaison pour les liaisons de type siphon ou orifice, -1 sinon
      paramMas.liaisons.section[i] = -1.;
      if (iliaison.isSiphon() || iliaison.isOrifice()) {
        paramMas.liaisons.section[i] = iliaison.getSection();
      }
      // coef de perte de charge de la liaison pour les liaisons de type siphon, -1 sinon
      paramMas.liaisons.coefPerteCharge[i] = -1.;
      if (iliaison.isSiphon()) {
        paramMas.liaisons.coefPerteCharge[i] = iliaison.getCoefPerteCharge();
      }
      // coef de d�bit de la liaison pour les liaisons de type seuil ou orifice, -1 sinon
      paramMas.liaisons.coefDebitSeuil[i] = -1.;
      if (iliaison.isSeuil() || iliaison.isOrifice()) {
        paramMas.liaisons.coefDebitSeuil[i] = iliaison.getCoefQ();
      }
      // coef d'activation de la liaison pour les liaisons de type seuil, -1 sinon
      paramMas.liaisons.coefActivation[i] = -1.;
      if (iliaison.isSeuil()) {
        paramMas.liaisons.coefActivation[i] = iliaison.getCoefActivation();
      }
      // coef de d�bit orifice de la liaison pour les liaisons de type orifice, -1 sinon
      paramMas.liaisons.coefDebitOrifice[i] = -1.;
      if (iliaison.isOrifice()) {
        paramMas.liaisons.coefDebitOrifice[i] = iliaison.getCoefQOrifice();
      }
      // type de l'orifice de la liaison pour les liaisons de type orifice, -1 sinon
      paramMas.liaisons.typeOrifice[i] = -1;
      if (iliaison.isOrifice()) {
        paramMas.liaisons.typeOrifice[i] = iliaison.getSensDebit().value() + 1;
      }
      // abscisse du bief de la liaison pour les liaisons de nature "Rivi�re-Casier", -1 sinon
      paramMas.liaisons.abscBief[i] = -1.;
      if (iliaison.isRiviereCasier()) {
        paramMas.liaisons.abscBief[i] = iliaison.getAbscisse();
      }
      // numero casier d'origine
      paramMas.liaisons.numCasierOrigine[i] = -1;
      if (iliaison.isRiviereCasier()) {
        paramMas.liaisons.numCasierOrigine[i]
                = iliaison.topologie().getCasierRattache().numero();
      }
      if (iliaison.isCasierCasier()) {
        paramMas.liaisons.numCasierOrigine[i]
                = iliaison.topologie().getCasierAmontRattache().numero();
      }
      // numero casier fin
      paramMas.liaisons.numCasierFin[i] = -1;
      if (iliaison.isCasierCasier()) {
        paramMas.liaisons.numCasierFin[i]
                = iliaison.topologie().getCasierAvalRattache().numero();
      }
      // numero bief
      paramMas.liaisons.numBiefAssocie[i] = -1;
      if (iliaison.isRiviereCasier()) {
        int indiceBief
                = ireseau.getIndiceBief(iliaison.topologie().getBiefRattache());
        paramMas.liaisons.numBiefAssocie[i] = indiceBief + 1;
      }
    }
    return paramMas;
  }

  private static final int[] initIRien(int taille) {
    int[] res = new int[taille];
    for (int i = 0; i < taille; i++) {
      res[i] = IRIEN;
    }
    return res;
  }

  private static final double[] initRien(int taille) {
    double[] res = new double[taille];
    for (int i = 0; i < taille; i++) {
      res[i] = RIEN;
    }
    return res;
  }

  private static final boolean isContientRien(int[] tab) {
    for (int i = 0; i < tab.length; i++) {
      if (tab[i] != IRIEN) {
        return false;
      }
    }
    return true;
  }

  private static final boolean isContientRien(double[] tab) {
    for (int i = 0; i < tab.length; i++) {
      if (!Double.isNaN(tab[i])) {
        return false;
      }
    }
    return true;
  }

  private static final SParametresApporDeversoirs convertirParametresApporDeversoirs(
          boolean isNoyauV5P2,
          MetierReseau ireseau,
          MetierDonneesHydrauliques idonneesHydrau) {
    SParametresApporDeversoirs paramMas = new SParametresApporDeversoirs();
    Vector numBrancheQApport = new Vector();
    Vector QApport = new Vector();
    Vector numBrancheDeversoir = new Vector();
    Vector deversoirs = new Vector();
    for (int i = 0; i < ireseau.biefs().length; i++) {
      MetierApport[] apportsHydr = ireseau.biefs()[i].apports();
      for (int j = 0; j < apportsHydr.length; j++) {
        numBrancheQApport.addElement(
                new Integer(ireseau.getIndiceBief(ireseau.biefs()[i]) + 1));
        QApport.addElement(apportsHydr[j]);
      }
      MetierDeversoir[] deversoirsHydr = ireseau.biefs()[i].deversoirs();
      for (int j = 0; j < deversoirsHydr.length; j++) {
        numBrancheDeversoir.addElement(
                new Integer(ireseau.getIndiceBief(ireseau.biefs()[i]) + 1));
        deversoirs.addElement(deversoirsHydr[j]);
      }
    }
    Vector apportCasier = new Vector();
    class NumLoiNumCasier {

      int numCasier;
      int numLoi;
    }
    MetierCasier[] icasiers = ireseau.casiers();
    for (int i = 0; i < icasiers.length; i++) {
      MetierCasier ic = icasiers[i];
      if (ic.loiRattachee() != null) {
        NumLoiNumCasier numLoiCasier = new NumLoiNumCasier();
        numLoiCasier.numCasier = i + 1;
        numLoiCasier.numLoi = idonneesHydrau.getIndiceLoi(ic.loiRattachee()) + 1;
        apportCasier.add(numLoiCasier);
      }
    }
    if (apportCasier.size() > 0) {
      int taille = apportCasier.size();
      paramMas.apportCasier = new SParametresApportCasier();
      paramMas.apportCasier.nbApportPluie = taille;
      paramMas.apportCasier.numCasier = new int[taille];
      paramMas.apportCasier.numLoi = new int[taille];
      for (int i = 0; i < taille; i++) {
        NumLoiNumCasier numLoiCasier = (NumLoiNumCasier) apportCasier.get(i);
        paramMas.apportCasier.numCasier[i] = numLoiCasier.numCasier;
        paramMas.apportCasier.numLoi[i] = numLoiCasier.numLoi;
      }
    }
    if (QApport.size() > 0) {
      paramMas.debitsApports = new SParametresQApport();
      SParametresQApport ApportsMas = paramMas.debitsApports;
      ApportsMas.nbQApport = QApport.size();
      ApportsMas.noms = new String[QApport.size()];
      ApportsMas.numBranche = new int[QApport.size()];
      ApportsMas.abscisses = new double[QApport.size()];
      ApportsMas.longueurs = new double[QApport.size()];
      ApportsMas.numLoi = new int[QApport.size()];
      for (int i = 0; i < QApport.size(); i++) {
        MetierApport a = (MetierApport) QApport.elementAt(i);
        ApportsMas.noms[i] = a.nom();
        ApportsMas.numBranche[i]
                = ((Integer) numBrancheQApport.elementAt(i)).intValue();
        ApportsMas.abscisses[i] = a.abscisse();
        ApportsMas.longueurs[i] = a.longueur();
        ApportsMas.numLoi[i] = idonneesHydrau.getIndiceLoi(a.loi()) + 1;
      }
    }
    SParametresDeversLateraux deversoirsMas = null;
    if (isNoyauV5P2) {
      if (deversoirs.size() > 0) {
        paramMas.deversLate = new SParametresDeversLateraux();
        deversoirsMas = paramMas.deversLate;
        deversoirsMas.nbDeversoirs = deversoirs.size();
        deversoirsMas.deversoirsV5P2 = new SParametresDeversoirsV5P2[deversoirs.size()];
        for (int i = 0; i < deversoirs.size(); i++) {
          MetierDeversoir d = (MetierDeversoir) deversoirs.elementAt(i);
          deversoirsMas.deversoirsV5P2[i] = new SParametresDeversoirsV5P2();
          deversoirsMas.deversoirsV5P2[i].nom = d.nom();
          deversoirsMas.deversoirsV5P2[i].abscisse = d.abscisse();
          deversoirsMas.deversoirsV5P2[i].numBranche
                  = ((Integer) numBrancheDeversoir.elementAt(i)).intValue();
          deversoirsMas.deversoirsV5P2[i].longueur = d.longueur();
          if (d instanceof MetierDeversoirComportementZCoefQ) {
            MetierDeversoirComportementZCoefQ dZQ = (MetierDeversoirComportementZCoefQ) d;
            deversoirsMas.deversoirsV5P2[i].type = 1;
            // type du deversoir : 1->"Cote Crete , Coeff Debit"
            deversoirsMas.deversoirsV5P2[i].coteCrete = dZQ.coteCrete();
            deversoirsMas.deversoirsV5P2[i].coeffDebit = dZQ.coefQ();
          } else if (d instanceof MetierDeversoirComportementLoi) {
            MetierDeversoirComportementLoi dLoi = (MetierDeversoirComportementLoi) d;
            deversoirsMas.deversoirsV5P2[i].type = 2;
            // type du deversoir : 2->"Loi de seuil"
            deversoirsMas.deversoirsV5P2[i].numLoi
                    = idonneesHydrau.getIndiceLoi(dLoi.loi()) + 1;
          }
        }
      }
    } else {
      if (deversoirs.size() > 0) {
        paramMas.deversLate = new SParametresDeversLateraux();
        deversoirsMas = paramMas.deversLate;
        deversoirsMas.nbDeversoirs = deversoirs.size();
        deversoirsMas.nom = new String[deversoirs.size()];
        deversoirsMas.type = new int[deversoirs.size()];
        deversoirsMas.numBranche = new int[deversoirs.size()];
        deversoirsMas.abscisse = new double[deversoirs.size()];
        deversoirsMas.longueur = new double[deversoirs.size()];
        deversoirsMas.coteCrete = new double[deversoirs.size()];
        deversoirsMas.coeffDebit = new double[deversoirs.size()];
        deversoirsMas.numLoi = new int[deversoirs.size()];
        for (int i = 0; i < deversoirs.size(); i++) {
          MetierDeversoir d = (MetierDeversoir) deversoirs.elementAt(i);
          deversoirsMas.nom[i] = d.nom();;
          deversoirsMas.numBranche[i] = ((Integer) numBrancheDeversoir.elementAt(i)).intValue();
          deversoirsMas.abscisse[i] = d.abscisse();
          deversoirsMas.longueur[i] = d.longueur();
          if (d instanceof MetierDeversoirComportementZCoefQ) {
            MetierDeversoirComportementZCoefQ dZQ = (MetierDeversoirComportementZCoefQ) d;
            deversoirsMas.type[i] = 1;
            // type du deversoir : 1->"Cote Crete , Coeff Debit"
            deversoirsMas.coteCrete[i] = dZQ.coteCrete();
            deversoirsMas.coeffDebit[i] = dZQ.coefQ();
          } else if (d instanceof MetierDeversoirComportementLoi) {
            MetierDeversoirComportementLoi dLoi = (MetierDeversoirComportementLoi) d;
            deversoirsMas.type[i] = 2;
            // type du deversoir : 2->"Loi de tarage"
            deversoirsMas.numLoi[i] = idonneesHydrau.getIndiceLoi(dLoi.loi()) + 1;
          }
        }
        deversoirsMas.deversoirsV5P2 = null;
      }
    }
    return paramMas;
  }

  private static final SParametresCalage convertirParametresCalage(
          MetierReseau ireseau,
          EnumMetierTypeFrottement typeFrot,EnumMetierTypeCoefficient typeCoeff) {
    SParametresCalage paramMas
            = new SParametresCalage(
                    new SParametresFrottement(),
                    new SParametresZoneStockage());
    SParametresZoneStockage zonesStockageMas = paramMas.zoneStockage;
    SParametresFrottement frottementsMas = paramMas.frottement;
    // loi de frottement : 1->STRICKLER, 2->CHEZY, 3->COLEBROOK, 4->BAZIN
    frottementsMas.loi = typeFrot.value() + 1;
    Vector numBrancheZonesFrottement = new Vector();
    Vector zonesFrottement = new Vector();
    Vector profilsAStockage = new Vector();
    for (int i = 0; i < ireseau.biefs().length; i++) {
      MetierZoneFrottement[] zonesFrottementHydr
              = ireseau.biefs()[i].zonesFrottement();
      for (int j = 0; j < zonesFrottementHydr.length; j++) {
        numBrancheZonesFrottement.addElement(
                new Integer(ireseau.getIndiceBief(ireseau.biefs()[i]) + 1));
        zonesFrottement.addElement(zonesFrottementHydr[j]);
      }
      MetierProfil[] profilsAStockageHydr
              = ireseau.biefs()[i].profilsAvecZonesStockage();
      for (int j = 0; j < profilsAStockageHydr.length; j++) {
        profilsAStockage.addElement(profilsAStockageHydr[j]);
      }
    }
    frottementsMas.nbZone = zonesFrottement.size();
    frottementsMas.numBranche = new int[zonesFrottement.size()];
    frottementsMas.absDebZone = new double[zonesFrottement.size()];
    frottementsMas.absFinZone = new double[zonesFrottement.size()];
    frottementsMas.coefLitMaj = new double[zonesFrottement.size()];
    frottementsMas.coefLitMin = new double[zonesFrottement.size()];
    for (int i = 0; i < zonesFrottement.size(); i++) {
      MetierZoneFrottement z = (MetierZoneFrottement) zonesFrottement.elementAt(i);
      frottementsMas.numBranche[i]
              = ((Integer) numBrancheZonesFrottement.elementAt(i)).intValue();
      frottementsMas.absDebZone[i] = z.abscisseDebut();
      frottementsMas.absFinZone[i] = z.abscisseFin();
      
      //-- ahadoux - si type coeff = manning, on exporte les inverses --// 
      if(typeCoeff.value() == EnumMetierTypeCoefficient._MANNING) {
    	  if(z.coefMajeur() !=0)
    		  frottementsMas.coefLitMaj[i] = 1.0/z.coefMajeur();
    	  if(z.coefMineur() !=0)
    		  frottementsMas.coefLitMin[i] = 1.0/z.coefMineur();
      }else {
	      frottementsMas.coefLitMaj[i] = z.coefMajeur();
	      frottementsMas.coefLitMin[i] = z.coefMineur();
      }
    }
    zonesStockageMas.nbProfils = profilsAStockage.size();
    zonesStockageMas.limDroitLitMaj = new double[profilsAStockage.size()];
    zonesStockageMas.limGauchLitMaj = new double[profilsAStockage.size()];
    zonesStockageMas.numProfil = new int[profilsAStockage.size()];
    for (int i = 0; i < profilsAStockage.size(); i++) {
      MetierProfil p = (MetierProfil) profilsAStockage.elementAt(i);
      zonesStockageMas.limDroitLitMaj[i] = p.points()[p.indiceLitMajDr()].x;
      zonesStockageMas.limGauchLitMaj[i] = p.points()[p.indiceLitMajGa()].x;
      zonesStockageMas.numProfil[i]
              = ireseau.getIndiceProfilNumero(p.numero()) + 1;
    }
    return paramMas;
  }

  private static final SParametresLoisHydrau convertirParametresLoisHydrau(MetierLoiHydraulique[] loisHydr, String nomFichier) {
    SParametresLoisHydrau loisMas = new SParametresLoisHydrau();

    int nbLoi = loisHydr.length + LoisSupplementaires_.length;
    MetierLoiHydraulique[] loisHydrPasGeo = new MetierLoiHydraulique[nbLoi];
    for (int k = 0; k < loisHydrPasGeo.length; k++) {
      if (k < loisHydr.length) {
        loisHydrPasGeo[k] = loisHydr[k];
      } else {
        loisHydrPasGeo[k] = LoisSupplementaires_[k - loisHydr.length];
      }
    }
    if (loisHydrPasGeo != null) {
      Arrays.sort(loisHydrPasGeo, new MetierLoiHydrogramme());
      loisMas.nb = nbLoi;
      loisMas.lois = new SParametresLoi[nbLoi];

      for (int i = 0; i < loisHydrPasGeo.length; i++) {
        MetierLoiHydraulique l = loisHydrPasGeo[i];
        loisMas.lois[i] = new SParametresLoi();
        loisMas.lois[i].nom = l.nom().trim().replace(' ', '_');
        if (l instanceof MetierLoiHydrogramme) {
          loisMas.lois[i].type = 1; // type de la loi : 1->"HYDROGRAMME Q(T)"
        } else if (l instanceof MetierLoiLimnigramme) {
          loisMas.lois[i].type = 2; // type de la loi : 2->"LIMNIGRAMME Z(T)"
        } else if (l instanceof MetierLoiLimniHydrogramme) {
          loisMas.lois[i].type = 3;//type de la loi : 3->"LIMNHYDROGRAMME Z,Q(T)"
        } else if (l instanceof MetierLoiTarage) {
          MetierLoiTarage ll = (MetierLoiTarage) l;
          if (ll.amont()) {
            // amont <=> Z=f(Q)
            loisMas.lois[i].type = 4; //type de loi : 4->"COURBE DE TARAGE Z=f(Q)"
          } else {
            // aval <=> Q=f(Z)
            loisMas.lois[i].type = 5; //type de loi : 5->"COURBE DE TARAGE Q=f(Z)"
          }
        } else if (l instanceof MetierLoiSeuil) {
          loisMas.lois[i].type = 6; // type de la loi : 6->"SEUIL Zam=f(Zav,Q)
        } else if (l instanceof MetierLoiRegulation) {
          loisMas.lois[i].type = 4;//type de loi : 4->"COURBE DE TARAGE Z=f(Q)"
        } else if (l instanceof MetierLoiOuvertureVanne) {
          loisMas.lois[i].type = 7;
          // type de la loi : 6->"OUVERTURE VANNE Zinf,Zsup=f(T)
        } else {
          loisMas.lois[i].type = IRIEN;
        }

        loisMas.lois[i].donnees = new SParametresDonneesLoi();
        // entr�e par fichier
        loisMas.lois[i].donnees.modeEntree = 1;
        loisMas.lois[i].donnees.fichier = nomFichier + "_" + i + ".loi";
        loisMas.lois[i].donnees.nbDebitsDifferents = IRIEN;
        loisMas.lois[i].donnees.uniteTps = IRIEN;
        loisMas.lois[i].donnees.nbPoints = IRIEN;

      }

    }

    return loisMas;
  }

  private static final SLoiHydraulique convertirDLoi(MetierLoiHydraulique iloi) {
    if (iloi instanceof MetierLoiHydrogramme) {
      return convertirDLoi((MetierLoiHydrogramme) iloi);
    }
    if (iloi instanceof MetierLoiLimnigramme) {
      return convertirDLoi((MetierLoiLimnigramme) iloi);
    }
    if (iloi instanceof MetierLoiLimniHydrogramme) {
      return convertirDLoi((MetierLoiLimniHydrogramme) iloi);
    }
    if (iloi instanceof MetierLoiOuvertureVanne) {
      return convertirDLoi((MetierLoiOuvertureVanne) iloi);
    }
    if (iloi instanceof MetierLoiRegulation) {
      return convertirDLoi((MetierLoiRegulation) iloi);
    }
    if (iloi instanceof MetierLoiSeuil) {
      return convertirDLoi((MetierLoiSeuil) iloi);
    }
    if (iloi instanceof MetierLoiTarage) {
      return convertirDLoi((MetierLoiTarage) iloi);
    }
    return null;

  }

  private static final SLoiHydraulique convertirDLoi(MetierLoiHydrogramme iloi) {
    SLoiHydraulique loiMas = new SLoiHydraulique();

    loiMas.nom = iloi.nom().trim().replace(' ', '_');
    System.out.println(" convertirDLoi(MetierLoiHydrogramme iloi)   loiMas.nom = " + loiMas.nom + " iloi.nom()=" + iloi.nom());

    loiMas.unitee = "S";
    // entete colonnes
    loiMas.entetesColonnes = new String[2];
    loiMas.entetesColonnes[0] = "Temps (s)";
    loiMas.entetesColonnes[1] = "Debit";

    loiMas.x = iloi.t();
    loiMas.y = iloi.q();

    return loiMas;
  }

  private static final MetierLoiHydrogramme creerDLoiHydrogramme(MetierSeuilLoi iseuil, double tempsInit, double tempsMax, int indice) {

    MetierLoiHydrogramme loi = new MetierLoiHydrogramme();
    System.out.println("creerDLoiHydrogramme iseuil.nom()=" + iseuil.nom());
    loi.indice(indice); //Ne pas modifier l'indice apres le nom, car indice() modifie le nom
    loi.nom(getS("Hydrogramme constant pour le seuil ") + iseuil.nom());

    double[] tabTemps = new double[2];
    tabTemps[0] = tempsInit;
    tabTemps[1] = tempsMax;

    double debit = iseuil.debit();
    double[] tabDebits = new double[2];
    tabDebits[0] = debit;
    tabDebits[1] = debit;

    loi.t(tabTemps);
    loi.q(tabDebits);

    return loi;
  }

  private static final SLoiHydraulique convertirDLoi(MetierLoiLimnigramme iloi) {
    SLoiHydraulique loiMas = new SLoiHydraulique();

    loiMas.nom = iloi.nom().trim().replace(' ', '_');

    loiMas.unitee = "S";
    // entete colonnes
    loiMas.entetesColonnes = new String[2];
    loiMas.entetesColonnes[0] = "Temps (s)";
    loiMas.entetesColonnes[1] = "Cote";

    loiMas.x = iloi.t();
    loiMas.y = iloi.z();

    return loiMas;
  }

  private static final SLoiHydraulique convertirDLoi(MetierLoiLimniHydrogramme iloi) {
    SLoiHydraulique loiMas = new SLoiHydraulique();

    loiMas.nom = iloi.nom().trim().replace(' ', '_');
    loiMas.unitee = "S";
    // entete colonnes
    loiMas.entetesColonnes = new String[3];
    loiMas.entetesColonnes[0] = "Temps (s)";
    loiMas.entetesColonnes[1] = "Cote";
    loiMas.entetesColonnes[2] = "Debit";

    loiMas.x = iloi.t();
    loiMas.y = iloi.z();
    loiMas.z = iloi.q();

    return loiMas;
  }

  private static final SLoiHydraulique convertirDLoi(MetierLoiOuvertureVanne iloi) {
    SLoiHydraulique loiMas = new SLoiHydraulique();

    loiMas.nom = iloi.nom().trim().replace(' ', '_');
    loiMas.unitee = "S";
    // entete colonnes
    loiMas.entetesColonnes = new String[3];
    loiMas.entetesColonnes[0] = "Temps (s)";
    loiMas.entetesColonnes[1] = "Cote inf�rieur";
    loiMas.entetesColonnes[2] = "Cote sup�rieur";

    loiMas.x = iloi.t();
    loiMas.y = iloi.zInf();
    loiMas.z = iloi.zSup();

    return loiMas;
  }

  private static final SLoiHydraulique convertirDLoi(MetierLoiTarage iloi) {
    SLoiHydraulique loiMas = new SLoiHydraulique();

    loiMas.nom = iloi.nom().trim().replace(' ', '_');
    loiMas.unitee = "";
    // entete colonnes
    loiMas.entetesColonnes = new String[2];
    if (iloi.amont()) {
      loiMas.entetesColonnes[0] = "Debit";
      loiMas.entetesColonnes[1] = "Cote";
      loiMas.x = iloi.q();
      loiMas.y = iloi.z();
    } else {
      loiMas.entetesColonnes[0] = "Cote";
      loiMas.entetesColonnes[1] = "Debit";
      loiMas.x = iloi.z();
      loiMas.y = iloi.q();
    }
    return loiMas;
  }

  private static final SLoiHydraulique convertirDLoi(MetierLoiRegulation iloi) {
    SLoiHydraulique loiMas = new SLoiHydraulique();

    loiMas.nom = iloi.nom().trim().replace(' ', '_');
    loiMas.unitee = "";
    // entete colonnes
    loiMas.entetesColonnes = new String[2];
    loiMas.entetesColonnes[0] = "Debit amont";
    loiMas.entetesColonnes[1] = "Cote aval";

    loiMas.x = iloi.qAmont();
    loiMas.y = iloi.zAval();
    return loiMas;
  }

  private static final SLoiHydraulique convertirDLoi(MetierLoiSeuil iloi) {
    SLoiHydraulique loiMas = new SLoiHydraulique();

    loiMas.nom = iloi.nom().trim().replace(' ', '_');
    loiMas.unitee = "";
    // entete colonnes
    loiMas.entetesColonnes = new String[3];
    loiMas.entetesColonnes[0] = "Debit";
    loiMas.entetesColonnes[1] = "Cote_Aval";
    loiMas.entetesColonnes[2] = "Cote_Amont";

    loiMas.x = convertirLoiSeuil2DebitLoi(iloi);
    loiMas.y = convertirLoiSeuil2Cote2Loi(iloi);
    loiMas.z = convertirLoiSeuil2CoteLoi(iloi);

    return loiMas;
  }

  private static final double[] convertirLoiSeuil2DebitLoi(MetierLoiSeuil loiSeuil) {
    int nbVal = loiSeuil.q().length * loiSeuil.zAval().length;
    double[] res = new double[nbVal];
    int indiceRes = 0;
    for (int i = 0; i < loiSeuil.q().length; i++) {
      for (int j = 0; j < loiSeuil.zAval().length; j++) {
        res[indiceRes] = loiSeuil.q()[i];
        indiceRes++;
      }
    }
    return res;
  }

  private static final double[] convertirLoiSeuil2CoteLoi(MetierLoiSeuil loiSeuil) {
    int nbVal = loiSeuil.q().length * loiSeuil.zAval().length;
    double[] res = new double[nbVal];
    int indiceRes = 0;
    for (int i = 0; i < loiSeuil.zAmont().length; i++) {
      for (int j = 0; j < loiSeuil.zAmont()[i].length; j++) {
        res[indiceRes] = loiSeuil.zAmont()[i][j];
        indiceRes++;
      }
    }
    return res;
  }

  private static final double[] convertirLoiSeuil2Cote2Loi(MetierLoiSeuil loiSeuil) {
    int nbVal = loiSeuil.q().length * loiSeuil.zAval().length;
    double[] res = new double[nbVal];
    int indiceRes = 0;
    for (int i = 0; i < loiSeuil.q().length; i++) {
      for (int j = 0; j < loiSeuil.zAval().length; j++) {
        res[indiceRes] = loiSeuil.zAval()[j];
        indiceRes++;
      }
    }
    return res;
  }

  private static final SParametresCondInit convertirParametresCondInit(
          MetierConditionsInitiales condInitHydr,
          String nomFichierSansExtension,
          MetierReseau ireseau) {
    SParametresCondInit condInitMas = new SParametresCondInit();
    condInitMas.repriseEtude = new SParametresReprEtude();
    condInitMas.repriseEtude.repriseCalcul
            = (condInitHydr.paramsReprise() != null);
    if (condInitMas.repriseEtude.repriseCalcul) {
      condInitMas.repriseEtude.fichRepriseLec
              = nomFichierSansExtension + "_lec.rep";
    }
    condInitMas.ligneEau = new SParametresLigEau();
    if (condInitHydr.ligneEauInitiale() != null) {
      // il existe une ligne d'eau initiale
      condInitMas.ligneEau.LigEauInit = true;
      condInitMas.ligneEau.modeEntree = 1;
      // mode d'entree de la ligne d'eau : 1->"entr�e par fichier"
      condInitMas.ligneEau.fichLigEau = nomFichierSansExtension + ".lig";
      condInitMas.ligneEau.formatFichLig = 2;//format 2->LIDOP
      condInitMas.ligneEau.nbPts = IRIEN;
      condInitMas.ligneEau.abscisse = null;
      condInitMas.ligneEau.branche = null;
      condInitMas.ligneEau.cote = null;
      condInitMas.ligneEau.debit = null;
      /*MetierConditionsInitiales condInitHydr
       condInitMas.ligneEau.nbPts =
       condInitHydr.ligneEauInitiale().points().length;
       condInitMas.ligneEau.abscisse = new double[condInitMas.ligneEau.nbPts];
       condInitMas.ligneEau.branche = new int[condInitMas.ligneEau.nbPts];
       condInitMas.ligneEau.cote = new double[condInitMas.ligneEau.nbPts];
       condInitMas.ligneEau.debit = new double[condInitMas.ligneEau.nbPts];
       for (int i = 0; i < condInitMas.ligneEau.nbPts; i++) {
       condInitMas.ligneEau.abscisse[i] =
       condInitHydr.ligneEauInitiale().points()[i].abscisse();
       condInitMas.ligneEau.branche[i] =
       condInitHydr.ligneEauInitiale().points()[i].numeroBief();
       condInitMas.ligneEau.cote[i] =
       condInitHydr.ligneEauInitiale().points()[i].cote();
       condInitMas.ligneEau.debit[i] =
       condInitHydr.ligneEauInitiale().points()[i].debit();
       }*/

    } else { // il n'existe pas de ligne d'eau initiale ni de reprise
      condInitMas.ligneEau.LigEauInit = false;
      condInitMas.ligneEau.modeEntree = 2;
      condInitMas.ligneEau.formatFichLig = 1;
      condInitMas.ligneEau.nbPts = 0;
      condInitMas.ligneEau.abscisse = new double[0];
      condInitMas.ligneEau.branche = new int[0];
      condInitMas.ligneEau.cote = new double[0];
      condInitMas.ligneEau.debit = new double[0];
    }
    if (condInitHydr.zonesSeches() != null) {
      MetierZone[] zones = condInitHydr.zonesSeches();
      if (zones.length > 0) {
        condInitMas.zonesSeches = new SParametresZoneSeche();
        condInitMas.zonesSeches.nb = zones.length;
        condInitMas.zonesSeches.branche = new int[zones.length];
        condInitMas.zonesSeches.absDebut = new double[zones.length];
        condInitMas.zonesSeches.absFin = new double[zones.length];
        for (int i = 0; i < zones.length; i++) {
          condInitMas.zonesSeches.branche[i]
                  = ireseau.getIndiceBief(zones[i].biefRattache()) + 1;
          condInitMas.zonesSeches.absDebut[i] = zones[i].abscisseDebut();
          condInitMas.zonesSeches.absFin[i] = zones[i].abscisseFin();
        }
      }
    }
    return condInitMas;
  }

  private static final SParametresImpressResult convertirParametresImpressResult(
          MetierParametresResultats paramResHydr,
          String description,
          String nomFichierSansExtension,
          MetierReseau ireseau,
          boolean presenceCasier) {
    SParametresImpressResult impressResultMas
            = new SParametresImpressResult(
                    "",
                    new SParametresImpress(),
                    new SParametresPasStock(),
                    new SParametresResult(),
                    new SParametresListing(),
                    new SParametresFichReprise(),
                    new SParametresRubens(),
                    new SParametresStockage(),
                    null);
    impressResultMas.titreCalcul = description;
    impressResultMas.impression.calcul = paramResHydr.optionsListing().calcul();
    impressResultMas.impression.geometrie
            = paramResHydr.optionsListing().geometrie();
    impressResultMas.impression.ligneEauInit
            = paramResHydr.optionsListing().ligneEauInitiale();
    impressResultMas.impression.loiHydrau
            = paramResHydr.optionsListing().loisHydrauliques();
    impressResultMas.impression.planimetrage
            = paramResHydr.optionsListing().planimetrage();
    impressResultMas.impression.reseau = paramResHydr.optionsListing().reseau();
    impressResultMas.rubens.ecartInterBranch = paramResHydr.decalage();
    if (paramResHydr.postOpthyca() && paramResHydr.postRubens()) {
      impressResultMas.resultats.postProcesseur = 3;
      // post-processeur : 1->RUBENS, 2->OPTHYCA
      impressResultMas.resultats.fichResultat2
              = nomFichierSansExtension + "_ecr.opt";
      impressResultMas.resultats.fichResultat = nomFichierSansExtension + ".rub";
    } else if (paramResHydr.postOpthyca()) {
      impressResultMas.resultats.postProcesseur = 2;
      // post-processeur : 1->RUBENS, 2->OPTHYCA
      impressResultMas.resultats.fichResultat
              = nomFichierSansExtension + "_ecr.opt";
    } else if (paramResHydr.postRubens()) {
      impressResultMas.resultats.postProcesseur = 1;
      // post-processeur : 1->RUBENS
      impressResultMas.resultats.fichResultat = nomFichierSansExtension + ".rub";
    }
    impressResultMas.listing.fichListing = nomFichierSansExtension + ".lis";
    impressResultMas.fichReprise.fichRepriseEcr
            = nomFichierSansExtension + "_ecr.rep";
    impressResultMas.pasStockage.pasImpression
            = paramResHydr.paramStockage().periodeListing();
    impressResultMas.pasStockage.pasStock
            = paramResHydr.paramStockage().periodeResultat();
    impressResultMas.pasStockage.premPasTpsStock
            = paramResHydr.paramStockage().premierPasTempsStocke();
    impressResultMas.stockage.option
            = paramResHydr.paramStockage().option().value() + 1;
    // option de stockage : 1->"Toutes les sections",
    //                      2->"A certains sites"
    if (impressResultMas.stockage.option == 2) { // 2->"A certains sites"
      MetierSite[] sites = paramResHydr.paramStockage().sites();
      impressResultMas.stockage.nbSite = sites.length;
      impressResultMas.stockage.branche = new int[sites.length];
      impressResultMas.stockage.abscisse = new double[sites.length];
      for (int i = 0; i < sites.length; i++) {
        impressResultMas.stockage.branche[i]
                = ireseau.getIndiceBief(sites[i].biefRattache()) + 1;
        impressResultMas.stockage.abscisse[i] = sites[i].abscisse();
      }
    }
    if (presenceCasier) {
      impressResultMas.casier = new SParametresFichResCasier();
      impressResultMas.casier.listingCasier
              = nomFichierSansExtension + ".cas_lis";
      impressResultMas.casier.listingLiaison
              = nomFichierSansExtension + ".liai_lis";
      impressResultMas.casier.resultatCasier
              = nomFichierSansExtension + ".cas_opt";
      impressResultMas.casier.resultatLiaison
              = nomFichierSansExtension + ".liai_opt";
      if (paramResHydr.postRubens()) {
        impressResultMas.casier.resultatCasier
                = nomFichierSansExtension + ".cas_rub";
        impressResultMas.casier.resultatLiaison
                = nomFichierSansExtension + ".liai_rub";
      }
    }
    return impressResultMas;
  }

  private static final SParametresVarCalc convertirParametresVarCalc() {
    SParametresVarCalc varCalcMas = new SParametresVarCalc();
    boolean[] varCalc = new boolean[15];
    for (int i = 0; i < 15; i++) {
      varCalc[i] = false;
    }
    return new SParametresVarCalc(varCalc);
  }

  public static final SLoiHydraulique[] convertirLoisHydrauliques(MetierEtude1d etude) {
    try {
      //Toujours faire une couversion de fichier cas (au moins des singularit�s) avant  les lois hydrauliques car il faut creer une loi hydrogramme pour les seuils transcritiques
      convertirParametresSingularite(
              etude.reseau(),
              etude.paramGeneraux(),
              etude.donneesHydro(),
              etude.paramTemps());

      //On r�cup�rent les lois dans l'etude
      MetierLoiHydraulique[] iloisHydraulique = etude.donneesHydro().getToutesLoisSaufTracerEtGeometrique();

      int nbLoi = iloisHydraulique.length + LoisSupplementaires_.length;

      //Ajout des lois supplemantaires
      MetierLoiHydraulique[] iloisHydrau = new MetierLoiHydraulique[nbLoi];
      for (int k = 0; k < iloisHydrau.length; k++) {
        if (k < iloisHydraulique.length) {
          iloisHydrau[k] = iloisHydraulique[k];
        } else {
          iloisHydrau[k] = LoisSupplementaires_[k - iloisHydraulique.length];
        }
      }

      Arrays.sort(iloisHydrau, new MetierLoiHydrogramme());
      SLoiHydraulique[] loisMas = new SLoiHydraulique[nbLoi];
      for (int i = 0; i < iloisHydrau.length; i++) {
        loisMas[i] = convertirDLoi(iloisHydrau[i]);
      }

      return loisMas;
    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new RuntimeException(getS("Probl�me lors de la conversion en fichiers de lois hydraulique") + "\n"
              + ex.getLocalizedMessage());
    }
  }

  public static final SResultatsTemporelSpatial convertirParametresLig(MetierLigneEauInitiale ligneEauInitiale,
          MetierReseau reseau, double distanceEntreBief) {

    try {

      //On recupere la ligne d'eau
      MetierLigneEauPoint[] tabLigneEauPoint = ligneEauInitiale.points();

      //Conversion du tableau de MetierLigneEauPoint vers MetierResultatsTemporelSpatial
      MetierResultatsTemporelSpatial ires = convertitDLigneEauPoint_IResultatTemporelSpatial(tabLigneEauPoint, reseau, distanceEntreBief, false);
      SResultatsTemporelSpatial paramsLig = convertirResultatsTemporelSpatialH1dToMas(ires);
      return paramsLig;

    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new RuntimeException(getS("Probl�me lors de la conversion en fichier de ligne d'eau initiale") + "\n : " + getS(
              "conversion des Parametres")
              + " " + ex.getLocalizedMessage());
    }

  }
  /*fonction renvoyant vrai si aucunes des listes du tableau n'est null*/

  public final static boolean AucunNull(List[] l) {
    boolean b = true;
    for (int i = 0; i < l.length; i++) {
      b = b && (l[i] != null);
    }
    return b;
  }

  /**
   * Conversion un tableau de MetierConcentrationInitiale en resultats m�tier.
   *
   * @param concentrations MetierConcentrationInitiale[]
   * @return MetierResultatsTemporelSpatial
   */
  public final static MetierResultatsTemporelSpatial convertitDConcentrationInitiale_IResultatTemporelSpatial(MetierConcentrationInitiale[] concs,
          String[][] nomsTraceurs) {

    if (concs == null) {
      return null;
    }
    if (concs.length == 0) {
      return null;
    }

    Notifieur.getNotifieur().setEventMuet(true);
    MetierResultatsTemporelSpatial ires = new MetierResultatsTemporelSpatial();

    ires.resultatsCasier(false);
    ires.resultatsLiaison(false);
    ires.resultatsTracer(true);
    ires.resultatsCalageAuto(false);
    ires.resultatsPermanent(false);

    int nbVariables = concs[0].concentrations().length;
    if (nomsTraceurs.length != nbVariables) {
      System.out.println("Erreur ! le nombre de traceur ne correspond pas entre le modele de qualit� d'eau et les concentrations initiales !!!");
      return null;
    }

    // Un seul pas de temps : 0
    double[] pasTps = {0};
    ires.pasTemps(pasTps);
    MetierDescriptionVariable[] descrVar = new MetierDescriptionVariable[nbVariables];

    // description des nbVariables
    for (int i = 0; i < nbVariables; i++) {

      descrVar[i] = new MetierDescriptionVariable();
      descrVar[i].nom("C" + (i + 1));//nomsTraceurs[i][0]) ;
      descrVar[i].description("Concentration " + nomsTraceurs[i][1]);
      descrVar[i].nbDecimales(11);
      descrVar[i].unite(EnumMetierUnite.RIEN);

    }

    ires.descriptionVariables(descrVar);

    List listeIResTempoSpatialBief = new ArrayList();
    MetierResultatsTemporelSpatialBief iresBief = null;
    List listeSection = null;
    List[] listeValeurs = new List[nbVariables];
    int iBiefPre = -5;

    for (int i = 0; i < concs.length; i++) {
      int iBief = concs[i].numeroBief();
      if (iBiefPre != iBief) {
        if ((iresBief != null) && (listeSection != null) && (AucunNull(listeValeurs))) {
          iresBief.abscissesSections(liste2Abscisse(listeSection));
          iresBief.valeursVariables(listes2valeurs(listeValeurs));
        }
        listeSection = new ArrayList();
        for (int j = 0; j < nbVariables; j++) {
          listeValeurs[j] = new ArrayList();
        }

        iresBief = new MetierResultatsTemporelSpatialBief();
        listeIResTempoSpatialBief.add(iresBief);
      }
      iBiefPre = iBief;
      listeSection.add(new Double(concs[i].abscisse()));
      for (int j = 0; j < nbVariables; j++) {
        listeValeurs[j].add(new Double(concs[i].concentrations()[j]));
      }
    }
    if ((iresBief != null) && (listeSection != null) && (AucunNull(listeValeurs))) {
      iresBief.abscissesSections(liste2Abscisse(listeSection));
      iresBief.valeursVariables(listes2valeurs(listeValeurs));
    }
    ires.resultatsBiefs((MetierResultatsTemporelSpatialBief[]) listeIResTempoSpatialBief.toArray(
            new MetierResultatsTemporelSpatialBief[0]));
    Notifieur.getNotifieur().setEventMuet(false);
    return ires;

  }

  /**
   * Conversion un tableau de MetierConcentrationInitiale en resultats m�tier.
   *
   * @param concentrations SConcentrationInitiale[]
   * @return MetierResultatsTemporelSpatial
   */
  public final static SResultatsTemporelSpatial convertitDConcentrationInitiale_ResultatTemporelSpatial(MetierConcentrationInitiale[] concs,
          String[][] nomsTraceurs) {

    if (concs == null) {
      return null;
    }
    if (concs.length == 0) {
      return null;
    }

    Notifieur.getNotifieur().setEventMuet(true);
    SResultatsTemporelSpatial res = new SResultatsTemporelSpatial();

    res.resultatsCasier = false;
    res.resultatsLiaison = false;
    res.resultatsCalageAuto = false;
    res.resultatsTracer = true;
    res.resultatsPermanent = false;

    int nbVariables = concs[0].concentrations().length;
    if (nomsTraceurs.length != nbVariables) {
      System.out.println("Erreur ! le nombre de traceur ne correspond pas entre le modele de qualit� d'eau et les concentrations initiales !!!");
      return null;
    }

    // Un seul pas de temps : 0
    double[] pasTps = {0};
    res.pasTemps = pasTps;
    SResultatsVariable[] descrVar = new SResultatsVariable[nbVariables];

    // description des nbVariables
    for (int i = 0; i < nbVariables; i++) {

      descrVar[i] = new SResultatsVariable();
      descrVar[i] = new SResultatsVariable();
      descrVar[i].nomCourt = "C" + (i + 1);
      descrVar[i].nomLong = "Concentration " + nomsTraceurs[i][1];
      descrVar[i].nbDecimal = 11;
      descrVar[i].unite = "";

    }

    res.variables = descrVar;

    List listeIResTempoSpatialBief = new ArrayList();
    SResultatsTemporelSpatialBief iresBief = null;
    List listeSection = null;
    List[] listeValeurs = new List[nbVariables];
    int iBiefPre = -5;

    for (int i = 0; i < concs.length; i++) {
      int iBief = concs[i].numeroBief();
      if (iBiefPre != iBief) {
        if ((iresBief != null) && (listeSection != null) && (AucunNull(listeValeurs))) {
          iresBief.abscissesSections = liste2Abscisse(listeSection);
          iresBief.valeursVariables = listes2valeurs(listeValeurs);
        }
        listeSection = new ArrayList();
        for (int j = 0; j < nbVariables; j++) {
          listeValeurs[j] = new ArrayList();
        }
        iresBief = new SResultatsTemporelSpatialBief();
        listeIResTempoSpatialBief.add(iresBief);
      }
      iBiefPre = iBief;
      listeSection.add(new Double(concs[i].abscisse()));
      for (int j = 0; j < nbVariables; j++) {
        listeValeurs[j].add(new Double(concs[i].concentrations()[j]));
      }
    }
    if ((iresBief != null) && (listeSection != null) && (AucunNull(listeValeurs))) {
      iresBief.abscissesSections = liste2Abscisse(listeSection);
      iresBief.valeursVariables = listes2valeurs(listeValeurs);
    }

    res.resultatsBiefs = ((SResultatsTemporelSpatialBief[]) listeIResTempoSpatialBief.toArray(new SResultatsTemporelSpatialBief[0]));

    Notifieur.getNotifieur().setEventMuet(false);

    return res;

  }

  /**
   * Conversion un tableau de MetierLigneEauPoint en resultats m�tier.
   *
   * @param lignes MetierLigneEauPoint[]
   * @return MetierResultatsTemporelSpatial
   */
  public final static MetierResultatsTemporelSpatial convertitDLigneEauPoint_IResultatTemporelSpatial(MetierLigneEauPoint[] lignes,
          MetierReseau reseau, double distanceEntreBief, boolean ConvertirEnAbsolu) {

    if (lignes == null) {
      return null;
    }
    if (lignes.length == 0) {
      return null;
    }

    Notifieur.getNotifieur().setEventMuet(true);
    MetierResultatsTemporelSpatial ires = new MetierResultatsTemporelSpatial();

    ires.resultatsCasier(false);
    ires.resultatsLiaison(false);
    ires.resultatsCalageAuto(false);
    ires.resultatsTracer(false);
    ires.resultatsPermanent(true);

// Un seul pas de temps : 0
    double[] pasTps = {
      0};
    ires.pasTemps(pasTps);
    MetierDescriptionVariable[] descrVar = new MetierDescriptionVariable[2];

// description des 3 variables
// description des cotes.
    descrVar[0] = new MetierDescriptionVariable();
    descrVar[0].nom("Z");
    descrVar[0].description("Cote de l eau");
    descrVar[0].nbDecimales(3);
    descrVar[0].type(EnumMetierTypeNombre.REEL);
    descrVar[0].unite(EnumMetierUnite.M);

// description des d�bit.
    descrVar[1] = new MetierDescriptionVariable();
    descrVar[1].nom("Q");
    descrVar[1].description("D�bit");
    descrVar[1].nbDecimales(3);
    descrVar[1].type(EnumMetierTypeNombre.REEL);
    descrVar[1].unite(EnumMetierUnite.M3_PAR_S);

    ires.descriptionVariables(descrVar);

    List listeIResTempoSpatialBief = new ArrayList();
    MetierResultatsTemporelSpatialBief iresBief = null;
    List listeSection = null;
    List listeValeursZ = null;
    List listeValeursQ = null;
    int iBiefPre = -5;
    for (int i = 0; i < lignes.length; i++) {
      int iBief = lignes[i].numeroBief();
      if (iBiefPre != iBief) {
        if ((iresBief != null) && (listeSection != null) && (listeValeursZ != null)
                && (listeValeursQ != null)) {
          iresBief.abscissesSections(liste2Abscisse(listeSection));
          iresBief.valeursVariables(listes2valeurs(listeValeursZ, listeValeursQ));
        }
        listeSection = new ArrayList();
        listeValeursZ = new ArrayList();
        listeValeursQ = new ArrayList();
        iresBief = new MetierResultatsTemporelSpatialBief();
        listeIResTempoSpatialBief.add(iresBief);
      }
      iBiefPre = iBief;
      if (ConvertirEnAbsolu) {
        listeSection.add(new Double(reseau.getAbscisseAbsolue(iBief,
                lignes[i].abscisse(), distanceEntreBief)));
      } else {
        listeSection.add(new Double(lignes[i].abscisse()));
      }
      listeValeursZ.add(new Double(lignes[i].cote()));
      listeValeursQ.add(new Double(lignes[i].debit()));
    }
    if ((iresBief != null) && (listeSection != null) && (listeValeursZ != null)
            && (listeValeursQ != null)) {
      iresBief.abscissesSections(liste2Abscisse(listeSection));
      iresBief.valeursVariables(listes2valeurs(listeValeursZ, listeValeursQ));
    }
    ires.resultatsBiefs((MetierResultatsTemporelSpatialBief[]) listeIResTempoSpatialBief.toArray(new MetierResultatsTemporelSpatialBief[0]));
    Notifieur.getNotifieur().setEventMuet(false);

    return ires;
  }

  /**
   * Conversion un tableau de lignes de ligne d'eau initiale en resultats mascaret.
   *
   * @param lignes MetierLigneEauPoint[]
   * @return SResultatsTemporelSpatial
   */
  public final static SResultatsTemporelSpatial convertitDLigneEauPoint_ResultatsTemporelSpatial(MetierLigneEauPoint[] lignes) {
    if (lignes == null) {
      return null;
    }
    if (lignes.length == 0) {
      return null;
    }

    SResultatsTemporelSpatial res = new SResultatsTemporelSpatial();
    res.resultatsCasier = false;
    res.resultatsLiaison = false;
    res.resultatsCalageAuto = false;
    res.resultatsTracer = false;
    res.resultatsPermanent = true;

    // Un seul pas de temps : 0
    double[] pasTps = {0};
    res.pasTemps = pasTps;
    SResultatsVariable[] descrVar = new SResultatsVariable[2];

    // description des 3 variables
    // description des cotes.
    descrVar[0] = new SResultatsVariable();
    descrVar[0].nomCourt = "Z";
    descrVar[0].nomLong = "Cote de l eau";
    descrVar[0].nbDecimal = 3;
    descrVar[0].unite = "m";

    // description des d�bit.
    descrVar[1] = new SResultatsVariable();
    descrVar[1].nomCourt = "Q";
    descrVar[1].nomLong = "D�bit";
    descrVar[1].nbDecimal = 3;
    descrVar[1].unite = "m3/s";

    res.variables = descrVar;

    List listeIResTempoSpatialBief = new ArrayList();
    SResultatsTemporelSpatialBief resBief = null;
    List listeSection = null;
    List listeValeursZ = null;
    List listeValeursQ = null;
    int iBiefPre = -5;
    for (int i = 0; i < lignes.length; i++) {
      int iBief = lignes[i].numeroBief();
      if (iBiefPre != iBief) {
        if ((resBief != null) && (listeSection != null) && (listeValeursZ != null) && (listeValeursQ != null)) {
          resBief.abscissesSections = liste2Abscisse(listeSection);
          resBief.valeursVariables = listes2valeurs(listeValeursZ, listeValeursQ);
        }
        listeSection = new ArrayList();
        listeValeursZ = new ArrayList();
        listeValeursQ = new ArrayList();
        resBief = new SResultatsTemporelSpatialBief();
        listeIResTempoSpatialBief.add(resBief);
      }
      iBiefPre = iBief;
      listeSection.add(new Double(lignes[i].abscisse()));
      listeValeursZ.add(new Double(lignes[i].cote()));
      listeValeursQ.add(new Double(lignes[i].debit()));
    }
    if ((resBief != null) && (listeSection != null) && (listeValeursZ != null) && (listeValeursQ != null)) {
      resBief.abscissesSections = liste2Abscisse(listeSection);
      resBief.valeursVariables = listes2valeurs(listeValeursZ, listeValeursQ);
    }
    res.resultatsBiefs = ((SResultatsTemporelSpatialBief[]) listeIResTempoSpatialBief.toArray(new SResultatsTemporelSpatialBief[0]));
    return res;
  }

  private final static double[] liste2Abscisse(List listeSection) {
    int taille = listeSection.size();
    double[] absc = new double[taille];
    for (int i = 0; i < absc.length; i++) {
      absc[i] = ((Double) listeSection.get(i)).doubleValue();
    }
    return absc;
  }

  private final static double[][][] listes2valeurs(List listeValeursZ, List listeValeursQ) {
    int taille = listeValeursZ.size();
    double[][][] vals = new double[2][1][taille];
    for (int i = 0; i < taille; i++) {
      vals[0][0][i] = ((Double) listeValeursZ.get(i)).doubleValue();
      vals[1][0][i] = ((Double) listeValeursQ.get(i)).doubleValue();
    }
    return vals;
  }

  private final static double[][][] listes2valeurs(List[] listeValeurs) {
    int taille = listeValeurs[0].size();
    double[][][] vals = new double[listeValeurs.length][1][taille];
    for (int i = 0; i < taille; i++) {
      for (int j = 0; j < listeValeurs.length; j++) {
        vals[j][0][i] = ((Double) listeValeurs[j].get(i)).doubleValue();
      }
    }
    return vals;
  }

  public static final SParametresGEO convertirParametresGeo(MetierBief[] biefs) {
    try {
      SParametresGEO geoMas = new SParametresGEO();
      geoMas.biefs = new SParametresBief[biefs.length];
      for (int i = 0; i < biefs.length; i++) {
        geoMas.biefs[i] = new SParametresBief();
        geoMas.biefs[i].nom = "Bief_" + (biefs[i].indice() + 1);
        geoMas.biefs[i].profils = new SParametresProfil[biefs[i].profils().length];
        for (int j = 0; j < biefs[i].profils().length; j++) {
          geoMas.biefs[i].profils[j] = new SParametresProfil();
          geoMas.biefs[i].profils[j].absc = biefs[i].profils()[j].abscisse();
          geoMas.biefs[i].profils[j].nom
                  = biefs[i].profils()[j].nom().trim().replace(' ', '_');
          geoMas.biefs[i].profils[j].avecGeoReferencement = biefs[i].profils()[j].avecGeoreferencement();
          geoMas.biefs[i].profils[j].infoGeoReferencement = biefs[i].profils()[j].infosGeoReferencement();
          geoMas.biefs[i].profils[j].pts
                  = new SParametresPt[biefs[i].profils()[j].points().length];
          for (int k = 0; k < biefs[i].profils()[j].points().length; k++) {
            geoMas.biefs[i].profils[j].pts[k] = new SParametresPt();
            geoMas.biefs[i].profils[j].pts[k].x
                    = biefs[i].profils()[j].points()[k].x;
            geoMas.biefs[i].profils[j].pts[k].y
                    = biefs[i].profils()[j].points()[k].y;
            geoMas.biefs[i].profils[j].pts[k].cx
                    = biefs[i].profils()[j].points()[k].cx;
            geoMas.biefs[i].profils[j].pts[k].cy
                    = biefs[i].profils()[j].points()[k].cy;
            if ((biefs[i].profils()[j].indiceLitMinGa() <= k)
                    && (k <= biefs[i].profils()[j].indiceLitMinDr())) {
              geoMas.biefs[i].profils[j].pts[k].lit = "B";
            } else {
              geoMas.biefs[i].profils[j].pts[k].lit = "T";
            }
          }
        }
      }
      return geoMas;
    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new RuntimeException(getS("Probl�me lors de la conversion en fichier g�om�trie") + "\n"
              + ex.getLocalizedMessage());
    }
  }

  public static final SGeoCasiers convertirParametresGeoCasiers(MetierCasier[] casiers) {
    try {
      SGeoCasiers geoCas = new SGeoCasiers();
      int nbCasier = casiers.length;
      geoCas.casiers = new SGeoCasier[nbCasier];
      for (int i = 0; i < nbCasier; i++) {
        geoCas.casiers[i] = convertirParametresGeoCasier(casiers[i]);
      }
      return geoCas;
    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new RuntimeException(getS("Probl�me lors de la conversion en fichier g�om�trie casier") + "\n"
              + ex.getLocalizedMessage());
    }
  }

  private static final SGeoCasier convertirParametresGeoCasier(MetierCasier casier) {
    SGeoCasier geoCas = new SGeoCasier();
    geoCas.nom = casier.nom().trim();
    MetierGeometrieCasier geo = casier.geometrie();
    if (geo.isPlanimetrage()) {
      MetierPlanimetrageCasier planim = (MetierPlanimetrageCasier) geo;
      double coteInitiale = planim.coteInitiale();
      double pas = planim.pasPlanimetrage();
      MetierPoint2D[] pts = geo.getPointsPlanimetrage();
      int taille = pts.length;
      geoCas.pts = new SPtCasier[taille];
      for (int i = 0; i < taille; i++) {
        MetierPoint2D pt = pts[i];
        geoCas.pts[i]
                = new SPtCasier((coteInitiale + (i * pas)), pt.x, pt.y, null);
      }
    }
    if (geo.isNuagePoints()) {
      MetierPoint[] ptsFront = geo.getPointsFrontiere();
      MetierPoint[] ptsInter = geo.getPointsInterieur();
      boolean ajoutUnPt = (!ptsFront[0].equals(ptsFront[ptsFront.length - 1]));
      int taille = ptsFront.length + ptsInter.length;
      if (ajoutUnPt) {
        taille++;
      }
      geoCas.pts = new SPtCasier[taille];
      for (int i = 0; i < ptsFront.length; i++) {
        MetierPoint pt = ptsFront[i];
        geoCas.pts[i] = new SPtCasier(pt.x, pt.y, pt.z, "F");
      }
      int decalage = 0;
      if (ajoutUnPt) {
        geoCas.pts[ptsFront.length] = geoCas.pts[0];
        decalage = 1;
      }
      for (int i = 0; i < ptsInter.length; i++) {
        MetierPoint pt = ptsInter[i];
        geoCas.pts[ptsFront.length + i + decalage]
                = new SPtCasier(pt.x, pt.y, pt.z, "I");
      }
    }
    return geoCas;
  }

  public static final SParametresREP convertirParametresRep(MetierParametresReprise repHydr) {
    try {
      return new SParametresREP(repHydr.contenu());
    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new RuntimeException(getS("Probl�me lors de la conversion en fichier Reprise en lecture") + "\n"
              + ex.getLocalizedMessage());
    }
  }

  public static SResultatsLIS convertirResultatsLisDamocles(byte[] listing) { // conversion Fichier listing Damocles
    return new SResultatsLIS(listing);
  }

  public static SResultatsLIS convertirResultatsLis(byte[] listing) { // conversion Fichier listing
    return new SResultatsLIS(listing);
  }

  public static SResultatsRUB convertirResultatsRub(byte[] contenu) { // conversion Fichier rubens
    return new SResultatsRUB(contenu);
  }

  public static SResultatsVariable[] convertirDescriptionsVariables2ResultatsVariable(MetierDescriptionVariable[] idesc) {
    SResultatsVariable[] descripVarMas = new SResultatsVariable[idesc.length];
    for (int i = 0; i < descripVarMas.length; i++) {
      descripVarMas[i]
              = new SResultatsVariable(
                      idesc[i].description(),
                      idesc[i].nom(),
                      ConvUnite.convertitUnite(idesc[i].unite()),
                      idesc[i].nbDecimales());
    }
    return descripVarMas;
  }

  public static SResultatsTemporelSpatial convertirResultatsTemporelSpatialH1dToMas(MetierResultatsTemporelSpatial ires) {
    SResultatsTemporelSpatial sres = new SResultatsTemporelSpatial();
    sres.variables = convertirDescriptionsVariables2ResultatsVariable(ires.descriptionVariables());
    sres.pasTemps = ires.pasTemps();
    sres.resultatsCasier = ires.resultatsCasier();
    sres.resultatsLiaison = ires.resultatsLiaison();
    sres.resultatsPermanent = ires.resultatsPermanent();
    sres.resultatsCalageAuto = ires.resultatsCalageAuto();
    sres.resultatsTracer = ires.resultatsTracer();

    int nbBief = ires.resultatsBiefs().length;
    sres.resultatsBiefs = new SResultatsTemporelSpatialBief[nbBief];
    for (int i = 0; i < nbBief; i++) {
      sres.resultatsBiefs[i] = new SResultatsTemporelSpatialBief();
      sres.resultatsBiefs[i].abscissesSections = ires.resultatsBiefs()[i].abscissesSections();
      sres.resultatsBiefs[i].valeursVariables = ires.resultatsBiefs()[i].valeursVariables();
    }
    return sres;
  }

  public static SResultatPasTemps[] convertirResultatsTemporelSpatial2ResultatsPasTemps(MetierResultatsTemporelSpatial ires) {
    int nbBief = ires.resultatsBiefs().length;
    int nbVar = ires.descriptionVariables().length;
    int nbPasTps = ires.pasTemps().length;
    SResultatPasTemps[] res = new SResultatPasTemps[nbPasTps];
    for (int i = 0; i < nbPasTps; i++) {
      res[i] = new SResultatPasTemps();
      res[i].t = ires.pasTemps()[i];
      res[i].resultatsBief = new SResultatBief[nbBief];
      for (int j = 0; j < res[i].resultatsBief.length; j++) {
        double[] abscSect = ires.resultatsBiefs()[j].abscissesSections();
        double[][][] vals = ires.resultatsBiefs()[j].valeursVariables();
        int nbSection = abscSect.length;
        res[i].resultatsBief[j] = new SResultatBief();
        res[i].resultatsBief[j].resultatsSection
                = new SResultatSection[nbSection];
        for (int k = 0; k < nbSection; k++) {
          res[i].resultatsBief[j].resultatsSection[k] = new SResultatSection();
          res[i].resultatsBief[j].resultatsSection[k].absc = abscSect[k];
          res[i].resultatsBief[j].resultatsSection[k].valeurs
                  = new double[nbVar];
          for (int l = 0; l < nbVar; l++) {
            res[i].resultatsBief[j].resultatsSection[k].valeurs[l]
                    = vals[l][i][k];
          }
        }
      }
    }
    return res;
  }

  public static SResultatsOPT convertirResultatsTemporelSpatial2ResultatsOpt(MetierResultatsTemporelSpatial res) {
    if (res == null) {
      return null;
    }
    if (res.descriptionVariables() == null) {
      return null;
    }
    if (res.descriptionVariables().length == 0) {
      return null;
    }
    if (res.pasTemps() == null) {
      return null;
    }
    if (res.pasTemps().length == 0) {
      return null;
    }
    if (res.resultatsBiefs() == null) {
      return null;
    }
    if (res.resultatsBiefs().length == 0) {
      return null;
    }
    if (res.resultatsBiefs()[0].abscissesSections() == null) {
      return null;
    }
    if (res.resultatsBiefs()[0].abscissesSections().length == 0) {
      return null;
    }
    if (res.resultatsBiefs()[0].valeursVariables() == null) {
      return null;
    }
    if (res.resultatsBiefs()[0].valeursVariables().length == 0) {
      return null;
    }
    SResultatsVariable[] vars
            = convertirDescriptionsVariables2ResultatsVariable(
                    res.descriptionVariables());
    SResultatPasTemps[] resPasTps
            = convertirResultatsTemporelSpatial2ResultatsPasTemps(res);
    SResultatsOPT r = new SResultatsOPT(vars, resPasTps);
    return r;
  }

  public static SLoiTracer[] convertirLoisTracer(MetierLoiTracer[] loisTracer, String[][] nomsTraceurs) {
    try {
      int nbLoi = loisTracer.length;
      SLoiTracer[] loisMas = new SLoiTracer[nbLoi];
      for (int i = 0; i < loisTracer.length; i++) {
        loisMas[i] = convertirDLoiTracer(loisTracer[i], nomsTraceurs);
      }

      return loisMas;
    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new RuntimeException(getS("Probl�me lors de la conversion en fichiers de lois traceur") + "\n"
              + ex.getLocalizedMessage());
    }
  }

  private static SLoiTracer convertirDLoiTracer(MetierLoiTracer iloiTracer, String[][] nomsTraceurs) {

    SLoiTracer loiMas = new SLoiTracer();
    int nbTracer = iloiTracer.conc()[0].length - 1;
    int nbLigne = iloiTracer.conc().length;

    loiMas.nom = iloiTracer.nom().trim().replace(' ', '_');
    loiMas.unitee = "S";

    // entete colonnes
    loiMas.entetesColonnes = new String[nbTracer + 1];
    loiMas.entetesColonnes[0] = "Temps (s)";
    for (int i = 0; i < nbTracer; i++) {
      loiMas.entetesColonnes[i + 1] = "C�_" + nomsTraceurs[i][0];
    }

    //tps
    loiMas.tps = new double[nbLigne];
    for (int i = 0; i < nbLigne; i++) {
      loiMas.tps[i] = iloiTracer.conc()[i][0];
    }

    double[][] tabConc = new double[nbLigne][nbTracer];
    for (int j = 0; j < nbTracer; j++) {
      for (int i = 0; i < nbLigne; i++) {
        tabConc[i][j] = iloiTracer.conc()[i][j + 1];
      }

    }
    loiMas.concentrations = tabConc;

    return loiMas;
  }

  public static SParamMeteoTracer convertirParametresMeteoQE(byte[] bs) {
    if (bs == null) {
      return null;
    }
    if (bs.length == 0) {
      return null;
    }
    SParamMeteoTracer paramsMeteo = new SParamMeteoTracer();
    paramsMeteo.paramMeteoTracer = bs;
    return paramsMeteo;
  }

  public static SParamPhysTracer[] convertirParametresPhysiqueQE(MetierParamPhysTracer[] iparams) {

    if (iparams == null) {
      return null;
    }
    if (iparams.length == 0) {
      return null;
    }
    SParamPhysTracer[] paramsPhyQE = new SParamPhysTracer[iparams.length];
    for (int i = 0; i < iparams.length; i++) {
      paramsPhyQE[i] = new SParamPhysTracer();
      paramsPhyQE[i].nomParamPhys = iparams[i].nomParamPhys();
      paramsPhyQE[i].valeurParamPhys = iparams[i].valeurParamPhys();
    }
    return paramsPhyQE;
  }

}
