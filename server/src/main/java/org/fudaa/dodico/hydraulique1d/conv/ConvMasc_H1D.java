/*
 * @file         ConvMasc_H1D.java
 * @creation     2004-03-01
 * @modification $Date: 2008-03-04 15:46:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.conv;

import gnu.trove.TDoubleArrayList;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JOptionPane;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.corba.mascaret.SParametresApporDeversoirs;
import org.fudaa.dodico.corba.mascaret.SParametresCAS;
import org.fudaa.dodico.corba.mascaret.SParametresCalage;
import org.fudaa.dodico.corba.mascaret.SParametresCalageAuto;
import org.fudaa.dodico.corba.mascaret.SParametresCasier;
import org.fudaa.dodico.corba.mascaret.SParametresCondInit;
import org.fudaa.dodico.corba.mascaret.SParametresConfluents;
import org.fudaa.dodico.corba.mascaret.SParametresGEO;
import org.fudaa.dodico.corba.mascaret.SParametresGen;
import org.fudaa.dodico.corba.mascaret.SParametresGeoReseau;
import org.fudaa.dodico.corba.mascaret.SParametresImpressResult;
import org.fudaa.dodico.corba.mascaret.SParametresLiaisons;
import org.fudaa.dodico.corba.mascaret.SParametresLoisHydrau;
import org.fudaa.dodico.corba.mascaret.SParametresModelPhy;
import org.fudaa.dodico.corba.mascaret.SParametresNum;
import org.fudaa.dodico.corba.mascaret.SParametresPlanimMaillage;
import org.fudaa.dodico.corba.mascaret.SParametresREP;
import org.fudaa.dodico.corba.mascaret.SParametresSingularite;
import org.fudaa.dodico.corba.mascaret.SParametresTemp;
import org.fudaa.dodico.corba.mascaret.SParametresTraceur;
import org.fudaa.dodico.corba.mascaret.SParametresVarStock;
import org.fudaa.dodico.corba.mascaret.SParametresZonesCalageAuto;
import org.fudaa.dodico.corba.mascaret.SResultatBief;
import org.fudaa.dodico.corba.mascaret.SResultatPasTemps;
import org.fudaa.dodico.corba.mascaret.SResultatSection;
import org.fudaa.dodico.corba.mascaret.SResultatsOPT;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatialBief;
import org.fudaa.dodico.corba.mascaret.SResultatsVariable;
import org.fudaa.dodico.hydraulique1d.Hydraulique1dResource;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierCondLimiteImposee;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierCritereArret;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierLimiteCalcule;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierMethodeMaillage;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierOptionStockage;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierSensDebitLiaison;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCompositionLits;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeFrottement;
import org.fudaa.dodico.hydraulique1d.metier.MetierBarragePrincipal;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSections;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeries;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeriesUnitaire;
import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauPoint;
import org.fudaa.dodico.hydraulique1d.metier.MetierLimite;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierMaillage;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresReprise;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresResultats;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresTemporels;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatialBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierSite;
import org.fudaa.dodico.hydraulique1d.metier.MetierZone;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;
import org.fudaa.dodico.hydraulique1d.metier.MetierZonePlanimetrage;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.EnumMetierMethodeOpt;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.EnumMetierTypeLit;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierApportCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierMesureCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierChenalLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierNuagePointsCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierOrificeLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierPlanimetrageCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierSeuilLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierSiphonLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierTopologieCasierCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierTopologieRiviereCasier;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimniHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimnigramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiOuvertureVanne;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierModeleQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierOptionConvec;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierOptionDiffus;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierTypeCondLimiteTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierLimiteQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierOptionTraceur;
import org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierTypeSource;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierBarrage;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementZCoefQ;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilDenoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLimniAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilNoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAval;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilVanne;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSource;
import org.fudaa.dodico.mascaret.DescriptionVariables;

/**
 * Classe qui contient les m�thodes statiques permettant de convertir le mod�le mascaret (les r�sultats du code) en mod�le m�tier hydraulique1D.
 *
 * @version $Revision: 1.4 $ $Date: 2008-03-04 15:46:53 $ by $Author: opasteur $
 * @author Jean-Marc Lacombe
 */
public class ConvMasc_H1D {

  private static StringBuffer sbufRecup_ = new StringBuffer();
  final static MetierResultatsTemporelSpatial RES_OPTHYCA_VIDE
          = convertirResultatsOpt2ResultatsTemporelSpatial(
                  new SResultatsOPT(new SResultatsVariable[0], new SResultatPasTemps[0]));
  final static MetierParametresReprise RES_REP_VIDE
          = convertirResultatsRep(new SParametresREP(new byte[0]));

  public static final String getS(final String _s) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(_s);
  }

  public static MetierParametresReprise convertirResultatsRep(SParametresREP param) { // conversion Fichier de reprise en �criture
    MetierParametresReprise repHydr
            = new MetierParametresReprise();
    if (param.contenu.length == 0) {
      return repHydr;
    }
    repHydr.fichier("");
    String chaine = new String(param.contenu);
    int index = chaine.indexOf("FIN"); // le temps final se trouve apr�s FIN
    if (index == -1) {
      JOptionPane.showInternalMessageDialog(null, getS("Le fichier de reprise produit par Mascaret n'est pas valide !"),
              getS("Attention !"),
              JOptionPane.ERROR_MESSAGE);
    }

    String chaineSansLigneInitiale = chaine.substring(index + 3);
    StringTokenizer st = new StringTokenizer(chaineSansLigneInitiale);
    repHydr.tFinal(Double.parseDouble(st.nextToken()));
    repHydr.contenu(chaine.getBytes());
    return repHydr;
  }

  public static MetierDescriptionVariable[] convertirResultatsVariables2DescriptionVariable(SResultatsVariable[] descVars) {
    if (descVars == null) {
      return null;
    }
    MetierDescriptionVariable[] res = new MetierDescriptionVariable[descVars.length];
    for (int i = 0; i < descVars.length; i++) {
      if ("".equals(descVars[i].nomLong)) { // Au cas ou la variable n'est pas correctement renseignee.
        res[i] = new MetierDescriptionVariable();
        String[] sdescr = DescriptionVariables.getDescription(descVars[i].nomCourt);
        res[i].description(sdescr[0]);
        res[i].nom(descVars[i].nomCourt);
        res[i].unite(ConvUnite.convertitUnite(sdescr[1]));
        res[i].nbDecimales(Integer.parseInt(sdescr[2]));
      } else {
        res[i] = new MetierDescriptionVariable();
        res[i].description(descVars[i].nomLong);
        res[i].nom(descVars[i].nomCourt);
        res[i].nbDecimales(descVars[i].nbDecimal);
        String unite = descVars[i].unite;
        res[i].unite(ConvUnite.convertitUnite(unite));
      }
    }
    return res;
  }

  public static double[] convertirResultatsPasTemps(SResultatPasTemps[] sres) {
    double res[] = new double[sres.length];
    for (int i = 0; i < res.length; i++) {
      res[i] = sres[i].t;
    }
    return res;
  }

  public static MetierResultatsTemporelSpatialBief[] convertirResultatsPasTemps2ResultatsTemporelSpatialBief(
          SResultatPasTemps[] sres,
          int nbVar) {
    if (sres.length == 0) {
      return new MetierResultatsTemporelSpatialBief[0];
    }
    if (nbVar == 0) {
      return new MetierResultatsTemporelSpatialBief[0];
    }
    int nbPasTps = sres.length;
    int nbBief = sres[0].resultatsBief.length;
    //il y a le m�me nombre de bief � tous la pas de temps
    MetierResultatsTemporelSpatialBief[] res
            = new MetierResultatsTemporelSpatialBief[nbBief];
    for (int i = 0; i < res.length; i++) {
      res[i]
              = new MetierResultatsTemporelSpatialBief();
      SResultatSection[] sectionsBief
              = sres[0].resultatsBief[i].resultatsSection;
      double[] absc = new double[sectionsBief.length];
      double[][][] valeurs = new double[nbVar][nbPasTps][absc.length];
      for (int j = 0; j < absc.length; j++) {
        absc[j] = sectionsBief[j].absc;
      }
      res[i].abscissesSections(absc);
      res[i].valeursVariables(valeurs);
    }
    for (int i = 0; i < sres.length; i++) { //nb de Pas Temps
      SResultatPasTemps resPasTps = sres[i];
      for (int j = 0; j < resPasTps.resultatsBief.length; j++) { //nb de Bief
        SResultatBief resBief = resPasTps.resultatsBief[j];
        for (int k = 0;
                k < resBief.resultatsSection.length;
                k++) { //nb de section
          SResultatSection resSec = resBief.resultatsSection[k];
          for (int l = 0; l < resSec.valeurs.length; l++) { //nb de variable
            double[][][] vals = res[j].valeursVariables();
            vals[l][i][k] = resSec.valeurs[l];
          }
        }
      }
    }
    return res;
  }

  public static MetierResultatsTemporelSpatial convertirResultatsOpt2ResultatsTemporelSpatial(SResultatsOPT opt) {
    MetierResultatsTemporelSpatial res
            = new MetierResultatsTemporelSpatial();
    res.descriptionVariables(convertirResultatsVariables2DescriptionVariable(opt.variables));
    res.valPas(convertirResultatsPasTemps(opt.resultatsPasTemps));
    res.resultatsBiefs(
            convertirResultatsPasTemps2ResultatsTemporelSpatialBief(
                    opt.resultatsPasTemps,
                    res.descriptionVariables().length));
    return res;
  }

  public static MetierResultatsTemporelSpatial convertirResultatsTemporelSpatialMasToH1d(SResultatsTemporelSpatial sres) {
    if (sres == null || sres.resultatsBiefs == null) {
      return null;
    }
    MetierResultatsTemporelSpatial ires = new MetierResultatsTemporelSpatial();

    ires.resultatsCasier(sres.resultatsCasier);
    ires.resultatsLiaison(sres.resultatsLiaison);
    ires.resultatsTracer(sres.resultatsTracer);
    ires.resultatsPermanent(sres.resultatsPermanent);
    ires.resultatsCalageAuto(sres.resultatsCalageAuto);
    ires.descriptionVariables(convertirResultatsVariables2DescriptionVariable(sres.variables));
    ires.valPas(sres.pasTemps);
    int nbBiefRes = sres.resultatsBiefs.length;
    MetierResultatsTemporelSpatialBief[] iresBiefs = new MetierResultatsTemporelSpatialBief[nbBiefRes];
    ires.resultatsBiefs(iresBiefs);
    for (int i = 0; i < nbBiefRes; i++) {
      iresBiefs[i] = new MetierResultatsTemporelSpatialBief();
      SResultatsTemporelSpatialBief sresBief = sres.resultatsBiefs[i];
      iresBiefs[i].abscissesSections(sresBief.abscissesSections);
      iresBiefs[i].valeursVariables(sresBief.valeursVariables);
    }
    return ires;
  }

  public static MetierLigneEauPoint[] convertirResultatsTemporelSpatial2LigneEauPoint(SResultatsTemporelSpatial param) { // conversion Fichier de reprise en �criture
    //Vector newLigneEau= new Vector();
    int indexeCote = rechercheIndexe("Z", param.variables);
    int indexeDebitMin = rechercheIndexe("QMIN", param.variables);
    int indexeDebitMaj = rechercheIndexe("QMAJ", param.variables);
    int indexeFrotMin = rechercheIndexe("S1", param.variables);
    int indexeFrotMaj = rechercheIndexe("S2", param.variables);

    int indexDernierPasTps = param.pasTemps.length - 1;
    if (indexDernierPasTps == -1) {
      return new MetierLigneEauPoint[0];
    }

    int nbBief = param.resultatsBiefs.length;
    if (nbBief < 1) {
      return new MetierLigneEauPoint[0];
    }
    int nbSection = 0;
    for (int i = 0; i < nbBief; i++) {
      nbSection += param.resultatsBiefs[i].abscissesSections.length;
    }
    MetierLigneEauPoint[] res = new MetierLigneEauPoint[nbSection];
    int indexLigneEau = 0;
    for (int i = 0; i < nbBief; i++) {
      SResultatsTemporelSpatialBief resBiefi = param.resultatsBiefs[i];
      double[] vAbsc = resBiefi.abscissesSections;
      double[] vCote = null;
      if (indexeCote != -1) {
        vCote = resBiefi.valeursVariables[indexeCote][indexDernierPasTps];
      }
      double[] vDebitMin = null;
      if (indexeDebitMin != -1) {
        vDebitMin = resBiefi.valeursVariables[indexeDebitMin][indexDernierPasTps];
      }
      double[] vDebitMaj = null;
      if (indexeDebitMaj != -1) {
        vDebitMaj = resBiefi.valeursVariables[indexeDebitMaj][indexDernierPasTps];
      }
      double[] vFrotMin = null;
      if (indexeFrotMin != -1) {
        vFrotMin = resBiefi.valeursVariables[indexeFrotMin][indexDernierPasTps];
      }
      double[] vFrotMaj = null;
      if (indexeFrotMaj != -1) {
        vFrotMaj = resBiefi.valeursVariables[indexeFrotMaj][indexDernierPasTps];
      }
      for (int j = 0; j < vCote.length; j++) {
        res[indexLigneEau] = new MetierLigneEauPoint();
        res[indexLigneEau].numeroBief(i + 1);
        if (vAbsc != null) {
          res[indexLigneEau].abscisse(vAbsc[j]);
        }
        if (vCote != null) {
          res[indexLigneEau].cote(vCote[j]);
        }
        if ((vDebitMin != null) && (vDebitMin != null)) {
          res[indexLigneEau].debit(vDebitMin[j] + vDebitMaj[j]);
        }
        if (vFrotMin != null) {
          res[indexLigneEau].coefFrottementMin(vFrotMin[j]);
        }
        if (vFrotMaj != null) {
          res[indexLigneEau].coefFrottementMaj(vFrotMaj[j]);
        }
        indexLigneEau++;
      }
    }
    return res;
  }

  /**
   * Conversion des r�sultats it�ratifs de calage vers zones de frottements cal�es.
   *
   * @param _zones La d�finition des zones � caler
   */
  public static MetierZoneFrottement[] convertirResultatsCalage2ZonesFrottement(
          MetierBief[] _biefs, SParametresZonesCalageAuto _zones, SResultatsTemporelSpatial _res) {

    MetierZoneFrottement[] r = new MetierZoneFrottement[_zones.nbZones];

    int icmin = rechercheIndexeInMultipleVariables("KMIN", _res.variables);
    int icmaj = rechercheIndexeInMultipleVariables("KMAJ", _res.variables);

    int isect = 0;
    for (int i = 0; i < r.length; i++) {
      r[i] = new MetierZoneFrottement();
      r[i].biefRattache(_biefs[0]);
      r[i].abscisseDebut(_zones.absDebZone[i]);
      r[i].abscisseFin(_zones.absFinZone[i]);
      while (_res.resultatsBiefs[0].abscissesSections[isect] < _zones.absDebZone[i]) {
        isect++;
      }
      r[i].coefMineur(icmin == -1 ? -1 : _res.resultatsBiefs[0].valeursVariables[icmin][_res.pasTemps.length - 1][isect]);
      r[i].coefMajeur(icmaj == -1 ? -1 : _res.resultatsBiefs[0].valeursVariables[icmaj][_res.pasTemps.length - 1][isect]);
    }

    return r;
  }

  private static int rechercheIndexe(
          String nomCourt,
          SResultatsVariable[] variables) {
    int indexe = -1;
    for (indexe = 0; indexe < variables.length; indexe++) {
      if (nomCourt.equals(variables[indexe].nomCourt)) {
        return indexe;
      }
    }
    return -1;
  }
  
  private static int rechercheIndexeInMultipleVariables(
          String nomCourt,
          SResultatsVariable[] variables) {
    int indexe = -1;
    for (indexe = 0; indexe < variables.length; indexe++) {
      if (variables[indexe].nomCourt != null && 
    		  variables[indexe].nomCourt.contains(nomCourt)) {
        return indexe;
      }
    }
    return -1;
  }

  /**
   * Conversion des profils structures en profils metiers.
   *
   * @param _params Les parametres du fichier des profils.
   * @return Les biefs, renseign�s uniquement avec la geometrie.
   */
  public static MetierBief[] convertirGEO(SParametresGEO _params) {
    MetierBief[] biefs = new MetierBief[_params.biefs.length];

    for (int i = 0; i < _params.biefs.length; i++) {
      biefs[i] = new MetierBief();
      biefs[i].indice(i);

      for (int j = 0; j < _params.biefs[i].profils.length; j++) {
        biefs[i].creeProfil();
        biefs[i].profils()[j].abscisse(_params.biefs[i].profils[j].absc);
        biefs[i].profils()[j].nom(_params.biefs[i].profils[j].nom);
        biefs[i].profils()[j].avecGeoreferencement(_params.biefs[i].profils[j].avecGeoReferencement);
        biefs[i].profils()[j].infosGeoReferencement(_params.biefs[i].profils[j].infoGeoReferencement);
        biefs[i].profils()[j].points(new MetierPoint2D[_params.biefs[i].profils[j].pts.length]);
        int indMinGa = _params.biefs[i].profils[j].pts.length;
        int indMinDr = 0;
        for (int k = 0; k < _params.biefs[i].profils[j].pts.length; k++) {
          biefs[i].profils()[j].points()[k] = new MetierPoint2D();
          biefs[i].profils()[j].points()[k].x = _params.biefs[i].profils[j].pts[k].x;
          biefs[i].profils()[j].points()[k].y = _params.biefs[i].profils[j].pts[k].y;
          biefs[i].profils()[j].points()[k].cx = _params.biefs[i].profils[j].pts[k].cx;
          biefs[i].profils()[j].points()[k].cy = _params.biefs[i].profils[j].pts[k].cy;
          if ("B".equals(_params.biefs[i].profils[j].pts[k].lit)) {
            indMinGa = Math.min(indMinGa, k);
            indMinDr = Math.max(indMinDr, k);
          }
        }
        biefs[i].profils()[j].indiceLitMinDr(indMinDr);
        biefs[i].profils()[j].indiceLitMinGa(indMinGa);
      }
    }
    return biefs;
  }

  /**
   * Conversion des parametres cas Mascaret vers etude.
   *
   * @param _params Les parametres du fichier CAS.
   * @param _etude L'objet metier etude
   * @param _biefsGeo Les biefs lus sur le fichier de geometrie.
   * @return Le stringbuffer des op�rations r�alis�es.
   */
  public static StringBuffer convertirCas(SParametresCAS _params, MetierEtude1d _etude, MetierBief[] _biefsGeo) {
    sbufRecup_.setLength(0);

    // Les parametres generaux
    if (_params.parametresGen != null) {
      convertirParametresGen(_etude.paramGeneraux(), _params.parametresGen);
    }
    // Le mod�le physique
    if (_params.parametresModelPhy != null) {
      convertirParametresModelPhy(_etude.paramGeneraux(), _params.parametresModelPhy);
    }
    // Les parametres numeriques
    if (_params.parametresNum != null) {
      convertirParametresNum(_etude.paramGeneraux(), _params.parametresNum);
    }
    // Les parametres de temps
    if (_params.parametresTemp != null) {
      convertirParametresTemp(_etude.paramTemps(), _params.parametresTemp);
    }
    // Les lois
    if (_params.parametresLoisHydrau != null) {
      convertirParametresLoiHydrau(_etude.donneesHydro(), _params.parametresLoisHydrau);
    }
    // Le reseau
    if (_params.parametresGeoReseau != null) {
      convertirParametresGeoReseau(_etude, _biefsGeo, _params.parametresGeoReseau);
    }
    // Les confluents
    if (_params.parametresConfluents != null) {
      convertirParametresConfluents(_etude.reseau().noeudsConnectesBiefs(), _params.parametresConfluents);
    }
    // Les singularites (avant les lois, pour lois speciales)
//    convertirParametresSingularite(_etude,_params.parametresSingularite);
    // Planimetrage, maillage.
    if (_params.parametresPlanimMaillage != null) {
      convertirParametresPlanimMaillage(_etude, _params.parametresPlanimMaillage);
    }
    // Casiers
    if (_params.parametresCasier != null) {
      convertirParametresCasiers(_etude, _params.parametresCasier);
    }
    // Les apports et deversoirs
    if (_params.parametresApporDeversoirs != null) {
      convertirParametresApports(_etude, _params.parametresApporDeversoirs);
    }
    // Les liaisons
    if (_params.parametresCasier != null && _params.parametresCasier.liaisons != null) {
      convertirParametresLiaisons(_etude, _params.parametresCasier.liaisons);
    }
    // Les singularit�s
    MetierLoiHydraulique[] loisASupprimer = null;
    if (_params.parametresSingularite != null) {
      loisASupprimer = convertirParametresSingularite(_etude, _params.parametresSingularite);
    }
    // Le calage
    if (_params.parametresCalage != null) {
      convertirParametresCalage(_etude, _params.parametresCalage);
    }
    // Le calage auto
    if (_params.parametresCalageAuto != null) {
      convertirParametresCalageAuto(_etude, _params.parametresCalageAuto);
    }
    // Les traceurs
    if (_params.parametresTracer != null) {
      convertirParametresTracer(_etude, _params.parametresTracer);
    }
    // Les conditions initiales
    if (_params.parametresCondInit != null) {
      convertirParametresCondInit(_etude, _params.parametresCondInit);
    }
    // Les impression de resultats
    if (_params.parametresImpressResult != null) {
      convertirParametresImpressResult(_etude, _params.parametresImpressResult);
    }
    // Les variables calculees : Non recup�r�es.
    // Les variables stock�es
    if (_params.parametresVarStock != null) {
      convertirParametresVarStock(_etude.paramResultats(), _params.parametresVarStock);
    }

    // Suppression des "fausses" lois.
    if (loisASupprimer != null && _etude.donneesHydro() != null) {
      _etude.donneesHydro().supprimeLois(loisASupprimer);
    }

    return sbufRecup_;
  }

  private final static SParametresTraceur convertirParametresTracer(MetierEtude1d _etd, SParametresTraceur _params) {

    // Params g�n�raux
    _etd.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs(_params.presenceTraceurs);

    if (!_etd.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs()) {
      return null; // Pas de qualit� d'eau.
    }
    _etd.qualiteDEau().parametresModeleQualiteEau().nbTraceur(_params.nbTraceur);
    String[][] vvNoms = new String[_params.nbTraceur][2];
    for (int i = 0; i < vvNoms.length; i++) {
      vvNoms[i][0] = "TRA" + (i + 1);
      vvNoms[i][1] = "Traceur " + (i + 1);
    }
    _etd.qualiteDEau().parametresModeleQualiteEau().vvNomTracer(vvNoms);

    if (_etd.qualiteDEau().parametresModeleQualiteEau().nbTraceur() == 0) {
      return null; // Pas de traceur.
    }
    // Parametres Diffusion - Convection

    MetierOptionTraceur[] options = new MetierOptionTraceur[_params.nbTraceur];
    for (int i = 0; i < options.length; i++) {
      options[i] = new MetierOptionTraceur();
      options[i].convectionDuTraceur(_params.parametresConvecDiffu.convectionTraceurs[i]);
      options[i].diffusionDuTraceur(_params.parametresConvecDiffu.diffusionTraceurs[i]);
    }
    _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers(options);

    if (_params.parametresConvecDiffu.optionConvection == 2) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionConvection(EnumMetierOptionConvec.HYP1FA_NON_CONS);
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().LimitPente(false);
    } else if (_params.parametresConvecDiffu.optionConvection == 3) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionConvection(EnumMetierOptionConvec.HYP1FA_CONS);
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().LimitPente(false);
    } else {// Volumes finis
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionConvection(EnumMetierOptionConvec.VOLUMES_FINIS);
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().ordreSchemaConvec(_params.parametresConvecDiffu.ordreSchemaConvec);

      if (_params.parametresConvecDiffu.ordreSchemaConvec == 2) {
        _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().paramW(_params.parametresConvecDiffu.paramW);
      }
      if (_params.parametresConvecDiffu.ordreSchemaConvec != 1) {
        _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().LimitPente(_params.parametresConvecDiffu.LimitPente);
      } else {
        _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().LimitPente(false);
      }
    }

    /**
     * OPTION DE CALCUL DE LA DISPERSION POUR LES TRACEURS les choix sont : 1 : Coefficient de diffusion constant 2 : Elder (1959) 3 : Fisher (1975) 4
     * : Liu (1977) 5 : Iwasa et Aya (1991) 6 : McQuivey et Keefer (1974) 7 : Kashefipur et Falconer (2002 ) 8 : Magazine et al. (1988) 9 : Koussis et
     * Rodriguez-Mirasol (1998) 10: Seo et Cheong (1998) 11: Deng et al. (2001)
     */
    if (_params.parametresConvecDiffu.optionCalculDiffusion == 1) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.K_C1U_C2);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 2) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.ELDER);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 3) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.FISHER);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 4) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.LIU);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 5) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.ISAWA_AGA);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 6) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.MC_QUIVEY_KEEFER);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 7) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.KASHEFIPUR_FALCONER);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 8) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.MAGAZINE_AL);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 9) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.KOUSSIS_RODRIGUEZ);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 10) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.SEO_CHEONG);
    } else if (_params.parametresConvecDiffu.optionCalculDiffusion == 11) {
      _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().optionCalculDiffusion(EnumMetierOptionDiffus.DENG_AL);
    } else // Pas autoris�
    {
      sbufRecup_.append("<br>").append(getS("Tracer : 'OPTION DE CALCUL DE LA DISPERSION POUR LES TRACEURS' doit �tre compris entre 1 et 11"));
    }

    _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().coeffDiffusion1(_params.parametresConvecDiffu.coeffDiffusion1);
    _etd.qualiteDEau().parametresGenerauxQualiteDEau().parametresConvecDiffu().coeffDiffusion2(_params.parametresConvecDiffu.coeffDiffusion2);

    // Parametre numeriques QualiteEau
    if (_params.parametresNumQualiteEau.modeleQualiteEau == 2) {
      _etd.qualiteDEau().parametresModeleQualiteEau().modeleQualiteEau(EnumMetierModeleQualiteDEau.O2);
    } else if (_params.parametresNumQualiteEau.modeleQualiteEau == 3) {
      _etd.qualiteDEau().parametresModeleQualiteEau().modeleQualiteEau(EnumMetierModeleQualiteDEau.BIOMASS);
    } else if (_params.parametresNumQualiteEau.modeleQualiteEau == 4) {
      _etd.qualiteDEau().parametresModeleQualiteEau().modeleQualiteEau(EnumMetierModeleQualiteDEau.EUTRO);
    } else if (_params.parametresNumQualiteEau.modeleQualiteEau == 5) {
      _etd.qualiteDEau().parametresModeleQualiteEau().modeleQualiteEau(EnumMetierModeleQualiteDEau.MICROPOL);
    } else if (_params.parametresNumQualiteEau.modeleQualiteEau == 6) {
      _etd.qualiteDEau().parametresModeleQualiteEau().modeleQualiteEau(EnumMetierModeleQualiteDEau.THERMIC);
    } else { // EnumMetierModeleQualiteDEau.TRANSPORT_PUR
      _etd.qualiteDEau().parametresModeleQualiteEau().modeleQualiteEau(EnumMetierModeleQualiteDEau.TRANSPORT_PUR);
    }

    _etd.qualiteDEau().parametresModeleQualiteEau().frequenceCouplHydroTracer(_params.parametresNumQualiteEau.frequenceCouplHydroTracer);

    // parametres d'impression
    _etd.paramResultats().optionsListingTracer().concentInit(_params.parametresImpressTracer.concentInit);
    _etd.paramResultats().optionsListingTracer().loiTracer(_params.parametresImpressTracer.loiTracer);
    _etd.paramResultats().optionsListingTracer().bilanTracer(_params.parametresImpressTracer.bilanTracer);
    _etd.paramResultats().optionsListingTracer().concentrations(_params.parametresImpressTracer.concentrations);
    _etd.paramResultats().postOpthycaQualiteDEau(_params.parametresImpressTracer.formatFichResultat == 2);

    // Les lois
    MetierLoiTracer[] loisTraceur = new MetierLoiTracer[_params.parametresLoisTracer.nbLoisTracer];
    for (int i = 0; i < loisTraceur.length; i++) {
      loisTraceur[i] = new MetierLoiTracer();
      _etd.donneesHydro().ajouteLoi(loisTraceur[i]); // L'ajout de la loi change le nom de la loi !!
      loisTraceur[i].nom(_params.parametresLoisTracer.loisTracer[i].nom);
      double[][] vals = new double[_params.nbTraceur + 1][];
      vals[0] = _params.parametresLoisTracer.loisTracer[i].tps;
      for (int t = 0; t < _params.nbTraceur; t++) {
        vals[t + 1] = _params.parametresLoisTracer.loisTracer[i].concentrations[t].concentrations;
      }
      vals = CtuluLibArray.transpose(vals);

      loisTraceur[i].setPoints(vals);
    }

    // Conditions Limites sur extremites libres
    MetierExtremite[] extrs = _etd.reseau().extremitesLibres();
    for (int i = 0; i < extrs.length; i++) {
      MetierLimiteQualiteDEau lim = extrs[i].conditionLimiteQualiteDEau();
      // typeCondLimiteTracer
      if (_params.parametresCondLimTracer.typeCondLimTracer[i] == 1) {
        lim.typeCondLimiteTracer(EnumMetierTypeCondLimiteTracer.NEUMANN);
      } else {
        lim.typeCondLimiteTracer(EnumMetierTypeCondLimiteTracer.DIRICHLET);
      }

      // Numero de loi
      if (_params.parametresCondLimTracer.numLoiCondLimTracer[i] == 0) {
        lim.loi(null);
      } else {
        lim.loi(loisTraceur[_params.parametresCondLimTracer.numLoiCondLimTracer[i] - 1]);
      }
    }

    // Conditions initiales
    if (_params.parametresConcInitTracer.presenceConcInit) {
      sbufRecup_.append("<br>").append("<b>Tracer</b> :").append(getS("Les concentrations initiales ne sont pas recup�r�es"));
    }

    // Les sources
    for (int i = 0; i < _params.parametresSourcesTraceurs.nbSources; i++) {
      MetierSource source = new MetierSource();
      source.nom(_params.parametresSourcesTraceurs.noms[i]);
      if (_params.parametresSourcesTraceurs.typeSources[i] == 3) {
        source.type(EnumMetierTypeSource.FLUX_UNITE_TEMPS);
      } else if (_params.parametresSourcesTraceurs.typeSources[i] == 2) {
        source.type(EnumMetierTypeSource.SURFACIQUE);
      } else {  // volumique
        source.type(EnumMetierTypeSource.VOLUME);
      }
      source.abscisse(_params.parametresSourcesTraceurs.abscisses[i]);
      source.longueur(_params.parametresSourcesTraceurs.longueurs[i]);
      source.loi(loisTraceur[_params.parametresSourcesTraceurs.numLoi[i] - 1]);

      int numBief = _params.parametresSourcesTraceurs.numBranche[i];
      _etd.reseau().biefs()[numBief - 1].ajouteSingularite(source);
    }

    return _params;
  }

  private final static void convertirParametresCalageAuto(MetierEtude1d _etd, SParametresCalageAuto _params) {

    // Param�tres g�n�raux
    if (!_params.parametres.modeCalageAuto) {
      return;
    }
/*
    if (_params.parametres.methOptimisation == 1) {
      _etd.calageAuto().parametres().methodeOpt(EnumMetierMethodeOpt.DESCENTE_OPTIMALE);
    } else if (_params.parametres.methOptimisation == 2) {
      _etd.calageAuto().parametres().methodeOpt(EnumMetierMethodeOpt.CASIER_NEWTON);
    } else {
      _etd.calageAuto().parametres().methodeOpt(EnumMetierMethodeOpt.ALGO_GENETIQUE);
    }
*/
    _etd.calageAuto().parametres().nbMaxIterations(_params.parametres.nbMaxIterations);
    //_etd.calageAuto().parametres().pasGradient(_params.parametres.pasGradient);
    _etd.calageAuto().parametres().precision(_params.parametres.precision);
    //_etd.calageAuto().parametres().roInit(_params.parametres.roInit);
    _etd.calageAuto().parametres().typeCoefficient(EnumMetierTypeCoefficient.STRICKLER);
    if (_params.parametres.typeLit == 1) {
      _etd.calageAuto().parametres().typeLit(EnumMetierTypeLit.MINEUR);
    } else if (_params.parametres.typeLit == 2){
      _etd.calageAuto().parametres().typeLit(EnumMetierTypeLit.MAJEUR);
    }else 
        _etd.calageAuto().parametres().typeLit(EnumMetierTypeLit.MINEUR_MAJEUR);

    // Zones de frottement initiaux
    MetierZoneFrottement[] zones = new MetierZoneFrottement[_params.zones.nbZones];
    for (int i = 0; i < zones.length; i++) {
      zones[i] = new MetierZoneFrottement();
      zones[i].abscisseDebut(_params.zones.absDebZone[i]);
      zones[i].abscisseFin(_params.zones.absFinZone[i]);
      zones[i].coefMineur(_params.zones.coefLitMin[i]);
      zones[i].coefMajeur(_params.zones.coefLitMaj[i]);
        //TOBEREPLACE      
      zones[i].coefLitMajBinf(_params.zones.coefLitMajBinf[i]);
      zones[i].coefLitMajBsup(_params.zones.coefLitMajBsup[i]);
      zones[i].coefLitMinBinf(_params.zones.coefLitMinBinf[i]);
      zones[i].coefLitMinBsup(_params.zones.coefLitMinBsup[i]);
      
      zones[i].biefRattache(_etd.reseau().biefs()[0]); // Le calage n'existe que si 1 seul bief existe.
    }
    _etd.calageAuto().zonesFrottement(zones);

    // Crues
    MetierCrueCalageAuto[] crues = new MetierCrueCalageAuto[_params.crues.nbCrues];
    for (int i = 0; i < crues.length; i++) {
      crues[i] = new MetierCrueCalageAuto();
      crues[i].coteAval(_params.crues.crues[i].coteAval);
      crues[i].debitAmont(_params.crues.crues[i].debitAmont);

      MetierApportCrueCalageAuto[] apports = new MetierApportCrueCalageAuto[_params.crues.crues[i].nbApports];
      for (int j = 0; j < apports.length; j++) {
        apports[j] = new MetierApportCrueCalageAuto();
        apports[j].abscisse(_params.crues.crues[i].absApports[j]);
        apports[j].debit(_params.crues.crues[i].debitApports[j]);
      }
      crues[i].apports(apports);

      MetierMesureCrueCalageAuto[] mesures = new MetierMesureCrueCalageAuto[_params.crues.crues[i].nbMesures];
      for (int j = 0; j < mesures.length; j++) {
        mesures[j] = new MetierMesureCrueCalageAuto();
        mesures[j].abscisse(_params.crues.crues[i].absMesures[j]);
        mesures[j].cote(_params.crues.crues[i].coteMesures[j]);
        mesures[j].coefficient(_params.crues.crues[i].pondMesures[j]);
      }
      crues[i].mesures(mesures);
    }
    _etd.calageAuto().crues(crues);
  }

  private static final void convertirParametresCondInit(MetierEtude1d _etd, SParametresCondInit _params) {

    if (_params.repriseEtude != null && _params.repriseEtude.repriseCalcul) {
      sbufRecup_.append("<br>").append(getS("L'initialisation des conditions initiales � partir d'un fichier de reprise de calcul n'est pas trait�e"));
      // @todo Il faudrait remplir le contenu avec le fichier de reprise.
      _etd.donneesHydro().conditionsInitiales().paramsReprise(new MetierParametresReprise());
      // _etd.donneesHydro().conditionsInitiales().paramsReprise().contenu(new byte[0]);
    } else {
      _etd.donneesHydro().conditionsInitiales().paramsReprise(null);
    }

    if (_params.ligneEau != null && _params.ligneEau.LigEauInit) {
      sbufRecup_.append("<br>").append(getS("L'initialisation des conditions initiales � partir d'un fichier de ligne d'eau n'est pas trait�e"));
      // @todo Il faudrait remplir le contenu avec le fichier de conditions.
      _etd.donneesHydro().conditionsInitiales().ligneEauInitiale(new MetierLigneEauInitiale());
    } else {
      _etd.donneesHydro().conditionsInitiales().ligneEauInitiale(null);
    }

    // Les zones seches
    if (_params.zonesSeches != null && _params.zonesSeches.nb != 0) {
      MetierZone[] zones = new MetierZone[_params.zonesSeches.nb];
      for (int i = 0; i < zones.length; i++) {
        int numBief = _params.zonesSeches.branche[i];

        zones[i] = new MetierZone();
        zones[i].abscisseDebut(_params.zonesSeches.absDebut[i]);
        zones[i].abscisseFin(_params.zonesSeches.absFin[i]);
        zones[i].biefRattache(_etd.reseau().biefs()[numBief - 1]);
      }
      _etd.donneesHydro().conditionsInitiales().zonesSeches(zones);
    }
  }

  private static final void convertirParametresCalage(MetierEtude1d _etd, SParametresCalage _params) {

    // loi de frottement : 1->STRICKLER, 2->CHEZY, 3->COLEBROOK, 4->BAZIN
    _etd.paramGeneraux().typeFrottement(EnumMetierTypeFrottement.from_int(_params.frottement.loi - 1));
    _etd.paramGeneraux().typeCoefficient(EnumMetierTypeCoefficient.STRICKLER);
    // Zones de frottement
    // Mapping num bief -> Vecteur Zone frottement
    Hashtable<Integer, Vector<MetierZoneFrottement>> hnumbief2Vzones = new Hashtable<Integer, Vector<MetierZoneFrottement>>();

    for (int i = 0; i < _params.frottement.nbZone; i++) {
      int numBief = _params.frottement.numBranche[i];
      Vector<MetierZoneFrottement> vzones = hnumbief2Vzones.get(new Integer(numBief));
      if (vzones == null) {
        vzones = new Vector<MetierZoneFrottement>();
        hnumbief2Vzones.put(new Integer(numBief), vzones);
      }
      MetierZoneFrottement zone = new MetierZoneFrottement();
      zone.abscisseDebut(_params.frottement.absDebZone[i]);
      zone.abscisseFin(_params.frottement.absFinZone[i]);
      zone.coefMajeur(_params.frottement.coefLitMaj[i]);
      zone.coefMineur(_params.frottement.coefLitMin[i]);           
      
      zone.biefRattache(_etd.reseau().biefs()[numBief - 1]);
      vzones.add(zone);
    }
    for (Enumeration<Integer> e = hnumbief2Vzones.keys(); e.hasMoreElements();) {
      int numBief = ((Integer) e.nextElement()).intValue();
      Vector<MetierZoneFrottement> vzones = hnumbief2Vzones.get(new Integer(numBief));
      _etd.reseau().biefs()[numBief - 1].zonesFrottement(vzones.toArray(new MetierZoneFrottement[0]));
    }

    // Zones de stockage (Information recuperable uniquement si les profils ont �t� lus).
    MetierProfil[] profs = _etd.reseau().profils();
    for (int i = 0; i < _params.zoneStockage.nbProfils; i++) {
      int numProf = _params.zoneStockage.numProfil[i];
      if (numProf <= profs.length) {
        double absLitMajGa = _params.zoneStockage.limGauchLitMaj[i];
        double absLitMajDr = _params.zoneStockage.limDroitLitMaj[i];
        MetierPoint2D[] pts = profs[numProf - 1].points();
        for (int j = 0; j < pts.length; j++) {
          if (pts[j].x == absLitMajGa) {
            profs[numProf - 1].indiceLitMajGa(j);
          }
          if (pts[j].x == absLitMajDr) {
            profs[numProf - 1].indiceLitMajDr(j);
          }
        }
      }
    }
  }

  private static final void convertirParametresImpressResult(MetierEtude1d _etd, SParametresImpressResult _params) {
    _etd.description(_params.titreCalcul);
    _etd.paramResultats().optionsListing().calcul(_params.impression.calcul);
    _etd.paramResultats().optionsListing().geometrie(_params.impression.geometrie);
    _etd.paramResultats().optionsListing().ligneEauInitiale(_params.impression.ligneEauInit);
    _etd.paramResultats().optionsListing().loisHydrauliques(_params.impression.loiHydrau);
    _etd.paramResultats().optionsListing().planimetrage(_params.impression.planimetrage);
    _etd.paramResultats().optionsListing().reseau(_params.impression.reseau);
    _etd.paramResultats().decalage(_params.rubens.ecartInterBranch);

    if (_params.resultats.postProcesseur == 3) { // post-processeur : 1->RUBENS, 2->OPTHYCA
      _etd.paramResultats().postOpthyca(true);
      _etd.paramResultats().postRubens(true);
    } else if (_params.resultats.postProcesseur == 2) {// post-processeur : 1->OPTHYCA
      _etd.paramResultats().postOpthyca(true);
      _etd.paramResultats().postRubens(false);
    } else if (_params.resultats.postProcesseur == 1) {
      _etd.paramResultats().postOpthyca(false);
      _etd.paramResultats().postRubens(true);
    }
    _etd.paramResultats().paramStockage().periodeListing(_params.pasStockage.pasImpression);
    _etd.paramResultats().paramStockage().periodeResultat(_params.pasStockage.pasStock);
    _etd.paramResultats().paramStockage().premierPasTempsStocke(_params.pasStockage.premPasTpsStock);
    _etd.paramResultats().paramStockage().option(EnumMetierOptionStockage.from_int(_params.stockage.option - 1));

    // option de stockage : 1->"Toutes les sections",
    // 2->"A certains sites"
    if (_params.stockage.option == 2) { // 2->"A certains sites"
      MetierSite[] sites = new MetierSite[_params.stockage.nbSite];
      for (int i = 0; i < sites.length; i++) {
        int numBief = _params.stockage.branche[i];

        sites[i] = new MetierSite();
        sites[i].abscisse(_params.stockage.abscisse[i]);
        sites[i].biefRattache(_etd.reseau().biefs()[numBief - 1]);
      }
      _etd.paramResultats().paramStockage().sites(sites);
    }
  }

  private static final void convertirParametresVarStock(MetierParametresResultats _res, SParametresVarStock _params) {
    for (int i = 0; i < _params.varStockees.length; i++) {
      if (_params.varStockees[i] == true) {
        _res.ajouteVariable(MetierDescriptionVariable.VARIABLES[i].nom());
      } else {
        _res.supprimeVariable(MetierDescriptionVariable.VARIABLES[i].nom());
      }
    }
  }

  /**
   * Retourne les lois hydrauliques pour les seuils qui sont a supprimer de la liste des lois.
   *
   * @param _etd
   * @param _params
   * @return
   */
  private final static MetierLoiHydraulique[] convertirParametresSingularite(MetierEtude1d _etd, SParametresSingularite _params) {
    Vector<MetierLoiHydraulique> vloisASupprimer = new Vector<MetierLoiHydraulique>();

    // Les pertes de charge
    if (_params.pertesCharges != null) {
      for (int i = 0; i < _params.pertesCharges.nbPerteCharge; i++) {
        int numBief = _params.pertesCharges.numBranche[i];

        MetierPerteCharge perte = new MetierPerteCharge();
        perte.abscisse(_params.pertesCharges.abscisses[i]);
        perte.coefficient(_params.pertesCharges.coefficients[i]);
        _etd.reseau().biefs()[numBief - 1].ajouteSingularite(perte);
      }
    }

    // Les seuils
    for (int i = 0; i < _params.nbSeuils; i++) {
      int numBief = _params.seuils[i].numBranche;

      // Creation du seuil suivant le type
      MetierSeuil seuil;

      if (_params.seuils[i].type == 1) { // Zam=f(Zav,Q)
        MetierSeuilNoye s = new MetierSeuilNoye(); // @todo controler le type de
        // loi
        s.coteCrete(_params.seuils[i].coteCrete);
        s.loi((MetierLoiSeuil) _etd.donneesHydro().lois()[_params.seuils[i].numLoi - 1]);
        seuil = s;
      } else if (_params.seuils[i].type == 2) {
        MetierSeuilDenoye s = new MetierSeuilDenoye(); // @todo controler le type
        // de loi
        s.coteCrete(_params.seuils[i].coteCrete);
        s.loi((MetierLoiTarage) _etd.donneesHydro().lois()[_params.seuils[i].numLoi - 1]);
        seuil = s;
      } else if (_params.seuils[i].type == 3) {
        MetierSeuilGeometrique s = new MetierSeuilGeometrique();
        s.coteMoyenneCrete(_params.seuils[i].coteCreteMoy);
        s.coefQ(_params.seuils[i].coeffDebit);
        // La loi n'existe pas dans la liste des lois. On la cr�e, on l'ajoute.
        s.loi(new MetierLoiGeometrique());
        s.loi().d(_params.seuils[i].abscTravCrete);
        s.loi().z(_params.seuils[i].cotesCrete);
        _etd.donneesHydro().ajouteLoi(s.loi());
        seuil = s;
      } else if (_params.seuils[i].type == 4) { // Zam=f(Cote Crete , Coeff Debit)
        MetierSeuilLoi s = new MetierSeuilLoi();
        s.coteCrete(_params.seuils[i].coteCrete);
        s.coefQ(_params.seuils[i].coeffDebit);
        s.epaisseur(EnumMetierEpaisseurSeuil.from_int(_params.seuils[i].epaisseur - 1));
        if (_etd.paramGeneraux().regime() == EnumMetierRegime.TRANSCRITIQUE) {
          // Une loi a �t� cr�ee sp�cifiquement pour ce seuil, elle sera supprim�e, inutile dans Fudaa-Mascaret.
          vloisASupprimer.add(_etd.donneesHydro().lois()[_params.seuils[i].numLoi - 1]);
          s.gradient(_params.seuils[i].gradient);
        }
        seuil = s;
      } else if (_params.seuils[i].type == 5) { // type du seuil : Zam=f(t)
        MetierSeuilLimniAmont s = new MetierSeuilLimniAmont();
        s.coteCrete(_params.seuils[i].coteCrete);
        s.loi((MetierLoiLimnigramme) _etd.donneesHydro().lois()[_params.seuils[i].numLoi - 1]);
        seuil = s;
      } else if (_params.seuils[i].type == 6) { // type du seuil : Q=f(Zam)
        MetierSeuilTarageAmont s = new MetierSeuilTarageAmont();
        s.coteCrete(_params.seuils[i].coteCrete);
        s.loi((MetierLoiTarage) _etd.donneesHydro().lois()[_params.seuils[i].numLoi - 1]);
        if (_etd.paramGeneraux().regime() == EnumMetierRegime.TRANSCRITIQUE) {
          s.gradient(_params.seuils[i].gradient);
        }
        seuil = s;
      } else if (_params.seuils[i].type == 7) { // type du seuil : Q=f(Zav)
        MetierSeuilTarageAval s = new MetierSeuilTarageAval();
        s.coteCrete(_params.seuils[i].coteCrete);
        s.loi((MetierLoiTarage) _etd.donneesHydro().lois()[_params.seuils[i].numLoi - 1]);
        seuil = s;
      } else if (_params.seuils[i].type == 8) { // type du seuil : vanne
        MetierSeuilVanne s = new MetierSeuilVanne();
        s.largeur(_params.seuils[i].largVanne);
        s.loi((MetierLoiOuvertureVanne) _etd.donneesHydro().lois()[_params.seuils[i].numLoi - 1]);
        seuil = s;
      } else {
        MetierBarrage s = new MetierBarrage();
        s.coteCrete(_params.seuils[i].coteCrete);
        seuil = s;
      }
      seuil.nom(_params.seuils[i].nom);
      seuil.abscisse(_params.seuils[i].abscisse);
      seuil.coteRupture(_params.seuils[i].coteRupture);

      _etd.reseau().biefs()[numBief - 1].ajouteSingularite(seuil);
    }

    // Le barrage principal
    if (_etd.paramGeneraux().ondeSubmersion()) {
      if (_params.barragePrincipal != null) {
        MetierBarragePrincipal barragePrincipal = new MetierBarragePrincipal();

        // 1->rupture progressive
        if (_params.barragePrincipal.typeRupture == 1) {
          barragePrincipal.ruptureProgressive(true);
        } // 2->rupture instantanee
        else {
          barragePrincipal.ruptureProgressive(false);
          if (barragePrincipal.site() != null) {
            int numBief = _params.barragePrincipal.numBranche;
            barragePrincipal.site().biefRattache(_etd.reseau().biefs()[numBief - 1]);
            barragePrincipal.site().abscisse(_params.barragePrincipal.abscisse);
            barragePrincipal.cotePlanEau(_params.barragePrincipal.coteCrete);
          }
        }
        _etd.paramGeneraux().barragePrincipal(barragePrincipal);
      }
    }

    return vloisASupprimer.toArray(new MetierLoiHydraulique[0]);
  }

  private static void convertirParametresLiaisons(MetierEtude1d _etd, SParametresLiaisons _params) {
    MetierLiaison[] liaisons = new MetierLiaison[_params.nbLiaisons];
    for (int i = 0; i < liaisons.length; i++) {
      liaisons[i] = new MetierLiaison();
      liaisons[i].numero(i + 1);
      if (_params.types[i] == 1) {  // Seuil
        liaisons[i].caracteristiques(new MetierSeuilLiaison());
      } else if (_params.types[i] == 2) {  // Chenal
        liaisons[i].caracteristiques(new MetierChenalLiaison());
      } else if (_params.types[i] == 3) {  // Siphon
        liaisons[i].caracteristiques(new MetierSiphonLiaison());
      } else if (_params.types[i] == 4) {  // Orifice
        liaisons[i].caracteristiques(new MetierOrificeLiaison());
      }
      if (_params.nature[i] == 1) { // Riviere casier
        liaisons[i].topologie(new MetierTopologieRiviereCasier());
      } else if (_params.nature[i] == 2) { // Casier casier
        liaisons[i].topologie(new MetierTopologieCasierCasier());
      }
      liaisons[i].setCote(_params.cote[i]);
      // largeur de la liaison pour les liaisons de type chenal ou seuil ou orifice
      if (liaisons[i].isSeuil() || liaisons[i].isChenal() || liaisons[i].isOrifice()) {
        liaisons[i].setLargeur(_params.largeur[i]);
      }
      // longueur de la liaison pour les liaisons de type chenal ou siphon
      if (liaisons[i].isSiphon() || liaisons[i].isChenal()) {
        liaisons[i].setLongueur(_params.longueur[i]);
      }
      // rugosit� de la liaison pour les liaisons de type chenal
      if (liaisons[i].isChenal()) {
        liaisons[i].setRugosite(_params.rugosite[i]);
      }
      // section de la liaison pour les liaisons de type siphon ou orifice
      if (liaisons[i].isSiphon() || liaisons[i].isOrifice()) {
        liaisons[i].setSection(_params.section[i]);
      }
      // coef de perte de charge de la liaison pour les liaisons de type siphon
      if (liaisons[i].isSiphon()) {
        liaisons[i].setCoefPerteCharge(_params.coefPerteCharge[i]);
      }
      // coef de d�bit de la liaison pour les liaisons de type seuil ou orifice
      if (liaisons[i].isSeuil() || liaisons[i].isOrifice()) {
        liaisons[i].setCoefQ(_params.coefDebitSeuil[i]);
      }
      // coef d'activation de la liaison pour les liaisons de type seuil
      if (liaisons[i].isSeuil()) {
        liaisons[i].setCoefActivation(_params.coefActivation[i]);
      }
      // coef de d�bit orifice de la liaison pour les liaisons de type orifice
      if (liaisons[i].isOrifice()) {
        liaisons[i].setCoefQOrifice(_params.coefDebitOrifice[i]);
      }
      // type de l'orifice de la liaison pour les liaisons de type orifice
      if (liaisons[i].isOrifice()) {
        liaisons[i].setSensDebit(EnumMetierSensDebitLiaison.from_int(_params.typeOrifice[i] - 1));
      }

      // abscisse du bief de la liaison pour les liaisons de nature "Rivi�re-Casier"
      if (liaisons[i].isRiviereCasier()) {
        liaisons[i].setAbscisse(_params.abscBief[i]);
      }
      // numero casier d'origine
      if (liaisons[i].isRiviereCasier()) {
        liaisons[i].topologie().setCasierRattache(_etd.reseau().casiers()[_params.numCasierOrigine[i] - 1]);
      }
      if (liaisons[i].isCasierCasier()) {
        liaisons[i].topologie().setCasierAmontRattache(_etd.reseau().casiers()[_params.numCasierOrigine[i] - 1]);
      }
      // numero casier fin
      if (liaisons[i].isCasierCasier()) {
        liaisons[i].topologie().setCasierAvalRattache(_etd.reseau().casiers()[_params.numCasierFin[i] - 1]);
      }
      // numero bief
      if (liaisons[i].isRiviereCasier()) {
        liaisons[i].topologie().setBiefRattache(_etd.reseau().biefs()[_params.numBiefAssocie[i] - 1]);
      }
    }
    _etd.reseau().liaisons(liaisons);
  }

  private static void convertirParametresCasiers(MetierEtude1d _etd, SParametresCasier _params) {
    sbufRecup_.append("<br><b>").append(getS("R�cuperation des casiers")).append("</b> : <br>").append(getS("Le fichiers casiers n'est pas lu"));
    boolean bplanauto = false;

    // Les casiers.
    MetierCasier[] casiers = new MetierCasier[_params.nbCasiers];
    for (int i = 0; i < _params.nbCasiers; i++) {
      casiers[i] = new MetierCasier();
      casiers[i].numero(i + 1);
      casiers[i].coteInitiale(_params.cotesInitiale[i]);
      if (_params.optionPlanimetrage[i] == 1) { // Planimetrage manuel
        casiers[i].geometrie(new MetierPlanimetrageCasier());
      } else { // Planimetrage automatique
        casiers[i].geometrie(new MetierNuagePointsCasier());
        casiers[i].geometrie().setSurfaceDependCote(_params.optionCalcul[i] == 2);
        casiers[i].geometrie().setPasPlanimetrage(_params.pasPlanimetrage[i]);
        /**
         * @todo Impossible de recuperer le nb de cotes de planimetrage sans lire auparavant le fichier des casiers.
         */
        if (!bplanauto) {
          sbufRecup_.append("<br><b>").append(getS("Casiers")).append("</b> :<br>&nbsp;").append(getS(
                  "Le nombre de cotes de planimetrage ne peut etre restitu�"));
        }
        bplanauto = true;
      }
    }
    _etd.reseau().casiers(casiers);
  }

  private static void convertirParametresApports(MetierEtude1d _etd, SParametresApporDeversoirs _params) {

    // Les apports sur les casiers
    if (_params.apportCasier != null) {
      for (int i = 0; i < _params.apportCasier.nbApportPluie; i++) {
        MetierLoiHydraulique loi = _etd.donneesHydro().lois()[_params.apportCasier.numLoi[i] - 1];
        _etd.reseau().casiers()[_params.apportCasier.numCasier[i] - 1].loiRattachee((MetierLoiHydrogramme) loi);
      }
    }

    // Les apports
    if (_params.debitsApports != null) {
      for (int i = 0; i < _params.debitsApports.nbQApport; i++) {
        int numBief = _params.debitsApports.numBranche[i];

        MetierApport apport = new MetierApport();
        apport.nom(_params.debitsApports.noms[i]);
        apport.abscisse(_params.debitsApports.abscisses[i]);
        apport.longueur(_params.debitsApports.longueurs[i]);
        apport.loi(_etd.donneesHydro().lois()[_params.debitsApports.numLoi[i] - 1]);
        _etd.reseau().biefs()[numBief - 1].ajouteSingularite(apport);
      }
    }

    // Les deversoirs
    if (_params.deversLate != null) {
      for (int i = 0; i < _params.deversLate.nbDeversoirs; i++) {
        int numBief = _params.deversLate.numBranche[i];

        // Deversoir en fonction du type de loi associ�.
        MetierDeversoir devers;
        if (_params.deversLate.type[i] == 1) {
          devers = new MetierDeversoirComportementZCoefQ();
          ((MetierDeversoirComportementZCoefQ) devers).coteCrete(_params.deversLate.coteCrete[i]);
          ((MetierDeversoirComportementZCoefQ) devers).coefQ(_params.deversLate.coeffDebit[i]);
        } else {
          devers = new MetierDeversoirComportementLoi();
          ((MetierDeversoirComportementLoi) devers).loi(_etd.donneesHydro().lois()[_params.deversLate.numLoi[i] - 1]);
        }
        devers.nom(_params.deversLate.nom[i]);
        devers.abscisse(_params.deversLate.abscisse[i]);
        devers.longueur(_params.deversLate.longueur[i]);
        _etd.reseau().biefs()[numBief - 1].ajouteSingularite(devers);
      }
    }
  }

  private static void convertirParametresPlanimMaillage(MetierEtude1d _etd, SParametresPlanimMaillage _params) {

    // Mapping profil -> num bief
    Hashtable<MetierProfil, Integer> hprof2numbief = new Hashtable<MetierProfil, Integer>();
    for (int i = 0; i < _etd.reseau().biefs().length; i++) {
      MetierProfil[] profs = _etd.reseau().biefs()[i].profils();
      for (int j = 0; j < profs.length; j++) {
        hprof2numbief.put(profs[j], new Integer(i + 1));
      }
    }

    MetierProfil[] allProfs = _etd.reseau().profils();

    initMaillageMethode(_etd.paramGeneraux().maillage(), _params.methodeMaillage);

    // Planimetrage
    // Mapping num bief -> Vecteur Zone planimetrage
    Hashtable<Integer, Vector<MetierZonePlanimetrage>> hnumbief2Vzones = new Hashtable<Integer, Vector<MetierZonePlanimetrage>>();
    for (int i = 0; i < _params.planim.nbZones; i++) {
      MetierProfil p1 = allProfs[_params.planim.num1erProf[i] - 1];
      MetierProfil p2 = allProfs[_params.planim.numDerProf[i] - 1];

      int numBief = hprof2numbief.get(p1).intValue();
      if (hprof2numbief.get(p2).intValue() != numBief) {
        sbufRecup_.append("<br>").append(getS("Planimetrage n�")).append(i + 1).append(" : ").append(getS(
                "Le 1er et le dernier profil n'appartiennent pas au m�me bief."));
        continue;
      }

      Vector<MetierZonePlanimetrage> vzones = hnumbief2Vzones.get(new Integer(numBief));
      if (vzones == null) {
        vzones = new Vector<MetierZonePlanimetrage>();
        hnumbief2Vzones.put(new Integer(numBief), vzones);
      }

      MetierZonePlanimetrage zone = new MetierZonePlanimetrage();
      zone.biefRattache(_etd.reseau().biefs()[numBief - 1]);
      zone.abscisseDebut(p1.abscisse());
      zone.abscisseFin(p2.abscisse());
      zone.taillePas(_params.planim.valeursPas[i]);
      vzones.add(zone);
    }
    for (Enumeration<Integer> e = hnumbief2Vzones.keys(); e.hasMoreElements();) {
      int numBief = e.nextElement().intValue();
      Vector<MetierZonePlanimetrage> vzones = hnumbief2Vzones.get(new Integer(numBief));
      _etd.reseau().biefs()[numBief - 1].zonesPlanimetrage((MetierZonePlanimetrage[]) vzones.toArray(new MetierZonePlanimetrage[0]));
    }

    // Maillage
    MetierDefinitionSectionsParSeries defSer;
    MetierDefinitionSectionsParSections defSec;
    MetierDefinitionSectionsParSeriesUnitaire[] units;

    switch (_params.methodeMaillage) {

      case 3: // section par section
        defSec = new MetierDefinitionSectionsParSections();
        MetierSite[] sites = new MetierSite[_params.maillage.maillageClavier.nbSections];
        for (int i = 0; i < sites.length; i++) {
          int numBief = _params.maillage.maillageClavier.branchesSection[i];

          sites[i] = new MetierSite();
          sites[i].abscisse(_params.maillage.maillageClavier.absSection[i]);
          sites[i].biefRattache(_etd.reseau().biefs()[numBief - 1]);
        }
        defSec.unitaires(sites);
        _etd.paramGeneraux().maillage().sections(defSec);
        break;

      case 2: // par serie
        defSer = new MetierDefinitionSectionsParSeries();
        defSer.surProfils(false);
        units = new MetierDefinitionSectionsParSeriesUnitaire[_params.maillage.maillageClavier.nbZones];
        for (int i = 0; i < _params.maillage.maillageClavier.nbZones; i++) {
          int numBief = _params.maillage.maillageClavier.numBrancheZone[i];

          units[i] = new MetierDefinitionSectionsParSeriesUnitaire();
          units[i].zone().abscisseDebut(_params.maillage.maillageClavier.absDebutZone[i]);
          units[i].zone().abscisseFin(_params.maillage.maillageClavier.absFinZone[i]);
          units[i].nbSections(_params.maillage.maillageClavier.nbSectionZone[i]);
          units[i].zone().biefRattache(_etd.reseau().biefs()[numBief - 1]);
        }
        defSer.unitaires(units);
        _etd.paramGeneraux().maillage().sections(defSer);
        break;

      case 5: // par serie dont les profils
        defSer = new MetierDefinitionSectionsParSeries();
        defSer.surProfils(true);
        units = new MetierDefinitionSectionsParSeriesUnitaire[_params.maillage.maillageClavier.nbPlages];
        for (int i = 0; i < _params.maillage.maillageClavier.nbPlages; i++) {
          MetierProfil p1 = allProfs[_params.maillage.maillageClavier.num1erProfPlage[i] - 1];
          MetierProfil p2 = allProfs[_params.maillage.maillageClavier.numDerProfPlage[i] - 1];

          int numBief = ((Integer) hprof2numbief.get(p1)).intValue();
          if (((Integer) hprof2numbief.get(p2)).intValue() != numBief) {
            sbufRecup_.append("<br>").append(getS("Maillage clavier, plage n�")).append(i + 1).append(" : ").append(getS(
                    "Le 1er et le dernier profil n'appartiennent pas au m�me bief."));
            continue;
          }

          units[i] = new MetierDefinitionSectionsParSeriesUnitaire();
          units[i].zone().abscisseDebut(p1.abscisse());
          units[i].zone().abscisseFin(p2.abscisse());
          units[i].pas(_params.maillage.maillageClavier.pasEspacePlage[i]);
          units[i].zone().biefRattache(_etd.reseau().biefs()[numBief - 1]);
        }
        defSer.unitaires(units);
        _etd.paramGeneraux().maillage().sections(defSer);
        break;
    }
  }

  private final static void initMaillageMethode(MetierMaillage _maillage, int _meth) {
    // methode de calcul du maillage : 1-> aux profils, 2-> par serie,
    // 3-> section par section, 4->repris de la ligne d'eau initiales,
    // 5-> par serie dont les profils
    switch (_meth) {
      case 1:
        _maillage.methode(EnumMetierMethodeMaillage.SECTIONS_SUR_PROFILS);
        return;
      case 2:
        _maillage.methode(EnumMetierMethodeMaillage.SECTIONS_PAR_SERIES);
        return;
      case 3:
        _maillage.methode(EnumMetierMethodeMaillage.SECTIONS_UTILISATEUR);
        return;
      case 4:
        _maillage.methode(EnumMetierMethodeMaillage.SECTIONS_LIGNE_EAU_INITIALE);
        return;
      case 5:
        _maillage.methode(EnumMetierMethodeMaillage.SECTIONS_PAR_SERIES);
        return;
    }
  }

  private static void convertirParametresConfluents(MetierNoeud[] _nds, SParametresConfluents _params) {
    for (int i = 0; i < _nds.length; i++) {
      MetierExtremite[] extrs = _nds[i].extremites();
      for (int j = 0; j < extrs.length; j++) {
        extrs[j].point1(new MetierPoint2D());
        extrs[j].point2(new MetierPoint2D());
        extrs[j].pointMilieu(new MetierPoint2D(_params.confluents[i].abscisses[j], _params.confluents[i].ordonnees[j]));
        extrs[j].angle(_params.confluents[i].angles[j]);
      }
    }
  }
  /*  private static int getIndiceAvalProfil(MetierZonePlanimetrage p, double abscisse) {
   int indiceProfil = p.biefRattache().getIndiceProfilAbscisse(abscisse);
   if (indiceProfil < 0) indiceProfil = -indiceProfil;
   return indiceProfil;

   }*/

  private static void convertirParametresLoiHydrau(MetierDonneesHydrauliques _obj, SParametresLoisHydrau _params) {
    int nbLoiSup = 0; // @todo Ces lois supplementaires sont a traiter auparavant.
    MetierLoiHydraulique[] objLois = new MetierLoiHydraulique[_params.nb - nbLoiSup];

    boolean btitle = false;
    NEXT_LOI:
    for (int i = 0; i < objLois.length; i++) {

      // Transformation des donn�es temps suivant l'unit� de temps
      int coef = 1;
      switch (_params.lois[i].donnees.uniteTps) {
        // Secondes
        case 1:
          coef = 1;
          break;
        // Minutes
        case 2:
          coef = 60;
          break;
        // Heures
        case 3:
          coef = 60 * 60;
          break;
        // Jour
        case 4:
          coef = 60 * 60 * 24;
          break;
      }

      if (_params.lois[i].donnees.tps != null) {
        for (int j = 0; j < _params.lois[i].donnees.tps.length; j++) {
          _params.lois[i].donnees.tps[j] *= coef;
        }
      }

      if (_params.lois[i].type == 1) { // type de la loi : 1->"HYDROGRAMME Q(T)"
        MetierLoiHydrogramme loi = new MetierLoiHydrogramme();
        if (_params.lois[i].donnees.modeEntree == 2) {
          loi.t(_params.lois[i].donnees.tps);
          loi.q(_params.lois[i].donnees.debit);
        }
        objLois[i] = loi;

      } else if (_params.lois[i].type == 2) { // type de la loi : 2->"LIMNIGRAMME Z(T)"
        MetierLoiLimnigramme loi = new MetierLoiLimnigramme();
        if (_params.lois[i].donnees.modeEntree == 2) {
          loi.t(_params.lois[i].donnees.tps);
          loi.z(_params.lois[i].donnees.cote);
        }
        objLois[i] = loi;

      } else if (_params.lois[i].type == 3) { // type de la loi : 3->"LIMNHYDROGRAMME Z,Q(T)"
        MetierLoiLimniHydrogramme loi = new MetierLoiLimniHydrogramme();
        if (_params.lois[i].donnees.modeEntree == 2) {
          loi.t(_params.lois[i].donnees.tps);
          loi.z(_params.lois[i].donnees.cote);
          loi.q(_params.lois[i].donnees.debit);
        }
        objLois[i] = loi;

      } else if (_params.lois[i].type == 4) { // type de loi : 4->"COURBE DE TARAGE Z=f(Q)"
        // @todo Je ne sais pas laquelle choisir. Peut �tre regulation ou tarage.
        if (!btitle) {
          btitle = true;
          sbufRecup_.append("<br><b>").append(getS("Lecture des lois")).append("</b> :");
        }
        sbufRecup_.append("<br>").append(getS("R�cup�ration d'une loi de type 4")).append(" : ").append(
                getS("Elle est transf�r�e en courbe de tarage"));
//        objLois[i]=new MetierLoiRegulation();
        MetierLoiTarage loi = new MetierLoiTarage();
        loi.amont(true);
        if (_params.lois[i].donnees.modeEntree == 2) {
          loi.z(_params.lois[i].donnees.cote);
          loi.q(_params.lois[i].donnees.debit);
        }
        objLois[i] = loi;

      } else if (_params.lois[i].type == 5) { // type de loi : 5->"COURBE DE TARAGE Q=f(Z)"
        MetierLoiTarage loi = new MetierLoiTarage();
        loi.amont(false);
        if (_params.lois[i].donnees.modeEntree == 2) {
          loi.z(_params.lois[i].donnees.cote);
          loi.q(_params.lois[i].donnees.debit);
        }
        objLois[i] = loi;

      } else if (_params.lois[i].type == 6) { // type de la loi : 6->"SEUIL Zam=f(Zav,Q)
        MetierLoiSeuil loi = new MetierLoiSeuil();
        if (_params.lois[i].donnees.modeEntree == 2) {
          TDoubleArrayList ldebit = new TDoubleArrayList();
          for (double q : _params.lois[i].donnees.debit) {
            if (!ldebit.contains(q)) {
              ldebit.add(q);
            }
          }

          TDoubleArrayList lzaval = new TDoubleArrayList();
          for (double q : _params.lois[i].donnees.cote) {
            if (!lzaval.contains(q)) {
              lzaval.add(q);
            }
          }

          double[] debit = ldebit.toNativeArray();
          double[] zaval = lzaval.toNativeArray();
          double[][] zamont = new double[debit.length][zaval.length];
          for (int j = 0; j < _params.lois[i].donnees.cote2.length; j++) {
            int idebit = ldebit.indexOf(_params.lois[i].donnees.debit[j]);
            int izaval = lzaval.indexOf(_params.lois[i].donnees.cote[j]);
            if (idebit == -1 || izaval == -1) {
              if (!btitle) {
                btitle = true;
                sbufRecup_.append("<br><b>").append(getS("Lecture des lois")).append("</b> :");
              }
              sbufRecup_.append("<br>").append(getS("Loi '")).append(_params.lois[i].nom).append("' de type 6").append(":\n");
              sbufRecup_.append(getS("Le nombre de cote amont doit �tre �gale au produit") + "\n");
              sbufRecup_.append(getS("du nombre de d�bits diff�rents avec le nombre de cotes avals diff�rentes") + "\n");

              continue NEXT_LOI;
            }
            zamont[idebit][izaval] = _params.lois[i].donnees.cote2[j];
          }

          loi.zAval(zaval);
          loi.zAmont(zamont);
          loi.q(debit);
        }
        objLois[i] = loi;

      } else if (_params.lois[i].type == 7) { // type de la loi : 6->"OUVERTURE VANNE Zinf,Zsup=f(T)
        MetierLoiOuvertureVanne loi = new MetierLoiOuvertureVanne();
        if (_params.lois[i].donnees.modeEntree == 2) {
          loi.t(_params.lois[i].donnees.tps);
          loi.zInf(_params.lois[i].donnees.cote);
          loi.zSup(_params.lois[i].donnees.cote2);
        }
        objLois[i] = loi;

      } else {
        System.err.println("Type de loi inconnu");
      }
      objLois[i].nom(_params.lois[i].nom);
      objLois[i].numero(i + 1);
    }
    _obj.lois(objLois);
    _obj.initIndiceLois();
  }

  /**
   * @param _obj
   * @param _biefsGeo Les biefs lus sur le fichier .geo. Leur nombre doit etre equivalent au nombre de branches.
   * @param _params
   */
  private static void convertirParametresGeoReseau(MetierEtude1d _obj, MetierBief[] _biefsGeo, SParametresGeoReseau _params) {
    if (_params.branches.nb != _biefsGeo.length) {
      throw new IllegalArgumentException(getS("Le nombre de branches du fichier Cas n'est pas egale ")
              + getS("au nombre de biefs lus sur le fichier Geometrie"));
    }

    _obj.paramGeneraux().profilsAbscAbsolu(_params.geometrie.profilsAbscAbsolu);

    MetierBief[] objBiefs = new MetierBief[_params.branches.nb];
    for (int i = 0; i < objBiefs.length; i++) {
      objBiefs[i] = new MetierBief();
      objBiefs[i].numero(i + 1);
      objBiefs[i].profils(_biefsGeo[i].profils());
// @todo Les profils sont issus du fichier geo, obligatoirement.
/*      if (objBiefs[i].extrAmont().profilRattache()==null) {
       objBiefs[i].extrAmont().profilRattache(new MetierProfil());
       }
       if (objBiefs[i].extrAval().profilRattache()==null) {
       objBiefs[i].extrAval().profilRattache(new MetierProfil());
       }
       objBiefs[i].extrAmont().profilRattache().abscisse(_params.branches.abscDebut[i]);
       objBiefs[i].extrAval().profilRattache().abscisse(_params.branches.abscFin[i]);*/
      objBiefs[i].extrAmont().numero(_params.branches.numExtremDebut[i]);
      objBiefs[i].extrAval().numero(_params.branches.numExtremFin[i]);
    }
    //FRED: correction d'un bug bizarre � l'ouverture: les indices des extremites sont chang�s sans raison
    //cette ligne:
    //    _obj.reseau().biefs(objBiefs);
    //est remplac�e par:
    _obj.reseau().biefsNoExtremiteChange(objBiefs);

    // Mapping numero -> extremite de bief
    HashMap<Integer, MetierExtremite> hnum2Extr = new HashMap<Integer, MetierExtremite>();
    for (int i = 0; i < objBiefs.length; i++) {
      hnum2Extr.put(Integer.valueOf(objBiefs[i].extrAmont().numero()), objBiefs[i].extrAmont());
      hnum2Extr.put(Integer.valueOf(objBiefs[i].extrAval().numero()), objBiefs[i].extrAval());
    }

    // Noeud connect�s
    for (int i = 0; i < _params.noeuds.nb; i++) {
      MetierNoeud objNoeudConnecte = new MetierNoeud();
      for (int j = 0; j < _params.noeuds.noeuds[i].num.length; j++) {
        int num = _params.noeuds.noeuds[i].num[j];
        if (num == 0) {
          break;
        }

        MetierExtremite objExtr = hnum2Extr.get(Integer.valueOf(num));
        objNoeudConnecte.ajouteExtremite(objExtr);
        //TODO FRED peut �tre null ici !
        objExtr.noeudRattache(objNoeudConnecte);
      }
    }

    // Mapping numero -> loi
    HashMap<Integer, MetierLoiHydraulique> hnum2Loi = new HashMap<Integer, MetierLoiHydraulique>();
    MetierLoiHydraulique[] objLois = _obj.donneesHydro().lois();
    for (int i = 0; i < objLois.length; i++) {
      hnum2Loi.put(new Integer(i + 1), objLois[i]);
    }

    // Conditions limites sur extremites libres
    for (int i = 0; i < _params.extrLibres.nb; i++) {
      MetierExtremite objExtr = (MetierExtremite) hnum2Extr.get(new Integer(_params.extrLibres.numExtrem[i]));
      MetierLimite objLim = new MetierLimite();
      // On initialise la loi seulement si impos�e.
      if (_params.extrLibres.typeCond[i] != 6 && _params.extrLibres.typeCond[i] != 7) {
        objLim.loi(hnum2Loi.get(new Integer(_params.extrLibres.numLoi[i])));
      }
      initLimiteType(objLim, _params.extrLibres.typeCond[i]);
      initLimiteCondImposee(objLim, _params.extrLibres.typeCond[i]);
      objLim.nom(_params.extrLibres.Nom[i]);

      objExtr.conditionLimite(objLim);
    }
  }

  private static void initLimiteType(MetierLimite limite, int _type) {
    // En principe la loi est nulle.
    if (_type == 6) { // Limite libre
      limite.typeLimiteCalcule(EnumMetierLimiteCalcule.EVACUATION_LIBRE);
    } else if (_type == 7) { // Limite hauteur normale
      limite.typeLimiteCalcule(EnumMetierLimiteCalcule.HAUTEUR_NORMALE);
    }
  }

  private static void initLimiteCondImposee(MetierLimite limite, int _type) {
    // En principe, la loi est non nulle.
    if (_type == 1) {
      limite.condLimiteImposee(EnumMetierCondLimiteImposee.DEBIT);
    } else if (_type == 2) {
      limite.condLimiteImposee(EnumMetierCondLimiteImposee.COTE);
    } else if (_type == 3) { // Z(Q) loi de tarage.
      if (!(limite.loi() instanceof MetierLoiTarage)) {
        throw new IllegalArgumentException(getS("Loi de tarage Z(Q) sans doute mal recuperee"));
      }
      ((MetierLoiTarage) limite.loi()).amont(true);
    } else if (_type == 4) { // Q(Z) loi de tarage.
      if (!(limite.loi() instanceof MetierLoiTarage)) {
        throw new IllegalArgumentException(getS("Loi de tarage Q(Z) sans doute mal recuperee"));
      }
      ((MetierLoiTarage) limite.loi()).amont(false);
    } else if (_type == 5) { // regulation : Zav(Qam)
      // Rien a faire
    } else if (_type == 6) { // Non impose
      // Rien a faire
    } else if (_type == 7) { // Non impose
      // Rien a faire
    } else if (_type == 8) {
      limite.condLimiteImposee(EnumMetierCondLimiteImposee.COTE_DEBIT);
    }
  }

  private static void convertirParametresGen(MetierParametresGeneraux _obj, SParametresGen _params) {
    // noyau code : 1->SARAP, 2->REZO, 3->MASCARET
    _obj.regime(EnumMetierRegime.from_int(_params.code - 1));
    // Presence casiers
    _obj.parametresCasier().activation(_params.presenceCasiers);
  }

  private static void convertirParametresModelPhy(MetierParametresGeneraux _obj, SParametresModelPhy _params) {
    _obj.perteChargeConfluents(_params.perteChargeConf);
    // composition des lits : 1->DEBORD, 2->FOND/BERGE
    _obj.compositionLits(EnumMetierTypeCompositionLits.from_int(_params.compositionLits - 1));
    // conservation du frottement sur les parois verticales
    _obj.frottementsParois(_params.conservFrotVertical);
    // elevation de cote arrivee du front
    _obj.elevationCoteArriveeFront(_params.elevCoteArrivFront);
    // interpolation lin�aire des strickler
    _obj.interpolationLineaireCoefFrottement(_params.interpolLinStrickler);
    // les parametres du debordement progressif
    _obj.debordProgressifLitMajeur(_params.debordement.litMajeur);
    _obj.debordProgressifZoneStockage(_params.debordement.zoneStock);
  }

  private static void convertirParametresNum(MetierParametresGeneraux _obj, SParametresNum _params) {
    // calcul d'une onde de submersion => "BARRAGE PRINCIPAL"
    _obj.ondeSubmersion(_params.calcOndeSubmersion);
    // Froude limite pour les conditions limites si code Mascaret
    _obj.froudeLimConditionLimite(_params.froudeLimCondLim);
    // traitement implicite du frottement
    _obj.traitementImpliciteFrottements(_params.traitImplicitFrot);
    // hauteur d'eau minimale
    _obj.hauteurEauMinimal(_params.hauteurEauMini);
    // implicitation du noyau transcritique
    _obj.implicitationNoyauTrans(_params.implicitNoyauTrans);
    // optimisation du noyau transcritique
    _obj.optimisationNoyauTrans(_params.optimisNoyauTrans);
    // perte de charge automatique en cas d'�largissement
    _obj.perteChargeAutoElargissement(_params.perteChargeAutoElargissement);
    // termes non hydrostatiques pour le noyau transcritique
    _obj.termesNonHydrostatiques(_params.termesNonHydrostatiques);
   
    _obj.apportDebit(_params.apportDebit == 1);
    _obj.attenuationConvection(_params.attenuationConvection);
  }

  private static void convertirParametresTemp(MetierParametresTemporels _obj, SParametresTemp _params) {
    // pas de temps
    _obj.pasTemps(_params.pasTemps);
    // temps initial
    _obj.tempsInitial(_params.tempsInit);
    // crit�re d'arret du calcul :1-> temps max, 2-> nb temps max, 3->Cote max
    _obj.critereArret(EnumMetierCritereArret.from_int(_params.critereArret - 1));
    // nombre de pas de temps Si critereArret==2
    _obj.nbPasTemps(_params.nbPasTemps);
    // temps maximum Si critereArret==1
    _obj.tempsFinal(_params.tempsMax);
    // cote max de controle Si critereArret==3
    _obj.coteMax(_params.coteMax);
    // abscisse  du point de controle
    _obj.abscisseControle(_params.abscisseControle);
    // numero de bief du point de controle
    _obj.biefControle(_params.biefControle);
    // pas de temps variable suivant nombre de courant
    _obj.pasTempsVariable(_params.pasTempsVar);
    // nombre de courant souhait�
    _obj.nbCourant(_params.nbCourant);
  }
}
