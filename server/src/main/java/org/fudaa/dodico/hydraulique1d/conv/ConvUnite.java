/*
 * @file         ConvUnite.java
 * @creation     2004-03-01
 * @modification $Date: 2007-11-20 11:43:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.conv;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierUnite;
/**
 * Classe qui permet de convertir une cha�ne de caract�re en ��EnumMetierUnite�� et inversement.
 * Elle est utilis�e dans les conversions.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:07 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class ConvUnite {
  public static EnumMetierUnite convertitUnite(String _unite) {
    String unite = _unite.trim();
    if (unite.equalsIgnoreCase("J"))
      return EnumMetierUnite.J;
    else if (unite.equalsIgnoreCase("m"))
      return EnumMetierUnite.M;
    else if (unite.equalsIgnoreCase("m1/3/s"))
      return EnumMetierUnite.M1_TIERS_PAR_S;
    else if (unite.equalsIgnoreCase("m2"))
      return EnumMetierUnite.M2;
    else if (unite.equalsIgnoreCase("m3"))
      return EnumMetierUnite.M3;
    else if (unite.equalsIgnoreCase("m3/s"))
      return EnumMetierUnite.M3_PAR_S;
    else if (unite.equalsIgnoreCase("m/s"))
      return EnumMetierUnite.M_PAR_S;
    else if (unite.equalsIgnoreCase("Pa"))
      return EnumMetierUnite.PA;
    else if (unite.equalsIgnoreCase(""))
      return EnumMetierUnite.RIEN;
    else if (unite.equalsIgnoreCase("s"))
      return EnumMetierUnite.S;
    else return EnumMetierUnite.RIEN;
  }
  public static String convertitUnite(EnumMetierUnite unite) {
    switch(unite.value()) {
      case EnumMetierUnite._J:
        return "J";
      case EnumMetierUnite._M:
        return "m";
      case EnumMetierUnite._M_PAR_S:
        return "m/s";
      case EnumMetierUnite._M1_TIERS_PAR_S:
        return "m1/3/s";
      case EnumMetierUnite._M2:
        return "m2";
      case EnumMetierUnite._M3:
        return "m3";
      case EnumMetierUnite._M3_PAR_S:
        return "m3/s";
      case EnumMetierUnite._PA:
        return "Pa";
      case EnumMetierUnite._RIEN:
        return "";
      case EnumMetierUnite._S:
        return "s";
      default: return "";
    }

  }
}
