/**
 * @file         DLoiGeometrique.java
 * @creation     2000-08-10
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiGeometrique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiGeometriqueOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DLoiHydraulique;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier d'une "loi g�om�trique" des donn�es hydraulique.
 * D�finie une courbe cote = f(distance).
 * @version      $Revision: 1.14 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DLoiGeometrique
  extends DLoiHydraulique
  implements ILoiGeometrique,ILoiGeometriqueOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ILoiGeometrique) {
      ILoiGeometrique l= (ILoiGeometrique)_o;
      d((double[])l.d().clone());
      z((double[])l.z().clone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILoiGeometrique l= UsineLib.findUsine().creeHydraulique1dLoiGeometrique();
    l.initialise(tie());
    return l;
  }
  /*** ILoiGeometrique ***/
  // constructeurs
  public DLoiGeometrique() {
    super();
    nom_= "loi profil 9999999999 crete";
    d_= new double[0];
    z_= new double[0];
  }
  @Override
  public void dispose() {
    nom_= null;
    d_= null;
    z_= null;
    super.dispose();
  }
  // attributs
  private double[] d_;
  @Override
  public double[] d() {
    return d_;
  }
  @Override
  public void d(double[] d) {
    if (Arrays.equals(d,d_)) return;
    d_= d;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "d");
  }
  private double[] z_;
  @Override
  public double[] z() {
    return z_;
  }
  @Override
  public void z(double[] z) {
    if (Arrays.equals(z,z_)) return;
    z_= z;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
  }
  // methodes
  @Override
  public double gdu(int i) {
    return d_[i];
  }
  @Override
  public void sdu(int i, double v) {
    d_[i]= v;
  }
  @Override
  public double gzu(int i) {
    return z_[i];
  }
  @Override
  public void szu(int i, double v) {
    z_[i]= v;
  }
  @Override
  public void creePoint(int indice) {
    int length= Math.min(d_.length, z_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[] newd= new double[length + 1];
    double[] newz= new double[length + 1];
    for (int i= 0; i < indice; i++) {
      newd[i]= d_[i];
      newz[i]= z_[i];
    }
    for (int i= indice; i < length; i++) {
      newd[i + 1]= d_[i];
      newz[i + 1]= z_[i];
    }
    d(newd);
    z(newz);
  }
  @Override
  public void supprimePoints(int[] indices) {
    int length= Math.min(d_.length, z_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[] newd= new double[length - nsup];
    double[] newz= new double[length - nsup];
    for (int i= 0; i < length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (indices[j] != i) {
          newd[i]= d_[i];
          newz[i]= z_[i];
        }
      }
    }
    d(newd);
    z(newz);
  }
  @Override
  public String typeLoi() {
    String classname= getClass().getName();
    int index= classname.lastIndexOf('.');
    if (index >= 0)
      classname= classname.substring(index + 1);
    return classname.substring(4);
  }
  @Override
  public int nbPoints() {
    return Math.min(d_.length, z_.length);
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  // on suppose colonne0:d et colonne1:z
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < d_.length)
          d_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < z_.length)
          z_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:d et colonne1:z
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < d_.length)
          return d_[ligne];
          return Double.NaN;
      case 1 :
        if (ligne < z_.length)
          return z_[ligne];
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {

    double[][] points = CtuluLibArray.transpose(pts);
    
	  if (points == null || points.length == 0) {
		   d_ = new double[0];
		   z_ = new double[0];
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "d");
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
	    	return;
	    	
	    } else {

    boolean debitModif = !Arrays.equals(d_,points[0]);
    boolean zModif = !Arrays.equals(z_,points[1]);

    if (debitModif || zModif) {
      d_ = points[0];
      z_ = points[1];
      if (debitModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "d");
      if (zModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
    }
	    }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[2][d_.length];
    tableau[0]= (double[])d_.clone();
    tableau[1]= (double[])z_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
