/**
 * @file         DLoiLimniHydrogramme.java
 * @creation     2000-08-10
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimniHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimniHydrogrammeOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DLoiHydraulique;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier d'une "loi limni-hydrogramme" des donn�es hydraulique.
 * D�finie 2 courbes : d�bit = f(temps) et cote = f(temps).
 * @version      $Revision: 1.13 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DLoiLimniHydrogramme
  extends DLoiHydraulique
  implements ILoiLimniHydrogramme,ILoiLimniHydrogrammeOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ILoiLimniHydrogramme) {
      ILoiLimniHydrogramme l= (ILoiLimniHydrogramme)_o;
      t((double[])l.t().clone());
      z((double[])l.z().clone());
      q((double[])l.q().clone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILoiLimniHydrogramme l=
      UsineLib.findUsine().creeHydraulique1dLoiLimniHydrogramme();
    l.initialise(tie());
    return l;
  }
  /*** ILoiGeometrique ***/
  // constructeurs
  public DLoiLimniHydrogramme() {
    super();
    nom_= "loi 9999999999 limnihydrogramme";
    t_= new double[0];
    z_= new double[0];
    q_= new double[0];
  }
  @Override
  public void dispose() {
    nom_= null;
    t_= null;
    z_= null;
    q_= null;
    super.dispose();
  }
  // attributs
  private double[] t_;
  @Override
  public double[] t() {
    return t_;
  }
  @Override
  public void t(double[] t) {
    if (Arrays.equals(t,t_)) return;
    t_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "t");
  }
  private double[] z_;
  @Override
  public double[] z() {
    return z_;
  }
  @Override
  public void z(double[] z) {
    if (Arrays.equals(z,z_)) return;
    z_= z;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
  }
  private double[] q_;
  @Override
  public double[] q() {
    return q_;
  }
  @Override
  public void q(double[] q) {
    if (Arrays.equals(q,q_)) return;
    q_= q;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "q");
  }
  // methodes
  @Override
  public double gtu(int i) {
    return t_[i];
  }
  @Override
  public void stu(int i, double v) {
    t_[i]= v;
  }
  @Override
  public double gzu(int i) {
    return z_[i];
  }
  @Override
  public void szu(int i, double v) {
    z_[i]= v;
  }
  @Override
  public double gqu(int i) {
    return q_[i];
  }
  @Override
  public void squ(int i, double v) {
    q_[i]= v;
  }
  @Override
  public void creePoint(int i) {}
  @Override
  public void supprimePoints(int[] i) {}
  @Override
  public String typeLoi() {
    String classname= getClass().getName();
    int index= classname.lastIndexOf('.');
    if (index >= 0)
      classname= classname.substring(index + 1);
    return classname.substring(4);
  }
  @Override
  public int nbPoints() {
    return Math.min(Math.min(t_.length, z_.length), q_.length);
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  // on suppose colonne0:t et colonne1:z et colonne2:q
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          t_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < z_.length)
          z_[ligne]= valeur;
        break;
      case 2 :
        if (ligne < q_.length)
          q_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:t et colonne1:z et colonne2:q
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          return t_[ligne];
        else
          return Double.NaN;
      case 1 :
        if (ligne < z_.length)
          return z_[ligne];
        else
          return Double.NaN;
      case 2 :
        if (ligne < q_.length)
          return q_[ligne];
        else
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {
    double[][] points = CtuluLibArray.transpose(pts);
    
	  if (points == null || points.length == 0) {
		   t_ = new double[0];
		   z_ = new double[0];
		   q_ = new double[0];
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "t");
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "q");
	    	return;
	    	
	    } else {

    boolean tModif = !Arrays.equals(t_,points[0]);
    boolean zModif = !Arrays.equals(z_,points[1]);
    boolean qModif = !Arrays.equals(q_,points[2]);

    if (tModif || zModif || qModif) {
      t_ = points[0];
      z_ = points[1];
      q_ = points[2];
      if (tModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "t");
      if (zModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
      if (qModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "q");
    }
	    }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[3][t_.length];
    tableau[0]= (double[])t_.clone();
    tableau[1]= (double[])z_.clone();
    tableau[2]= (double[])q_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
