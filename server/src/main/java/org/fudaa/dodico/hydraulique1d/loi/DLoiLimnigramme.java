/**
 * @file         DLoiLimnigramme.java
 * @creation     2000-08-10
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigrammeOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DLoiHydraulique;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier d'une "loi limnigramme" des donn�es hydraulique.
 * D�finie une courbe cote = f(temps).
 * @version      $Revision: 1.14 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DLoiLimnigramme
  extends DLoiHydraulique
  implements ILoiLimnigramme,ILoiLimnigrammeOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ILoiLimnigramme) {
      ILoiLimnigramme l= (ILoiLimnigramme)_o;
      t((double[])l.t().clone());
      z((double[])l.z().clone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILoiLimnigramme l= UsineLib.findUsine().creeHydraulique1dLoiLimnigramme();
    l.initialise(tie());
    return l;
  }
  /*** ILoiLimnigramme ***/
  // constructeurs
  public DLoiLimnigramme() {
    super();
    nom_= "loi 9999999999 limnigramme";
    t_= new double[0];
    z_= new double[0];
  }
  @Override
  public void dispose() {
    t_= null;
    z_= null;
    super.dispose();
  }
  // attributs
  private double[] t_;
  @Override
  public double[] t() {
    return t_;
  }
  @Override
  public void t(double[] t) {
    if (Arrays.equals(t,t_)) return;
    t_= t;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "t");
  }
  private double[] z_;
  @Override
  public double[] z() {
    return z_;
  }
  @Override
  public void z(double[] z) {
    if (Arrays.equals(z,z_)) return;
    z_= z;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
  }
  // methodes
  @Override
  public double gtu(int i) {
    return t_[i];
  }
  @Override
  public void stu(int i, double v) {
    t_[i]= v;
  }
  @Override
  public double gzu(int i) {
    return z_[i];
  }
  @Override
  public void szu(int i, double v) {
    z_[i]= v;
  }
  @Override
  public void creePoint(int indice) {
    int length= Math.min(t_.length, z_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[] newt= new double[length + 1];
    double[] newz= new double[length + 1];
    for (int i= 0; i < indice; i++) {
      newt[i]= t_[i];
      newz[i]= z_[i];
    }
    for (int i= indice; i < length; i++) {
      newt[i + 1]= t_[i];
      newz[i + 1]= z_[i];
    }
    t(newt);
    z(newz);
  }

  @Override
  public void supprimePoints(int[] indices) {
    int length= Math.min(t_.length, z_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[] newt= new double[length - nsup];
    double[] newz= new double[length - nsup];
    for (int i= 0; i < length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (indices[j] != i) {
          newt[i]= t_[i];
          newz[i]= z_[i];
        }
      }
    }
    t(newt);
    z(newz);
  }
  @Override
  public String typeLoi() {
    String classname= getClass().getName();
    int index= classname.lastIndexOf('.');
    if (index >= 0)
      classname= classname.substring(index + 1);
    return classname.substring(4);
  }
  @Override
  public int nbPoints() {
    return Math.min(t_.length, z_.length);
  }
  @Override
  public boolean verifiePermanent() {
    if ((t_ == null) || (z_ == null) || (z_.length == 0))
      return false;
    boolean res= false;
    double q0= z_[0];
    for (int i= 1; i < z_.length; i++) {
      if (z_[i] != q0) {
        res= false;
        break;
      }
    }
    return res;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    if ((t_ == null) || (t_.length == 0))
      return false;
    boolean res= true;
    for (int i= 1; i < t_.length; i++) {
      if (t_[i] <= t_[i - 1]) {
        res= false;
      }
    }
    return res;
  }
  @Override
  public boolean verifieCote(double cote) {
    if ((z_ == null) || (z_.length == 0))
      return false;
    boolean res= true;
    for (int i= 0; i < z_.length; i++) {
      if (z_[i] <= cote) {
        res= false;
      }
    }
    return res;
  }
  // on suppose colonne0:t et colonne1:z
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          t_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < z_.length)
          z_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:t et colonne1:z
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          return t_[ligne];
        else
          return Double.NaN;
      case 1 :
        if (ligne < z_.length)
          return z_[ligne];
        else
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {
    double[][] points = CtuluLibArray.transpose(pts);
    
	  if (points == null || points.length == 0) {
		   t_ = new double[0];
		   z_ = new double[0];
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "t");
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
	    	return;
	    	
	    } else {

    boolean tModif = !Arrays.equals(t_,points[0]);
    boolean zModif = !Arrays.equals(z_,points[1]);

    if (tModif || zModif) {
      t_ = points[0];
      z_ = points[1];
      if (tModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "t");
      if (zModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
    }
  }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[2][t_.length];
    tableau[0]= (double[])t_.clone();
    tableau[1]= (double[])z_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
