/**
 * @file         DLoiSeuil.java
 * @creation     2000-08-10
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiSeuil;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiSeuilOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DLoiHydraulique;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier d'une "loi de seuil" des donn�es hydraulique.
 * D�finie des courbes d�bit = f(cote amont, cote aval).
 * @version      $Revision: 1.14 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DLoiSeuil extends DLoiHydraulique implements ILoiSeuil,ILoiSeuilOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ILoiSeuil) {
      ILoiSeuil l= (ILoiSeuil)_o;
      zAmont((double[][])l.zAmont().clone());
      zAval((double[])l.zAval().clone());
      q((double[])l.q().clone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILoiSeuil l= UsineLib.findUsine().creeHydraulique1dLoiSeuil();
    l.initialise(tie());
    return l;
  }
  /*** ILoiGeometrique ***/
  // constructeurs
  public DLoiSeuil() {
    super();
    nom_= "loi 9999999999 seuil";
    zAmont_= new double[1][1];
    zAval_= new double[1];
    q_= new double[1];
  }
  @Override
  public void dispose() {
    zAmont_= null;
    zAval_= null;
    q_= null;
    super.dispose();
  }
  // attributs
  private double[][] zAmont_;
  @Override
  public double[][] zAmont() {
    return zAmont_;
  }
  @Override
  public void zAmont(double[][] zAmont) {
    if (Arrays.equals(zAmont,zAmont_)) return;
    zAmont_= zAmont;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zAmont");
  }
  private double[] zAval_;
  @Override
  public double[] zAval() {
    return zAval_;
  }
  @Override
  public void zAval(double[] zAval) {
    if (Arrays.equals(zAval,zAval_)) return;
    zAval_= zAval;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zAval");
  }
  private double[] q_;
  @Override
  public double[] q() {
    return q_;
  }
  @Override
  public void q(double[] q) {
    if (Arrays.equals(q,q_)) return;
    q_= q;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "q");
  }
  // methodes
  @Override
  public double gzAmontu(int i, int j) {
    return zAmont_[i][j];
  }
  @Override
  public void szAmontu(int i, int j, double v) {
    zAmont_[i][j]= v;
  }
  @Override
  public double gzAvalu(int i) {
    return zAval_[i];
  }
  @Override
  public void szAvalu(int i, double v) {
    zAval_[i]= v;
  }
  @Override
  public double gqu(int i) {
    return q_[i];
  }
  @Override
  public void squ(int i, double v) {
    q_[i]= v;
  }
  @Override
  public void creePoint(int i) {}
  @Override
  public void supprimePoints(int[] i) {}
  @Override
  public void creePointDebit(int indice) {
    int length= Math.min(zAval_.length, q_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[][] newz= new double[length + 1][];
    double[] newq= new double[length + 1];
    for (int i= 0; i < indice; i++) {
      newz[i]= zAmont_[i];
      newq[i]= q_[i];
    }
    newz[indice]= new double[zAval_.length];
    for (int i= indice; i < length; i++) {
      newz[i + 1]= zAmont_[i];
      newq[i + 1]= q_[i];
    }
    zAmont(newz);
    q(newq);
  }
  @Override
  public void supprimePointsDebit(int[] indices) {
    int length= Math.min(zAval_.length, q_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[][] newz= new double[length - nsup][];
    double[] newq= new double[length - nsup];
    for (int i= 0; i < length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (indices[j] != i) {
          newz[i]= zAmont_[i];
          newq[i]= q_[i];
        }
      }
    }
    zAmont(newz);
    q(newq);
  }
  @Override
  public void creePointCoteAval(int indice) {
    if (q_.length == 0)
      return;
    int length= Math.min(q_.length, zAval_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[][] newzm= new double[q_.length][];
    double[] newzv= new double[length + 1];
    for (int debit= 0; debit < q_.length; debit++) {
      double[] newzmu= new double[length + 1];
      for (int i= 0; i < indice; i++) {
        newzmu[i]= zAmont_[debit][i];
        newzv[i]= zAval_[i];
      }
      for (int i= indice; i < length; i++) {
        newzmu[i + 1]= zAmont_[debit][i];
        newzv[i + 1]= zAval_[i];
      }
      newzm[debit]= newzmu;
    }
    zAmont(newzm);
    zAval(newzv);
  }
  @Override
  public void supprimePointsCoteAval(int[] indices) {
    if (q_.length == 0)
      return;
    int length= Math.min(zAval_.length, q_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[][] newzm= new double[q_.length][];
    double[] newzv= new double[length - nsup];
    for (int debit= 0; debit < q_.length; debit++) {
      double[] newzmu= new double[length - nsup];
      for (int i= 0; i < length; i++) {
        for (int j= 0; j < indices.length; j++) {
          if (indices[j] != i) {
            newzmu[i]= zAmont_[debit][i];
            newzv[i]= zAval_[i];
          }
        }
      }
      newzm[debit]= newzmu;
    }
    zAmont(newzm);
    zAval(newzv);
  }
  @Override
  public String typeLoi() {
    String classname= getClass().getName();
    int index= classname.lastIndexOf('.');
    if (index >= 0)
      classname= classname.substring(index + 1);
    return classname.substring(4);
  }
  @Override
  public int nbPoints() {
    return q_.length;
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  // on suppose colonne0:q et ligne0:zAval
  //            (ligne1, colonne1)=zAmont[0][0]
  //            (lignei, colonnej)=zAmont[i+1][j+1]
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    try {
      if (colonne == 0)
        q_[ligne]= valeur;
      else if (ligne == 0)
        zAval_[colonne]= valeur;
      else
        zAmont_[ligne - 1][colonne - 1]= valeur;
    } catch (ArrayIndexOutOfBoundsException e) {}
  }
  // on suppose colonne0:q et ligne0:zAval
  //            (ligne1, colonne1)=zAmont[0][0]
  //            (lignei, colonnej)=zAmont[i+1][j+1]
  @Override
  public double getValeur(int ligne, int colonne) {
    try {
      if (colonne == 0)
        return q_[ligne];
      else if (ligne == 0)
        return zAval_[colonne];
      else
        return zAmont_[ligne - 1][colonne - 1];
    } catch (ArrayIndexOutOfBoundsException e) {
      return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] points) {
    //CtuluLibMessage.DEBUG = true;
    CtuluLibMessage.println("DLOiSeuil setPoints",points);
    double[] q= new double[points.length - 1];
    double[] zAval= new double[points[0].length - 1];
    double[][] zAmont= new double[points.length - 1][points[0].length - 1];
    for (int i= 0; i < points.length; i++) {
      if (i > 0)
        q[i - 1]= points[i][0];
      for (int j= 0; j < points[i].length; j++) {
        if ((i == 0) && (j > 0))
          zAval[j - 1]= points[0][j];
        else if ((i > 0) && (j > 0))
          zAmont[i - 1][j - 1]= points[i][j];
      }
    }
    boolean debitModif  = !Arrays.equals(q,q_);
    boolean zAvalModif  = !Arrays.equals(zAval,zAval_);
    boolean zAmontModif = !CtuluLibArray.equals(zAmont,zAmont_);
    CtuluLibMessage.println("zAmontModif="+zAmontModif);
    CtuluLibMessage.println("zAmont",zAmont);
    CtuluLibMessage.println("zAmont_",zAmont_);

    if (debitModif || zAvalModif || zAmontModif) {
      q_= q;
      CtuluLibMessage.println("DLOiSeuil q_",q_);
      zAval_ = zAval;
      CtuluLibMessage.println("DLOiSeuil zAval_",zAval_);
      zAmont_ =zAmont;
      CtuluLibMessage.println("DLOiSeuil zAmont_",zAmont_);
      if (debitModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "q");
      if (zAvalModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zAval");
      if (zAmontModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "zAmont");
    }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[zAmont_.length + 1][zAmont_[0].length + 1];
    tableau[0][0]= Double.POSITIVE_INFINITY;
    for (int i= 0; i < tableau.length; i++) {
      if (i > 0)
        tableau[i][0]= q_[i - 1];
      for (int j= 0; j < tableau[i].length; j++) {
        if ((i == 0) && (j > 0))
          tableau[0][j]= zAval_[j - 1];
        else if ((i > 0) && (j > 0))
          tableau[i][j]= zAmont_[i - 1][j - 1];
      }
    }
    //CtuluLibMessage.DEBUG = true;
    CtuluLibMessage.println("DLOiSeuil pointsToDoubleArray", tableau);
    return tableau;
  }
  @Override
  public double[][] arrayZaval() {
    double[][] tableau= new double[zAmont_[0].length + 1][q_.length];
    for (int i= 0; i < tableau[0].length; i++) {
      tableau[0][i]= q_[i];
      for (int j= 1; j < tableau.length; j++) {
        tableau[j][i]= zAmont_[i][j - 1];
      }
    }
    return tableau;
  }
  @Override
  public double[][] arrayDebit() {
    double[][] tableau= new double[zAval_.length][zAmont_.length + 1];
    for (int i= 0; i < tableau.length; i++) {
      tableau[i][0]= zAval_[i];
      for (int j= 1; j < tableau[i].length; j++) {
        tableau[i][j]= zAmont_[j - 1][i];
      }
    }
    return tableau;
  }
}
