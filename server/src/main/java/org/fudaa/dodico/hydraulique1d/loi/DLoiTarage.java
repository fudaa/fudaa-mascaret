/**
 * @file         DLoiTarage.java
 * @creation     2000-08-10
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarageOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DLoiHydraulique;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier d'une "loi de tarage" des donn�es hydraulique.
 * D�finie soit une courbe d�bit = f(cote) ou cote = f(d�bit) suivant la valeur de l'indicateur amonZ.
 * @version      $Revision: 1.14 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DLoiTarage
  extends DLoiHydraulique
  implements ILoiTarage,ILoiTarageOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ILoiTarage) {
      ILoiTarage l= (ILoiTarage)_o;
      amont(l.amont());
      z((double[])l.z().clone());
      q((double[])l.q().clone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ILoiTarage l= UsineLib.findUsine().creeHydraulique1dLoiTarage();
    l.initialise(tie());
    return l;
  }
  /*** ILoiTarage ***/
  // constructeurs
  public DLoiTarage() {
    super();
    nom_= "loi 9999999999 tarage";
    amonz_= true;
    z_= new double[0];
    q_= new double[0];
  }
  @Override
  public void dispose() {
    nom_= null;
    amonz_= true;
    z_= null;
    q_= null;
    super.dispose();
  }
  // attributs
  private boolean amonz_;
  @Override
  public boolean amont() {
    return amonz_;
  }
  @Override
  public void amont(boolean z) {
    amonz_= z;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "amont");
  }
  private double[] z_;
  @Override
  public double[] z() {
    return z_;
  }
  @Override
  public void z(double[] z) {
    if (Arrays.equals(z,z_)) return;
    z_= z;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
  }
  private double[] q_;
  @Override
  public double[] q() {
    return q_;
  }
  @Override
  public void q(double[] q) {
    if (Arrays.equals(q,q_)) return;
    q_= q;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "q");
  }
  // methodes
  @Override
  public double gzu(int i) {
    return z_[i];
  }
  @Override
  public void szu(int i, double v) {
    z_[i]= v;
  }
  @Override
  public double gqu(int i) {
    return q_[i];
  }
  @Override
  public void squ(int i, double v) {
    q_[i]= v;
  }
  @Override
  public void creePoint(int indice) {
    int length= Math.min(z_.length, q_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[] newz= new double[length + 1];
    double[] newq= new double[length + 1];
    for (int i= 0; i < indice; i++) {
      newz[i]= z_[i];
      newq[i]= q_[i];
    }
    for (int i= indice; i < length; i++) {
      newz[i + 1]= z_[i];
      newq[i + 1]= q_[i];
    }
    z(newz);
    q(newq);
  }
  @Override
  public void supprimePoints(int[] indices) {
    int length= Math.min(z_.length, q_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[] newz= new double[length - nsup];
    double[] newq= new double[length - nsup];
    for (int i= 0; i < length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (indices[j] != i) {
          newz[i]= z_[i];
          newq[i]= q_[i];
        }
      }
    }
    z(newz);
    q(newq);
  }
  @Override
  public String typeLoi() {
    String classname= getClass().getName();
    int index= classname.lastIndexOf('.');
    if (index >= 0)
      classname= classname.substring(index + 1);
    return classname.substring(4);
  }
  @Override
  public int nbPoints() {
    return Math.min(z_.length, q_.length);
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  @Override
  public boolean verifieCote(double cote) {
    if ((z_ == null) || (z_.length == 0))
      return false;
    boolean res= true;
    for (int i= 0; i < z_.length; i++) {
      if (z_[i] <= cote) {
        res= false;
      }
    }
    return res;
  }
  // on suppose colonne0:q et colonne1:z
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < q_.length)
          q_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < z_.length)
          z_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:q et colonne1:z
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < q_.length)
          return q_[ligne];
        else
          return Double.NaN;
      case 1 :
        if (ligne < z_.length)
          return z_[ligne];
        else
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {
    double[][] points = CtuluLibArray.transpose(pts);
    
    if (points == null || points.length == 0) {
    	q_ = new double[0];
    	z_ = new double[0];
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "q");
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
	    	return;
	    	
	    } else {
    boolean zModif = !Arrays.equals(q_,points[0]);
    boolean qModif = !Arrays.equals(z_,points[1]);

    if (zModif || qModif) {
      z_ = points[1];
      q_ = points[0];
      if (zModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "z");
      if (qModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "q");
    }
    q(points[0]);
    z(points[1]);
	    }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[2][q_.length];
    tableau[0]= (double[])q_.clone();
    tableau[1]= (double[])z_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
