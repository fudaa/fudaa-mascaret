/**
 * @file         DLoiTracer.java
 * @creation     2006-02-27
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */

package org.fudaa.dodico.hydraulique1d.loi;

import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTracer;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DLoiHydraulique;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Impl�mentation de l'objet m�tier d'une "loi tracer" des donn�es de qualit� d'eau.
 * D�finie des courbes concentration = f(temps).
 * @version      $Revision: 1.6 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 * @author       Olivier Pasteur
 */
public class DLoiTracer extends DLoiHydraulique implements ILoiTracer {

  @Override
    public void initialise(IObjet _o) {
        super.initialise(_o);
        if (_o instanceof ILoiTracer) {
            ILoiTracer l = (ILoiTracer) _o;
            conc((double[][])l.conc().clone());
        }
    }

  @Override
    final public IObjet creeClone() {
        ILoiTracer l = UsineLib.findUsine().creeHydraulique1dLoiTracer();
        l.initialise(tie());
        return l;
    }

     // constructeurs
    public DLoiTracer() {
        super();
        nom_ = "loi 9999999999 tracer";
        conc_ = new double[0][0];

    }

  @Override
    public void dispose() {
      conc_= null;
      super.dispose();
  }



    /**
     * creePoint
     *
     * @param indice int
     */
  @Override
    public void creePoint(int indice) {
        //@todo Cette methode n'est jamais utilis�e
    }


    /**
     * getValeur
     *
     * @param ligne int
     * @param colonne int
     * @return double
     */
  @Override
    public double getValeur(int ligne, int colonne) {
        try {
                return conc_[colonne][ligne];
        } catch (ArrayIndexOutOfBoundsException e) {
            return Double.NaN;
        }
    }



    /**
     * nbPoints
     *
     * @return int
     */
  @Override
    public int nbPoints() {
    	if (conc_ == null) return 0;
        if (conc_.length>0)
            return conc_[0].length;
        else
            return 0;
    }




    /**
     * pointsToDoubleArray
     *
     * @return double[][]
     *   method
     */
  @Override
    public double[][] pointsToDoubleArray() {

        return conc_;//CtuluLibArray.transpose(tableau);

    }



    /**
     * setPoints
     *
     * @param points double[][]
     */
  @Override
    public void setPoints(double[][] points) {
        CtuluLibMessage.println("DLoiTracer setPoints",points);


    boolean concModif  = !CtuluLibArray.equals(points,conc_);
    CtuluLibMessage.println("concModif="+concModif);


    if (concModif) {
      conc_ = points;
      CtuluLibMessage.println("DLoiTracer conc_",conc_);

      if (concModif)
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "conc");

    }

    }

    /**
     * setValeur
     *
     * @param valeur double
     * @param ligne int
     * @param colonne int
     *   // on suppose colonne0:t et les suivantes:conc
     */
  @Override
    public void setValeur(double valeur, int ligne, int colonne) {
        try {
                conc_[colonne][ligne] = valeur;
        } catch (ArrayIndexOutOfBoundsException e) {}
    }

    /**
     * supprimePoints
     *
     * @param indices int[]
     */
  @Override
    public void supprimePoints(int[] indices) {
                //@todo Cette methode n'est jamais utilis�e
    }



    /**
     * verifiePermanent
     *
     * @return boolean
     */
  @Override
    public boolean verifiePermanent() {
                //@todo Cette methode n'est jamais utilis�e
        return false;
    }

    /**
     * verifieTempsNonPermanent
     *
     * @return boolean
     */
  @Override
    public boolean verifieTempsNonPermanent() {
       //@todo Cette methode n'est jamais utilis�e
        return false;
    }

    // attributs
        /**
         * conc
         * @param newConc double[][]
         */
        private double[][] conc_;
  @Override
        public void conc(double[][] newConc) {
            if (Arrays.equals(newConc,conc_)) return;
            conc_= newConc;
            UsineLib.findUsine().fireObjetModifie(toString(), tie(), "conc");
        }

        /**
         * conc
         * @return double[][]
         */
  @Override
        public double[][] conc() {
            return conc_;
        }




    /**
     * typeLoi
     *
     * @return String
     */
  @Override
    public String typeLoi() {
        String classname= getClass().getName();
        int index = classname.lastIndexOf('.');
        if (index >= 0)
            classname = classname.substring(index + 1);
        return classname.substring(4);
    }
    /**
     * Modifie la taille du tableau en fonction du nombre de traceur
     *
     * @param nbLigne int
     * @param nbColonne int
     *
     */
  @Override
    public void setTailleTableau(int nbColonne) {
        try {
                int nbLigne = 0;
                conc_= new double[nbLigne][nbColonne];

        } catch (ArrayIndexOutOfBoundsException e) {
        System.out.println("Une erreur est survenue lors du redimenssionnement du tableau");}
    }


}
