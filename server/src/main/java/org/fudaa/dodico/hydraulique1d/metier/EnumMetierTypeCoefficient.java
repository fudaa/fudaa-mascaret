package org.fudaa.dodico.hydraulique1d.metier;

public class EnumMetierTypeCoefficient {
	
		  private        int __value;
		  private static int __size = 2;
		  private static org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient[] __array = new org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient [__size];
		  
		  public static final int _STRICKLER = 0;
		  public static final int _MANNING = 1;
		  public static final org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient STRICKLER = new org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient(_STRICKLER);
		  public static final org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient MANNING = new org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient(_MANNING);
			
		  public int value ()
		  {
		    return __value;
		  }

		  public static org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient from_int (int value)
		  {
		    if (value >= 0 && value < __size)
		      return __array[value];
		    else
		      throw new org.omg.CORBA.BAD_PARAM ();
		  }

		  //  public EnumMetierTypeFrottement ()
		  //  {}
		  protected EnumMetierTypeCoefficient (int value)
		  {
		    __value = value;
		    __array[__value] = this;
		  }
		} // class EnumMetierTypeFrottement
