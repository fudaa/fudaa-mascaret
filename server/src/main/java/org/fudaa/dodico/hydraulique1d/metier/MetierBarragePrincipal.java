/**
 * @file         MetierBarragePrincipal.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/**
 * Implémentation de l'objet métier "barrage principal".
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:34 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierBarragePrincipal
  extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierBarragePrincipal) {
      MetierBarragePrincipal q= (MetierBarragePrincipal)_o;
      site((MetierSite)q.site().creeClone());
      ruptureProgressive(q.ruptureProgressive());
      cotePlanEau(q.cotePlanEau());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierBarragePrincipal p=new MetierBarragePrincipal();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "barragePrincipal";
    if (site_ != null)
      s += " (" + site_.toString() + ")";
    return s;
  }
  /*** MetierBarragePrincipal ***/
  // constructeurs
  public MetierBarragePrincipal() {
    super();
    site_= new MetierSite();
    ruptureProgressive_= false;
    cotePlanEau_= 0.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    site_= null;
    ruptureProgressive_= false;
    cotePlanEau_= 0.;
    super.dispose();
  }
  // Attributs
  private MetierSite site_;
  public MetierSite site() {
    return site_;
  }
  public void site(MetierSite s) {
    site_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "site");
  }
  private boolean ruptureProgressive_;
  public boolean ruptureProgressive() {
    return ruptureProgressive_;
  }
  public void ruptureProgressive(boolean s) {
    ruptureProgressive_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "ruptureProgressive");
  }
  private double cotePlanEau_;
  public double cotePlanEau() {
    return cotePlanEau_;
  }
  public void cotePlanEau(double s) {
    cotePlanEau_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "cotePlanEau");
  }
}
