/*
 * @file         MetierBief.java
 * @creation     2000-07-24
 * @modification $Date: 2007-11-20 11:42:33 $
 * @modification $Date: 2007-11-20 11:42:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierBarrage;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementZCoefQ;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilDenoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLimniAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilNoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAval;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSource;
import org.fudaa.dodico.hydraulique1d.CGlobal;

/**
 * Impl�mentation de l'objet m�tier "bief" (�l�ment du r�seau hydraulique).
 * Contient 2 extremit�s (amont et aval), un tableau de profils.
 * On peut y attacher des singularit�s.
 * On associe les zones de frottements et de planim�trage.
 * Les r�sultats biefs et les sections calcul�es ne sont pas utilis�s dans Fudaa-Mascaret.
 *
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:33 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierBief extends MetierHydraulique1d {
  public final static BiefComparator COMPARATOR= new BiefComparator();
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierBief) {
      MetierBief q= (MetierBief)_o;
      if (q.profils() != null) {
        MetierProfil[] ip= new MetierProfil[q.profils().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (MetierProfil)q.profils()[i].creeClone();
        profils(ip);
      }
      if (q.extrAval() != null)
        extrAval((MetierExtremite)q.extrAval());
      if (q.extrAmont() != null)
        extrAmont((MetierExtremite)q.extrAmont());
      if (q.zonesFrottement() != null) {
        MetierZoneFrottement[] izf= new MetierZoneFrottement[q.zonesFrottement().length];
        for (int i= 0; i < izf.length; i++) {
          izf[i] = (MetierZoneFrottement) q.zonesFrottement()[i].creeClone();
        }
        zonesFrottement(izf);
      }
      if (q.zonesPlanimetrage() != null) {
        MetierZonePlanimetrage[] izp=
          new MetierZonePlanimetrage[q.zonesPlanimetrage().length];
        for (int i= 0; i < izp.length; i++)
          izp[i]= (MetierZonePlanimetrage)q.zonesPlanimetrage()[i].creeClone();
        zonesPlanimetrage(izp);
      }
/*      if (q.singularites() != null) {
        MetierSingularite[] is= new MetierSingularite[q.singularites().length];
        for (int i= 0; i < is.length; i++)
          is[i]= (MetierSingularite)q.singularites()[i].creeClone();
        singularites(is);
      }*/
      if (q.sectionsCalculees() != null) {
        MetierSectionCalculee[] is=
          new MetierSectionCalculee[q.sectionsCalculees().length];
        for (int i= 0; i < is.length; i++)
          is[i]= (MetierSectionCalculee)q.sectionsCalculees()[i].creeClone();
        sectionsCalculees(is);
      }
//      resultatsBief((DResultatsBief)q.resultatsBief().creeClone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierBief p= new MetierBief();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= getS("bief")+" " + (indice_+1);
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Bief")+(indice_+1);
    res[1]= getS("Abscisse d�but")+" ";
    try {
      res[1]= res[1] + extrAmont_.profilRattache().abscisse();
    } catch (NullPointerException e) {
      res[1]= res[1] + getS("inconnu");
    }
    res[1]= res[1] + " "+getS("Abscisse fin")+" ";
    try {
      res[1]= res[1] + extrAval_.profilRattache().abscisse();
    } catch (NullPointerException e) {
      res[1]= res[1] + getS("inconnu");
    }
    return res;
  }
  /*** MetierBief ***/
  // constructeurs
  public MetierBief() {
    super();
    numero_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    indice_=0;
    profils_= new MetierProfil[0];
    extrAmont_= new MetierExtremite();
    extrAval_= new MetierExtremite();
    zonesFrottement_= new MetierZoneFrottement[0];
    zonesPlanimetrage_= new MetierZonePlanimetrage[0];
    singularites_= new MetierSingularite[0];
    sectionsCalculees_= new MetierSectionCalculee[0];
    resultatsBief_= null;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    indice_=-1;
    profils_= null;
    extrAmont_= null;
    extrAval_= null;
    zonesFrottement_= null;
    zonesPlanimetrage_= null;
    singularites_= null;
    sectionsCalculees_= null;
    resultatsBief_= null;
  }
  // attributs
  private int indice_=-1;
  public int indice() {
    return indice_;
  }
  
  /**
   * met � jour l'indice du bief ( si n�cessaire) mais ne change pas les numeros des extremit�s
   * @param s 
   */
  public void indiceNoExtremiteChange(int s) {
    if (indice_==s) return;
    indice_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "indice");
  }
   /**
   * met � jour l'indice du bief ( si n�cessaire) ET change les numeros des extremit�s
   * @param s 
   */
  public void indice(int s) {
    if (indice_==s) return;
    indice_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "indice");
    if (extrAmont_ != null) {
      extrAmont_.numero(2*indice_+1);
    }
    if (extrAval_ != null) {
      extrAval_.numero(2*indice_+2);
    }
  }
  private int numero_;
  public int numero() {
    return numero_;
  }
  public void numero(int s) {
    if (numero_==s) return;
    numero_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numero");
  }
  private MetierProfil[] profils_;
  public MetierProfil[] profils() {
    return profils_;
  }
  public void profils(MetierProfil[] s) {
    profils(s, true);
  }
  public void profils(MetierProfil[] s, boolean _trie) {
    if (egale(profils_ , s)) {
      if (_trie)
        trieProfils();
      miseAJourExtremite();
    } else {
      profils_= s;
      if (_trie)
        trieProfils();
      miseAJourExtremite();
      Notifieur.getNotifieur().fireObjetModifie(toString(), this, "profils");
    }
  }
  private MetierExtremite extrAval_;
  public MetierExtremite extrAval() {
    return extrAval_;
  }
  public void extrAval(MetierExtremite s) {
    if (extrAval_==s) return;
    extrAval_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "extrAval");
  }
  private MetierExtremite extrAmont_;
  public MetierExtremite extrAmont() {
    return extrAmont_;
  }
  public void extrAmont(MetierExtremite s) {
    if (extrAmont_==s) return;
    extrAmont_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "extrAmont");
  }
  private MetierZoneFrottement[] zonesFrottement_;
  public MetierZoneFrottement[] zonesFrottement() {
    return zonesFrottement_;
  }
  public void zonesFrottement(MetierZoneFrottement[] s) {
    if (egale(zonesFrottement_,s)) return;
    zonesFrottement_= s;
    trieZonesFrottement();
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zonesFrottement");
  }
  private MetierZonePlanimetrage[] zonesPlanimetrage_;
  public MetierZonePlanimetrage[] zonesPlanimetrage() {
    return zonesPlanimetrage_;
  }
  public void zonesPlanimetrage(MetierZonePlanimetrage[] s) {
    if (egale(zonesPlanimetrage_,s)) return;
    zonesPlanimetrage_= s;
    trieZonesPlanimetrage();
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "zonesPlanimetrage");
  }
  private MetierSingularite[] singularites_;
  public MetierSingularite[] singularites() {
    return singularites_;
  }
  public void singularites(MetierSingularite[] s) {
    if (egale(singularites_,s)) return;
    singularites_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "singularites");
  }
  private MetierSectionCalculee[] sectionsCalculees_;
  public MetierSectionCalculee[] sectionsCalculees() {
    return sectionsCalculees_;
  }
  public void sectionsCalculees(MetierSectionCalculee[] s) {
    if (egale(sectionsCalculees_,s)) return;
    sectionsCalculees_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "sectionsCalculees");
  }
  private MetierResultatsBief resultatsBief_;
  public MetierResultatsBief resultatsBief() {
    return resultatsBief_;
  }
  public void resultatsBief(MetierResultatsBief s) {
    if (resultatsBief_== s) return;
    resultatsBief_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatsBief");
  }
  // methodes
  public MetierApport creeApport() {
    MetierApport sing= new MetierApport();
    ajouteSingularite(sing);
    return sing;
  }
  public MetierSource creeSource() {
  MetierSource sing= new MetierSource();
  ajouteSingularite(sing);
  return sing;
}

  public MetierPerteCharge creePerteCharge() {
    MetierPerteCharge sing= new MetierPerteCharge();
    ajouteSingularite(sing);
    return sing;
  }
  public MetierSeuilNoye creeSeuilNoye() {
    MetierSeuilNoye sing= new MetierSeuilNoye();
    ajouteSingularite(sing);
    return sing;
  }
  public MetierSeuilDenoye creeSeuilDenoye() {
    MetierSeuilDenoye sing= new MetierSeuilDenoye();
    ajouteSingularite(sing);
    return sing;
  }
  public MetierSeuilGeometrique creeSeuilGeometrique() {
    MetierSeuilGeometrique sing=
      new MetierSeuilGeometrique();
    ajouteSingularite(sing);
    return sing;
  }
  public MetierSeuilTarageAmont creeSeuilTarageAmont() {
    MetierSeuilTarageAmont sing=
      new MetierSeuilTarageAmont();
    ajouteSingularite(sing);
    return sing;
  }
  public MetierSeuilTarageAval creeSeuilTarageAval() {
    MetierSeuilTarageAval sing=
      new MetierSeuilTarageAval();
    ajouteSingularite(sing);
    return sing;
  }
  public MetierSeuilLimniAmont creeSeuilLimniAmont() {
    MetierSeuilLimniAmont sing=
      new MetierSeuilLimniAmont();
    ajouteSingularite(sing);
    return sing;
  }
  public void supprimeSingularite(MetierSingularite sing) {
    if (singularites_ == null)
      return;
    Vector newsings= new Vector();
    for (int i= 0; i < singularites_.length; i++) {
      if (singularites_[i] == sing) {
        MetierSingularite.supprimeSingularite(singularites_[i]);
      } else
        newsings.add(singularites_[i]);
    }
    singularites_= new MetierSingularite[newsings.size()];
    for (int i= 0; i < singularites_.length; i++)
      singularites_[i]= (MetierSingularite)newsings.get(i);
  }
  public void ajouteSingularite(MetierSingularite sing) {
    if (sing == null)
      return;
    List listSing = new ArrayList(Arrays.asList(singularites_));
    if (listSing.contains(sing))
      return;
    listSing.add(sing);
    singularites_= (MetierSingularite[])listSing.toArray(new MetierSingularite[0]);
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "singularites");
  }
  public MetierProfil dupliqueProfil(MetierProfil _profil, int nouvPos) {
    MetierProfil profil= (MetierProfil)_profil.creeClone();
    MetierProfil[] profils= new MetierProfil[profils_.length + 1];
    for (int i= 0; i < nouvPos; i++)
      profils[i]= profils_[i];
    profils[nouvPos]= profil;
    for (int i= nouvPos + 1; i < profils.length; i++)
      profils[i]= profils_[i - 1];
    profils(profils);
    return profil;
  }
  public MetierProfil creeProfil() {
    MetierProfil profil= new MetierProfil();
    MetierProfil profils[]= new MetierProfil[profils_.length + 1];
    //DEBUG: changement pour ajouter le nouveau profil au debut.
/*    for (int i= 0; i < profils_.length; i++)
      profils[i + 1]= profils_[i];
    profils[0]= profil;*/
    for (int i=0; i<profils_.length; i++)
      profils[i]=profils_[i];
    profils[profils.length-1]=profil;
    // profils(profils);
    //trie non obligatoire
    //L'abscisse de cahque profil peut etre modifie sans que le bief soit
    // informe
    profils(profils, false);
    return profil;
  }
  public void supprimeProfils(MetierProfil[] profils) {
    if (profils_ == null)
      return;
    Vector newprofs= new Vector();

    boolean profilTrouvee =false;
    for (int i= 0; i < profils_.length; i++) {
      for (int j= 0; j < profils.length; j++) {
        if (profils_[i] == profils[j]) {
            profils_[i].supprime();
          profilTrouvee = true;
        }
        if (profilTrouvee) break;
      }
      if (!profilTrouvee) newprofs.add(profils_[i]);
      profilTrouvee = false;
   }

    MetierProfil[] nprofils= new MetierProfil[newprofs.size()];
    for (int i= 0; i < nprofils.length; i++)
      nprofils[i]= (MetierProfil)newprofs.get(i);
    profils(nprofils);
  }
  public void miseAZeroProfils(MetierProfil[] profils) {
    if (profils == null)
      return;
    for (int i= 0; i < profils.length; i++)
      profils[i].abscisse(0.);
  }
  public void decalageAbscisseProfils(MetierProfil[] profils, double offset) {
    if (profils == null)
      return;
    for (int i= 0; i < profils.length; i++)
      profils[i].abscisse(profils[i].abscisse() + offset);
  }
  public boolean normaliseProfils(MetierProfil[] profils) {
    if (profils == null)
      return false;
    boolean res= false;
    System.err.println("normalisation des profils");
    for (int i= 0; i < profils.length; i++) {
      MetierPoint2D[] points= profils[i].points();
      if (points.length > 0) {
        if (points[0].y < points[points.length - 1].y) {
          MetierPoint2D[] tmpPoints= new MetierPoint2D[points.length + 1];
          tmpPoints[0]= new MetierPoint2D();
          tmpPoints[0].y= points[points.length - 1].y;
          tmpPoints[0].x= points[0].x;
          for (int j= 0; j < points.length; j++) {
            tmpPoints[j + 1]= new MetierPoint2D();
            tmpPoints[j + 1].y= points[j].y;
            tmpPoints[j + 1].x= points[j].x;
          }
          profils[i].points(tmpPoints);
          res= res || true;
        } else if (points[0].y > points[points.length - 1].y) {
          MetierPoint2D[] tmpPoints= new MetierPoint2D[points.length + 1];
          for (int j= 0; j < points.length; j++) {
            tmpPoints[j].y= points[j].y;
            tmpPoints[j].x= points[j].x;
          }
          tmpPoints[points.length].y= points[0].y;
          tmpPoints[points.length].x= points[points.length - 1].x;
          profils[i].points(tmpPoints);
          res= res || true;
        }
      }
    }
    return res;
  }
  public void supprimeLimite(MetierLimite limite) {
    if (extrAmont_.conditionLimite() == limite) {
      extrAmont_.conditionLimite(null);
      limite.supprime();
    }
  }
  public MetierZoneFrottement creeZoneFrottement() {
    MetierZoneFrottement zone= new MetierZoneFrottement();
    if (extrAmont().profilRattache() != null) {
      zone.abscisseDebut( extrAmont().profilRattache().abscisse());
    }
    if (extrAval().profilRattache() != null) {
      zone.abscisseFin(extrAval().profilRattache().abscisse());
    }
    zone.biefRattache(this);
    MetierZoneFrottement zones[]= new MetierZoneFrottement[zonesFrottement_.length + 1];
    for (int i= 0; i < zonesFrottement_.length; i++)
      zones[i]= zonesFrottement_[i];
    zones[zones.length - 1]= zone;
    zonesFrottement(zones);
    return zone;
  }
  public void supprimeZoneFrottement(MetierZoneFrottement zone) {
    if (zonesFrottement_ == null)
      return;
    Vector newfrots= new Vector();
    for (int i= 0; i < zonesFrottement_.length; i++) {
      if (zonesFrottement_[i] == zone) {
        zonesFrottement_[i].supprime();
      } else
        newfrots.add(zonesFrottement_[i]);
    }
    MetierZoneFrottement[] zonesFrottement= new MetierZoneFrottement[newfrots.size()];
    for (int i= 0; i < zonesFrottement.length; i++)
      zonesFrottement[i]= (MetierZoneFrottement)newfrots.get(i);
    zonesFrottement(zonesFrottement);
  }
  public MetierZonePlanimetrage creeZonePlanimetrage() {
    MetierZonePlanimetrage zone=
      new MetierZonePlanimetrage();
    if (extrAmont() != null) {
      if (extrAmont().profilRattache() != null) {
        zone.abscisseDebut(extrAmont().profilRattache().abscisse());
      }
    }
    if (extrAval() != null) {
      if (extrAval().profilRattache() != null) {
        zone.abscisseFin(extrAval().profilRattache().abscisse());
      }
    }
    zone.biefRattache(this);
    MetierZonePlanimetrage zones[]=
      new MetierZonePlanimetrage[zonesPlanimetrage_.length + 1];
    for (int i= 0; i < zonesPlanimetrage_.length; i++)
      zones[i]= zonesPlanimetrage_[i];
    zones[zonesPlanimetrage_.length]= zone;
    zonesPlanimetrage(zones);
    return zone;
  }
  public void supprimeZonePlanimetrage(MetierZonePlanimetrage zone) {
    if (zonesPlanimetrage_ == null)
      return;
    Vector newplani= new Vector();
    for (int i= 0; i < zonesPlanimetrage_.length; i++) {
      if (zonesPlanimetrage_[i] == zone) {
        zonesPlanimetrage_[i].supprime();
      } else
        newplani.add(zonesPlanimetrage_[i]);
    }
    MetierZonePlanimetrage[] zonesPlanimetrage=
      new MetierZonePlanimetrage[newplani.size()];
    for (int i= 0; i < zonesPlanimetrage.length; i++)
      zonesPlanimetrage[i]= (MetierZonePlanimetrage)newplani.get(i);
    zonesPlanimetrage(zonesPlanimetrage);
  }
  public double getXOrigine() {
    double res= Double.MAX_VALUE;
    for (int i= 0; i < profils_.length; i++) {
      double rb= profils_[i].abscisse();
      if (rb < res)
        res= rb;
    }
    return res;
  }
  public double getXFin() {
    double res= Double.NEGATIVE_INFINITY;
    for (int i= 0; i < profils_.length; i++) {
      double rb= profils_[i].abscisse();
      if (rb > res)
        res= rb;
    }
    return res;
  }
  public MetierSeuil[] seuils() {
    if (singularites_ == null)
      return new MetierSeuil[0];
    Vector seuils= new Vector();
    for (int i= 0; i < singularites_.length; i++) {
      MetierSingularite s= singularites_[i];
      if (s instanceof MetierSeuil)
        seuils.addElement(s);
    }
    MetierSeuil[] res= new MetierSeuil[seuils.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (MetierSeuil)seuils.get(i);
    return res;
  }
  public MetierPerteCharge[] pertesCharges() {
    if (singularites_ == null)
      return new MetierPerteCharge[0];
    Vector pertesCharges= new Vector();
    for (int i= 0; i < singularites_.length; i++) {
      MetierSingularite s= singularites_[i];
      if (s instanceof MetierPerteCharge)
        pertesCharges.addElement(s);
    }
    MetierPerteCharge[] res= new MetierPerteCharge[pertesCharges.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (MetierPerteCharge)pertesCharges.get(i);
    return res;
  }
  public MetierApport[] apports() {
    if (singularites_ == null)
      return new MetierApport[0];
    Vector apports= new Vector();
    for (int i= 0; i < singularites_.length; i++) {
      MetierSingularite s= singularites_[i];
      if (s instanceof MetierApport)
        apports.addElement(s);
    }
    MetierApport[] res= new MetierApport[apports.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (MetierApport)apports.get(i);
    return res;
  }
  public MetierSource[] sources() {
  if (singularites_ == null)
    return new MetierSource[0];
  Vector sources= new Vector();
  for (int i= 0; i < singularites_.length; i++) {
    MetierSingularite s= singularites_[i];
    if (s instanceof MetierSource)
      sources.addElement(s);
  }
  MetierSource[] res= new MetierSource[sources.size()];
  for (int i= 0; i < res.length; i++)
    res[i]= (MetierSource)sources.get(i);
  return res;
}

  public MetierDeversoir[] deversoirs() {
    if (singularites_ == null)
      return new MetierDeversoir[0];
    Vector deversoirs= new Vector();
    for (int i= 0; i < singularites_.length; i++) {
      MetierSingularite s= singularites_[i];
      if (s instanceof MetierDeversoir)
        deversoirs.addElement(s);
    }
    MetierDeversoir[] res= new MetierDeversoir[deversoirs.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (MetierDeversoir)deversoirs.get(i);
    return res;
  }
  public MetierDeversoirComportementLoi transformeDeversoirZQ2Loi(MetierDeversoirComportementZCoefQ aRemplacer) {
    int num= aRemplacer.numero();
    String nom= aRemplacer.nom();
    double abs= aRemplacer.abscisse();
    double lg= aRemplacer.longueur();
    int i;
    for (i= 0; i < singularites_.length; i++) {
      if (singularites_[i].id() == aRemplacer.id()) {
        aRemplacer.supprime();
        MetierDeversoirComportementLoi deversoirNew=
          new MetierDeversoirComportementLoi();
        deversoirNew.numero(num);
        deversoirNew.nom(nom);
        deversoirNew.abscisse(abs);
        deversoirNew.longueur(lg);
        singularites_[i]= deversoirNew;
        break;
      }
    }
    return (MetierDeversoirComportementLoi)singularites_[i];
  }
  public MetierDeversoirComportementZCoefQ transformeDeversoirLoi2ZQ(MetierDeversoirComportementLoi aRemplacer) {
    int num= aRemplacer.numero();
    String nom= aRemplacer.nom();
    double abs= aRemplacer.abscisse();
    double lg= aRemplacer.longueur();
    int i;
    for (i= 0; i < singularites_.length; i++) {
      if (singularites_[i].equals(aRemplacer)) {
        aRemplacer.supprime();
        MetierDeversoirComportementZCoefQ deversoirNew=
          new MetierDeversoirComportementZCoefQ();
        deversoirNew.numero(num);
        deversoirNew.nom(nom);
        deversoirNew.abscisse(abs);
        deversoirNew.longueur(lg);
        singularites_[i]= deversoirNew;
        break;
      }
    }
    return (MetierDeversoirComportementZCoefQ)singularites_[i];
  }
  public MetierBarrage transformeSeuilLoi2Barrage(MetierSeuilLoi aRemplacer) {
    int num= aRemplacer.numero();
    String nom= aRemplacer.nom();
    double abs= aRemplacer.abscisse();
    double zRupture= aRemplacer.coteRupture();
    double zCrete= aRemplacer.coteCrete();
    int i;
    for (i= 0; i < singularites_.length; i++) {
      if (singularites_[i].equals(aRemplacer)) {
        aRemplacer.supprime();
        MetierBarrage seuilNew= new MetierBarrage();
        seuilNew.numero(num);
        seuilNew.nom(nom);
        seuilNew.abscisse(abs);
        seuilNew.coteRupture(zRupture);
        seuilNew.coteCrete(zCrete);
        singularites_[i]= seuilNew;
        break;
      }
    }
    return (MetierBarrage)singularites_[i];
  }
  public MetierSeuilLoi transformeBarrage2SeuilLoi(MetierBarrage aRemplacer) {
    int num= aRemplacer.numero();
    String nom= aRemplacer.nom();
    double abs= aRemplacer.abscisse();
    double zRupture= aRemplacer.coteRupture();
    double zCrete= aRemplacer.coteCrete();
    int i;
    for (i= 0; i < singularites_.length; i++) {
      if (singularites_[i].equals(aRemplacer)) {
        aRemplacer.supprime();
        MetierSeuilLoi seuilNew= new MetierSeuilLoi();
        seuilNew.numero(num);
        seuilNew.nom(nom);
        seuilNew.abscisse(abs);
        seuilNew.coteRupture(zRupture);
        seuilNew.coteCrete(zCrete);
        singularites_[i]= seuilNew;
        break;
      }
    }
    return (MetierSeuilLoi)singularites_[i];
  }
  public MetierLimite[] limites() {
    Vector limites= new Vector();
    if (extrAmont_.conditionLimite() != null)
      limites.add(extrAmont_.conditionLimite());
    if (extrAval_.conditionLimite() != null)
      limites.add(extrAval_.conditionLimite());
    MetierLimite[] res= new MetierLimite[limites.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (MetierLimite)limites.get(i);
    return res;
  }
  public MetierProfil[] profilsAvecZonesStockage() {
    if (profils_ == null)
      return new MetierProfil[0];
    Vector profilsAStockage= new Vector();
    MetierProfil p;
    for (int i= 0; i < profils_.length; i++) {
      p= profils_[i];
      if (p.hasZoneStockage())
        profilsAStockage.addElement(p);
    }
    MetierProfil[] res= new MetierProfil[profilsAStockage.size()];
    for (int i= 0; i < res.length; i++)
      res[i]= (MetierProfil)profilsAStockage.get(i);
    return res;
  }
  public int getIndiceProfilNumero(int numeroProfil) {
    for (int i= 0; i < profils_.length; i++) {
      if (profils_[i].numero() == numeroProfil)
        return i;
    }
    return -1;
  }
  public int getIndiceProfilAbscisse(double abscisseProfil) {
    for (int i= 0; i < profils_.length; i++) {
      if (CGlobal.egale(profils_[i].abscisse(), abscisseProfil))
        return i;
      else {
        if (profils_[i].abscisse() > abscisseProfil) {
          return -i;
        }
      }
    }
    return Integer.MIN_VALUE;
  }
  public boolean contientProfilNumero(int numeroProfil) {
    return getIndiceProfilNumero(numeroProfil) >= 0;
  }
  public boolean contientProfilAbscisse(double abscisseProfil) {
    return getIndiceProfilAbscisse(abscisseProfil) >= 0;
  }
  public boolean contientAbscisse(double abscisse) {
    if ((profils_ == null) || (profils_.length == 0))
      return false;
    return CGlobal.appartient(
      abscisse,
      profils_[0].abscisse(),
      profils_[profils_.length - 1].abscisse());
  }
  public boolean contientSingularite(MetierSingularite sing) {
    if (singularites_ == null)
      return false;
    for (int i= 0; i < singularites_.length; i++)
      if (singularites_[i] == sing)
        return true;
    return false;
  }
  public int getNbPasPlanimetrage() {
    return getNbPasPlanimetrage(zonesPlanimetrage_);
  }

  public int getNbPasPlanimetrage(MetierZonePlanimetrage[] zonesPlanimetrage) {
	    int nbPas= 0;
	    org.omg.CORBA.DoubleHolder coteMin= new org.omg.CORBA.DoubleHolder();
	    org.omg.CORBA.DoubleHolder coteMax= new org.omg.CORBA.DoubleHolder();
	    for (int i= 0; i < zonesPlanimetrage.length; i++) {
	      double pas= zonesPlanimetrage[i].taillePas();
	      int indDebut=
	        getIndiceProfilAbscisse(zonesPlanimetrage[i].abscisseDebut());
	      int indFin= getIndiceProfilAbscisse(zonesPlanimetrage[i].abscisseFin());
	      if (indDebut < 0) indDebut = -indDebut;
	      if (indFin < 0) indFin = -indFin;
	      for (int j= indDebut; j <= indFin; j++) {
	        profils_[j].coteMinMax(coteMin, coteMax);
	        double h= coteMax.value - coteMin.value;
	        nbPas= Math.max((int) (h / pas) + 1, nbPas);
	      }
	    }
	    return nbPas;
	  }
  public double interpoleFond(double abscisse) {
    double res= Double.POSITIVE_INFINITY;
    MetierProfil p1= null;
    MetierProfil p2= null;
    for (int i= 0; i < profils_.length; i++) {
      if (profils_[i].abscisse() >= abscisse) {
        if (i > 0)
          p1= profils_[i - 1];
        p2= profils_[i];
        break;
      }
    }
    if ((p1 != null) && (p2 != null)) {
      double n1= p1.getFond();
      double n2= p2.getFond();
      res=
        n2
          + (n1 - n2)
            * (p2.abscisse() - abscisse)
            / (p2.abscisse() - p1.abscisse());
    }
    return res;
  }

  public double getLongueur() {
    if (profils_ == null) return 0;
    if (profils_.length == 0) return 0;
    return getXFin() - getXOrigine();
  }

  private void trieProfils() {
    if (profils_ == null)
      return;
    Arrays.sort(profils_, MetierProfil.COMPARATOR);
  }
  private void miseAJourExtremite() {
    if (profils_ == null) return;
    if (profils_.length > 0) {
      if (extrAmont_ != null)
        extrAmont_.profilRattache(profils_[0]);
      if (extrAval_ != null)
        extrAval_.profilRattache(profils_[profils_.length - 1]);
    }
  }
  private void trieZonesFrottement() {
    if (zonesFrottement_ == null)
      return;
    Arrays.sort(zonesFrottement_, MetierZone.COMPARATOR);
  }
  private void trieZonesPlanimetrage() {
    if (zonesPlanimetrage_ == null)
      return;
    Arrays.sort(zonesPlanimetrage_, MetierZone.COMPARATOR);
  }
}
class BiefComparator implements Comparator {
  BiefComparator() {}
  @Override
  public int compare(Object o1, Object o2) {
    MetierProfil p1= ((MetierBief)o1).extrAmont().profilRattache();
    MetierProfil p2= ((MetierBief)o2).extrAmont().profilRattache();
    double xo1= (p1 == null ? Double.MAX_VALUE : p1.abscisse());
    double xo2= (p2 == null ? Double.MAX_VALUE : p2.abscisse());
    return xo1 < xo2 ? -1 : xo1 == xo2 ? 0 : 1;
  }
  @Override
  public boolean equals(Object obj) {
    return false;
  }
}
