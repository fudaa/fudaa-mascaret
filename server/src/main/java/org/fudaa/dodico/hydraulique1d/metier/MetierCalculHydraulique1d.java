/*
 * @file         MetierCalculHydraulique1d.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.corba.calcul.ICalcul;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Implémentation de l'objet métier "calcul hydraulique1d".
 * Associe une étude 1D et un calcul.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:30 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierCalculHydraulique1d extends MetierHydraulique1d/*DService*/ {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierCalculHydraulique1d) {
      MetierCalculHydraulique1d q= (MetierCalculHydraulique1d)_o;
      calculCode((ICalcul)q.calculCode().creeClone());
      etude((MetierEtude1d)q.etude().creeClone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierCalculHydraulique1d p=
      new MetierCalculHydraulique1d();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "calculHydraulique1d";
    return s;
  }
  // IService
/*  public String description() {
    return "Hydraulique1d, serveur de calcul metier: " + super.description();
  }*/
  /*** MetierCalculHydraulique1d ***/
  // constructeurs
  public MetierCalculHydraulique1d() {
    super();
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    super.dispose();
  }
  // Attributs
  private ICalcul calculCode_;
  public ICalcul calculCode() {
    return calculCode_;
  }
  public void calculCode(ICalcul s) {
    if(calculCode_==s) return;
    calculCode_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "calculCode");
  }
  private MetierEtude1d etude_;
  public MetierEtude1d etude() {
    return etude_;
  }
  public void etude(MetierEtude1d s) {
    if(etude_==s) return;
    etude_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "etude");
  }
}
