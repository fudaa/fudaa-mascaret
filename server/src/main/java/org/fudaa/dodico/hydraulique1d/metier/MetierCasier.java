/*
 * @file         MetierCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierGeometrieCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierNuagePointsCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierPlanimetrageCasier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
/**
 * Impl�mentation de l'objet m�tier "casier".
 * Contient une cote, un nom et la g�om�trie du casier.
 * Une loi de type hydrogramme peut y �tre rattach�.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:29 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierCasier extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierCasier) {
      MetierCasier q= (MetierCasier)_o;
      if (q.geometrie() != null)
        geometrie((MetierGeometrieCasier)q.geometrie().creeClone());
      loiRattachee((MetierLoiHydrogramme)q.loiRattachee());
      if (q.nom() != null) {
        nom(new String(q.nom()));
      }
      coteInitiale(q.coteInitiale());
      numero(q.numero());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierCasier p= new MetierCasier();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    return nom_;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Casier")+numero_+" '" + nom_ + "'";
    res[1]= "";
    res[1] += getS("Zinit")+" : " + coteInitiale_;
    if (geometrie_ != null) {
      res[1] += " " + geometrie_.getInfos()[0];
    }
    if (loiRattachee_ != null) {
      res[1] += " "+getS("loi")+" : " + loiRattachee_.nom();
    } else {
      res[1] += " "+getS("sans d�bit d'apport");
    }
    return res;
  }
  /*** MetierCasier ***/
  // constructeurs
  public MetierCasier() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    nom_= null;
    coteInitiale_= 0.;
    geometrie_= new MetierNuagePointsCasier();
    loiRattachee_= null;
    numero_= 0;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    id_= 0;
    nom_= null;
    coteInitiale_= 0.;
    geometrie_= null;
    loiRattachee_= null;
    numero_= 0;
    super.dispose();
  }
  // attributs
  private int id_;
  private String nom_;
  public String nom() {
    return nom_;
  }
  public void nom(String s) {
    if (nom_ != null) {
      if (nom_.equals(s))
        return;
    }
    nom_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nom");
  }
  private double coteInitiale_;
  public double coteInitiale() {
    return coteInitiale_;
  }
  public void coteInitiale(double s) {
    if (coteInitiale_==s) return;
    coteInitiale_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteInitiale");
  }
  private MetierLoiHydrogramme loiRattachee_;
  public MetierLoiHydrogramme loiRattachee() {
    return loiRattachee_;
  }
  public void loiRattachee(MetierLoiHydrogramme s) {
    if (loiRattachee_==s) return;
    loiRattachee_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loiRattachee");
  }
  private MetierGeometrieCasier geometrie_;
  public MetierGeometrieCasier geometrie() {
    return geometrie_;
  }
  public void geometrie(MetierGeometrieCasier s) {
    if (geometrie_==s) return;
    geometrie_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "geometrie");
  }
  private int numero_;
  public int numero() {
    return numero_;
  }
  public void numero(int s) {
    if (numero_ == s)
      return;
    if (nom_ == null)
      nom("casier"+s);
    else if (nom_.equalsIgnoreCase("casier"+numero_))
      nom("casier"+s);
    numero_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numero");
  }
  // les m�thodes
  public boolean isNuagePoints() {
    return geometrie_.isNuagePoints();
  }
  public void toNuagePoints() {
    if (isNuagePoints())
      return;
    MetierGeometrieCasier g=
      new MetierNuagePointsCasier();
    geometrie(g);
  }
  public boolean isPlanimetrage() {
    return geometrie_.isPlanimetrage();
  }
  public void toPlanimetrage() {
    if (isPlanimetrage())
      return;
    MetierGeometrieCasier g=
      new MetierPlanimetrageCasier();
    geometrie(g);
  }
}
