/**
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Vector;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier "conditions initiales".
 * Peut �tre compos� d'une ligne d'eau initiale.
*  En noyau transcritique, elle peut �tre compos� d'une reprise de calcul ou d'une zone s�che.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:25 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierConditionsInitiales extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierConditionsInitiales) {
      MetierConditionsInitiales c= (MetierConditionsInitiales)_o;
      paramsReprise((MetierParametresReprise)c.paramsReprise().creeClone());
      ligneEauInitiale((MetierLigneEauInitiale)c.ligneEauInitiale().creeClone());

      if (c.zonesSeches() != null) {
        MetierZone[] izs= new MetierZone[c.zonesSeches().length];
        for (int i= 0; i < izs.length; i++)
          izs[i]= (MetierZone)c.zonesSeches()[i].creeClone();
        zonesSeches(izs);
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierConditionsInitiales c=
      new MetierConditionsInitiales();
    c.initialise(this);
    return c;
  }
  @Override
  final public String toString() {
    String s= "conditionsInitiales";
    return s;
  }
  /*** MetierConditionsInitiales ***/
  // constructeurs
  public MetierConditionsInitiales() {
    super();
    paramsReprise_= null;
    ligneEauInitiale_=null;
    zonesSeches_= new MetierZone[0];
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    paramsReprise_= null;
    ligneEauInitiale_= null;
    zonesSeches_= null;
    super.dispose();
  }
  // attributs
  private MetierParametresReprise paramsReprise_;
  public MetierParametresReprise paramsReprise() {
    return paramsReprise_;
  }
  public void paramsReprise(MetierParametresReprise paramsReprise) {
    if (paramsReprise_==paramsReprise) return;
    paramsReprise_= paramsReprise;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "paramsReprise");
  }
  private MetierLigneEauInitiale ligneEauInitiale_;
  public MetierLigneEauInitiale ligneEauInitiale() {
    return ligneEauInitiale_;
  }
  /**
   * @param ligneEauInitiale Null : la ligne d'eau initiale n'est pas consideree dans le calcul.
   */
  public void ligneEauInitiale(MetierLigneEauInitiale ligneEauInitiale) {
    if (ligneEauInitiale_==ligneEauInitiale) return;
    ligneEauInitiale_= ligneEauInitiale;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "ligneEauInitiale");
  }
  private MetierZone[] zonesSeches_;
  public MetierZone[] zonesSeches() {
    return zonesSeches_;
  }
  public void zonesSeches(MetierZone[] zonesSeches) {
    if (egale(zonesSeches_, zonesSeches)) return;
    zonesSeches_= zonesSeches;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zonesSeches");
  }
  private final static boolean egale(MetierZone[] p, MetierZone[] p2) {
    if (p == p2)
      return true;
    if (p == null || p2 == null)
      return false;

    int length = p.length;
    if (p2.length != length)
      return false;

    for (int i = 0; i < length; i++)
      if (p[i] != p2[i])
        return false;

    return true;
  }



  // methodes
  public void creeParamsReprise() {
    paramsReprise_= new MetierParametresReprise();
  }
  public void creeLigneInitiale() {
    ligneEauInitiale_= new MetierLigneEauInitiale();
  }
  public MetierZone creeZoneSeche() {
    MetierZone zone= new MetierZone();
    MetierZone zones[]= new MetierZone[zonesSeches_.length + 1];
    for (int i= 0; i < zonesSeches_.length; i++)
      zones[i]= zonesSeches_[i];
    zones[zones.length - 1]= zone;
    zonesSeches(zones);
    return zone;
  }
  private void supprimeZoneSeche(MetierZone zone) {
    Vector newZones= new Vector(zonesSeches_.length - 1);
    for (int i= 0; i < zonesSeches_.length; i++) {
      if (zonesSeches_[i] == zone) {
        zonesSeches_[i].supprime();
      } else
        newZones.add(zonesSeches_[i]);
    }
    MetierZone[] zonesSeches= new MetierZone[newZones.size()];
    for (int i= 0; i < zonesSeches.length; i++) {
      zonesSeches[i]= (MetierZone)newZones.get(i);
    }
    zonesSeches(zonesSeches);
  }
  public void supprimeZonesSeches(MetierZone[] zones) {
    //System.out.println("supprimeZonesSeches(MetierZone[] zones) zones.length="+zones.length);
    for (int i= 0; i < zones.length; i++) {
      supprimeZoneSeche(zones[i]);
    }
  }
  public void supprimeZonesSechesAvecBief(MetierBief bief) {
    if (zonesSeches_ == null) return;

    Vector newZones= new Vector();
    for (int i= 0; i < zonesSeches_.length; i++) {
      if (zonesSeches_[i].biefRattache() == bief) {
        zonesSeches_[i].supprime();
      } else
        newZones.add(zonesSeches_[i]);
    }

    if (newZones.size() == zonesSeches_.length)
      // aucune  suppression n'a �t� faite.
      return;

    MetierZone[] zonesSeches= new MetierZone[newZones.size()];
    for (int i= 0; i < zonesSeches.length; i++) {
      zonesSeches[i]= (MetierZone)newZones.get(i);
    }
    zonesSeches(zonesSeches);
  }


}
