/**
 * @file         MetierDefinitionSections.java
 * @creation     2001-07-17
 * @modification $Date: 2007-11-20 11:42:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
/**
 * Implémentation de l'objet métier "définitions des sections de calculs".
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:31 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public abstract class MetierDefinitionSections
  extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
  }
  @Override
  public String toString() {
    String s= "definitionSections ?";
    return s;
  }
  // constructeurs
  public MetierDefinitionSections() {
    super();
  }
  @Override
  public void dispose() {
    super.dispose();
  }

  public abstract void supprimeSectionMaillageAvecBief(MetierBief bief);
}
