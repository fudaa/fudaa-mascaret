/**
 * @file         MetierDefinitionSectionsParSections.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Vector;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier "d�finitions des sections de calculs sections par sections".
 * Contient un tableau de sites (association une abscisse et d'un bief).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:31 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierDefinitionSectionsParSections extends MetierDefinitionSections {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierDefinitionSectionsParSections) {
      MetierDefinitionSectionsParSections q= (MetierDefinitionSectionsParSections)_o;
      if (q.unitaires() != null) {
        MetierSite[] iu= new MetierSite[q.unitaires().length];
        for (int i= 0; i < iu.length; i++)
          iu[i]= (MetierSite)q.unitaires()[i].creeClone();
        unitaires(iu);
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierDefinitionSectionsParSections p=
      new MetierDefinitionSectionsParSections();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionsSections";
    return s;
  }
  /*** MetierDefinitionSectionsParSections ***/
  // constructeurs
  public MetierDefinitionSectionsParSections() {
    super();
    unitaires_= new MetierSite[0];
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    unitaires_= null;
    super.dispose();
  }
  // Attributs
  private MetierSite[] unitaires_;
  public MetierSite[] unitaires() {
    return unitaires_;
  }
  public void unitaires(MetierSite[] s) {
    if (egale(unitaires_, s)) return;
    unitaires_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "unitaires");
  }
  // methodes
  public MetierSite creeSection(int indice) {
    MetierSite unitaire= new MetierSite();
    MetierSite[] us= new MetierSite[unitaires_.length + 1];
    for (int i= 0; i < indice; i++)
      us[i]= unitaires_[i];
    us[indice]= unitaire;
    for (int i= indice + 1; i < us.length; i++)
      us[i]= unitaires_[i - 1];
    unitaires(us);
    return unitaire;
  }
  public void creeSectionALaFin(MetierBief biefRattache) {
    MetierSite unitaire= new MetierSite();
    unitaire.biefRattache(biefRattache);
    MetierSite[] us= new MetierSite[unitaires_.length + 1];
    for (int i= 0; i < unitaires_.length; i++)
      us[i]= unitaires_[i];
    us[unitaires_.length]= unitaire;
    unitaires(us);
  }
  public void supprimeSections(MetierSite[] sections) {
    Vector newus= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      boolean trouve= false;
      for (int j= 0; j < sections.length; j++) {
        if (unitaires_[i] == sections[j])
          trouve= true;
      }
      if (!trouve)
        newus.add(unitaires_[i]);
    }
    MetierSite[] us= new MetierSite[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (MetierSite)newus.get(i);
    unitaires(us);
  }
  @Override
  public void supprimeSectionMaillageAvecBief(MetierBief bief) {
    if (unitaires_ == null) return;
    Vector newus= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      if (unitaires_[i].biefRattache() == bief) {
        unitaires_[i].supprime();
      } else {
        newus.add(unitaires_[i]);
      }
    }

    if (newus.size() == unitaires_.length)
      // aucune suppression n'a �t� faite.
      return;

    MetierSite[] us= new MetierSite[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (MetierSite)newus.get(i);
    unitaires(us);
  }
}
