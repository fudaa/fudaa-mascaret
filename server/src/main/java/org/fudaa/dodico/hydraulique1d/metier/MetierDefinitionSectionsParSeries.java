/**
 * @file         MetierDefinitionSectionsParSeries.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Vector;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier "d�finitions des sections de calculs par s�ries de zones".
 * Contient un tableau d'�l�ment associant une zone (un bief et 2 abscisses) et un pas d'espace.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:29 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierDefinitionSectionsParSeries extends MetierDefinitionSections {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierDefinitionSectionsParSeries) {
      MetierDefinitionSectionsParSeries q= (MetierDefinitionSectionsParSeries)_o;
      surProfils(q.surProfils());
      if (q.unitaires() != null) {
        MetierDefinitionSectionsParSeriesUnitaire[] iu=
          new MetierDefinitionSectionsParSeriesUnitaire[q.unitaires().length];
        for (int i= 0; i < iu.length; i++)
          iu[i]=
            (MetierDefinitionSectionsParSeriesUnitaire)q.unitaires()[i].creeClone();
        unitaires(iu);
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierDefinitionSectionsParSeries p=
      new MetierDefinitionSectionsParSeries();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionsSeries";
    return s;
  }
  /*** MetierDefinitionSectionsParSeries ***/
  // constructeurs
  public MetierDefinitionSectionsParSeries() {
    super();
    surProfils_= true;
    unitaires_= new MetierDefinitionSectionsParSeriesUnitaire[0];
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    surProfils_= true;
    unitaires_= null;
    super.dispose();
  }
  // Attributs
  private boolean surProfils_;
  public boolean surProfils() {
    return surProfils_;
  }
  public void surProfils(boolean s) {
    if (surProfils_==s) return;
    surProfils_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "surProfils");
  }
  private MetierDefinitionSectionsParSeriesUnitaire[] unitaires_;
  public MetierDefinitionSectionsParSeriesUnitaire[] unitaires() {
    return unitaires_;
  }
  public void unitaires(MetierDefinitionSectionsParSeriesUnitaire[] s) {
    if (egale(unitaires_, s)) return;
    unitaires_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "unitaires");
  }
  // methodes
  public MetierDefinitionSectionsParSeriesUnitaire creeSerie(int indice) {
    MetierDefinitionSectionsParSeriesUnitaire unitaire=
      new MetierDefinitionSectionsParSeriesUnitaire();
    MetierDefinitionSectionsParSeriesUnitaire[] us=
      new MetierDefinitionSectionsParSeriesUnitaire[unitaires_.length + 1];
    for (int i= 0; i < indice; i++)
      us[i]= unitaires_[i];
    us[indice]= unitaire;
    for (int i= indice + 1; i < us.length; i++)
      us[i]= unitaires_[i - 1];
    unitaires(us);
    return unitaire;
  }
  public void creeSerieALaFin(MetierBief biefRattache) {
    MetierDefinitionSectionsParSeriesUnitaire unitaire=
      new MetierDefinitionSectionsParSeriesUnitaire();
    unitaire.zone().biefRattache(biefRattache);
    if ((biefRattache.extrAmont() != null)
      && (biefRattache.extrAmont().profilRattache() != null)) {
      unitaire.zone().abscisseDebut(
        biefRattache.extrAmont().profilRattache().abscisse());
    }
    if ((biefRattache.extrAval() != null)
      && (biefRattache.extrAval().profilRattache() != null)) {
      unitaire.zone().abscisseFin(
        biefRattache.extrAval().profilRattache().abscisse());
    }
    MetierDefinitionSectionsParSeriesUnitaire[] us=
      new MetierDefinitionSectionsParSeriesUnitaire[unitaires_.length + 1];
    for (int i= 0; i < unitaires_.length; i++)
      us[i]= unitaires_[i];
    us[unitaires_.length]= unitaire;
    unitaires(us);
  }
  public void supprimeSeries(MetierDefinitionSectionsParSeriesUnitaire[] series) {
    Vector newus= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      boolean trouve= false;
      for (int j= 0; j < series.length; j++) {
        if (unitaires_[i] == series[j])
          trouve= true;
      }
      if (!trouve)
        newus.add(unitaires_[i]);
    }
    MetierDefinitionSectionsParSeriesUnitaire[] us=
      new MetierDefinitionSectionsParSeriesUnitaire[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (MetierDefinitionSectionsParSeriesUnitaire)newus.get(i);
    unitaires(us);
  }
  @Override
  public void supprimeSectionMaillageAvecBief(MetierBief bief) {
    if (unitaires_ == null) return;

    Vector newus= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      if (unitaires_[i].zone() != null) {
        if (unitaires_[i].zone().biefRattache() == bief) {
          unitaires_[i].zone().supprime();
          unitaires_[i].supprime();
        }
        else {
          newus.add(unitaires_[i]);
        }
      }
    }
    MetierDefinitionSectionsParSeriesUnitaire[] us=
      new MetierDefinitionSectionsParSeriesUnitaire[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (MetierDefinitionSectionsParSeriesUnitaire)newus.get(i);
    unitaires(us);
  }
  public MetierDefinitionSectionsParSeriesUnitaire[] verifieContraintesProfil(MetierReseau reseau) {
    MetierDefinitionSectionsParSeriesUnitaire[] errs=
      new MetierDefinitionSectionsParSeriesUnitaire[0];
    if ((unitaires_ == null))
      return errs;
    Vector erreurs= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      MetierProfil b1=
        reseau.getProfilAbscisse(unitaires_[i].zone().abscisseDebut());
      MetierProfil b2= reseau.getProfilAbscisse(unitaires_[i].zone().abscisseFin());
      if (b1 == b2)
        erreurs.add(unitaires_[i]);
    }
    errs= new MetierDefinitionSectionsParSeriesUnitaire[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++)
      errs[i]= (MetierDefinitionSectionsParSeriesUnitaire)erreurs.get(i);
    return errs;
  }
  public MetierDefinitionSectionsParSeriesUnitaire[] verifieContraintesBief(MetierReseau reseau) {
    MetierDefinitionSectionsParSeriesUnitaire[] errs=
      new MetierDefinitionSectionsParSeriesUnitaire[0];
    if ((unitaires_ == null))
      return errs;
    Vector erreurs= new Vector();
    for (int i= 0; i < unitaires_.length; i++) {
      MetierBief b1=
        reseau.getBiefContenantAbscisse(unitaires_[i].zone().abscisseDebut());
      MetierBief b2=
        reseau.getBiefContenantAbscisse(unitaires_[i].zone().abscisseFin());
      if (b1 != b2)
        erreurs.add(unitaires_[i]);
    }
    errs= new MetierDefinitionSectionsParSeriesUnitaire[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++)
      errs[i]= (MetierDefinitionSectionsParSeriesUnitaire)erreurs.get(i);
    return errs;
  }
  public MetierDefinitionSectionsParSeriesUnitaire[] verifieContraintesChevauchement(MetierReseau reseau) {
    MetierDefinitionSectionsParSeriesUnitaire[] errs=
      new MetierDefinitionSectionsParSeriesUnitaire[0];
    if ((unitaires_ == null))
      return errs;
    Vector erreurs= new Vector();
    MetierProfil bmax= null;
    for (int i= 0; i < unitaires_.length; i++) {
      MetierProfil b1=
        reseau.getProfilAbscisse(unitaires_[i].zone().abscisseDebut());
      MetierProfil b2= reseau.getProfilAbscisse(unitaires_[i].zone().abscisseFin());
      if ((bmax != null) && (b1.abscisse() < bmax.abscisse())) {
        erreurs.add(unitaires_[i - 1]);
        erreurs.add(unitaires_[i]);
      }
      bmax= b2;
    }
    errs= new MetierDefinitionSectionsParSeriesUnitaire[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++)
      errs[i]= (MetierDefinitionSectionsParSeriesUnitaire)erreurs.get(i);
    return errs;
  }
  public void remplitAvecProfils(MetierReseau reseau) {
    if (unitaires_ == null)
      return;
    MetierDefinitionSectionsParSeriesUnitaire[] nouvu= null;
    Vector vu= new Vector();
    MetierBief[] biefs= reseau.biefs();
    int nb= 0;
    supprimeSeries(unitaires_);
    for (int b= 0; b < biefs.length; b++) {
      MetierProfil[] profils= biefs[b].profils();
      for (int i= 0; i < profils.length - 1; i++) {
        MetierDefinitionSectionsParSeriesUnitaire u= creeSerie(nb++);
        MetierZone zone= u.zone();
        zone.abscisseDebut(profils[i].abscisse());
        zone.abscisseFin(profils[i + 1].abscisse());
        u.pas(zone.abscisseFin() - zone.abscisseDebut());
        vu.add(u);
      }
    }
    nouvu= new MetierDefinitionSectionsParSeriesUnitaire[vu.size()];
    for (int i= 0; i < nouvu.length; i++) {
      nouvu[i]= (MetierDefinitionSectionsParSeriesUnitaire)vu.get(i);
    }
    unitaires(nouvu);
  }
}
