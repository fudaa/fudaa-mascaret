/*
 * @file         MetierDefinitionSectionsParSeriesUnitaire.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier "�l�ment d'une d�finition des sections de calculs par zone".
 * Association d'une zone (un bief et 2 abscisses) et un pas d'espace.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:33 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierDefinitionSectionsParSeriesUnitaire extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierDefinitionSectionsParSeriesUnitaire) {
      MetierDefinitionSectionsParSeriesUnitaire q=
        (MetierDefinitionSectionsParSeriesUnitaire)_o;
      zone((MetierZone)q.zone().creeClone());
      pas(q.pas());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierDefinitionSectionsParSeriesUnitaire p=new MetierDefinitionSectionsParSeriesUnitaire();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionSerieUnitaire(pas " + pas_;
    if (zone_ != null)
      s += ",zone " + zone_.toString();
    s += ")";
    return s;
  }
  /*** MetierDefinitionSectionsParSeriesUnitaire ***/
  // constructeurs
  public MetierDefinitionSectionsParSeriesUnitaire() {
    super();
    zone_= new MetierZone();
    pas_= 1.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    zone_= null;
    pas_= 0.;
    super.dispose();
  }
  // Attributs
  private MetierZone zone_;
  public MetierZone zone() {
    return zone_;
  }
  public void zone(MetierZone s) {
    if (zone_==s) return;
    zone_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zone");
  }
  private double pas_;
  public double pas() {
    return pas_;
  }
  public void pas(double s) {
    if (pas_==s) return;
    pas_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pas");
  }
  // methodes
  public int nbSections() {
    double res= ((zone_.abscisseFin() - zone_.abscisseDebut()) / pas_) - 1;
    return (int)Math.round(res);
  }
  public void nbSections(int nb) {
    double pas= (zone_.abscisseFin() - zone_.abscisseDebut()) / (nb + 1);
    pas(pas);
  }
}
