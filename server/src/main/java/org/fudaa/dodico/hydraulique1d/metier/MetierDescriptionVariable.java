/**
 * @file         MetierDescriptionVariable.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:32 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier "Description d'une variable r�sultat".
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:32 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierDescriptionVariable extends MetierHydraulique1d {
  public static final MetierDescriptionVariable ZREF = new MetierDescriptionVariable("ZREF", getS("Cote du fond"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 4);
  public static final MetierDescriptionVariable RGC  = new MetierDescriptionVariable("RGC",  getS("Cote de la rive gauche"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 4);
  public static final MetierDescriptionVariable RDC  = new MetierDescriptionVariable("RDC",  getS("Cote de la rive droite"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 4);
  public static final MetierDescriptionVariable KMIN = new MetierDescriptionVariable("KMIN", getS("Coefficient de frottement mineur"), EnumMetierUnite.M1_TIERS_PAR_S, EnumMetierTypeNombre.REEL, 0);
  public static final MetierDescriptionVariable KMAJ = new MetierDescriptionVariable("KMAJ", getS("Coefficient de frottement majeur"), EnumMetierUnite.M1_TIERS_PAR_S, EnumMetierTypeNombre.REEL, 0);
  public static final MetierDescriptionVariable Z    = new MetierDescriptionVariable("Z",    getS("Cote de l'eau"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QMIN = new MetierDescriptionVariable("QMIN", getS("D�bit mineur"), EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QMAJ = new MetierDescriptionVariable("QMAJ", getS("D�bit majeur"), EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable S1   = new MetierDescriptionVariable("S1",   getS("Section mouillee mineur"), EnumMetierUnite.M2, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable S2   = new MetierDescriptionVariable("S2",   getS("Section mouillee majeur"), EnumMetierUnite.M2, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable FR   = new MetierDescriptionVariable("FR",   getS("Nombre de Froude"), EnumMetierUnite.RIEN, EnumMetierTypeNombre.REEL, 5);
  public static final MetierDescriptionVariable BETA = new MetierDescriptionVariable("BETA", getS("Coefficient beta modele DEBORD"), EnumMetierUnite.RIEN, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable B1   = new MetierDescriptionVariable("B1",   getS("Largeur au miroir mineur"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable B2   = new MetierDescriptionVariable("B2",   getS("Largeur au miroir majeur"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable BS   = new MetierDescriptionVariable("BS",   getS("Largeur au miroir du stockage"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable P1   = new MetierDescriptionVariable("P1",   getS("Perimetre mouille mineur"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable P2   = new MetierDescriptionVariable("P2",   getS("Perimetre mouille majeur"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable RH1  = new MetierDescriptionVariable("RH1",  getS("Rayon hydraulique mineur"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable RH2  = new MetierDescriptionVariable("RH2",  getS("Rayon hydraulique majeur"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable VMIN = new MetierDescriptionVariable("VMIN", getS("Vitesse mineur"), EnumMetierUnite.M_PAR_S, EnumMetierTypeNombre.REEL, 4);
  public static final MetierDescriptionVariable VMAJ = new MetierDescriptionVariable("VMAJ", getS("Vitesse majeur"), EnumMetierUnite.M_PAR_S, EnumMetierTypeNombre.REEL, 4);
  public static final MetierDescriptionVariable TAUF = new MetierDescriptionVariable("TAUF", getS("Contrainte au fond"), EnumMetierUnite.PA, EnumMetierTypeNombre.REEL, 6);
  public static final MetierDescriptionVariable Y    = new MetierDescriptionVariable("Y",    getS("Hauteur d'eau maximale"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable HMOY = new MetierDescriptionVariable("HMOY", getS("Hauteur d'eau moyenne"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable Q2G  = new MetierDescriptionVariable("Q2G",  getS("Debit majeur gauche"), EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable Q2D  = new MetierDescriptionVariable("Q2D",  getS("Debit majeur droit"), EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable SS   = new MetierDescriptionVariable("SS",   getS("Section mouillee du stockage"), EnumMetierUnite.M2, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable VOL  = new MetierDescriptionVariable("VOL",  getS("Volume du lit actif"), EnumMetierUnite.M3, EnumMetierTypeNombre.REEL, 0);
  public static final MetierDescriptionVariable VOLS = new MetierDescriptionVariable("VOLS", getS("Volume de stockage"), EnumMetierUnite.M3, EnumMetierTypeNombre.REEL, 0);
  public static final MetierDescriptionVariable CHAR = new MetierDescriptionVariable("CHAR", getS("Charge"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable ZMAX = new MetierDescriptionVariable("ZMAX", getS("Cote maximale atteinte"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable TZMA = new MetierDescriptionVariable("TZMA", getS("Instant de cote maximale"), EnumMetierUnite.S, EnumMetierTypeNombre.REEL, 1);
  public static final MetierDescriptionVariable VZMX = new MetierDescriptionVariable("VZMX", getS("Vitesse a la cote maximale"), EnumMetierUnite.M_PAR_S, EnumMetierTypeNombre.REEL, 4);
  public static final MetierDescriptionVariable ZMIN = new MetierDescriptionVariable("ZMIN", getS("Cote minimale atteinte"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable TZMI = new MetierDescriptionVariable("TZMI", getS("Instant de cote minimale"), EnumMetierUnite.S, EnumMetierTypeNombre.REEL, 1);
  public static final MetierDescriptionVariable VINF = new MetierDescriptionVariable("VINF", getS("Vitesse minimale mineur"), EnumMetierUnite.M_PAR_S, EnumMetierTypeNombre.REEL, 4);
  public static final MetierDescriptionVariable VSUP = new MetierDescriptionVariable("VSUP", getS("Vitesse maximale mineur"), EnumMetierUnite.M_PAR_S, EnumMetierTypeNombre.REEL, 4);
  public static final MetierDescriptionVariable BMAX = new MetierDescriptionVariable("BMAX", getS("Largeur au miroir maximale"), EnumMetierUnite.M, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable TOND = new MetierDescriptionVariable("TOND", getS("Instant d'arrivee d'onde"), EnumMetierUnite.S, EnumMetierTypeNombre.REEL, 1);
  public static final MetierDescriptionVariable QMAX = new MetierDescriptionVariable("QMAX", getS("Debit maximal"), EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 2);
  public static final MetierDescriptionVariable TQMA = new MetierDescriptionVariable("TQMA", getS("Instant de debit maximal"), EnumMetierUnite.S, EnumMetierTypeNombre.REEL, 1);
  public static final MetierDescriptionVariable EMAX = new MetierDescriptionVariable("EMAX", getS("Energie maximale"), EnumMetierUnite.J, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable PENE = new MetierDescriptionVariable("PENE", getS("Pente d'�nergie"), EnumMetierUnite.RIEN, EnumMetierTypeNombre.REEL, 3);

  /** 
   * Variables pour la s�dimentologie
   */
  public static final MetierDescriptionVariable QS_MEYERPETER    = new MetierDescriptionVariable("QSMEY",  getS("D�bit solide")+" (Meyer-Peter)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_LEFORT91      = new MetierDescriptionVariable("QSL91",  getS("D�bit solide")+" (Lefort 1991)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_LEFORT07      = new MetierDescriptionVariable("QSL07",  getS("D�bit solide")+" (Lefort 2007)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_ENGELUND      = new MetierDescriptionVariable("QSENG",  getS("D�bit solide")+" (Engelund/Hansen)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_RECKING10     = new MetierDescriptionVariable("QSR10",  getS("D�bit solide")+" (Recking 2010)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_RECKING11     = new MetierDescriptionVariable("QSR11",  getS("D�bit solide")+" (Recking 2011)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_VANRIJN       = new MetierDescriptionVariable("QSVAN",  getS("D�bit solide")+" (Van Rijn)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_SMARTJAGGI    = new MetierDescriptionVariable("QSSMA",  getS("D�bit solide")+" (Smart/J�ggi)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_RICKENMANN    = new MetierDescriptionVariable("QSRIC",  getS("D�bit solide")+" (Rickenmann)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_MEYERPETER= new MetierDescriptionVariable("QSMMEY", getS("D�bit solide moyenn�")+" (Meyer-Peter)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_LEFORT91  = new MetierDescriptionVariable("QSML91", getS("D�bit solide moyenn�")+" (Lefort 1991)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_LEFORT07  = new MetierDescriptionVariable("QSML07", getS("D�bit solide moyenn�")+" (Lefort 2007)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_ENGELUND  = new MetierDescriptionVariable("QSMENG", getS("D�bit solide moyenn�")+" (Engelund/Hansen)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_RECKING10 = new MetierDescriptionVariable("QSMR10", getS("D�bit solide moyenn�")+" (Recking 2010)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_RECKING11 = new MetierDescriptionVariable("QSMR11", getS("D�bit solide moyenn�")+" (Recking 2011)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_VANRIJN   = new MetierDescriptionVariable("QSMVAN", getS("D�bit solide moyenn�")+" (Van Rijn)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_SMARTJAGGI= new MetierDescriptionVariable("QSMSMA", getS("D�bit solide moyenn�")+" (Smart/J�ggi)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);
  public static final MetierDescriptionVariable QS_MOY_RICKENMANN= new MetierDescriptionVariable("QSMRIC", getS("D�bit solide moyenn�")+" (Rickenmann)", EnumMetierUnite.M3_PAR_S, EnumMetierTypeNombre.REEL, 3);

  /**
   * Toutes les variables possibles, certaines issues de calculs, certaines
   * calcul�es � la vol�e. Index�s dans l'ordre d'apparition.
   */
  public static final MetierDescriptionVariable[] VARIABLES= {
    ZREF,
    RGC,
    RDC,
    KMIN,
    KMAJ,
    Z,
    QMIN,
    QMAJ,
    S1,
    S2,
    FR,
    BETA,
    B1,
    B2,
    BS,
    P1,
    P2,
    RH1,
    RH2,
    VMIN,
    VMAJ,
    TAUF,
    Y,
    HMOY,
    Q2G,
    Q2D,
    SS,
    VOL,
    VOLS,
    CHAR,
    ZMAX,
    TZMA,
    VZMX,
    ZMIN,
    TZMI,
    VINF,
    VSUP,
    BMAX,
    TOND,
    QMAX,
    TQMA,
    EMAX,
    QS_MEYERPETER,
    QS_LEFORT91,
    QS_LEFORT07,
    QS_ENGELUND,
    QS_RECKING10,
    QS_RECKING11,
    QS_VANRIJN,
    QS_SMARTJAGGI,
    QS_RICKENMANN,
    QS_MOY_MEYERPETER,
    QS_MOY_LEFORT91,
    QS_MOY_LEFORT07,
    QS_MOY_ENGELUND,
    QS_MOY_RECKING10,
    QS_MOY_RECKING11,
    QS_MOY_VANRIJN,
    QS_MOY_SMARTJAGGI,
    QS_MOY_RICKENMANN,
    PENE
  };
  
  
  @Override
  public void initialise(MetierHydraulique1d _o) {
//    super.initialise(_o);
    if (_o instanceof MetierDescriptionVariable) {
      MetierDescriptionVariable q= (MetierDescriptionVariable)_o;
      nom(q.nom());
      description(q.description());
      unite(q.unite());
      type(q.type());
      nbDecimales(q.nbDecimales());
    }
  }
  @Override
  final public MetierDescriptionVariable creeClone() {
    MetierDescriptionVariable p=
      new MetierDescriptionVariable();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "variable " + nom_;
    return s;
  }
  /*** MetierDescriptionVariable ***/
  // constructeurs
  public MetierDescriptionVariable() {
    super();
    nom_= "var";
    description_= "Variable";
    unite_= EnumMetierUnite.RIEN;
    type_= EnumMetierTypeNombre.ENTIER;
    nbDecimales_= 0;
    
    notifieObjetCree();
  }
  

  /**
   * Creation d'une variable a partir de ses informations.
   */
  public  MetierDescriptionVariable(String _nom, String _desc, EnumMetierUnite _unite, EnumMetierTypeNombre _type, int _nbDec) {
    super();
    nom_=_nom;
    description_=_desc;
    unite_=_unite;
    type_=_type;
    nbDecimales_=_nbDec;
    
    notifieObjetCree();
  }
  
  
  @Override
  public void dispose() {
    nom_= null;
    description_= null;
    unite_= null;
    type_= null;
    nbDecimales_= 0;
    super.dispose();
  }
  // Le nom, comme il apparait dans les fichiers de r�sultats.
  private String nom_;
  public String nom() {
    return nom_;
  }
  public void nom(String s) {
    nom_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nom");
  }
  private String description_;
  public String description() {
    return description_;
  }
  public void description(String s) {
    if (description_.equals(s)) return;
    description_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "description");
  }
  private EnumMetierUnite unite_;
  public EnumMetierUnite unite() {
    return unite_;
  }
  public void unite(EnumMetierUnite s) {
    if (unite_.value()==s.value()) return;
    unite_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "unite");
  }
  private EnumMetierTypeNombre type_;
  public EnumMetierTypeNombre type() {
    return type_;
  }
  public void type(EnumMetierTypeNombre s) {
    if (type_.value()==s.value()) return;
    type_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "type");
  }
  private int nbDecimales_;
  public int nbDecimales() {
    return nbDecimales_;
  }
  public void nbDecimales(int s) {
    if (nbDecimales_==s) return;
    nbDecimales_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nbDecimales");
  }
}
