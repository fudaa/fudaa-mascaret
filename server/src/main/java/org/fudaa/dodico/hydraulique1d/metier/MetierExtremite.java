/**
 * @file         MetierExtremite.java
 * @creation     2000-07-24
 * @modification $Date: 2007-12-10 18:35:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierLimiteQualiteDEau;
/**
 * Implémentation de l'objet métier "Extremite d'un bief".
 * On peut y associer une condition limite (DLimite), un noeud ou un profil.
 * De plus pour le noyau transcritique, on utilise 3 points (1, 2 et milieu) et un angle.
 * @version      $Revision: 1.3 $ $Date: 2007-12-10 18:35:38 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class MetierExtremite extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierExtremite) {
      MetierExtremite q= (MetierExtremite)_o;
      profilRattache((MetierProfil)q.profilRattache());
      noeudRattache((MetierNoeud)q.noeudRattache());
      conditionLimite((MetierLimite)q.conditionLimite());
      conditionLimiteQualiteDEau((MetierLimiteQualiteDEau)q.conditionLimiteQualiteDEau());
      point1(q.point1());
      point2(q.point2());
      pointMilieu(q.pointMilieu());
      angle(q.angle());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierExtremite p= new MetierExtremite();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= getS("extrémité")+" " + numero_;
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Extremité bief");
    if (conditionLimite_ == null) {
      res[0]= getS("Extremité bief")+" "+numero_;
      res[1]= "";
    } else {
      res[0]= getS("Extremité libre")+" "+numero_;
      res[1]= "";
      if (conditionLimite_.loi() != null) {
        res[1]=
          getS("loi")+" : "
            + conditionLimite_.loi().typeLoi()
            + " "
            + conditionLimite_.loi().nom();
      } else {
        if (conditionLimite_.typeLimiteCalcule().value()
          == EnumMetierLimiteCalcule._EVACUATION_LIBRE)
          res[1]= getS("Evacuation libre");
        else if (
          conditionLimite_.typeLimiteCalcule().value()
            == EnumMetierLimiteCalcule._HAUTEUR_NORMALE)
          res[1]= getS("Hauteur normale");
      }
    }

    if (conditionLimiteQualiteDEau_ != null) {
        if (conditionLimiteQualiteDEau_.loi() != null) {
            res[1] += " - "+getS("loi Tracer")+" : " +
                    conditionLimiteQualiteDEau_.loi().nom();
        }
    }

    return res;
  }
  /*** MetierExtremite ***/
  // constructeurs
  public MetierExtremite() {
    super();
    numero_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    profilRattache_= null;
    noeudRattache_= null;
    conditionLimite_= null;
    creeLimiteQualiteDEau();
    // conditionLimiteQualiteDEau_= null;
    point1_= null;
    point2_= null;
    pointMilieu_= null;
    angle_= 0.;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    profilRattache_= null;
    noeudRattache_= null;
    conditionLimite_= null;
    conditionLimiteQualiteDEau_=null;
    point1_= null;
    point2_= null;
    pointMilieu_= null;
    angle_= 0.;
    super.dispose();
  }
  // attributs
  private int numero_;
  public int numero() {
    return numero_;
  }
  public void numero(int s) {
    if (numero_==s) return;
    numero_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numero");
  }
  private MetierProfil profilRattache_;
  public MetierProfil profilRattache() {
    return profilRattache_;
  }
  public void profilRattache(MetierProfil s) {
    if (profilRattache_==s) return;
    profilRattache_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "profilRattache");
  }
  private MetierNoeud noeudRattache_;
  public MetierNoeud noeudRattache() {
    return noeudRattache_;
  }
  public void noeudRattache(MetierNoeud s) {
    if (noeudRattache_==s) return;
    noeudRattache_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "noeudRattache");
  }
  private MetierLimite conditionLimite_;
  public MetierLimite conditionLimite() {
    return conditionLimite_;
  }
  public void conditionLimite(MetierLimite s) {
    if (conditionLimite_==s) return;
    if (s != null)
      s.nom("limite" + numero());
    conditionLimite_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "conditionLimite");
  }
  private MetierLimiteQualiteDEau conditionLimiteQualiteDEau_;
  public MetierLimiteQualiteDEau conditionLimiteQualiteDEau() {
      return conditionLimiteQualiteDEau_;
  }

  public void conditionLimiteQualiteDEau(MetierLimiteQualiteDEau s) {
      if (conditionLimiteQualiteDEau_ == s)
          return;
      if (s != null)
      conditionLimiteQualiteDEau_ = s;
      Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                            "conditionLimiteQualiteDEau");
  }

  private MetierPoint2D point1_;
  public MetierPoint2D point1() {
    return point1_;
  }
  public void point1(MetierPoint2D s) {
    if (point1_==s) return;
    if ((point1_ != null)&&(s != null)) {
      if ( (point1_.x == s.x) && (point1_.y == s.y))
        return;
    }
    point1_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "point1");
  }
  private MetierPoint2D point2_;
  public MetierPoint2D point2() {
    return point2_;
  }
  public void point2(MetierPoint2D s) {
    if (point2_==s) return;
    if ((point2_ != null)&&(s != null)) {
      if ( (point2_.x == s.x) && (point2_.y == s.y))
        return;
    }
    point2_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "point2");
  }
  private MetierPoint2D pointMilieu_;
  public MetierPoint2D pointMilieu() {
    return pointMilieu_;
  }
  public void pointMilieu(MetierPoint2D s) {
    if (pointMilieu_==s) return;
    if ((pointMilieu_ != null)&&(s != null)) {
      if ( (pointMilieu_.x == s.x) && (pointMilieu_.y == s.y))
        return;
    }
    pointMilieu_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pointMilieu");
  }
  private double angle_;
  public double angle() {
    return angle_;
  }
  public void angle(double s) {
    if (angle_==s) return;
    angle_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "angle");
  }
  // methode
  public void creePoints() {
    point1(new MetierPoint2D(0, 0));
    point2(new MetierPoint2D(0, 0));
    pointMilieu(new MetierPoint2D(0, 0));
  }
  public MetierLimite creeLimite() {
    MetierLimite limite= new MetierLimite();
    conditionLimite(limite);
    return conditionLimite_;
  }

  public MetierLimiteQualiteDEau creeLimiteQualiteDEau() {
  MetierLimiteQualiteDEau limiteQualiteDEau= new MetierLimiteQualiteDEau();
  conditionLimiteQualiteDEau(limiteQualiteDEau);
  return conditionLimiteQualiteDEau_;
}



  public MetierNoeud creeNoeud() {
    MetierNoeud noeud= new MetierNoeud();
    noeud.ajouteExtremite((MetierExtremite)this);
    noeudRattache(noeud);
    return noeudRattache_;
  }
}
