/**
 * @file         MetierHydraulique1d.java
 * @creation     2001-07-16
 * @modification $Date: 2008-01-08 11:44:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.Hydraulique1dResource;

/**
 * Classe de base abstraite des objets m�tier hydraulique1d.
 *
 * @version      $Revision: 1.3 $ $Date: 2008-01-08 11:44:35 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public abstract class MetierHydraulique1d {
  protected MetierHydraulique1d() {
    super();
  }
  protected final static String getS(String chaine) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(chaine);
  }
  /**
   * Suppression de l'objet
   */
  public void supprime() {
    dispose();
    notifieObjetSupprime();
  }
  /**
   * Destructeur.
   */
  public void dispose() {
	/** Rien */
  }

  /**
   * Notifie la cr�ation de l'objet. Cette m�thode doit �tre appel�e � la fin du constructeur
   * de chaque classe d�rivante de MetierHydraulique1d, lorsque tous les attributs des classes sup�rieures
   * ont �t� affect�s.
   */
  protected final void notifieObjetCree() {
    String name=getClass().getName().substring(getClass().getName().lastIndexOf(".")+1).substring(6);
    Notifieur.getNotifieur().fireObjetCree(name, this);
  }

  /**
   * Notifie la suppression de l'objet.
   */
  private void notifieObjetSupprime() {
    String name=getClass().getName().substring(getClass().getName().lastIndexOf(".")+1).substring(6);
    Notifieur.getNotifieur().fireObjetSupprime(name, this);
  }

  /**
   * Renvoie un clone de l'objet par une duplication au premier niveau.
   *
   * @return le nouvel objet
   * @since 20/03/01
   */
  public MetierHydraulique1d creeClone() {
    throw new RuntimeException(getClass().getName() + ": creeClone not implemented");
  }

  /**
   * Permet de comparer cet objet a un autre. Accede a la methode equals() de Object.
   *
   * @param _o
   * @return true or false
   */
  public final boolean egale(final MetierHydraulique1d _o) {
    return this.equals(_o);
  }

  /**
   * Retourne une representation d'un objet sous forme de chaine.
   *
   * @return une chaine
   * @see java.lang.Object
   */
  public final String enChaine() {
    return toString();
  }

  /**
   * Initialise un objet a partir d'un autre objet. Le parametre doit etre de meme interface ou d'une interface derivee.
   *
   * @param _o
   */
  public void initialise(final MetierHydraulique1d _o) {
    throw new RuntimeException(getClass().getName() + ": initialise not implemented");
  }


  /**
   * Implementation par d�faut de la m�thode d�fini dans l'IDL.
   * @return String[]
   */
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= enChaine();
    res[1]= "";
    return res;
  }
  protected final static boolean egale(MetierHydraulique1d[] p, MetierHydraulique1d[] p2) {
    if (p == p2)
      return true;
    if (p == null || p2 == null)
      return false;

    int length = p.length;
    if (p2.length != length)
      return false;

    for (int i = 0; i < length; i++)
      if (p[i] != p2[i])
        return false;

    return true;
  }
}
