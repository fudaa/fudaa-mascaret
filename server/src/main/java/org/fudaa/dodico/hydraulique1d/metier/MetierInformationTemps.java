/**
 * @file         MetierInformationTemps.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Implémentation de l'objet métier "Information sur le Temps".
 * Pas utilisé dans Fudaa-Mascaret.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:34 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierInformationTemps extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierInformationTemps) {
      MetierInformationTemps q= (MetierInformationTemps)_o;
      info(q.info());
      numeroPas(q.numeroPas());
      t(q.t());
      dt(q.dt());
      dtlevy(q.dtlevy());
      jour(q.jour());
      heure(q.heure());
      minute(q.minute());
      seconde(q.seconde());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierInformationTemps p=
      new MetierInformationTemps();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "infoTemps(pas " + numeroPas_ + ",t " + t_ + ")";
    return s;
  }
  /*** MetierInformationTemps ***/
  // constructeurs
  public MetierInformationTemps() {
    super();
    info_= "";
    numeroPas_= -1;
    t_= 0.;
    dt_= 0.;
    dtlevy_= 0.;
    jour_= 0;
    heure_= 0;
    minute_= 0;
    seconde_= 0;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    info_= null;
    numeroPas_= -1;
    t_= 0.;
    dt_= 0.;
    dtlevy_= 0.;
    jour_= 0;
    heure_= 0;
    minute_= 0;
    seconde_= 0;
    super.dispose();
  }
  // Attributs
  private String info_;
  public String info() {
    return info_;
  }
  public void info(String s) {
    if (info_.equals(s)) return;
    info_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "info");
  }
  private int numeroPas_;
  public int numeroPas() {
    return numeroPas_;
  }
  public void numeroPas(int s) {
    if (numeroPas_==s) return;
    numeroPas_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numeroPas");
  }
  private double t_;
  public double t() {
    return t_;
  }
  public void t(double s) {
    if (t_==s) return;
    t_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
  }
  private double dt_;
  public double dt() {
    return dt_;
  }
  public void dt(double s) {
    if (dt_==s) return;
    dt_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "dt");
  }
  private double dtlevy_;
  public double dtlevy() {
    return dtlevy_;
  }
  public void dtlevy(double s) {
    if (dtlevy_==s) return;
    dtlevy_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "dtlevy");
  }
  private int jour_;
  public int jour() {
    return jour_;
  }
  public void jour(int s) {
    if (jour_==s) return;
    jour_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "jour");
  }
  private int heure_;
  public int heure() {
    return heure_;
  }
  public void heure(int s) {
    if (heure_==s) return;
    heure_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "heure");
  }
  private int minute_;
  public int minute() {
    return minute_;
  }
  public void minute(int s) {
    if (minute_==s) return;
    minute_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "minute");
  }
  private int seconde_;
  public int seconde() {
    return seconde_;
  }
  public void seconde(int s) {
    if (seconde_==s) return;
    seconde_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "seconde");
  }
}
