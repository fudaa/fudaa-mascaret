/**
 * @file         MetierLaisse.java
 * @creation     2000-12-05
 * @modification $Date: 2007-11-20 11:42:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Implémentation de l'objet métier "Un élément d'une laisse de crue".
 * Associe une cote, un site (une abscisse sur un bief) et un nom.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:25 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierLaisse extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierLaisse) {
      MetierLaisse q= (MetierLaisse)_o;
      nom(q.nom());
      site(q.site());
      cote(q.cote());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLaisse p= new MetierLaisse();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "laisse " + nom_;
    if (site_ != null)
      s += " (" + site_.toString() + ")";
    return s;
  }
  /*** MetierLaisse ***/
  // constructeurs
  public MetierLaisse() {
    super();
    site_= new MetierSite();
    nom_= "Laisse";
    cote_= 0.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    site_= null;
    nom_= null;
    cote_= 0.;
    super.dispose();
  }
  // attributs
  private String nom_;
  public String nom() {
    return nom_;
  }
  public void nom(String s) {
    if (nom_.equals(s)) return;
    nom_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nom");
  }
  private MetierSite site_;
  public MetierSite site() {
    return site_;
  }
  public void site(MetierSite s) {
    if (site_==s) return;
    site_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "site");
  }
  private double cote_;
  public double cote() {
    return cote_;
  }
  public void cote(double s) {
    if (cote_==s) return;
    cote_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "cote");
  }
}
