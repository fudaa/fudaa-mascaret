/*
 * @file         MetierLiaison.java
 * @creation
 * @modification $Date: 2007-11-20 11:42:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierCaracteristiqueLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierChenalLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierOrificeLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierSeuilLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierSiphonLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierTopologieCasierCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierTopologieLiaison;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierTopologieRiviereCasier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Implémentation de l'objet métier "Liaison casier".
 * Associe des caractéristiques d'une liaison et une topologie (relation avec les autres éléments du réseau).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:34 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLiaison extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierLiaison) {
      MetierLiaison q= (MetierLiaison)_o;
      if (q.topologie() != null)
        topologie((MetierTopologieLiaison)q.topologie().creeClone());
      if (q.caracteristiques() != null)
        caracteristiques(
          (MetierCaracteristiqueLiaison)q.caracteristiques().creeClone());
      numero(q.numero());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLiaison p= new MetierLiaison();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    return " "+getS("liaison")+" "+getS("numero")+" :" + numero_;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Liaison")+numero_;
    res[1]= "";
    if (topologie_ != null) {
      res[1]= topologie_.getInfos()[1] + " ";
    }
    if (caracteristiques_ != null) {
      res[0] += " " + caracteristiques_.getInfos()[0];
      res[1] += caracteristiques_.getInfos()[1];
    }
    return res;
  }
  /*** MetierLiaison ***/
  // constructeurs
  public MetierLiaison() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    //System.err.println("Constructeur liaison !"+this.toString());
    topologie_= null;
    caracteristiques_= new MetierSeuilLiaison();

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    id_= 0;
    topologie_= null;
    caracteristiques_= null;
    super.dispose();
  }
  // attributs
  private int id_;
  private int numero_;
  public int numero() {
    return numero_;
  }
  public void numero(int s) {
    if (numero_ == s) return;
    numero_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numero");
  }
  private MetierTopologieLiaison topologie_;
  public MetierTopologieLiaison topologie() {
    return topologie_;
  }
  public void topologie(MetierTopologieLiaison s) {
    if (topologie_==s) return;
    topologie_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "topologie");
  }
  private MetierCaracteristiqueLiaison caracteristiques_;
  public MetierCaracteristiqueLiaison caracteristiques() {
    return caracteristiques_;
  }
  public void caracteristiques(MetierCaracteristiqueLiaison s) {
    if (caracteristiques_==s) return;
    caracteristiques_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "caracteristiques");
  }
  // méthodes
  public boolean isRiviereCasier() {
    if (topologie_ == null) {
      MetierTopologieLiaison g=
        new MetierTopologieRiviereCasier();
      topologie(g);
      return true;
    }
    return topologie_.isRiviereCasier();
  }
  public void toRiviereCasier() {
    if (isRiviereCasier())
      return;
    MetierTopologieLiaison g=
      new MetierTopologieRiviereCasier();
    topologie(g);
  }
  public boolean isCasierCasier() {
    if (topologie_ == null) {
      MetierTopologieLiaison g=
        new MetierTopologieCasierCasier();
      topologie(g);
      return true;
    }
    return topologie_.isCasierCasier();
  }
  public void toCasierCasier() {
    if (isCasierCasier())
      return;
    MetierTopologieLiaison g=
      new MetierTopologieCasierCasier();
    topologie(g);
  }
  public double getAbscisse() {
    return topologie_.getAbscisse();
  }
  public void setAbscisse(double abscisse) {
    topologie_.setAbscisse(abscisse);
  }
  public MetierCasier getCasierRattache() {
    return topologie_.getCasierRattache();
  }
  public void setCasierRattache(MetierCasier casierRattache) {
    topologie_.setCasierRattache(casierRattache);
  }
  public MetierBief getBiefRattache() {
    return topologie_.getBiefRattache();
  }
  public void setBiefRattache(MetierBief biefRattache) {
    topologie_.setBiefRattache(biefRattache);
  }
  public MetierCasier getCasierAmontRattache() {
    return topologie_.getCasierAmontRattache();
  }
  public void setCasierAmontRattache(MetierCasier casierRattache) {
    topologie_.setCasierAmontRattache(casierRattache);
  }
  public MetierCasier getCasierAvalRattache() {
    return topologie_.getCasierAvalRattache();
  }
  public void setCasierAvalRattache(MetierCasier casierRattache) {
    topologie_.setCasierAvalRattache(casierRattache);
  }
  public boolean isSeuil() {
    return caracteristiques_.isSeuil();
  }
  public void toSeuil() {
    if (isSeuil())
      return;
    double z = getCote();
    supprimeCaracteristique();
    MetierCaracteristiqueLiaison carac=
      new MetierSeuilLiaison();
    carac.setCote(z);
    caracteristiques(carac);
  }
  public boolean isChenal() {
    return caracteristiques_.isChenal();
  }
  public void toChenal() {
    if (isChenal())
      return;
    double z = getCote();
    supprimeCaracteristique();
    MetierCaracteristiqueLiaison carac=
      new MetierChenalLiaison();
    carac.setCote(z);
    caracteristiques(carac);
  }
  public boolean isSiphon() {
    return caracteristiques_.isSiphon();
  }
  public void toSiphon() {
    if (isSiphon())
      return;
    double z = getCote();
    supprimeCaracteristique();
    MetierCaracteristiqueLiaison carac=
      new MetierSiphonLiaison();
    carac.setCote(z);
    caracteristiques(carac);
  }
  public boolean isOrifice() {
    return caracteristiques_.isOrifice();
  }
  public void toOrifice() {
    if (isOrifice())
      return;
    double z = getCote();
    supprimeCaracteristique();
    MetierCaracteristiqueLiaison carac=
      new MetierOrificeLiaison();
    carac.setCote(z);
    caracteristiques(carac);
  }
  public double getCote() {
    return caracteristiques_.getCote();
  }
  public void setCote(double cote) {
    caracteristiques_.setCote(cote);
  }
  public double getCoefQOrifice() {
    return caracteristiques_.getCoefQOrifice();
  }
  public void setCoefQOrifice(double coefQOrifice) {
    caracteristiques_.setCoefQOrifice(coefQOrifice);
  }
  public double getLargeur() {
    return caracteristiques_.getLargeur();
  }
  public void setLargeur(double largeur) {
    caracteristiques_.setLargeur(largeur);
  }
  public double getLongueur() {
    return caracteristiques_.getLongueur();
  }
  public void setLongueur(double longueur) {
    caracteristiques_.setLongueur(longueur);
  }
  public double getCoefQ() {
    return caracteristiques_.getCoefQ();
  }
  public void setCoefQ(double coefQ) {
    caracteristiques_.setCoefQ(coefQ);
  }
  public double getSection() {
    return caracteristiques_.getSection();
  }
  public void setSection(double section) {
    caracteristiques_.setSection(section);
  }
  public double getCoefActivation() {
    return caracteristiques_.getCoefActivation();
  }
  public void setCoefActivation(double coefActivation) {
    caracteristiques_.setCoefActivation(coefActivation);
  }
  public double getRugosite() {
    return caracteristiques_.getRugosite();
  }
  public void setRugosite(double rugosite) {
    caracteristiques_.setRugosite(rugosite);
  }
  public double getCoefPerteCharge() {
    return caracteristiques_.getCoefPerteCharge();
  }
  public void setCoefPerteCharge(double coefPerteCharge) {
    caracteristiques_.setCoefPerteCharge(coefPerteCharge);
  }
  public EnumMetierSensDebitLiaison getSensDebit() {
    return caracteristiques_.getSensDebit();
  }
  public void setSensDebit(EnumMetierSensDebitLiaison sensDebit) {
    caracteristiques_.setSensDebit(sensDebit);
  }
  private void supprimeCaracteristique() {
    if (caracteristiques_ == null) return;
    if (caracteristiques_.isChenal()) {
      MetierChenalLiaison li = (MetierChenalLiaison)caracteristiques_;
      li.supprime();
    } else if (caracteristiques_.isOrifice()) {
      MetierOrificeLiaison li = (MetierOrificeLiaison)caracteristiques_;
      li.supprime();
    } else if (caracteristiques_.isSeuil()) {
      MetierSeuilLiaison li = (MetierSeuilLiaison) caracteristiques_;
      li.supprime();
    } else if (caracteristiques_.isSiphon()) {
      MetierSiphonLiaison li = (MetierSiphonLiaison) caracteristiques_;
      li.supprime();
    }

  }
}
