/**
 * @file         MetierLigneEauInitiale.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Vector;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier "ligne d'eau initiale".
 * Contient un tableau de points de type MetierLigneEauPoint.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:27 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLigneEauInitiale extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierLigneEauInitiale) {
      MetierLigneEauInitiale l= (MetierLigneEauInitiale)_o;
      nom(l.nom());
      format(l.format());
      if (l.points() != null) {
        MetierLigneEauPoint[] ip= new MetierLigneEauPoint[l.points().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (MetierLigneEauPoint)l.points()[i].creeClone();
        points(ip);
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLigneEauInitiale l=
      new MetierLigneEauInitiale();
    l.initialise(this);
    return l;
  }
  @Override
  final public String toString() {
    String s= "ligneEauInitiale";
    return s;
  }
  /*** MetierLigneEauInitiale ***/
  // constructeurs
  public MetierLigneEauInitiale() {
    super();
    nom_= "";
    //    lig.format(EnumMetierFormatFichier.LIDO_PERMANENT); le format est null (rentr� au clavier par d�faut)
    format_= null;
    points_= new MetierLigneEauPoint[0];
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    format_= null;
    points_= null;
    super.dispose();
  }
  // attributs
  private String nom_;
  public String nom() {
    return nom_;
  }
  public void nom(String nom) {
    if (nom_.equals(nom)) return;
    nom_= nom;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nom");
  }
  private MetierLigneEauPoint[] points_;
  public MetierLigneEauPoint[] points() {
    return points_;
  }
  public void points(MetierLigneEauPoint[] points) {
    if (egale(points_, points)) return;
    points_= points;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "points");
  }
  private EnumMetierFormatFichier format_;
  public EnumMetierFormatFichier format() {
    return format_;
  }
  public void format(EnumMetierFormatFichier format) {
    if (format_.value()==format.value()) return;
    format_= format;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "format");
  }
  // methodes
  public int[][] delimitationsSectionsBiefs() {
    int[][] delim= null;
    int biefprec= 0, sect1= 0;
    Vector vdelim= new Vector();
    if ((points_ != null) && (points_.length > 0)) {
      biefprec= points_[0].numeroBief();
      sect1= 0;
    }
    for (int i= 1; i < points_.length; i++) {
      int b= points_[i].numeroBief();
      if (biefprec == -1) {
        if (b == -1) {
          continue;
        } else {
          sect1= i - 1;
          biefprec= b;
          continue;
        }
      }
      if ((b != biefprec) || (i == points_.length - 1)) {
        int[] del= new int[2];
        del[0]= sect1 + 1;
        if (i == points_.length - 1)
          del[1]= points_.length;
        else
          del[1]= i;
        vdelim.add(del);
        biefprec= b;
        sect1= i - 1;
      }
    }
    delim= new int[vdelim.size()][2];
    for (int i= 0; i < delim.length; i++)
      delim[i]= (int[])vdelim.get(i);
    return delim;
  }
  public void creePointALaFin() {
    MetierLigneEauPoint point= new MetierLigneEauPoint();
    MetierLigneEauPoint[] us= new MetierLigneEauPoint[points_.length + 1];
    for (int i= 0; i < points_.length; i++)
      us[i]= points_[i];
    us[points_.length]= point;
    points(us);
  }
  public MetierLigneEauPoint[] creePoints(int nombre) {
    MetierLigneEauPoint[] res= new MetierLigneEauPoint[nombre];
    for (int i= 0; i < nombre; i++)
      res[i]= new MetierLigneEauPoint();
    return res;
  }
  public void supprimePoints(MetierLigneEauPoint[] points) {
    Vector newus= new Vector();
    for (int i= 0; i < points_.length; i++) {
      boolean trouve= false;
      for (int j= 0; j < points.length; j++) {
        if (points_[i] == points[j])
          trouve= true;
      }
      if (!trouve)
        newus.add(points_[i]);
    }
    MetierLigneEauPoint[] us= new MetierLigneEauPoint[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (MetierLigneEauPoint)newus.get(i);
    points(us);
  }
  public void supprimePoint(MetierLigneEauPoint point) {
    point.supprime();
  }
  public void supprimePointsBiefNumero(int numeroBief) {
    System.out.println("MetierLigneEauInitiale supprimePointsBiefNumero("+numeroBief+")");
    System.out.println("Avant : points_.length="+points_.length);
    for (int i = 0; i < points_.length; i++) {
      System.out.println(points_[i].numeroBief()+" "+points_[i].abscisse());
    }
    Vector newus= new Vector();
    for (int i= 0; i < points_.length; i++) {
      if (numeroBief == points_[i].numeroBief()) {
        supprimePoint(points_[i]);
      } else {
        newus.add(points_[i]);
      }
    }
    if (newus.size() == points_.length) {
      // aucune suppression n'a �t� faite.
      return;
    }
    MetierLigneEauPoint[] us= new MetierLigneEauPoint[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (MetierLigneEauPoint)newus.get(i);
    points(us);
    System.out.println("Apres : points_.length="+points_.length);
    for (int i = 0; i < points_.length; i++) {
      System.out.println(points_[i].numeroBief()+" "+points_[i].abscisse());
    }
  }
  public void miseAJourNumeroBiefPointsLigneEauInit(int nouveauNumero, int ancienNumero) {
    System.out.println("MetierLigneEauInitiale miseAJourNumeroBiefPointsLigneEauInit(nouveau="+nouveauNumero+", ancien="+ancienNumero+")");
    System.out.println("Avant : points_.length="+points_.length);
//    if (points_.length == 0) return false;
    for (int i = 0; i < points_.length; i++) {
      System.out.println(points_[i].numeroBief()+" "+points_[i].abscisse());
    }
    for (int i= 0; i < points_.length; i++) {
      if (ancienNumero == points_[i].numeroBief()) {
        points_[i].numeroBief(nouveauNumero);
      }
    }
    System.out.println("Apres :");
    for (int i = 0; i < points_.length; i++) {
      System.out.println(points_[i].numeroBief()+" "+points_[i].abscisse());
    }
//    return true;

  }

  public boolean existePointsBiefNumero(int numeroBief) {
    for (int i = 0; i < points_.length; i++) {
      if (points_[i].numeroBief() == numeroBief) {
        return true;
      }
    }
    return false;
  }
  public double  amontPointsBiefNumero(int numeroBief) {
    double amont = Double.MAX_VALUE;
    for (int i = 0; i < points_.length; i++) {
      if (points_[i].numeroBief() == numeroBief) {
        amont = Math.min(amont, points_[i].abscisse());
      }
    }
    return amont;
  }
  public double  avalPointsBiefNumero(int numeroBief) {
    double aval = Double.NEGATIVE_INFINITY;
    for (int i = 0; i < points_.length; i++) {
      if (points_[i].numeroBief() == numeroBief) {
        aval = Math.max(aval, points_[i].abscisse());
      }
    }
    return aval;
  }

}
