/**
 * @file         MetierLigneEauPoint.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier "point d'une ligne d'eau initiale".
 * Contient une abscisse, un num�ro de bief, une cote, un d�bit,
 * 2 coef. de frottement (mineur et majeur).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:26 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLigneEauPoint extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierLigneEauPoint) {
      MetierLigneEauPoint l= (MetierLigneEauPoint)_o;
      numeroBief(l.numeroBief());
      abscisse(l.abscisse());
      cote(l.cote());
      debit(l.debit());
      coefFrottementMin(l.coefFrottementMin());
      coefFrottementMaj(l.coefFrottementMaj());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLigneEauPoint l= new MetierLigneEauPoint();
    l.initialise(this);
    return l;
  }
  @Override
  final public String toString() {
    String s= "ligneEauPoint (b";
    s += numeroBief_;
    s += "," + abscisse_ + ")";
    return s;
  }
  /*** MetierLigneEauPoint ***/
  // constructeurs
  public MetierLigneEauPoint() {
    super();
    numeroBief_= 1;
    abscisse_= 0.;
    cote_= 0.;
    debit_= 0.;
    coefFrottementMin_= 20.;
    coefFrottementMaj_= 15.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    numeroBief_= -1;
    abscisse_= 0.;
    cote_= 0.;
    debit_= 0.;
    coefFrottementMin_= 0.;
    coefFrottementMaj_= 0.;
    super.dispose();
  }
  // attributs
  private int numeroBief_;
  public int numeroBief() {
    return numeroBief_;
  }
  public void numeroBief(int numeroBief) {
    if (numeroBief_==numeroBief) return;
    numeroBief_= numeroBief;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numeroBief");
  }
  private double abscisse_;
  public double abscisse() {
    return abscisse_;
  }
  public void abscisse(double abscisse) {
    if (abscisse_==abscisse) return;
    abscisse_= abscisse;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisse");
  }
  private double cote_;
  public double cote() {
    return cote_;
  }
  public void cote(double cote) {
    if (cote_==cote) return;
    cote_= cote;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "cote");
  }
  private double debit_;
  public double debit() {
    return debit_;
  }
  public void debit(double debit) {
    if (debit_==debit) return;
    debit_= debit;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "debit");
  }
  private double coefFrottementMin_;
  public double coefFrottementMin() {
    return coefFrottementMin_;
  }
  public void coefFrottementMin(double coefFrottementMin) {
    if (coefFrottementMin_==coefFrottementMin) return;
    coefFrottementMin_= coefFrottementMin;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "coefFrottementMin");
  }
  private double coefFrottementMaj_;
  public double coefFrottementMaj() {
    return coefFrottementMaj_;
  }
  public void coefFrottementMaj(double coefFrottementMaj) {
    if (coefFrottementMaj_==coefFrottementMaj) return;
    coefFrottementMaj_= coefFrottementMaj;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "coefFrottementMaj");
  }
}
