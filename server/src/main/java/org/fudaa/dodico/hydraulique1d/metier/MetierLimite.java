/**
 * @file         DLimite.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimnigramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
/**
 * Implémentation de l'objet métier d'une "condition limite d'un bief".
 * Contient un nom et un type (Par loi, évacuation Libre ou hauteur normale)
 * Si le type est par loi, on y rattche une loi hydraulique.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:27 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLimite extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierLimite) {
      MetierLimite q= (MetierLimite)_o;
      if (q.loi() != null)
        loi((MetierLoiHydraulique)q.loi().creeClone());
      nom(q.nom());
      typeLimiteCalcule(q.typeLimiteCalcule());
      condLimiteImposee(q.condLimiteImposee());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLimite l= new MetierLimite();
    l.initialise(this);
    return l;
  }
  @Override
  final public String toString() {
    String s= "limite " + nom_;
    if (loi_ != null)
      s += "(loi " + loi_.toString() + ")";
    return s;
  }
  /*** DLimite ***/
  // constructeurs
  public MetierLimite() {
    super();
    nom_= "limite";
    typeLimiteCalcule_= EnumMetierLimiteCalcule.EVACUATION_LIBRE;
    loi_= null;
    condLimiteImposee_=EnumMetierCondLimiteImposee.DEBIT;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    typeLimiteCalcule_= null;
    loi_= null;
    condLimiteImposee_=null;
    super.dispose();
  }
  // attributs
  private String nom_;
  public String nom() {
    return nom_;
  }
  public void nom(String nom) {
    if (nom_.equals(nom)) return;
    nom_= nom;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nom");
  }
  private EnumMetierLimiteCalcule typeLimiteCalcule_;
  public EnumMetierLimiteCalcule typeLimiteCalcule() {
    return typeLimiteCalcule_;
  }
  public void typeLimiteCalcule(EnumMetierLimiteCalcule typeLimiteCalcule) {
    if (typeLimiteCalcule_.value() == typeLimiteCalcule.value()) return;
    typeLimiteCalcule_= typeLimiteCalcule;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "typeLimiteCalcule");
  }
  private MetierLoiHydraulique loi_;
  public MetierLoiHydraulique loi() {
    return loi_;
  }
  public void loi(MetierLoiHydraulique loi) {
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }
  // methodes
  public MetierLoiTarage creeLoiTarage() {
    MetierLoiTarage tar= new MetierLoiTarage();
    loi(tar);
    return tar;
  }
  public MetierLoiHydrogramme creeLoiHydrogramme() {
    MetierLoiHydrogramme tar= new MetierLoiHydrogramme();
    loi(tar);
    return tar;
  }
  public MetierLoiLimnigramme creeLoiLimnigramme() {
    MetierLoiLimnigramme tar= new MetierLoiLimnigramme();
    loi(tar);
    return tar;
  }

  private EnumMetierCondLimiteImposee condLimiteImposee_;
  public EnumMetierCondLimiteImposee condLimiteImposee() {
      return condLimiteImposee_;
  }

  public void condLimiteImposee(EnumMetierCondLimiteImposee newCondLimiteImposee) {

      if (condLimiteImposee_.value() == newCondLimiteImposee.value()) return;
      condLimiteImposee_=newCondLimiteImposee;
      Notifieur.getNotifieur().fireObjetModifie(
            toString(),
            this,
            "condLimiteImposee");


  }
}
