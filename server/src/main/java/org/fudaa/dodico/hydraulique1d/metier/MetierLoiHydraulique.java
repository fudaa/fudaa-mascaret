/**
 * @file         DLoiHydraulique.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Comparator;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimniHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimnigramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiOuvertureVanne;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiRegulation;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
/**
 * Impl�mentation abstraite de l'objet m�tier "loi hydraulique" des donn�es hydrauliques.
 * Associe un nom et un num�ro.
 * @see org.fudaa.dodico.hydraulique1d.metier.loi.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:28 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public abstract class MetierLoiHydraulique extends MetierHydraulique1d
  implements Comparator {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierLoiHydraulique) {
      MetierLoiHydraulique q= (MetierLoiHydraulique)_o;
      nom(q.nom());
    }
  }
  @Override
  public String toString() {
    return nom() + " numero " + (indice()+1);
  }

  /**
   * Compares its two arguments for order.  Returns a negative integer,
   * zero, or a positive integer as the first argument is less than, equal
   * to, or greater than the second.<p>
   *
   * The implementor must ensure that <tt>sgn(compare(x, y)) ==
   * -sgn(compare(y, x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
   * implies that <tt>compare(x, y)</tt> must throw an exception if and only
   * if <tt>compare(y, x)</tt> throws an exception.)<p>
   *
   * The implementor must also ensure that the relation is transitive:
   * <tt>((compare(x, y)&gt;0) &amp;&amp; (compare(y, z)&gt;0))</tt> implies
   * <tt>compare(x, z)&gt;0</tt>.<p>
   *
   * Finally, the implementer must ensure that <tt>compare(x, y)==0</tt>
   * implies that <tt>sgn(compare(x, z))==sgn(compare(y, z))</tt> for all
   * <tt>z</tt>.<p>
   *
   * It is generally the case, but <i>not</i> strictly required that
   * <tt>(compare(x, y)==0) == (x.equals(y))</tt>.  Generally speaking,
   * any comparator that violates this condition should clearly indicate
   * this fact.  The recommended language is "Note: this comparator
   * imposes orderings that are inconsistent with equals."
   *
   * @param o1 the first object to be compared.
   * @param o2 the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the
   * 	       first argument is less than, equal to, or greater than the
   *	       second.
   */
  @Override
  public int compare(Object o1, Object o2) {
    if ((o1 instanceof MetierLoiHydraulique) && (o1 instanceof MetierLoiHydraulique)) {
      MetierLoiHydraulique loi1= (MetierLoiHydraulique)o1;
      MetierLoiHydraulique loi2= (MetierLoiHydraulique)o2;
      return loi1.numero() - loi2.numero();
    } else
      return 0;
  }
  /*** DLoiHydraulique ***/
  // constructeurs
  public MetierLoiHydraulique() {
    super();
    nom_= toString();
    numero_= Identifieur.IDENTIFIEUR.identificateurLibre("loi");
    indice_=0;
  }
  @Override
  public void dispose() {
    nom_= null;
    numero_=0;
    indice_=0;
    super.dispose();
  }
  // attributs
  /**
   * numero_ est �gale � un identifiant immuable de la loi.
   * Il est donc invariable lors de suppression d'autres lois contrairement � l'indice.
   * Il est utilis� pour des comparaisons de lois.
   * Pour l'utilisateur, cette attribut n'existe pas.
   */
  protected int numero_;
  /**
   * numero est �gale � un identifiant immuable de la loi.
   * Il est donc invariable lors de suppression d'autres lois contrairement � l'indice.
   * Il est utilis� pour des comparaisons de lois.
   * Pour l'utilisateur, cette attribut n'existe pas.
   * @return int le num�ro identifieur de cette loi.
   */
  public int numero() {
    return numero_;
  }
  public void numero(int numero) {
    if (numero_==numero) return;
    numero_= numero;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numero");
  }
  /**
   * indice_ correspond � l'indice de cette loi dans le tableau de lois de {@link MetierDonneesHydrauliques}.
   * Il est variable lors de suppression d'autres lois.
   * Pour l'utilisateur, indice + 1 est �gale au num�ro de la loi.
   */
  protected int indice_;

  /**
   * indice correspond � l'indice de cette loi dans le tableau de lois de {@link MetierDonneesHydrauliques}.
   * Il est variable lors de suppression d'autres lois.
   * Pour l'utilisateur, indice + 1 est �gale au num�ro de la loi.
   * @return int l'indice de cette loi dans le tableau de lois de {@link MetierDonneesHydrauliques}.
   */
  public int indice() {
    return indice_;
  }

  /**
   * indice permet de modifier l'indice de cette loi.
   * Modifie �ventuellement le nom de la loi.
   * @param indice int le nouveau indice.
   */
  public void indice(int indice) {
	  
	  
    // si le nom contient un marqueur "9999999999" lors de la premi�re instanciation.
    // on remplace le marqueur par la valeur de l'indice + 1.
    if (nom_ != null) {
      int indexe = nom_.indexOf("9999999999");
      if (indexe != -1) {
        String debutNom = nom_.substring(0,indexe);
        String finNom = nom_.substring(indexe+10,nom_.length());
        nom(debutNom+(indice+1)+finNom);
      }
    }

    if (indice_==indice) return;

    // Si l'ancien indice (indice_) est diff�rent du nouveau (indice)
    // Si en plus le nom commence par "loi " suivi de l'ancien num�ro variable (indice_+1)
    // on remplace ce num�ro par le nouveau (indice+1).
    if (nom_ != null) {
    	String chaineIndice = "" + (indice_ + 1);
        int indexe = nom_.indexOf(chaineIndice);

        if (indexe != -1 && nom_.substring(0,indexe).equals("loi ")) {

          String debutNom = nom_.substring(0, indexe);
          String finNom = nom_.substring(indexe+chaineIndice.length(),nom_.length());
          nom(debutNom + (indice + 1)+finNom);
      }
        
        //Pour traiter les anciens fichier dans lesquels le numero est � la fin du nom
        if (nom_.endsWith(""+(indice_+1))) {
              int indexe2 = nom_.indexOf("" + (indice_ + 1));
              if (indexe2 != -1){
                String debutNom = nom_.substring(0, indexe2);
                nom(debutNom + (indice + 1));
              }
            }
        
    }
    indice_= indice;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "indice");
  }
  protected String nom_;
  public String nom() {
    return nom_;
  }
  public void nom(String nom) {
    if (nom_.equals(nom)) return;
    nom_= nom;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nom");
  }
  // methodes
  public void creePoint(int i) {}
  public void supprimePoints(int[] i) {}
  public String typeLoi() {
    return null;
  }
  public int nbPoints() {
    return 0;
  }
  public boolean verifiePermanent() {
    return false;
  }
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  public void setValeur(double valeur, int ligne, int colonne) {}
  public double getValeur(int ligne, int colonne) {
    return Double.NaN;
  }
  public void setPoints(double[][] points) {}
  public double[][] pointsToDoubleArray() {
    return null;
  }
  public static void supprimeLoiHydraulique(MetierLoiHydraulique loi) {
    if (loi instanceof MetierLoiGeometrique)
      loi.supprime();
    else if (loi instanceof MetierLoiLimniHydrogramme)
      loi.supprime();
    else if (loi instanceof MetierLoiHydrogramme)
      loi.supprime();
    else if (loi instanceof MetierLoiLimnigramme)
      loi.supprime();
    else if (loi instanceof MetierLoiOuvertureVanne)
      loi.supprime();
    else if (loi instanceof MetierLoiRegulation)
      loi.supprime();
    else if (loi instanceof MetierLoiSeuil)
      loi.supprime();
    else if (loi instanceof MetierLoiTarage)
      loi.supprime();
    else if (loi instanceof MetierLoiTracer)
      loi.supprime();
    else {
      try {
        throw new RuntimeException(
          "MetierLoiHydraulique: aucun destructeur pour "
            + loi.getClass().getName());
      } catch (RuntimeException e) {
        e.printStackTrace();
      }
    }
  }

}
