/**
 * @file         DMaillage.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier "maillage".
 * Contient un indicateur de la m�thode de maillage et de la d�finition des sections de calcul.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:31 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierMaillage extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierMaillage) {
      MetierMaillage q= (MetierMaillage)_o;
      methode(q.methode());
      if (q.sections() != null)
        sections((MetierDefinitionSections)q.sections().creeClone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierMaillage p= new MetierMaillage();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "maillage";
    return s;
  }
  /*** DMaillage ***/
  // constructeurs
  public MetierMaillage() {
    super();
    methode_= EnumMetierMethodeMaillage.SECTIONS_SUR_PROFILS;
    sections_= null;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    methode_= null;
    sections_= null;
    super.dispose();
  }
  // Attributs
  private EnumMetierMethodeMaillage methode_;
  public EnumMetierMethodeMaillage methode() {
    return methode_;
  }
  public void methode(EnumMetierMethodeMaillage s) {
    if (methode_.value()==s.value()) return;
    methode_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "methode");
  }
  private MetierDefinitionSections sections_;
  public MetierDefinitionSections sections() {
    return sections_;
  }
  public void sections(MetierDefinitionSections s) {
    if (sections_==s) return;
    sections_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "sections");
  }
  // Methodes
  public void creeSectionsParSeries() {
    if (sections()!=null) sections().supprime();
    sections(new MetierDefinitionSectionsParSeries());
  }
  public void creeSectionsParSections() {
    if (sections()!=null) sections().supprime();
    sections(new MetierDefinitionSectionsParSections());
  }
}
