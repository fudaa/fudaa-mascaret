/**
 * @file         DOptionsListing.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:32 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Implémentation de l'objet métier "option" d'impression sur le fichier "listing".
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:32 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierOptionsListing extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierOptionsListing) {
      MetierOptionsListing q= (MetierOptionsListing)_o;
      geometrie(q.geometrie());
      planimetrage(q.planimetrage());
      reseau(q.reseau());
      loisHydrauliques(q.loisHydrauliques());
      ligneEauInitiale(q.ligneEauInitiale());
      calcul(q.calcul());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierOptionsListing p= new MetierOptionsListing();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "optionsListing";
    return s;
  }
  /*** DOptionsListing ***/
  // constructeurs
  public MetierOptionsListing() {
    super();
    geometrie_= false;
    planimetrage_= false;
    reseau_= false;
    loisHydrauliques_= false;
    ligneEauInitiale_= false;
    calcul_= true;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    geometrie_= false;
    planimetrage_= false;
    reseau_= false;
    loisHydrauliques_= false;
    ligneEauInitiale_= false;
    calcul_= true;
    super.dispose();
  }
  // Attributs
  private boolean geometrie_;
  public boolean geometrie() {
    return geometrie_;
  }
  public void geometrie(boolean s) {
    if (geometrie_==s) return;
    geometrie_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "geometrie");
  }
  private boolean planimetrage_;
  public boolean planimetrage() {
    return planimetrage_;
  }
  public void planimetrage(boolean s) {
    if (planimetrage_==s) return;
    planimetrage_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "planimetrage");
  }
  private boolean reseau_;
  public boolean reseau() {
    return reseau_;
  }
  public void reseau(boolean s) {
    if (reseau_==s) return;
    reseau_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "reseau");
  }
  private boolean loisHydrauliques_;
  public boolean loisHydrauliques() {
    return loisHydrauliques_;
  }
  public void loisHydrauliques(boolean s) {
    if (loisHydrauliques_==s) return;
    loisHydrauliques_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loisHydrauliques");
  }
  private boolean ligneEauInitiale_;
  public boolean ligneEauInitiale() {
    return ligneEauInitiale_;
  }
  public void ligneEauInitiale(boolean s) {
    if (ligneEauInitiale_==s) return;
    ligneEauInitiale_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "ligneEauInitiale");
  }
  private boolean calcul_;
  public boolean calcul() {
    return calcul_;
  }
  public void calcul(boolean s) {
    if (calcul_==s) return;
    calcul_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "calcul");
  }
}
