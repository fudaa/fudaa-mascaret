package org.fudaa.dodico.hydraulique1d.metier;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/*
 * @file         DOptionsListingTracer.java
 * @creation     2006-02-28
 * @modification $Date: 2007-11-20 11:42:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class MetierOptionsListingTracer extends MetierHydraulique1d {
  @Override
    public void initialise(MetierHydraulique1d _o) {
      super.initialise(_o);
      if (_o instanceof MetierOptionsListingTracer) {
          MetierOptionsListingTracer q= (MetierOptionsListingTracer)_o;
          concentInit(q.concentInit());
          loiTracer(q.loiTracer());
          concentrations(q.concentrations());
          bilanTracer(q.bilanTracer());

      }
    }
  @Override
    final public MetierHydraulique1d creeClone() {
      MetierOptionsListingTracer p= new MetierOptionsListingTracer();
      p.initialise(this);
      return p;
    }
  @Override
    final public String toString() {
      String s= "optionsListing tracer";
      return s;
    }

  @Override
    public void dispose() {
        concentInit_= false;
        loiTracer_= false;
        concentrations_= false;
        bilanTracer_= false;
        super.dispose();

    }

    public MetierOptionsListingTracer() {
         super();
        concentInit_= false;
        loiTracer_ = false;
        concentrations_ = true;
        bilanTracer_ = true;
        
        notifieObjetCree();
    }

    private boolean concentInit_;
    public boolean concentInit() {
      return concentInit_;
    }
    public void concentInit(boolean newConcentInit) {
        if (concentInit_==newConcentInit) return;
        concentInit_ = newConcentInit;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "concentInit");
    }

    private boolean loiTracer_;
    public boolean loiTracer() {
        return loiTracer_;
    }
    public void loiTracer(boolean newLoiTracer) {
        if (loiTracer_==newLoiTracer) return;
       loiTracer_ = newLoiTracer;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "loiTracer");


    }

    private boolean concentrations_;
    public boolean concentrations() {
        return concentrations_;
    }
    public void concentrations(boolean newConcentrations) {
        if (concentrations_==newConcentrations) return;
        concentrations_ = newConcentrations;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "concentrations");


    }

    private boolean bilanTracer_;
    public boolean bilanTracer() {
        return bilanTracer_;
    }

    public void bilanTracer(boolean newBilanTracer) {
        if (bilanTracer_==newBilanTracer) return;
        bilanTracer_ = newBilanTracer;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "bilanTracer");


    }
}
