/*
 * @file         DParametresGeneraux.java
 * @creation     2000-07-27
 * @modification $Date: 2007-12-05 09:37:32 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEventListener;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier des "parametres g�n�raux" de l'�tude.
 * Peut contenir, entre autre, le barrage principal d'une onde de submersion,
 *  les param�tres g�n�raux d'une �tude avec des casiers et le maillage.
 *
 * @version      $Revision: 1.3 $ $Date: 2007-12-05 09:37:32 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class MetierParametresGeneraux extends MetierHydraulique1d implements H1dObjetEventListener {

	@Override
	public void initialise(MetierHydraulique1d _o) {
		super.initialise(_o);
		if (_o instanceof MetierParametresGeneraux) {
			MetierParametresGeneraux q= (MetierParametresGeneraux)_o;
			regime(q.regime());
			noyauV5P2(q.noyauV5P2());
			ondeSubmersion(q.ondeSubmersion());
			froudeLimConditionLimite(q.froudeLimConditionLimite());
			if (q.barragePrincipal() != null)
				barragePrincipal((MetierBarragePrincipal)q.barragePrincipal().creeClone());
			compositionLits(q.compositionLits());
			frottementsParois(q.frottementsParois());
			traitementImpliciteFrottements(q.traitementImpliciteFrottements());
			typeFrottement(q.typeFrottement());
			typeCoefficient(q.typeCoefficient());
			perteChargeConfluents(q.perteChargeConfluents());
			elevationCoteArriveeFront(q.elevationCoteArriveeFront());
			interpolationLineaireCoefFrottement(
					q.interpolationLineaireCoefFrottement());
			debordProgressifLitMajeur(q.debordProgressifLitMajeur());
			debordProgressifZoneStockage(q.debordProgressifZoneStockage());
			implicitationNoyauTrans(q.implicitationNoyauTrans());
			optimisationNoyauTrans(q.optimisationNoyauTrans());
			perteChargeAutoElargissement(q.perteChargeAutoElargissement());
			hauteurEauMinimal(q.hauteurEauMinimal());
			termesNonHydrostatiques(q.termesNonHydrostatiques());
			if (q.maillage() != null)
				maillage((MetierMaillage)q.maillage().creeClone());
			if (q.zoneEtude() != null)
				zoneEtude((MetierZone)q.zoneEtude().creeClone());
			if (q.parametresCasier() != null)
				parametresCasier((MetierParametresGenerauxCasier)q.parametresCasier().creeClone());

		}
	}
	@Override
	final public MetierHydraulique1d creeClone() {
		MetierParametresGeneraux p=
				new MetierParametresGeneraux();
		p.initialise(this);
		return p;
	}
	@Override
	final public String toString() {
		String s= "Parametres Generaux";
		return s;
	}
	@Override
	public void objetCree(H1dObjetEvent e) {}
	@Override
	public void objetSupprime(H1dObjetEvent e) {
		MetierHydraulique1d src=e.getSource();
		/*        System.out.println("DParametresGeneraux objetSupprime(H1dObjetEvent e=)");
        System.out.println("\t e.getSource="+e.getSource().getClass().getName());
        System.out.println("\t e.getChamp="+e.getChamp());
        System.out.println("\t e.getMessage="+e.getMessage());*/
		if (src instanceof MetierBief) {
			supprimeSectionMaillageAvecBief((MetierBief)src);
		}
	}
	@Override
	public void objetModifie(H1dObjetEvent e) {}
	/*** DParametresGeneraux ***/
	// constructeurs
	public MetierParametresGeneraux() {
		super();
		regime_= EnumMetierRegime.FLUVIAL_PERMANENT;
		noyauV5P2_=false;
		ondeSubmersion_= false;
		froudeLimConditionLimite_= 1000;
		barragePrincipal_= null;
		compositionLits_= EnumMetierTypeCompositionLits.MINEUR_MAJEUR;
		attenuationConvection_ = false;
		apportDebit_ = false;
		frottementsParois_= false;
		profilsAbscAbsolu_= false;
		traitementImpliciteFrottements_= false;
		typeFrottement_= EnumMetierTypeFrottement.STRICKLER;
		typeCoefficient_ = EnumMetierTypeCoefficient.STRICKLER;
		perteChargeConfluents_= false;
		elevationCoteArriveeFront_= 0.05;
		interpolationLineaireCoefFrottement_= false;
		debordProgressifLitMajeur_= false;
		debordProgressifZoneStockage_= false;
		implicitationNoyauTrans_= false;
		optimisationNoyauTrans_ = false;
		perteChargeAutoElargissement_= false;
		hauteurEauMinimal_= 0.005;
		maillage_= new MetierMaillage();
		zoneEtude_= new MetierZone();
		parametresCasier_ = new MetierParametresGenerauxCasier();
		termesNonHydrostatiques_ = false;
		/*    evtSupport_= DObjetEventListenerSupport.createEventSupport();
    evtSupport_.clientListener(this);
    ((DHydraulique1dEventSender)UsineLib.findUsine()).addObjetEventListener(
      (DHydraulique1dEventListenerSupport)evtSupport_.this);*/
		Notifieur.getNotifieur().addObjetEventListener(this);

		notifieObjetCree();
	}
	@Override
	public void dispose() {
		/*    ((DHydraulique1dEventSender)UsineLib.findUsine()).removeObjetEventListener(
      (DHydraulique1dEventListenerSupport)evtSupport_.this);
    evtSupport_.clientListener(null);
    evtSupport_= null;*/
		Notifieur.getNotifieur().removeObjetEventListener(this);
		regime_= null;
		noyauV5P2_=false;
		ondeSubmersion_= false;
		froudeLimConditionLimite_= 0.;
		barragePrincipal_= null;
		compositionLits_= null;
		attenuationConvection_ = false;
		apportDebit_ = false;
		frottementsParois_= false;
		traitementImpliciteFrottements_= true;
		typeFrottement_= null;
		typeCoefficient_ = null;
		perteChargeConfluents_= false;
		elevationCoteArriveeFront_= 0.;
		interpolationLineaireCoefFrottement_= true;
		debordProgressifLitMajeur_= false;
		debordProgressifZoneStockage_= false;
		implicitationNoyauTrans_= false;
		optimisationNoyauTrans_ = false;
		perteChargeAutoElargissement_= false;
		hauteurEauMinimal_= 0.;
		maillage_= null;
		zoneEtude_= null;
		parametresCasier_= null;
		super.dispose();
	}
	// Attributs
	private boolean noyauV5P2_;
	public boolean noyauV5P2() {
		return noyauV5P2_;
	}
	public void noyauV5P2(boolean s) {
		if (noyauV5P2_==s) return;
		noyauV5P2_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"noyauV5P2");
	}
	private boolean ondeSubmersion_;
	public boolean ondeSubmersion() {
		return ondeSubmersion_;
	}
	public void ondeSubmersion(boolean s) {
		if (ondeSubmersion_==s) return;
		ondeSubmersion_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "ondeSubmersion");
	}
	private EnumMetierRegime regime_;
	public EnumMetierRegime regime() {
		return regime_;
	}
	public void regime(EnumMetierRegime s) {
		if (regime_.value()==s.value()) return;
		regime_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "regime");
	}
	private double froudeLimConditionLimite_;
	public double froudeLimConditionLimite() {
		return froudeLimConditionLimite_;
	}
	public void froudeLimConditionLimite(double s) {
		if (froudeLimConditionLimite_==s) return;
		froudeLimConditionLimite_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"froudeLimConditionLimite");
	}
	private MetierBarragePrincipal barragePrincipal_;
	public MetierBarragePrincipal barragePrincipal() {
		return barragePrincipal_;
	}
	public void barragePrincipal(MetierBarragePrincipal s) {
		if (barragePrincipal_==s) return;
		barragePrincipal_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "barragePrincipal");
	}
	private EnumMetierTypeCompositionLits compositionLits_;

	public EnumMetierTypeCompositionLits compositionLits() {
		return compositionLits_;
	}
	public void compositionLits(EnumMetierTypeCompositionLits s) {
		if (compositionLits_.value()==s.value()) return;
		compositionLits_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "compositionLits");
	}

	private boolean apportDebit_;

	public boolean apportDebit() {
		return apportDebit_;
	}
	public void apportDebit(boolean s) {
		if (apportDebit_ == s) {
			return;
		}
		apportDebit_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "apportDebit");
	}

	private boolean attenuationConvection_;

	public boolean attenuationConvection() {
		return attenuationConvection_;
	}
	public void attenuationConvection(boolean s) {
		if (attenuationConvection_==s) return;
		attenuationConvection_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "attenuationConvection");
	}


	private boolean frottementsParois_;
	public boolean frottementsParois() {
		return frottementsParois_;
	}
	public void frottementsParois(boolean s) {
		if (frottementsParois_==s) return;
		frottementsParois_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"frottementsParois");
	}
	private boolean profilsAbscAbsolu_;
	public boolean profilsAbscAbsolu() {
		return profilsAbscAbsolu_;
	}
	public void profilsAbscAbsolu(boolean s) {
		if (profilsAbscAbsolu_==s) return;
		profilsAbscAbsolu_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"profilsAbscAbsolu");
	}
	private boolean traitementImpliciteFrottements_;
	public boolean traitementImpliciteFrottements() {
		return traitementImpliciteFrottements_;
	}
	public void traitementImpliciteFrottements(boolean s) {
		if (traitementImpliciteFrottements_==s) return;
		traitementImpliciteFrottements_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"traitementImpliciteFrottements");
	}
	private EnumMetierTypeFrottement typeFrottement_;
	public EnumMetierTypeFrottement typeFrottement() {
		return typeFrottement_;
	}
	public void typeFrottement(EnumMetierTypeFrottement s) {
		if (typeFrottement_.value()==s.value()) return;
		typeFrottement_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "typeFrottement");
	}

	private EnumMetierTypeCoefficient typeCoefficient_;
	public EnumMetierTypeCoefficient typeCoefficient() {
		return typeCoefficient_;
	}
	public void typeCoefficient(EnumMetierTypeCoefficient s) {
		if (typeCoefficient_.value()==s.value()) return;
		typeCoefficient_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "typeCoefficient");
	}


	private boolean perteChargeConfluents_;
	public boolean perteChargeConfluents() {
		return perteChargeConfluents_;
	}
	public void perteChargeConfluents(boolean s) {
		if (perteChargeConfluents_==s) return;
		perteChargeConfluents_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"perteChargeConfluents");
	}
	private double elevationCoteArriveeFront_;
	public double elevationCoteArriveeFront() {
		return elevationCoteArriveeFront_;
	}
	public void elevationCoteArriveeFront(double s) {
		if (elevationCoteArriveeFront_==s) return;
		elevationCoteArriveeFront_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"elevationCoteArriveeFront");
	}
	private boolean interpolationLineaireCoefFrottement_;
	public boolean interpolationLineaireCoefFrottement() {
		return interpolationLineaireCoefFrottement_;
	}
	public void interpolationLineaireCoefFrottement(boolean s) {
		if (interpolationLineaireCoefFrottement_==s) return;
		interpolationLineaireCoefFrottement_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"interpolationLineaireCoefFrottement");
	}
	private boolean debordProgressifLitMajeur_;
	public boolean debordProgressifLitMajeur() {
		return debordProgressifLitMajeur_;
	}
	public void debordProgressifLitMajeur(boolean s) {
		if (debordProgressifLitMajeur_==s) return;
		debordProgressifLitMajeur_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"debordProgressifLitMajeur");
	}
	private boolean debordProgressifZoneStockage_;
	public boolean debordProgressifZoneStockage() {
		return debordProgressifZoneStockage_;
	}
	public void debordProgressifZoneStockage(boolean s) {
		if (debordProgressifZoneStockage_==s) return;
		debordProgressifZoneStockage_= s;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"debordProgressifZoneStockage");
	}
	private boolean implicitationNoyauTrans_;
	public boolean implicitationNoyauTrans() {
		return implicitationNoyauTrans_;
	}
	public void implicitationNoyauTrans(boolean implicitationNoyauTrans) {
		if (implicitationNoyauTrans_==implicitationNoyauTrans) return;
		implicitationNoyauTrans_= implicitationNoyauTrans;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"implicitationNoyauTrans");
	}


	private boolean optimisationNoyauTrans_;
	public boolean optimisationNoyauTrans() {
		return optimisationNoyauTrans_;
	}
	public void optimisationNoyauTrans(boolean optimisationNoyauTrans) {
		if (optimisationNoyauTrans_==optimisationNoyauTrans) return;
		optimisationNoyauTrans_= optimisationNoyauTrans;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"optimisationNoyauTrans");
	}



	private boolean perteChargeAutoElargissement_;
	public boolean perteChargeAutoElargissement() {
		return perteChargeAutoElargissement_;
	}
	public void perteChargeAutoElargissement(boolean perteChargeAutoElargissement) {
		if (perteChargeAutoElargissement_==perteChargeAutoElargissement) return;
		perteChargeAutoElargissement_= perteChargeAutoElargissement;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"perteChargeAutoElargissement");
	}
	private double hauteurEauMinimal_;
	public double hauteurEauMinimal() {
		return hauteurEauMinimal_;
	}
	public void hauteurEauMinimal(double hauteurEauMinimal) {
		if (hauteurEauMinimal_==hauteurEauMinimal) return;
		hauteurEauMinimal_= hauteurEauMinimal;
		Notifieur.getNotifieur().fireObjetModifie(
				toString(),
				this,
				"hauteurEauMinimal");
	}
	private MetierMaillage maillage_;
	public MetierMaillage maillage() {
		return maillage_;
	}
	public void maillage(MetierMaillage s) {
		if (maillage_==s) return;
		maillage_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "maillage");
	}
	private MetierZone zoneEtude_;
	public MetierZone zoneEtude() {
		return zoneEtude_;
	}
	public void zoneEtude(MetierZone s) {
		if (zoneEtude_==s) return;
		zoneEtude_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zoneEtude");
	}
	private MetierParametresGenerauxCasier parametresCasier_;
	public MetierParametresGenerauxCasier parametresCasier() {
		return parametresCasier_;
	}
	public void parametresCasier(MetierParametresGenerauxCasier s) {
		if (parametresCasier_==s) return;
		parametresCasier_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "parametresCasier");
	}

	private boolean termesNonHydrostatiques_;
	public boolean termesNonHydrostatiques() {
		return termesNonHydrostatiques_;
	}
	public void termesNonHydrostatiques(boolean s) {
		if (termesNonHydrostatiques_==s) return;
		termesNonHydrostatiques_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "termes non hydrostatiques");
	}


	// methodes
	public void recalculeZoneEtude(MetierReseau reseau) {
		double xori= reseau.getXOrigine();
		double xfin= reseau.getXFin();
		if (xori < Double.MAX_VALUE)
			zoneEtude_.abscisseDebut(xori);
		if (xfin > Double.NEGATIVE_INFINITY)
			zoneEtude_.abscisseFin(xfin);
		zoneEtude(zoneEtude_);
	}
	public MetierBarragePrincipal creeBarragePrincipal() {
		barragePrincipal_= new MetierBarragePrincipal();
		return barragePrincipal_;
	}
	public MetierMaillage creeMaillage() {
		return new MetierMaillage();
	}
	public void supprimeSectionMaillageAvecBief(MetierBief bief) {
		System.out.println("supprimeSectionMaillageAvecBief(MetierBief bief)");
		if (maillage() != null) {
			if (maillage().sections() != null) {
				maillage().sections().supprimeSectionMaillageAvecBief(bief);
			}
		}
	}
}
