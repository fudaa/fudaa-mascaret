/**
 * @creation     2003-05-30
 * @modification $Date: 2007-11-20 11:42:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Deniger: laisser pour compatibilit� avec existant * Impl�mentation de l'objet m�tier des "param�tres g�n�raux d'une �tude avec des casiers".
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:25 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierParametresGenerauxCasier extends MetierHydraulique1d {
  /*** MetierHydraulique1d ***/
  @Override
  public void initialise(MetierHydraulique1d _o) {
//    if (_o instanceof MetierParametresGenerauxCasier) {
//      MetierParametresGenerauxCasier q= (MetierParametresGenerauxCasier)_o;
//      coefImplicitationCasier(q.coefImplicitationCasier());
//      coefImplicitationCouplage(q.coefImplicitationCouplage());
//      nbMaxIterationCouplage(q.nbMaxIterationCouplage());
//      activation(q.activation());
//    }
  }
  @Override
  public MetierHydraulique1d creeClone() {
    MetierParametresGenerauxCasier p=
      new MetierParametresGenerauxCasier();
    p.initialise(this);
    return p;
  }
  @Override
  public String toString() {
    String s= "ParametresGenerauxCasier";
//    s += ": "
//      + coefImplicitationCasier_
//      + ","
//      + coefImplicitationCouplage_
//      + ","
//      + nbMaxIterationCouplage_;
    return s;
  }
  /*** DParametresGenerauxCasier ***/
  // constructeurs
  public MetierParametresGenerauxCasier() {
    super();
//    coefImplicitationCasier_= 0.5;
//    coefImplicitationCouplage_= 0.5;
//    nbMaxIterationCouplage_= 1;
    activation_= true;
    
    notifieObjetCree();
  }
  // destructeur
  @Override
  public void dispose() {
//    coefImplicitationCasier_= 0.;
//    coefImplicitationCouplage_= 0;
//    nbMaxIterationCouplage_= 0;
    activation_= false;
    super.dispose();
  }
  // attributs
//  private double coefImplicitationCasier_;
//  public double coefImplicitationCasier() {
//    return coefImplicitationCasier_;
//  }
//  public void coefImplicitationCasier(double t) {
//    if (coefImplicitationCasier_==t) return;
//    coefImplicitationCasier_= t;
//    Notifieur.getNotifieur().fireObjetModifie(
//      toString(),
//      this,
//      "coefImplicitationCasier");
//  }
//  private double coefImplicitationCouplage_;
//  public double coefImplicitationCouplage() {
//    return coefImplicitationCouplage_;
//  }
//  public void coefImplicitationCouplage(double t) {
//    if (coefImplicitationCouplage_==t) return;
//    coefImplicitationCouplage_= t;
//    Notifieur.getNotifieur().fireObjetModifie(
//      toString(),
//      this,
//      "coefImplicitationCouplage");
//  }
//  private int nbMaxIterationCouplage_;
//  public int nbMaxIterationCouplage() {
//    return nbMaxIterationCouplage_;
//  }
//  public void nbMaxIterationCouplage(int t) {
//    if (nbMaxIterationCouplage_==t) return;
//    nbMaxIterationCouplage_= t;
//    Notifieur.getNotifieur().fireObjetModifie(
//      toString(),
//      this,
//      "nbMaxIterationCouplage");
//  }
  private boolean activation_;
  public boolean activation() {
    return activation_;
  }
  public void activation(boolean t) {
    if (activation_==t) return;
    activation_= t;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "activation");
  }
}
