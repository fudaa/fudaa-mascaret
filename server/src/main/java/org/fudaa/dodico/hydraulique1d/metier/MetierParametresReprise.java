/**
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier des "param�tres de reprise d'un calcul".
 * Le contenu du fichier est stock� dans un tableau d'octets.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:28 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierParametresReprise extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    //DEBUG: a distance c'est toujours faux
    if (_o instanceof MetierParametresReprise) {
      MetierParametresReprise p= (MetierParametresReprise)_o;
      fichier(p.fichier());
      contenu(p.contenu());
      tFinal(p.tFinal());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierParametresReprise p=
      new MetierParametresReprise();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "param�tresReprise";
    return s;
  }
  /*** DParametresReprise ***/
  // constructeurs
  public MetierParametresReprise() {
    super();
    fichier_= "";
    contenu_= new byte[0];
    tFinal_= 0.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    fichier_= null;
    contenu_= null;
    tFinal_= 0.;
    super.dispose();
  }
  // attributs
  private String fichier_;
  public String fichier() {
    return fichier_;
  }
  public void fichier(String fichier) {
    if (fichier_.equals(fichier)) return;
    fichier_= fichier;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "fichier");
  }
  private byte[] contenu_;
  public byte[] contenu() {
    return contenu_;
  }
  public void contenu(byte[] contenu) {
    contenu_= contenu;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "contenu");
  }
  private double tFinal_;
  public double tFinal() {
    return tFinal_;
  }
  public void tFinal(double tFinal) {
    if (tFinal_==tFinal) return;
    tFinal_= tFinal;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "tFinal");
  }
}
