/**
 * @file         DParametresResultats.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/**
 * Impl�mentation de l'objet m�tier des "param�tres des fichiers r�sultats" de
 * l'�tude. Contients entre autres un tableau des description des variables dans
 * le fichier r�sultat, les options d'impression dans le fichier listing, les
 * tailles maximum des fichiers autoris�s par Fudaa-Mascaret et les param�tres
 * de stockages dans les fichiers r�sultats et listing (p�riode, d�but).
 * 
 * @version $Revision: 1.2 $ $Date: 2007-11-20 11:42:28 $ by $Author:
 *          bmarchan $
 * @author Axel von Arnim
 */
public class MetierParametresResultats extends MetierHydraulique1d {

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierParametresResultats) {
      MetierParametresResultats q=(MetierParametresResultats)_o;
      if (q.optionsListing()!=null)
        optionsListing((MetierOptionsListing)q.optionsListing().creeClone());
      if (q.optionsListingTracer()!=null)
        optionsListingTracer((MetierOptionsListingTracer)q.optionsListingTracer().creeClone());
      if (q.paramStockage()!=null)
        paramStockage((MetierParametresStockage)q.paramStockage().creeClone());
      if (q.paramTailleMaxFichier()!=null)
        paramTailleMaxFichier((MetierParametresTailleMaxFichier)q.paramTailleMaxFichier().creeClone());
      postRubens(q.postRubens());
      postOpthyca(q.postOpthyca());
      postOpthycaQualiteDEau(q.postOpthycaQualiteDEau());
      decalage(q.decalage());
      if (q.variables()!=null) {
        MetierDescriptionVariable[] ip=new MetierDescriptionVariable[q.variables().length];
        for (int i=0; i<ip.length; i++)
          ip[i]=(MetierDescriptionVariable)q.variables()[i].creeClone();
        variables(ip);
      }
    }
  }

  @Override
  final public MetierHydraulique1d creeClone() {
    MetierParametresResultats p=new MetierParametresResultats();
    p.initialise(this);
    return p;
  }

  @Override
  final public String toString() {
    String s="parametresResultats";
    return s;
  }

  /** * DParametresResultats ** */
  // constructeurs
  public MetierParametresResultats() {
    super();
    optionsListing_=new MetierOptionsListing();
    optionsListingTracer_=new MetierOptionsListingTracer();
    paramStockage_=new MetierParametresStockage();
    paramStockage_=new MetierParametresStockage();
    paramTailleMaxFichier_=new MetierParametresTailleMaxFichier();
    postRubens_=true;
    postOpthyca_=false;
    postOpthycaQualiteDEau_=true;
    decalage_=1.;
    variables_=new MetierDescriptionVariable[0];
    
    ajouteVariable("Y");
    ajouteVariable("VMIN");
    ajouteVariable("ZREF");
    ajouteVariable("Z");
    ajouteVariable("QMIN");
    ajouteVariable("QMAJ");

    notifieObjetCree();
  }

  @Override
  public void dispose() {
    optionsListing_=null;
    optionsListingTracer_=null;
    paramStockage_=null;
    postRubens_=false;
    postOpthyca_=false;
    postOpthycaQualiteDEau_=false;
    decalage_=0.;
    variables_=null;
    super.dispose();
  }

  // Attributs
  private MetierOptionsListing optionsListing_;

  public MetierOptionsListing optionsListing() {
    return optionsListing_;
  }

  public void optionsListing(MetierOptionsListing s) {
    if (optionsListing_==s)
      return;
    optionsListing_=s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "optionsListing");
  }

  private MetierOptionsListingTracer optionsListingTracer_;

  public MetierOptionsListingTracer optionsListingTracer() {
    return optionsListingTracer_;
  }

  public void optionsListingTracer(MetierOptionsListingTracer s) {
    if (optionsListingTracer_==s)
      return;
    optionsListingTracer_=s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "optionsListingTracer");
  }

  private MetierParametresStockage paramStockage_;

  public MetierParametresStockage paramStockage() {
    return paramStockage_;
  }

  public void paramStockage(MetierParametresStockage s) {
    if (paramStockage_==s)
      return;
    paramStockage_=s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "paramStockage");
  }

  private MetierParametresTailleMaxFichier paramTailleMaxFichier_;

  public MetierParametresTailleMaxFichier paramTailleMaxFichier() {
    return paramTailleMaxFichier_;
  }

  public void paramTailleMaxFichier(MetierParametresTailleMaxFichier t) {
    if (paramTailleMaxFichier_==t)
      return;
    paramTailleMaxFichier_=t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "paramTailleMaxFichier");
  }

  private boolean postRubens_;

  public boolean postRubens() {
    return postRubens_;
  }

  public void postRubens(boolean s) {
    if (postRubens_==s)
      return;
    postRubens_=s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "postRubens");
  }

  private boolean postOpthyca_;

  public boolean postOpthyca() {
    return postOpthyca_;
  }

  public void postOpthyca(boolean s) {
    if (postOpthyca_==s)
      return;
    postOpthyca_=s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "postOpthyca");
  }

  private boolean postOpthycaQualiteDEau_;

  public boolean postOpthycaQualiteDEau() {
    return postOpthycaQualiteDEau_;
  }

  public void postOpthycaQualiteDEau(boolean s) {
    if (postOpthycaQualiteDEau_==s)
      return;
    postOpthycaQualiteDEau_=s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "postOpthycaQualiteDEau");
  }

  private double decalage_;

  public double decalage() {
    return decalage_;
  }

  public void decalage(double s) {
    if (decalage_==s)
      return;
    decalage_=s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "decalage");
  }

  private MetierDescriptionVariable[] variables_;

  public MetierDescriptionVariable[] variables() {
    return variables_;
  }

  public void variables(MetierDescriptionVariable[] s) {
    if (egale(variables_, s))
      return;
    variables_=s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "variables");
  }

  // M�thodes
  public boolean existeVariable(String nomVariable) {
    boolean existe=false;
    for (int i=0; i<variables_.length; i++) {
      if (variables_[i].nom().equals(nomVariable))
        return true;
    }
    return existe;
  }

  /**
   * Ajoute une variable par son nom
   * @param _nom Le nom court de la variable
   */
  public void ajouteVariable(String _nom) {
    if (existeVariable(_nom)) return;
    
    for (int i=0; i<MetierDescriptionVariable.VARIABLES.length; i++) {
      if (MetierDescriptionVariable.VARIABLES[i].nom().equals(_nom)) {
        List<MetierDescriptionVariable> listVar=new ArrayList<MetierDescriptionVariable>(Arrays.asList(variables_));
        listVar.add(MetierDescriptionVariable.VARIABLES[i].creeClone());
        variables_=listVar.toArray(new MetierDescriptionVariable[0]);
        break;
      }
    }
  }
  
  public void ajouteVariable(String nom, String description, EnumMetierUnite unite, EnumMetierTypeNombre type, int nbDecimal) {
    if (existeVariable(nom))
      return;
    MetierDescriptionVariable descrip=new MetierDescriptionVariable();
    descrip.nom(nom);
    descrip.description(description);
    descrip.unite(unite);
    descrip.type(type);
    descrip.nbDecimales(nbDecimal);
    List<MetierDescriptionVariable> listVar=new ArrayList<MetierDescriptionVariable>(Arrays.asList(variables_));
    listVar.add(descrip);
    variables_=listVar.toArray(new MetierDescriptionVariable[0]);
  }

  public void supprimeVariable(String nomVariable) {
    Vector<MetierDescriptionVariable> newVars=new Vector<MetierDescriptionVariable>();
    for (int i=0; i<variables_.length; i++) {
      if (variables_[i].nom().equals(nomVariable)) {
        variables_[i].supprime();
      }
      else
        newVars.add(variables_[i]);
    }
    variables_=new MetierDescriptionVariable[newVars.size()];
    for (int i=0; i<variables_.length; i++)
      variables_[i]=(MetierDescriptionVariable)newVars.get(i);
  }

}
