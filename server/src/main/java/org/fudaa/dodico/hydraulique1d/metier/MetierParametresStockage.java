/**
 * @file         DParametresStockage.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Vector;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier des "param�tres de stockage" dans les fichiers listing et r�sultats.
 * Pr�cise notamment les p�riodes et le d�but d'�criture dans les fichiers listing et r�sultats.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:30 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 **/
public class MetierParametresStockage extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierParametresStockage) {
      MetierParametresStockage q= (MetierParametresStockage)_o;
      option(q.option());
      premierPasTempsStocke(q.premierPasTempsStocke());
      periodeListing(q.periodeListing());
      periodeResultat(q.periodeResultat());
      pasTempsImpression(q.pasTempsImpression());
      if (q.sites() != null) {
        MetierSite[] ip= new MetierSite[q.sites().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (MetierSite)q.sites()[i].creeClone();
        sites(ip);
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierParametresStockage p=
      new MetierParametresStockage();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "parametresStockage";
    return s;
  }
  /*** DParametresStockage ***/
  // constructeurs
  public MetierParametresStockage() {
    super();
    option_= EnumMetierOptionStockage.TOUTES_SECTIONS;
    premierPasTempsStocke_= 1;
    periodeListing_= 1;
    periodeResultat_= 1;
    pasTempsImpression_= 0.;
    sites_= new MetierSite[0];
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    option_= null;
    premierPasTempsStocke_= 0;
    periodeListing_= 0;
    periodeResultat_= 0;
    pasTempsImpression_= 0.;
    sites_= null;
    super.dispose();
  }
  // Attributs
  private EnumMetierOptionStockage option_;
  public EnumMetierOptionStockage option() {
    return option_;
  }
  public void option(EnumMetierOptionStockage s) {
    if (option_.value()==s.value()) return;
    option_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "option");
  }
  private int premierPasTempsStocke_;
  public int premierPasTempsStocke() {
    return premierPasTempsStocke_;
  }
  public void premierPasTempsStocke(int s) {
    if (premierPasTempsStocke_==s) return;
    premierPasTempsStocke_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "premierPasTempsStocke");
  }
  private int periodeListing_;
  public int periodeListing() {
    return periodeListing_;
  }
  public void periodeListing(int s) {
    if (periodeListing_==s) return;
    periodeListing_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "periodeListing");
  }
  private int periodeResultat_;
  public int periodeResultat() {
    return periodeResultat_;
  }
  public void periodeResultat(int s) {
    if (periodeResultat_==s) return;
    periodeResultat_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "periodeResultat");
  }
  private double pasTempsImpression_;
  public double pasTempsImpression() {
    return pasTempsImpression_;
  }
  public void pasTempsImpression(double s) {
    if (pasTempsImpression_==s) return;
    pasTempsImpression_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "pasTempsImpression");
  }
  private MetierSite[] sites_;
  public MetierSite[] sites() {
    return sites_;
  }
  public void sites(MetierSite[] s) {
    if (sites_==s) return;
    sites_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "sites");
  }
  // methodes
  public void creeSite(MetierBief biefRattache) {
    MetierSite site= new MetierSite();
    site.biefRattache(biefRattache);
    MetierSite[] us= new MetierSite[sites_.length + 1];
    for (int i= 0; i < sites_.length; i++)
      us[i]= sites_[i];
    us[sites_.length]= site;
    sites(us);
  }
  public void supprimeSites(MetierSite[] sites) {
    Vector newus= new Vector();
    for (int i= 0; i < sites_.length; i++) {
      boolean trouve= false;
      for (int j= 0; j < sites.length; j++) {
        if (sites_[i] == sites[j])
          trouve= true;
      }
      if (!trouve)
        newus.add(sites_[i]);
    }
    MetierSite[] us= new MetierSite[newus.size()];
    for (int i= 0; i < us.length; i++)
      us[i]= (MetierSite)newus.get(i);
    sites(us);
  }
}
