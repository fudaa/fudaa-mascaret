/**
 * @creation     2001-10-01
 * @modification $Date: 2007-11-20 11:42:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier des "param�tres des tailles maximum des fichiers" g�r� par Fudaa-Mascaret.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:30 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierParametresTailleMaxFichier extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierParametresTailleMaxFichier) {
      MetierParametresTailleMaxFichier q= (MetierParametresTailleMaxFichier)_o;
      maxListingCode(q.maxListingCode());
//      maxListingDamocles(q.maxListingDamocles());
      maxListingCalage(q.maxListingCalage());
      maxListingTracer(q.maxListingTracer());
      maxResultatOpthyca(q.maxResultatOpthyca());
      maxResultatRubens(q.maxResultatRubens());
      maxResultatOpthycaTracer(q.maxResultatOpthycaTracer());
      maxResultatRubensTracer(q.maxResultatRubensTracer());
      maxResultatReprise(q.maxResultatReprise());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierParametresTailleMaxFichier p=
      new MetierParametresTailleMaxFichier();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "parametresTailleMaxFichier";
    return s;
  }
  /*** DParametresTemporels ***/
  // constructeurs
  public MetierParametresTailleMaxFichier() {
    super();
    maxListingCode_= 500.;
//    maxListingDamocles_= 10.;
    maxListingCalage_=1000;
    maxListingTracer_=5000;
    maxResultatOpthyca_= 40000.;
    maxResultatRubens_= 20000.;
    maxResultatOpthycaTracer_= 40000.;
    maxResultatRubensTracer_= 20000.;
    maxResultatReprise_= 1000.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    maxListingCode_= 0.;
//    maxListingDamocles_= 0.;
    maxListingCalage_=0;
    maxListingTracer_=0;
    maxResultatOpthyca_= 0.;
    maxResultatRubens_= 0.;
    maxResultatOpthycaTracer_= 0.;
    maxResultatRubensTracer_= 0.;
    maxResultatReprise_= 0.;
    super.dispose();
  }
  // Attributs
  private double maxListingCode_;
  public double maxListingCode() {
    return maxListingCode_;
  }
  public void maxListingCode(double s) {
    if (maxListingCode_==s) return;
    maxListingCode_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "maxListingCode");
  }
//  private double maxListingDamocles_;
//  public double maxListingDamocles() {
//    return maxListingDamocles_;
//  }
//  public void maxListingDamocles(double s) {
//    if (maxListingDamocles_==s) return;
//    maxListingDamocles_= s;
//    Notifieur.getNotifieur().fireObjetModifie(
//      toString(),
//      this,
//      "maxListingDamocles");
//  }
  private double maxResultatOpthyca_;
  public double maxResultatOpthyca() {
    return maxResultatOpthyca_;
  }
  public void maxResultatOpthyca(double s) {
    if (maxResultatOpthyca_==s) return;
    maxResultatOpthyca_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "maxResultatOpthyca");
  }
  private double maxResultatRubens_;
  public double maxResultatRubens() {
    return maxResultatRubens_;
  }
  public void maxResultatRubens(double s) {
    if (maxResultatRubens_==s) return;
    maxResultatRubens_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "maxResultatRubens");
  }
  private double maxResultatOpthycaTracer_;
  public double maxResultatOpthycaTracer() {
    return maxResultatOpthycaTracer_;
  }
  public void maxResultatOpthycaTracer(double s) {
    if (maxResultatOpthycaTracer_==s) return;
    maxResultatOpthycaTracer_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "maxResultatOpthycaTracer");
  }
  private double maxResultatRubensTracer_;
  public double maxResultatRubensTracer() {
    return maxResultatRubensTracer_;
  }
  public void maxResultatRubensTracer(double s) {
    if (maxResultatRubensTracer_==s) return;
    maxResultatRubens_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "maxResultatRubensTracer");
  }

  private double maxResultatReprise_;
  public double maxResultatReprise() {
    return maxResultatReprise_;
  }
  public void maxResultatReprise(double s) {
    if (maxResultatReprise_==s) return;
    maxResultatReprise_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "maxResultatReprise");
  }

  private double maxListingCalage_;
  public double maxListingCalage() {
    return maxListingCalage_;
  }

  public void maxListingCalage(double s) {
    if (maxListingCalage_==s) return;
    maxListingCalage_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "maxListingCalage");
  }

  private double maxListingTracer_;
  public double maxListingTracer() {
      return maxListingTracer_;
  }

  public void maxListingTracer(double s) {
      if (maxListingTracer_ == s)
          return;
      maxListingTracer_ = s;
      Notifieur.getNotifieur().fireObjetModifie(
              toString(),
              this,
              "maxListingTracer");
}


}
