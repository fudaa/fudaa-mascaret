/**
 * @creation     2000-07-27
 * @modification $Date: 2007-11-20 11:42:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation de l'objet m�tier des "param�tres temporelles" de l'�tude.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:24 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierParametresTemporels extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierParametresTemporels) {
      MetierParametresTemporels q= (MetierParametresTemporels)_o;
      tempsInitial(q.tempsInitial());
      tempsFinal(q.tempsFinal());
      pasTemps(q.pasTemps());
      pasTempsImpression(q.pasTempsImpression());
      critereArret(q.critereArret());
      nbPasTemps(q.nbPasTemps());
      pasTempsVariable(q.pasTempsVariable());
      nbCourant(q.nbCourant());
      coteMax(q.coteMax());
      biefControle(q.biefControle());
      abscisseControle(q.abscisseControle());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierParametresTemporels p=
      new MetierParametresTemporels();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "parametresTemporels";
    return s;
  }
  /*** DParametresTemporels ***/
  // constructeurs
  public MetierParametresTemporels() {
    super();
    tempsInitial_= 0.;
    tempsFinal_= 1.;
    pasTemps_= 1.;
    critereArret_= EnumMetierCritereArret.NB_PAS_TEMPS;
    nbPasTemps_= 1;
    pasTempsVariable_= false;
    nbCourant_= 0.8;
    coteMax_=0;
    biefControle_=1;
    abscisseControle_=0;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    tempsInitial_= 0.;
    tempsFinal_= 0.;
    pasTemps_= 0.;
    critereArret_= null;
    nbPasTemps_= 0;
    pasTempsVariable_= false;
    nbCourant_= 0.;
    coteMax_=0;
    biefControle_=0;
    abscisseControle_=0;
    super.dispose();
  }
  // Attributs
  private double tempsInitial_;
  public double tempsInitial() {
    return tempsInitial_;
  }
  public void tempsInitial(double s) {
    if (tempsInitial_==s) return;
    tempsInitial_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "tempsInitial");
  }
  private double tempsFinal_;
  public double tempsFinal() {
    return tempsFinal_;
  }
  public void tempsFinal(double s) {
    if (tempsFinal_==s) return;
    tempsFinal_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "tempsFinal");
  }
  private double pasTemps_;
  public double pasTemps() {
    return pasTemps_;
  }
  public void pasTemps(double s) {
    if (pasTemps_==s) return;
    pasTemps_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pasTemps");
  }
  private double pasTempsImpression_;
  public double pasTempsImpression() {
    return pasTempsImpression_;
  }
  public void pasTempsImpression(double s) {
    if (pasTempsImpression_==s) return;
    pasTempsImpression_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "pasTempsImpression");
  }
  private EnumMetierCritereArret critereArret_;
  public EnumMetierCritereArret critereArret() {
    return critereArret_;
  }
  public void critereArret(EnumMetierCritereArret s) {
    if (critereArret_.value()==s.value()) return;
    critereArret_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "critereArret");
  }
  private int nbPasTemps_;
  public int nbPasTemps() {
    return nbPasTemps_;
  }
  public void nbPasTemps(int s) {
    if (nbPasTemps_==s) return;
    nbPasTemps_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nbPasTemps");
  }
  private boolean pasTempsVariable_;
  public boolean pasTempsVariable() {
    return pasTempsVariable_;
  }
  public void pasTempsVariable(boolean s) {
    if (pasTempsVariable_==s) return;
    pasTempsVariable_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pasTempsVariable");
  }

  private double nbCourant_;
  public double nbCourant() {
    return nbCourant_;
  }
  public void nbCourant(double s) {
    if (nbCourant_==s) return;
    nbCourant_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nbCourant");
  }


  private double coteMax_;
    public void coteMax(double coteMax) {
        if (coteMax_==coteMax) return;
        coteMax_ = coteMax;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteMax");
    }
  public double coteMax() {
    return coteMax_;
  }

  private int biefControle_;
  public void biefControle(int biefControle) {
    if (biefControle_==biefControle) return;
       biefControle_ = biefControle;
       Notifieur.getNotifieur().fireObjetModifie(toString(), this, "biefControle");
  }
  public int biefControle() {
     return biefControle_;
  }

  private double abscisseControle_;
   public void abscisseControle(double abscisseControle) {
        if (abscisseControle_==abscisseControle) return;
       abscisseControle_ = abscisseControle;
       Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisseControle");
   }
   public double abscisseControle() {
    return abscisseControle_;
   }
}
