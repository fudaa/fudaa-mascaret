/**
 * @file         DProfil.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Comparator;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
/**
 * Impl�mentation de l'objet m�tier des "profils" d'un bief.
 * G�re un tableau des points (X,Y) du profil, une abscisse curviligne du bief, un nom,
 * les limites pour le lit majeur droit et gauche et pour le lit mineur droit et gauche.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:27 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierProfil extends MetierHydraulique1d {
  public final static ProfilComparator COMPARATOR= new ProfilComparator();
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierProfil) {
      MetierProfil q= (MetierProfil)_o;
      nom(q.nom());
      abscisse(q.abscisse());
      points((MetierPoint2D[])q.points().clone());
      indiceLitMinDr(q.indiceLitMinDr());
      indiceLitMinGa(q.indiceLitMinGa());
      indiceLitMajDr(q.indiceLitMajDr());
      indiceLitMajGa(q.indiceLitMajGa());
      avecGeoreferencement(q.avecGeoreferencement());
      infosGeoReferencement(q.infosGeoReferencement());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierProfil p= new MetierProfil();
    p.abscisse(abscisse_+0.01);
    p.nom(nom_ + "bis");
    MetierPoint2D[] copie= new MetierPoint2D[points_.length];
    for (int i= 0; i < copie.length; i++) {
      MetierPoint2D pt= points_[i];
      copie[i]= new MetierPoint2D(pt.x, pt.y,pt.cx,pt.cy);
    }
    p.points(copie);
    p.indiceLitMajGa(indiceLitMajGa_);
    p.indiceLitMinGa(indiceLitMinGa_);
    p.indiceLitMinDr(indiceLitMinDr_);
    p.indiceLitMajDr(indiceLitMajDr_);
    p.avecGeoreferencement(avecGeoreferencement_);
    p.infosGeoReferencement(infosGeoReferencement_);
    return p;
  }
  @Override
  final public String toString() {
    String s= "profil " + numero_;
    return s;
  }
  /*** DProfil ***/
  // constructeurs
  public MetierProfil() {
    super();
    numero_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    nom_= "Profil " + numero_;
    abscisse_= 0.;
    points_= new MetierPoint2D[0];
    indiceLitMinDr_= 0;
    indiceLitMinGa_= 0;
    indiceLitMajDr_= 0;
    indiceLitMajGa_= 0;
    avecGeoreferencement_=false;
    infosGeoReferencement_="";
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    abscisse_= 0.;
    points_= null;
    indiceLitMinDr_= -1;
    indiceLitMinGa_= -1;
    indiceLitMajDr_= -1;
    indiceLitMajGa_= -1;
    avecGeoreferencement_=false;
    infosGeoReferencement_="";
    super.dispose();
  }
  // attributs
  private int numero_;
  public int numero() {
    return numero_;
  }
  public void numero(int s) {
    if (numero_==s) return;
    numero_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numero");
  }
  private String nom_;
  public String nom() {
    return nom_;
  }
  public void nom(String s) {
    if (nom_.equals(s)) return;
    nom_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nom");
  }
  private double abscisse_;
  public double abscisse() {
    return abscisse_;
  }
  public void abscisse(double s) {
    if (abscisse_==s) return;
    abscisse_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisse");
  }
  
  private boolean avecGeoreferencement_;
  public boolean avecGeoreferencement() {
    return avecGeoreferencement_;
  }
  public void avecGeoreferencement(boolean b) {
    if (avecGeoreferencement_==b) return;
    avecGeoreferencement_= b;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "avecGeoreferencement");
  }
  
  private String infosGeoReferencement_;
  public String infosGeoReferencement() {
    return infosGeoReferencement_;
  }
  public void infosGeoReferencement(String s) {
    if (infosGeoReferencement_==s) return;
    infosGeoReferencement_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "infosGeoReferencement");
  }
  private MetierPoint2D[] points_;
  public MetierPoint2D[] points() {
    return points_;
  }
  public void points(MetierPoint2D[] s) {
    if (egale(points_, s)) return;
    points_= s;
    if (s != null)
      if (s.length <= 1)
        indiceLitMinDr(0);
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "points");
  }
  private int indiceLitMinDr_;
  public int indiceLitMinDr() {
    if (points_.length == 0)
      indiceLitMinDr(-1);
    if ((indiceLitMinDr_ == -1) && (points_.length != 0))
      indiceLitMinDr(0);
    return indiceLitMinDr_;
  }
  public void indiceLitMinDr(int s) {
    if (points_.length == 0) {
      indiceLitMinDr_= -1;
      indiceLitMajDr_= -1;
      indiceLitMinGa_= -1;
      indiceLitMajGa_= -1;
    } else if (points_.length == 1) {
      indiceLitMinDr_= 0;
      indiceLitMajDr_= 0;
      indiceLitMinGa_= 0;
      indiceLitMajGa_= 0;
    } else {
      if (s >= points_.length)
        s= points_.length - 1;
      if (s < 0)
        s= 0;
      if (s < indiceLitMinGa_)
        indiceLitMinGa(s);
      if (s < indiceLitMajGa_)
        indiceLitMajGa(s);
      if (s > indiceLitMajDr_)
        indiceLitMajDr(s);
      if (indiceLitMinDr_==s) return;
      indiceLitMinDr_= s;
    }
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "indiceLitMinDr");
  }
  private int indiceLitMinGa_;
  public int indiceLitMinGa() {
    if (points_.length == 0)
      indiceLitMinGa(-1);
    if ((indiceLitMinGa_ == -1) && (points_.length != 0))
      indiceLitMinGa(0);
    return indiceLitMinGa_;
  }
  public void indiceLitMinGa(int s) {
    if (points_.length == 0) {
      indiceLitMinDr_= -1;
      indiceLitMajDr_= -1;
      indiceLitMinGa_= -1;
      indiceLitMajGa_= -1;
    } else if (points_.length == 1) {
      indiceLitMinDr_= 0;
      indiceLitMajDr_= 0;
      indiceLitMinGa_= 0;
      indiceLitMajGa_= 0;
    } else {
      if (s >= points_.length)
        s= points_.length - 1;
      if (s < 0)
        s= 0;
      if (s > indiceLitMinDr_)
        indiceLitMinDr(s);
      if (s > indiceLitMajDr_)
        indiceLitMajDr(s);
      if (s < indiceLitMajGa_)
        indiceLitMajGa(s);
      if (indiceLitMinGa_==s) return;
      indiceLitMinGa_= s;
    }
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "indiceLitMinGa");
  }
  private int indiceLitMajDr_;
  public int indiceLitMajDr() {
    if (points_.length == 0)
      indiceLitMajDr(-1);
    if ((indiceLitMajDr_ == -1) && (points_.length != 0))
      indiceLitMajDr(0);
    return indiceLitMajDr_;
  }
  public void indiceLitMajDr(int s) {
    if (points_.length == 0) {
      indiceLitMinDr_= -1;
      indiceLitMajDr_= -1;
      indiceLitMinGa_= -1;
      indiceLitMajGa_= -1;
    } else if (points_.length == 1) {
      indiceLitMinDr_= 0;
      indiceLitMajDr_= 0;
      indiceLitMinGa_= 0;
      indiceLitMajGa_= 0;
    } else {
      if (s >= points_.length)
        s= points_.length - 1;
      if (s < 0)
        s= 0;
      if (s < indiceLitMinGa_)
        indiceLitMinGa(s);
      if (s < indiceLitMajGa_)
        indiceLitMajGa(s);
      if (s < indiceLitMinDr_)
        indiceLitMinDr(s);
      if (indiceLitMajDr_==s) return;
      indiceLitMajDr_= s;
    }
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "indiceLitMajDr");
  }
  private int indiceLitMajGa_;
  public int indiceLitMajGa() {
    if (points_.length == 0)
      indiceLitMajGa(-1);
    if ((indiceLitMajGa_ == -1) && (points_.length != 0))
      indiceLitMajGa(0);
    return indiceLitMajGa_;
  }
  public void indiceLitMajGa(int s) {
    if (points_.length == 0) {
      indiceLitMinDr_= -1;
      indiceLitMajDr_= -1;
      indiceLitMinGa_= -1;
      indiceLitMajGa_= -1;
    } else if (points_.length == 1) {
      indiceLitMinDr_= 0;
      indiceLitMajDr_= 0;
      indiceLitMinGa_= 0;
      indiceLitMajGa_= 0;
    } else {
      if (s >= points_.length)
        s= points_.length - 1;
      if (s < 0)
        s= 0;

      if (s > indiceLitMinGa_)
        indiceLitMinGa(s);
      if (s > indiceLitMajDr_)
        indiceLitMajDr(s);
      if (s > indiceLitMinDr_)
        indiceLitMinDr(s);

      if (indiceLitMajGa_==s) return;
      indiceLitMajGa_= s;
    }
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "indiceLitMajGa");
  }
  // methodes
  public void indicModifPoint(int indice) {
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "points" + indice);
  }
  public void coteMinMax(
    org.omg.CORBA.DoubleHolder coteMin,
    org.omg.CORBA.DoubleHolder coteMax) {
    double zMin= Double.MAX_VALUE;
    double zMax= Double.NEGATIVE_INFINITY;
    for (int i= 0; i < points_.length; i++) {
      zMin= Math.min(zMin, points_[i].y);
      zMax= Math.max(zMax, points_[i].y);
    }
    coteMin.value= zMin;
    coteMax.value= zMax;
  }
  public double getFond() {
    double profLitI= Double.MAX_VALUE;
    for (int z= 0; z < points_.length; z++) {
      if (points_[z].y < profLitI)
        profLitI= points_[z].y;
    }
    return profLitI;
  }
  public void creePoint(int indice) {
    MetierPoint2D[] newpts= new MetierPoint2D[points_.length + 1];
    if (indice < 0)
      indice= 0;
    if (indice >= points_.length)
      indice= points_.length;
    for (int i= 0; i < indice; i++)
      newpts[i]= points_[i];
    MetierPoint2D ptCree= new MetierPoint2D();
    try {
      ptCree.x= points_[indice - 1].x;
      ptCree.y= points_[indice - 1].y;
      ptCree.x= (points_[indice - 1].x + points_[indice].x) / 2;
      ptCree.y= (points_[indice - 1].y + points_[indice].y) / 2;
    } catch (ArrayIndexOutOfBoundsException ex) {}
    newpts[indice]= ptCree;
    //System.err.println("Point "+(indice)+" ins�r�");
    for (int i= indice + 1; i < newpts.length; i++)
      newpts[i]= points_[i - 1];
    points(newpts);
    if (points_.length == 0) {
      indiceLitMinGa(-1);
      indiceLitMinDr(-1);
      indiceLitMajGa(-1);
      indiceLitMajDr(-1);
    } else if (points_.length == 1) {
      indiceLitMinGa(0);
      indiceLitMinDr(0);
      indiceLitMajGa(0);
      indiceLitMajDr(0);
    } else {
      if (indice <= indiceLitMinDr_) {
        indiceLitMinDr(indiceLitMinDr_ + 1);
      }
      if (indice <= indiceLitMajDr_) {
        indiceLitMajDr(indiceLitMajDr_ + 1);
      }
      if (indice <= indiceLitMinGa_) {
        indiceLitMinGa(indiceLitMinGa_ + 1);
      }
      if (indice <= indiceLitMajGa_) {
        indiceLitMajGa(indiceLitMajGa_ + 1);
      }
    }
  }
  public void supprimePoint(int indice) {
    if ((indice < 0) || (indice >= points_.length))
      return;
    MetierPoint2D[] newpts= new MetierPoint2D[points_.length - 1];
    for (int i= 0; i < indice; i++)
      newpts[i]= points_[i];
    //System.err.println("Point "+indice+" supprim�");
    for (int i= indice + 1; i < points_.length; i++)
      newpts[i - 1]= points_[i];
    points(newpts);
    if (points_.length == 0) { // dernier point enleve
      indiceLitMinGa(-1);
      indiceLitMinDr(-1);
      indiceLitMajGa(-1);
      indiceLitMajDr(-1);
      //System.err.println("Dernier point supprim�");
    } else if (points_.length == 1) {
      indiceLitMinGa(0);
      indiceLitMinDr(0);
      indiceLitMajGa(0);
      indiceLitMajDr(0);
    } else {
      if (indice <= indiceLitMinDr()) {
        indiceLitMinDr(indiceLitMinDr_ - 1);
      }
      if (indice <= indiceLitMajDr()) {
        indiceLitMajDr(indiceLitMajDr_ - 1);
      }
      if (indice <= indiceLitMinGa()) {
        indiceLitMinGa(indiceLitMinGa_ - 1);
      }
      if (indice <= indiceLitMajGa()) {
        indiceLitMajGa(indiceLitMajGa_ - 1);
      }
    }
  }
  public String description() {
    return nom() + " X=" + abscisse();
  }
  public boolean hasZoneStockage() {
    if ((indiceLitMajGa() == 0) || (indiceLitMajGa() == points().length - 1)) {
      if ((indiceLitMajDr() == 0)
        || (indiceLitMajDr() == points().length - 1)) {} else
        return true;
    } else
      return true;
    return false;
  }
  private final static boolean egale(MetierPoint2D[] p, MetierPoint2D[] p2) {
    if (p==p2) return true;
    if (p==null || p2==null) return false;

    int length = p.length;
    if (p2.length != length) return false;

    for (int i=0; i<length; i++)
        if (!((p[i].x==p2[i].x)&&(p[i].y==p2[i].y) &&(p[i].cx==p2[i].cx)&&(p[i].cy==p2[i].cy)))
            return false;

    return true;
  }
}
class ProfilComparator implements Comparator {
  ProfilComparator() {}
  @Override
  public int compare(Object o1, Object o2) {
    double xo1= ((MetierProfil)o1).abscisse();
    double xo2= ((MetierProfil)o2).abscisse();
    return xo1 < xo2 ? -1 : xo1 == xo2 ? 0 : 1;
  }
  @Override
  public boolean equals(Object obj) {
    return false;
  }
}
