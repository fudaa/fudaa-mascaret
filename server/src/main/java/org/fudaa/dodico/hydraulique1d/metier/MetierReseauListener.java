/*
 GPL 2
 */
package org.fudaa.dodico.hydraulique1d.metier;

import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEventListener;

/**
 *
 * @author Frederic Deniger
 */
public class MetierReseauListener {

  private final MetierEtude1d etude1d;

  MetierReseauListener(MetierEtude1d etude1d) {
    this.etude1d = etude1d;
  }

  protected void updateCasier() {
    if (etude1d.paramGeneraux().parametresCasier().activation() && etude1d.reseau().hasNoCasier()) {
      etude1d.paramGeneraux().parametresCasier().activation(false);

    }
  }

}
