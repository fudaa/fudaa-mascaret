/**
 * @file         DResultatsBief.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Implémentation de l'objet métier "résultats du bief".
 * Pas utilisé dans Fudaa-Mascaret.
 * @version      $Id: MetierResultatsBief.java,v 1.2 2007-11-20 11:42:31 bmarchan Exp $
 * @author       Axel von Arnim
 */
public class MetierResultatsBief extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierResultatsBief) {
      MetierResultatsBief q= (MetierResultatsBief)_o;
      if (q.pasTemps() != null) {
        MetierResultatsBiefPasTemps[] ip=
          new MetierResultatsBiefPasTemps[q.pasTemps().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (MetierResultatsBiefPasTemps)q.pasTemps()[i].creeClone();
        pasTemps(ip);
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierResultatsBief p= new MetierResultatsBief();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "resultatsBief";
    return s;
  }
  /*** DResultatsBief ***/
  // constructeurs
  public MetierResultatsBief() {
    super();
    pasTemps_= new MetierResultatsBiefPasTemps[0];
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    pasTemps_= null;
    super.dispose();
  }
  // Attributs
  private MetierResultatsBiefPasTemps[] pasTemps_;
  public MetierResultatsBiefPasTemps[] pasTemps() {
    return pasTemps_;
  }
  public void pasTemps(MetierResultatsBiefPasTemps[] s) {
    if (egale(pasTemps_, s)) return;
    pasTemps_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pasTemps");
  }
}
