/**
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Implémentation de l'objet métier "résultats du bief par pas de temps".
 * Pas utilisé dans Fudaa-Mascaret.
 * @version      $Id: MetierResultatsBiefPasTemps.java,v 1.2 2007-11-20 11:42:30 bmarchan Exp $
 * @author       Axel von Arnim
 */
public class MetierResultatsBiefPasTemps extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierResultatsBiefPasTemps) {
      MetierResultatsBiefPasTemps q=(MetierResultatsBiefPasTemps)_o;
      if (q.infoTemps() != null)
        infoTemps((MetierInformationTemps)q.infoTemps().creeClone());
      volum(q.volum());
      volums(q.volums());
      vbief(q.vbief());
      vappo(q.vappo());
      vperdu(q.vperdu());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierResultatsBiefPasTemps p=
      new MetierResultatsBiefPasTemps();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "resultatsBief ";
    if (infoTemps_ != null)
      s += "pas " + infoTemps_.toString();
    else
      s += "?";
    return s;
  }
  /*** DResultatsBiefPasTemps ***/
  // constructeurs
  public MetierResultatsBiefPasTemps() {
    super();
    infoTemps_= null;
    volum_= 0.;
    volums_= 0.;
    vbief_= 0.;
    vappo_= 0.;
    vperdu_= 0.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    infoTemps_= null;
    volum_= 0.;
    volums_= 0.;
    vbief_= 0.;
    vappo_= 0.;
    vperdu_= 0.;
    super.dispose();
  }
  // Attributs
  private MetierInformationTemps infoTemps_;
  public MetierInformationTemps infoTemps() {
    return infoTemps_;
  }
  public void infoTemps(MetierInformationTemps s) {
    if (infoTemps_==s) return;
    infoTemps_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "infoTemps");
  }
  private double volum_;
  public double volum() {
    return volum_;
  }
  public void volum(double s) {
    if (volum_==s) return;
    volum_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "volum");
  }
  private double volums_;
  public double volums() {
    return volums_;
  }
  public void volums(double s) {
    if (volums_==s) return;
    volums_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "volums");
  }
  private double vbief_;
  public double vbief() {
    return vbief_;
  }
  public void vbief(double s) {
    if (vbief_==s) return;
    vbief_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "vbief");
  }
  private double vappo_;
  public double vappo() {
    return vappo_;
  }
  public void vappo(double s) {
    if (vappo_==s) return;
    vappo_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "vappo");
  }
  private double vperdu_;
  public double vperdu() {
    return vperdu_;
  }
  public void vperdu(double s) {
    if (vperdu_==s) return;
    vperdu_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "vperdu");
  }
}
