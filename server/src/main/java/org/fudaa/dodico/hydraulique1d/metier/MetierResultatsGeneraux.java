/**
 * @file         DResultatsGeneraux.java
 * @creation     2000-08-10
 * @modification $Date: 2008-02-29 16:47:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */

package org.fudaa.dodico.hydraulique1d.metier;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/**
 * Impl�mentation de l'objet m�tier "r�sultats g�n�raux" de l'�tude.
 * Contient tous les r�sultats d'une �tude.
 * Les fichiers listing et messages sont stock�s dans des tableaux d'octets.
 * Les r�sultats Rubens sont �galement stock�s dans des tableaux d'octets, ils existent
 * encore pour des raisons de compatibilit� anscendante lors de la d�s�rialisation
 * de vieux fichiers '.masc'. Maintenant, quelquesoit le format de fichier d'origine,
 * on stocke les r�sultats dans des objets MetierResultatsTemporelSpatial.
 *
 * @version      $Revision: 1.3 $ $Date: 2008-02-29 16:47:07 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */

public class MetierResultatsGeneraux extends MetierHydraulique1d {

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierResultatsGeneraux) {
      MetierResultatsGeneraux q= (MetierResultatsGeneraux)_o;
      listing(q.listing());
      resultatReprise(q.resultatReprise());
//      listingDamocles(q.listingDamocles());
      listingCasier(q.listingCasier());
      listingLiaison(q.listingLiaison());
      listingTracer(q.listingTracer());
      avertissements(q.avertissements());
      messagesEcran(q.messagesEcran());
      messagesEcranErreur(q.messagesEcranErreur());
      resultatsRubens(q.resultatsRubens());
      resultatsRubensCasier(q.resultatsRubensCasier());
      resultatsRubensLiaison(q.resultatsRubensLiaison());
      if (q.resultatsTemporelSpatial()!=null)
        resultatsTemporelSpatial(q.resultatsTemporelSpatial());
      resultatsTemporelCasier(q.resultatsTemporelCasier());
      resultatsTemporelLiaison(q.resultatsTemporelLiaison());
      resultatsTemporelTracer(q.resultatsTemporelTracer());
      //resultatsRubensTracer(q.resultatsRubensTracer());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierResultatsGeneraux p=
      new MetierResultatsGeneraux();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "resultatsGeneraux";
    return s;
  }
  /*** DResultatsGeneraux ***/
  // constructeurs
  public MetierResultatsGeneraux() {
    super();
    listing_= null;
    resultatReprise_= new MetierParametresReprise();
    avertissements_=null;
    messagesEcran_= null;
    messagesEcranErreur_= null;
//    listingDamocles_= null;
    listingCasier_= null;
    listingLiaison_= null;
    listingTracer_= null;
    resultatsRubens_= null;
    resultatsRubensCasier_= null;
    resultatsRubensLiaison_= null;
    resultatsTemporelSpatial_= null;
//      new MetierResultatsTemporelSpatial();
    resultatsTemporelCasier_=
      new MetierResultatsTemporelSpatial();
    resultatsTemporelLiaison_=
      new MetierResultatsTemporelSpatial();
    resultatsTemporelTracer_=
      new MetierResultatsTemporelSpatial();
    //resultatsRubensTracer_= null;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    listing_= null;
    listingCasier_= null;
    listingLiaison_= null;
    listingTracer_= null;
    if (resultatReprise_ != null) {
      resultatReprise_.dispose();
      resultatReprise_= null;
    }
    avertissements_=null;
    messagesEcran_= null;
    messagesEcranErreur_= null;
//    listingDamocles_= null;
    if (resultatsTemporelSpatial_ != null) {
      resultatsTemporelSpatial_.dispose();
      resultatsTemporelSpatial_= null;
    }
    if (resultatsTemporelCasier_ != null) {
      resultatsTemporelCasier_.dispose();
      resultatsTemporelCasier_= null;
    }
    if (resultatsTemporelLiaison_ != null) {
      resultatsTemporelLiaison_.dispose();
      resultatsTemporelLiaison_= null;
    }
    if (resultatsTemporelTracer_ != null) {
        resultatsTemporelTracer_.dispose();
        resultatsTemporelTracer_ = null;
    }
    resultatsRubens_= null;
    resultatsRubensCasier_= null;
    resultatsRubensLiaison_= null;
    //resultatsRubensTracer_=null;

    super.dispose();
  }
  // Attributs
  private byte[] listingCasier_;
  public byte[] listingCasier() {
    return listingCasier_;
  }
  public void listingCasier(byte[] s) {
    listingCasier_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "listingCasier");
  }
  private byte[] listingLiaison_;
  public byte[] listingLiaison() {
    return listingLiaison_;
  }
  public void listingLiaison(byte[] s) {
    listingLiaison_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "listingLiaison");
  }
  private byte[] listingTracer_;
  public byte[] listingTracer() {
    return listingTracer_;
  }
  public void listingTracer(byte[] s) {
    listingTracer_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "listingTracer");
  }

  private byte[] listing_;
  public byte[] listing() {
    return listing_;
  }
  public void listing(byte[] s) {
    listing_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "listing");
  }
  private MetierParametresReprise resultatReprise_;
  public MetierParametresReprise resultatReprise() {
    return resultatReprise_;
  }
  public void resultatReprise(MetierParametresReprise resultatReprise) {
    if (resultatReprise_==resultatReprise) return;
    resultatReprise_= resultatReprise;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatReprise");
  }
//  private byte[] listingDamocles_;
//  public byte[] listingDamocles() {
//    return listingDamocles_;
//  }
//  public void listingDamocles(byte[] s) {
//    listingDamocles_= s;
//    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "listingDamocles");
//  }
  private String avertissements_;
  public String avertissements() {
	  System.err.println("GETAVERTISSEMENTS ="+avertissements_);
    return avertissements_;
  }
  public void avertissements(String s) {
	  avertissements_= s;
	  System.err.println("AVERTISSEMENTS ="+s);
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "Avertissements");
  }
  
  private byte[] messagesEcran_;
  public byte[] messagesEcran() {
    return messagesEcran_;
  }
  public void messagesEcran(byte[] s) {
    messagesEcran_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "messagesEcran");
  }
  private byte[] messagesEcranErreur_;
  public byte[] messagesEcranErreur() {
    return messagesEcranErreur_;
  }
  public void messagesEcranErreur(byte[] s) {
    messagesEcranErreur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "messagesEcranErreur");
  }
  private byte[] resultatsRubens_;
  public byte[] resultatsRubens() {
    return resultatsRubens_;
  }
  public void resultatsRubens(byte[] s) {
    resultatsRubens_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatsRubens");
  }
  private byte[] resultatsRubensCasier_;
  public byte[] resultatsRubensCasier() {
    return resultatsRubensCasier_;
  }
  public void resultatsRubensCasier(byte[] s) {
    resultatsRubensCasier_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "resultatsRubensCasier");
  }
  /*private byte[] resultatsRubensTracer_;
  public byte[] resultatsRubensTracer() {
    return resultatsRubensTracer_;
  }
  public void resultatsRubensTracer(byte[] s) {
    resultatsRubensTracer_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "resultatsRubensTracer");
  }*/
  private byte[] resultatsRubensLiaison_;
  public byte[] resultatsRubensLiaison() {
    return resultatsRubensLiaison_;
  }
  public void resultatsRubensLiaison(byte[] s) {
    resultatsRubensLiaison_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "resultatsRubensLiaison");
  }
 
  public boolean hasResultatsTemporelSpatial() {
    return resultatsTemporelSpatial_!=null;
  }
  
  private MetierResultatsTemporelSpatial resultatsTemporelSpatial_;
  public MetierResultatsTemporelSpatial resultatsTemporelSpatial() {
    return resultatsTemporelSpatial_;
  }
  public void resultatsTemporelSpatial(MetierResultatsTemporelSpatial r) {
    if (resultatsTemporelSpatial_==r) return;
    resultatsTemporelSpatial_= r;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "resultatsTemporelSpatial");
  }
  private MetierResultatsTemporelSpatial resultatsTemporelCasier_;
  public MetierResultatsTemporelSpatial resultatsTemporelCasier() {
    if (resultatsTemporelCasier_ != null) {
      resultatsTemporelCasier_.resultatsCasier(true);
      return resultatsTemporelCasier_;
    } else {
      return null;
    }
  }
  public void resultatsTemporelCasier(MetierResultatsTemporelSpatial r) {
    if (resultatsTemporelCasier_==r) return;
    resultatsTemporelCasier_= r;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "resultatsTemporelCasier");
  }
  private MetierResultatsTemporelSpatial resultatsTemporelLiaison_;
  public MetierResultatsTemporelSpatial resultatsTemporelLiaison() {
    if (resultatsTemporelLiaison_ != null) {
      resultatsTemporelLiaison_.resultatsLiaison(true);
      return resultatsTemporelLiaison_;
    } else {
      return null;
    }
  }
  public void resultatsTemporelLiaison(MetierResultatsTemporelSpatial r) {
    if (resultatsTemporelLiaison_==r) return;
    resultatsTemporelLiaison_= r;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "resultatsTemporelLiaison");
  }

  private MetierResultatsTemporelSpatial resultatsTemporelTracer_;
  public MetierResultatsTemporelSpatial resultatsTemporelTracer() {
      if (resultatsTemporelTracer_ != null) {
          resultatsTemporelTracer_.resultatsTracer(true);
          return resultatsTemporelTracer_;
      } else {
          return null;
      }
  }

  public void resultatsTemporelTracer(MetierResultatsTemporelSpatial r) {
      if (resultatsTemporelTracer_ == r)
          return;
      resultatsTemporelTracer_ = r;
      Notifieur.getNotifieur().fireObjetModifie(
              toString(),
              this,
              "resultatsTemporelTracer");
  }


}
