package org.fudaa.dodico.hydraulique1d.metier;

/**
 * Une interface permettrant de retourner pour un bief, un temps et une section
 * la valeur d'une variable.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public interface MetierResultatsTemporelSpacialI {

  /**
   * @param _var La variable
   * @param _ibief L'indice de bief
   * @param _itps L'indice du temps
   * @param _isect L'indice de la section
   * @return La valeur pour la variable. Double.NaN si la varaible n'est pas
   *         dans les résultats.
   */
  public abstract double getValue(MetierDescriptionVariable _var, int _ibief, int _itps, int _isect);

  /**
   * @return Le nombre de biefs pour le résultat
   */
  public abstract int getNbBiefs();

  /**
   * @return Le nombre pas de temps
   */
  public abstract int getNbTemps();

  /**
   * @param _ibief L'indice du bief
   * @return Le nombre de sections du bief considéré
   */
  public abstract int getNbSections(int _ibief);

}