/**
 * @file MetierResultatsTemporelSpatial.java
 * @creation 2001-10-01
 * @modification $Date: 2007-11-20 11:42:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;

import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/**
 * Impl�mentation des objets m�tiers "r�sultats temporels/iteratifs et spatial" des r�sultats g�n�raux. Contients la descriptions des variables, les
 * valeurs des diff�rents pas, les valeurs des variables par biefs et des indicateurs pr�cisant si c'est un r�sultat casier, liaison ou biefs.
 *
 * @version $Revision: 1.2 $ $Date: 2007-11-20 11:42:34 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class MetierResultatsTemporelSpatial extends MetierHydraulique1d implements MetierResultatsTemporelSpacialI {

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierResultatsTemporelSpatial) {
      MetierResultatsTemporelSpatial q = (MetierResultatsTemporelSpatial) (_o);
      if (q.resultatsBiefs() != null) {
        MetierResultatsTemporelSpatialBief[] ib =
                new MetierResultatsTemporelSpatialBief[q.resultatsBiefs().length];
        for (int i = 0; i < ib.length; i++) {
          ib[i] =
                  (MetierResultatsTemporelSpatialBief) (q.resultatsBiefs()[i].creeClone());
        }
        resultatsBiefs(ib);
      }
      if (q.valPas() != null) {
        valPas((double[]) q.valPas().clone());
      }
      if (q.descriptionVariables() != null) {
        MetierDescriptionVariable[] ip =
                new MetierDescriptionVariable[q.descriptionVariables().length];
        for (int i = 0; i < ip.length; i++) {
          ip[i] = (MetierDescriptionVariable) q.descriptionVariables()[i].creeClone();
        }
        descriptionVariables(ip);
      }
      resultatsCasier(q.resultatsCasier());
      resultatsLiaison(q.resultatsLiaison());
      resultatsPermanent(q.resultatsPermanent());
      resultatsTracer(q.resultatsTracer());
      resultatsCalageAuto(q.resultatsCalageAuto());
      setTemporel(q.isTemporel());
    }
  }

  @Override
  final public MetierHydraulique1d creeClone() {
    MetierResultatsTemporelSpatial p =
            new MetierResultatsTemporelSpatial();
    p.initialise(this);
    return p;
  }

  @Override
  final public String toString() {
    String s = "resultatsTemporelSpatial";
    return s;
  }

  /**
   * * DResultatsGeneraux **
   */
  // constructeurs
  public MetierResultatsTemporelSpatial() {
    super();
    pasTemps_ = new double[0];
    resultatsBiefs_ = new MetierResultatsTemporelSpatialBief[0];
    descriptionVariables_ = new MetierDescriptionVariable[0];
    resultatsCasier_ = false;
    resultatsLiaison_ = false;
    resultatsPermanent_ = false;
    resultatsTracer_ = false;
    resultatsCalageAuto_ = false;
    btemporel_ = true;

    notifieObjetCree();
  }

  @Override
  public void dispose() {
    pasTemps_ = null;
    if (resultatsBiefs_ != null) {
      for (int i = 0; i < resultatsBiefs_.length; i++) {
        resultatsBiefs_[i].dispose();
      }
      resultatsBiefs_ = null;
    }
    if (descriptionVariables_ != null) {
      for (int i = 0; i < descriptionVariables_.length; i++) {
        descriptionVariables_[i].dispose();
      }
      descriptionVariables_ = null;
    }
    super.dispose();
  }
  // Attributs
  private MetierDescriptionVariable[] descriptionVariables_;

  public MetierDescriptionVariable[] descriptionVariables() {
    return descriptionVariables_;
  }

  public void descriptionVariables(MetierDescriptionVariable[] s) {
    if (egale(descriptionVariables_, s)) {
      return;
    }
    descriptionVariables_ = s;
    Notifieur.getNotifieur().fireObjetModifie(
            toString(),
            this,
            "descriptionVariables");
  }
  private MetierResultatsTemporelSpatialBief[] resultatsBiefs_;

  public MetierResultatsTemporelSpatialBief[] resultatsBiefs() {
    return resultatsBiefs_;
  }

  public void resultatsBiefs(MetierResultatsTemporelSpatialBief[] resultatsBiefs) {
    if (egale(resultatsBiefs_, resultatsBiefs)) {
      return;
    }
    resultatsBiefs_ = resultatsBiefs;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatsBiefs");
  }
  private double[] pasTemps_;

  /**
   * @deprecated Remplac� par valPas()
   */
  public double[] pasTemps() {
    return valPas();
  }

  /**
   * @deprecated Remplac� par valPas(double[] _pas)
   */
  public void pasTemps(double[] _pas) {
    valPas(_pas);
  }
  private boolean resultatsLiaison_;

  public boolean resultatsLiaison() {
    return resultatsLiaison_;
  }

  public void resultatsLiaison(boolean resultatsLiaison) {
    if (resultatsLiaison_ == resultatsLiaison) {
      return;
    }
    resultatsLiaison_ = resultatsLiaison;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatsLiaison");
  }
  private boolean resultatsCasier_;

  public boolean resultatsCasier() {
    return resultatsCasier_;
  }

  public void resultatsCasier(boolean resultatsCasier) {
    if (resultatsCasier_ == resultatsCasier) {
      return;
    }
    resultatsCasier_ = resultatsCasier;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatsCasier");
  }
  private boolean resultatsPermanent_;

  public boolean resultatsPermanent() {
    return resultatsPermanent_;
  }

  public void resultatsPermanent(boolean resultatsPermanent) {
    if (resultatsPermanent_ == resultatsPermanent) {
      return;
    }
    resultatsPermanent_ = resultatsPermanent;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatsPermanent");
  }
  private boolean resultatsTracer_;

  public boolean resultatsTracer() {
    return resultatsTracer_;
  }

  public void resultatsTracer(boolean resultatsTracer) {
    if (resultatsTracer_ == resultatsTracer) {
      return;
    }
    resultatsTracer_ = resultatsTracer;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatsTracer");
  }
  private boolean resultatsCalageAuto_;

  public boolean resultatsCalageAuto() {
    return resultatsCalageAuto_;
  }

  public void resultatsCalageAuto(boolean resultatsCalageAuto) {
    if (resultatsCalageAuto_ == resultatsCalageAuto) {
      return;
    }
    resultatsCalageAuto_ = resultatsCalageAuto;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultatsCalageAuto");
  }

  /**
   * Retourne les valeurs des pas. Les valeurs des pas peuvent �tre des n� d'it�ration ou des valeurs de pas de temps.
   *
   * @return Les valeurs de pas.
   */
  public double[] valPas() {
    return pasTemps_;
  }

  /**
   * Affecte les valeurs des pas. Les valeurs des pas peuvent �tre des n� d'it�ration ou des valeurs de pas de temps.
   *
   * @param _pas Les valeurs de pas.
   */
  public void valPas(double[] _pas) {
    if (Arrays.equals(pasTemps_, _pas)) {
      return;
    }
    pasTemps_ = _pas;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "valPas");
  }
  private boolean btemporel_;

  /**
   * Retourne la nature des pas (Temporels ou it�ratifs).
   *
   * @return true : Temporels, false : It�ratifs
   */
  public boolean isTemporel() {
    return btemporel_;
  }

  /**
   * D�finit la nature des pas (Temporels ou it�ratifs).
   *
   * @param _b true : Temporels, false : It�ratifs
   */
  public void setTemporel(boolean _b) {
    btemporel_ = _b;
  }

  /**
   * end l'indice de colonne correspondant � ce nom de variable.
   *
   * @param _var : La variable
   * @return L'indice dans le tableau ou -1 si cette variable est introuvable.
   */
  public int getIndiceVariable(MetierDescriptionVariable _var) {
    return getIndiceVariable(_var.nom());
  }

  /**
   * end l'indice de colonne correspondant � ce nom de variable.
   *
   * @param _c : nom de la variable
   * @return L'indice dans le tableau ou -1 si cette variable est introuvable.
   */
  public int getIndiceVariable(String _c) {
    for (int i = 0; i < descriptionVariables_.length; i++) {
      if (descriptionVariables_[i].nom().equals(_c)) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Fusionne 2 MetierResultatsTemporelSpatial.
   *
   * @param MetierResultatsTemporelSpatial a et b
   */
  static public MetierResultatsTemporelSpatial fusionnerDResultatsTemporelSpatial(MetierResultatsTemporelSpatial a, MetierResultatsTemporelSpatial b) throws Exception {

    if ((a == null) && (b == null)) {
      return null;
    } else if (a == null || a.descriptionVariables() == null) {//TODO Correction Genesis
      return b;
    } else if (b == null || b.descriptionVariables() == null) {//TODO Correction Genesis
      return a;
    }

    MetierResultatsTemporelSpatial ires = new MetierResultatsTemporelSpatial();

    //descriptionVariables
    int descriptionVariablesLenghtA = a.descriptionVariables().length;
    int descriptionVariablesLenghtB = b.descriptionVariables().length;
    int descriptionVariablesLenght = descriptionVariablesLenghtA + descriptionVariablesLenghtB;
    MetierDescriptionVariable[] descrVar = new MetierDescriptionVariable[descriptionVariablesLenght];
    for (int i = 0; i < descriptionVariablesLenghtA; i++) {
      descrVar[i] = a.descriptionVariables()[i];
    }
    for (int i = 0; i < descriptionVariablesLenghtB; i++) {
      descrVar[descriptionVariablesLenghtA + i] = b.descriptionVariables()[i];
    }
    ires.descriptionVariables(descrVar);

    //pasTemps
    /*
     if (!Arrays.equals(a.pasTemps(), b.pasTemps())) {
      throw new Exception("Les 2 r�sultats n'ont pas les m�mes pas de temps ! Fusion impossible");
    }
    ires.pasTemps(a.pasTemps());
*/
    //valPas
    if (!Arrays.equals(a.valPas(), b.valPas())) {

    	if(a.valPas().length != b.valPas().length) 
    	throw new Exception(
              "Les 2 r�sultats n'ont pas les m�mes pas de temps ! Fusion impossible");
    	else {
    		//-- meme nombre mais pas la m�me valeur, on prend ceux qui ont le plus de probabilit� d'avoir de vrai valeur et  non des incr�mens --//
    		if(a.valPas().length>1 && a.valPas()[1] == a.valPas()[0]+1.0d)
    			ires.valPas(b.valPas());
    		else
    			ires.valPas(a.valPas());
    	}
    }else
    	ires.valPas(a.valPas());


    //resultatsBiefs

    if (a.resultatsBiefs().length != b.resultatsBiefs().length) {
      throw new Exception(
              "Les 2 r�sultats n'ont pas le m�me nombre de bief ! Fusion impossible");
    }
    MetierResultatsTemporelSpatialBief[] newResultatsBiefs = new MetierResultatsTemporelSpatialBief[a.resultatsBiefs().length];
    for (int i = 0; i < a.resultatsBiefs().length; i++) {
      MetierResultatsTemporelSpatialBief newBief = new MetierResultatsTemporelSpatialBief();
      MetierResultatsTemporelSpatialBief aBief = a.resultatsBiefs()[i];
      MetierResultatsTemporelSpatialBief bBief = b.resultatsBiefs()[i];

      if (!CtuluLibArray.equals(aBief.abscissesSections(),
              bBief.abscissesSections(), 0.5)) {
        throw new Exception(
                "Les 2 r�sultats n'ont pas les m�mes  sections de calcul pour le bief  "
                + (i + 1) + " ! Fusion impossible");
      }

      //On compare la precision (taille en String)pour conserver les section les plus precises
      if (CtuluLibArray.precision(aBief.abscissesSections()) > CtuluLibArray.precision(bBief.abscissesSections())) {
        newBief.abscissesSections(aBief.abscissesSections());
      } else {
        newBief.abscissesSections(bBief.abscissesSections());
      }
      double[][][] aBiefTab = aBief.valeursVariables();
      double[][][] bBiefTab = bBief.valeursVariables();
      double[][][] newBiefTab = new double[aBiefTab.length + bBiefTab.length][aBiefTab[0].length][aBiefTab[0][0].length];
      for (int tps = 0; tps < aBiefTab[0].length; tps++) {
        for (int absc = 0; absc < aBiefTab[0][0].length; absc++) {
          for (int var = 0; var < aBiefTab.length; var++) {
            newBiefTab[var][tps][absc] = aBiefTab[var][tps][absc];
          }
          for (int var = 0; var < bBiefTab.length; var++) {
            newBiefTab[aBiefTab.length + var][tps][absc] = bBiefTab[var][tps][absc];
          }
        }

      }
      newBief.valeursVariables(newBiefTab);
      newResultatsBiefs[i] = newBief;
    }
    ires.resultatsBiefs(newResultatsBiefs);

    if (a.resultatsCasier() || b.resultatsCasier()) {
      ires.resultatsCasier(true);
    } else {
      ires.resultatsCasier(false);
    }
    if (a.resultatsLiaison() || b.resultatsLiaison()) {
      ires.resultatsLiaison(true);
    } else {
      ires.resultatsLiaison(false);
    }
    if (a.resultatsPermanent() || b.resultatsPermanent()) {
      ires.resultatsPermanent(true);
    } else {
      ires.resultatsPermanent(false);
    }
    if (a.resultatsTracer() || b.resultatsTracer()) {
      ires.resultatsTracer(true);
    } else {
      ires.resultatsTracer(false);
    }
    if (a.resultatsCalageAuto() || b.resultatsCalageAuto()) {
      ires.resultatsCalageAuto(true);
    } else {
      ires.resultatsCalageAuto(false);
    }


    return ires;

  }
  
  /**
   * Ajoute les r�sultats pour une variable donn�e.
   * 
   * @param _var La variable pour laquelle on ajoute les valeurs.
   * @param _vals Un tableau � 3 dimensions [nbbief][nbtps][nbsect]. Le nombre de biefs, le nombre de sections par biefs, 
   * le nombre de temps doit �tre identique � l'existant.
   * @return True : Si l'ajout s'est correctement pass�. False si la variable existe d�j�
   * @exception IllegalArgumentException Si le tableau des valeurs pass�es est incoh�rent avec les r�sultats existants.
   */
  public boolean addResultsVariable(MetierDescriptionVariable _var, double[][][] _vals) {
    if (getIndiceVariable(_var.nom())!=-1) return false;
    
    // Controle que le nombre de valeurs est bien correspondant.
    if (resultatsBiefs_.length!=0) {
      if (resultatsBiefs_.length!=_vals.length) {
        throw new IllegalArgumentException();
      }
      else {
        for (int ibief=0; ibief<resultatsBiefs_.length; ibief++) {
          double[][][] vals=resultatsBiefs_[ibief].valeursVariables();
          if (vals[0].length!=_vals[ibief].length) {
            throw new IllegalArgumentException();
          }
          else {
            for (int itps=0; itps<vals[0].length; itps++) {
              if (vals[0][itps].length!=_vals[ibief][itps].length) {
                throw new IllegalArgumentException();
              }
            }
          }
        }
      }
    }
    
    // Ajout des r�sultats
    for (int ibief=0; ibief<resultatsBiefs_.length; ibief++) {
      double[][][] oldvals=resultatsBiefs_[ibief].valeursVariables();
      double[][][] newvals=(double[][][])ArrayUtils.add(oldvals, _vals[ibief]);
      
      resultatsBiefs_[ibief].valeursVariables(newvals);
    }

    // Ajout de la variable.
    // ATTENTION : Pour que Yapod fonctionne correctement, la variable est
    // copi�e avant d'�tre ajout�e.
    MetierDescriptionVariable[] vars = (MetierDescriptionVariable[]) ArrayUtils.add(descriptionVariables(), _var.creeClone());
    descriptionVariables(vars);
    
    return true;
  }
  
  /**
   * Supprime les r�sultats pour une variable donn�e.
   * @param _var La variable pour laquelle on supprime les valeurs.
   * 
   * @return True : Si la suppression a �t� r�alis�e.
   */
  public boolean removeResultsVariable(MetierDescriptionVariable _var) {
    int ind=getIndiceVariable(_var.nom());
    if (ind==-1) return false;
    
    // Suppression des r�sultats
    for (int ibief=0; ibief<resultatsBiefs_.length; ibief++) {
      double[][][] oldvals=resultatsBiefs_[ibief].valeursVariables();
      double[][][] newvals=(double[][][])ArrayUtils.remove(oldvals, ind);
      
      resultatsBiefs_[ibief].valeursVariables(newvals);
    }
    
    // Suppression de la variable.
    MetierDescriptionVariable[] vars=(MetierDescriptionVariable[])ArrayUtils.remove(descriptionVariables(),ind);
    descriptionVariables(vars);
    
    return true;
  }

  /**
   * 
   * @param _var Variable dont on veut r�cuperer les r�sultats.
   * @return Un tableau � 3 dimensions [nbbief][nbtps][nbsect], repr�sentant les
   *         r�sultats pour la variable en entr�e, ou null s'ils n'y a pas de
   *         r�sultats pour la variable.
   */
  public double[][][] getResultsVariable(MetierDescriptionVariable _var) {
    int ind=getIndiceVariable(_var.nom());
    if (ind==-1) return null;
    
    int nbbiefs=resultatsBiefs_.length;
    if (nbbiefs==0) return null;
    
    double[][][] vals=new double[nbbiefs][][];
    for (int ibief=0; ibief<nbbiefs; ibief++) {
      vals[ibief]=resultatsBiefs_[ibief].valeursVariable(ind);
    }
    
    return vals;
  }

  @Override
  public int getNbBiefs() {
    return resultatsBiefs_.length;
  }

  @Override
  public int getNbTemps() {
    return pasTemps_.length;
  }

  @Override
  public int getNbSections(int _ibief) {
    return resultatsBiefs_[_ibief].abscissesSections().length;
  }
  
  @Override
  public double getValue(MetierDescriptionVariable _var, int _ibief, int _itps, int _isect) {

    double[][][] vals = resultatsBiefs_[_ibief].valeursVariables();
    int indvar = getIndiceVariable(_var);
    if (indvar == -1) {
      return Double.NaN;
    }

    return vals[indvar][_itps][_isect];
  }
}
