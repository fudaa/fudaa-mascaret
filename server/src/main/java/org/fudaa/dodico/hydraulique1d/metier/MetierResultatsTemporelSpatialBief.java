/**
 * @file         DResultatsTemporelSpatialBief.java
 * @creation     2001-10-01
 * @modification $Date: 2008-03-07 14:01:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation des objets m�tiers "r�sultats temporels et spatial d'un bief" des r�sultats temporels et spatial.
 * Contients les valeurs des diff�rentes variables pour un bief ou un casier ou une liaison suivant l'indicateur
 * de l'objet contenaire MetierResultatsTemporelSpatial.
 * @version      $Revision: 1.4 $ $Date: 2008-03-07 14:01:59 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class MetierResultatsTemporelSpatialBief extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierResultatsTemporelSpatialBief) {
      MetierResultatsTemporelSpatialBief q= (MetierResultatsTemporelSpatialBief)(_o);
      if (q.abscissesSections() != null) {
        abscissesSections((double[])q.abscissesSections().clone());
      }
      if (q.valeursVariables() != null) {
        valeursVariables((double[][][])q.valeursVariables().clone());
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierResultatsTemporelSpatialBief p=
      new MetierResultatsTemporelSpatialBief();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "resultatsTemporelSpatialBief";
   /* s=s+"abscissesSections  : ";
    for (int i = 0; i < abscissesSections_.length; i++) {
		s=s+abscissesSections_[i]+" ";
	}
    s=s+"\nvaleursVariables : ";
    for (int j = 0; j < valeursVariables_.length; j++) {
		for (int k = 0; k < valeursVariables_[j].length; k++) {
			for (int l = 0; l < valeursVariables_[j][k].length; l++) {
				s=s+valeursVariables_[j][k][l]+" ";
			}
			s=s+"\n";
		}
		s=s+"\n";
	};*/
    return s;
  }
  /*** DResultatsGeneraux ***/
  // constructeurs
  public MetierResultatsTemporelSpatialBief() {
    super();
    abscissesSections_= new double[0];
    valeursVariables_= new double[0][0][0];
  }
  @Override
  public void dispose() {
    abscissesSections_= null;
    valeursVariables_= null;
    super.dispose();
  }
  // Attributs
  private double[] abscissesSections_;
  public double[] abscissesSections() {
    return abscissesSections_;
  }
  public void abscissesSections(double[] abscissesSections) {
    abscissesSections_= abscissesSections;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "abscissesSections");
  }
  private double[][][] valeursVariables_;

  /**
   * Retourne les valeurs des variables.
   * @return valeursVariables[iVariable][iPasTemps][iSectionCalcul]
   */
  public double[][][] valeursVariables() {
    return valeursVariables_;
  }
  
  /**
   * @param _indVar L'indice de la varaible dans la liste des variables.
   * @return Un tableau � 2 dimensions [nbpts][nbsect] representant les valeurs
   *         pour la variable.
   */
  public double[][] valeursVariable(int _indVar) {
    if (valeursVariables_ == null || _indVar < 0 || _indVar >= valeursVariables_.length)
      return null;

    return valeursVariables_[_indVar];
  }
  
  /**
   * Initialise les valeurs des variables.
   * @param valeursVariables valeursVariables[iVariable][iPasTemps][iSectionCalcul]
   */
  public void valeursVariables(double[][][] valeursVariables) {
    valeursVariables_= valeursVariables;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "valeursVariables");
  }
}
