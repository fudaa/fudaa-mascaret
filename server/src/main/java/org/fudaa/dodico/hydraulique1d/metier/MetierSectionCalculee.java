/**
 * @file         DSectionCalculee.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation des objets m�tiers "sections calcul�es" d'un bief.
 * Pas utilis� dans Fudaa-Mascaret.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:33 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierSectionCalculee extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSectionCalculee) {
      MetierSectionCalculee q= (MetierSectionCalculee)_o;
      abscisse(q.abscisse());
      if (q.pasTemps() != null) {
        MetierSectionCalculeePasTemps[] ip=
          new MetierSectionCalculeePasTemps[q.pasTemps().length];
        for (int i= 0; i < ip.length; i++)
          ip[i]= (MetierSectionCalculeePasTemps)q.pasTemps()[i].creeClone();
        pasTemps(ip);
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSectionCalculee p= new MetierSectionCalculee();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionCalculee abs " + abscisse_;
    return s;
  }
  /*** DSectionCalculee ***/
  // constructeurs
  public MetierSectionCalculee() {
    super();
    abscisse_= 0.;
    pasTemps_= new MetierSectionCalculeePasTemps[0];
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    abscisse_= 0.;
    pasTemps_= null;
    super.dispose();
  }
  // Attributs
  private double abscisse_;
  public double abscisse() {
    return abscisse_;
  }
  public void abscisse(double s) {
    if (abscisse_==s) return;
    abscisse_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisse");
  }
  private MetierSectionCalculeePasTemps[] pasTemps_;
  public MetierSectionCalculeePasTemps[] pasTemps() {
    return pasTemps_;
  }
  public void pasTemps(MetierSectionCalculeePasTemps[] s) {
    if (egale(pasTemps_,s)) return;
    pasTemps_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pasTemps");
  }
}
