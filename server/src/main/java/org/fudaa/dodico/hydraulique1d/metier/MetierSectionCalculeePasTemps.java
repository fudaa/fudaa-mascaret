/**
 * @file         DSectionCalculeePasTemps.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Arrays;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation des objets m�tiers "sections calcul�es des pas de temps"
 * des sections calcul�es d'un bief.
 * Pas utilis� dans Fudaa-Mascaret.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:28 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierSectionCalculeePasTemps extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSectionCalculeePasTemps) {
      MetierSectionCalculeePasTemps q= (MetierSectionCalculeePasTemps)_o;
      if (q.infoTemps() != null)
        infoTemps((MetierInformationTemps)q.infoTemps().creeClone());
      if (q.valeurs() != null)
        valeurs((double[])q.valeurs().clone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSectionCalculeePasTemps p=
      new MetierSectionCalculeePasTemps();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "sectionCalculee ";
    if (infoTemps_ != null)
      s += "pas " + infoTemps_.toString();
    else
      s += "?";
    return s;
  }
  /*** DSectionCalculeePasTemps ***/
  // constructeurs
  public MetierSectionCalculeePasTemps() {
    super();
    infoTemps_= null;
    valeurs_= new double[0];
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    infoTemps_= null;
    valeurs_= null;
    super.dispose();
  }
  // Attributs
  private MetierInformationTemps infoTemps_;
  public MetierInformationTemps infoTemps() {
    return infoTemps_;
  }
  public void infoTemps(MetierInformationTemps s) {
    if (infoTemps_==s) return;
    infoTemps_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "infoTemps");
  }
  private double[] valeurs_;
  public double[] valeurs() {
    return valeurs_;
  }
  public void valeurs(double[] s) {
    if (Arrays.equals(valeurs_, s)) return;
    valeurs_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "valeurs");
  }
}
