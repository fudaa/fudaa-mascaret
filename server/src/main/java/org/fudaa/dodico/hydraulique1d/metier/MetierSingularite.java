/**
 * @file         MetierSingularite.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:42:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierBarrage;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementZCoefQ;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilDenoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLimniAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilNoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAval;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilVanne;
/**
 * Impl�mentation des objets m�tiers "singularit�s" des biefs ou du r�seau hydraulique.
 * Abstraction de la notion de singularit�.
 * @see org.fudaa.dodico.hydraulique1d.metier.singularite
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:34 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public abstract class MetierSingularite extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierSingularite) {
      MetierSingularite q= (MetierSingularite)_o;
      numero(q.numero());
      nom(q.nom());
      abscisse(q.abscisse());
    }
  }
  @Override
  public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "singularit� " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  /*** MetierSingularite ***/
  // constructeurs
  public MetierSingularite() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre("singularite");
    numero_=id_;
    nom_= "singularite " + numero_;
    abscisse_= 0.;
  }
  @Override
  public void dispose() {
    id_=0;
    numero_=0;
    nom_= null;
    abscisse_= 0.;
    super.dispose();
  }
  // Attributs
  protected int id_;
  public int id() {
    return id_;
  }

  public void id(int s) {
    id_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "id");
  }
  protected int numero_;
  public int numero() {
    return numero_;
  }
  public void numero(int s) {
    if (numero_!= s){

        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numero");
        //Pour les anciennes versions (nom= Seuil 5)
        if (nom_.endsWith(" "+numero_)) nom(nom_.replaceAll(" "+numero_," "+s));
        //Pour les nouvelles versions (nom= Seuil-Singularit� n�5)
        nom(nom_.replaceAll("-Singularit� n�"+numero_,"-Singularit� n�"+s));
        numero_ = s;
    }
  }
  protected String nom_;
  public String nom() {
    return nom_;
  }
  public void nom(String s) {
    if (nom_.equals(s)) return;
    nom_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nom");
  }
  protected double abscisse_;
  public double abscisse() {
    return abscisse_;
  }
  public void abscisse(double s) {
    if (abscisse_==s) return;
    abscisse_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisse");
  }
  // Methode
  public MetierLoiHydraulique creeLoi() {
    return null;
  }
  public MetierLoiHydraulique getLoi() {
    return null;
  }
  public static void supprimeSingularite(MetierSingularite loi) {
    System.out.println("supprimeSingularite="+loi.getClass());
    if (loi instanceof MetierApport)
      loi.supprime();
    else if (loi instanceof MetierPerteCharge)
      loi.supprime();
    else if (loi instanceof MetierBarrage)
      loi.supprime();
    else if (loi instanceof MetierSeuilGeometrique)
      loi.supprime();
    else if (loi instanceof MetierSeuilLoi)
      loi.supprime();
    else if (loi instanceof MetierSeuilLimniAmont)
      loi.supprime();
    else if (loi instanceof MetierSeuilNoye)
      loi.supprime();
    else if (loi instanceof MetierSeuilDenoye)
      loi.supprime();
    else if (loi instanceof MetierSeuilTarageAmont)
      loi.supprime();
    else if (loi instanceof MetierSeuilTarageAval)
      loi.supprime();
    else if (loi instanceof MetierSeuilVanne)
      loi.supprime();
    else if (loi instanceof MetierDeversoirComportementLoi)
      loi.supprime();
    else if (loi instanceof MetierDeversoirComportementZCoefQ)
      loi.supprime();
    else {
      try {
        throw new RuntimeException(
          "MetierSingularite: aucun destructeur pour " + loi.getClass().getName());
      } catch (RuntimeException e) {
        e.printStackTrace();
      }
    }
  }
}
