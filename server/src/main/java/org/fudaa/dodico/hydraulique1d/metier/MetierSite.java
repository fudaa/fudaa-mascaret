/**
 * @file         MetierSite.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Implémentation des objets métiers "sites" des paramètres de stockage ou
 * des laisses de crue ou du barrage principal ou des définitions de sections.
 * Associe une abscisse et une référence vers un bief.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:34 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierSite extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierSite) {
      MetierSite q= (MetierSite)_o;
      abscisse(q.abscisse());
      if (q.biefRattache() != null)
        biefRattache(q.biefRattache());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSite p= new MetierSite();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    String s= "site (b";
    if (biefRattache_ != null)
      s += (biefRattache_.indice()+1);
    else
      s += "?";
    s += "," + abscisse_ + ")";
    return s;
  }
  /*** MetierSite ***/
  // constructeurs
  public MetierSite() {
    super();
    abscisse_= 0.;
    biefRattache_= null;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    abscisse_= 0.;
    biefRattache_= null;
    super.dispose();
  }
  // attributs
  private double abscisse_;
  public double abscisse() {
    return abscisse_;
  }
  public void abscisse(double t) {
    if (abscisse_==t) return;
    abscisse_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisse");
  }
  private MetierBief biefRattache_;
  public MetierBief biefRattache() {
    return biefRattache_;
  }
  public void biefRattache(MetierBief t) {
    if (biefRattache_==t) return;
    biefRattache_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "biefRattache");
  }
}
