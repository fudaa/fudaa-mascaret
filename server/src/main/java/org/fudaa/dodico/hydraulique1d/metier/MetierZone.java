/**
 * @file         MetierZone.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import java.util.Comparator;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.CGlobal;
/**
 * Implémentation des objets métiers "zones" des conditions initiales ou
 * des définitions de sections.
 * Associe une abscisse et une référence vers un bief.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:34 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class MetierZone extends MetierHydraulique1d {
  public final static ZoneComparator COMPARATOR= new ZoneComparator();
  /*** MetierHydraulique1d ***/
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierZone) {
      MetierZone q= (MetierZone)_o;
      abscisseDebut(q.abscisseDebut());
      abscisseFin(q.abscisseFin());
      if (q.biefRattache() != null){
//        biefRattache((MetierBief) q.biefRattache().creeClone());
        biefRattache((MetierBief) q.biefRattache());
      }
    }
  }
  @Override
  public MetierHydraulique1d creeClone() {
    MetierZone p= new MetierZone();
    p.initialise(this);
    return p;
  }
  @Override
  public String toString() {
    String s= "zone (b";
    if (biefRattache_ != null)
      s += (biefRattache_.indice()+1);
    else
      s += "?";
    s += "," + abscisseDebut_ + "-" + abscisseFin_ + ")";
    return s;
  }
  /*** MetierZone ***/
  // constructeurs
  protected MetierZone(boolean _notify) {
    super();
    abscisseDebut_= 0.;
    abscisseFin_= 0.;
    biefRattache_= null;

    if (_notify) notifieObjetCree();
  }

  public MetierZone() {
	this(true);
  }
  // destructeur
  @Override
  public void dispose() {
    abscisseDebut_= 0.;
    abscisseFin_= 0.;
    biefRattache_= null;
    super.dispose();
  }
  // attributs
  private double abscisseDebut_;
  public double abscisseDebut() {
    return abscisseDebut_;
  }
  public void abscisseDebut(double t) {
    if (abscisseDebut_==t) return;
    abscisseDebut_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisseDebut");
  }
  private double abscisseFin_;
  public double abscisseFin() {
    return abscisseFin_;
  }
  public void abscisseFin(double t) {
    if (abscisseFin_==t) return;
    abscisseFin_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisseFin");
  }
  private MetierBief biefRattache_;
  public MetierBief biefRattache() {
    return biefRattache_;
  }
  public void biefRattache(MetierBief t) {
    if (biefRattache_==t) return;
    biefRattache_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "biefRattache");
  }
  // methodes
  public boolean contientAbscisse(double abscisse) {
    return CGlobal.appartient(abscisse, abscisseDebut_, abscisseFin_);
  }
  public boolean appartientA(MetierZone[] zones) {
    for (int i= 0; i < zones.length; i++) {
      if (zones[i] == this)
        return true;
    }
    return false;
  }
}
class ZoneComparator implements Comparator {
  ZoneComparator() {}
  @Override
  public int compare(Object o1, Object o2) {
    double xo1= ((MetierZone)o1).abscisseDebut();
    double xo2= ((MetierZone)o2).abscisseDebut();
    return xo1 < xo2 ? -1 : xo1 == xo2 ? 0 : 1;
  }
  @Override
  public boolean equals(Object obj) {
    return false;
  }
}
