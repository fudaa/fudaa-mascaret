/**
 * @file         MetierZoneFrottement.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/**
*  Impl�mentation des objets m�tiers "zones de frottement".
 * Ajoute des coefficients Majeur et Mineur � une Zone.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:29 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */

public class MetierZoneFrottement extends MetierZone {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierZoneFrottement) {
      MetierZoneFrottement q= (MetierZoneFrottement)_o;
      super.initialise(q);
      coefMajeur(q.coefMajeur());
      coefMineur(q.coefMineur());
      coefLitMajBinf(q.coefLitMajBinf());
      coefLitMajBsup(q.coefLitMajBsup());
      coefLitMinBinf(q.coefLitMinBinf());
      coefLitMinBsup(q.coefLitMinBsup());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierZoneFrottement p= new MetierZoneFrottement();
    p.initialise(this);
    return p;
  }
  /*** MetierZoneFrottement ***/
  // constructeurs
  public MetierZoneFrottement() {
    super(false);
    coefMajeur_= 30.;
    coefMineur_= 40.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    coefMajeur_= 0.;
    coefMineur_= 0.;
    coefLitMinBinf_ = 0;
    coefLitMajBinf_ = 0;
    coefLitMajBsup_ = 0;
    coefLitMinBsup_ = 0;
    super.dispose();
  }
  // attributs
  private double coefMajeur_;
  public double coefMajeur() {
    return coefMajeur_;
  }
  public void coefMajeur(double t) {
    if (coefMajeur_==t) return;
    coefMajeur_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefMajeur");
  }
  private double coefMineur_;
  public double coefMineur() {
    return coefMineur_;
  }
  public void coefMineur(double t) {
    if (coefMineur_==t) return;
    coefMineur_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefMineur");
  }
  
  private double coefLitMinBinf_;
  public double coefLitMinBinf() {
    return coefLitMinBinf_;
  }
  public void coefLitMinBinf(double t) {
    if (coefLitMinBinf_==t) return;
    coefLitMinBinf_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefLitMinBinf");
  }
  
  private double coefLitMinBsup_;
  public double coefLitMinBsup() {
    return coefLitMinBsup_;
  }
  public void coefLitMinBsup(double t) {
    if (coefLitMinBsup_==t) return;
    coefLitMinBsup_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefLitMinBsup");
  }
  
  private double coefLitMajBinf_;
  public double coefLitMajBinf() {
    return coefLitMajBinf_;
  }
  public void coefLitMajBinf(double t) {
    if (coefLitMajBinf_==t) return;
    coefLitMajBinf_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefLitMajBinf");
  }
  
  private double coefLitMajBsup_;
  public double coefLitMajBsup() {
    return coefLitMajBsup_;
  }
  public void coefLitMajBsup(double t) {
    if (coefLitMajBsup_==t) return;
    coefLitMajBsup_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefLitMajBsup");
  }
  
}
