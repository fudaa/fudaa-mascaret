/*
 * @file         MetierZonePlanimetrage.java
 * @creation     2000-12-07
 * @modification $Date: 2007-11-20 11:42:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Impl�mentation des objets m�tiers "zones de planim�trage".
 * Ajoute une taille de Pas � une Zone.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:27 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierZonePlanimetrage extends MetierZone {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierZonePlanimetrage) {
      MetierZonePlanimetrage q= (MetierZonePlanimetrage)_o;
      super.initialise(q);
      taillePas(q.taillePas());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierZonePlanimetrage p=
      new MetierZonePlanimetrage();
    p.initialise(this);
    return p;
  }
  /*** MetierZonePlanimetrage ***/
  // constructeurs
  public MetierZonePlanimetrage() {
    super(false);
    taillePas_= 1.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    taillePas_= 0.;
    super.dispose();
  }
  // attributs
  private double taillePas_;
  public double taillePas() {
    return taillePas_;
  }
  public void taillePas(double t) {
    if (taillePas_==t) return;
    taillePas_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "taillePas");
  }
  
  /*public  String toString(){
	  return "MetierZonePlanimetrage bief="+this.biefRattache().numero()+" abs debut "+this.abscisseDebut()+" abs fin "+this.abscisseFin()+" taille pas "+this.taillePas();
  }*/
}
