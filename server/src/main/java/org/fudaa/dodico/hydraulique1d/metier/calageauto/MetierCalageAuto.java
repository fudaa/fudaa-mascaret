/*
 * @file         MetierCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.calageauto;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * Implémentation de l'objet racine du calage automatique "MetierCalageAuto".
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:26 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class MetierCalageAuto extends MetierHydraulique1d {
  private MetierParametresCalageAuto params_;
  private MetierZoneFrottement[]     zonesFrot_;
  private MetierCrueCalageAuto[]     crues_;
  private MetierResultatsCalageAuto  resultats_;

  // Constructeur.
  public MetierCalageAuto() {
    super();
    params_=new MetierParametresCalageAuto();
    zonesFrot_=new MetierZoneFrottement[0];
    crues_=new MetierCrueCalageAuto[0];
    resultats_=new MetierResultatsCalageAuto();

    notifieObjetCree();
  }

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierCalageAuto) {
      MetierCalageAuto q= (MetierCalageAuto)_o;
      parametres(q.parametres());
      zonesFrottement(q.zonesFrottement());
      crues(q.crues());
      resultats(q.resultats());
    }
  }

  @Override
  final public MetierHydraulique1d creeClone() {
    MetierCalageAuto p=new MetierCalageAuto();
    p.initialise(this);
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("CalageAuto");
    res[1]=super.getInfos()[1];

    return res;
  }

  @Override
  public void dispose() {
    if (params_!=null) params_.dispose();
    zonesFrot_=null;
    crues_=null;
    if (resultats_!=null) resultats_.dispose();
    super.dispose();
  }

  //---  Interface MetierCalageAuto {  ----------------------------------------------

  public MetierParametresCalageAuto parametres() {
    return params_;
  }

  public void parametres(MetierParametresCalageAuto _params) {
    if (params_==_params) return;
    params_=_params;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "params");
  }

  public MetierZoneFrottement[] zonesFrottement() {
    return zonesFrot_;
  }

  public void zonesFrottement(MetierZoneFrottement[] _zones) {
    if (zonesFrot_==_zones) return;
    zonesFrot_=_zones;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zonesFrottement");
  }

  public MetierCrueCalageAuto[] crues() {
    return crues_;
  }

  public void crues(MetierCrueCalageAuto[] _crues) {
    if (crues_==_crues) return;
    crues_=_crues;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "crues");
  }

  public MetierResultatsCalageAuto resultats() {
    return resultats_;
  }

  public void resultats(MetierResultatsCalageAuto _resultats) {
    if (resultats_==_resultats) return;
    resultats_=_resultats;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultats");
  }

  //---  } Interface MetierCalageAuto  ----------------------------------------------
}
