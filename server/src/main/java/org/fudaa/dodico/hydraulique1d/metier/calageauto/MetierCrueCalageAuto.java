/*
 * @file         MetierCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.calageauto;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * Implémentation de l'objet "MetierCrueCalageAuto" contenant la description d'une crue pour le calage automatique.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:25 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class MetierCrueCalageAuto extends MetierHydraulique1d {
  private double                   debitAmont_;
  private double                   coteAval_;
  private MetierApportCrueCalageAuto [] apports_;
  private MetierMesureCrueCalageAuto [] mesures_;

  // Constructeur.
  public MetierCrueCalageAuto() {
    super();
    debitAmont_=0;
    coteAval_=0;
    apports_=new MetierApportCrueCalageAuto[0];
    mesures_=new MetierMesureCrueCalageAuto[0];

    notifieObjetCree();
  }

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierCrueCalageAuto) {
      MetierCrueCalageAuto q=(MetierCrueCalageAuto)_o;
      debitAmont(q.debitAmont());
      coteAval(q.coteAval());
      apports(q.apports());
      mesures(q.mesures());
    }
  }

  @Override
  final public MetierHydraulique1d creeClone() {
    MetierCrueCalageAuto p= new MetierCrueCalageAuto();
    p.initialise(this);
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Crue");
    res[1]=
      super.getInfos()[1]
        + " "+getS("debitAmont")+" : "
        + debitAmont_
        + " "+getS("coteAval")+" : "
        + coteAval_
        + " "+getS("nbApports")+" : "
        + apports_.length
        + " "+getS("nbMesures")+" : "
        + mesures_.length;

    return res;
  }

  @Override
  public void dispose() {
    debitAmont_=0;
    coteAval_=0;
    apports_=null;
    mesures_=null;
    super.dispose();
  }

  //---  Interface MetierCrueCalageAuto {  ------------------------------------------

  public double debitAmont() {
    return debitAmont_;
  }

  public void debitAmont(double _d) {
    if (debitAmont_==_d) return;
    debitAmont_=_d;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "debitAmont");
  }

  public double coteAval() {
    return coteAval_;
  }

  public void coteAval(double _cote) {
    if (coteAval_==_cote) return;
    coteAval_=_cote;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteAval");
  }

  public MetierApportCrueCalageAuto[] apports() {
    return apports_;
  }

  public void apports(MetierApportCrueCalageAuto[] _apports) {
    if (apports_==_apports) return;
    apports_=_apports;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "apports");
  }

  public MetierMesureCrueCalageAuto[] mesures() {
    return mesures_;
  }

  public void mesures(MetierMesureCrueCalageAuto[] _mesures) {
    if (mesures_==_mesures) return;
    mesures_=_mesures;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "mesures");
  }

  //---  } Interface MetierCrueCalageAuto  ------------------------------------------
}
