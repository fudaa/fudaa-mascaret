/*
 * @file         MetierCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.calageauto;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * Implémentation de l'objet mesure pour une crue de calage "MetierMesureCrueCalageAuto".
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:25 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class MetierMesureCrueCalageAuto extends MetierHydraulique1d {
  private double abscisse_;
  private double cote_;
  private double coefficient_;

  // Constructeur.
  public MetierMesureCrueCalageAuto() {
    super();
    abscisse_=0;
    cote_=0;
    coefficient_=1.0;

    notifieObjetCree();
  }

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierMesureCrueCalageAuto) {
      MetierMesureCrueCalageAuto q=(MetierMesureCrueCalageAuto)_o;
      abscisse(q.abscisse());
      cote(q.cote());
      coefficient(q.coefficient());
    }
  }

  @Override
  final public MetierHydraulique1d creeClone() {
    MetierMesureCrueCalageAuto p= new MetierMesureCrueCalageAuto();
    p.initialise(this);
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Mesure");
    res[1]=
      super.getInfos()[1]
        + " "+getS("abscisse")+" : "
        + abscisse_
        + " "+getS("cote")+" : "
        + cote_
        + " "+getS("coefficient")+" : "
        + coefficient_;

    return res;
  }

  @Override
  public void dispose() {
    abscisse_=0;
    cote_=0;
    coefficient_=0;
    super.dispose();
  }

  //---  Interface MetierMesureCrueCalageAuto {  ------------------------------------

  public double abscisse() {
    return abscisse_;
  }

  public void abscisse(double _abscisse) {
    if (abscisse_==_abscisse) return;
    abscisse_=_abscisse;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisse");
  }

  public double cote() {
    return cote_;
  }

  public void cote(double _cote) {
    if (cote_==_cote) return;
    cote_=_cote;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "cote");
  }

  public double coefficient() {
    return coefficient_;
  }

  public void coefficient(double _coefficient) {
    if (coefficient_==_coefficient) return;
    coefficient_=_coefficient;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefficient");
  }

  //---  } Interface MetierMesureCrueCalageAuto  ------------------------------------
}
