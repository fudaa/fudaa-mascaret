/*
 * @file         MetierCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.calageauto;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * Implémentation de l'objet "MetierParametresCalageAuto" contenant les parametres pour le lancement du calage automatique.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:24 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class MetierParametresCalageAuto extends MetierHydraulique1d {
  //private double      pasGradient_;
  private int         nbMaxIterations_;
  private EnumMetierTypeLit    typeLit_;
  private double      precision_;
  //private double      roInit_;
  //private EnumMetierMethodeOpt methodeOpt_;
  private EnumMetierTypeCoefficient typeCoefficient_;
	
  // Constructeur.
  public MetierParametresCalageAuto() {
    super();
    //pasGradient_=1;
    nbMaxIterations_=100;
    typeLit_= org.fudaa.dodico.hydraulique1d.metier.calageauto.EnumMetierTypeLit.MINEUR;
    typeCoefficient_ = EnumMetierTypeCoefficient.STRICKLER;
    precision_=0.001;
    //roInit_=1;
    //methodeOpt_=EnumMetierMethodeOpt.DESCENTE_OPTIMALE;

    notifieObjetCree();
  }

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierParametresCalageAuto) {
      MetierParametresCalageAuto q=(MetierParametresCalageAuto)_o;
      //pasGradient(q.pasGradient());
      nbMaxIterations(q.nbMaxIterations());
      typeLit(q.typeLit());
      precision(q.precision());
      //roInit(roInit());
      //methodeOpt(methodeOpt());
    }
  }
  
  
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierParametresCalageAuto p= new MetierParametresCalageAuto();
    p.initialise(this);
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Parametres");
    res[1]=
      super.getInfos()[1]
       // + " pasGradient : "
        //+ pasGradient_
        + " nbMaxIterations : "
        + nbMaxIterations_
        + " typeLit : "
        + (typeLit_.equals( org.fudaa.dodico.hydraulique1d.metier.calageauto.EnumMetierTypeLit.MINEUR) ? "mineur":"majeur")
        + " precision : "
        + precision_;
       // + " roInit : "
        //+ roInit_
       // + "methodeOpt : "
       // + (methodeOpt_.equals(EnumMetierMethodeOpt.DESCENTE_OPTIMALE) ? "Descente optimale":
       //    methodeOpt_.equals(EnumMetierMethodeOpt.CASIER_NEWTON) ? "Casier newton":"Algo genetique");

    return res;
  }

  @Override
  public void dispose() {
    //pasGradient_=0;
    nbMaxIterations_=0;
    typeLit_=null;
    precision_=0;
    //roInit_=0;
    //methodeOpt_=null;
    typeCoefficient_= null;
    super.dispose();
  }

  //---  Interface MetierParametresCalageAuto {  ------------------------------------
/*
  public double pasGradient() {
    return pasGradient_;
  }

  public void pasGradient(double _pas) {
    if (pasGradient_==_pas) return;
    pasGradient_=_pas;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pasGradient");
  }
*/
  public int nbMaxIterations() {
    return nbMaxIterations_;
  }

  public void nbMaxIterations(int _nb) {
    if (nbMaxIterations_==_nb) return;
    nbMaxIterations_=_nb;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nbMaxIterations");
  }

  public EnumMetierTypeLit typeLit() {
    return typeLit_;
  }

  public void typeLit(EnumMetierTypeLit _type) {
    if (typeLit_.equals(_type)) return;
    typeLit_=_type;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "typeLit");
  }

  public double precision() {
    return precision_;
  }

  public void precision(double _prec) {
    if (precision_==_prec) return;
    precision_=_prec;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "precision");
  }

 /* public double roInit() {
    return roInit_;
  }

  public void roInit(double _ro) {
    if (roInit_==_ro) return;
    roInit_=_ro;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "roInit");
  }
*/
  /*
  public EnumMetierMethodeOpt methodeOpt() {
    return methodeOpt_;
  }

  public void methodeOpt(EnumMetierMethodeOpt _mth) {
    if (methodeOpt_.equals(_mth)) return;
    methodeOpt_=_mth;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "methodeOpt");
  }
*/
  public EnumMetierTypeCoefficient typeCoefficient() {
		return typeCoefficient_;
	}
	public void typeCoefficient(EnumMetierTypeCoefficient s) {
		if (typeCoefficient_.value()==s.value()) return;
		typeCoefficient_= s;
		Notifieur.getNotifieur().fireObjetModifie(toString(), this, "typeCoefficient");
	}

  
  //---  } Interface MetierParametresCalageAuto  ------------------------------------
}
