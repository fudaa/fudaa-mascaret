/*
 * @file         MetierCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.calageauto;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * Implémentation de l'objet "MetierResultatsCalageAuto" contenant les résultats du calage automatique.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:26 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class MetierResultatsCalageAuto extends MetierHydraulique1d {
  private byte[]                    listingMascaret_;
//  private byte[]                    listingDamocles_;
  private byte[]                    listingCalage_;
  private byte[]                    messagesEcran_;
  private byte[]                    messagesEcranErreur_;
  private MetierResultatsTemporelSpatial resultats_;
  private MetierZoneFrottement[]         zonesCalees_;

  // Constructeur.
  public MetierResultatsCalageAuto() {
    super();
    listingMascaret_=new byte[0];
//    listingDamocles_=new byte[0];
    listingCalage_=new byte[0];
    messagesEcran_=new byte[0];
    messagesEcranErreur_=new byte[0];
    resultats_=new MetierResultatsTemporelSpatial();
    zonesCalees_=new MetierZoneFrottement[0];
    
    notifieObjetCree();
  }

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierResultatsCalageAuto) {
      MetierResultatsCalageAuto q=(MetierResultatsCalageAuto)_o;
      listingMascaret(q.listingMascaret());
//      listingDamocles(q.listingDamocles());
      listingCalage(q.listingCalage());
      messagesEcran(q.messagesEcran());
      messagesEcranErreur(q.messagesEcranErreur());
      resultats(resultats());
      zonesFrottementCalees(zonesFrottementCalees());
    }
  }

  @Override
  final public MetierHydraulique1d creeClone() {
    MetierResultatsCalageAuto p= new MetierResultatsCalageAuto();
    p.initialise(this);
    return p;
  }

  @Override
  final public String toString() {  // Utile lors les déclenchement de notification sur modification d'attribut.
    return "resultatsCalage";
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= toString();
    res[1]=
      super.getInfos()[1];

    return res;
  }

  @Override
  public void dispose() {
    listingMascaret_=null;
//    listingDamocles_=null;
    listingCalage_=null;
    messagesEcran_=null;
    messagesEcranErreur_=null;
    if (resultats_!=null) { resultats_.dispose(); resultats_=null; }
    zonesCalees_=null;
    super.dispose();
  }

  //---  Interface MetierResultatsCalageAuto {  ------------------------------------

  public byte[] listingMascaret() {
    return listingMascaret_;
  }

  public void listingMascaret(byte[] _lis) {
    if (listingMascaret_==_lis) return;
    listingMascaret_=_lis;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "listingMascaret");
  }

//  public byte[] listingDamocles() {
//    return listingDamocles_;
//  }
//
//  public void listingDamocles(byte[] _lis) {
//    if (listingDamocles_==_lis) return;
//    listingDamocles_=_lis;
//    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "listingDamocles");
//  }

  public byte[] listingCalage() {
    return listingCalage_;
  }

  public void listingCalage(byte[] _lis) {
    if (listingCalage_==_lis) return;
    listingCalage_=_lis;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "listingCalage");
  }

  public byte[] messagesEcran() {
    return messagesEcran_;
  }

  public void messagesEcran(byte[] _lis) {
    if (messagesEcran_==_lis) return;
    messagesEcran_=_lis;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "messagesEcran");
  }

  public byte[] messagesEcranErreur() {
    return messagesEcranErreur_;
  }

  public void messagesEcranErreur(byte[] _lis) {
    if (messagesEcranErreur_==_lis) return;
    messagesEcranErreur_=_lis;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "messagesEcranErreur");
  }

  public MetierResultatsTemporelSpatial resultats() {
    return resultats_;
  }

  public void resultats(MetierResultatsTemporelSpatial _res) {
    if (resultats_==_res) return;
    resultats_=_res;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "resultats");
  }

  public MetierZoneFrottement[] zonesFrottementCalees() {
    return zonesCalees_;
  }

  public void zonesFrottementCalees(MetierZoneFrottement[] _zones) {
    if (zonesCalees_==_zones) return;
    zonesCalees_=_zones;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zonesFrottementCalees");
  }

  //---  } Interface MetierResultatsCalageAuto  ------------------------------------
}
