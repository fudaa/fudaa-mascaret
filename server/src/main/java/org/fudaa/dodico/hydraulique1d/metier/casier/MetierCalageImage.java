/**
 * @file         MetierCalageImage.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier "calage image" pour aider � d�finir la g�om�trie d'un casier.
 * Pas r�ellement utilis�.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:24 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class MetierCalageImage extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierCalageImage) {
      MetierCalageImage q= (MetierCalageImage)_o;
      pt1Pixel(clone(q.pt1Pixel()));
      pt2Pixel(clone(q.pt2Pixel()));
      pt3Pixel(clone(q.pt3Pixel()));
      pt1Utilisateur(clone(q.pt1Utilisateur()));
      pt2Utilisateur(clone(q.pt2Utilisateur()));
      pt3Utilisateur(clone(q.pt3Utilisateur()));
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierCalageImage p= new MetierCalageImage();
    p.initialise(this);
    return p;
  }
  /*** MetierLiaison ***/
  // constructeurs
  public MetierCalageImage() {
    super();
    pt1Pixel_.x= 0;
    pt1Pixel_.y= 0;
    pt1Utilisateur_.x= 0;
    pt1Utilisateur_.y= 0;
    pt2Pixel_.x= 10;
    pt2Pixel_.y= 0;
    pt2Utilisateur_.x= 10;
    pt2Utilisateur_.y= 0;
    pt3Pixel_.x= 0;
    pt3Pixel_.y= 10;
    pt3Utilisateur_.x= 0;
    pt3Utilisateur_.y= 10;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    pt1Pixel_.x= 0;
    pt1Pixel_.y= 0;
    pt1Utilisateur_.x= 0;
    pt1Utilisateur_.y= 0;
    pt2Pixel_.x= 0;
    pt2Pixel_.y= 0;
    pt2Utilisateur_.x= 0;
    pt2Utilisateur_.y= 0;
    pt3Pixel_.x= 0;
    pt3Pixel_.y= 0;
    pt3Utilisateur_.x= 0;
    pt3Utilisateur_.y= 0;
    super.dispose();
  }
  /*** MetierCalageImage ***/
  // attributs
  private MetierPoint2D pt1Pixel_;
  public MetierPoint2D pt1Pixel() {
    return pt1Pixel_;
  }
  public void pt1Pixel(MetierPoint2D s) {
    if ((pt1Pixel_ != null)&&(s != null)) {
      if ( (s.x == pt1Pixel_.x) && (s.y == pt1Pixel_.y))
        return;
    }
    pt1Pixel_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pt1Pixel");
  }
  private MetierPoint2D pt1Utilisateur_;
  public MetierPoint2D pt1Utilisateur() {
    return pt1Utilisateur_;
  }
  public void pt1Utilisateur(MetierPoint2D s) {
    if ((pt1Utilisateur_ != null)&&(s != null)) {
      if ( (s.x == pt1Utilisateur_.x) && (s.y == pt1Utilisateur_.y))
        return;
    }
    pt1Utilisateur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pt1Utilisateur");
  }
  private MetierPoint2D pt2Pixel_;
  public MetierPoint2D pt2Pixel() {
    return pt2Pixel_;
  }
  public void pt2Pixel(MetierPoint2D s) {
    if ((pt2Pixel_ != null)&&(s != null)) {
      if ( (s.x == pt2Pixel_.x) && (s.y == pt2Pixel_.y))
        return;
    }
    pt2Pixel_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pt2Pixel");
  }
  private MetierPoint2D pt2Utilisateur_;
  public MetierPoint2D pt2Utilisateur() {
    return pt2Utilisateur_;
  }
  public void pt2Utilisateur(MetierPoint2D s) {
    if ((pt2Utilisateur_ != null)&&(s != null)) {
      if ( (s.x == pt2Utilisateur_.x) && (s.y == pt2Utilisateur_.y))
        return;
    }
    pt2Utilisateur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pt2Utilisateur");
  }
  private MetierPoint2D pt3Pixel_;
  public MetierPoint2D pt3Pixel() {
    return pt3Pixel_;
  }
  public void pt3Pixel(MetierPoint2D s) {
    if ((pt3Pixel_ != null)&&(s != null)) {
      if ( (s.x == pt3Pixel_.x) && (s.y == pt3Pixel_.y))
        return;
    }
    pt3Pixel_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pt3Pixel");
  }
  private MetierPoint2D pt3Utilisateur_;
  public MetierPoint2D pt3Utilisateur() {
    return pt3Utilisateur_;
  }
  public void pt3Utilisateur(MetierPoint2D s) {
    if ((pt3Utilisateur_ != null)&&(s != null)) {
      if ( (s.x == pt3Utilisateur_.x) && (s.y == pt3Utilisateur_.y))
        return;
    }
    pt3Utilisateur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pt3Utilisateur");
  }
  private static MetierPoint2D clone(MetierPoint2D pt) {
    MetierPoint2D ptClone= new MetierPoint2D();
    ptClone.x= pt.x;
    ptClone.y= pt.y;
    return ptClone;
  }
}
