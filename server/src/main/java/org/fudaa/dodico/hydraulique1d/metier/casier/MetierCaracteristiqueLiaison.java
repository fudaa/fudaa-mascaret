/**
 * @file         DCaracteristiqueLiaison.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierSensDebitLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.Identifieur;
/**
 * Implémentation de l'objet métier "caractéristique d'une liaison".
 * Abstraction des différents type de liaison (seuil, chenal, siphon, orifice).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:24 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public abstract class MetierCaracteristiqueLiaison extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    if (_o instanceof MetierCaracteristiqueLiaison) {
      MetierCaracteristiqueLiaison q= (MetierCaracteristiqueLiaison)_o;
      cote(q.cote());
      id(q.id());
    }
  }
  @Override
  final public String toString() {
    return "CaracteristiqueLiaison" + id_;
  }
  public MetierCaracteristiqueLiaison() {
    super();
    cote_=1;
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(
        "org.fudaa.dodico.hydraulique1d.metier.casier.MetierCaracteristiqueLiaison");
  }
  @Override
  public void dispose() {
    cote_= 0;
    id_=0;
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Caractéristiques liaison");
    res[1]= getS("cote")+" : " + cote_;
    return res;
  }
  /*** DCaracteristiqueLiaison ***/
  // attributs
  protected int id_;
  public int id() {
    return id_;
  }
  public void id(int s) {
    if (id_==s) return;
    id_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "id");
  }
  protected double cote_;
  public double cote() {
    return cote_;
  }
  public void cote(double s) {
    if (cote_==s) return;
    cote_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "cote");
  }
  // méthodes
  public boolean isSeuil() {
    return false;
  }
  public boolean isChenal() {
    return false;
  }
  public boolean isSiphon() {
    return false;
  }
  public boolean isOrifice() {
    return false;
  }
  public double getCote() {
    return cote();
  }
  public void setCote(double cote) {
    cote(cote);
  }
  public double getLargeur() {
    throw new IllegalArgumentException("methode invalide : getLargeur()");
  }
  public void setLargeur(double largeur) {
    throw new IllegalArgumentException("methode invalide : setLargeur()");
  }
  public double getLongueur() {
    throw new IllegalArgumentException("methode invalide : getLongueur()");
  }
  public void setLongueur(double longueur) {
    throw new IllegalArgumentException("methode invalide : setLongueur()");
  }
  public double getCoefQ() {
    throw new IllegalArgumentException("methode invalide : getCoefQ()");
  }
  public void setCoefQ(double coefQ) {
    throw new IllegalArgumentException("methode invalide : setCoefQ()");
  }
  public double getSection() {
    throw new IllegalArgumentException("methode invalide : getSection()");
  }
  public void setSection(double section) {
    throw new IllegalArgumentException("methode invalide : setSection()");
  }
  public double getCoefActivation() {
    throw new IllegalArgumentException("methode invalide : getCoefActivation()");
  }
  public void setCoefActivation(double coefActivation) {
    throw new IllegalArgumentException("methode invalide : setCoefActivation()");
  }
  public double getRugosite() {
    throw new IllegalArgumentException("methode invalide : getRugosite()");
  }
  public void setRugosite(double rugosite) {
    throw new IllegalArgumentException("methode invalide : setRugosite()");
  }
  public double getCoefPerteCharge() {
    throw new IllegalArgumentException("methode invalide : getCoefPerteCharge()");
  }
  public void setCoefPerteCharge(double coefPerteCharge) {
    throw new IllegalArgumentException("methode invalide : setCoefPerteCharge()");
  }
  public double getCoefQOrifice() {
    throw new IllegalArgumentException("methode invalide : getCoefQOrifice()");
  }
  public void setCoefQOrifice(double coteMin) {
    throw new IllegalArgumentException("methode invalide : setCoefQOrifice()");
  }
  public EnumMetierSensDebitLiaison getSensDebit() {
    throw new IllegalArgumentException("methode invalide : getSensDebit()");
  }
  public void setSensDebit(EnumMetierSensDebitLiaison sensDebit) {
    throw new IllegalArgumentException("methode invalide : setSensDebit()");
  }
}
