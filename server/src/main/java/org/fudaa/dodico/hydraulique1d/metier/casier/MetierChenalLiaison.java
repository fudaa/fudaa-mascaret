/**
 * @file         MetierSeuilLiaison.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Implémentation de l'objet métier caractéristique d'une "liaison chenal".
 * Associe une largeur, une longueur et une rugosité.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:24 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class MetierChenalLiaison extends MetierCaracteristiqueLiaison {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierChenalLiaison) {
      MetierChenalLiaison q= (MetierChenalLiaison)_o;
      largeur(q.largeur());
      longueur(q.longueur());
      rugosite(q.rugosite());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierChenalLiaison p= new MetierChenalLiaison();
    p.initialise(this);
    return p;
  }
  /*** MetierLiaison ***/
  // constructeurs
  public MetierChenalLiaison() {
    super();
    largeur_= 1;
    longueur_= 10;
    rugosite_= 30;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    id_= 0;
    largeur_= 0;
    longueur_= 0;
    rugosite_= 0;
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Chenal");
    res[1]=
      super.getInfos()[1]
        + " "+getS("larg.")+" : "
        + largeur_
        + " "+getS("long.")+" : "
        + longueur_
        + " "+getS("rugosité")+" : "
        + rugosite_;
    return res;
  }
  /*** MetierSeuilLiaison ***/
  // attributs
  private double largeur_;
  public double largeur() {
    return largeur_;
  }
  public void largeur(double s) {
    if (largeur_==s) return;
    largeur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "largeur");
  }
  private double longueur_;
  public double longueur() {
    return longueur_;
  }
  public void longueur(double s) {
    if (longueur_==s) return;
    longueur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "longueur");
  }
  private double rugosite_;
  public double rugosite() {
    return rugosite_;
  }
  public void rugosite(double s) {
    if (rugosite_==s) return;
    rugosite_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "rugosite");
  }
  // méthodes
  @Override
  public boolean isChenal() {
    return true;
  }
  @Override
  public double getLargeur() {
    return largeur();
  }
  @Override
  public void setLargeur(double largeur) {
    largeur(largeur);
  }
  @Override
  public double getLongueur() {
    return longueur();
  }
  @Override
  public void setLongueur(double longueur) {
    longueur(longueur);
  }
  @Override
  public double getRugosite() {
    return rugosite();
  }
  @Override
  public void setRugosite(double rugosite) {
    rugosite(rugosite);
  }
  public double getCoteMoyenneFond() {
    return cote();
  }
  public void setCoteMoyenneFond(double coteMoyenneFond) {
    cote(coteMoyenneFond);
  }
}
