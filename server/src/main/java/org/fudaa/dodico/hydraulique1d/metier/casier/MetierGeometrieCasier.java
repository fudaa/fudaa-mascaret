/**
 * @file         MetierGeometrieCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.Identifieur;
/**
 * Implémentation de l'objet métier "géométrie d'un casier".
 * Abstraction des différentes géométrie (planimétrage ou nuage de point).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:22 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public abstract class MetierGeometrieCasier extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierGeometrieCasier) {
      MetierGeometrieCasier q= (MetierGeometrieCasier)_o;
      pasPlanimetrage(q.pasPlanimetrage());
    }
  }
  @Override
  final public String toString() {
    String s= "Geometrie casier" + id_;
    return s;
  }
  /*** MetierGeometrieCasier ***/
  // constructeurs
  public MetierGeometrieCasier() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
    pasPlanimetrage_= 1;
  }
  @Override
  public void dispose() {
    id_= 0;
    pasPlanimetrage_= 0;
    super.dispose();
  }
  // attributs
  private int id_;
  private double pasPlanimetrage_;
  public double pasPlanimetrage() {
    return pasPlanimetrage_;
  }
  public void pasPlanimetrage(double pasPlanimetrage) {
    if (pasPlanimetrage_==pasPlanimetrage) return;
    pasPlanimetrage_= pasPlanimetrage;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pasPlanimetrage");
  }
  // les méthodes
  public boolean isSurfaceDependCote() {
    return false;
  }
  public void setSurfaceDependCote(boolean surfaceDependCote) {
    throw new IllegalArgumentException("methode invalide : setSurfaceDependCote()");
  }
  public double getPasPlanimetrage() {
    return pasPlanimetrage();
  }
  public void setPasPlanimetrage(double pasPlanimetrage) {
    pasPlanimetrage(pasPlanimetrage);
  }
  public MetierPoint[] getPointsFrontiere() {
    throw new IllegalArgumentException("methode invalide : getPointsFrontiere()");
  }
  public void setPointsFrontiere(MetierPoint[] points) {
    throw new IllegalArgumentException("methode invalide : setPointsFrontiere()");
  }
  public MetierPoint[] getPointsInterieur() {
    throw new IllegalArgumentException("methode invalide : getPointsInterieur()");
  }
  public void setPointsInterieur(MetierPoint[] points) {
    throw new IllegalArgumentException("methode invalide : setPointsInterieur()");
  }
  public int getNbCotePlanimetrage() {
    throw new IllegalArgumentException("methode invalide : getNbCotePlanimetrage()");
  }
  public boolean isNuagePoints() {
    return false;
  }
  public boolean isPlanimetrage() {
    return false;
  }
  public MetierPoint2D[] getPointsPlanimetrage() {
    throw new IllegalArgumentException("methode invalide : getPointsPlanimetrage()");
  }
  public void setPointsPlanimetrage(MetierPoint2D[] points) {
    throw new IllegalArgumentException("methode invalide : setPointsPlanimetrage()");
  }
}
