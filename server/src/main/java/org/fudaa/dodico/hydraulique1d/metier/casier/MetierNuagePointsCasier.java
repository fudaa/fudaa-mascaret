/**
 * @file         MetierNuagePointsCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:20 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import java.util.Arrays;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier g�om�trie d'un casier d�crit par des "nuages de points 3D".
 * Associe les nuages de points fronti�res et int�rieurs largeur, un indicateur
 * de d�pendance de la surface vis a vis de la cote.
 * L'image et le calage de l'image ne sont pas encore utilis�s.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:20 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class MetierNuagePointsCasier extends MetierGeometrieCasier {
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Semis de points");
    res[1]= "";
    return res;
  }
  /*** MetierNuagePointsCasier ***/
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierNuagePointsCasier) {
      MetierNuagePointsCasier q= (MetierNuagePointsCasier)_o;
      surfaceDependCote(q.surfaceDependCote());
      pointsFrontiere((MetierPoint[])q.pointsFrontiere().clone());
      pointsInterieur((MetierPoint[])q.pointsInterieur().clone());
      image((byte[])q.image().clone());
      if (q.calage() != null)
        calage((MetierCalageImage)q.calage().creeClone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierNuagePointsCasier p=
      new MetierNuagePointsCasier();
    p.initialise(this);
    return p;
  }
  // constructeurs
  public MetierNuagePointsCasier() {
    super();
    surfaceDependCote_= false;
    pointsFrontiere_= new MetierPoint[0];
    pointsInterieur_= new MetierPoint[0];
    image_= new byte[0];
    calage_= null;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    surfaceDependCote_= false;
    pointsFrontiere_= null;
    pointsInterieur_= null;
    image_= null;
    calage_= null;
    super.dispose();
  }
  // attributs
  private boolean surfaceDependCote_;
  public boolean surfaceDependCote() {
    return surfaceDependCote_;
  }
  public void surfaceDependCote(boolean surfaceDependCote) {
    if (surfaceDependCote_==surfaceDependCote) return;
    surfaceDependCote_= surfaceDependCote;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "surfaceDependCote");
  }
  private MetierPoint[] pointsFrontiere_;
  public MetierPoint[] pointsFrontiere() {
    return pointsFrontiere_;
  }
  public void pointsFrontiere(MetierPoint[] points) {
    if (egale(pointsFrontiere_,points)) return;
    pointsFrontiere_= points;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pointsFrontiere");
  }
  private MetierPoint[] pointsInterieur_;
  public MetierPoint[] pointsInterieur() {
    return pointsInterieur_;
  }
  public void pointsInterieur(MetierPoint[] points) {
    if (egale(pointsInterieur_,points)) return;
    pointsInterieur_= points;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "pointsInterieur");
  }
  private byte[] image_;
  public byte[] image() {
    return image_;
  }
  public void image(byte[] image) {
    if (Arrays.equals(image_, image)) return;
    image_= image;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "image");
  }
  private MetierCalageImage calage_;
  public MetierCalageImage calage() {
    return calage_;
  }
  public void calage(MetierCalageImage s) {
    if (calage_== s) return;
    calage_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "calage");
  }
  private final static boolean egale(MetierPoint[] p, MetierPoint[] p2) {
    if (p==p2) return true;
    if (p==null || p2==null) return false;

    int length = p.length;
    if (p2.length != length) return false;

    for (int i=0; i<length; i++)
        if (!((p[i].x==p2[i].x)&&(p[i].y==p2[i].y)&&(p[i].z==p2[i].z)))
            return false;

    return true;
  }
  // les m�thodes
  @Override
  public boolean isSurfaceDependCote() {
    return surfaceDependCote_;
  }
  @Override
  public void setSurfaceDependCote(boolean surfaceDependCote) {
    surfaceDependCote(surfaceDependCote);
  }
  @Override
  public double getPasPlanimetrage() {
    return pasPlanimetrage();
  }
  @Override
  public void setPasPlanimetrage(double pasPlanimetrage) {
    pasPlanimetrage(pasPlanimetrage);
  }
  @Override
  public MetierPoint[] getPointsFrontiere() {
    return pointsFrontiere_;
  }
  @Override
  public void setPointsFrontiere(MetierPoint[] points) {
    pointsFrontiere(points);
  }
  @Override
  public MetierPoint[] getPointsInterieur() {
    return pointsInterieur_;
  }
  @Override
  public void setPointsInterieur(MetierPoint[] points) {
    pointsInterieur(points);
  }
  @Override
  public int getNbCotePlanimetrage() {
    double min1= coteMin(pointsFrontiere_);
    double min2= coteMin(pointsInterieur_);
    double min= Math.min(min1, min2);
    double max1= coteMax(pointsFrontiere_);
    double max2= coteMax(pointsInterieur_);
    double max= Math.max(max1, max2);
    double diff= max - min;
    return (int) (diff / pasPlanimetrage());
  }
  @Override
  public boolean isNuagePoints() {
    return true;
  }
  private final double coteMin(MetierPoint[] pts) {
    double res= Double.MAX_VALUE;
    for (int i= 0; i < pts.length; i++) {
      res= Math.min(res, pts[i].z);
    }
    return res;
  }
  private final double coteMax(MetierPoint[] pts) {
    double res= Double.NEGATIVE_INFINITY;
    for (int i= 0; i < pts.length; i++) {
      res= Math.max(res, pts[i].z);
    }
    return res;
  }
}
