/**
 * @file         MetierOrificeLiaison.java
 * @creation     2003-10-20
 * @modification $Date: 2007-11-20 11:43:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierSensDebitLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier caract�ristique d'une "liaison orifice".
 * Associe une largeur, une section, un sens et 2 coefficients de d�bit.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:21 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class MetierOrificeLiaison extends MetierCaracteristiqueLiaison {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierOrificeLiaison) {
      MetierOrificeLiaison q= (MetierOrificeLiaison)_o;
      coefQSeuil(q.coefQSeuil());
      coefQOrifice(q.coefQOrifice());
      largeur(q.largeur());
      section(q.section());
      sensDebit(EnumMetierSensDebitLiaison.from_int(q.sensDebit().value()));
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierOrificeLiaison p= new MetierOrificeLiaison();
    p.initialise(this);
    return p;
  }
  @Override
  public String[] getInfos() {
    String sensDebit = getS("les 2");
    if (sensDebit_.value() == EnumMetierSensDebitLiaison._BIEF_VERS_CASIER_OU_VERS_AMONT) {
      sensDebit = getS("Fin->Origine");
    } else if (sensDebit_.value() == EnumMetierSensDebitLiaison._CASIER_VERS_BIEF_OU_VERS_AVAL) {
      sensDebit = getS("Origine->Fin");
    }
    String[] res= new String[2];
    res[0]= getS("Orifice");
    res[1]=
      super.getInfos()[1]
        + " "+getS("larg.")+" : "
        + largeur_
        + " "+getS("coef. Q seuil")+" : "
        + coefQSeuil_
        + " "+getS("coef. Q orifice")+" : "
        + coefQOrifice_
        + " "+getS("sens Q")+" : "
        + sensDebit;
    return res;
  }
  /*** MetierLiaison ***/
  // constructeurs
  public MetierOrificeLiaison() {
    super();
    largeur_= 1;
    coefQSeuil_= 0.40;
    section_= 1;
    coefQOrifice_= 0.5;
    sensDebit_= EnumMetierSensDebitLiaison.DEUX_SENS;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    id_= 0;
    largeur_= 0;
    coefQSeuil_= 0;
    section_= 0;
    coefQOrifice_= 0;
    sensDebit_= null;
    super.dispose();
  }
  /*** MetierSeuilLiaison ***/
  // attributs
  private double largeur_;
  public double largeur() {
    return largeur_;
  }
  public void largeur(double s) {
    if (largeur_==s) return;
    largeur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "largeur");
  }
  private double coefQSeuil_;
  public double coefQSeuil() {
    return coefQSeuil_;
  }
  public void coefQSeuil(double s) {
    if (coefQSeuil_==s) return;
    coefQSeuil_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefQSeuil");
  }
  private double section_;
  public double section() {
    return section_;
  }
  public void section(double s) {
    if (section_==s) return;
    section_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "section");
  }
  private double coefQOrifice_;
  public double coefQOrifice() {
    return coefQOrifice_;
  }
  public void coefQOrifice(double s) {
    if (coefQOrifice_==s) return;
    coefQOrifice_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefQOrifice");
  }
  private EnumMetierSensDebitLiaison sensDebit_;
  public EnumMetierSensDebitLiaison sensDebit() {
    return sensDebit_;
  }
  public void sensDebit(EnumMetierSensDebitLiaison s) {
    /*System.out.println("MetierOrificeLiaison sensDebit(EnumMetierSensDebitLiaison s)");
    System.out.println("s.value()="+s.value());
    System.out.println("sensDebit_.value()="+sensDebit_.value());
    System.out.println("(sensDebit_==s) ="+(sensDebit_==s));*/
    if (sensDebit_==s) return;
    //System.out.println("Affectation sensDebit_= s;");
    sensDebit_= s;
    //System.out.println("CDodico.findUsine().fireObjetModifie");
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "sensDebit");
  }
  // m�thodes
  @Override
  public boolean isOrifice() {
    return true;
  }
  @Override
  public double getLargeur() {
    return largeur();
  }
  @Override
  public void setLargeur(double largeur) {
    largeur(largeur);
  }
  @Override
  public double getCoefQ() {
    return coefQSeuil();
  }
  @Override
  public void setCoefQ(double coefQ) {
    coefQSeuil(coefQ);
  }
  @Override
  public double getSection() {
    return section();
  }
  @Override
  public void setSection(double section) {
    section(section);
  }
  @Override
  public double getCoefQOrifice() {
    return coefQOrifice();
  }
  @Override
  public void setCoefQOrifice(double coefQOrifice) {
    coefQOrifice(coefQOrifice);
  }
  @Override
  public EnumMetierSensDebitLiaison getSensDebit() {
    return sensDebit();
  }
  @Override
  public void setSensDebit(EnumMetierSensDebitLiaison sensDebit) {
    sensDebit(sensDebit);
  }
  public double getCoteMoyenneRadier() {
    return cote();
  }
  public void setCoteMoyenneRadier(double coteMoyenneRadier) {
    cote(coteMoyenneRadier);
  }
}
