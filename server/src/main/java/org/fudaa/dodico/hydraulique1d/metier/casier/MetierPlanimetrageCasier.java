/**
 * @file         MetierPlanimetrageCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Implémentation de l'objet métier géométrie d'un casier décrit par un "planimétrage".
 * Associe les points 2D du planimétrage et la cote du fond.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:24 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class MetierPlanimetrageCasier extends MetierGeometrieCasier {
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Zfond")+" : "+coteInitiale_+" "+getS("Planimétrie")+" ";
    res[1]= "";
    return res;
  }
  /*** MetierPlanimetrageCasier ***/
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierPlanimetrageCasier) {
      MetierPlanimetrageCasier q= (MetierPlanimetrageCasier)_o;
      coteInitiale(q.coteInitiale());
      points((MetierPoint2D[])q.points().clone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierPlanimetrageCasier p=
      new MetierPlanimetrageCasier();
    p.initialise(this);
    return p;
  }
  // constructeurs
  public MetierPlanimetrageCasier() {
    super();
    coteInitiale_= 0;
    points_= new MetierPoint2D[0];

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    coteInitiale_= -1;
    points_= null;
    super.dispose();
  }
  // attributs
  private double coteInitiale_;
  public double coteInitiale() {
    return coteInitiale_;
  }
  public void coteInitiale(double coteInitiale) {
    if (coteInitiale_==coteInitiale) return;
    coteInitiale_= coteInitiale;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteInitiale");
  }
  private MetierPoint2D[] points_;
  public MetierPoint2D[] points() {
    return points_;
  }
  public void points(MetierPoint2D[] points) {
    if (egale(points_, points)) return;
    points_= points;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "points");
  }
  private final static boolean egale(MetierPoint2D[] p, MetierPoint2D[] p2) {
    if (p==p2) return true;
    if (p==null || p2==null) return false;

    int length = p.length;
    if (p2.length != length) return false;

    for (int i=0; i<length; i++)
        if (!((p[i].x==p2[i].x)&&(p[i].y==p2[i].y)))
            return false;

    return true;
  }
  // les méthodes
  @Override
  public MetierPoint2D[] getPointsPlanimetrage() {
    return points_;
  }
  @Override
  public void setPointsPlanimetrage(MetierPoint2D[] points) {
    points(points);
  }
  @Override
  public boolean isPlanimetrage() {
    return true;
  }
}
