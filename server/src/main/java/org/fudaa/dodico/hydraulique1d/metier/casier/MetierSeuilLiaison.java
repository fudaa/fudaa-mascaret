/**
 * @file         MetierSeuilLiaison.java
 * @creation
 * @modification $Date: 2007-12-05 09:39:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier caract�ristique d'une "liaison seuil".
 * Associe une largeur, un coefficient de d�bit et un coefficient d'activation.
 * @version      $Revision: 1.3 $ $Date: 2007-12-05 09:39:14 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public final class MetierSeuilLiaison extends MetierCaracteristiqueLiaison {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuilLiaison) {
      MetierSeuilLiaison q= (MetierSeuilLiaison)_o;
      largeur(q.largeur());
      coefQ(q.coefQ());
      coefActivation(q.coefActivation());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSeuilLiaison p= new MetierSeuilLiaison();
    p.initialise(this);
    return p;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Seuil");
    res[1]=
      super.getInfos()[1]
        + " "+getS("larg.")+" : "
        + largeur_
        + " "+getS("coef. Q")+" : "
        + coefQ_
        + " "+getS("coef. Activation")+" : "
        + coefActivation_;
    return res;
  }
  /*** MetierLiaison ***/
  // constructeurs
  public MetierSeuilLiaison() {
    super();
    largeur_= 1;
    coefQ_= 0.40;
    coefActivation_= 0.80;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    id_= 0;
    largeur_= 0;
    coefQ_= 0;
    coefActivation_= 0;
    super.dispose();
  }
  /*** MetierSeuilLiaison ***/
  // attributs
  private double largeur_;
  public double largeur() {
    return largeur_;
  }
  public void largeur(double s) {
    if (largeur_==s) return;
    largeur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "largeur");
  }
  private double coefQ_;
  public double coefQ() {
    return coefQ_;
  }
  public void coefQ(double s) {
    if (coefQ_==s) return;
    coefQ_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefQ");
  }
  private double coefActivation_;
  public double coefActivation() {
    return coefActivation_;
  }
  public void coefActivation(double s) {
    if (coefActivation_==s) return;
    coefActivation_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefActivation");
  }
  // m�thodes
  @Override
  public boolean isSeuil() {
    return true;
  }
  @Override
  public double getLargeur() {
    return largeur();
  }
  @Override
  public void setLargeur(double largeur) {
    largeur(largeur);
  }
  @Override
  public double getCoefQ() {
    return coefQ();
  }
  @Override
  public void setCoefQ(double coefQ) {
    coefQ(coefQ);
  }
  @Override
  public double getCoefActivation() {
    return coefActivation();
  }
  @Override
  public void setCoefActivation(double coefActivation) {
    coefActivation(coefActivation);
  }
  public double getCoteMoyenneCrete() {
    return cote();
  }
  public void setCoteMoyenneCrete(double coteMoyenneFond) {
    cote(coteMoyenneFond);
  }
}
