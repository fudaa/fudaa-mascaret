/**
 * @file         MetierSiphonLiaison.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Implémentation de l'objet métier caractéristique d'une "liaison siphon".
 * Associe une longueur, une section et un coefficient de perte de charge.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:22 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class MetierSiphonLiaison extends MetierCaracteristiqueLiaison {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSiphonLiaison) {
      MetierSiphonLiaison q= (MetierSiphonLiaison)_o;
      longueur(q.longueur());
      section(q.section());
      coefPerteCharge(q.coefPerteCharge());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSiphonLiaison p= new MetierSiphonLiaison();
    p.initialise(this);
    return p;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Siphon");
    res[1]=
      super.getInfos()[1]
        + " "+getS("long.")+" : "
        + longueur_
        + " "+getS("sect.")+" : "
        + section_
        + " "+getS("perte charge")+" : "
        + coefPerteCharge_;
    return res;
  }
  /*** MetierLiaison ***/
  // constructeurs
  public MetierSiphonLiaison() {
    super();
    longueur_= 1;
    section_= 10;
    coefPerteCharge_= 0.40;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    id_= 0;
    longueur_= 0;
    section_= 0;
    coefPerteCharge_= 0;
    super.dispose();
  }
  /*** MetierSiphonLiaison ***/
  // attributs
  private double longueur_;
  public double longueur() {
    return longueur_;
  }
  public void longueur(double s) {
    if (longueur_==s) return;
    longueur_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "longueur");
  }
  private double section_;
  public double section() {
    return section_;
  }
  public void section(double s) {
    if (section_==s) return;
    section_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "section");
  }
  private double coefPerteCharge_;
  public double coefPerteCharge() {
    return coefPerteCharge_;
  }
  public void coefPerteCharge(double s) {
    if (coefPerteCharge_==s) return;
    coefPerteCharge_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefPerteCharge");
  }
  // méthodes
  @Override
  public boolean isSiphon() {
    return true;
  }
  @Override
  public double getLongueur() {
    return longueur();
  }
  @Override
  public void setLongueur(double longueur) {
    longueur(longueur);
  }
  @Override
  public double getSection() {
    return section();
  }
  @Override
  public void setSection(double section) {
    section(section);
  }
  @Override
  public double getCoefPerteCharge() {
    return coefPerteCharge();
  }
  @Override
  public void setCoefPerteCharge(double coefPerteCharge) {
    coefPerteCharge(coefPerteCharge);
  }
  public double getCoteMoyenneFond() {
    return cote();
  }
  public void setCoteMoyenneFond(double coteMoyenneFond) {
    cote(coteMoyenneFond);
  }
}
