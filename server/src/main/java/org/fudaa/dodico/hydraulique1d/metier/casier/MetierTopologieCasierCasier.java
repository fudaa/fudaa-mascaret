/**
 * @file         MetierTopologieCasierCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier topologie d'une liaison reliant 2 casiers.
 * Associe 2 r�f�rences vers le casier amont et le casier aval.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:23 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class MetierTopologieCasierCasier extends MetierTopologieLiaison {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierTopologieCasierCasier) {
      MetierTopologieCasierCasier q= (MetierTopologieCasierCasier)_o;
      casierAmontRattache(q.casierAmontRattache());
      casierAvalRattache(q.casierAvalRattache());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierTopologieCasierCasier p=
      new MetierTopologieCasierCasier();
    p.initialise(this);
    return p;
  }
  public MetierTopologieCasierCasier() {
    super();
    casierAmontRattache_= null;
    casierAvalRattache_= null;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    casierAmontRattache_= null;
    casierAvalRattache_= null;
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Casier-Casier");
    res[1]= "";
    return res;
  }
  /*** MetierTopologieCasierCasier ***/
  // attributs
  private MetierCasier casierAmontRattache_;
  public MetierCasier casierAmontRattache() {
    return casierAmontRattache_;
  }
  public void casierAmontRattache(MetierCasier s) {
    if (casierAmontRattache_ == s) return;
    casierAmontRattache_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "casierAmontRattache");
  }
  private MetierCasier casierAvalRattache_;
  public MetierCasier casierAvalRattache() {
    return casierAvalRattache_;
  }
  public void casierAvalRattache(MetierCasier s) {
    if (casierAvalRattache_== s) return;
    casierAvalRattache_= s;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "casierAvalRattache");
  }
  // m�thodes
  @Override
  public boolean isCasierCasier() {
    return true;
  }
  @Override
  public MetierCasier getCasierAmontRattache() {
    return casierAmontRattache();
  }
  @Override
  public void setCasierAmontRattache(MetierCasier casierRattache) {
    casierAmontRattache(casierRattache);
  }
  @Override
  public MetierCasier getCasierAvalRattache() {
    return casierAvalRattache();
  }
  @Override
  public void setCasierAvalRattache(MetierCasier casierRattache) {
    casierAvalRattache(casierRattache);
  }
}
