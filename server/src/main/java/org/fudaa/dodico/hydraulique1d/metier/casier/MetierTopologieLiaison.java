/**
 * @file         MetierTopologieLiaison.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.Identifieur;
/**
 * Impl�mentation de l'objet m�tier "topologie d'une liaison".
 * Abstraction des diff�rents type de topologie (casier-casier, bief-casier).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:22 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public abstract class MetierTopologieLiaison extends MetierHydraulique1d {
  @Override
  public void initialise(MetierHydraulique1d _o) {
//    if (_o instanceof MetierTopologieLiaison) {
//      MetierTopologieLiaison q= (MetierTopologieLiaison)_o;
//    }
  }
  @Override
  final public String toString() {
    return "TopologieLiaison" + id_;
  }
  public MetierTopologieLiaison() {
    super();
    id_= Identifieur.IDENTIFIEUR.identificateurLibre(getClass().getName());
  }
  @Override
  public void dispose() {
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Topologie liaison");
    res[1]= "";
    return res;
  }
  /*** MetierTopologieLiaison ***/
  // attributs
  protected int id_;
  // m�thodes
  public boolean isRiviereCasier() {
    return false;
  }
  public boolean isCasierCasier() {
    return false;
  }
  public double getAbscisse() {
    throw new IllegalArgumentException("methode invalide : getAbscisse()");
  }
  public void setAbscisse(double abscisse) {
    throw new IllegalArgumentException("methode invalide : setAbscisse()");
  }
  public MetierCasier getCasierRattache() {
    throw new IllegalArgumentException("methode invalide : getCasierRattache()");
  }
  public void setCasierRattache(MetierCasier casierRattache) {
    throw new IllegalArgumentException("methode invalide : setCasierRattache()");
  }
  public MetierBief getBiefRattache() {
    throw new IllegalArgumentException("methode invalide : getBiefRattache()");
  }
  public void setBiefRattache(MetierBief biefRattache) {
    throw new IllegalArgumentException("methode invalide : setBiefRattache()");
  }
  public MetierCasier getCasierAmontRattache() {
    throw new IllegalArgumentException("methode invalide : getCasierAmontRattache()");
  }
  public void setCasierAmontRattache(MetierCasier casierRattache) {
    throw new IllegalArgumentException("methode invalide : setCasierAmontRattache()");
  }
  public MetierCasier getCasierAvalRattache() {
    throw new IllegalArgumentException("methode invalide : getCasierAvalRattache()");
  }
  public void setCasierAvalRattache(MetierCasier casierRattache) {
    throw new IllegalArgumentException("methode invalide : setCasierAvalRattache()");
  }
}
