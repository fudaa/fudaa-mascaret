/**
 * @file         MetierTopologieRiviereCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.casier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier topologie d'une liaison reliant 1 casier avec une liaison.
 * Associe une r�f�rence vers un bief (et une abscisse) et une r�f�rence vers un casier.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:24 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class MetierTopologieRiviereCasier extends MetierTopologieLiaison {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierTopologieRiviereCasier) {
      MetierTopologieRiviereCasier q= (MetierTopologieRiviereCasier)_o;
      abscisse(q.abscisse());
      biefRattache(q.biefRattache());
      casierRattache(q.casierRattache());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierTopologieRiviereCasier p=
      new MetierTopologieRiviereCasier();
    p.initialise(this);
    return p;
  }
  public MetierTopologieRiviereCasier() {
    super();
    abscisse_= 0;
    casierRattache_= null;
    biefRattache_= null;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    abscisse_= 0;
    casierRattache_= null;
    biefRattache_= null;
    super.dispose();
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Rivi�re-Casier");
    res[1]= getS("abscisse")+" : " + abscisse_;
    return res;
  }
  /*** MetierTopologieRiviereCasier ***/
  // attributs
  private double abscisse_;
  public double abscisse() {
    return abscisse_;
  }
  public void abscisse(double s) {
    if (abscisse_==s) return;
    abscisse_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisse");
  }
  private MetierCasier casierRattache_;
  public MetierCasier casierRattache() {
    return casierRattache_;
  }
  public void casierRattache(MetierCasier s) {
    if ((casierRattache_!=null)&&(s!=null)) {
      if (casierRattache_==s) return;
    }
    casierRattache_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "casierRattache");
  }
  private MetierBief biefRattache_;
  public MetierBief biefRattache() {
    return biefRattache_;
  }
  public void biefRattache(MetierBief s) {
    if (biefRattache_==s) return;
    biefRattache_= s;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "biefRattache");
  }
  // m�thodes
  @Override
  public boolean isRiviereCasier() {
    return true;
  }
  @Override
  public double getAbscisse() {
    return abscisse();
  }
  @Override
  public void setAbscisse(double abscisse) {
    abscisse(abscisse);
  }
  @Override
  public MetierCasier getCasierRattache() {
    return casierRattache();
  }
  @Override
  public void setCasierRattache(MetierCasier casierRattache) {
    casierRattache(casierRattache);
  }
  @Override
  public MetierBief getBiefRattache() {
    return biefRattache();
  }
  @Override
  public void setBiefRattache(MetierBief biefRattache) {
    biefRattache(biefRattache);
  }
}
