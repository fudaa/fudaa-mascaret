/*
 * @creation     2000-10-20
 * @modification $Date: 2007-11-20 11:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.hydraulique1d.metier.evenement;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * @version $Id: H1dObjetEvent.java,v 1.2 2007-11-20 11:43:26 bmarchan Exp $
 * @author Axel von Arnim
 */
public class H1dObjetEvent {
  private String msg_;
  private MetierHydraulique1d source_;
  private String field_;
  private boolean consumed_;

  /**
   * Initialise tous les champs.
   */
  public H1dObjetEvent() {
    msg_ = "";
    source_ = null;
    field_ = "";
    consumed_ = false;
  }

  public void dispose() {
    setConsomme();
    msg_ = null;
    source_ = null;
    field_ = null;
  }

  // methodes
  public void setMessage(final String _msg) {
    msg_ = _msg;
  }

  public void setSource(final MetierHydraulique1d _src) {
    source_ = _src;
  }

  public void setChamp(final String _field) {
    field_ = _field;
  }

  private void setConsomme() {
    consumed_ = true;
  }

  public String getMessage() {
    return msg_;
  }

  public MetierHydraulique1d getSource() {
    return source_;
  }

  public String getChamp() {
    return field_;
  }

  public boolean isConsomme() {
    return consumed_;
  }
}
