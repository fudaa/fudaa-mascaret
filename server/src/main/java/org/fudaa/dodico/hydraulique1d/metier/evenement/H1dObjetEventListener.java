/**
 * @creation     2000-10-20
 * @modification $Date: 2007-11-20 11:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.hydraulique1d.metier.evenement;

/**
 * @version $Id: H1dObjetEventListener.java,v 1.2 2007-11-20 11:43:26 bmarchan Exp $
 * @author Axel von Arnim
 */
public interface H1dObjetEventListener {
  /**
   * @param _e l'evt de creation
   */
  void objetCree(H1dObjetEvent _e);

  /**
   * @param _e l'evt de suppression
   */
  void objetSupprime(H1dObjetEvent _e);

  /**
   * @param _e l'evt de modification
   */
  void objetModifie(H1dObjetEvent _e);
}
