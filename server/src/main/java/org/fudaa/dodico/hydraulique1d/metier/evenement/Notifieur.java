package org.fudaa.dodico.hydraulique1d.metier.evenement;

import java.util.HashSet;
import java.util.Iterator;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * Une classe pour notifier les evenements survenant sur les objets metiers.
 * @author bmarchan
 *
 */
public class Notifieur {
  public static Notifieur notifier_=null;
  public boolean muet_=false;

  HashSet listeners_=new HashSet();
  
  public static Notifieur getNotifieur() {
	if (notifier_==null) notifier_=new Notifieur();
	return notifier_;
  }
  
  public void addObjetEventListener(H1dObjetEventListener _l) {
    listeners_.add(_l);
  }
  
  public void removeObjetEventListener(H1dObjetEventListener _l) {
    listeners_.remove(_l);
  }
  
  public synchronized void fireObjetCree(String msg, MetierHydraulique1d o) {
    if(muet_) return ;

    H1dObjetEvent e=new H1dObjetEvent();

    e.setMessage(msg);
    e.setSource(o);
    e.setChamp("");

    if ((msg != null) && ((msg.startsWith(getPlus())) || (msg.startsWith(getMoins())) || (msg.startsWith(getMod())))) {
      msg = msg.substring(2);
    }
    e.setMessage(getPlus() + msg);
    for (Iterator i=listeners_.iterator(); i.hasNext(); ) {
      ((H1dObjetEventListener)i.next()).objetCree(e);
    }
    e.dispose();
  }

  public synchronized void fireObjetModifie(String msg, MetierHydraulique1d o, String champ){
    if(muet_) return ;

    H1dObjetEvent e=new H1dObjetEvent();

    e.setMessage(msg);
    e.setSource(o);
    e.setChamp(champ);

    if ((msg != null) && ((msg.startsWith(getPlus())) || (msg.startsWith(getMoins())) || (msg.startsWith(getMod())))) {
      msg = msg.substring(2);
    }
    e.setMessage(getMod() + msg);
    for (Iterator i=listeners_.iterator(); i.hasNext(); ) {
      ((H1dObjetEventListener)i.next()).objetModifie(e);
    }
    e.dispose();
  }

  public synchronized void fireObjetSupprime(String msg, MetierHydraulique1d o){
    if(muet_) return ;
	  
    H1dObjetEvent e=new H1dObjetEvent();

    e.setMessage(msg);
    e.setSource(o);
    e.setChamp("");
    
    if ((msg != null) && ((msg.startsWith(getPlus())) || (msg.startsWith(getMoins())) || (msg.startsWith(getMod())))) {
      msg = msg.substring(2);
    }
    e.setMessage(getMoins() + msg);
    for (Iterator i=listeners_.iterator(); i.hasNext(); ) {
    	((H1dObjetEventListener)i.next()).objetSupprime(e);
    }
    e.dispose();
  }

  public void setEventMuet(boolean _b){
    muet_=_b;
  }

  public boolean getEventMuet(){
    return muet_;
  }

  private String getMod() {
    return "M ";
  }

  private String getMoins() {
    return "- ";
  }

  private String getPlus() {
    return "+ ";
  }
}
