package org.fudaa.dodico.hydraulique1d.metier.geometrie;


/**
 * Une classe point
 */

public class MetierPoint extends MetierPoint2D
{
  public double z = (double)0;

  public MetierPoint ()
  {
	  super();
  }

  public MetierPoint (double _x, double _y, double _z)
  {
    x = _x;
    y = _y;
    z = _z;
  }

}
