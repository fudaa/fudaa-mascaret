/**
 * @file         MetierLoiGeometrique.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:43:20 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier d'une "loi g�om�trique" des donn�es hydraulique.
 * D�finie une courbe cote = f(distance).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:20 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLoiGeometrique extends MetierLoiHydraulique {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierLoiGeometrique) {
      MetierLoiGeometrique l= (MetierLoiGeometrique)_o;
      d((double[])l.d().clone());
      z((double[])l.z().clone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLoiGeometrique l= new MetierLoiGeometrique();
    l.initialise(this);
    return l;
  }
  /*** MetierLoiGeometrique ***/
  // constructeurs
  public MetierLoiGeometrique() {
    super();
    nom_= getS("loi profil")+" 9999999999 "+getS("crete");
    d_= new double[0];
    z_= new double[0];

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    d_= null;
    z_= null;
    super.dispose();
  }
  // attributs
  private double[] d_;
  public double[] d() {
    return d_;
  }
  public void d(double[] d) {
    if (Arrays.equals(d,d_)) return;
    d_= d;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "d");
  }
  private double[] z_;
  public double[] z() {
    return z_;
  }
  public void z(double[] z) {
    if (Arrays.equals(z,z_)) return;
    z_= z;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
  }
  // methodes
  public double gdu(int i) {
    return d_[i];
  }
  public void sdu(int i, double v) {
    d_[i]= v;
  }
  public double gzu(int i) {
    return z_[i];
  }
  public void szu(int i, double v) {
    z_[i]= v;
  }
  @Override
  public void creePoint(int indice) {
    int length= Math.min(d_.length, z_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[] newd= new double[length + 1];
    double[] newz= new double[length + 1];
    for (int i= 0; i < indice; i++) {
      newd[i]= d_[i];
      newz[i]= z_[i];
    }
    for (int i= indice; i < length; i++) {
      newd[i + 1]= d_[i];
      newz[i + 1]= z_[i];
    }
    d(newd);
    z(newz);
  }
  @Override
  public void supprimePoints(int[] indices) {
    int length= Math.min(d_.length, z_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[] newd= new double[length - nsup];
    double[] newz= new double[length - nsup];
    for (int i= 0; i < length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (indices[j] != i) {
          newd[i]= d_[i];
          newz[i]= z_[i];
        }
      }
    }
    d(newd);
    z(newz);
  }
  @Override
  public String typeLoi() {
    return "Geometrique";
  }
  @Override
  public int nbPoints() {
    return Math.min(d_.length, z_.length);
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  // on suppose colonne0:d et colonne1:z
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < d_.length)
          d_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < z_.length)
          z_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:d et colonne1:z
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < d_.length)
          return d_[ligne];
          return Double.NaN;
      case 1 :
        if (ligne < z_.length)
          return z_[ligne];
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {

    double[][] points = CtuluLibArray.transpose(pts);

	  if (points == null || points.length == 0) {
		   d_ = new double[0];
		   z_ = new double[0];
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "d");
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
	    	return;

	    } else {

    boolean debitModif = !Arrays.equals(d_,points[0]);
    boolean zModif = !Arrays.equals(z_,points[1]);

    if (debitModif || zModif) {
      d_ = points[0];
      z_ = points[1];
      if (debitModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "d");
      if (zModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
    }
	    }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[2][d_.length];
    tableau[0]= (double[])d_.clone();
    tableau[1]= (double[])z_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
