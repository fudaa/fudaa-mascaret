/**
 * @file         MetierLoiHydrogramme.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:43:20 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier d'une "loi hydrogramme" des donn�es hydraulique.
 * D�finie une courbe d�bit = f(temps).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:20 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLoiHydrogramme extends MetierLoiHydraulique {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierLoiHydrogramme) {
      MetierLoiHydrogramme l= (MetierLoiHydrogramme)_o;
      t((double[])l.t().clone());
      q((double[])l.q().clone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLoiHydrogramme l= new MetierLoiHydrogramme();
    l.initialise(this);
    return l;
  }
  /*** MetierLoiGeometrique ***/
  // constructeurs
  public MetierLoiHydrogramme() {
    super();
    nom_= getS("loi")+" 9999999999 "+getS("hydrogramme");
    t_= new double[0];
    q_= new double[0];

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    t_= null;
    q_= null;
    super.dispose();
  }
  // attributs
  private double[] t_;
  public double[] t() {
    return t_;
  }
  public void t(double[] t) {
    if (Arrays.equals(t,t_)) return;
    t_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
  }
  private double[] q_;
  public double[] q() {
    return q_;
  }
  public void q(double[] q) {
    if (Arrays.equals(q,q_)) return;
    q_= q;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "q");
  }
  // methodes
  public double gtu(int i) {
    return t_[i];
  }
  public void stu(int i, double v) {
    t_[i]= v;
  }
  public double gqu(int i) {
    return q_[i];
  }
  public void squ(int i, double v) {
    q_[i]= v;
  }
  @Override
  public void creePoint(int indice) {
    int length= Math.min(t_.length, q_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[] newt= new double[length + 1];
    double[] newq= new double[length + 1];
    for (int i= 0; i < indice; i++) {
      newt[i]= t_[i];
      newq[i]= q_[i];
    }
    for (int i= indice; i < length; i++) {
      newt[i + 1]= t_[i];
      newq[i + 1]= q_[i];
    }
    t(newt);
    q(newq);
  }
  @Override
  public void supprimePoints(int[] indices) {
    int length= Math.min(t_.length, q_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[] newt= new double[length - nsup];
    double[] newq= new double[length - nsup];
    for (int i= 0; i < length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (indices[j] != i) {
          newt[i]= t_[i];
          newq[i]= q_[i];
        }
      }
    }
    t(newt);
    q(newq);
  }
  @Override
  public String typeLoi() {
    return "Hydrogramme";
  }
  @Override
  public int nbPoints() {
    return Math.min(t_.length, q_.length);
  }
  @Override
  public boolean verifiePermanent() {
    if ((t_ == null) || (q_ == null) || (q_.length == 0))
      return false;
    boolean res= true;
    double q0= q_[0];
    for (int i= 1; i < q_.length; i++) {
      if (q_[i] != q0) {
        res= false;
        break;
      }
    }
    System.err.println(
      "MetierLoiHydrogramme: verifiePermanent :" + res + " q.length=" + q_.length);
    return res;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    if ((t_ == null) || (t_.length == 0))
      return false;
    boolean res= false;
    for (int i= 1; i < t_.length; i++) {
      if (t_[i] <= t_[i - 1]) {
        res= false;
      }
    }
    return res;
  }
  // on suppose colonne0:t et colonne1:q
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          t_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < q_.length)
          q_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:t et colonne1:q
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          return t_[ligne];
        else
          return Double.NaN;
      case 1 :
        if (ligne < q_.length)
          return q_[ligne];
        else
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {

	  double[][] points = CtuluLibArray.transpose(pts);
	  if (points == null || points.length == 0) {
		   t_ = new double[0];
		   q_ = new double[0];
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "q");
	    	return;

	    } else {

    boolean tModif = !Arrays.equals(t_,points[0]);
    boolean qModif = !Arrays.equals(q_,points[1]);

    if (tModif || qModif) {
      t_ = points[0];
      q_ = points[1];
      if (tModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
      if (qModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "q");
    }
  }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[2][t_.length];
    tableau[0]= (double[])t_.clone();
    tableau[1]= (double[])q_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
