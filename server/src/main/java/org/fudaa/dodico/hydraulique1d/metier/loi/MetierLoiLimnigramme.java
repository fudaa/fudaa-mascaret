/**
 * @file         MetierLoiLimnigramme.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:43:19 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier d'une "loi limnigramme" des donn�es hydraulique.
 * D�finie une courbe cote = f(temps).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:19 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLoiLimnigramme extends MetierLoiHydraulique {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierLoiLimnigramme) {
      MetierLoiLimnigramme l= (MetierLoiLimnigramme)_o;
      t((double[])l.t().clone());
      z((double[])l.z().clone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLoiLimnigramme l= new MetierLoiLimnigramme();
    l.initialise(this);
    return l;
  }
  /*** MetierLoiLimnigramme ***/
  // constructeurs
  public MetierLoiLimnigramme() {
    super();
    nom_= getS("loi")+" 9999999999 "+getS("limnigramme");
    t_= new double[0];
    z_= new double[0];

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    t_= null;
    z_= null;
    super.dispose();
  }
  // attributs
  private double[] t_;
  public double[] t() {
    return t_;
  }
  public void t(double[] t) {
    if (Arrays.equals(t,t_)) return;
    t_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
  }
  private double[] z_;
  public double[] z() {
    return z_;
  }
  public void z(double[] z) {
    if (Arrays.equals(z,z_)) return;
    z_= z;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
  }
  // methodes
  public double gtu(int i) {
    return t_[i];
  }
  public void stu(int i, double v) {
    t_[i]= v;
  }
  public double gzu(int i) {
    return z_[i];
  }
  public void szu(int i, double v) {
    z_[i]= v;
  }
  @Override
  public void creePoint(int indice) {
    int length= Math.min(t_.length, z_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[] newt= new double[length + 1];
    double[] newz= new double[length + 1];
    for (int i= 0; i < indice; i++) {
      newt[i]= t_[i];
      newz[i]= z_[i];
    }
    for (int i= indice; i < length; i++) {
      newt[i + 1]= t_[i];
      newz[i + 1]= z_[i];
    }
    t(newt);
    z(newz);
  }

  @Override
  public void supprimePoints(int[] indices) {
    int length= Math.min(t_.length, z_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[] newt= new double[length - nsup];
    double[] newz= new double[length - nsup];
    for (int i= 0; i < length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (indices[j] != i) {
          newt[i]= t_[i];
          newz[i]= z_[i];
        }
      }
    }
    t(newt);
    z(newz);
  }
  @Override
  public String typeLoi() {
    return "Limnigramme";
  }
  @Override
  public int nbPoints() {
    return Math.min(t_.length, z_.length);
  }
  @Override
  public boolean verifiePermanent() {
    if ((t_ == null) || (z_ == null) || (z_.length == 0))
      return false;
    boolean res= false;
    double q0= z_[0];
    for (int i= 1; i < z_.length; i++) {
      if (z_[i] != q0) {
        res= false;
        break;
      }
    }
    return res;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    if ((t_ == null) || (t_.length == 0))
      return false;
    boolean res= true;
    for (int i= 1; i < t_.length; i++) {
      if (t_[i] <= t_[i - 1]) {
        res= false;
      }
    }
    return res;
  }
  public boolean verifieCote(double cote) {
    if ((z_ == null) || (z_.length == 0))
      return false;
    boolean res= true;
    for (int i= 0; i < z_.length; i++) {
      if (z_[i] <= cote) {
        res= false;
      }
    }
    return res;
  }
  // on suppose colonne0:t et colonne1:z
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          t_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < z_.length)
          z_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:t et colonne1:z
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          return t_[ligne];
        else
          return Double.NaN;
      case 1 :
        if (ligne < z_.length)
          return z_[ligne];
        else
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {
    double[][] points = CtuluLibArray.transpose(pts);

	  if (points == null || points.length == 0) {
		   t_ = new double[0];
		   z_ = new double[0];
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
	    	return;

	    } else {

    boolean tModif = !Arrays.equals(t_,points[0]);
    boolean zModif = !Arrays.equals(z_,points[1]);

    if (tModif || zModif) {
      t_ = points[0];
      z_ = points[1];
      if (tModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
      if (zModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
    }
  }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[2][t_.length];
    tableau[0]= (double[])t_.clone();
    tableau[1]= (double[])z_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
