/**
 * @file         MetierLoiOuvertureVanne.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:43:20 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier d'une "loi ouverture d'une vanne" des donn�es hydraulique.
 * D�finie 2 courbes : cote inf�rieure = f(temps) et cote sup�rieure = f(temps).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:20 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLoiOuvertureVanne extends MetierLoiHydraulique {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierLoiOuvertureVanne) {
      MetierLoiOuvertureVanne l= (MetierLoiOuvertureVanne)_o;
      t((double[])l.t().clone());
      zInf((double[])l.zInf().clone());
      zSup((double[])l.zSup().clone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLoiOuvertureVanne l=
      new MetierLoiOuvertureVanne();
    l.initialise(this);
    return l;
  }
  /*** MetierLoiGeometrique ***/
  // constructeurs
  public MetierLoiOuvertureVanne() {
    super();
    nom_= getS("loi")+" 9999999999 "+getS("ouverture vanne");
    t_= new double[0];
    zInf_= new double[0];
    zSup_= new double[0];

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    t_= null;
    zInf_= null;
    zSup_= null;
    super.dispose();
  }
  // attributs
  private double[] t_;
  public double[] t() {
    return t_;
  }
  public void t(double[] t) {
    if (Arrays.equals(t,t_)) return;
    t_= t;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
  }
  private double[] zInf_;
  public double[] zInf() {
    return zInf_;
  }
  public void zInf(double[] zInf) {
    if (Arrays.equals(zInf,zInf_)) return;
    zInf_= zInf;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zInf");
  }
  private double[] zSup_;
  public double[] zSup() {
    return zSup_;
  }
  public void zSup(double[] zSup) {
    if (Arrays.equals(zSup,zSup_)) return;
    zSup_= zSup;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zSup");
  }
  // methodes
  public double gzInfu(int i) {
    return zInf_[i];
  }
  public void szInfu(int i, double v) {
    zInf_[i]= v;
  }
  public double gzSupu(int i) {
    return zSup_[i];
  }
  public void szSupu(int i, double v) {
    zSup_[i]= v;
  }
  @Override
  public void creePoint(int i) {}
  @Override
  public void supprimePoints(int[] i) {}
  @Override
  public String typeLoi() {
    return "OuvertureVanne";
  }
  @Override
  public int nbPoints() {
    return Math.min(t_.length, Math.min(zInf_.length, zSup_.length));
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  // on suppose colonne0:t  colonne1:zInf et colonne2:zSup
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          t_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < zInf_.length)
          zInf_[ligne]= valeur;
        break;
      case 2 :
        if (ligne < zSup_.length)
          zSup_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne1:zInf et colonne2:zSup
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < t_.length)
          return t_[ligne];
        else
          return Double.NaN;
      case 1 :
        if (ligne < zInf_.length)
          return zInf_[ligne];
        else
          return Double.NaN;
      case 2 :
        if (ligne < zSup_.length)
          return zSup_[ligne];
        else
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {
    double[][] points = CtuluLibArray.transpose(pts);

    if (points == null || points.length == 0) {
		   t_ = new double[0];
		   zInf_ = new double[0];
		   zSup_ = new double[0];
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zInf");
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zSup");
	    	return;

	    } else {


    boolean tModif    = !Arrays.equals(t_,points[0]);
    boolean zInfModif = !Arrays.equals(zInf_,points[1]);
    boolean zSupModif = !Arrays.equals(zSup(),points[2]);

    if (tModif || zInfModif || zSupModif) {
      t_ = points[0];
      zInf_ = points[1];
      zSup_ = points[2];
      if (tModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "t");
      if (zInfModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zInf");
      if (zSupModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zSup");
    }
	    }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[3][t_.length];
    tableau[0]= (double[])t_.clone();
    tableau[1]= (double[])zInf_.clone();
    tableau[2]= (double[])zSup_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
