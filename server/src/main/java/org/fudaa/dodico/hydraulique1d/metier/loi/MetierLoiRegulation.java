/**
 * @file         MetierLoiRegulation.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:43:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier d'une "loi de r�gulation" des donn�es hydraulique.
 * D�finie une courbe d�bit amont = f(cote aval).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:18 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLoiRegulation extends MetierLoiHydraulique {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierLoiRegulation) {
      MetierLoiRegulation l= (MetierLoiRegulation)_o;
      qAmont((double[])l.qAmont().clone());
      zAval((double[])l.zAval().clone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLoiRegulation l= new MetierLoiRegulation();
    l.initialise(this);
    return l;
  }
  /*** MetierLoiRegulation ***/
  // constructeurs
  public MetierLoiRegulation() {
    super();
    nom_= getS("loi")+" 9999999999 "+getS("r�gulation");
    qAmont_= new double[0];
    zAval_= new double[0];

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    qAmont_= null;
    zAval_= null;
    super.dispose();
  }
  // attributs
  private double[] qAmont_;
  public double[] qAmont() {
    return qAmont_;
  }
  public void qAmont(double[] qAmont) {
    if (Arrays.equals(qAmont,qAmont_)) return;
    qAmont_= qAmont;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "qAmont");
  }
  private double[] zAval_;
  public double[] zAval() {
    return zAval_;
  }
  public void zAval(double[] zAval) {
    if (Arrays.equals(zAval,zAval_)) return;
    zAval_= zAval;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zAval");
  }
  // methodes
  public double gqAmontu(int i) {
    return qAmont_[i];
  }
  public void sqAmontu(int i, double v) {
    qAmont_[i]= v;
  }
  public double gzAvalu(int i) {
    return zAval_[i];
  }
  public void szAvalu(int i, double v) {
    zAval_[i]= v;
  }
  @Override
  public void creePoint(int i) {}
  @Override
  public void supprimePoints(int[] i) {}
  @Override
  public String typeLoi() {
    return "Regulation";
  }
  @Override
  public int nbPoints() {
    return Math.min(qAmont_.length, zAval_.length);
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  // on suppose colonne0:qAmont et colonne1:zAval
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < qAmont_.length)
          qAmont_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < zAval_.length)
          zAval_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:qAmont et colonne1:zAval
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < qAmont_.length)
          return qAmont_[ligne];
        else
          return Double.NaN;
      case 1 :
        if (ligne < zAval_.length)
          return zAval_[ligne];
        else
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {
    double[][] points = CtuluLibArray.transpose(pts);

    if (points == null || points.length == 0) {
    	qAmont_ = new double[0];
    	zAval_ = new double[0];
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "qAmont");
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zAval");
	    	return;

	    } else {

    boolean qAmontModif = !Arrays.equals(qAmont_,points[0]);
    boolean zAvalModif  = !Arrays.equals(zAval_,points[1]);

    if (qAmontModif || zAvalModif) {
      qAmont_ = points[0];
      zAval_ = points[1];
      if (qAmontModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "qAmont");
      if (zAvalModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "zAval");
    }
	    }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[2][qAmont_.length];
    tableau[0]= (double[])qAmont_.clone();
    tableau[1]= (double[])zAval_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
