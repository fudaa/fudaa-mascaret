/**
 * @file         MetierLoiTarage.java
 * @creation     2000-08-10
 * @modification $Date: 2007-11-20 11:43:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.loi;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier d'une "loi de tarage" des donn�es hydraulique.
 * D�finie soit une courbe d�bit = f(cote) ou cote = f(d�bit) suivant la valeur de l'indicateur amonZ.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:18 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierLoiTarage extends MetierLoiHydraulique {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierLoiTarage) {
      MetierLoiTarage l= (MetierLoiTarage)_o;
      amont(l.amont());
      z((double[])l.z().clone());
      q((double[])l.q().clone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierLoiTarage l= new MetierLoiTarage();
    l.initialise(this);
    return l;
  }
  /*** MetierLoiTarage ***/
  // constructeurs
  public MetierLoiTarage() {
    super();
    nom_= getS("loi")+" 9999999999 "+getS("tarage");
    amonz_= true;
    z_= new double[0];
    q_= new double[0];

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    amonz_= true;
    z_= null;
    q_= null;
    super.dispose();
  }
  // attributs
  private boolean amonz_;
  public boolean amont() {
    return amonz_;
  }
  public void amont(boolean z) {
    amonz_= z;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "amont");
  }
  private double[] z_;
  public double[] z() {
    return z_;
  }
  public void z(double[] z) {
    if (Arrays.equals(z,z_)) return;
    z_= z;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
  }
  private double[] q_;
  public double[] q() {
    return q_;
  }
  public void q(double[] q) {
    if (Arrays.equals(q,q_)) return;
    q_= q;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "q");
  }
  // methodes
  public double gzu(int i) {
    return z_[i];
  }
  public void szu(int i, double v) {
    z_[i]= v;
  }
  public double gqu(int i) {
    return q_[i];
  }
  public void squ(int i, double v) {
    q_[i]= v;
  }
  @Override
  public void creePoint(int indice) {
    int length= Math.min(z_.length, q_.length);
    if ((indice < 0) || (indice >= length))
      return;
    double[] newz= new double[length + 1];
    double[] newq= new double[length + 1];
    for (int i= 0; i < indice; i++) {
      newz[i]= z_[i];
      newq[i]= q_[i];
    }
    for (int i= indice; i < length; i++) {
      newz[i + 1]= z_[i];
      newq[i + 1]= q_[i];
    }
    z(newz);
    q(newq);
  }
  @Override
  public void supprimePoints(int[] indices) {
    int length= Math.min(z_.length, q_.length);
    int nsup= 0;
    for (int i= 0; i < indices.length; i++) {
      if ((indices[i] >= 0) && (indices[i] < length))
        nsup++;
    }
    double[] newz= new double[length - nsup];
    double[] newq= new double[length - nsup];
    for (int i= 0; i < length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (indices[j] != i) {
          newz[i]= z_[i];
          newq[i]= q_[i];
        }
      }
    }
    z(newz);
    q(newq);
  }
  @Override
  public String typeLoi() {
    return "Tarage";
  }
  @Override
  public int nbPoints() {
    return Math.min(z_.length, q_.length);
  }
  @Override
  public boolean verifiePermanent() {
    return false;
  }
  @Override
  public boolean verifieTempsNonPermanent() {
    return true;
  }
  public boolean verifieCote(double cote) {
    if ((z_ == null) || (z_.length == 0))
      return false;
    boolean res= true;
    for (int i= 0; i < z_.length; i++) {
      if (z_[i] <= cote) {
        res= false;
      }
    }
    return res;
  }
  // on suppose colonne0:q et colonne1:z
  @Override
  public void setValeur(double valeur, int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < q_.length)
          q_[ligne]= valeur;
        break;
      case 1 :
        if (ligne < z_.length)
          z_[ligne]= valeur;
        break;
    }
  }
  // on suppose colonne0:q et colonne1:z
  @Override
  public double getValeur(int ligne, int colonne) {
    switch (colonne) {
      case 0 :
        if (ligne < q_.length)
          return q_[ligne];
        else
          return Double.NaN;
      case 1 :
        if (ligne < z_.length)
          return z_[ligne];
        else
          return Double.NaN;
      default :
        return Double.NaN;
    }
  }
  @Override
  public void setPoints(double[][] pts) {
    double[][] points = CtuluLibArray.transpose(pts);

    if (points == null || points.length == 0) {
    	q_ = new double[0];
    	z_ = new double[0];
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "q");
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
	    	return;

	    } else {
    boolean zModif = !Arrays.equals(q_,points[0]);
    boolean qModif = !Arrays.equals(z_,points[1]);

    if (zModif || qModif) {
      z_ = points[1];
      q_ = points[0];
      if (zModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "z");
      if (qModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "q");
    }
    q(points[0]);
    z(points[1]);
	    }
  }
  @Override
  public double[][] pointsToDoubleArray() {
    double[][] tableau= new double[2][q_.length];
    tableau[0]= (double[])q_.clone();
    tableau[1]= (double[])z_.clone();
    return CtuluLibArray.transpose(tableau);
  }
}
