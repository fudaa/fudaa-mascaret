/**
 * @file         MetierLoiTracer.java
 * @creation     2006-02-27
 * @modification $Date: 2007-11-20 11:43:19 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */

package org.fudaa.dodico.hydraulique1d.metier.loi;

import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * Impl�mentation de l'objet m�tier d'une "loi tracer" des donn�es de qualit� d'eau.
 * D�finie des courbes concentration = f(temps).
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:19 $ by $Author: bmarchan $
 * @author       Olivier Pasteur
 */
public class MetierLoiTracer extends MetierLoiHydraulique {

  @Override
    public void initialise(MetierHydraulique1d _o) {
        super.initialise(_o);
        if (_o instanceof MetierLoiTracer) {
            MetierLoiTracer l = (MetierLoiTracer) _o;
            conc((double[][])l.conc().clone());
        }
    }

  @Override
    final public MetierHydraulique1d creeClone() {
        MetierLoiTracer l = new MetierLoiTracer();
        l.initialise(this);
        return l;
    }

     // constructeurs
    public MetierLoiTracer() {
        super();
        nom_ = getS("loi")+" 9999999999 "+getS("tracer");
        conc_ = new double[0][0];

        notifieObjetCree();
    }

  @Override
    public void dispose() {
      conc_= null;
      super.dispose();
  }


    /**
     * getValeur
     *
     * @param ligne int
     * @param colonne int
     * @return double
     */
  @Override
    public double getValeur(int ligne, int colonne) {
        try {
                return conc_[colonne][ligne];
        } catch (ArrayIndexOutOfBoundsException e) {
            return Double.NaN;
        }
    }



    /**
     * nbPoints
     *
     * @return int
     */
  @Override
    public int nbPoints() {
    	if (conc_ == null) return 0;
        if (conc_.length>0)
            return conc_[0].length;
        else
            return 0;
    }




    /**
     * pointsToDoubleArray
     *
     * @return double[][]
     *   method
     */
  @Override
    public double[][] pointsToDoubleArray() {

        return conc_;//CtuluLibArray.transpose(tableau);

    }



    /**
     * setPoints
     *
     * @param points double[][]
     */
  @Override
    public void setPoints(double[][] points) {
        CtuluLibMessage.println("MetierLoiTracer setPoints",points);


    boolean concModif  = !CtuluLibArray.equals(points,conc_);
    CtuluLibMessage.println("concModif="+concModif);


    if (concModif) {
      conc_ = points;
      CtuluLibMessage.println("MetierLoiTracer conc_",conc_);

      if (concModif)
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "conc");

    }

    }

    /**
     * setValeur
     *
     * @param valeur double
     * @param ligne int
     * @param colonne int
     *   // on suppose colonne0:t et les suivantes:conc
     */
  @Override
    public void setValeur(double valeur, int ligne, int colonne) {
        try {
                conc_[colonne][ligne] = valeur;
        } catch (ArrayIndexOutOfBoundsException e) {}
    }

    // attributs
        /**
         * conc
         * @param newConc double[][]
         */
        private double[][] conc_;
        public void conc(double[][] newConc) {
            if (Arrays.equals(newConc,conc_)) return;
            conc_= newConc;
            Notifieur.getNotifieur().fireObjetModifie(toString(), this, "conc");
        }

        /**
         * conc
         * @return double[][]
         */
        public double[][] conc() {
            return conc_;
        }




    /**
     * typeLoi
     *
     * @return String
     */
  @Override
    public String typeLoi() {
      return "Tracer";
    }
    /**
     * Modifie la taille du tableau en fonction du nombre de traceur
     *
     * @param nbLigne int
     * @param nbColonne int
     *
     */
    public void setTailleTableau(int nbColonne) {
        try {
                int nbLigne = 0;
                conc_= new double[nbLigne][nbColonne];

        } catch (ArrayIndexOutOfBoundsException e) {
        System.out.println("Une erreur est survenue lors du redimenssionnement du tableau");}
    }


}
