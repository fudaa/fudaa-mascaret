package org.fudaa.dodico.hydraulique1d.metier.qualitedeau;

import java.util.Arrays;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * @file         MetierConcentrationInitiale.java
 * @creation     2006-02-28
 * @modification $Date: 2007-11-20 11:42:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
public class MetierConcentrationInitiale extends MetierHydraulique1d {
  @Override
    final public MetierHydraulique1d creeClone() {
      MetierConcentrationInitiale c=
        new MetierConcentrationInitiale();
      c.initialise(this);
      return c;
    }
  @Override
    public void initialise(MetierHydraulique1d _o) {
        if (_o instanceof MetierConcentrationInitiale) {
             MetierConcentrationInitiale c= (MetierConcentrationInitiale)_o;
             numeroBief(c.numeroBief());
             abscisse(c.abscisse());
             concentrations((double[])c.concentrations().clone());
     }
    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
        numeroBief_=0;
        abscisse_ = 0;
        concentrations_=null;
        super.dispose();
    }

  @Override
    final public String toString() {
      String s= "Concentration initiale";
      return s;
    }
    public MetierConcentrationInitiale() {
        super();
        numeroBief_ = 1;
        abscisse_=0;
        concentrations_ = new double[0];

        notifieObjetCree();
    }

    /**
     * abscisse
     *
     * @return double
     */
    private double abscisse_;
    public double abscisse() {
        return abscisse_;
    }

    /**
     * abscisse
     *
     * @param newAbscisse double
     */
    public void abscisse(double abscisse) {
        if (abscisse_==abscisse) return;
        abscisse_ = abscisse;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "abscisse");

    }


    /**
     * concentrations
     *
     * @return double[]
     */
    private double[] concentrations_;
    public double[] concentrations() {
        return concentrations_;
    }

    /**
     * concentrations
     *
     * @param newConcentrations double[]
     */
    public void concentrations(double[] newConcentrations) {
        if (Arrays.equals(newConcentrations,concentrations_)) return;
        concentrations_ = newConcentrations;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "concentrations");

    }



    /**
     * numeroBief
     *
     * @return int
     */
        private int numeroBief_;
    public int numeroBief() {
        return numeroBief_;
    }

    /**
     * numeroBief
     *
     * @param newNumeroBief int
     */
    public void numeroBief(int newNumeroBief) {
        if (numeroBief_==newNumeroBief) return;
        numeroBief_ = newNumeroBief;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "numeroBief");

    }

}
