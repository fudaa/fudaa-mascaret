package org.fudaa.dodico.hydraulique1d.metier.qualitedeau;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

        /*
         * @file         MetierLimiteQualiteDEau.java
         * @creation     2006-02-28
         * @modification $Date: 2007-11-20 11:42:56 $
         * @license      GNU General Public License 2
         * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
         * @mail         devel@fudaa.org
 */
public class MetierLimiteQualiteDEau extends MetierHydraulique1d {
    public MetierLimiteQualiteDEau() {
       super();
       typeCondLimiteTracer_=EnumMetierTypeCondLimiteTracer.DIRICHLET;
       loi_=null;

       notifieObjetCree();
    }


  @Override
    final public String toString() {
      String s= "Limite qualit� d'eau";
      return s;
    }
    /**
     * creeClone
     *
     * @return MetierHydraulique1d
     */
  @Override
    public MetierHydraulique1d creeClone() {
        MetierLimiteQualiteDEau p=new MetierLimiteQualiteDEau();
        p.initialise(this);
        return p;

    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
       typeCondLimiteTracer_=EnumMetierTypeCondLimiteTracer.DIRICHLET;
       loi_=null;
    }


    /**
     * initialise
     *
     * @param o MetierHydraulique1d

     */
  @Override
    public void initialise(MetierHydraulique1d _o) {
        if (_o instanceof MetierLimiteQualiteDEau) {
            MetierLimiteQualiteDEau l = (MetierLimiteQualiteDEau)_o;
            typeCondLimiteTracer(l.typeCondLimiteTracer());
            if (l.loi() != null)  loi((MetierLoiTracer) l.loi());
         }
    }

    /**
     * loi
     *
     * @param newLoi MetierLoiTracer
     */

    public void loi(MetierLoiTracer newLoi) {
        if (loi_==newLoi) return;
        loi_ = newLoi;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");

    }

    /**
     * loi
     *
     * @return MetierLoiTracer
     */
    private MetierLoiTracer loi_;
    public MetierLoiTracer loi() {
        return loi_;
    }





    /**
     * typeCondLimiteTracer
     *
     * @return EnumMetierTypeCondLimiteTracer
     */
    private EnumMetierTypeCondLimiteTracer typeCondLimiteTracer_;
    public EnumMetierTypeCondLimiteTracer typeCondLimiteTracer() {
        return typeCondLimiteTracer_;
    }

    /**
     * typeCondLimiteTracer
     *
     * @param newTypeCondLimiteTracer EnumMetierTypeCondLimiteTracer
     */
    public void typeCondLimiteTracer(EnumMetierTypeCondLimiteTracer
                                     newTypeCondLimiteTracer) {
        if (typeCondLimiteTracer_.value() == newTypeCondLimiteTracer.value())   return;
        typeCondLimiteTracer_ = newTypeCondLimiteTracer;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "typeCondLimiteTracer");

    }
}
