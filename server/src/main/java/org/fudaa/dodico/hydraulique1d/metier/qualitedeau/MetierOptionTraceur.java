package org.fudaa.dodico.hydraulique1d.metier.qualitedeau;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/*
 * @file         MetierOptionTraceur.java
 * @creation     2006-02-28
 * @modification $Date: 2007-11-20 11:42:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class MetierOptionTraceur extends MetierHydraulique1d {
    public MetierOptionTraceur() {
        super();
        diffusionDuTraceur_= true;
        convectionDuTraceur_=true;

        notifieObjetCree();
    }

  @Override
    final public String toString() {
      String s= "Option Traceur";
      return s;
    }

    /**
     * convectionDuTraceur
     *
     * @param newConvectionDuTraceur boolean
     */
    public void convectionDuTraceur(boolean newConvectionDuTraceur) {
        if (convectionDuTraceur_==newConvectionDuTraceur) return;
        convectionDuTraceur_ = newConvectionDuTraceur;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "convectionDuTraceur");

    }

    /**
     * convectionDuTraceur
     *
     * @return boolean
     */
    private boolean convectionDuTraceur_;
    public boolean convectionDuTraceur() {
        return convectionDuTraceur_;
    }

    /**
     * creeClone
     *
     * @return MetierHydraulique1d
     */
  @Override
    public MetierHydraulique1d creeClone() {
        MetierOptionTraceur p=new MetierOptionTraceur();
        p.initialise(this);
        return p;

    }

    /**
     * diffusionDuTraceur
     *
     * @return boolean
     */
    private boolean diffusionDuTraceur_;
    public boolean diffusionDuTraceur() {
        return diffusionDuTraceur_;
    }

    /**
     * diffusionDuTraceur
     *
     * @param newDiffusionDuTraceur boolean
     */
    public void diffusionDuTraceur(boolean newDiffusionDuTraceur) {
        if (diffusionDuTraceur_==newDiffusionDuTraceur) return;
        diffusionDuTraceur_ = newDiffusionDuTraceur;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "diffusionDuTraceur");

    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
        diffusionDuTraceur_= true;
        convectionDuTraceur_=true;
    }




    /**
     * initialise
     *
     * @param o MetierHydraulique1d
     */
  @Override
    public void initialise(MetierHydraulique1d _o) {
        if (_o instanceof MetierOptionTraceur) {
            MetierOptionTraceur p =(MetierOptionTraceur)_o;
            diffusionDuTraceur(p.diffusionDuTraceur());
            convectionDuTraceur(p.convectionDuTraceur());

        }
    }


}
