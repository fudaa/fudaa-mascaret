package org.fudaa.dodico.hydraulique1d.metier.qualitedeau;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/*
 * @file         MetierParamPhysTracer.java
 * @creation     2006-02-28
 * @modification $Date: 2007-11-20 11:42:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class MetierParamPhysTracer extends MetierHydraulique1d {

		  final static String[] ParamsO2Nom = {
	         "CONST. DE CINET. DE DEGRADATION DE LA CHARGE ORGANIQUE K1 (J-1)",
	         "CONST. DE CINET. DE NITRIFICATION K4 (J-1)",
	         "DEMANDE BENTHIQUE BEN (GO2/M2/J)",
	         "PHOTOSYNTHESE P (MGO2/J/L)",
	         "RESPIRATION VEGETALE R (MGO2/J/L)",
	         "COEFFICIENT DE REAERATION K2 (J-1)",
	         "FORMULE DE CALCUL DE K2",
	         "CONCENTRATION DE SATURATION EN OXYGENE DE L'EAU CS (MG/L)",
	         "FORMULE DE CALCUL DE CS",
	         "TEMPERATURE DE L'EAU T (�C)",
	         "COEFFICIENT DE REAERATION AUX SEUILS R (.)",
	         "FORMULE DE CALCUL DE R",
	         "NOMBRE DE SEUILS N",
	         "COEFFICIENT A DES FORMULES DE CALCUL DE R POUR LE SEUIL N�1",
	         "COEFFICIENT B DES FORMULES DE CALCUL DE R POUR LE SEUIL N�1",
	         "N� DU SEUIL N�1"};
	  final static String[] ParamsBiomassNom = {
	         "VITESSE DE SEDIMENTATION DU PHOSPHORE ORGANIQUE (M/S)",
	         "VITESSE DE SEDIMENTATION DE L'AZOTE ORGANIQUE NON ALGALE (M/S)",
	         "TAUX DE CROISSANCE ALGALE MAXIMUM A 20�C",
	         "PROFONDEUR DE SECCHI (M)",
	         "COEFFICIENT D'EXTINCTION DU RAY SANS PHYTO (M-1)",
	         "COEFFICIENT DE TURBIDITE VEGETALE (M2/MICROGCHLA)",
	         "PARAMETRE DE CALAGE DE LA FORMULE DE SMITH (M-1)",
	         "CONSTANTE DE DEMI-SATURATION EN PHOSPHATE (MG/L)",
	         "CONSTANTE DE DEMI-SATURATION EN AZOTE (MG/L)",
	         "COEFFICIENT 1 DE TOXICITE DE L'EAU POUR LES ALGUES (ALPHA)",
	         "COEFFICIENT 2 DE TOXICITE DE L'EAU POUR LES ALGUES (ALPHA2)",
	         "TAUX DE RESPIRATION DE LA BIOMASSE ALGALE A 20�C (J-1)",
	         "PROP DE PHOSPHORE DANS LES CELLULES DU PHYTO (MGP/MICROGCHLA)",
	         "POURCENT. DE PHOSPHORE DIRECT. ASSIMILABLE DANS LE PHYTO MORT (%)",
	         "TAUX DE TRANSFORMATION DU POR EN PO4 (J-1)",
	         "PROP D'AZOTE DANS LES CELLULES DU PHYTO (MGN/MICROGCHLA)",
	         "POURCENT. D'AZOTE DIRECT. ASSIMILABLE DANS LE PHYTO MORT (%)",
	         "TAUX DE TRANSFORMATION DU NOR EN NO3 (J-1)",
	         "COEFFICIENT 1 DE MORTALITE ALGALE A 20�C (J-1)",
	         "COEFFICIENT 2 DE MORTALITE ALGALE A 20�C (J-1/MG)"};

	  final static String[] ParamsEutroNom = {
	         "VITESSE DE SEDIMENTATION DU PHOSPHORE ORGANIQUE (M/S)",
	         "VITESSE DE SEDIMENTATION DE L'AZOTE ORGANIQUE NON ALGALE (M/S)",
	         "TAUX DE CROISSANCE ALGALE MAXIMUM A 20�C",
	         "PROFONDEUR DE SECCHI (M)",
	         "COEFFICIENT D'EXTINCTION DU RAY SANS PHYTO (M-1)",
	         "COEFFICIENT DE TURBIDITE VEGETALE (M2/MICROGCHLA)",
	         "PARAMETRE DE CALAGE DE LA FORMULE DE SMITH (M-1)",
	         "CONSTANTE DE DEMI-SATURATION EN PHOSPHATE (MG/L)",
	         "CONSTANTE DE DEMI-SATURATION EN AZOTE (MG/L)",
	         "COEFFICIENT 1 DE TOXICITE DE L'EAU POUR LES ALGUES (ALPHA)",
	         "COEFFICIENT 2 DE TOXICITE DE L'EAU POUR LES ALGUES (ALPHA2)",
	         "TAUX DE RESPIRATION DE LA BIOMASSE ALGALE A 20�C (J-1)",
	         "PROP DE PHOSPHORE DANS LES CELLULES DU PHYTO (MGP/MICROGCHLA)",
	         "POURCENT. DE PHOSPHORE DIRECT. ASSIMILABLE DANS LE PHYTO MORT (%)",
	         "TAUX DE TRANSFORMATION DU POR EN PO4 (J-1)",
	         "PROP D'AZOTE DANS LES CELLULES DU PHYTO (MGN/MICROGCHLA)",
	         "POURCENT. D'AZOTE DIRECT. ASSIMILABLE DANS LE PHYTO MORT (%)",
	         "TAUX DE TRANSFORMATION DU NOR EN NO3 (J-1)",
	         "COEFFICIENT 1 DE MORTALITE ALGALE A 20�C (J-1)",
	         "COEFFICIENT 2 DE MORTALITE ALGALE A 20�C (J-1/MG)",
	         "VITESSE DE SEDIMENTATION DE LA CHARGE ORGANIQUE (M/S)",
	         "CONST. DE CINET. DE DEGRADATION DE LA CHARGE ORGANIQUE K120 (J-1)",
	         "CONST. DE CINET. DE NITRIFICATION K520 (J-1)",
	         "QTTE D'OXYGENE PRODUITE PAR PHOTOSYNTHESE F (MGO2/MICROGCHLA)",
	         "QTTE D'OXYGENE CONSOMMEE PAR NITRIFICATION N (MGO2/MGNH4)",
	         "DEMANDE BENTHIQUE BEN (GO2/M2/J)",
	         "COEFFICIENT DE REAERATION K2 (J-1)",
	         "FORMULE DE CALCUL DE K2",
	         "CONCENTRATION DE SATURATION EN OXYGENE DE L'EAU CS (MG/L)",
	         "FORMULE DE CALCUL DE CS",
	         "COEFFICIENT DE REAERATION AUX SEUILS R (.)",
	         "FORMULE DE CALCUL DE R",
	         "NOMBRE DE SEUILS N",
	         "COEFFICIENT A DES FORMULES DE CALCUL DE R POUR LE SEUIL N�1",
	         "COEFFICIENT B DES FORMULES DE CALCUL DE R POUR LE SEUIL N�1",
	         "N� DU SEUIL N�1"};

	  final static String[] ParamsMicropolNom = {
	         "TAUX D'EROSION (KG/M2/S)",
	         "CONTRAINTE CRITIQUE DE SEDIMENTATION (PA)",
	         "CONTRAINTE CRITIQUE DE REMISE EN SUSPENSION (PA)",
	         "VITESSE DE CHUTE DES M.E.S. (M/S)",
	         "CONSTANTE DE DESINTEGRATION EXPONENTIELLE (S-1)",
	         "COEFFICIENT DE DISTRIBUTION (M3/KG)",
	         "CONSTANTE CINETIQUE DE DESORPTION (S-1)"};

	  final static String[] ParamsThermicNom = {
	         "MASSE VOLUMIQUE DE L'EAU RHO (KG/M3)",
	         "CHALEUR SPECIFIQUE DE L'EAU CPE (J/KG�C)",
	         "CHALEUR SPECIFIQUE DE L'AIR A PRESSION CONSTANTE CPA (J/KG�C)",
	         "COEFFICIENT A DE LA FORMULE D'AERATION A+B*U",
	         "COEFFICIENT B DE LA FORMULE D'AERATION A+B*U",
	         "COEFFICIENT REPRESENTATIF DE LA COUVERTURE NUAGEUSE K",
	         "COEFFICIENT DE CALAGE DU RAYONNEMENT ATMOSPHERIQUE EMA",
                 "COEFFICIENT DE CALAGE DU RAYONNEMENT DU PLAN D'EAU EME"};

	  final static double[] ParamsO2Valeur={0.25,0.35,0.1,1,0.06,0.9,0,9,0,7,0,1,0,1.2,0.7,1};
	  final static double[] ParamsBiomassValeur={0,0,2,0,3,0.005,120,0.005,0.03,1,0,0.05,0.0025,0.5,0.03,0.0035,0.5,0.35,0.1,0.003};
	  final static double[] ParamsEutroValeur={0,0,2,0,3,0.005,120,0.005,0.03,1,0,0.05,0.0025,0.5,0.03,0.0035,0.5,0.35,0.1,0.003,0,0.35,0.35,0.15,5.2,0,0.9,0,11,1,0,1,1,1.2,0.7,1};
	  final static double[] ParamsMicropolValeur={0,5,1000,0.000006,0.000000113,1775,0.00000025};
	  final static double[] ParamsThermicValeur={1000,4180,1002,0.002,0.0012,0.2,0.75,0.97};


    public MetierParamPhysTracer() {
         super();
         nomParamPhys_="";
         valeurParamPhys_=0;
         notifieObjetCree();
    }

  @Override
    final public String toString() {
      String s= "Param�tres Physique Tracer";
      return s;
    }

  @Override
    final public MetierHydraulique1d creeClone() {
        MetierParamPhysTracer p =
        new MetierParamPhysTracer();
        p.initialise(this);
        return p;
         }

  @Override
      public void initialise(MetierHydraulique1d _o) {
      if (_o instanceof MetierParamPhysTracer) {
           MetierParamPhysTracer p= (MetierParamPhysTracer)_o;
           nomParamPhys(p.nomParamPhys());
           valeurParamPhys(p.valeurParamPhys());
        }
       }

       /**
      * dispose
      *
      */
  @Override
     public void dispose() {
         nomParamPhys_ = "";
         valeurParamPhys_ = 0;
         super.dispose();

     }


    /**
     * nomParamPhys
     *
     * @param newNomParamPhys String
     */
    private String nomParamPhys_;
    public void nomParamPhys(String newNomParamPhys) {
        if (nomParamPhys_==newNomParamPhys) return;
        nomParamPhys_ = newNomParamPhys;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "nomParamPhys");

    }

    /**
     * nomParamPhys
     *
     * @return String
     */
    public String nomParamPhys() {
        return nomParamPhys_;
    }

    /**
     * valeurParamPhys
     *
     * @return double
     */
    private double valeurParamPhys_;
    public double valeurParamPhys() {
        return valeurParamPhys_;
    }

    /**
     * valeurParamPhys
     *
     * @param newValeurParamPhys double
     */
    public void valeurParamPhys(double newValeurParamPhys) {
        if (valeurParamPhys_==newValeurParamPhys) return;
        valeurParamPhys_ = newValeurParamPhys;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "valeurParamPhys");

    }









}
