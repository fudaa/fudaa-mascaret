package org.fudaa.dodico.hydraulique1d.metier.qualitedeau;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/*
 * @file         MetierParametresQualiteDEau.java
 * @creation     2006-02-28
 * @modification $Date: 2007-11-20 11:42:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class MetierParametresGenerauxQualiteDEau extends MetierHydraulique1d {
    public MetierParametresGenerauxQualiteDEau() {
            super();
            paramsPhysTracer_=new MetierParamPhysTracer[0];
            parametresConvecDiffu_ = new MetierParametresConvecDiffu();
            paramMeteoTracer_=null;

            notifieObjetCree();
        }

    /**
     * creeClone
     *
     * @return MetierHydraulique1d
     */
  @Override
    public MetierHydraulique1d creeClone() {
        MetierParametresGenerauxQualiteDEau p=new MetierParametresGenerauxQualiteDEau();
        p.initialise(this);
        return p;
    }

  @Override
    final public String toString() {
      String s= "Param�tres g�neraux de la Qualit� d'eau";
      return s;
    }

    /**
     * dispose
     */
  @Override
    public void dispose() {
        paramsPhysTracer_ = null;
        parametresConvecDiffu_ = null;
        paramMeteoTracer_ = null;
    }


    /**
     * initialise
     *
     * @param o MetierHydraulique1d
     */
  @Override
    public void initialise(MetierHydraulique1d _o) {
        if (_o instanceof MetierParametresGenerauxQualiteDEau) {
            MetierParametresGenerauxQualiteDEau q =(MetierParametresGenerauxQualiteDEau)_o;
            paramMeteoTracer(q.paramMeteoTracer());
            paramsPhysTracer((MetierParamPhysTracer[])q.paramsPhysTracer().clone());
            if (q.parametresConvecDiffu() != null)
        parametresConvecDiffu((MetierParametresConvecDiffu) q.parametresConvecDiffu().
                         creeClone());
        }
    }


    //Attributs




    /**
     * parametresConvecDiffu
     *
     * @return MetierParametresConvecDiffu
     */
    private MetierParametresConvecDiffu parametresConvecDiffu_;
    public MetierParametresConvecDiffu parametresConvecDiffu() {
        return parametresConvecDiffu_;
    }

    /**
     * parametresConvecDiffu
     *
     * @param newParametresConvecDiffu MetierParametresConvecDiffu
     */
    public void parametresConvecDiffu(MetierParametresConvecDiffu
                                      newParametresConvecDiffu) {
        if (parametresConvecDiffu_==newParametresConvecDiffu) return;
        parametresConvecDiffu_ = newParametresConvecDiffu;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "parametresConvecDiffu");

    }



    /**
     * paramsPhysTracer
     *
     * @return MetierParamPhysTracer[]
     */
    private MetierParamPhysTracer[] paramsPhysTracer_;
    public MetierParamPhysTracer[] paramsPhysTracer() {
        return paramsPhysTracer_;
    }


    /**
     * paramsPhysTracer
     *
     * @param newParamsPhysTracer MetierParamPhysTracer[]

     */
    public void paramsPhysTracer(MetierParamPhysTracer[]
                                        newParamsPhysTracer) {
        if (paramsPhysTracer_==newParamsPhysTracer) return;
        paramsPhysTracer_ = newParamsPhysTracer;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "paramsPhysTrace");

    }

    private byte[] paramMeteoTracer_;
    public byte[] paramMeteoTracer() {
      return paramMeteoTracer_;
    }
    public void paramMeteoTracer(byte[] s) {
      paramMeteoTracer_= s;
      Notifieur.getNotifieur().fireObjetModifie(toString(), this, "paramMeteoTracer");
    }


}
