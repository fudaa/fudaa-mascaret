package org.fudaa.dodico.hydraulique1d.metier.qualitedeau;

import java.util.Arrays;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

        /*
         * @file         MetierParametresModeleQualiteEau.java
         * @creation     2006-02-28
         * @modification $Date: 2007-11-20 11:42:56 $
         * @license      GNU General Public License 2
         * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
         * @mail         devel@fudaa.org
 */
public class MetierParametresModeleQualiteEau extends MetierHydraulique1d {
    public MetierParametresModeleQualiteEau() {
            super();
            presenceTraceurs_=false;	// presence de traceurs
            nbTraceur_=1;
            modeleQualiteEau_ = null;//EnumMetierModeleQualiteDEau.TRANSPORT_PUR;
            vvNomTracer_ = new String[1][2];
            vvNomTracer_[0][0]="TRA1";
            vvNomTracer_[0][1]="Traceur 1";
            frequenceCouplHydroTracer_ = 1;

            notifieObjetCree();
    }


  @Override
    final public String toString() {
      String s= "Param�tres du mod�le de qualit� d'eau";
      return s;
    }
    /**
     * creeClone
     *
     * @return MetierHydraulique1d
     */
  @Override
    public MetierHydraulique1d creeClone() {
       MetierParametresModeleQualiteEau p=
       new MetierParametresModeleQualiteEau();
       p.initialise(this);
        return p;

    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
        presenceTraceurs_=false;
        nbTraceur_=0;
        modeleQualiteEau_ = EnumMetierModeleQualiteDEau.TRANSPORT_PUR;
        vvNomTracer_ = null;
        frequenceCouplHydroTracer_ = 0;
    }


    /**
     * initialise
     *
     * @param o MetierHydraulique1d
     */
  @Override
    public void initialise(MetierHydraulique1d _o) {
      if (_o instanceof MetierParametresModeleQualiteEau) {
        MetierParametresModeleQualiteEau p =(MetierParametresModeleQualiteEau)_o;
        presenceTraceurs(p.presenceTraceurs());
        nbTraceur(p.nbTraceur());
        modeleQualiteEau(p.modeleQualiteEau());
        vvNomTracer((String[][])p.vvNomTracer().clone());
        frequenceCouplHydroTracer(p.frequenceCouplHydroTracer());
      }
    }


    //Attribut



    /**
     * presenceTraceurs
     *
     * @return presenceTraceurs
     */
    private boolean presenceTraceurs_;
    public boolean presenceTraceurs() {
        return presenceTraceurs_;
    }

    /**
     * presenceTraceurs
     *
     * @param presenceTraceurs boolean
     */
    public void presenceTraceurs(boolean
                                 presenceTraceurs) {
        if (presenceTraceurs_ == presenceTraceurs)
            return;
        presenceTraceurs_ = presenceTraceurs;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "presenceTraceurs");

    }


    /**
     * nbTraceur
     *
     * @return nbTraceur
     */
    private int nbTraceur_;
    public int nbTraceur() {
        return nbTraceur_;
    }


    /**
     * nbTraceur
     *
     * @param nbTraceur int
     */
    public void nbTraceur(int
                          nbTraceur) {
        if (nbTraceur_ == nbTraceur)
            return;
        nbTraceur_ = nbTraceur;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "nbTraceur");

    }





    /**
     * frequenceCouplHydroTracer
     *
     * @return int
     */
    private int frequenceCouplHydroTracer_;
    public int frequenceCouplHydroTracer() {
        return frequenceCouplHydroTracer_;
    }

    /**
     * frequenceCouplHydroTracer
     *
     * @param newFrequenceCouplHydroTracer int
     */
    public void frequenceCouplHydroTracer(int newFrequenceCouplHydroTracer) {
        if (frequenceCouplHydroTracer_==newFrequenceCouplHydroTracer) return;
       frequenceCouplHydroTracer_ = newFrequenceCouplHydroTracer;
       Notifieur.getNotifieur().fireObjetModifie(toString(), this, "frequenceCouplHydroTracer");

    }






    /**
     * modeleQualiteEau
     *
     * @return EnumMetierModeleQualiteDEau
     */
    private EnumMetierModeleQualiteDEau modeleQualiteEau_;
    public EnumMetierModeleQualiteDEau modeleQualiteEau() {
        return modeleQualiteEau_;
    }

    /**
     * modeleQualiteEau
     *
     * @param newModeleQualiteEau EnumMetierModeleQualiteDEau
     */
    public void modeleQualiteEau(EnumMetierModeleQualiteDEau newModeleQualiteEau) {
    	if (modeleQualiteEau_!=null && newModeleQualiteEau!=null) {
	        if (modeleQualiteEau_.value() == newModeleQualiteEau.value())  return;
    	}
	        modeleQualiteEau_ = newModeleQualiteEau;
	        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "modeleQualiteEau");
    }

    /**
     *vvNomTracer
     *
     * @return String[][]
     */
    private String[][] vvNomTracer_;
    public String[][] vvNomTracer() {
        return vvNomTracer_;
    }

    /**
     * vvNomTracer
     *
     * @param newVNomTracer String[][]
     */

    public void vvNomTracer(String[][] newVVNomTracer) {
        if (Arrays.equals(newVVNomTracer,vvNomTracer_)) return;
        vvNomTracer_ = newVVNomTracer;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "vvNomTracer");


    }


}
