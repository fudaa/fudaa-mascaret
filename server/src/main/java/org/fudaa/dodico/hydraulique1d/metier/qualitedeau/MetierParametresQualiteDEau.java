package org.fudaa.dodico.hydraulique1d.metier.qualitedeau;

import java.util.Vector;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/*
 * @file         MetierParametresQualiteDEau.java
 * @creation     2006-02-28
 * @modification $Date: 2007-11-20 11:42:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class MetierParametresQualiteDEau extends MetierHydraulique1d {
    public MetierParametresQualiteDEau() {
            super();
            parametresGenerauxQualiteDEau_= new MetierParametresGenerauxQualiteDEau();
            parametresModeleQualiteEau_ = new MetierParametresModeleQualiteEau();
            concentrationsInitiales_ = new MetierConcentrationInitiale[0];

            notifieObjetCree();
        }

    /**
     * creeClone
     *
     * @return MetierHydraulique1d
     */
  @Override
    public MetierHydraulique1d creeClone() {
        MetierParametresQualiteDEau p=new MetierParametresQualiteDEau();
        p.initialise(this);
        return p;
    }

  @Override
    final public String toString() {
      String s= "Param�tres Qualit� d'Eau";
      return s;
    }

    /**
     * dispose
     */
  @Override
    public void dispose() {
        parametresGenerauxQualiteDEau_ = null;
        parametresModeleQualiteEau_ = null;
        concentrationsInitiales_=null;

    }


    /**
     * initialise
     *
     * @param o MetierHydraulique1d
     */
  @Override
    public void initialise(MetierHydraulique1d _o) {
        if (_o instanceof MetierParametresQualiteDEau) {
            MetierParametresQualiteDEau q =(MetierParametresQualiteDEau)_o;
            if (q.parametresGenerauxQualiteDEau() != null)
                parametresGenerauxQualiteDEau((MetierParametresGenerauxQualiteDEau) q.
                                        parametresGenerauxQualiteDEau().
                                        creeClone());
            if (q.parametresModeleQualiteEau() != null)
        parametresModeleQualiteEau((MetierParametresModeleQualiteEau) q.parametresModeleQualiteEau().
                         creeClone());
             if (q.concentrationsInitiales() != null) {
                 MetierConcentrationInitiale[] ci = new MetierConcentrationInitiale[q.
                                               concentrationsInitiales().length];
                 for (int i = 0; i < ci.length; i++)
                     ci[i] = (MetierConcentrationInitiale) q.concentrationsInitiales()[
                             i].creeClone();
                 concentrationsInitiales(ci);
             }

        }
    }


    //Attributs

    /**
     * parametresGenerauxQualiteDEau
     *
     * @return MetierParametresGenerauxQualiteDEau
     */
    private MetierParametresGenerauxQualiteDEau parametresGenerauxQualiteDEau_;
    public MetierParametresGenerauxQualiteDEau parametresGenerauxQualiteDEau() {
        return parametresGenerauxQualiteDEau_;
    }

    /**
     * MetierParametresGenerauxQualiteDEau
     *
     * @param parametresGenerauxQualiteDEau MetierParametresGenerauxQualiteDEau
     */
    public void parametresGenerauxQualiteDEau(MetierParametresGenerauxQualiteDEau
                                      parametresGenerauxQualiteDEau) {
        if (parametresGenerauxQualiteDEau_==parametresGenerauxQualiteDEau) return;
        parametresGenerauxQualiteDEau_ = parametresGenerauxQualiteDEau;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "parametresGenerauxQualiteDEau");

    }



    /**
     * parametresModeleQualiteEau
     *
     * @return MetierParametresModeleQualiteEau
     */
    private MetierParametresModeleQualiteEau parametresModeleQualiteEau_;
    public MetierParametresModeleQualiteEau parametresModeleQualiteEau() {
        return parametresModeleQualiteEau_;
    }


    /**
     * parametresModeleQualiteEau
     *
     * @param newParametresModeleQualiteEau IPparametresModeleQualiteEau

     */
    public void parametresModeleQualiteEau(MetierParametresModeleQualiteEau
                                        newParametresModeleQualiteEau) {
        if (parametresModeleQualiteEau_==newParametresModeleQualiteEau) return;
        parametresModeleQualiteEau_ = newParametresModeleQualiteEau;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this,
                                              "parametresModeleQualiteEau");

    }

    private MetierConcentrationInitiale[] concentrationsInitiales_;
     public MetierConcentrationInitiale[] concentrationsInitiales() {
       return concentrationsInitiales_;
     }
     public void concentrationsInitiales(MetierConcentrationInitiale[] concentrationsInitiales) {
       if (egale(concentrationsInitiales_, concentrationsInitiales)) return;
       concentrationsInitiales_= concentrationsInitiales;
       Notifieur.getNotifieur().fireObjetModifie(toString(), this, "concentrationsInitiales");
     }
     private final static boolean egale(MetierConcentrationInitiale[] p, MetierConcentrationInitiale[] p2) {
       if (p == p2)
         return true;
       if (p == null || p2 == null)
         return false;

       int length = p.length;
       if (p2.length != length)
         return false;

       for (int i = 0; i < length; i++)
         if (p[i] != p2[i])
           return false;

       return true;
     }

     public MetierConcentrationInitiale creeConcentrationsInitiales() {
        MetierConcentrationInitiale conc= new MetierConcentrationInitiale();
        MetierConcentrationInitiale concs[]= new MetierConcentrationInitiale[concentrationsInitiales_.length + 1];
        for (int i= 0; i < concentrationsInitiales_.length; i++)
          concs[i]= concentrationsInitiales_[i];
        concs[concs.length - 1]= conc;
        concentrationsInitiales(concs);
        return conc;
      }
      private void supprimeConcentrationInitiale(MetierConcentrationInitiale conc) {
        Vector newConcs= new Vector(concentrationsInitiales_.length - 1);
        for (int i= 0; i < concentrationsInitiales_.length; i++) {
          if (concentrationsInitiales_[i] == conc) {
        	  concentrationsInitiales_[i].supprime();
          } else
            newConcs.add(concentrationsInitiales_[i]);
        }
        MetierConcentrationInitiale[] concs= new MetierConcentrationInitiale[newConcs.size()];
        for (int i= 0; i < concs.length; i++) {
          concs[i]= (MetierConcentrationInitiale)newConcs.get(i);
        }
        concentrationsInitiales(concs);
      }
      public void supprimeConcentrationsInitiales(MetierConcentrationInitiale[] concs) {
        //System.out.println("supprimeConcentrationsInitiales(MetierConcentrationInitiale[] concs) concs.length="+concs.length);
        for (int i= 0; i < concs.length; i++) {
          supprimeConcentrationInitiale(concs[i]);
        }
      }

      private MetierParamPhysTracer[] construireParamsPhysParDefaut(String[] noms,double[] valeur){
          MetierParamPhysTracer[] params = new MetierParamPhysTracer[noms.length];
          MetierParamPhysTracer param;
          for (int i = 0; i < noms.length; i++) {
              param = new MetierParamPhysTracer();
              param.nomParamPhys(noms[i]);
              param.valeurParamPhys(valeur[i]);
              params[i]=param;
          }
          return params;
      }

      private void  construireParamPhys(EnumMetierModeleQualiteDEau modele) {
          //Remplissage du tableau selon le mod�le de qualit� d'eau
          switch (parametresModeleQualiteEau_.modeleQualiteEau().value()) {
          case EnumMetierModeleQualiteDEau._O2 : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(MetierParamPhysTracer.ParamsO2Nom,MetierParamPhysTracer.ParamsO2Valeur));break;
          case EnumMetierModeleQualiteDEau._BIOMASS : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(MetierParamPhysTracer.ParamsBiomassNom,MetierParamPhysTracer.ParamsBiomassValeur));break;
          case EnumMetierModeleQualiteDEau._EUTRO : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(MetierParamPhysTracer.ParamsEutroNom,MetierParamPhysTracer.ParamsEutroValeur));break;
          case EnumMetierModeleQualiteDEau._MICROPOL : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(MetierParamPhysTracer.ParamsMicropolNom,MetierParamPhysTracer.ParamsMicropolValeur));break;
          case EnumMetierModeleQualiteDEau._THERMIC : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(MetierParamPhysTracer.ParamsThermicNom,MetierParamPhysTracer.ParamsThermicValeur));break;
          default  : parametresGenerauxQualiteDEau_.paramsPhysTracer(null);
          }
      };

      public void initialisationSelonModeleQE(){
    	    int nbTraceurs = parametresModeleQualiteEau_.nbTraceur();

    	    //Mise � jour des param�tres g�n�raux
    	    if (parametresGenerauxQualiteDEau_.parametresConvecDiffu().optionDesTracers().length!=nbTraceurs){
    	        MetierOptionTraceur[] newTabOptions = new MetierOptionTraceur[nbTraceurs];
    	        for (int i = 0; i < newTabOptions.length; i++) {
    	            newTabOptions[i]= new MetierOptionTraceur();
    	        }
    	        if (parametresModeleQualiteEau_.modeleQualiteEau().value()==EnumMetierModeleQualiteDEau._MICROPOL){
    	            newTabOptions[1].convectionDuTraceur(false);
    	            newTabOptions[1].diffusionDuTraceur(false);
    	            newTabOptions[4].convectionDuTraceur(false);
    	            newTabOptions[4].diffusionDuTraceur(false);
    	        }
    	        parametresGenerauxQualiteDEau_.parametresConvecDiffu().optionDesTracers(newTabOptions);
    	    }

    	    //Mise � jour des param�tres physique
    	    construireParamPhys(parametresModeleQualiteEau_.modeleQualiteEau());

    	    //Mise � jour des param�tres m�t�o
    	    parametresGenerauxQualiteDEau_.paramMeteoTracer(null);

    	    //Mise � jour des concentrations initiales
    	    this.concentrationsInitiales_ = new MetierConcentrationInitiale[0];
     	    //Idealement il faudrait �galement supprimer toutes les lois Tracer
      }



}
