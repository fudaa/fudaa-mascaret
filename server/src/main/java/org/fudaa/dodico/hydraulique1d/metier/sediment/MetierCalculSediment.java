package org.fudaa.dodico.hydraulique1d.metier.sediment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.util.CourbeLissable;
import org.fudaa.dodico.hydraulique1d.util.Hydraulique1DUtils;

/**
 * Calcul des resultats sp�cifiques s�dimentaires. Les r�sultats sont ajout�s
 * aux r�sultats g�n�raux.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class MetierCalculSediment {
  
  public class CourbeDebitSolide implements CourbeLissable {
    double[] results_;
    double[] abscisses_;
    
    public CourbeDebitSolide(double[] _res, double[] _abscisses) {
      results_=_res;
      abscisses_=_abscisses;
    }
    
    @Override
    public int getNbPoint() {
      return results_.length;
    }

    @Override
    public double getX(int _isect) {
      return abscisses_[_isect];
    }

    @Override
    public double getY(int _isect) {
      return results_[_isect];
    }

    @Override
    public boolean canModifyY(int _ipt) {
      return true;
    }

    @Override
    public void setY(int _isect, double _y) {
      results_[_isect]=_y;
    }
  }
  
  private MetierResultatsTemporelSpatial res_;
  private MetierParametresSediment params_;

  public MetierCalculSediment(MetierResultatsTemporelSpatial _res, MetierParametresSediment _params) {
    res_ = _res;
    params_=_params;
  }

  /**
   * Supprime tous les r�sultats s�dimentaires
   */
  public void clearResultatsSediment() {
    for (MetierDescriptionVariable var : MetierSediment.VARIABLES) {
      res_.removeResultsVariable(var);
      res_.removeResultsVariable(MetierSediment.getResultatMoyenneAssocie(var));
    }
  }

  /**
   * Calcule et ajoute les r�sultats pour la variable s�dimentaire des param�tres.
   */
  public MetierDescriptionVariable[] computeResultsForVariables() {
    Set<MetierDescriptionVariable> neededVars=new HashSet<MetierDescriptionVariable>();
    
    for (MetierFormuleSediment form : params_.getFormules()) {
      MetierDescriptionVariable[] vars = computeResultsForVariable(form.getVariable());
      neededVars.addAll(Arrays.asList(vars));
    }
    
    // Les resultats moyennes
    for (MetierFormuleSediment form : params_.getFormules()) {
      addResultatMoyennes(form.getVariable());
    }
    
    return neededVars.toArray(new MetierDescriptionVariable[0]);
  }

  /**
   * Ajoute les resultats moyennes
   */
  public void addResultatMoyennes(MetierDescriptionVariable _var) {
    
    // Suppression eventuelle de l'ancien r�sultat moyenn�
    MetierDescriptionVariable moyVar=MetierSediment.getResultatMoyenneAssocie(_var);
    res_.removeResultsVariable(moyVar);
    
    // Ajout eventuel des r�sultats moyenn�s
    if (params_.isResMoyCalcules()) {
      int nbBief=res_.resultatsBiefs().length;
      int nbTps=res_.valPas().length;
      
      double[][][] vals=res_.getResultsVariable(_var);
      if (vals==null) return;

      double[][][] moyVals = new double[nbBief][nbTps][];
      for (int ibief = 0; ibief < nbBief; ibief++) {
        for (int itps = 0; itps < nbTps; itps++) {
          moyVals[ibief][itps] = Arrays.copyOf(vals[ibief][itps], vals[ibief][itps].length);
          CourbeDebitSolide crb = new CourbeDebitSolide(moyVals[ibief][itps], res_.resultatsBiefs()[ibief].abscissesSections());
          Hydraulique1DUtils.lisser(crb, params_.getLargeurFenetre(), Hydraulique1DUtils.LISSAGE_MOYENNE, false);
        }
      }
      res_.addResultsVariable(moyVar, moyVals);
    }
  }
  
  /**
   * Calcule et ajoute les r�sultats pour une variable s�dimentaire.
   * @param _var La variable s�dimentaire.
   * @return Les variables manquantes pour le calcul, de longueur=0 si le calcul a pu �tre effectu�
   */
  public MetierDescriptionVariable[] computeResultsForVariable(MetierDescriptionVariable _var) {
    MetierFormuleSediment formule=MetierFormuleSediment.getFormule(_var);
    List<MetierDescriptionVariable> ls=new ArrayList<MetierDescriptionVariable>();
    
    for (MetierDescriptionVariable var : formule.getRequiredVariable()) {
      if (res_.getIndiceVariable(var) == -1) {
        ls.add(var);
//        sb.append("\n- ").append(var.description());
      }
    }
    if (ls.size() != 0) {
      return ls.toArray(new MetierDescriptionVariable[0]);
    }

    boolean found=false;
    for (MetierDescriptionVariable var : MetierSediment.VARIABLES) {
      if (var.equals(_var)) {
        found=true;
        break;
      }
    }
    if (!found)
      throw new IllegalArgumentException("Variable non s�dimentaire");
    
    MetierResultatTemporelSpatialAdapter resAdapter=new MetierResultatTemporelSpatialAdapter(res_);
    
    // Suppression eventuelle de l'ancienne variable de m�me nom
    res_.removeResultsVariable(_var);
    
    int nbBief=res_.resultatsBiefs().length;
    int nbTps=res_.valPas().length;
    double[][][] vals=new double[nbBief][nbTps][];
    for (int ibief=0; ibief<nbBief; ibief++) {
      int nbSect=res_.resultatsBiefs()[ibief].abscissesSections().length;
      for (int itps=0; itps<nbTps; itps++) {
        vals[ibief][itps]=new double[nbSect];
        for (int isect=0; isect<nbSect; isect++) {
          vals[ibief][itps][isect]=formule.calculer(params_, resAdapter, ibief, isect, itps);
        }
      }
    }
    res_.addResultsVariable(_var, vals);

    // A activer si on veut stocker la pente d'energie
    boolean addPente=false;
    if (addPente) {
      res_.removeResultsVariable(MetierDescriptionVariable.PENE);
      vals = new double[nbBief][nbTps][];
      for (int ibief = 0; ibief < nbBief; ibief++) {
        int nbSect = res_.resultatsBiefs()[ibief].abscissesSections().length;
        for (int itps = 0; itps < nbTps; itps++) {
          vals[ibief][itps] = new double[nbSect];
          for (int isect = 0; isect < nbSect; isect++) {
            vals[ibief][itps][isect] = resAdapter.getValue(MetierDescriptionVariable.PENE, ibief, itps, isect);
          }
        }
      }
      res_.addResultsVariable(MetierDescriptionVariable.PENE, vals);
    }

    return ls.toArray(new MetierDescriptionVariable[0]);
  }
}
