package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * Calcul sÚdimentaire avec la formule de Hangelung & Hansen.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: MetierFormuleLefort1991.java 8524 2013-10-18 08:01:47Z
 *          bmarchan$
 */
public class MetierFormuleEngelundHansen extends MetierFormuleSediment {

  @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {

    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double larg = _adapter.getValue(MetierDescriptionVariable.B1, _ibief, _itps, _isect);
    double rh1 = _adapter.getValue(MetierDescriptionVariable.RH1, _ibief, _itps, _isect);
    double vmin = _adapter.getValue(MetierDescriptionVariable.VMIN, _ibief, _itps, _isect);
    
    double dens = _params.getDensiteMateriau();
    double d50 = _params.getD50();
    RetTauMoy taumoy = calculerTaumoy(_params, _adapter, _ibief, _isect, _itps);
    double teta = taumoy.tetaD50;

    double f = 2 * 9.81 * rh1 * pente / Math.pow(vmin, 2);
    double qs = larg * 0.1 / f * Math.pow((9.81 * (dens - 1) * Math.pow(d50, 3)), 0.5) * Math.pow(teta, (5. / 2));

    return qs;
  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] { 
        MetierDescriptionVariable.B1,
        MetierDescriptionVariable.CHAR,
        MetierDescriptionVariable.KMIN,
        MetierDescriptionVariable.RH1,
        MetierDescriptionVariable.VMIN
    };
  }

  @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_ENGELUND;
  }

  @Override
  public String getName() {
    return "Engelund & Hansen";
  }
}
