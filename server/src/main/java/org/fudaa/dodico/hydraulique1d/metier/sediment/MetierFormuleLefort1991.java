package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * Calcul s�dimentaire avec la formule de Lefort 1991.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class MetierFormuleLefort1991 extends MetierFormuleSediment {
  
  /**
   * Calcul de la capacit� de transport dans chaque section selon la formule de
   * Lefort. La formule calcule la capacit� de transport apparante avec une
   * densit� apparente de 2. Un coefficient de 0.755 est ajout� pour revenir au
   * volume r�el et non au calcul du volume en place (voir �tablissement formule
   * de lefort).
   */
   @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {

    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double qmin = _adapter.getValue(MetierDescriptionVariable.QMIN, _ibief, _itps, _isect);
    
    double dens = _params.getDensiteMateriau();
    double d90 = _params.getD90();
    double d30 = _params.getD30();
    double diam = _params.getDmoyen();

    // Calcul du d�bit liquide de d�but d'entrainement
    double qc;
    if (pente <= 0) {
      qc = 999999;
    }
    else {
      qc = 0.0776 * Math.pow((9.81 * Math.pow(diam, 5)), 0.5) * Math.pow((dens - 1), (8. / 3)) * Math.pow((1 - 1.2 * pente), (8. / 3)) / Math.pow(pente, (13. / 6));
    }

    // Calcul du d�bit solide
    double qs;
    if (qc > qmin) {
      qs = 0;
    }
    else {
      qs = (qmin * 4.45 * 0.755) / (dens - 1) * Math.pow((d90 / d30), 0.2) * Math.pow(pente, 1.5) * (1 - Math.pow((qc / qmin), 0.375));
    }
    
    return qs;
  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] {
        MetierDescriptionVariable.CHAR,
        MetierDescriptionVariable.QMIN
    };
  }

  @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_LEFORT91;
  }

  @Override
  public String getName() {
    return "Lefort 1991";
  }
}
