package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * Calcul s�dimentaire avec la formule de Lefort 1991.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: MetierFormuleLefort1991.java 8523 2013-10-17 12:33:25Z bmarchan$
 */
public class MetierFormuleLefort2007 extends MetierFormuleSediment {

  /**
   * Calcul de la capacit� de transport dans chaque section selon la formule de
   * Lefort. La formule calcule la capacit� de transport apparante avec une
   * densit� apparente de 2. Un coefficient de 0.755 est ajout� pour revenir au
   * volume r�el et non au calcul du volume en place (voir �tablissement formule
   * de lefort).
   */
  @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {
    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double larg = _adapter.getValue(MetierDescriptionVariable.B1, _ibief, _itps, _isect);
    double ks = _adapter.getValue(MetierDescriptionVariable.KMIN, _ibief, _itps, _isect);
    double qmin = _adapter.getValue(MetierDescriptionVariable.QMIN, _ibief, _itps, _isect);
    double h = _adapter.getValue(MetierDescriptionVariable.Y, _ibief, _itps, _isect);

    double nu = _params.getViscosite();
    double diam = _params.getDmoyen();
    double dens = _params.getDensiteMateriau();
    double d90 = _params.getD90();
    double d30 = _params.getD30();

    // ' initialisation des colonnes et des lignes
    // Col_Lefort07 = Range("Lefort07").Column
    // Col_deb_cible = Range("Qcible").Column
    // Col_H = Range("Hauteur").Column
    //
    // Lgn_Qc = Range("Ligne_Qc").Row
    // lgn_calcul = Range("ligne_debut").Row
    // ligne = lgn_calcul

    // calcul des coef
    double rap = Math.pow((d90 / d30), 0.21);
    double detoile = diam * Math.pow((9.81 * (dens - 1) / Math.pow(nu, 2)), (1. / 3));
    double kr = 21.1 / Math.pow(diam, (1. / 6));
    double n = 1.725 + 0.09 * Math.log10(pente);

    double cdm;
    if (diam < 0.008) {
      cdm = 0.0269;
    }
    else {
      cdm = 0.0269 + 0.532 / (1.1 + detoile) - 0.0589 * Math.exp(-detoile / 60);
    }
    double m = 1.887 + 0.09 * Math.log10(pente);

    // calcul du debit de debut d'entrainement
    double qc = Math.pow((9.81 * Math.pow(diam, 5)), 0.5) * cdm * Math.pow((dens - 1), (5. / 3)) * Math.pow((larg / diam), (2. / 3)) * Math.pow((ks / kr), -0.42) * Math.pow(pente, (-n));

    // Cells(Lgn_Qc, Col_Lefort07) = Qc

    // calcul du d�bit solide
    // Do While Cells(ligne, Col_deb_cible) <> ""
    // Qmin = Cells(ligne, Col_deb_cible)
    double cp;
    if (qmin > qc) {

      double qetoile = qmin / qc;
      // calcul de getoile
      double gQetoile;
      if (qetoile > 2.5) {
        gQetoile = 3.88 * Math.pow((1 - Math.pow((0.75 / qetoile), 0.25)), (5. / 3));
      }
      else {
        gQetoile = 0.4 * Math.pow((qetoile / 2.5), (6.25 * (1 - 0.37 * qetoile)));
      }

      // Coef de correction de dune
      double cor;
      if (ks / kr < 0.6) {
        cor = 1 - 0.9 * Math.exp(-0.08 * Math.pow((ks / kr), 2.4) * qetoile);
      }
      else {
        cor = 1;
      }

      // Calcul du reynolds
      double rh = (h * larg) / (larg + 2 * h);
      double re = qmin * rh / ((larg * h) * nu);

      double z = 0.78 + 1.53 * Math.pow(re, 0.14) / Math.pow(detoile, 0.78);

      // concentration en !!!! T/m3
      cp = 3.176 * cor * rap * dens / Math.pow((dens - 1), (1.38)) * Math.pow(pente, m) * Math.pow(gQetoile, z);
    }
    else {
      cp = 0;
    }

    double qs = qmin * cp / dens;
    return qs;
  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] { 
        MetierDescriptionVariable.CHAR, 
        MetierDescriptionVariable.QMIN,
        MetierDescriptionVariable.B1, 
        MetierDescriptionVariable.KMIN,
        MetierDescriptionVariable.Y 
    };
  }

  @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_LEFORT07;
  }

  @Override
  public String getName() {
    return "Lefort 2007";
  }
}
