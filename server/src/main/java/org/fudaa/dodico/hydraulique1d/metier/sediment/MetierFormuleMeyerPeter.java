package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * Calcul sédimentaire avec la formule de Meyer-Peter.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: MetierFormuleLefort1991.java 8524 2013-10-18 08:01:47Z bmarchan$
 */
public class MetierFormuleMeyerPeter extends MetierFormuleSediment {

  @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {
    double kr = _params.getRugosite();
    double ks = _adapter.getValue(MetierDescriptionVariable.KMIN, _ibief, _itps, _isect);
    double larg = _adapter.getValue(MetierDescriptionVariable.B1, _ibief, _itps, _isect);
    double dens = _params.getDensiteMateriau();

    double diam = _params.getDmoyen();

    // Contrainte critique adimentionnelle de début d'entrainement
    double taustarc = 0.047;
    double kpeau;
    if (kr > ks) {
      kpeau = kr;
    }
    else {
      kpeau = ks;
    }

    RetTauMoy taumoy = calculerTaumoy(_params, _adapter, _ibief, _isect, _itps);

    // Call Debit_crit(Taustarc / (ks / Kpeau) ^ 1.5, Col_Meyer)

    double teta = taumoy.teta;
    double taustarEff = Math.pow((ks / kpeau), 1.5) * teta;
    double qsmp;
    if (taustarEff - taustarc < 0) {
      qsmp = 0;
    }
    else {
      qsmp = (larg * Math.pow((9.81 * (dens - 1) * Math.pow(diam, 3)), 0.5)) * 8 * Math.pow((taustarEff - taustarc), 1.5);
    }
    return qsmp;
  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] {
        MetierDescriptionVariable.B1,
        MetierDescriptionVariable.CHAR,
        MetierDescriptionVariable.KMIN,
        MetierDescriptionVariable.RH1
    };
  }

  @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_MEYERPETER;
  }

  @Override
  public String getName() {
    return "Meyer-Peter";
  }
}
