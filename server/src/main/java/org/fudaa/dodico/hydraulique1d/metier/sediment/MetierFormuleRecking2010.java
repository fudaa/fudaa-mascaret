package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * Calcul s�dimentaire avec la formule de Recking 2010.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: MetierFormuleLefort1991.java 8524 2013-10-18 08:01:47Z
 *          bmarchan$
 */
public class MetierFormuleRecking2010 extends MetierFormuleSediment {

  @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {

    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double larg = _adapter.getValue(MetierDescriptionVariable.B1, _ibief, _itps, _isect);
    double qmin = _adapter.getValue(MetierDescriptionVariable.QMIN, _ibief, _itps, _isect);

    double dens = _params.getDensiteMateriau();
    double d84 = _params.getD84();
    double d50 = _params.getD50();

    // Diametre cible
    double dcible;
    if (d50 > 0.002) {
      dcible = 3.5 * d84;
    }
    else {
      dcible = d50;
    }

    // Calcul de RH
//    double rh = calculerRH(h, d84, qmin, pente, larg);
    // Calcul de RH cible
    double rhcible = calculerRHCible(qmin, larg, pente, dcible);

    double tetac84;
    if (d50 < 0.002) {
      tetac84 = 0.045;
    }
    else {
      tetac84 = (1.32 * pente + 0.037) * Math.pow(d84 / d50, -0.93);
    }

    // 'valeur de tetac prise pour la validation des valeurs labo
    // 'tetac84 = 0.15 * pente ^ 0.275

    double l = 12.53 * Math.pow((d84 / d50), (4.445 * Math.pow(pente, 0.5))) * Math.pow(tetac84, 1.605);

    // calcul du d�bit solide
    double teta84 = rhcible * pente / (d84 * (dens - 1));
    double cpl;
    if (teta84 < l) {
      cpl = 0.0005 * (dens * 1000) * Math.pow((9.81 * (dens - 1) * Math.pow(d84, 3)), 0.5) * Math.pow((d84 / d50), (-18 * Math.pow(pente, 0.5))) * Math.pow((teta84 / tetac84), 6.5);
    }
    else {
      cpl = 14 * (dens * 1000) * Math.pow((9.81 * (dens - 1) * Math.pow(d84, 3)), 0.5) * Math.pow(teta84, 2.45);
    }
    double qs = cpl / (dens * 1000) * larg;

    return qs;
  }

  protected double calculerRH(double _h, double _d84, double _qmin, double _pente, double _larg) {
    double rh;
    if (_h / _d84 < 5) {
      rh = Math.pow((_qmin / _larg * Math.pow(_d84, 0.52)) / (3.2 * Math.pow((9.81 * _pente), 0.5)), 0.5);
    }
    else {
      rh = Math.pow(((_qmin / _larg * Math.pow(_d84, 0.27)) / (4.7 * Math.pow((9.81 * _pente), 0.5))), 0.57);
    }
    return rh;
  }

  /**
   * Calcul du RH cible par dicothomie.
   * 
   * @param _qmin Le d�bit en lit mineur
   * @param _larg La largeur du profil
   * @param _pente La pente
   * @param _dcible Le diametre de grain cible
   * @param _rescible Le r�sultat cible � trouver
   * @return La RH correspondant au r�sultat cible demand�.
   */
  private double calculerRHCible(double _qmin, double _larg, double _pente, double _dcible) {
    double rhSup = 1.e20;
    double rhInf = 1.e-20;
    double rhCible = 0;
    double rescible = 0;
    double val = rescible + 1;

    // It�rations par dicothomie jusqu'� obtention du r�sultat.
    while (true) {
      rhCible = (rhSup + rhInf) / 2;
      val = _qmin * (_larg - 2 * rhCible) / (rhCible * _larg * _larg * Math.pow((9.81 * rhCible * (_pente)), 0.5)) - 6.25 - 5.75 * Math.log10(rhCible / (_dcible));

      if (val - rescible > 0) {
        rhInf = rhCible;
      }
      else if (val < rescible - 1.e-8) {
        rhSup = rhCible;
      }
      // Sortie si convergence
      else {
        break;
      }
    }
//    FuLog.debug("rhCible=" + rhCible);
    return rhCible;
  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] { 
        MetierDescriptionVariable.B1, 
        MetierDescriptionVariable.CHAR, 
        MetierDescriptionVariable.QMIN 
    };
  }

  @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_RECKING10;
  }

  @Override
  public String getName() {
    return "Recking 2010";
  }
}
