package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * Calcul sÚdimentaire avec la formule de Recking 2011.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: MetierFormuleLefort1991.java 8524 2013-10-18 08:01:47Z
 *          bmarchan$
 */
public class MetierFormuleRecking2011 extends MetierFormuleSediment {

  @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {

    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double larg = _adapter.getValue(MetierDescriptionVariable.B1, _ibief, _itps, _isect);
    double qmin = _adapter.getValue(MetierDescriptionVariable.QMIN, _ibief, _itps, _isect);

    double dens = _params.getDensiteMateriau();
    double d50 = _params.getD50();
    double d84 = _params.getD84();

    double tetaMEt;
    
    // calcul des coef
    if (d50 < 0.002) {
      tetaMEt = 0.045;
    }
    else {
      tetaMEt = (5 * pente + 0.06) * Math.pow((d84 / d50), (4.4 * Math.pow(pente, 0.5) - 1.5));
      // teta_m_et = (5 * pente + 0.06) * (D84 / D50) ^ (-1.5)
    }

    double test = qmin / larg / Math.pow((9.81 * pente * Math.pow(d84, 3)), 0.5);

    double alfa;
    double beta;
    double ceta;
    double delta;
    if (test > 100) {
      alfa = 3.2;
      beta = 0.3;
      ceta = -0.61;
      delta = -0.09;
    }
    else {
      alfa = 1.6;
      beta = 0.23;
      ceta = -0.46;
      delta = -0.32;
    }

    double tetaEt = pente / ((dens - 1) * d84 * (2 / larg + alfa * Math.pow((9.81 * pente), beta) * Math.pow((qmin / larg), ceta) * Math.pow(d84, delta)));

    double phi = 14 * Math.pow(tetaEt, 2.5) / (1 + Math.pow((tetaMEt / tetaEt), 4));
//    double qs_unit = dens * 1000 * Phi * Math.pow((9.81 * (dens - 1) * Math.pow(D84, 3)), 0.5);
    double qs = larg * phi * Math.pow((9.81 * (dens - 1) * Math.pow(d84, 3)), 0.5);

    return qs;
  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] { 
        MetierDescriptionVariable.QMIN, 
        MetierDescriptionVariable.B1, 
        MetierDescriptionVariable.CHAR
    };
  }

 @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_RECKING11;
  }

  @Override
  public String getName() {
    return "Recking 2011";
  }
}
