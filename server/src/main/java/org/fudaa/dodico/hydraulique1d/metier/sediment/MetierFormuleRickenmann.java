package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * Calcul s�dimentaire avec la formule de Rickenmann.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: MetierFormuleLefort1991.java 8524 2013-10-18 08:01:47Z
 *          bmarchan$
 */
public class MetierFormuleRickenmann extends MetierFormuleSediment {

  @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {

    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double larg = _adapter.getValue(MetierDescriptionVariable.B1, _ibief, _itps, _isect);
    double qmin = _adapter.getValue(MetierDescriptionVariable.QMIN, _ibief, _itps, _isect);

    double dens = _params.getDensiteMateriau();
    double d90 = _params.getD90();
    double d30 = _params.getD30();
    double d50 = _params.getD50();

    double rap = Math.pow((d90 / d30), 0.2);

    // Calcul du d�bit de d�but d'entrainement
    double qc = 0.065 * Math.pow((dens - 1), (5. / 3)) * Math.pow(9.81, 0.5) * Math.pow(d50, 1.5) * Math.pow(pente, (-1.12));

    double qs;
    if (qc > qmin / larg) {
      qs = 0;
    }
    else {
      if (pente < 0.03) {
        qs = larg * 1.5 * (qmin / larg - qc) * Math.pow(pente, 1.5);
      }
      else {
        qs = larg * 12.6 / Math.pow((dens - 1), 1.6) * rap * (qmin / larg - qc) * Math.pow(pente, 2);
      }
    }

    return qs;

  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] { 
        MetierDescriptionVariable.B1, 
        MetierDescriptionVariable.CHAR, 
        MetierDescriptionVariable.QMIN
    };
  }

  @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_RICKENMANN;
  }

  @Override
  public String getName() {
    return "Rickenmann";
  }
}
