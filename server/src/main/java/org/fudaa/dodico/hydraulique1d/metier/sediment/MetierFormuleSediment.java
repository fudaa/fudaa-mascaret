package org.fudaa.dodico.hydraulique1d.metier.sediment;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * La classe abstraire pour toutes les formules de s�diment
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * 
 */
public abstract class MetierFormuleSediment {
  
  /**
   * Valeurs de retour de la fonction de calcul de tau moyen
   * 
   * @author Bertrand Marchand (marchand@deltacad.fr)
   */
  public class RetTauMoy {
    public double tau;
    public double taueff;
    public double teta;
    public double tetaD50;
  }

  /**
   * Contient les instances de chaque formule.
   */
  private static List<MetierFormuleSediment> formules_ = new ArrayList<MetierFormuleSediment>();
  static {
    formules_.add(new MetierFormuleEngelundHansen());
    formules_.add(new MetierFormuleLefort1991());
    formules_.add(new MetierFormuleLefort2007());
    formules_.add(new MetierFormuleMeyerPeter());
    formules_.add(new MetierFormuleRecking2010());
    formules_.add(new MetierFormuleRecking2011());
    formules_.add(new MetierFormuleRickenmann());
    formules_.add(new MetierFormuleSmartJaggi());
    formules_.add(new MetierFormuleVanRijn());
  }

  public MetierFormuleSediment() {
    super();
  }

  public abstract double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps);

  /**
   * @return La variable associ�e et calcul�e par la formule.
   */
  public abstract MetierDescriptionVariable getVariable();

  /**
   * @return Le nom de la formule (pour affichage)
   */
  public abstract String getName();

  /**
   * @return Les variables necessaires au calcul par la formule. Sans ces
   *         variables, le calcul n'est pas possible.
   */
  public abstract MetierDescriptionVariable[] getRequiredVariable();

  /**
   * Necessaire pour comparer correctement 2 enums.
   */
  @Override
  public boolean equals(Object _obj) {
    if (_obj == null || !(_obj instanceof MetierFormuleSediment))
      return false;
    return getVariable().equals(((MetierFormuleSediment) _obj).getVariable());
  }

  @Override
  public String toString() {
    return getName();
  }

  /**
   * @return Toutes les formules possibles.
   */
  public static MetierFormuleSediment[] values() {
    return formules_.toArray(new MetierFormuleSediment[0]);
  }

  /**
   * La formule utilis�e pour le calcul de la variable en entr�e
   * 
   * @param _var La variable
   * @return La formule, ou null si la variable n'est pas s�dimentaire.
   */
  public static MetierFormuleSediment getFormule(MetierDescriptionVariable _var) {
    for (MetierFormuleSediment formule : values()) {
      if (formule.getVariable().nom().equals(_var.nom()))
        return formule;
    }
    return null;
  }

  /**
   * Calcul de la contrainte hydrodynamique moyenne sur la section (utilis� par
   * quelques formules).
   * Utilise les r�sultats KMIN, PENE, B1 et Y.
   * 
   * @param _params Les parametres de sedimentologie.
   * @param _adapter Les resultats
   * @param _ibief L'indice de bief
   * @param _isect L'indice de section
   * @param _itps L'indice de temps.
   */
  public RetTauMoy calculerTaumoy(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {
    RetTauMoy ret = new RetTauMoy();

    double kr = _params.getRugosite();
    double ks = _adapter.getValue(MetierDescriptionVariable.KMIN, _ibief, _itps, _isect);
    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double rh1 = _adapter.getValue(MetierDescriptionVariable.RH1, _ibief, _itps, _isect);
    
    double d50 = _params.getD50();
    double diam = _params.getDmoyen();
    double dens = _params.getDensiteMateriau();

    // Calcul de tau
//    double rh1 = (h1 * larg) / (larg + 2. * h1);

    double tau = 9.81 * 1000. * rh1 * pente;
    double kpeau;
    if (kr > ks) {
      kpeau = kr;
    }
    else {
      kpeau = ks;
    }

    double taueff = Math.pow((ks / kpeau), 1.5) * tau;

    // ecriture de Tau
    ret.tau = tau;
    ret.taueff = taueff;
    ret.teta = tau / (9.81 * 1000. * (dens - 1) * diam);
    ret.tetaD50 = tau / (9.81 * 1000. * (dens - 1) * d50);

    return ret;
  }

  /**
   * Calcul du d�bit d'entrainement (pour certaines formules)
   * @param _params
   * @param _tauCible
   * @param _adapter
   * @param _ibief
   * @param _isect
   * @param _itps
   * @return
   */
  public double calculerDebitCrit(MetierParametresSediment _params, double _tauCible, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {
    double ks = _adapter.getValue(MetierDescriptionVariable.KMIN, _ibief, _itps, _isect);
    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double Larg = _adapter.getValue(MetierDescriptionVariable.B1, _ibief, _itps, _isect);

    double dens = _params.getDensiteMateriau();
    double dm = _params.getDmoyen();

    double rhc = _tauCible * (dens - 1) * dm / pente;
    double hc = rhc * Larg / (Larg - 2 * rhc);
    double surf = hc * Larg;

    return ks * surf / Math.pow(pente, (1. / 6)) * Math.pow((_tauCible * (dens - 1) * dm), (2. / 3));
  }
}