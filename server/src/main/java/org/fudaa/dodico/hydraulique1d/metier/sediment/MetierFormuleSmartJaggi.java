package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

public class MetierFormuleSmartJaggi extends MetierFormuleSediment {

  @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {
    double pente = _adapter.getValue(MetierDescriptionVariable.PENE, _ibief, _itps, _isect);
    double qmin = _adapter.getValue(MetierDescriptionVariable.QMIN, _ibief, _itps, _isect);

    double dens = _params.getDensiteMateriau();
    double d30 = _params.getD30();
    double d90 = _params.getD90();

    double rap = Math.pow(d90 / d30, 0.2);
    double tetac = 0.05 * Math.cos(Math.atan(pente)) * (1. - pente / Math.tan(0.61));

    RetTauMoy taumoy = calculerTaumoy(_params, _adapter, _ibief, _isect, _itps);

    double qs;
    if (tetac > taumoy.teta) {
      qs = 0;
    }
    else {
      qs = qmin * 4 * rap * Math.pow(pente, 1.6) / (dens - 1) * (1 - tetac / taumoy.teta);
    }

    return qs;
  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] { 
        MetierDescriptionVariable.CHAR,
        MetierDescriptionVariable.QMIN,
        MetierDescriptionVariable.KMIN,
        MetierDescriptionVariable.RH1
    };
  }

  @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_SMARTJAGGI;
  }

  @Override
  public String getName() {
    return "Smart & J�ggi";
  }
}
