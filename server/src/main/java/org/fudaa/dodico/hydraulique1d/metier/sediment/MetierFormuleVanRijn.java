package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;

/**
 * Calcul sÚdimentaire avec la formule de Van Rijn.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: MetierFormuleLefort1991.java 8524 2013-10-18 08:01:47Z
 *          bmarchan$
 */
public class MetierFormuleVanRijn extends MetierFormuleSediment {

  @Override
  public double calculer(MetierParametresSediment _params, MetierResultatsTemporelSpacialI _adapter, int _ibief, int _isect, int _itps) {

    double larg = _adapter.getValue(MetierDescriptionVariable.B1, _ibief, _itps, _isect);
    double rh1 = _adapter.getValue(MetierDescriptionVariable.RH1, _ibief, _itps, _isect);
    double vmin = _adapter.getValue(MetierDescriptionVariable.VMIN, _ibief, _itps, _isect);

    double dens = _params.getDensiteMateriau();
    double d50 = _params.getD50();
    double d90 = _params.getD90();
    double nu = _params.getViscosite();

    // ' initialisation des colonnes et des lignes
    // Col_Rijn = Range("Rijn").Column
    // Col_deb_cible = Range("Qcible").Column
    // Col_H = Range("hauteur").Column
    //
    // lgn_calcul = Range("ligne_debut").Row
    // ligne = lgn_calcul
    //
    // Do While Cells(ligne, Col_deb_cible) <> ""
    //
    // Qmin = Cells(ligne, Col_deb_cible)
    // H1 = Cells(ligne, Col_H)

//    double rh1 = (h1 * larg) / (larg + 2 * h1);
//    double u = qmin / (larg * h1);
    double det = d50 * Math.pow(((9.81 * (dens - 1)) / Math.pow(nu, 2)), (1. / 3));

    // 'calcul de uc*^2 via la courbe de schield
    double alfa;
    double beta;
    if (det < 4) {
      alfa = 0.24;
      beta = -1;
    }
    else if (det < 10) {
      alfa = 0.14;
      beta = -0.64;
    }
    else if (det < 20) {
      alfa = 0.04;
      beta = -0.1;
    }
    else if (det < 150) {
      alfa = 0.013;
      beta = 0.29;
    }
    else {
      alfa = 0.055;
      beta = 0;
    }

    double ucetCarre = 9.81 * (dens - 1) * d50 * (alfa * Math.pow(det, beta));
    double uet = Math.pow(9.81, 0.5) * vmin / (18 * Math.log10(4 * rh1 / d90));
    double t;

    if (Math.pow(uet, 2) > ucetCarre) {
      t = (Math.pow(uet, 2) - ucetCarre) / ucetCarre;
    }
    else {
      t = 0;
    }

    double qs = larg * 0.053 * Math.pow((9.81 * (dens - 1) * Math.pow(d50, 3)), 0.5) * Math.pow(t, 2.1) / Math.pow(det, 0.3);
    return qs;
  }

  @Override
  public MetierDescriptionVariable[] getRequiredVariable() {
    return new MetierDescriptionVariable[] { 
        MetierDescriptionVariable.B1, 
        MetierDescriptionVariable.RH1,
        MetierDescriptionVariable.VMIN
    };
  }

  @Override
  public MetierDescriptionVariable getVariable() {
    return MetierDescriptionVariable.QS_VANRIJN;
  }

  @Override
  public String getName() {
    return "Van Rijn";
  }
}
