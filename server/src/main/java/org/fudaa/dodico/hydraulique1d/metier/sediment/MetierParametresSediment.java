package org.fudaa.dodico.hydraulique1d.metier.sediment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/**
 * Classe de stockage des parametres de granulometrie pour le calcul de s�diments.
 * 
 * @version      $Id$
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class MetierParametresSediment extends MetierHydraulique1d {
  /** Le calcul s�dimentaire est-il activ� ? */
  private boolean isActif_;
  /** Temperature de l'eau */
  private double tEau_;
  /** Diametre moyen */
  private double dmoy_;
  /** Diametre fractile 30 */
  private double d30_;
  /** Diametre fractile 50 */
  private double d50_;
  /** Diametre fractile 84 */
  private double d84_;
  /** Diametre fractile 90 */
  private double d90_;
  /** Rugosite de grain */
  private double rugo_=60;
  /** Densite de materiau */
  private double densMat_=2.65;
  /** Densite apparente */
  private double densApparente_=2;
  /** Contrainte critique adimensionnelle */
  private double tauc_=0.047;
  /** R�sultats calcul�s */
  private MetierFormuleSediment[] formules_=new MetierFormuleSediment[0];
  /** Les r�sultats moyenn�s sont-ils calcul�s */
  private boolean isResMoyCalcules_;
  /** La fenetre pour le calcul de moyennation */
  private double lgFenetre_=0.0;

  // Constructeur.
  public MetierParametresSediment() {
    super();

    notifieObjetCree();
  }

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierParametresSediment) {
      MetierParametresSediment q=(MetierParametresSediment)_o;
      setTEau(q.getTauc());
      setDmoyen(q.getDmoyen());
      setD30(q.getD30());
      setD50(q.getD50());
      setD84(q.getD84());
      setD90(q.getD90());
      setRugosite(q.getRugosite());
      setDensiteApparente(q.getDensiteApparente());
      setDensiteMateriau(q.getDensiteMateriau());
      setTauc(q.getTauc());
      setFormules(q.getFormules());
      setActif(q.isActif());
      setLargeurFenetre(q.getLargeurFenetre());
      setResMoyCalcules(q.isResMoyCalcules());
    }
  }

  @Override
  final public MetierParametresSediment creeClone() {
    MetierParametresSediment p= new MetierParametresSediment();
    p.initialise(this);
    return p;
  }

  @Override
  final public String toString() {
    String s = "parametresSediment";
    return s;
  }

  public void setActif(boolean _b) {
    if (isActif_==_b) return;
    this.isActif_ = _b;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "CalculActif");
  }
  
  public boolean isActif() {
    return isActif_;
  }
  
  public double getTEau() {
    return tEau_;
  }
  
  public double getViscosite() {
    return 0.001382/Math.pow((getTEau()+50),1.7);
  }

  public void setTEau(double tEau) {
    if (tEau_==tEau) return;
    this.tEau_ = tEau;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "TEau");
  }

  public double getDmoyen() {
    return dmoy_;
  }

  public void setDmoyen(double dmoy) {
    if (dmoy_==dmoy) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "DMoyen");
    this.dmoy_ = dmoy;
  }

  public double getD30() {
    return d30_;
  }

  public void setD30(double d30) {
    if (d30_==d30) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "D30");
    this.d30_ = d30;
  }

  public double getD50() {
    return d50_;
  }

  public void setD50(double d50) {
    if (d50_==d50) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "D50");
    this.d50_ = d50;
  }

  public double getD84() {
    return d84_;
  }

  public void setD84(double d84) {
    if (d84_==d84) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "D84");
    this.d84_ = d84;
  }

  public double getD90() {
    return d90_;
  }

  public void setD90(double d90) {
    if (d90_==d90) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "D90");
    this.d90_ = d90;
  }

  public double getRugosite() {
    return rugo_;
  }

  public void setRugosite(double rugo) {
    if (rugo_==rugo) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "Rugosite");
    this.rugo_ = rugo;
  }

  public double getDensiteMateriau() {
    return densMat_;
  }

  public void setDensiteMateriau(double densMat) {
    if (densMat_==densMat) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "DensiteMateriau");
    this.densMat_ = densMat;
  }

  public double getDensiteApparente() {
    return densApparente_;
  }

  public void setDensiteApparente(double densApparente) {
    if (densApparente_==densApparente) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "DensiteApparente");
    this.densApparente_ = densApparente;
  }

  public double getTauc() {
    return tauc_;
  }

  public void setTauc(double tauc) {
    if (tauc_==tauc) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "Tauc");
    this.tauc_ = tauc;
  }

  public MetierFormuleSediment[] getFormules() {
    return formules_;
  }

  public void setFormules(MetierFormuleSediment[] _formules) {
    if (Arrays.equals(formules_, _formules)) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "Formules");
    this.formules_ = _formules;
  }
  
  public boolean isResMoyCalcules() {
    return isResMoyCalcules_;
  }

  public void setResMoyCalcules(boolean isResMoyCalcules) {
    if (isResMoyCalcules_==isResMoyCalcules) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "ResultatsMoyennes");
    this.isResMoyCalcules_ = isResMoyCalcules;
  }

  public double getLargeurFenetre() {
    return lgFenetre_;
  }

  public void setLargeurFenetre(double largeurFenetre) {
    if (lgFenetre_==largeurFenetre) return;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "LargeurFenetre");
    this.lgFenetre_ = largeurFenetre;
  }



  /**
   * Controle que les variables pass�es en param�tres suffisent a calculer les formules s�dimentaires.
   * 
   * @param _vars Les variables existantes.
   * @return Les variables manquantes, ou vide si aucune variable ne manque.
   */
  public MetierDescriptionVariable[] isVariablesManquantes(MetierDescriptionVariable[] _vars) {
    Map<String,MetierDescriptionVariable> reqVariables=new HashMap<String,MetierDescriptionVariable>();
    
    for (MetierFormuleSediment form : formules_) {
      MetierDescriptionVariable[] vars=form.getRequiredVariable();
      for (MetierDescriptionVariable var : vars) {
        reqVariables.put(var.nom(),var);
      }
    }
    
    for (MetierDescriptionVariable var : _vars) {
      reqVariables.remove(var.nom());
    }
    
    return reqVariables.values().toArray(new MetierDescriptionVariable[0]);
  }

  
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Parametres");
    res[1]=
      super.getInfos()[1]
        + " TEau=" + tEau_
        + " Dmoyen=" + dmoy_
        + " D30=" + d30_
        + " D50=" + d50_
        + " D84=" + d84_
        + " D90=" + d90_
        + " Rugosite=" + rugo_
        + " Densite apparente=" + densApparente_
        + " Densite materiau=" + densMat_
        + " Tauc=" + tauc_
        + " Nb formules=" + formules_ ==null ? "0":""+formules_.length
        + "Resultats moyennes=" + isResMoyCalcules_
        + "Fenetre moyennation=" + lgFenetre_;

    return res;
  }
}
