package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpacialI;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;

/**
 * Un adapter pour les r�sultats temporels. Retourne la valeur d'un r�sultat pour un bief donn�,
 * un profil et un temps donn�e.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class MetierResultatTemporelSpatialAdapter implements MetierResultatsTemporelSpacialI {
  private MetierResultatsTemporelSpatial res_;
  
  public MetierResultatTemporelSpatialAdapter(MetierResultatsTemporelSpatial _res) {
    res_=_res;
  }
  
  @Override
  public double getValue(MetierDescriptionVariable _var, int _ibief, int _itps, int _isect) {
    
    double[][][] vals=res_.resultatsBiefs()[_ibief].valeursVariables();
    double[] absSect=res_.resultatsBiefs()[_ibief].abscissesSections();
    
    // Calcul a la vol�e de la pente d'energie
    if (MetierDescriptionVariable.PENE.equals(_var)) {
      int indChargeVar=res_.getIndiceVariable(MetierDescriptionVariable.CHAR);
      if (indChargeVar==-1) {
        return Double.NaN;
      }
      if (vals[indChargeVar][_itps].length==1) {
        return vals[indChargeVar][_itps][_isect];
      }
      else {
        int isectInf = Math.max(_isect - 1, 0);
        int isectSup = Math.min(_isect + 1, vals[indChargeVar][_itps].length - 1);
        return Math.abs((vals[indChargeVar][_itps][isectSup] - vals[indChargeVar][_itps][isectInf]) / (absSect[isectSup] - absSect[isectInf]));
      }
    }
    // Pour les autres variables, acc�s direct
    else {
      return res_.getValue(_var, _ibief, _itps, _isect);
    }
  }

  @Override
  public int getNbBiefs() {
    return res_.getNbBiefs();
  }

  @Override
  public int getNbTemps() {
    return res_.getNbTemps();
  }

  @Override
  public int getNbSections(int _ibief) {
    return res_.getNbSections(_ibief);
  }
}
