/*
 * @file         MetierCasier.java
 * @creation
 * @modification $Date: 2007-11-20 11:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.sediment;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/**
 * Objet racine de la s�dimentologie.
 * 
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:26 $ by $Author: bmarchan $
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class MetierSediment extends MetierHydraulique1d {

  /**
   * L'ensemble des variables s�dimentaires
   */
  public final static MetierDescriptionVariable[] VARIABLES = { 
    MetierDescriptionVariable.QS_ENGELUND, 
    MetierDescriptionVariable.QS_LEFORT07, 
    MetierDescriptionVariable.QS_LEFORT91,
    MetierDescriptionVariable.QS_MEYERPETER, 
    MetierDescriptionVariable.QS_RECKING10, 
    MetierDescriptionVariable.QS_RECKING11, 
    MetierDescriptionVariable.QS_RICKENMANN,
    MetierDescriptionVariable.QS_SMARTJAGGI, 
    MetierDescriptionVariable.QS_VANRIJN,
  };
  
  /**
   * L'ensemble des variables s�dimentaires moyennees associ�es
   */
  public final static MetierDescriptionVariable[] VARIABLE_MOY = {
    MetierDescriptionVariable.QS_MOY_ENGELUND, 
    MetierDescriptionVariable.QS_MOY_LEFORT07, 
    MetierDescriptionVariable.QS_MOY_LEFORT91,
    MetierDescriptionVariable.QS_MOY_MEYERPETER, 
    MetierDescriptionVariable.QS_MOY_RECKING10, 
    MetierDescriptionVariable.QS_MOY_RECKING11, 
    MetierDescriptionVariable.QS_MOY_RICKENMANN,
    MetierDescriptionVariable.QS_MOY_SMARTJAGGI, 
    MetierDescriptionVariable.QS_MOY_VANRIJN 
  };
  
  private MetierParametresSediment params_;

  // Constructeur.
  public MetierSediment() {
    super();
    params_=new MetierParametresSediment();

    notifieObjetCree();
  }

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSediment) {
      MetierSediment q= (MetierSediment)_o;
      parametres(q.parametres().creeClone());
    }
  }

  @Override
  final public MetierSediment creeClone() {
    MetierSediment p=new MetierSediment();
    p.initialise(this);
    return p;
  }

  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("S�diment");
    res[1]=super.getInfos()[1];

    return res;
  }

  @Override
  public void dispose() {
    if (params_!=null) params_.dispose();
    super.dispose();
  }
  
  /**
   * @param _var La variable
   * @return Le r�sultat moyenn� associ� � la variable.
   */
  public static MetierDescriptionVariable getResultatMoyenneAssocie(MetierDescriptionVariable _var) {
    for (int i=0; i<VARIABLES.length; i++) {
      if (VARIABLES[i].nom().equals(_var.nom())) {
        return VARIABLE_MOY[i];
      }
    }
    
    throw new IllegalArgumentException("Cette variable n'est pas s�dimentaire");
  }

  //---  Interface MetierCalageAuto {  ----------------------------------------------

  public MetierParametresSediment parametres() {
    return params_;
  }

  public void parametres(MetierParametresSediment _params) {
    if (params_==_params) return;
    params_=_params;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "params");
  }

  //---  } Interface MetierCalageAuto  ----------------------------------------------
}
