package org.fudaa.dodico.hydraulique1d.metier.singularite;


/**
* org/fudaa/dodico/corba/hydraulique1d/singularite/EnumMetierEpaisseurSeuil.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from D:/projets/fudaa/devel/produit/fudaa_devel/dodico/idl/metier/hydraulique1d.idl
* mercredi 11 juillet 2007 15 h 16 CEST
*/

public class EnumMetierEpaisseurSeuil
{
  private        int __value;
  private static int __size = 2;
  private static org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil[] __array = new org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil [__size];

  public static final int _EPAIS = 0;
  public static final org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil EPAIS = new org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil(_EPAIS);
  public static final int _MINCE = 1;
  public static final org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil MINCE = new org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil(_MINCE);

  public int value ()
  {
    return __value;
  }

  public static org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil from_int (int value)
  {
    if (value >= 0 && value < __size)
      return __array[value];
    else
      throw new org.omg.CORBA.BAD_PARAM ();
  }

  //  public EnumMetierEpaisseurSeuil ()
  //  {}

  protected EnumMetierEpaisseurSeuil (int value)
  {
    __value = value;
    __array[__value] = this;
  }
} // class EnumMetierEpaisseurSeuil
