/**
 * @file         MetierApport.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimnigramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "apport" de d�bit.
 * Associe une longueur, un coefficient de d�bit et une r�f�rence vers une loi
 * de type hydrogramme.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:36 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierApport extends MetierSingularite {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierApport) {
      MetierApport a= (MetierApport)_o;
      coefficient(a.coefficient());
      longueur(a.longueur());
      if (a.loi() != null)
        loi((MetierLoiHydrogramme)a.loi());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierApport a= new MetierApport();
    a.initialise(this);
    return a;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= getS("apport")+" " + nom_;
    if (l != null)
      s += "("+getS("loi")+" " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("D�bit apport-Singularit� n�")+numero_;
    res[1]= getS("Abscisse")+" : " + abscisse_ + " "+getS("Longueur")+" : " + longueur_;
    if (loi_ != null)
      res[1]= res[1] + " "+getS("Loi")+" : " + loi_.nom();
    else
      res[1]= res[1] + " "+getS("Loi inconnue");
    return res;
  }
  /*** MetierApport ***/
  // constructeurs
  public MetierApport() {
    super();
    nom_= getS("D�bit apport-Singularit� n�")+numero_;
    coefficient_= 1.;
    longueur_= 0.;
    loi_= null;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    coefficient_= 0.;
    longueur_= 0.;
    loi_= null;
    super.dispose();
  }
  // attributs
  private double coefficient_;
  public double coefficient() {
    return coefficient_;
  }
  public void coefficient(double coefficient) {
    if (coefficient_== coefficient) return;
    coefficient_= coefficient;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefficient");
  }
  private double longueur_;
  public double longueur() {
    return longueur_;
  }
  public void longueur(double longueur) {
    if (longueur_== longueur) return;
    longueur_= longueur;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "longueur");
  }
  private MetierLoiHydraulique loi_;
  public MetierLoiHydraulique loi() {
    return loi_;
  }
  public void loi(MetierLoiHydraulique loi) {
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }
  // Methode
  public MetierLoiTarage creeLoiTarage() {
    MetierLoiTarage tar= new MetierLoiTarage();
    loi(tar);
    return tar;
  }
  public MetierLoiHydrogramme creeLoiHydrogramme() {
    MetierLoiHydrogramme tar= new MetierLoiHydrogramme();
    loi(tar);
    return tar;
  }
  public MetierLoiLimnigramme creeLoiLimnigramme() {
    MetierLoiLimnigramme tar= new MetierLoiLimnigramme();
    loi(tar);
    return tar;
  }
  @Override
  public MetierLoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return loi_;
  }
}
