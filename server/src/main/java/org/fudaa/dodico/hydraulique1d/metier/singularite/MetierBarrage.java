/**
 * @file         MetierBarrage.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
/**
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:35 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierBarrage extends MetierSeuil {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierBarrage) {
      MetierBarrage b= (MetierBarrage)_o;
      coteCrete(b.coteCrete());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierBarrage b= new MetierBarrage();
    b.initialise(this);
    return b;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "barrage " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  /*** MetierBarrage ***/
  // constructeurs
  public MetierBarrage() {
    super();
    coteCrete_= 0.;
    
    notifieObjetCree();
  }
  @Override
  public void dispose() {
    coteCrete_= 0.;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  public double coteCrete() {
    return coteCrete_;
  }
  public void coteCrete(double coteCrete) {
    if (coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteCrete");
  }
}
