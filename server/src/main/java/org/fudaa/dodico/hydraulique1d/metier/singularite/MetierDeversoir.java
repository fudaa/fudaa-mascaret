/**
 * @file         MetierDeversoir.java
 * @creation     2000-11-17
 * @modification $Date: 2007-11-20 11:42:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
/**
 * Abstraction de l'objet m�tier singularit� de type "d�versoir" de lat�ral de d�bit.
 * Ajoute une longueur.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:37 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public abstract class MetierDeversoir extends MetierSingularite {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierDeversoir) {
      MetierDeversoir s= (MetierDeversoir)_o;
      longueur(s.longueur());
    }
  }
  @Override
  public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "deversoir " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  // Methode
  @Override
  public MetierLoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return null;
  }
  /*** MetierDeversoir ***/
  // constructeurs
  public MetierDeversoir() {
    super();
    nom_= "D�versoir lat�ral-Singularit� n�"+numero_;
    longueur_= 1.;
  }
  @Override
  public void dispose() {
    nom_= null;
    longueur_= 0;
    super.dispose();
  }
  // Attributs
  protected double longueur_;
  public double longueur() {
    return longueur_;
  }
  public void longueur(double longueur) {
    if (longueur_== longueur) return;
    longueur_= longueur;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "longueur");
  }
  // Methode
  public MetierDeversoirComportementLoi transformeDeversoirLoi() {
    MetierDeversoirComportementLoi deversoir;
    deversoir= new MetierDeversoirComportementLoi();
    deversoir.numero(numero_);
    deversoir.nom(nom_);
    deversoir.abscisse(abscisse_);
    deversoir.longueur(longueur_);
    return deversoir;
  }
  public MetierDeversoirComportementZCoefQ transformeDeversoirZQ() {
    MetierDeversoirComportementZCoefQ deversoir;
    deversoir=
      new MetierDeversoirComportementZCoefQ();
    deversoir.numero(numero_);
    deversoir.nom(nom_);
    deversoir.abscisse(abscisse_);
    deversoir.longueur(longueur_);
    return deversoir;
  }
}
