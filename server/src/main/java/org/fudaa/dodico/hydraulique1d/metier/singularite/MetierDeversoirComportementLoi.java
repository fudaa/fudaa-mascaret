/**
 * @file         MetierDeversoirComportementLoi.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "d�versoir" de lat�ral
 * dont le "comportement" est d�fini par une "loi".
 * Ajoute une r�f�rence de type loi de  tarage.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:37 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierDeversoirComportementLoi extends MetierDeversoir {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierDeversoirComportementLoi) {
      MetierDeversoirComportementLoi s= (MetierDeversoirComportementLoi)_o;
      if (s.loi() != null){
          if (s.loi() instanceof MetierLoiTarage)
              loi((MetierLoiTarage) s.loi().creeClone());
      }else{
          loi(null);
      }
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierDeversoirComportementLoi s=
      new MetierDeversoirComportementLoi();
    s.initialise(this);
    return s;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "deversoirComportementLoi " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("D�versoir-Singularit� n�")+numero_;
    res[1]= getS("Abscisse")+" : " + abscisse_;
    if (loi_ != null)
      res[1]= res[1] + " "+getS("loi de tarage")+" : " + loi_.nom();
    else
      res[1]= res[1] + " "+getS("Loi inconnue");
    return res;
  }
  /*** MetierDeversoirComportementLoi ***/
  // constructeurs
  public MetierDeversoirComportementLoi() {
    super();
    nom_= getS("D�versoir lat�ral-Singularit� n�")+numero_;
    longueur_= 1.;
    loi_= null;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    longueur_= 0;
    loi_= null;
    super.dispose();
  }
  // attributs
  private MetierLoiHydraulique loi_;
 public MetierLoiHydraulique loi() {
   return loi_;
 }
 public void loi(MetierLoiHydraulique loi) {
   if (loi_==loi) return;
   loi_= loi;
   Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
 }

  /*private MetierLoiTarage loi_;
  public MetierLoiTarage loi() {
    return loi_;
  }
  public void loi(MetierLoiTarage loi) {
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }*/
  // Methode

  @Override
  public MetierLoiHydraulique creeLoi() {
  MetierLoiTarage loi= new MetierLoiTarage();
  loi(loi);
  return loi;
}

  @Override
  public MetierLoiHydraulique getLoi() {
    return loi_;
  }
}
