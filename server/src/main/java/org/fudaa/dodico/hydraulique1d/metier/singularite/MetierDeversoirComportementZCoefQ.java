/**
 * @file         MetierDeversoirComportementZCoefQ.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "d�versoir" de lat�ral
 * dont le "comportement" est d�fini par une coefficient de d�bit.
 * Ajoute un coefficient de d�bit et la cote de la cr�te.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:36 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierDeversoirComportementZCoefQ extends MetierDeversoir {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierDeversoirComportementZCoefQ) {
      MetierDeversoirComportementZCoefQ d= (MetierDeversoirComportementZCoefQ)_o;
      coteCrete(d.coteCrete());
      coefQ(d.coefQ());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierDeversoirComportementZCoefQ d=
      new MetierDeversoirComportementZCoefQ();
    d.initialise(this);
    return d;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "deversoirComportementZCoefQ " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("D�versoir-Singularit� n�")+numero_;
    res[1]=
      getS("Abscisse")+" : "
        + abscisse_
        + " "+getS("cote cr�te")+" : "
        + coteCrete_
        + " "+getS("coef. Q")+" : "
        + coefQ_;
    return res;
  }
  /*** MetierDeversoirComportementZCoefQ ***/
  // constructeurs
  public MetierDeversoirComportementZCoefQ() {
    super();
    nom_= getS("D�versoir lat�ral-Singularit� n�")+numero_;
    longueur_= 1.;
    coefQ_= 0;
    coteCrete_= 0;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    longueur_= 0;
    coefQ_= 0;
    coteCrete_= 0;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  public double coteCrete() {
    return coteCrete_;
  }
  public void coteCrete(double coteCrete) {
    if (coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteCrete");
  }
  private double coefQ_;
  public double coefQ() {
    return coefQ_;
  }
  public void coefQ(double coefQ) {
    if (coefQ_== coefQ) return;
    coefQ_= coefQ;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefQ");
  }
}
