/**
 * @file         MetierPerteCharge.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "perte de charge" singuli�re.
 * Ajoute un coefficient de perte de charge.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:36 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierPerteCharge extends MetierSingularite {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierPerteCharge) {
      MetierPerteCharge a= (MetierPerteCharge)_o;
      coefficient(a.coefficient());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierPerteCharge p= new MetierPerteCharge();
    p.initialise(this);
    return p;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "perteCharge " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Perte de charge-Singularit� n�")+numero_;
    res[1]= getS("Abscisse")+" : " + abscisse_ + " "+getS("Coefficient")+" : " + coefficient_;
    return res;
  }
  // Methode
  @Override
  public MetierLoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return null;
  }
  /*** MetierPerteCharge ***/
  // constructeurs
  public MetierPerteCharge() {
    super();
    nom_= "Perte de charge-Singularit� n�"+numero_;
    coefficient_= 1.;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    coefficient_= 0;
    super.dispose();
  }
  // attributs

  private double coefficient_;
  public double coefficient() {
    return coefficient_;
  }
  public void coefficient(double coefficient) {
    if (coefficient_==coefficient) return;
    coefficient_= coefficient;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefficient");
  }
}
