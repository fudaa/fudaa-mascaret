/**
 * @file         MetierSeuil.java
 * @creation     2000-11-17
 * @modification $Date: 2007-11-20 11:42:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
/**
 * Abstraction de l'objet m�tier singularit� de type "seuil".
 * Ajoute une cote de rupture.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:37 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public abstract class MetierSeuil extends MetierSingularite {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuil) {
      MetierSeuil s= (MetierSeuil)_o;
      coteRupture(s.coteRupture());
    }
  }
  @Override
  public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "seuil " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Seuil-Singularit� n�")+numero_;
    res[1]= getS("Abscisse")+" : " + abscisse_ + " "+getS("Cote rupture")+" : " + coteRupture_;
    if (getLoi() != null)
      res[1]= res[1] + " " + getLoi().typeLoi() + " : " + getLoi().nom();
    else
      res[1]= res[1] + " "+getS("Loi inconnue");
    return res;
  }
  // Methode
  @Override
  public MetierLoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return null;
  }
  // constructeurs
  public MetierSeuil() {
    super();
    nom_=getS("Barrage/Seuil-Singularit� n�")+numero_;
    coteRupture_= 10000.;
  }
  @Override
  public void dispose() {
    nom_= null;
    coteRupture_= 0.;
    super.dispose();
  }
  // Attributs
  protected double coteRupture_;
  public double coteRupture() {
    return coteRupture_;
  }
  public void coteRupture(double coteRupture) {
    if (coteRupture_==coteRupture) return;
    coteRupture_= coteRupture;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteRupture");
  }
}
