/**
 * @file         MetierSeuilDenoye.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil d�noy�".
 * Ajoute une cote de cr�te, une cote moyenne de cr�te, une r�f�rence vers une loi de tarage.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:39 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierSeuilDenoye extends MetierSeuil {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuilDenoye) {
        MetierSeuilDenoye s = (MetierSeuilDenoye) _o;
        coteMoyenneCrete(s.coteMoyenneCrete());
        coteCrete(s.coteCrete());
        if (s.loi() != null)
            loi((MetierLoiTarage) s.loi().creeClone());
        /*paramAvances(s.paramAvances());
        gradient(s.gradient());
        loiHydro(s.loiHydro());*/
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSeuilDenoye s= new MetierSeuilDenoye();
    s.initialise(this);
    return s;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "seuilDenoye " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Seuil abaques (Q, Zam)-Singularit� n�")+numero_;
    res[1]= getS("Abscisse")+" : " + abscisse_ + " "+getS("Cote rupture")+" : " + coteRupture_;
    if (loi_ != null)
      res[1]= res[1] + " "+getS("Loi de tarage")+" : " + loi_.nom();
    else
      res[1]= res[1] + " "+getS("Loi inconnue");
   if (paramAvances_) {
      res[1] += " "+getS("gradient Q")+" : " + gradient_;
      if (loiHydro_ != null)
          res[1] += " "+getS("Loi Hydrogramme")+" : " + loiHydro_.nom();
      else
          res[1] += " "+getS("Loi Hydrogramme inconnue");
  }

    return res;
  }
  /*** MetierSeuilDenoye ***/
  // constructeurs
  public MetierSeuilDenoye() {
    super();
    nom_= getS("Seuil d�noy�-Singularit� n�")+numero_;
    coteMoyenneCrete_= 0.;
    coteCrete_= 0.;
    paramAvances_= true;
    gradient_= 5000.;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    coteMoyenneCrete_= 0.;
    coteCrete_= 0.;
    paramAvances_= true;
    gradient_= 0;
    super.dispose();
  }
  // attributs
  private double coteMoyenneCrete_;
  public double coteMoyenneCrete() {
    return coteMoyenneCrete_;
  }
  public void coteMoyenneCrete(double coteMoyenneCrete) {
    if(coteMoyenneCrete_== coteMoyenneCrete) return;
    coteMoyenneCrete_= coteMoyenneCrete;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteMoyenneCrete");
  }
  private double coteCrete_;
  public double coteCrete() {
    return coteCrete_;
  }
  public void coteCrete(double coteCrete) {
    if(coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteCrete");
  }
  private MetierLoiTarage loi_;
  public MetierLoiTarage loi() {
    return loi_;
  }
  public void loi(MetierLoiTarage loi) {
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }
  // Methode
  @Override
  public MetierLoiHydraulique creeLoi() {
    MetierLoiTarage loi= new MetierLoiTarage();
    loi(loi);
    return loi;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return loi_;
  }
  private boolean paramAvances_;
public boolean paramAvances() {
  return paramAvances_;
}
public void paramAvances(boolean paramAvances) {
  if (paramAvances_==paramAvances) return;
  paramAvances_= paramAvances;
  Notifieur.getNotifieur().fireObjetModifie(toString(), this, "paramAvances");
}
private double gradient_;
public double gradient() {
  return gradient_;
}
public void gradient(double gradient) {
  if (gradient_==gradient) return;
  gradient_= gradient;
  Notifieur.getNotifieur().fireObjetModifie(toString(), this, "gradient");
}
private MetierLoiHydrogramme loiHydro_;
public MetierLoiHydrogramme loiHydro() {
  return loiHydro_;
}
public void loiHydro(MetierLoiHydrogramme loiHydro) {
  if (loiHydro_==loiHydro) return;
  loiHydro_= loiHydro;
  Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loiHydro");
}

}
