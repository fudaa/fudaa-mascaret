/**
 * @file         MetierSeuilGeometrique.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil g�om�trique".
 * Ajoute une r�f�rence vers une loi g�om�trique.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:38 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierSeuilGeometrique extends MetierSeuil {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuilGeometrique) {
      MetierSeuilGeometrique s= (MetierSeuilGeometrique)_o;
      coteMoyenneCrete(s.coteMoyenneCrete());
      coefQ(s.coefQ());
      if (s.loi() != null)
        loi((MetierLoiGeometrique)s.loi().creeClone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSeuilGeometrique s=
      new MetierSeuilGeometrique();
    s.initialise(this);
    return s;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "seuilGeometrique " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Seuil profil de cr�te-Singularit� n�")+numero_;
    res[1]= getS("Abscisse")+" : "
            + abscisse_
            + " "+getS("Cote rupture")+" : "
            + coteRupture_
            + getS("cote Moyenne Crete")
            + coteMoyenneCrete_
            + " "+getS("coef. Q")+" : "
            + coefQ_;
    if (loi_ != null)
      res[1]= res[1] + " "+getS("Loi geom�trique")+" : " + loi_.nom();
    else
      res[1]= res[1] + " "+getS("Loi inconnue");
    return res;
  }
  /*** MetierSeuilGeometrique ***/
  // constructeurs
  public MetierSeuilGeometrique() {
    super();
    nom_= getS("Seuil g�om�trique-Singularit� n�")+numero_;
    coteMoyenneCrete_= 0.;
    coefQ_= 0.38;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    coteMoyenneCrete_= 0.;
    coefQ_= 0;
    super.dispose();
  }
  // attributs
  private double coefQ_;
  public double coefQ() {
    return coefQ_;
  }
  public void coefQ(double coefQ) {
    if (coefQ_== coefQ) return;
    coefQ_= coefQ;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefQ");
  }
  private double coteMoyenneCrete_;
    public double coteMoyenneCrete() {
      return coteMoyenneCrete_;
    }
    public void coteMoyenneCrete(double coteMoyenneCrete) {
      if(coteMoyenneCrete_== coteMoyenneCrete) return;
      coteMoyenneCrete_= coteMoyenneCrete;
      Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteMoyenneCrete");
  }
  private MetierLoiGeometrique loi_;
  public MetierLoiGeometrique loi() {
    return loi_;
  }
  public void loi(MetierLoiGeometrique loi) {
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }
  // Methode
  @Override
  public MetierLoiHydraulique creeLoi() {
    MetierLoiGeometrique loi= new MetierLoiGeometrique();
    loi(loi);
    return loi;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return loi_;
  }
}
