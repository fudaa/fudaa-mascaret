/**
 * @file MetierSeuilLoi.java
 * @creation 2000-08-09
 * @modification $Date: 2007-11-20 11:42:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 EDF/LNHE
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;

import org.fudaa.dodico.boony.BoonyDeserializationAware;
import static org.fudaa.dodico.corba.mascaret.SParametresSeuil.PARAMETRES_SEUIL_DEFAULT_VALUE;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil loi". Ajoute une cote de cr�te, un coefficient de d�bit et une �paisseur (mince ou
 * �pais).
 *
 * @version $Revision: 1.2 $ $Date: 2007-11-20 11:42:36 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class MetierSeuilLoi extends MetierSeuil implements BoonyDeserializationAware {

  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuilLoi) {
      MetierSeuilLoi s = (MetierSeuilLoi) _o;
      coteCrete(s.coteCrete());
      coefQ(s.coefQ());
      epaisseur(s.epaisseur());
      gradient(s.gradient());
      debit(s.debit());
    }
  }

  @Override
  public void endDeserialization() {
    if (epaisseur_ == null) {
      epaisseur_ = EnumMetierEpaisseurSeuil.EPAIS;
    }
  }

  @Override
  public MetierHydraulique1d creeClone() {
    MetierSeuilLoi s = new MetierSeuilLoi();
    s.initialise(this);
    return s;
  }

  @Override
  public String toString() {
    MetierLoiHydraulique l = getLoi();
    String s = "seuilLoi " + nom_;
    if (l != null) {
      s += "(loi " + l.toString() + ")";
    }
    return s;
  }

  @Override
  public String[] getInfos() {
    String epaisseur = getS("mince");
    if (epaisseur_.value() == EnumMetierEpaisseurSeuil._EPAIS) {
      epaisseur = getS("�pais");
    }
    String[] res = new String[2];
    res[0] = getS("Seuil (Zcr�te,coef.Q)-Singularit� n�") + numero_;
    res[1]
            = getS("Absc.") + " : "
            + abscisse_
            + " " + getS("Zrupture") + " : "
            + coteRupture_
            + " " + getS("Zcr�te") + " : "
            + coteCrete_
            + " " + getS("Coef. Q") + " : "
            + coefQ_
            + " " + getS("Epaisseur") + " : "
            + epaisseur
            + " " + getS("gradient Q") + " : "
            + gradient_
            + " " + getS("d�bit turbin�") + " : "
            + debit_;
    return res;
  }

  /**
   * * MetierSeuilLoi **
   */
  // constructeurs
  protected MetierSeuilLoi(boolean _notify) {
    super();
    nom_ = getS("Seuil loi-Singularit� n�") + numero_;
    coteCrete_ = 0.;
    coefQ_ = 0.38;
    epaisseur_ = EnumMetierEpaisseurSeuil.EPAIS;
    gradient_ = 5000.;
    debit_ = 0;

    if (_notify) {
      notifieObjetCree();
    }
  }

  public MetierSeuilLoi() {
    this(true);
  }

  @Override
  public void dispose() {
    coteCrete_ = 0.;
    coefQ_ = 0;
    epaisseur_ = null;
    gradient_ = 0.;
    debit_ = 0;
    super.dispose();
  }
  // attributs
  protected double coteCrete_;

  public double coteCrete() {
    return coteCrete_;
  }

  public void coteCrete(double coteCrete) {
    if (coteCrete_ == coteCrete) {
      return;
    }
    coteCrete_ = coteCrete;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteCrete");
  }
  protected double coefQ_;

  public double coefQ() {
    return coefQ_;
  }

  public void coefQ(double coefQ) {
    if (coefQ_ == coefQ) {
      return;
    }
    coefQ_ = coefQ;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coefQ");
  }
  protected EnumMetierEpaisseurSeuil epaisseur_;

  public EnumMetierEpaisseurSeuil epaisseur() {
    return epaisseur_;
  }

  public void epaisseur(EnumMetierEpaisseurSeuil epaisseur) {
    if (epaisseur_.value() == epaisseur.value()) {
      return;
    }
    epaisseur_ = epaisseur;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "epaisseur");
  }

  private double gradient_;

  public double gradient() {
    return gradient_;
  }

  public void gradient(double gradient) {
    if (gradient_ == gradient) {
      return;
    }
    gradient_ = gradient;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "gradient");
  }
  private double debit_;

  public double debit() {
    return debit_;
  }

  public void debit(double debit) {
    if (debit_ == debit) {
      return;
    }
    debit_ = debit;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "debit");
  }

}
