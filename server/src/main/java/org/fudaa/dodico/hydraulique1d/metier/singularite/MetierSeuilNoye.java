/**
 * @file         MetierSeuilNoye.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil noy�".
 * Ajoute une cote moyenne de cr�te, une r�f�rence vers une loi de seuil.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:37 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierSeuilNoye extends MetierSeuil {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuilNoye) {
      MetierSeuilNoye s= (MetierSeuilNoye)_o;
      coteMoyenneCrete(s.coteMoyenneCrete());
      coteCrete(s.coteCrete());
      if (s.loi() != null)
        loi((MetierLoiSeuil)s.loi().creeClone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSeuilNoye s= new MetierSeuilNoye();
    s.initialise(this);
    return s;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "seuilNoye " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Seuil (Zam, Zav, Q)-Singularit� n�")+numero_;
    res[1]=
      getS("Abscisse")+" : "
        + abscisse_
        + " "+getS("Cote rupture")+" : "
        + coteRupture_
        + " "+getS("Cote moyenne cr�te")+" : "
        + coteMoyenneCrete_;
    if (loi_ != null)
      res[1]= res[1] + " "+getS("Loi de seuil")+" : " + loi_.nom();
    else
      res[1]= res[1] + " "+getS("Loi inconnue");
    return res;
  }
  /*** MetierSeuilNoye ***/
  // constructeurs
  public MetierSeuilNoye() {
    super();
    nom_= getS("Seuil noy�-Singularit� n�")+numero_;
    coteCrete_= 0.;
    coteMoyenneCrete_= 0.;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    coteCrete_= 0.;
    coteMoyenneCrete_= 0.;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  public double coteCrete() {
    return coteCrete_;
  }
  public void coteCrete(double coteCrete) {
    if(coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteCrete");
  }

  private double coteMoyenneCrete_;
  public double coteMoyenneCrete() {
    return coteMoyenneCrete_;
  }
  public void coteMoyenneCrete(double coteMoyenneCrete) {
    if (coteMoyenneCrete_==coteMoyenneCrete) return;
    coteMoyenneCrete_= coteMoyenneCrete;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteMoyenneCrete");
  }
  private MetierLoiSeuil loi_;
  public MetierLoiSeuil loi() {
    return loi_;
  }
  public void loi(MetierLoiSeuil loi) {
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }
  // Methode
  @Override
  public MetierLoiHydraulique creeLoi() {
    MetierLoiSeuil loi= new MetierLoiSeuil();
    loi(loi);
    return loi;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return loi_;
  }
}
