/**
 * @file         MetierSeuilTarageAval.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil tarage aval".
 * Ajoute une r�f�rence vers une loi de tarage.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:37 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierSeuilTarageAval extends MetierSeuil {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuilTarageAval) {
      MetierSeuilTarageAval s= (MetierSeuilTarageAval)_o;
      coteCrete(s.coteCrete());
      if (s.loi() != null)
        loi((MetierLoiTarage)s.loi().creeClone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSeuilTarageAval s= new MetierSeuilTarageAval();
    s.initialise(this);
    return s;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "seuilTarageAval " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Seuil abaques (Q, Zav)-Singularit� n�")+numero_;
    res[1]= getS("Abscisse")+" : " + abscisse_ + " "+getS("Cote rupture")+" : " + coteRupture_;
    if (loi_ != null)
      res[1]= res[1] + " "+getS("Loi de tarage")+" : " + loi_.nom();
    else
      res[1]= res[1] + " "+getS("Loi inconnue");
    return res;
  }
  /*** MetierSeuilTarageAval ***/
  // constructeurs
  public MetierSeuilTarageAval() {
    super();
    nom_= getS("Seuil tarage aval-Singularit� n�")+numero_;
    coteCrete_= 0.;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    nom_= null;
    coteCrete_= 0.;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  public double coteCrete() {
    return coteCrete_;
  }
  public void coteCrete(double coteCrete) {
    if(coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "coteCrete");
  }

  private MetierLoiTarage loi_;
  public MetierLoiTarage loi() {
    return loi_;
  }
  public void loi(MetierLoiTarage loi) {
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }
  // Methode
  @Override
  public MetierLoiHydraulique creeLoi() {
    MetierLoiTarage loi= new MetierLoiTarage();
    loi.amont(false);
    loi(loi);
    return loi;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return loi_;
  }
}
