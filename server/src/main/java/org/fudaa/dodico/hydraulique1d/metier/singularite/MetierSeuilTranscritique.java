/**
 * @file         MetierSeuilTranscritique.java
 * @creation     2001-08-01
 * @modification $Date: 2007-11-20 11:42:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil loi transcritique".
 * Ajoute une r�f�rence vers une loi hydrogramme, un gradient,
 * et des indicateurs : param�tres avanc�s et rupture instantan�e.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:38 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierSeuilTranscritique extends MetierSeuilLoi {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuilTranscritique) {
      MetierSeuilTranscritique s= (MetierSeuilTranscritique)_o;
      ruptureInstantanee(s.ruptureInstantanee());//deprecated
      paramAvances(s.paramAvances());//deprecated
      gradient(s.gradient());
      debit(s.debit());
      loi(s.loi()); //deprecated
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSeuilTranscritique s=
      new MetierSeuilTranscritique();
    s.initialise(this);
    return s;
  }
  @Override
  final public String toString() {
    //DLoiHydraulique l= getLoi();
    String s= "seuilTranscritique " + nom_;
    /*if (l != null)
      s += "(loi " + l.toString() + ")";*/ //deprecated
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    String epaisseur=getS("mince");
    if (epaisseur_.value() == EnumMetierEpaisseurSeuil._EPAIS) {
      epaisseur=getS("�pais");
    }
    res[0]= getS("Seuil Loi Transc.-Singularit� n�")+numero_;
    res[1]=
      getS("Absc.")+" : "
        + abscisse_
        + " "+getS("Zrupture")+" : "
        + coteRupture_
        + " "+getS("Zcr�te")+" : "
        + coteCrete_
        + " "+getS("Coef. Q")+" : "
        + coefQ_
        + " "+getS("Epais.")+" : "
        + epaisseur;
    if (ruptureInstantanee_)
      res[1] += " "+getS("rupture instantan�e");
    else
      res[1] += " "+getS("Coef. Q")+" : " + coefQ_;
    if (paramAvances_) {
      res[1] += " "+getS("gradient Q")+" : " + gradient_;
      res[1] += " "+getS("d�bit turbin�")+" : " + debit_;
      /*if (loi_ != null)
        res[1] += " Loi Hydrogramme : " + loi_.nom();
      else
        res[1] += " Loi inconnue";*/
    }
    return res;
  }
  /*** MetierSeuilLoi ***/
  // constructeurs
  public MetierSeuilTranscritique() {
    super(false);
    nom_=getS("Seuil transc.-Singularit� n�")+numero_;
    ruptureInstantanee_= false;
    paramAvances_= true;
    gradient_= 5000.;
    debit_=0;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    ruptureInstantanee_= false;
    paramAvances_= true;
    gradient_= 0;
    debit_=0;
    super.dispose();
  }
  // attributs
  private boolean ruptureInstantanee_;
  public boolean ruptureInstantanee() {
    return ruptureInstantanee_;
  }
  public void ruptureInstantanee(boolean ruptureInstantanee) {
    if (ruptureInstantanee_==ruptureInstantanee) return;
    ruptureInstantanee_= ruptureInstantanee;
    Notifieur.getNotifieur().fireObjetModifie(
      toString(),
      this,
      "ruptureInstantanee");
  }
  private boolean paramAvances_;
  public boolean paramAvances() {
    return paramAvances_;
  }
  public void paramAvances(boolean paramAvances) {
    if (paramAvances_==paramAvances) return;
    paramAvances_= paramAvances;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "paramAvances");
  }
  private double gradient_;
  @Override
  public double gradient() {
    return gradient_;
  }
  @Override
  public void gradient(double gradient) {
    if (gradient_==gradient) return;
    gradient_= gradient;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "gradient");
  }
  private double debit_;
  @Override
  public double debit() {
      return debit_;
  }

  @Override
  public void debit(double debit) {
      if (debit_ == debit)
          return;
      debit_ = debit;
      Notifieur.getNotifieur().fireObjetModifie(toString(), this, "debit");
  }


  private MetierLoiHydrogramme loi_;//deprecated
  public MetierLoiHydrogramme loi() {//deprecated
    return loi_;
  }
  public void loi(MetierLoiHydrogramme loi) {//deprecated
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }
}
