/**
 * @file         MetierSeuilVanne.java
 * @creation     2000-08-09
 * @modification $Date: 2007-11-20 11:42:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiOuvertureVanne;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil de vanne".
 * Ajoute une largeur, une r�f�rence vers une loi d'ouverture de vanne.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:42:39 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class MetierSeuilVanne extends MetierSeuil {
  @Override
  public void initialise(MetierHydraulique1d _o) {
    super.initialise(_o);
    if (_o instanceof MetierSeuilVanne) {
      MetierSeuilVanne s= (MetierSeuilVanne)_o;
      largeur(s.largeur());
      if (s.loi() != null)
        loi((MetierLoiOuvertureVanne)s.loi().creeClone());
    }
  }
  @Override
  final public MetierHydraulique1d creeClone() {
    MetierSeuilVanne s= new MetierSeuilVanne();
    s.initialise(this);
    return s;
  }
  @Override
  final public String toString() {
    MetierLoiHydraulique l= getLoi();
    String s= "Seuil Vanne-Singularit� n�"+numero_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= getS("Seuil Vanne-Singularit� n�")+numero_;
    res[1]=
      getS("Abscisse")+" : "
        + abscisse_
        + " "+getS("Z rupture")+" : "
        + coteRupture_
        + " "+getS("largeur")+" : "
        + largeur_;
    if (loi_ != null)
      res[1]= res[1] + " "+getS("Loi ouverture vanne")+": " + loi_.nom();
    else
      res[1]= res[1] + " "+getS("Loi inconnue");
    return res;
  }
  /*** MetierSeuilVanne ***/
  // constructeurs
  public MetierSeuilVanne() {
    super();
    largeur_= 1;
    loi_= null;

    notifieObjetCree();
  }
  @Override
  public void dispose() {
    largeur_= 0;
    loi_= null;
    super.dispose();
  }
  // attributs
  private double largeur_;
  public double largeur() {
    return largeur_;
  }
  public void largeur(double largeur) {
    if (largeur_==largeur) return;
    largeur_= largeur;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "largeur");
  }
  private MetierLoiOuvertureVanne loi_;
  public MetierLoiOuvertureVanne loi() {
    return loi_;
  }
  public void loi(MetierLoiOuvertureVanne loi) {
    if (loi_==loi) return;
    loi_= loi;
    Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");
  }
  // Methode
  @Override
  public MetierLoiHydraulique creeLoi() {
    MetierLoiOuvertureVanne loi=
      new MetierLoiOuvertureVanne();
    loi(loi);
    return loi;
  }
  @Override
  public MetierLoiHydraulique getLoi() {
    return loi_;
  }
}
