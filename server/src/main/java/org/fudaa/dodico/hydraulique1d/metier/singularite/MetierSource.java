
/**
 * @file         MetierSource.java
 * @creation     2006-02-27
 * @modification $Date: 2007-12-10 18:36:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.metier.singularite;

import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
/**
 * Implémentation de l'objet métier singularité de type "source" de traceur.
 * @version      $Revision: 1.3 $ $Date: 2007-12-10 18:36:50 $ by $Author: jm_lacombe $
 * @author       Olivier Pasteur
 */
public class MetierSource extends MetierSingularite {

  @Override
    public void initialise(MetierHydraulique1d _o) {
        super.initialise(_o);
        if (_o instanceof MetierSource) {
            MetierSource s = (MetierSource) _o;
            longueur(s.longueur());
            type(s.type());
            if (s.loi() != null)
                loi((MetierLoiTracer) s.loi());
        }
    }

  @Override
    final public MetierHydraulique1d creeClone() {
        MetierSource s = new MetierSource();
        s.initialise(this);
        return s;
    }

  @Override
    final public String toString() {
        MetierLoiTracer l = (MetierLoiTracer) getLoi();
        String s = "source " + nom_;
        if (l != null)
            s += "(loi traceur " + l.toString() + ")";
        return s;
    }

  @Override
    public String[] getInfos() {
        String[] res = new String[2];
        res[0] = getS("Source-Singularité n°")+numero_;
        res[1] = getS("Abscisse")+" : " + abscisse_ + " "+getS("Longueur")+" : " + longueur_;
        if (loi_ != null)
            res[1] = res[1] + " "+getS("Loi Tracer")+" : " + loi_.nom();
        else
            res[1] = res[1] + " "+getS("Loi Tracer inconnue");
        return res;
    }

    // constructeurs
    public MetierSource() {
        super();
        nom_ = getS("Source-Singularité n°")+numero_;
        type_ = EnumMetierTypeSource.VOLUME;
        longueur_ = 0.;
        loi_ = null;

        notifieObjetCree();
    }

  @Override
    public void dispose() {
        nom_ = null;
        type_ = EnumMetierTypeSource.VOLUME;
        longueur_ = 0.;
        loi_ = null;
        super.dispose();
    }

    // attributs
    /**
     * longueur
     *
     * @return double
     */
    private double longueur_;
    public double longueur() {
        return longueur_;
    }

    /**
     * longueur
     *
     * @param newLongueur double
     */
    public void longueur(double longueur) {
        if (longueur_ == longueur)
            return;
        longueur_ = longueur;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "longueur");

    }


    /**
     * type
     *
     * @return EnumMetierTypeSource
     */
    private EnumMetierTypeSource type_;
    public EnumMetierTypeSource type() {
        return type_;
    }

    /**
     * type
     *
     * @param newType EnumMetierTypeSource
     */
    public void type(EnumMetierTypeSource newType) {
        if (type_.value() == newType.value())
            return;
        type_ = newType;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "type");
    }


    /**
     * loi
     *
     * @return MetierLoiTracer
     */
    private org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer loi_;
    public MetierLoiTracer loi() {
        return loi_;
    }

    /**
     * loi
     *
     * @param newLoi MetierLoiTracer
     */
    public void loi(MetierLoiTracer newLoi) {
        if (loi_==newLoi) return;
        loi_ = newLoi;
        Notifieur.getNotifieur().fireObjetModifie(toString(), this, "loi");

    }

    /**
     * creeLoi
     *
     * @return DLoiHydraulique
     */
  @Override
    public MetierLoiHydraulique creeLoi() {
        MetierLoiTracer tra = new MetierLoiTracer();
        System.out.println("!!!!!!!!!!!Erreur on utilise la fonction creeLoi dans MetierSource, celle-ci n'est pas valable, il faut utiliser creeLoiTracer sinon le tableau n'est pas dimenssionné en fonction du nombre de traceur");
        loi(tra);
        return tra;

    }

/**
 * creeLoiTracer
 *
 * @return MetierLoiTracer
 */
public MetierLoiTracer creeLoiTracer(int nbColonne) {
    MetierLoiTracer tra = new MetierLoiTracer();
    tra.setTailleTableau(nbColonne);
    loi(tra);
    return tra;
}



    /**
     * getLoi
     *
     * @return DLoiHydraulique
     */
  @Override
    public MetierLoiHydraulique getLoi() {
        return loi_;
    }
}

