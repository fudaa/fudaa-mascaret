package org.fudaa.dodico.hydraulique1d.qualitedeau;

import java.util.Arrays;

import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IConcentrationInitiale;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IConcentrationInitialeOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/**
 * @file         DConcentrationInitiale.java
 * @creation     2006-02-28
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
public class DConcentrationInitiale extends DHydraulique1d
        implements IConcentrationInitiale, IConcentrationInitialeOperations {
  @Override
    final public IObjet creeClone() {
      IConcentrationInitiale c=
        UsineLib.findUsine().creeHydraulique1dConcentrationInitiale();
      c.initialise(tie());
      return c;
    }
  @Override
    public void initialise(IObjet _o) {
        if (_o instanceof IConcentrationInitiale) {
             IConcentrationInitiale c= (IConcentrationInitiale)_o;
             numeroBief(c.numeroBief());
             abscisse(c.abscisse());
             concentrations((double[])c.concentrations().clone());
     }
    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
        numeroBief_=0;
        abscisse_ = 0;
        concentrations_=null;
        super.dispose();

    }

  @Override
    final public String toString() {
      String s= "ConcentrationInitiale";
      return s;
    }
    public DConcentrationInitiale() {
        super();
        numeroBief_ = 1;
        abscisse_=0;
        concentrations_ = new double[0];


    }

    /**
     * abscisse
     *
     * @return double
     */
    private double abscisse_;
  @Override
    public double abscisse() {
        return abscisse_;
    }

    /**
     * abscisse
     *
     * @param newAbscisse double
     */
  @Override
    public void abscisse(double abscisse) {
        if (abscisse_==abscisse) return;
        abscisse_ = abscisse;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "abscisse");

    }


    /**
     * concentrations
     *
     * @return double[]
     */
    private double[] concentrations_;
  @Override
    public double[] concentrations() {
        return concentrations_;
    }

    /**
     * concentrations
     *
     * @param newConcentrations double[]
     */
  @Override
    public void concentrations(double[] newConcentrations) {
        if (Arrays.equals(newConcentrations,concentrations_)) return;
        concentrations_ = newConcentrations;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "concentrations");

    }



    /**
     * numeroBief
     *
     * @return int
     */
        private int numeroBief_;
  @Override
    public int numeroBief() {
        return numeroBief_;
    }

    /**
     * numeroBief
     *
     * @param newNumeroBief int
     */
  @Override
    public void numeroBief(int newNumeroBief) {
        if (numeroBief_==newNumeroBief) return;
        numeroBief_ = newNumeroBief;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "numeroBief");

    }

}
