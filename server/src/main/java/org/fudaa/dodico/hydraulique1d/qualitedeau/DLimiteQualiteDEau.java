package org.fudaa.dodico.hydraulique1d.qualitedeau;

import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTracer;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.ILimiteQualiteDEau;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.ILimiteQualiteDEauHelper;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.ILimiteQualiteDEauOperations;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.LTypeCondLimiteTracer;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

        /*
         * @file         DLimiteQualiteDEau.java
         * @creation     2006-02-28
         * @modification $Date: 2006-09-12 08:35:02 $
         * @license      GNU General Public License 2
         * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
         * @mail         devel@fudaa.org
 */
public class DLimiteQualiteDEau extends DHydraulique1d implements ILimiteQualiteDEauOperations,ILimiteQualiteDEau {
    public DLimiteQualiteDEau() {
       super();
       typeCondLimiteTracer_=LTypeCondLimiteTracer.DIRICHLET;
       loi_=null;
    }


    /**
     * creeClone
     *
     * @return IObjet
     */
  @Override
    public IObjet creeClone() {
        ILimiteQualiteDEau p=UsineLib.findUsine().creeHydraulique1dLimiteQualiteDEau();
        p.initialise(tie());
        return p;

    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
       typeCondLimiteTracer_=LTypeCondLimiteTracer.DIRICHLET;
       loi_=null;
    }


    /**
     * initialise
     *
     * @param o IObjet

     */
  @Override
    public void initialise(IObjet _o) {
        if (_o instanceof ILimiteQualiteDEau) {
            ILimiteQualiteDEau l = ILimiteQualiteDEauHelper.narrow(
                    _o);
            typeCondLimiteTracer(l.typeCondLimiteTracer());
            if (l.loi() != null)  loi((ILoiTracer) l.loi());
         }
    }

    /**
     * loi
     *
     * @param newLoi ILoiTracer
     */

  @Override
    public void loi(ILoiTracer newLoi) {
        if (loi_==newLoi) return;
        loi_ = newLoi;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");

    }

    /**
     * loi
     *
     * @return ILoiTracer
     */
    private ILoiTracer loi_;
  @Override
    public ILoiTracer loi() {
        return loi_;
    }





    /**
     * typeCondLimiteTracer
     *
     * @return LTypeCondLimiteTracer
     */
    private LTypeCondLimiteTracer typeCondLimiteTracer_;
  @Override
    public LTypeCondLimiteTracer typeCondLimiteTracer() {
        return typeCondLimiteTracer_;
    }

    /**
     * typeCondLimiteTracer
     *
     * @param newTypeCondLimiteTracer LTypeCondLimiteTracer
     */
  @Override
    public void typeCondLimiteTracer(LTypeCondLimiteTracer
                                     newTypeCondLimiteTracer) {
        if (typeCondLimiteTracer_.value() == newTypeCondLimiteTracer.value())   return;
        typeCondLimiteTracer_ = newTypeCondLimiteTracer;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "typeCondLimiteTracer");

    }
}
