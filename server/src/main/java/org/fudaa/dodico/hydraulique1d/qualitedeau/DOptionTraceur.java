package org.fudaa.dodico.hydraulique1d.qualitedeau;

import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IOptionTraceur;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IOptionTraceurHelper;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IOptionTraceurOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/*
 * @file         DOptionTraceur.java
 * @creation     2006-02-28
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class DOptionTraceur extends DHydraulique1d implements IOptionTraceur,IOptionTraceurOperations {
    public DOptionTraceur() {
        super();
        diffusionDuTraceur_= true;
        convectionDuTraceur_=true;
    }




    /**
     * convectionDuTraceur
     *
     * @param newConvectionDuTraceur boolean
     */
  @Override
    public void convectionDuTraceur(boolean newConvectionDuTraceur) {
        if (convectionDuTraceur_==newConvectionDuTraceur) return;
        convectionDuTraceur_ = newConvectionDuTraceur;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "convectionDuTraceur");

    }

    /**
     * convectionDuTraceur
     *
     * @return boolean
     */
    private boolean convectionDuTraceur_;
  @Override
    public boolean convectionDuTraceur() {
        return convectionDuTraceur_;
    }

    /**
     * creeClone
     *
     * @return IObjet
     */
  @Override
    public IObjet creeClone() {
        IOptionTraceur p=UsineLib.findUsine().creeHydraulique1dOptionTraceur();
        p.initialise(tie());
        return p;

    }

    /**
     * diffusionDuTraceur
     *
     * @return boolean
     */
    private boolean diffusionDuTraceur_;
  @Override
    public boolean diffusionDuTraceur() {
        return diffusionDuTraceur_;
    }

    /**
     * diffusionDuTraceur
     *
     * @param newDiffusionDuTraceur boolean
     */
  @Override
    public void diffusionDuTraceur(boolean newDiffusionDuTraceur) {
        if (diffusionDuTraceur_==newDiffusionDuTraceur) return;
        diffusionDuTraceur_ = newDiffusionDuTraceur;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "diffusionDuTraceur");

    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
        diffusionDuTraceur_= true;
        convectionDuTraceur_=true;
    }




    /**
     * initialise
     *
     * @param o IObjet
     */
  @Override
    public void initialise(IObjet _o) {
        if (_o instanceof IOptionTraceur) {
            IOptionTraceur p =
                    IOptionTraceurHelper.narrow(_o);
            diffusionDuTraceur(p.diffusionDuTraceur());
            convectionDuTraceur(p.convectionDuTraceur());

        }
    }


}
