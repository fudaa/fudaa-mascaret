package org.fudaa.dodico.hydraulique1d.qualitedeau;

import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IOptionTraceur;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresConvecDiffu;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresConvecDiffuHelper;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresConvecDiffuOperations;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.LOptionConvec;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.LOptionDiffus;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/*
 * @file         DParametresConvecDiffu.java
 * @creation     2006-02-28
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class DParametresConvecDiffu extends DHydraulique1d implements IParametresConvecDiffuOperations, IParametresConvecDiffu {
    public DParametresConvecDiffu() {
         super();
         optionDesTracers_=new IOptionTraceur[0];
         optionConvection_=LOptionConvec.HYP1FA_CONS;
         ordreSchemaConvec_=1;
         paramW_=0;
         LimitPente_=false;
         optionCalculDiffusion_=LOptionDiffus.K_C1U_C2;
         coeffDiffusion1_=0;
         coeffDiffusion2_=0;

    }

    /**
     * LimitPente
     *
     * @return boolean
     */
    private boolean LimitPente_;
  @Override
    public boolean LimitPente() {
        return LimitPente_;
    }

    /**
     * LimitPente
     *
     * @param newLimitPente boolean
     */
  @Override
    public void LimitPente(boolean newLimitPente) {
        if (LimitPente_==newLimitPente) return;
        LimitPente_ = newLimitPente;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "LimitPente");

    }



    /**
     * coeffDiffusion1
     *
     * @return double
     */
    private double coeffDiffusion1_;
  @Override
    public double coeffDiffusion1() {
        return coeffDiffusion1_;
    }

    /**
     * coeffDiffusion1
     *
     * @param newCoeffDiffusion1 double
     */
  @Override
    public void coeffDiffusion1(double newCoeffDiffusion1) {
        if (coeffDiffusion1_==newCoeffDiffusion1) return;
        coeffDiffusion1_ = newCoeffDiffusion1;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "coeffDiffusion1");

    }

    /**
     * coeffDiffusion2
     *
     * @return double
     */
    private double coeffDiffusion2_;
  @Override
    public double coeffDiffusion2() {
        return coeffDiffusion2_;
    }

    /**
     * coeffDiffusion2
     *
     * @param newCoeffDiffusion2 double
     */
  @Override
    public void coeffDiffusion2(double newCoeffDiffusion2) {
        if (coeffDiffusion2_==newCoeffDiffusion2) return;
        coeffDiffusion2_ = newCoeffDiffusion2;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "coeffDiffusion2");

    }

    /**
     * creeClone
     *
     * @return IObjet
     */
  @Override
    public IObjet creeClone() {
        IParametresConvecDiffu p=
            UsineLib.findUsine().creeHydraulique1dParametresConvecDiffu();
        p.initialise(tie());
        return p;

    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
        optionDesTracers_=null;
        optionConvection_ = LOptionConvec.HYP1FA_CONS;
        ordreSchemaConvec_ = 0;
        paramW_ = 0;
        LimitPente_ = false;
        optionCalculDiffusion_ = LOptionDiffus.K_C1U_C2;
        coeffDiffusion1_ = 0;
        coeffDiffusion2_ = 0;

    }



    /**
     * initialise
     *
     * @param o IObjet

     */
  @Override
    public void initialise(IObjet _o) {
        if (_o instanceof IParametresConvecDiffu) {
            IParametresConvecDiffu q = IParametresConvecDiffuHelper.narrow(
                    _o);
            optionDesTracers((IOptionTraceur[]) q.optionDesTracers().clone());
            optionConvection(q.optionConvection());
            ordreSchemaConvec(q.ordreSchemaConvec());
            paramW(q.paramW());
            LimitPente(q.LimitPente());
            optionCalculDiffusion(q.optionCalculDiffusion());
            coeffDiffusion1(q.coeffDiffusion1());
            coeffDiffusion2(q.coeffDiffusion2());
}

    }




    /**
     * optionCalculDiffusion
     *
     * @return LOptionDiffus
     */
    private LOptionDiffus optionCalculDiffusion_;
  @Override
    public LOptionDiffus optionCalculDiffusion() {
        return optionCalculDiffusion_;
    }

    /**
     * optionCalculDiffusion
     *
     * @param newOptionCalculDiffusion LOptionDiffus
     */
  @Override
    public void optionCalculDiffusion(LOptionDiffus newOptionCalculDiffusion) {
        if (optionCalculDiffusion_==newOptionCalculDiffusion) return;
        optionCalculDiffusion_ = newOptionCalculDiffusion;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "optionCalculDiffusion");

    }

    /**
     * optionConvection
     *
     * @return LOptionConvec
     */
    private LOptionConvec optionConvection_;
  @Override
    public LOptionConvec optionConvection() {
        return optionConvection_;
    }

    /**
     * optionConvection
     *
     * @param newOptionConvection LOptionConvec
     */
  @Override
    public void optionConvection(LOptionConvec newOptionConvection) {
        if (optionConvection_==newOptionConvection) return;
        optionConvection_ = newOptionConvection;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "optionConvection");

    }

    /**
     * optionDesTracers
     *
     * @param newOptionDesTracers IOptionTraceur[]
     */
  @Override
    public void optionDesTracers(IOptionTraceur[] newOptionDesTracers) {
        if (optionDesTracers_==newOptionDesTracers) return;
        optionDesTracers_ = newOptionDesTracers;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "optionDesTracers");

    }

    /**
     * optionDesTracers
     *
     * @return IOptionTraceur[]
     */
    private IOptionTraceur[] optionDesTracers_;
  @Override
    public IOptionTraceur[] optionDesTracers(){
        return optionDesTracers_;
    }

    /**
     * ordreSchemaConvec
     *
     * @param newOrdreSchemaConvec int
     */
  @Override
    public void ordreSchemaConvec(int newOrdreSchemaConvec) {
        if (ordreSchemaConvec_==newOrdreSchemaConvec) return;
        ordreSchemaConvec_ = newOrdreSchemaConvec;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "ordreSchemaConvec");

    }

    /**
     * ordreSchemaConvec
     *
     * @return int
     */
    private int ordreSchemaConvec_;
  @Override
    public int ordreSchemaConvec() {
        return ordreSchemaConvec_;
    }

    /**
     * paramW
     *
     * @param newParamW double
     */
  @Override
    public void paramW(double newParamW) {
        if (paramW_==newParamW) return;
        paramW_ = newParamW;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "paramW");

    }

    /**
     * paramW
     *
     * @return double
     */
    private double paramW_;
  @Override
    public double paramW() {
        return paramW_;
    }


}
