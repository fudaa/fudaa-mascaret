package org.fudaa.dodico.hydraulique1d.qualitedeau;

import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParamPhysTracer;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresConvecDiffu;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresGenerauxQualiteDEau;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresGenerauxQualiteDEauHelper;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresGenerauxQualiteDEauOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/*
 * @file         DParametresQualiteDEau.java
 * @creation     2006-02-28
 * @modification $Date: 2006-09-12 08:35:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class DParametresGenerauxQualiteDEau extends DHydraulique1d implements IParametresGenerauxQualiteDEau, IParametresGenerauxQualiteDEauOperations {
    public DParametresGenerauxQualiteDEau() {
            super();
            paramsPhysTracer_=new IParamPhysTracer[0];
            parametresConvecDiffu_ = UsineLib.findUsine().creeHydraulique1dParametresConvecDiffu();
            paramMeteoTracer_=null;
        }

    /**
     * creeClone
     *
     * @return IObjet
     */
  @Override
    public IObjet creeClone() {
        IParametresGenerauxQualiteDEau p=UsineLib.findUsine().creeHydraulique1dParametresGenerauxQualiteDEau();
        p.initialise(tie());
        return p;
    }

  @Override
    final public String toString() {
      String s= "ParametresGenerauxQualiteDEau";
      return s;
  }

    /**
     * dispose
     */
  @Override
    public void dispose() {
        paramsPhysTracer_ = null;
        parametresConvecDiffu_ = null;
        paramMeteoTracer_ = null;
    }


    /**
     * initialise
     *
     * @param o IObjet
     */
  @Override
    public void initialise(IObjet _o) {
        if (_o instanceof IParametresGenerauxQualiteDEau) {
            IParametresGenerauxQualiteDEau q =
                    IParametresGenerauxQualiteDEauHelper.narrow(_o);
            paramMeteoTracer(q.paramMeteoTracer());
            paramsPhysTracer((IParamPhysTracer[])q.paramsPhysTracer().clone());
            if (q.parametresConvecDiffu() != null)
        parametresConvecDiffu((IParametresConvecDiffu) q.parametresConvecDiffu().
                         creeClone());
        }
    }


    //Attributs




    /**
     * parametresConvecDiffu
     *
     * @return IParametresConvecDiffu
     */
    private IParametresConvecDiffu parametresConvecDiffu_;
  @Override
    public IParametresConvecDiffu parametresConvecDiffu() {
        return parametresConvecDiffu_;
    }

    /**
     * parametresConvecDiffu
     *
     * @param newParametresConvecDiffu IParametresConvecDiffu
     */
  @Override
    public void parametresConvecDiffu(IParametresConvecDiffu
                                      newParametresConvecDiffu) {
        if (parametresConvecDiffu_==newParametresConvecDiffu) return;
        parametresConvecDiffu_ = newParametresConvecDiffu;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "parametresConvecDiffu");

    }



    /**
     * paramsPhysTracer
     *
     * @return IParamPhysTracer[]
     */
    private IParamPhysTracer[] paramsPhysTracer_;
  @Override
    public IParamPhysTracer[] paramsPhysTracer() {
        return paramsPhysTracer_;
    }


    /**
     * paramsPhysTracer
     *
     * @param newParamsPhysTracer IParamPhysTracer[]

     */
  @Override
    public void paramsPhysTracer(IParamPhysTracer[]
                                        newParamsPhysTracer) {
        if (paramsPhysTracer_==newParamsPhysTracer) return;
        paramsPhysTracer_ = newParamsPhysTracer;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "paramsPhysTrace");

    }

    private byte[] paramMeteoTracer_;
  @Override
    public byte[] paramMeteoTracer() {
      return paramMeteoTracer_;
    }
  @Override
    public void paramMeteoTracer(byte[] s) {
      paramMeteoTracer_= s;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(), "paramMeteoTracer");
    }


}
