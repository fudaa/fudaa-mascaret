package org.fudaa.dodico.hydraulique1d.qualitedeau;

import java.util.Arrays;

import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresModeleQualiteEau;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresModeleQualiteEauHelper;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresModeleQualiteEauOperations;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.LModeleQualiteDEau;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

        /*
         * @file         DParametresModeleQualiteEau.java
         * @creation     2006-02-28
         * @modification $Date: 2006-09-28 13:21:06 $
         * @license      GNU General Public License 2
         * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
         * @mail         devel@fudaa.org
 */
public class DParametresModeleQualiteEau extends DHydraulique1d implements
        IParametresModeleQualiteEauOperations, IParametresModeleQualiteEau {
    public DParametresModeleQualiteEau() {
            super();
            presenceTraceurs_=false;	// presence de traceurs
            nbTraceur_=1;
            modeleQualiteEau_ = null;//LModeleQualiteDEau.TRANSPORT_PUR;
            vvNomTracer_ = new String[1][2];
            vvNomTracer_[0][0]="TRA1";
            vvNomTracer_[0][1]="Traceur 1";
            frequenceCouplHydroTracer_ = 1;
    }


    /**
     * creeClone
     *
     * @return IObjet
     */
  @Override
    public IObjet creeClone() {
       IParametresModeleQualiteEau p=
       UsineLib.findUsine().creeHydraulique1dParametresModeleQualiteEau();
       p.initialise(tie());
        return p;

    }

    /**
     * dispose
     *
     */
  @Override
    public void dispose() {
        presenceTraceurs_=false;
        nbTraceur_=0;
        modeleQualiteEau_ = LModeleQualiteDEau.TRANSPORT_PUR;
        vvNomTracer_ = null;
        frequenceCouplHydroTracer_ = 0;
    }


    /**
     * initialise
     *
     * @param o IObjet
     */
  @Override
    public void initialise(IObjet _o) {
        if (_o instanceof IParametresModeleQualiteEau) {
                   IParametresModeleQualiteEau p =
                           IParametresModeleQualiteEauHelper.narrow(_o);
      presenceTraceurs(p.presenceTraceurs());
      nbTraceur(p.nbTraceur());
      modeleQualiteEau(p.modeleQualiteEau());
      vvNomTracer((String[][])p.vvNomTracer().clone());
      frequenceCouplHydroTracer(p.frequenceCouplHydroTracer());
        }
    }


    //Attribut



    /**
     * presenceTraceurs
     *
     * @return presenceTraceurs
     */
    private boolean presenceTraceurs_;
  @Override
    public boolean presenceTraceurs() {
        return presenceTraceurs_;
    }

    /**
     * presenceTraceurs
     *
     * @param presenceTraceurs boolean
     */
  @Override
    public void presenceTraceurs(boolean
                                 presenceTraceurs) {
        if (presenceTraceurs_ == presenceTraceurs)
            return;
        presenceTraceurs_ = presenceTraceurs;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "presenceTraceurs");

    }


    /**
     * nbTraceur
     *
     * @return nbTraceur
     */
    private int nbTraceur_;
  @Override
    public int nbTraceur() {
        return nbTraceur_;
    }


    /**
     * nbTraceur
     *
     * @param nbTraceur int
     */
  @Override
    public void nbTraceur(int
                          nbTraceur) {
        if (nbTraceur_ == nbTraceur)
            return;
        nbTraceur_ = nbTraceur;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "nbTraceur");

    }





    /**
     * frequenceCouplHydroTracer
     *
     * @return int
     */
    private int frequenceCouplHydroTracer_;
  @Override
    public int frequenceCouplHydroTracer() {
        return frequenceCouplHydroTracer_;
    }

    /**
     * frequenceCouplHydroTracer
     *
     * @param newFrequenceCouplHydroTracer int
     */
  @Override
    public void frequenceCouplHydroTracer(int newFrequenceCouplHydroTracer) {
        if (frequenceCouplHydroTracer_==newFrequenceCouplHydroTracer) return;
       frequenceCouplHydroTracer_ = newFrequenceCouplHydroTracer;
       UsineLib.findUsine().fireObjetModifie(toString(), tie(), "frequenceCouplHydroTracer");

    }






    /**
     * modeleQualiteEau
     *
     * @return LModeleQualiteDEau
     */
    private LModeleQualiteDEau modeleQualiteEau_;
  @Override
    public LModeleQualiteDEau modeleQualiteEau() {
        return modeleQualiteEau_;
    }

    /**
     * modeleQualiteEau
     *
     * @param newModeleQualiteEau LModeleQualiteDEau
     */
  @Override
    public void modeleQualiteEau(LModeleQualiteDEau newModeleQualiteEau) {
    	if (modeleQualiteEau_!=null && newModeleQualiteEau!=null) {
	        if (modeleQualiteEau_.value() == newModeleQualiteEau.value())  return;
    	}
	        modeleQualiteEau_ = newModeleQualiteEau;
	        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "modeleQualiteEau");
    }

    /**
     *vvNomTracer
     *
     * @return String[][]
     */
    private String[][] vvNomTracer_;
  @Override
    public String[][] vvNomTracer() {
        return vvNomTracer_;
    }

    /**
     * vvNomTracer
     *
     * @param newVNomTracer String[][]
     */

  @Override
    public void vvNomTracer(String[][] newVVNomTracer) {
        if (Arrays.equals(newVVNomTracer,vvNomTracer_)) return;
        vvNomTracer_ = newVVNomTracer;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "vvNomTracer");


    }


}
