package org.fudaa.dodico.hydraulique1d.qualitedeau;

import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IConcentrationInitiale;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IOptionTraceur;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParamPhysTracer;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresGenerauxQualiteDEau;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresModeleQualiteEau;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresQualiteDEau;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresQualiteDEauHelper;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.IParametresQualiteDEauOperations;
import org.fudaa.dodico.corba.hydraulique1d.qualitedeau.LModeleQualiteDEau;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DHydraulique1d;
import org.fudaa.dodico.objet.UsineLib;

/*
 * @file         DParametresQualiteDEau.java
 * @creation     2006-02-28
 * @modification $Date: 2006-11-29 15:11:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
public class DParametresQualiteDEau extends DHydraulique1d implements IParametresQualiteDEau, IParametresQualiteDEauOperations {
    public DParametresQualiteDEau() {
            super();
            parametresGenerauxQualiteDEau_= UsineLib.findUsine().creeHydraulique1dParametresGenerauxQualiteDEau();
            parametresModeleQualiteEau_ = UsineLib.findUsine().creeHydraulique1dParametresModeleQualiteEau();
            concentrationsInitiales_ = new IConcentrationInitiale[0];
        }

    /**
     * creeClone
     *
     * @return IObjet
     */
  @Override
    public IObjet creeClone() {
        IParametresQualiteDEau p=UsineLib.findUsine().creeHydraulique1dParametresQualiteDEau();
        p.initialise(tie());
        return p;
    }

  @Override
    final public String toString() {
      String s= "ParametresQualiteDEau";
      return s;
  }

    /**
     * dispose
     */
  @Override
    public void dispose() {
        parametresGenerauxQualiteDEau_ = null;
        parametresModeleQualiteEau_ = null;
        concentrationsInitiales_=null;

    }


    /**
     * initialise
     *
     * @param o IObjet
     */
  @Override
    public void initialise(IObjet _o) {
        if (_o instanceof IParametresQualiteDEau) {
            IParametresQualiteDEau q =
                    IParametresQualiteDEauHelper.narrow(_o);
            if (q.parametresGenerauxQualiteDEau() != null)
                parametresGenerauxQualiteDEau((IParametresGenerauxQualiteDEau) q.
                                        parametresGenerauxQualiteDEau().
                                        creeClone());
            if (q.parametresModeleQualiteEau() != null)
        parametresModeleQualiteEau((IParametresModeleQualiteEau) q.parametresModeleQualiteEau().
                         creeClone());
             if (q.concentrationsInitiales() != null) {
                 IConcentrationInitiale[] ci = new IConcentrationInitiale[q.
                                               concentrationsInitiales().length];
                 for (int i = 0; i < ci.length; i++)
                     ci[i] = (IConcentrationInitiale) q.concentrationsInitiales()[
                             i].creeClone();
                 concentrationsInitiales(ci);
             }

        }
    }


    //Attributs

    /**
     * parametresGenerauxQualiteDEau
     *
     * @return IParametresGenerauxQualiteDEau
     */
    private IParametresGenerauxQualiteDEau parametresGenerauxQualiteDEau_;
  @Override
    public IParametresGenerauxQualiteDEau parametresGenerauxQualiteDEau() {
        return parametresGenerauxQualiteDEau_;
    }

    /**
     * IParametresGenerauxQualiteDEau
     *
     * @param parametresGenerauxQualiteDEau IParametresGenerauxQualiteDEau
     */
  @Override
    public void parametresGenerauxQualiteDEau(IParametresGenerauxQualiteDEau
                                      parametresGenerauxQualiteDEau) {
        if (parametresGenerauxQualiteDEau_==parametresGenerauxQualiteDEau) return;
        parametresGenerauxQualiteDEau_ = parametresGenerauxQualiteDEau;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "parametresGenerauxQualiteDEau");

    }



    /**
     * parametresModeleQualiteEau
     *
     * @return IParametresModeleQualiteEau
     */
    private IParametresModeleQualiteEau parametresModeleQualiteEau_;
  @Override
    public IParametresModeleQualiteEau parametresModeleQualiteEau() {
        return parametresModeleQualiteEau_;
    }


    /**
     * parametresModeleQualiteEau
     *
     * @param newParametresModeleQualiteEau IPparametresModeleQualiteEau

     */
  @Override
    public void parametresModeleQualiteEau(IParametresModeleQualiteEau
                                        newParametresModeleQualiteEau) {
        if (parametresModeleQualiteEau_==newParametresModeleQualiteEau) return;
        parametresModeleQualiteEau_ = newParametresModeleQualiteEau;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(),
                                              "parametresModeleQualiteEau");

    }

    private IConcentrationInitiale[] concentrationsInitiales_;
  @Override
     public IConcentrationInitiale[] concentrationsInitiales() {
       return concentrationsInitiales_;
     }
  @Override
     public void concentrationsInitiales(IConcentrationInitiale[] concentrationsInitiales) {
       if (egale(concentrationsInitiales_, concentrationsInitiales)) return;
       concentrationsInitiales_= concentrationsInitiales;
       UsineLib.findUsine().fireObjetModifie(toString(), tie(), "concentrationsInitiales");
     }
     private final static boolean egale(IConcentrationInitiale[] p, IConcentrationInitiale[] p2) {
       if (p == p2)
         return true;
       if (p == null || p2 == null)
         return false;

       int length = p.length;
       if (p2.length != length)
         return false;

       for (int i = 0; i < length; i++)
         if (p[i] != p2[i])
           return false;

       return true;
     }

     public IConcentrationInitiale creeConcentrationsInitiales() {
        IConcentrationInitiale conc= UsineLib.findUsine().creeHydraulique1dConcentrationInitiale();
        IConcentrationInitiale concs[]= new IConcentrationInitiale[concentrationsInitiales_.length + 1];
        for (int i= 0; i < concentrationsInitiales_.length; i++)
          concs[i]= concentrationsInitiales_[i];
        concs[concs.length - 1]= conc;
        concentrationsInitiales(concs);
        return conc;
      }
      private void supprimeConcentrationInitiale(IConcentrationInitiale conc) {
        Vector newConcs= new Vector(concentrationsInitiales_.length - 1);
        for (int i= 0; i < concentrationsInitiales_.length; i++) {
          if (concentrationsInitiales_[i] == conc) {
            UsineLib.findUsine().supprimeHydraulique1dConcentrationInitiale(concentrationsInitiales_[i]);
          } else
            newConcs.add(concentrationsInitiales_[i]);
        }
        IConcentrationInitiale[] concs= new IConcentrationInitiale[newConcs.size()];
        for (int i= 0; i < concs.length; i++) {
          concs[i]= (IConcentrationInitiale)newConcs.get(i);
        }
        concentrationsInitiales(concs);
      }
  @Override
      public void supprimeConcentrationsInitiales(IConcentrationInitiale[] concs) {
        //System.out.println("supprimeConcentrationsInitiales(IConcentrationInitiale[] concs) concs.length="+concs.length);
        for (int i= 0; i < concs.length; i++) {
          supprimeConcentrationInitiale(concs[i]);
        }
      }

      private IParamPhysTracer[] construireParamsPhysParDefaut(String[] noms,double[] valeur){
          IParamPhysTracer[] params = new IParamPhysTracer[noms.length];
          IParamPhysTracer param;
          for (int i = 0; i < noms.length; i++) {
              param = UsineLib.findUsine().creeHydraulique1dParamPhysTracer();
              param.nomParamPhys(noms[i]);
              param.valeurParamPhys(valeur[i]);
              params[i]=param;
          }
          return params;
      }
      
      private void  construireParamPhys(LModeleQualiteDEau modele) {
          //Remplissage du tableau selon le mod�le de qualit� d'eau
          switch (parametresModeleQualiteEau_.modeleQualiteEau().value()) {
          case LModeleQualiteDEau._O2 : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(DParamPhysTracer.ParamsO2Nom,DParamPhysTracer.ParamsO2Valeur));break;
          case LModeleQualiteDEau._BIOMASS : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(DParamPhysTracer.ParamsBiomassNom,DParamPhysTracer.ParamsBiomassValeur));break;
          case LModeleQualiteDEau._EUTRO : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(DParamPhysTracer.ParamsEutroNom,DParamPhysTracer.ParamsEutroValeur));break;
          case LModeleQualiteDEau._MICROPOL : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(DParamPhysTracer.ParamsMicropolNom,DParamPhysTracer.ParamsMicropolValeur));break;
          case LModeleQualiteDEau._THERMIC : parametresGenerauxQualiteDEau_.paramsPhysTracer(construireParamsPhysParDefaut(DParamPhysTracer.ParamsThermicNom,DParamPhysTracer.ParamsThermicValeur));break;
          default  : parametresGenerauxQualiteDEau_.paramsPhysTracer(null);
          }
      };
      
  @Override
      public void initialisationSelonModeleQE(){
    	    int nbTraceurs = parametresModeleQualiteEau_.nbTraceur();
    	    
    	    //Mise � jour des param�tres g�n�raux
    	    if (parametresGenerauxQualiteDEau_.parametresConvecDiffu().optionDesTracers().length!=nbTraceurs){
    	        IOptionTraceur[] newTabOptions = new IOptionTraceur[nbTraceurs];
    	        for (int i = 0; i < newTabOptions.length; i++) {
    	            newTabOptions[i]= UsineLib.findUsine().creeHydraulique1dOptionTraceur();
    	        }
    	        if (parametresModeleQualiteEau_.modeleQualiteEau().value()==LModeleQualiteDEau._MICROPOL){
    	            newTabOptions[1].convectionDuTraceur(false);
    	            newTabOptions[1].diffusionDuTraceur(false);
    	            newTabOptions[4].convectionDuTraceur(false);
    	            newTabOptions[4].diffusionDuTraceur(false);
    	        }
    	        parametresGenerauxQualiteDEau_.parametresConvecDiffu().optionDesTracers(newTabOptions);
    	    }
    	    
    	    //Mise � jour des param�tres physique
    	    construireParamPhys(parametresModeleQualiteEau_.modeleQualiteEau());
    	    
    	    //Mise � jour des param�tres m�t�o
    	    parametresGenerauxQualiteDEau_.paramMeteoTracer(null);
    	    
    	    //Mise � jour des concentrations initiales
    	    this.concentrationsInitiales_ = new IConcentrationInitiale[0];
     	    //Idealement il faudrait �galement supprimer toutes les lois Tracer
      }
      
      

}
