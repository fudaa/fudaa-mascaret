/**
 * @file         DApport.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IApport;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IApportOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DSingularite;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "apport" de d�bit.
 * Associe une longueur, un coefficient de d�bit et une r�f�rence vers une loi
 * de type hydrogramme.
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DApport extends DSingularite implements IApport,IApportOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IApport) {
      IApport a= (IApport)_o;
      coefficient(a.coefficient());
      longueur(a.longueur());
      if (a.loi() != null)
        loi((ILoiHydrogramme)a.loi());
    }
  }
  @Override
  final public IObjet creeClone() {
    IApport a= UsineLib.findUsine().creeHydraulique1dApport();
    a.initialise(tie());
    return a;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "apport " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "D�bit apport-Singularit� n�"+numero_;
    res[1]= "Abscisse : " + abscisse_ + " Longueur : " + longueur_;
    if (loi_ != null)
      res[1]= res[1] + " Loi : " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  /*** IApport ***/
  // constructeurs
  public DApport() {
    super();
    nom_= "D�bit apport-Singularit� n�"+numero_;
    coefficient_= 1.;
    longueur_= 0.;
    loi_= null;
  }
  @Override
  public void dispose() {
    nom_= null;
    coefficient_= 0.;
    longueur_= 0.;
    loi_= null;
    super.dispose();
  }
  // attributs
  private double coefficient_;
  @Override
  public double coefficient() {
    return coefficient_;
  }
  @Override
  public void coefficient(double coefficient) {
    if (coefficient_== coefficient) return;
    coefficient_= coefficient;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefficient");
  }
  private double longueur_;
  @Override
  public double longueur() {
    return longueur_;
  }
  @Override
  public void longueur(double longueur) {
    if (longueur_== longueur) return;
    longueur_= longueur;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "longueur");
  }
  private ILoiHydraulique loi_;
  @Override
  public ILoiHydraulique loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiHydraulique loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // Methode
  @Override
  public ILoiTarage creeLoiTarage() {
    ILoiTarage tar= UsineLib.findUsine().creeHydraulique1dLoiTarage();
    loi(tar);
    return tar;
  }
  @Override
  public ILoiHydrogramme creeLoiHydrogramme() {
    ILoiHydrogramme tar= UsineLib.findUsine().creeHydraulique1dLoiHydrogramme();
    loi(tar);
    return tar;
  }
  @Override
  public ILoiLimnigramme creeLoiLimnigramme() {
    ILoiLimnigramme tar= UsineLib.findUsine().creeHydraulique1dLoiLimnigramme();
    loi(tar);
    return tar;
  }
  @Override
  public ILoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
}
