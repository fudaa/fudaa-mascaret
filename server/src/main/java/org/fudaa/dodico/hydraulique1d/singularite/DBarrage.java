/**
 * @file         DBarrage.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IBarrage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IBarrageOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DBarrage extends DSeuil implements IBarrage,IBarrageOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IBarrage) {
      IBarrage b= (IBarrage)_o;
      coteCrete(b.coteCrete());
    }
  }
  @Override
  final public IObjet creeClone() {
    IBarrage b= UsineLib.findUsine().creeHydraulique1dBarrage();
    b.initialise(tie());
    return b;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "barrage " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  /*** IBarrage ***/
  // constructeurs
  public DBarrage() {
    super();
    coteCrete_= 0.;
  }
  @Override
  public void dispose() {
    coteCrete_= 0.;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  @Override
  public double coteCrete() {
    return coteCrete_;
  }
  @Override
  public void coteCrete(double coteCrete) {
    if (coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteCrete");
  }
}
