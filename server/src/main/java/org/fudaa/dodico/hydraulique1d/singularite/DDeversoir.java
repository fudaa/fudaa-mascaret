/**
 * @file         DDeversoir.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoir;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirComportementLoi;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirComportementZCoefQ;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DSingularite;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Abstraction de l'objet m�tier singularit� de type "d�versoir" de lat�ral de d�bit.
 * Ajoute une longueur.
 * @version      $Revision: 1.10 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DDeversoir extends DSingularite implements IDeversoir,IDeversoirOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IDeversoir) {
      IDeversoir s= (IDeversoir)_o;
      longueur(s.longueur());
    }
  }
  @Override
  public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "deversoir " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return null;
  }
  /*** IDeversoir ***/
  // constructeurs
  public DDeversoir() {
    super();
    nom_= "D�versoir lat�ral-Singularit� n�"+numero_;
    longueur_= 1.;
  }
  @Override
  public void dispose() {
    nom_= null;
    longueur_= 0;
    super.dispose();
  }
  // Attributs
  protected double longueur_;
  @Override
  public double longueur() {
    return longueur_;
  }
  @Override
  public void longueur(double longueur) {
    if (longueur_== longueur) return;
    longueur_= longueur;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "longueur");
  }
  // Methode
  public IDeversoirComportementLoi transformeDeversoirLoi() {
    IDeversoirComportementLoi deversoir;
    deversoir= UsineLib.findUsine().creeHydraulique1dDeversoirComportementLoi();
    deversoir.numero(numero_);
    deversoir.nom(nom_);
    deversoir.abscisse(abscisse_);
    deversoir.longueur(longueur_);
    return deversoir;
  }
  public IDeversoirComportementZCoefQ transformeDeversoirZQ() {
    IDeversoirComportementZCoefQ deversoir;
    deversoir=
      UsineLib.findUsine().creeHydraulique1dDeversoirComportementZCoefQ();
    deversoir.numero(numero_);
    deversoir.nom(nom_);
    deversoir.abscisse(abscisse_);
    deversoir.longueur(longueur_);
    return deversoir;
  }
}
