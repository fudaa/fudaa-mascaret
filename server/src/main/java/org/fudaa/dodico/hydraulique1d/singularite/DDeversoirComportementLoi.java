/**
 * @file         DDeversoirComportementLoi.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirComportementLoi;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirComportementLoiOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "d�versoir" de lat�ral
 * dont le "comportement" est d�fini par une "loi".
 * Ajoute une r�f�rence de type loi de  tarage.
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DDeversoirComportementLoi
  extends DDeversoir
  implements IDeversoirComportementLoi,IDeversoirComportementLoiOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IDeversoirComportementLoi) {
      IDeversoirComportementLoi s= (IDeversoirComportementLoi)_o;
      if (s.loi() != null){
          if (s.loi() instanceof ILoiTarage)
              loi((ILoiTarage) s.loi().creeClone());
      }else{
          loi(null);
      }
    }
  }
  @Override
  final public IObjet creeClone() {
    IDeversoirComportementLoi s=
      UsineLib.findUsine().creeHydraulique1dDeversoirComportementLoi();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "deversoirComportementLoi " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "D�versoir-Singularit� n�"+numero_;
    res[1]= "Abscisse : " + abscisse_;
    if (loi_ != null)
      res[1]= res[1] + " loi de tarage : " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  /*** IDeversoirComportementLoi ***/
  // constructeurs
  public DDeversoirComportementLoi() {
    super();
    nom_= "D�versoir lat�ral-Singularit� n�"+numero_;
    longueur_= 1.;
    loi_= null;
  }
  @Override
  public void dispose() {
    nom_= null;
    longueur_= 0;
    loi_= null;
    super.dispose();
  }
  // attributs
  private ILoiHydraulique loi_;
  @Override
 public ILoiHydraulique loi() {
   return loi_;
 }
  @Override
 public void loi(ILoiHydraulique loi) {
   if (loi_==loi) return;
   loi_= loi;
   UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
 }

  /*private ILoiTarage loi_;
  public ILoiTarage loi() {
    return loi_;
  }
  public void loi(ILoiTarage loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }*/
  // Methode

  @Override
  public ILoiHydraulique creeLoi() {
  ILoiTarage loi= UsineLib.findUsine().creeHydraulique1dLoiTarage();
  loi(loi);
  return loi;
}

  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
}
