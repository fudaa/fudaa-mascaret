/**
 * @file         DDeversoirComportementZCoefQ.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirComportementZCoefQ;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IDeversoirComportementZCoefQOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "d�versoir" de lat�ral
 * dont le "comportement" est d�fini par une coefficient de d�bit.
 * Ajoute un coefficient de d�bit et la cote de la cr�te.
 * @version      $Revision: 1.11 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DDeversoirComportementZCoefQ
  extends DDeversoir
  implements IDeversoirComportementZCoefQ,IDeversoirComportementZCoefQOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IDeversoirComportementZCoefQ) {
      IDeversoirComportementZCoefQ d= (IDeversoirComportementZCoefQ)_o;
      coteCrete(d.coteCrete());
      coefQ(d.coefQ());
    }
  }
  @Override
  final public IObjet creeClone() {
    IDeversoirComportementZCoefQ d=
      UsineLib.findUsine().creeHydraulique1dDeversoirComportementZCoefQ();
    d.initialise(tie());
    return d;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "deversoirComportementZCoefQ " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "D�versoir-Singularit� n�"+numero_;
    res[1]=
      "Abscisse : "
        + abscisse_
        + " cote cr�te : "
        + coteCrete_
        + " coef. Q : "
        + coefQ_;
    return res;
  }
  /*** IDeversoirComportementZCoefQ ***/
  // constructeurs
  public DDeversoirComportementZCoefQ() {
    super();
    nom_= "D�versoir lat�ral-Singularit� n�"+numero_;
    longueur_= 1.;
    coefQ_= 0;
    coteCrete_= 0;
  }
  @Override
  public void dispose() {
    nom_= null;
    longueur_= 0;
    coefQ_= 0;
    coteCrete_= 0;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  @Override
  public double coteCrete() {
    return coteCrete_;
  }
  @Override
  public void coteCrete(double coteCrete) {
    if (coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteCrete");
  }
  private double coefQ_;
  @Override
  public double coefQ() {
    return coefQ_;
  }
  @Override
  public void coefQ(double coefQ) {
    if (coefQ_== coefQ) return;
    coefQ_= coefQ;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefQ");
  }
}
