/**
 * @file         DPerteCharge.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IPerteCharge;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IPerteChargeOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DSingularite;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "perte de charge" singuli�re.
 * Ajoute un coefficient de perte de charge.
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DPerteCharge
  extends DSingularite
  implements IPerteCharge,IPerteChargeOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof IPerteCharge) {
      IPerteCharge a= (IPerteCharge)_o;
      coefficient(a.coefficient());
    }
  }
  @Override
  final public IObjet creeClone() {
    IPerteCharge p= UsineLib.findUsine().creeHydraulique1dPerteCharge();
    p.initialise(tie());
    return p;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "perteCharge " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Perte de charge-Singularit� n�"+numero_;
    res[1]= "Abscisse : " + abscisse_ + " Coefficient : " + coefficient_;
    return res;
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return null;
  }
  /*** IPerteCharge ***/
  // constructeurs
  public DPerteCharge() {
    super();
    nom_= "Perte de charge-Singularit� n�"+numero_;
    coefficient_= 1.;
  }
  @Override
  public void dispose() {
    nom_= null;
    coefficient_= 0;
    super.dispose();
  }
  // attributs

  private double coefficient_;
  @Override
  public double coefficient() {
    return coefficient_;
  }
  @Override
  public void coefficient(double coefficient) {
    if (coefficient_==coefficient) return;
    coefficient_= coefficient;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefficient");
  }
}
