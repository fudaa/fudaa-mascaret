/**
 * @file         DSeuil.java
 * @creation     2000-11-17
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuil;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DSingularite;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Abstraction de l'objet m�tier singularit� de type "seuil".
 * Ajoute une cote de rupture.
 * @version      $Revision: 1.11 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public abstract class DSeuil extends DSingularite implements ISeuil,ISeuilOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuil) {
      ISeuil s= (ISeuil)_o;
      coteRupture(s.coteRupture());
    }
  }
  @Override
  public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "seuil " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil-Singularit� n�"+numero_;
    res[1]= "Abscisse : " + abscisse_ + " Cote rupture : " + coteRupture_;
    if (getLoi() != null)
      res[1]= res[1] + " " + getLoi().typeLoi() + " : " + getLoi().nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    return null;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return null;
  }
  // constructeurs
  public DSeuil() {
    super();
    nom_="Barrage/Seuil-Singularit� n�"+numero_;
    coteRupture_= 10000.;
  }
  @Override
  public void dispose() {
    nom_= null;
    coteRupture_= 0.;
    super.dispose();
  }
  // Attributs
  protected double coteRupture_;
  @Override
  public double coteRupture() {
    return coteRupture_;
  }
  @Override
  public void coteRupture(double coteRupture) {
    if (coteRupture_==coteRupture) return;
    coteRupture_= coteRupture;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteRupture");
  }
}
