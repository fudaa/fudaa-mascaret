/**
 * @file         DSeuilDenoye.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilDenoye;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilDenoyeOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil d�noy�".
 * Ajoute une cote de cr�te, une cote moyenne de cr�te, une r�f�rence vers une loi de tarage.
 * @version      $Revision: 1.15 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilDenoye extends DSeuil implements ISeuilDenoye,ISeuilDenoyeOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilDenoye) {
        ISeuilDenoye s = (ISeuilDenoye) _o;
        coteMoyenneCrete(s.coteMoyenneCrete());
        coteCrete(s.coteCrete());
        if (s.loi() != null)
            loi((ILoiTarage) s.loi().creeClone());
        /*paramAvances(s.paramAvances());
        gradient(s.gradient());
        loiHydro(s.loiHydro());*/
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilDenoye s= UsineLib.findUsine().creeHydraulique1dSeuilDenoye();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "seuilDenoye " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil abaques (Q, Zam)-Singularit� n�"+numero_;
    res[1]= "Abscisse : " + abscisse_ + " Cote rupture : " + coteRupture_;
    if (loi_ != null)
      res[1]= res[1] + " Loi de tarage : " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
   if (paramAvances_) {
      res[1] += " gradient Q : " + gradient_;
      if (loiHydro_ != null)
          res[1] += " Loi Hydrogramme : " + loiHydro_.nom();
      else
          res[1] += " Loi Hydrogramme inconnue";
  }

    return res;
  }
  /*** ISeuilDenoye ***/
  // constructeurs
  public DSeuilDenoye() {
    super();
    nom_= "Seuil d�noy�-Singularit� n�"+numero_;
    coteMoyenneCrete_= 0.;
    coteCrete_= 0.;
    paramAvances_= true;
    gradient_= 5000.;
  }
  @Override
  public void dispose() {
    nom_= null;
    coteMoyenneCrete_= 0.;
    coteCrete_= 0.;
    paramAvances_= true;
    gradient_= 0;
    super.dispose();
  }
  // attributs
  private double coteMoyenneCrete_;
  @Override
  public double coteMoyenneCrete() {
    return coteMoyenneCrete_;
  }
  @Override
  public void coteMoyenneCrete(double coteMoyenneCrete) {
    if(coteMoyenneCrete_== coteMoyenneCrete) return;
    coteMoyenneCrete_= coteMoyenneCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteMoyenneCrete");
  }
  private double coteCrete_;
  @Override
  public double coteCrete() {
    return coteCrete_;
  }
  @Override
  public void coteCrete(double coteCrete) {
    if(coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteCrete");
  }
  private ILoiTarage loi_;
  @Override
  public ILoiTarage loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiTarage loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    ILoiTarage loi= UsineLib.findUsine().creeHydraulique1dLoiTarage();
    loi(loi);
    return loi;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
  private boolean paramAvances_;
public boolean paramAvances() {
  return paramAvances_;
}
public void paramAvances(boolean paramAvances) {
  if (paramAvances_==paramAvances) return;
  paramAvances_= paramAvances;
  UsineLib.findUsine().fireObjetModifie(toString(), tie(), "paramAvances");
}
private double gradient_;
  @Override
  public double gradient() {
  return gradient_;
}
  @Override
  public void gradient(double gradient) {
  if (gradient_==gradient) return;
  gradient_= gradient;
  UsineLib.findUsine().fireObjetModifie(toString(), tie(), "gradient");
}
private ILoiHydrogramme loiHydro_;
public ILoiHydrogramme loiHydro() {
  return loiHydro_;
}
public void loiHydro(ILoiHydrogramme loiHydro) {
  if (loiHydro_==loiHydro) return;
  loiHydro_= loiHydro;
  UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loiHydro");
}

}
