/**
 * @file         DSeuilGeometrique.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiGeometrique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilGeometrique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilGeometriqueOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil g�om�trique".
 * Ajoute une r�f�rence vers une loi g�om�trique.
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilGeometrique
  extends DSeuil
  implements ISeuilGeometrique,ISeuilGeometriqueOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilGeometrique) {
      ISeuilGeometrique s= (ISeuilGeometrique)_o;
      coteMoyenneCrete(s.coteMoyenneCrete());
      coefQ(s.coefQ());
      if (s.loi() != null)
        loi((ILoiGeometrique)s.loi().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilGeometrique s=
      UsineLib.findUsine().creeHydraulique1dSeuilGeometrique();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "seuilGeometrique " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil profil de cr�te-Singularit� n�"+numero_;
    res[1]= "Abscisse : "
            + abscisse_
            + " Cote rupture : "
            + coteRupture_
            + "cote Moyenne Crete"
            + coteMoyenneCrete_
            + " coef. Q : "
            + coefQ_;
    if (loi_ != null)
      res[1]= res[1] + " Loi geom�trique : " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  /*** ISeuilGeometrique ***/
  // constructeurs
  public DSeuilGeometrique() {
    super();
    nom_= "Seuil g�om�trique-Singularit� n�"+numero_;
    coteMoyenneCrete_= 0.;
    coefQ_= 0.38;
  }
  @Override
  public void dispose() {
    nom_= null;
    coteMoyenneCrete_= 0.;
    coefQ_= 0;
    super.dispose();
  }
  // attributs
  private double coefQ_;
  @Override
  public double coefQ() {
    return coefQ_;
  }
  @Override
  public void coefQ(double coefQ) {
    if (coefQ_== coefQ) return;
    coefQ_= coefQ;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefQ");
  }
  private double coteMoyenneCrete_;
  @Override
    public double coteMoyenneCrete() {
      return coteMoyenneCrete_;
    }
  @Override
    public void coteMoyenneCrete(double coteMoyenneCrete) {
      if(coteMoyenneCrete_== coteMoyenneCrete) return;
      coteMoyenneCrete_= coteMoyenneCrete;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteMoyenneCrete");
  }
  private ILoiGeometrique loi_;
  @Override
  public ILoiGeometrique loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiGeometrique loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    ILoiGeometrique loi= UsineLib.findUsine().creeHydraulique1dLoiGeometrique();
    loi(loi);
    return loi;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
}
