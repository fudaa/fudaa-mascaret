/**
 * @file         DSeuilLimniAmont.java
 * @creation     2000-08-09
 * @modification $Date: 2007-03-28 15:35:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigramme;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLimniAmont;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLimniAmontOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil limnim�tre amont".
 * Ajoute une r�f�rence vers une loi limnigramme.
 * @version      $Revision: 1.12 $ $Date: 2007-03-28 15:35:27 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilLimniAmont
  extends DSeuil
  implements ISeuilLimniAmont,ISeuilLimniAmontOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilLimniAmont) {
      ISeuilLimniAmont s= (ISeuilLimniAmont)_o;
      coteCrete(s.coteCrete());
      if (s.loi() != null)
        loi((ILoiLimnigramme)s.loi().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilLimniAmont s= UsineLib.findUsine().creeHydraulique1dSeuilLimniAmont();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "seuilLimniAmont " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil abaques (Zam, t)-Singularit� n�"+numero_;
    res[1]= "Abscisse : " + abscisse_ + " Cote rupture : " + coteRupture_;
    if (loi_ != null)
      res[1]= res[1] + " Loi limnigramme : " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  /*** ISeuilLimniAmont ***/
  // constructeurs
  public DSeuilLimniAmont() {
    super();
    nom_= "Seuil limni amont-Singularit� n�"+numero_;
    coteCrete_= 0.;
  }
  @Override
  public void dispose() {
    nom_= null;
    coteCrete_= 0.;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  @Override
  public double coteCrete() {
    return coteCrete_;
  }
  @Override
  public void coteCrete(double coteCrete) {
    if(coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteCrete");
  }

  private ILoiLimnigramme loi_;
  @Override
  public ILoiLimnigramme loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiLimnigramme loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    ILoiLimnigramme loi= UsineLib.findUsine().creeHydraulique1dLoiLimnigramme();
    loi(loi);
    return loi;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
}
