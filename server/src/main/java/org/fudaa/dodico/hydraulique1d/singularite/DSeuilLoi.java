/**
 * @file         DSeuilLoi.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLoi;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLoiOperations;
import org.fudaa.dodico.corba.hydraulique1d.singularite.LEpaisseurSeuil;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil loi".
 * Ajoute une cote de cr�te, un coefficient de d�bit et une �paisseur (mince ou �pais).
 * @version      $Revision: 1.15 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilLoi extends DSeuil implements ISeuilLoi,ISeuilLoiOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilLoi) {
      ISeuilLoi s= (ISeuilLoi)_o;
      coteCrete(s.coteCrete());
      coefQ(s.coefQ());
      epaisseur(s.epaisseur());
      gradient(s.gradient());
      debit(s.debit());
    }
  }
  @Override
  public IObjet creeClone() {
    ISeuilLoi s= UsineLib.findUsine().creeHydraulique1dSeuilLoi();
    s.initialise(tie());
    return s;
  }
  @Override
  public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "seuilLoi " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String epaisseur="mince";
    if (epaisseur_.value() == LEpaisseurSeuil._EPAIS) {
      epaisseur="�pais";
    }
    String[] res= new String[2];
    res[0]= "Seuil (Zcr�te,coef.Q)-Singularit� n�"+numero_;
    res[1]=
      "Absc. : "
        + abscisse_
        + " Zrupture : "
        + coteRupture_
        + " Zcr�te : "
        + coteCrete_
        + " Coef. Q : "
        + coefQ_
        + " Epaisseur : "
        + epaisseur
        +" gradient Q : "
        + gradient_
        +" d�bit turbin� : "
        + debit_;
    return res;
  }
  /*** ISeuilLoi ***/
  // constructeurs
  public DSeuilLoi() {
    super();
    nom_="Seuil loi-Singularit� n�"+numero_;
    coteCrete_= 0.;
    coefQ_= 0.38;
    epaisseur_= LEpaisseurSeuil.EPAIS;
    gradient_= 5000.;
    debit_=0;
  }
  @Override
  public void dispose() {
    coteCrete_= 0.;
    coefQ_= 0;
    epaisseur_= null;
    gradient_= 0.;
    debit_=0;
    super.dispose();
  }
  // attributs
  protected double coteCrete_;
  @Override
  public double coteCrete() {
    return coteCrete_;
  }
  @Override
  public void coteCrete(double coteCrete) {
    if (coteCrete_==coteCrete) return;
    coteCrete_= coteCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteCrete");
  }
  protected double coefQ_;
  @Override
  public double coefQ() {
    return coefQ_;
  }
  @Override
  public void coefQ(double coefQ) {
    if (coefQ_==coefQ) return;
    coefQ_= coefQ;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coefQ");
  }
  protected LEpaisseurSeuil epaisseur_;
  @Override
  public LEpaisseurSeuil epaisseur() {
    return epaisseur_;
  }
  @Override
  public void epaisseur(LEpaisseurSeuil epaisseur) {
    if (epaisseur_.value()==epaisseur.value()) return;
    epaisseur_= epaisseur;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "epaisseur");
  }

  private double gradient_;
  @Override
  public double gradient() {
    return gradient_;
  }
  @Override
  public void gradient(double gradient) {
    if (gradient_==gradient) return;
    gradient_= gradient;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "gradient");
  }
  private double debit_;
  @Override
  public double debit() {
      return debit_;
  }

  @Override
  public void debit(double debit) {
      if (debit_ == debit)
          return;
      debit_ = debit;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(), "debit");
  }

}
