/**
 * @file         DSeuilNoye.java
 * @creation     2000-08-09
 * @modification $Date: 2007-03-28 15:35:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiSeuil;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilNoye;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilNoyeOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil noy�".
 * Ajoute une cote moyenne de cr�te, une r�f�rence vers une loi de seuil.
 * @version      $Revision: 1.12 $ $Date: 2007-03-28 15:35:27 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilNoye extends DSeuil implements ISeuilNoye,ISeuilNoyeOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilNoye) {
      ISeuilNoye s= (ISeuilNoye)_o;
      coteMoyenneCrete(s.coteMoyenneCrete());
      coteCrete(s.coteCrete());
      if (s.loi() != null)
        loi((ILoiSeuil)s.loi().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilNoye s= UsineLib.findUsine().creeHydraulique1dSeuilNoye();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "seuilNoye " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil (Zam, Zav, Q)-Singularit� n�"+numero_;
    res[1]=
      "Abscisse : "
        + abscisse_
        + " Cote rupture : "
        + coteRupture_
        + " Cote moyenne cr�te : "
        + coteMoyenneCrete_;
    if (loi_ != null)
      res[1]= res[1] + " Loi de seuil : " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  /*** ISeuilNoye ***/
  // constructeurs
  public DSeuilNoye() {
    super();
    nom_= "Seuil noy�-Singularit� n�"+numero_;
    coteCrete_= 0.;
    coteMoyenneCrete_= 0.;
  }
  @Override
  public void dispose() {
    nom_= null;
    coteCrete_= 0.;
    coteMoyenneCrete_= 0.;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  @Override
  public double coteCrete() {
    return coteCrete_;
  }
  @Override
  public void coteCrete(double coteCrete) {
    if(coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteCrete");
  }

  private double coteMoyenneCrete_;
  @Override
  public double coteMoyenneCrete() {
    return coteMoyenneCrete_;
  }
  @Override
  public void coteMoyenneCrete(double coteMoyenneCrete) {
    if (coteMoyenneCrete_==coteMoyenneCrete) return;
    coteMoyenneCrete_= coteMoyenneCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteMoyenneCrete");
  }
  private ILoiSeuil loi_;
  @Override
  public ILoiSeuil loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiSeuil loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    ILoiSeuil loi= UsineLib.findUsine().creeHydraulique1dLoiSeuil();
    loi(loi);
    return loi;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
}
