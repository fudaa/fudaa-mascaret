/**
 * @file         DSeuilTarageAmont.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAmont;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAmontOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil tarage amont".
 * Ajoute une r�f�rence vers une loi de tarage.
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilTarageAmont
  extends DSeuil
  implements ISeuilTarageAmont,ISeuilTarageAmontOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilTarageAmont) {
      ISeuilTarageAmont s= (ISeuilTarageAmont)_o;
      coteCrete(s.coteCrete());
      gradient(s.gradient());
      if (s.loi() != null)
        loi((ILoiTarage)s.loi().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilTarageAmont s=
      UsineLib.findUsine().creeHydraulique1dSeuilTarageAmont();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "seuilTarageAmont " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil abaques (Zam, Q)-Singularit� n�"+numero_;
    res[1]= "Abscisse : " + abscisse_ + " Cote rupture : " + coteRupture_;
    if (loi_ != null)
      res[1]= res[1] + " Loi de tarage : " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  /*** ISeuilTarageAmont ***/
  // constructeurs
  public DSeuilTarageAmont() {
    super();
    coteCrete_= 0.;
    gradient_= 5000.;
    nom_= "Seuil tarage amont-Singularit� n�"+numero_;
  }
  @Override
  public void dispose() {
    nom_= null;
    coteCrete_= 0.;
    gradient_= 0;

    super.dispose();
  }
  // attributs
  private double coteCrete_;
  @Override
 public double coteCrete() {
   return coteCrete_;
 }
  @Override
 public void coteCrete(double coteCrete) {
   if(coteCrete_== coteCrete) return;
   coteCrete_= coteCrete;
   UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteCrete");
 }
 private double gradient_;
  @Override
 public double gradient() {
   return gradient_;
 }
  @Override
 public void gradient(double gradient) {
   if (gradient_==gradient) return;
   gradient_= gradient;
   UsineLib.findUsine().fireObjetModifie(toString(), tie(), "gradient");
}
  private ILoiTarage loi_;
  @Override
  public ILoiTarage loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiTarage loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    ILoiTarage loi= UsineLib.findUsine().creeHydraulique1dLoiTarage();
    loi.amont(true);
    loi(loi);
    return loi;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
}
