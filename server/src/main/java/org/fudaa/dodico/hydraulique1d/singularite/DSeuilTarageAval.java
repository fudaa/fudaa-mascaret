/**
 * @file         DSeuilTarageAval.java
 * @creation     2000-08-09
 * @modification $Date: 2007-03-28 15:35:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAval;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAvalOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil tarage aval".
 * Ajoute une r�f�rence vers une loi de tarage.
 * @version      $Revision: 1.12 $ $Date: 2007-03-28 15:35:27 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilTarageAval
  extends DSeuil
  implements ISeuilTarageAval,ISeuilTarageAvalOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilTarageAval) {
      ISeuilTarageAval s= (ISeuilTarageAval)_o;
      coteCrete(s.coteCrete());
      if (s.loi() != null)
        loi((ILoiTarage)s.loi().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilTarageAval s= UsineLib.findUsine().creeHydraulique1dSeuilTarageAval();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "seuilTarageAval " + nom_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil abaques (Q, Zav)-Singularit� n�"+numero_;
    res[1]= "Abscisse : " + abscisse_ + " Cote rupture : " + coteRupture_;
    if (loi_ != null)
      res[1]= res[1] + " Loi de tarage : " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  /*** ISeuilTarageAval ***/
  // constructeurs
  public DSeuilTarageAval() {
    super();
    nom_= "Seuil tarage aval-Singularit� n�"+numero_;
    coteCrete_= 0.;
  }
  @Override
  public void dispose() {
    nom_= null;
    coteCrete_= 0.;
    super.dispose();
  }
  // attributs
  private double coteCrete_;
  @Override
  public double coteCrete() {
    return coteCrete_;
  }
  @Override
  public void coteCrete(double coteCrete) {
    if(coteCrete_== coteCrete) return;
    coteCrete_= coteCrete;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "coteCrete");
  }

  private ILoiTarage loi_;
  @Override
  public ILoiTarage loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiTarage loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    ILoiTarage loi= UsineLib.findUsine().creeHydraulique1dLoiTarage();
    loi.amont(false);
    loi(loi);
    return loi;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
}
