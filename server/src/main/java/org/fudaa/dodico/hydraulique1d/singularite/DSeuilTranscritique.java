/**
 * @file         DSeuilTranscritique.java
 * @creation     2001-08-01
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTranscritique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTranscritiqueOperations;
import org.fudaa.dodico.corba.hydraulique1d.singularite.LEpaisseurSeuil;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil loi transcritique".
 * Ajoute une r�f�rence vers une loi hydrogramme, un gradient,
 * et des indicateurs : param�tres avanc�s et rupture instantan�e.
 * @version      $Revision: 1.15 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilTranscritique
  extends DSeuilLoi
  implements ISeuilTranscritique,ISeuilTranscritiqueOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilTranscritique) {
      ISeuilTranscritique s= (ISeuilTranscritique)_o;
      ruptureInstantanee(s.ruptureInstantanee());//deprecated
      paramAvances(s.paramAvances());//deprecated
      gradient(s.gradient());
      debit(s.debit());
      loi(s.loi()); //deprecated
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilTranscritique s=
      UsineLib.findUsine().creeHydraulique1dSeuilTranscritique();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    //ILoiHydraulique l= getLoi();
    String s= "seuilTranscritique " + nom_;
    /*if (l != null)
      s += "(loi " + l.toString() + ")";*/ //deprecated
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    String epaisseur="mince";
    if (epaisseur_.value() == LEpaisseurSeuil._EPAIS) {
      epaisseur="�pais";
    }
    res[0]= "Seuil Loi Transc.-Singularit� n�"+numero_;
    res[1]=
      "Absc. : "
        + abscisse_
        + " Zrupture : "
        + coteRupture_
        + " Zcr�te : "
        + coteCrete_
        + " Coef. Q : "
        + coefQ_
        + " Epais. : "
        + epaisseur;
    if (ruptureInstantanee_)
      res[1] += " rupture instantan�e";
    else
      res[1] += " Coef. Q : " + coefQ_;
    if (paramAvances_) {
      res[1] += " gradient Q : " + gradient_;
      res[1] += " d�bit turbin� : " + debit_;
      /*if (loi_ != null)
        res[1] += " Loi Hydrogramme : " + loi_.nom();
      else
        res[1] += " Loi inconnue";*/
    }
    return res;
  }
  /*** ISeuilLoi ***/
  // constructeurs
  public DSeuilTranscritique() {
    super();
    nom_="Seuil transc.-Singularit� n�"+numero_;
    ruptureInstantanee_= false;
    paramAvances_= true;
    gradient_= 5000.;
    debit_=0;
  }
  @Override
  public void dispose() {
    ruptureInstantanee_= false;
    paramAvances_= true;
    gradient_= 0;
    debit_=0;
    super.dispose();
  }
  // attributs
  private boolean ruptureInstantanee_;
  @Override
  public boolean ruptureInstantanee() {
    return ruptureInstantanee_;
  }
  @Override
  public void ruptureInstantanee(boolean ruptureInstantanee) {
    if (ruptureInstantanee_==ruptureInstantanee) return;
    ruptureInstantanee_= ruptureInstantanee;
    UsineLib.findUsine().fireObjetModifie(
      toString(),
      tie(),
      "ruptureInstantanee");
  }
  private boolean paramAvances_;
  @Override
  public boolean paramAvances() {
    return paramAvances_;
  }
  @Override
  public void paramAvances(boolean paramAvances) {
    if (paramAvances_==paramAvances) return;
    paramAvances_= paramAvances;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "paramAvances");
  }
  private double gradient_;
  @Override
  public double gradient() {
    return gradient_;
  }
  @Override
  public void gradient(double gradient) {
    if (gradient_==gradient) return;
    gradient_= gradient;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "gradient");
  }
  private double debit_;
  @Override
  public double debit() {
      return debit_;
  }

  @Override
  public void debit(double debit) {
      if (debit_ == debit)
          return;
      debit_ = debit;
      UsineLib.findUsine().fireObjetModifie(toString(), tie(), "debit");
  }


  private ILoiHydrogramme loi_;//deprecated
  @Override
  public ILoiHydrogramme loi() {//deprecated
    return loi_;
  }
  @Override
  public void loi(ILoiHydrogramme loi) {//deprecated
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
}
