/**
 * @file         DSeuilVanne.java
 * @creation     2000-08-09
 * @modification $Date: 2006-09-12 08:35:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiOuvertureVanne;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilVanne;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilVanneOperations;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Impl�mentation de l'objet m�tier singularit� de type "seuil de vanne".
 * Ajoute une largeur, une r�f�rence vers une loi d'ouverture de vanne.
 * @version      $Revision: 1.11 $ $Date: 2006-09-12 08:35:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class DSeuilVanne extends DSeuil implements ISeuilVanne,ISeuilVanneOperations {
  @Override
  public void initialise(IObjet _o) {
    super.initialise(_o);
    if (_o instanceof ISeuilVanne) {
      ISeuilVanne s= (ISeuilVanne)_o;
      largeur(s.largeur());
      if (s.loi() != null)
        loi((ILoiOuvertureVanne)s.loi().creeClone());
    }
  }
  @Override
  final public IObjet creeClone() {
    ISeuilVanne s= UsineLib.findUsine().creeHydraulique1dSeuilVanne();
    s.initialise(tie());
    return s;
  }
  @Override
  final public String toString() {
    ILoiHydraulique l= getLoi();
    String s= "Seuil Vanne-Singularit� n�"+numero_;
    if (l != null)
      s += "(loi " + l.toString() + ")";
    return s;
  }
  @Override
  public String[] getInfos() {
    String[] res= new String[2];
    res[0]= "Seuil Vanne-Singularit� n�"+numero_;
    res[1]=
      "Abscisse : "
        + abscisse_
        + " Z rupture : "
        + coteRupture_
        + " largeur : "
        + largeur_;
    if (loi_ != null)
      res[1]= res[1] + " Loi ouverture vanne: " + loi_.nom();
    else
      res[1]= res[1] + " Loi inconnue";
    return res;
  }
  /*** ISeuilVanne ***/
  // constructeurs
  public DSeuilVanne() {
    super();
    largeur_= 1;
    loi_= null;
  }
  @Override
  public void dispose() {
    largeur_= 0;
    loi_= null;
    super.dispose();
  }
  // attributs
  private double largeur_;
  @Override
  public double largeur() {
    return largeur_;
  }
  @Override
  public void largeur(double largeur) {
    if (largeur_==largeur) return;
    largeur_= largeur;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "largeur");
  }
  private ILoiOuvertureVanne loi_;
  @Override
  public ILoiOuvertureVanne loi() {
    return loi_;
  }
  @Override
  public void loi(ILoiOuvertureVanne loi) {
    if (loi_==loi) return;
    loi_= loi;
    UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");
  }
  // Methode
  @Override
  public ILoiHydraulique creeLoi() {
    ILoiOuvertureVanne loi=
      UsineLib.findUsine().creeHydraulique1dLoiOuvertureVanne();
    loi(loi);
    return loi;
  }
  @Override
  public ILoiHydraulique getLoi() {
    return loi_;
  }
}
