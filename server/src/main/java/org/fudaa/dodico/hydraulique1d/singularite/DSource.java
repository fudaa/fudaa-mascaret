
/**
 * @file         DSource.java
 * @creation     2006-02-27
 * @modification $Date: 2006-11-17 16:37:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.hydraulique1d.singularite;

import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTracer;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISource;
import org.fudaa.dodico.corba.hydraulique1d.singularite.LTypeSource;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.hydraulique1d.DSingularite;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Implémentation de l'objet métier singularité de type "source" de traceur.
 * @version      $Revision: 1.6 $ $Date: 2006-11-17 16:37:34 $ by $Author: opasteur $
 * @author       Olivier Pasteur
 */
public class DSource extends DSingularite implements ISource {

  @Override
    public void initialise(IObjet _o) {
        super.initialise(_o);
        if (_o instanceof ISource) {
            ISource s = (ISource) _o;
            longueur(s.longueur());
            type(s.type());
            if (s.loi() != null)
                loi((ILoiTracer) s.loi());
        }
    }

  @Override
    final public IObjet creeClone() {
        ISource s = UsineLib.findUsine().creeHydraulique1dSource();
        s.initialise(tie());
        return s;
    }

  @Override
    final public String toString() {
        ILoiTracer l = (ILoiTracer) getLoi();
        String s = "source " + nom_;
        if (l != null)
            s += "(loi traceur " + l.toString() + ")";
        return s;
    }

  @Override
    public String[] getInfos() {
        String[] res = new String[2];
        res[0] = "Source-Singularité n°"+numero_;
        res[1] = "Abscisse : " + abscisse_ + " Longueur : " + longueur_;
        if (loi_ != null)
            res[1] = res[1] + " Loi : " + loi_.nom();
        else
            res[1] = res[1] + " Loi inconnue";
        return res;
    }

    // constructeurs
    public DSource() {
        super();
        nom_ = "Source-Singularité n°"+numero_;
        type_ = LTypeSource.VOLUME;
        longueur_ = 0.;
        loi_ = null;
    }

  @Override
    public void dispose() {
        nom_ = null;
        type_ = LTypeSource.VOLUME;
        longueur_ = 0.;
        loi_ = null;
        super.dispose();
    }

    // attributs
    /**
     * longueur
     *
     * @return double
     */
    private double longueur_;
  @Override
    public double longueur() {
        return longueur_;
    }

    /**
     * longueur
     *
     * @param newLongueur double
     */
  @Override
    public void longueur(double longueur) {
        if (longueur_ == longueur)
            return;
        longueur_ = longueur;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "longueur");

    }


    /**
     * type
     *
     * @return LTypeSource
     */
    private LTypeSource type_;
  @Override
    public LTypeSource type() {
        return type_;
    }

    /**
     * type
     *
     * @param newType LTypeSource
     */
  @Override
    public void type(LTypeSource newType) {
        if (type_.value() == newType.value())
            return;
        type_ = newType;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "type");
    }


    /**
     * loi
     *
     * @return ILoiTracer
     */
    private org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTracer loi_;
  @Override
    public ILoiTracer loi() {
        return loi_;
    }

    /**
     * loi
     *
     * @param newLoi ILoiTracer
     */
  @Override
    public void loi(ILoiTracer newLoi) {
        if (loi_==newLoi) return;
        loi_ = newLoi;
        UsineLib.findUsine().fireObjetModifie(toString(), tie(), "loi");

    }

    /**
     * creeLoi
     *
     * @return ILoiHydraulique
     */
  @Override
    public ILoiHydraulique creeLoi() {
        ILoiTracer tra = UsineLib.findUsine().creeHydraulique1dLoiTracer();
        System.out.println("!!!!!!!!!!!Erreur on utilise la fonction creeLoi dans DSource, celle-ci n'est pas valable, il faut utiliser creeLoiTracer sinon le tableau n'est pas dimenssionné en fonction du nombre de traceur");
        loi(tra);
        return tra;

    }

/**
 * creeLoiTracer
 *
 * @return ILoiTracer
 */
  @Override
  public ILoiTracer creeLoiTracer(int nbColonne) {
    ILoiTracer tra = UsineLib.findUsine().creeHydraulique1dLoiTracer();
    tra.setTailleTableau(nbColonne);
    loi(tra);
    return tra;
}



    /**
     * getLoi
     *
     * @return ILoiHydraulique
     */
  @Override
    public ILoiHydraulique getLoi() {
        return loi_;
    }
}

