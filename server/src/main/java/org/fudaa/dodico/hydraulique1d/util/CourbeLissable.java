package org.fudaa.dodico.hydraulique1d.util;

/**
 * A implementer pour l'algorithme de lissage.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 *
 */
public interface CourbeLissable {
  
  /**
   * @return Le nombre de points de la courbe
   */
  public int getNbPoint();
  
  /**
   * @param _ipt L'indice du point
   * @return La coordonn�e X du point
   */
  public double getX(int _ipt);
  
  /**
   * @param _ipt L'indice du point
   * @return La coordonn�e Y du point
   */
  public double getY(int _ipt);
  
  /**
   * @param _ipt L'indice du point
   * @return True si le point peut �tre liss�.
   */
  public boolean canModifyY(int _ipt);
  
  /**
   * Modification du point donn� avec la valeur donn�e.
   * @param _ipt L'indice du point
   * @param _y La valeur y.
   */
  public void setY(int _ipt, double _y);
}
