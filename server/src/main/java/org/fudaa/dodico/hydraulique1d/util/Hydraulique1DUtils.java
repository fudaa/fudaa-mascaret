package org.fudaa.dodico.hydraulique1d.util;

import java.util.Arrays;
import java.util.Vector;

/**
 * Des algorithmes de calcul g�n�raux.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class Hydraulique1DUtils {
  /** Types de lissage */
  public static final int LISSAGE_MOYENNE=1;
  public static final int LISSAGE_MEDIANE=0;
  
  /**
   * @param _crb La courbe a lisser
   * @param _lissPtExtr True : Les points extremit�s sont liss�s.
   * @param _meth La m�thode de lissage.
   * @param _larg La largeur de fenetre de lissage.
   */
  public static void lisser(CourbeLissable _crb, double _larg, int _meth, boolean _lissPtExtr) {
    
    int nb=_crb.getNbPoint();
    if (nb==0) return;

    // Pour les y modifi�s on travaille sur un tableau temporaire.
    double[] ypts=new double[nb];
    // Les y des points dans la fenetre
    Vector<Double> vyptsFen=new Vector<Double>();

    double xmax=_crb.getX(nb-1);
    double xmin=_crb.getX(0);
    
    for (int i=0; i<nb; i++) {
      vyptsFen.clear();

      double xpt=_crb.getX(i);
      vyptsFen.add(_crb.getY(i));
      
      boolean isPtExtr = xpt<xmin+_larg/2 | xpt>xmax-_larg/2;

      // Les points supportant des limites ne doivent pas �tre modifi�s.
      if (_crb.canModifyY(i) && (_lissPtExtr | !isPtExtr)) {

        int ind=i-1;
        // Les points pr�c�dents (on passe les points nuls)
        while (ind>=0&&Math.abs(_crb.getX(ind)-xpt)<_larg/2) {
          vyptsFen.add(_crb.getY(ind));
          ind--;
        }
        // Les points suivants (on passe les points nuls)
        ind=i+1;
        while (ind<nb&&Math.abs(_crb.getX(ind)-xpt)<_larg/2) {
          vyptsFen.add(_crb.getY(ind));
          ind++;
        }
      }

      Double[] yptsFen=vyptsFen.toArray(new Double[vyptsFen.size()]);

      // M�diane glissante : La moyenne des 2 valeurs centrales apr�s tri des valeurs.
      if (_meth==LISSAGE_MEDIANE) {
        Arrays.sort(yptsFen);
        if (yptsFen.length%2==1)
          ypts[i]=yptsFen[yptsFen.length/2].doubleValue();
        else
          ypts[i]=(yptsFen[yptsFen.length/2].doubleValue()+yptsFen[yptsFen.length/2-1].doubleValue())/2.;
      }

      // Moyenne glissante : La moyenne de toutes les valeurs.
      else {
        for (int j=0; j<yptsFen.length; j++) ypts[i]+=yptsFen[j].doubleValue();
        ypts[i]/=yptsFen.length;
      }
    }

    // Mise a jour du profil
    for (int i=0; i<nb; i++) _crb.setY(i,ypts[i]);
  }

}
