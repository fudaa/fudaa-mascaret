/**
 * @file DParametresMascaret.java
 * @creation 2000-05-10
 * @modification $Date: 2007-12-10 18:33:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 EDF/LNHE
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;
import org.fudaa.ctulu.CtuluIOResult;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.corba.mascaret.*;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;

/**
 * Classe qui impl�mente cot� serveur l\u2019interface IParametresMascaret qui contient les param�tres du mod�le Mascaret.
 *
 * @version $Revision: 1.27 $ $Date: 2007-12-10 18:33:50 $ by $Author: jm_lacombe $
 * @author Jean-Marc Lacombe , Axel von Arnim
 */
public class DParametresMascaret
        extends DParametres
        implements IParametresMascaret, IParametresMascaretOperations {

  private final static boolean DEBUG =
          "yes".equals(System.getProperty("fudaa.debug"));
  private SParametresCAS paramsCAS_; // le fichier cas (parametres) de mascaret
  private SResultatsTemporelSpatial paramsLigneEauInitiale_; // le fichier de ligne d'eau initiale
  private SParametresGEO paramsGEO_;
  // Fichier geometrie contenant les profils en travers
  private SGeoCasiers casierGEO_;
  // Fichier geometrie contenant la g�om�trie des casiers
  private SParametresREP paramsREP_; // Fichier de reprise en lecture
  private SParametresNCA paramsNCA_; // Fichier Fichiercas.txt
  private SParametresTailleMaxFichier paramsTailleMaxFichier_; // Taille maximum des fichier
  private SLoiHydraulique[] loisHydrauliques_; // Fichier des loi hydrauliques
  private SLoiTracer[] loisTracer_; // Fichier des loi Tracer
  private SResultatsTemporelSpatial paramsConcentInitiales_; // le fichier des concentrations initiales
  private SParamMeteoTracer paramMeteoTracer_;   // le fichier meteo Tracer
  private SParamPhysTracer[] parametresPhysModele_;  // le fichier des parametres physique Tracer

  public DParametresMascaret() {
    super();
    paramsCAS_ = new SParametresCAS();
    paramsLigneEauInitiale_ = new SResultatsTemporelSpatial();
    paramsGEO_ = new SParametresGEO();
    paramsNCA_ = new SParametresNCA();
    paramsTailleMaxFichier_ = new SParametresTailleMaxFichier();
    loisHydrauliques_ = new SLoiHydraulique[0];
    loisTracer_ = new SLoiTracer[0];
    paramsConcentInitiales_ = new SResultatsTemporelSpatial();
    paramMeteoTracer_ = new SParamMeteoTracer();
    parametresPhysModele_ = new SParamPhysTracer[0];
  }

  @Override
  public final Object clone() throws CloneNotSupportedException {
    return new DParametresMascaret();
  }

  @Override
  public String toString() {
    return CDodico.objectToString(this);
  }
  ///********************** Fonctions d'Ecriture des donnees dans les fichiers *********************
  //******************* .CAS ***********************

  @Override
  public SParametresCAS parametresCAS() {
    return paramsCAS_;
  }

  @Override
  public void parametresCAS(SParametresCAS _paramsCAS) {
    paramsCAS_ = _paramsCAS;
  }

  public static void ecritParametresXCAS(File fichier, SParametresCAS params) {
    EdamoxXmlReaderWriter writer = EdamoxXmlReaderWriter.createReaderWriter();
    EdamoxXMLContent content = new EdamoxXMLContent(params);
    writer.writeXML(content, fichier);
  }

  public static void ecritParametresCAS(File fichier, SParametresCAS params) {
    try {
      PrintWriter fcas = new PrintWriter(new FileWriter(fichier));
      System.out.println("Creation du fichier " + fichier.getName());
      String dateCourante = SimpleDateFormat.getInstance().format(new Date());
      // l'entete du fichier
      fcas.println("/* genere par fudaaMascaret le " + dateCourante);
      fcas.println("/"); // 1 lignes de commentaire vide
      fcas.println("/Rubriques");
      fcas.println("/"); // 1 lignes de commentaire vide
      //
      // ecriture des differentes rubriques
      //
      if (params.parametresGen != null) {
        ecritParametresGen(fcas, params.parametresGen);
      }
      if (params.parametresModelPhy != null) {
        ecritParametresModelPhy(fcas, params.parametresModelPhy);
      }
      if (params.parametresNum != null) {
        ecritParametresNum(fcas, params.parametresNum);
      }
      if (params.parametresTemp != null) {
        ecritParametresTemp(fcas, params.parametresTemp);
      }
      if (params.parametresGeoReseau != null) {
        ecritParametresGeoReseau(fcas, params.parametresGeoReseau);
      }
      if (params.parametresConfluents != null) {
        ecritParametresConfluents(fcas, params.parametresConfluents);
      }
      if (params.parametresPlanimMaillage != null) {
        ecritParametresPlanimMaillage(fcas, params.parametresPlanimMaillage);
      }
      if (params.parametresSingularite != null) {
        ecritParametresSingularite(fcas, params.parametresSingularite);
      }
      if (params.parametresCasier != null) {
        ecritParametresCasier(fcas, params.parametresCasier);
      }
      if (params.parametresApporDeversoirs != null) {
        ecritParametresApportDeversoirs(fcas, params.parametresApporDeversoirs);
      }
      if (params.parametresCalage != null) {
        ecritParametresCalage(fcas, params.parametresCalage);
      }
      if (params.parametresCalageAuto != null) {
        ecritParametresCalageAuto(fcas, params.parametresCalageAuto);
      }
      if (params.parametresTracer != null) {
        ecritParametresTracer(fcas, params.parametresTracer);
      }
      if (params.parametresLoisHydrau != null) {
        ecritParametresLoisHydrau(fcas, params.parametresLoisHydrau);
      }
      if (params.parametresCondInit != null) {
        ecritParametresCondInit(fcas, params.parametresCondInit);
      }
      if (params.parametresImpressResult != null) {
        ecritParametresImpressResult(fcas, params.parametresImpressResult);
      }
      if (params.parametresVarCalc != null) {
        ecritParametresVarCalc(fcas, params.parametresVarCalc);
      }
      if (params.parametresVarStock != null) {
        ecritParametresVarStock(fcas, params.parametresVarStock);
      }
      fcas.close();
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      throw new RuntimeException("Probl�me lors de l'�criture du fichier cas\n"
              + ex.getLocalizedMessage());
    };
  }

  private final static void ecritParametresGen(
          Writer fcas,
          SParametresGen params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("PARAMETRES GENERAUX (1)");
    Object[] descriptionChamp = new Object[11];
    descriptionChamp[0] = "VERSION DU CODE";
    descriptionChamp[1] = "NOYAU DE CALCUL";
    descriptionChamp[2] = "FICHIER DES MOT-CLES";
    descriptionChamp[3] = "DICTIONNAIRE";
    descriptionChamp[4] = "PROGRAMME PRINCIPAL";
    descriptionChamp[5] = "SAUVEGARDE DU MODELE";
    descriptionChamp[6] = "FICHIER SAUVEGARDE DU MODELE";
    descriptionChamp[7] = "CALCUL POUR VALIDATION DU CODE";
    descriptionChamp[8] = "TYPE DE CALCUL DE VALIDATION EFFECTUE";
    descriptionChamp[9] = "PRESENCE DE CASIERS";
    // description sous rubrique bibliotheques
    Object[] descriptionBiblio = new Object[2];
    if (params.bibliotheques != null) {
      descriptionBiblio[0] = "BIBLIOTHEQUES (2)";
      Object[] descriptionChampBiblio = new Object[1];
      descriptionChampBiblio[0] = "BIBLIOTHEQUES";
      descriptionBiblio[1] = descriptionChampBiblio;
    }
    descriptionChamp[10] = descriptionBiblio;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresModelPhy(
          Writer fcas,
          SParametresModelPhy params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("MODELISATION PHYSIQUE (1)");
    Object[] descriptionChamp = new Object[6];
    descriptionChamp[0] = "PERTES DE CHARGE AUTOMATIQUE AUX CONFLUENTS";
    descriptionChamp[1] = "COMPOSITION DES LITS";
    descriptionChamp[2] = "CONSERVATION DU FROTTEMENT SUR LES PAROIS VERTICALES";
    descriptionChamp[3] = "ELEVATION DE COTE ARRIVEE DU FRONT";
    descriptionChamp[4] = "INTERPOLATION LINEAIRE DES STRICKLER";
    // description sous rubrique debordement progressif
    Object[] descriptionDebord = new Object[2];
    if (params.debordement != null) {
      descriptionDebord[0] = "DEBORDEMENT PROGRESSIF (2)";
      Object[] descriptionChampDebord = new Object[2];
      descriptionChampDebord[0] = "DEBORDEMENT PROGRESSIF LIT MAJEUR";
      descriptionChampDebord[1] = "DEBORDEMENT PROGRESSIF ZONES DE STOCKAGE";
      descriptionDebord[1] = descriptionChampDebord;
    }
    descriptionChamp[5] = descriptionDebord;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresNum(
          Writer fcas,
          SParametresNum params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("PARAMETRES NUMERIQUES (1)");
    Object[] descriptionChamp = new Object[11];
    int n = 0;
    descriptionChamp[n++] = "CALCUL D'UNE ONDE DE SUBMERSION";
    descriptionChamp[n++] = "FROUDE LIMITE POUR LES CONDITIONS LIMITES";
    descriptionChamp[n++] = "TRAITEMENT IMPLICITE DU FROTTEMENT";
    descriptionChamp[n++] = "HAUTEUR D'EAU MINIMALE";
    descriptionChamp[n++] = "IMPLICITATION DU NOYAU TRANSCRITIQUE";
    descriptionChamp[n++] = "OPTIMISATION DU NOYAU TRANSCRITIQUE";
    descriptionChamp[n++] = "PERTES DE CHARGE AUTOMATIQUE NOYAU TRANSCRITIQUE";
    descriptionChamp[n++] = "TERMES NON HYDROSTATIQUES POUR LE NOYAU TRANSCRITIQUE";
    descriptionChamp[n++] = "APPORT DE DEBIT DANS LA QUANTITE DE MVT";
    descriptionChamp[n++] = "ATTENUATION DE LA CONVECTION";

    // description sous rubrique casier
    Object[] descriptionCasier = new Object[2];
    descriptionChamp[n++] = descriptionCasier;
    
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresTemp(
          Writer fcas,
          SParametresTemp params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("PARAMETRES TEMPORELS (1)");
    Object[] descriptionChamp = new Object[10];
    descriptionChamp[0] = "PAS DE TEMPS";
    descriptionChamp[1] = "TEMPS INITIAL";
    descriptionChamp[2] = "CRITERE D'ARRET DU CALCUL";
    descriptionChamp[3] = "NOMBRE DE PAS DE TEMPS";
    descriptionChamp[4] = "TEMPS MAXIMUM";
    descriptionChamp[5] = "PAS DE TEMPS VARIABLE SUIVANT NOMBRE DE COURANT";
    descriptionChamp[6] = "NOMBRE DE COURANT SOUHAITE";
    descriptionChamp[7] = "COTE MAXIMALE DE CONTROLE";
    descriptionChamp[8] = "POINT DE CONTROLE ABSCISSE";
    descriptionChamp[9] = "POINT DE CONTROLE BIEF ASSOCIE";
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresGeoReseau(
          Writer fcas,
          SParametresGeoReseau params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("GEOMETRIE-RESEAU (1)");
    Object[] descriptionChamp = new Object[4];
    // description sous rubrique geometrie
    Object[] descriptionRubGeom = new Object[2];
    if (params.geometrie != null) {
      descriptionRubGeom[0] = "GEOMETRIE (2)";
      Object[] descriptionChampRubGeom = new Object[3];
      descriptionChampRubGeom[0] = "FICHIER DE GEOMETRIE";
      descriptionChampRubGeom[1] = "FORMAT DU FICHIER DE GEOMETRIE";
      descriptionChampRubGeom[2] = "PROFILS EN ABSCISSE ABSOLUE";
      descriptionRubGeom[1] = descriptionChampRubGeom;
    }
    descriptionChamp[0] = descriptionRubGeom;
    // description sous rubrique branche
    Object[] descriptionRubBranch = new Object[2];
    if (params.branches != null) {
      descriptionRubBranch[0] = "BRANCHES (2)";
      Object[] descriptionChampRubBranch = new Object[6];
      descriptionChampRubBranch[0] = "NOMBRE DE BRANCHES";
      descriptionChampRubBranch[1] = "BRANCHE NUMERO";
      descriptionChampRubBranch[2] = "ABSCISSE DEBUT";
      descriptionChampRubBranch[3] = "ABSCISSE FIN";
      descriptionChampRubBranch[4] = "NUM DE L'EXTREMITE DE DEBUT";
      descriptionChampRubBranch[5] = "NUM DE L'EXTREMITE DE FIN";
      descriptionRubBranch[1] = descriptionChampRubBranch;
    }
    descriptionChamp[1] = descriptionRubBranch;
    // description sous rubrique noeuds
    Object[] descriptionRubNoeuds = new Object[2];
    if (params.noeuds != null) {
      descriptionRubNoeuds[0] = "NOEUDS (2)";
      Object[] descriptionChampRubNoeuds = new Object[2];
      descriptionChampRubNoeuds[0] = "NOMBRE DE NOEUDS";
      // description des sous sous rubrique noeud
      if (params.noeuds.noeuds != null) {
        Object[] descriptionRubNoeud = new Object[params.noeuds.noeuds.length];
        Object[] descriptionRubNoeud_i = new Object[2];
        descriptionRubNoeud_i[0] = "";
        Object[] descriptionChampRubNoeud_i = new Object[1];
        for (int i = 0; i < params.noeuds.noeuds.length; i++) {
          descriptionChampRubNoeud_i[0] = "NOEUD " + (i + 1);
          descriptionRubNoeud_i[1] = descriptionChampRubNoeud_i.clone();
          descriptionRubNoeud[i] = descriptionRubNoeud_i.clone();
        }
        descriptionChampRubNoeuds[1] = descriptionRubNoeud;
      }
      descriptionRubNoeuds[1] = descriptionChampRubNoeuds;
    }
    descriptionChamp[2] = descriptionRubNoeuds;
    // description sous rubrique extremites libres
    Object[] descriptionRubExtLib = new Object[2];
    if (params.extrLibres != null) {
      descriptionRubExtLib[0] = "EXTREMITES LIBRES (2)";
      Object[] descriptionChampRubExtLib = new Object[6];
      descriptionChampRubExtLib[0] = "NOMBRE D'EXTREMITES LIBRES";
      descriptionChampRubExtLib[1] = "EXTREMITE LIBRE NUMERO";
      descriptionChampRubExtLib[2] = "EXTREMITE NUMERO";
      descriptionChampRubExtLib[3] = "NOM EXTREMITE";
      descriptionChampRubExtLib[4] = "TYPE DE CONDITION";
      descriptionChampRubExtLib[5] = "NUMERO DE LA LOI";
      descriptionRubExtLib[1] = descriptionChampRubExtLib;
    }
    descriptionChamp[3] = descriptionRubExtLib;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresConfluents(
          Writer fcas,
          SParametresConfluents params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("CONFLUENTS (1)");
    Object[] descriptionChamp = new Object[2];
    descriptionChamp[0] = "NOMBRE DE CONFLUENTS";
    // description des sous rubriques confluent
    Object[] descriptionLesConf = new Object[params.confluents.length];
    if (params.confluents != null) {
      Object[] descriptionConf_i = new Object[2];
      Object[] descriptionChampConf_i = new Object[5];
      for (int i = 0; i < params.confluents.length; i++) {
        descriptionConf_i[0] = "CONFLUENT NUMERO " + (i + 1) + " (2)";
        descriptionChampConf_i[0] = "NOMBRE D'AFFLUENTS DU CONFLUENT " + (i + 1);
        descriptionChampConf_i[1] = "NOM DU CONFLUENT " + (i + 1);
        descriptionChampConf_i[2] =
                "ABSCISSE DE L'AFFLUENT DU CONFLUENT " + (i + 1);
        descriptionChampConf_i[3] =
                "ORDONNEE DE L'AFFLUENT DU CONFLUENT " + (i + 1);
        descriptionChampConf_i[4] =
                "ANGLE DE L'AFFLUENT DU CONFLUENT " + (i + 1);
        descriptionConf_i[1] = descriptionChampConf_i.clone();
        descriptionLesConf[i] = descriptionConf_i.clone();
      }
    }
    descriptionChamp[1] = descriptionLesConf;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresPlanimMaillage(
          Writer fcas,
          SParametresPlanimMaillage params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("PLANIMETRAGE ET MAILLAGE (1)");
    Object[] descriptionChamp = new Object[3];
    descriptionChamp[0] = "METHODE DE CALCUL DU MAILLAGE";
    // description sous rubrique planimetrage
    Object[] descriptionPlanim = new Object[2];
    if (params.planim != null) {
      descriptionPlanim[0] = "PLANIMETRAGE (2)";
      Object[] descriptionChampPlanim = new Object[5];
      descriptionChampPlanim[0] = "NOMBRE DE PAS DE PLANIMETRAGE";
      descriptionChampPlanim[1] = "NOMBRE DE ZONES DE PLANIMETRAGE";
      descriptionChampPlanim[2] = "VALEUR DU PAS";
      descriptionChampPlanim[3] = "NUMERO DU PREMIER PROFIL";
      descriptionChampPlanim[4] = "NUMERO DU DERNIER PROFIL";
      descriptionPlanim[1] = descriptionChampPlanim;
    }
    descriptionChamp[1] = descriptionPlanim;
    // description sous rubrique maillage
    Object[] descriptionMaill = new Object[2];
    if (params.maillage != null) {
      descriptionMaill[0] = "MAILLAGE (2)";
      Object[] descriptionChampMaill = new Object[5];
      descriptionChampMaill[0] = "MODE DE SAISIE DU MAILLAGE";
      descriptionChampMaill[1] = "FICHIER DU MAILLAGE";
      descriptionChampMaill[2] = "SAUVEGARDE MAILLAGE";
      descriptionChampMaill[3] = "FICHIER DE SAUVEGARDE DU MAILLAGE";
      // description sous sous rubrique maillage clavier
      Object[] descriptionMaillClavier = new Object[2];
      if (params.maillage.maillageClavier != null) {
        descriptionMaillClavier[0] = "MAILLAGE PAR CLAVIER (3)";
        Object[] descriptionChampMaillClavier = new Object[13];
        descriptionChampMaillClavier[0] = "NOMBRE DE SECTIONS DE CALCUL";
        descriptionChampMaillClavier[1] = "NUMEROS DES SECTIONS DE CALCUL";
        descriptionChampMaillClavier[2] = "BRANCHES DES SECTIONS DE CALCUL";
        descriptionChampMaillClavier[3] = "ABSCISSES DES SECTIONS DE CALCUL";
        descriptionChampMaillClavier[4] = "NOMBRE DE PLAGES DE DISCRETISATION";
        descriptionChampMaillClavier[5] = "NUMERO DU PREMIER PROFIL DE LA SERIE";
        descriptionChampMaillClavier[6] = "NUMERO DU DERNIER PROFIL DE LA SERIE";
        descriptionChampMaillClavier[7] = "PAS D'ESPACE DE LA SERIE";
        descriptionChampMaillClavier[8] = "NOMBRE DE ZONES DE DISCRETISATION";
        descriptionChampMaillClavier[9] = "NUMERO DE BRANCHE DE ZONE";
        descriptionChampMaillClavier[10] = "ABSCISSE DE DEBUT DE ZONE";
        descriptionChampMaillClavier[11] = "ABSCISSE DE FIN DE ZONE";
        descriptionChampMaillClavier[12] = "NOMBRE DE SECTIONS DE LA ZONE";
        descriptionMaillClavier[1] = descriptionChampMaillClavier;
      }
      descriptionChampMaill[4] = descriptionMaillClavier;
      descriptionMaill[1] = descriptionChampMaill;
    }
    descriptionChamp[2] = descriptionMaill;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresSingularite(
          Writer fcas,
          SParametresSingularite params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("SINGULARITE (BARRAGE-SEUIL) (1)");
    Object[] descriptionChamp = new Object[4];
    descriptionChamp[0] = "NOMBRE DE SEUILS";
    // description sous rubrique barrage principal
    Object[] descriptionBarr = new Object[2];
    if (params.barragePrincipal != null) {
      descriptionBarr[0] = "BARRAGE PRINCIPAL (2)";
      Object[] descriptionChampBarr = new Object[4];
      descriptionChampBarr[0] = "NUM BRANCHE DU BARRAGE PRINCIPAL";
      descriptionChampBarr[1] = "ABSCISSE DU BARRAGE PRINCIPAL";
      descriptionChampBarr[2] = "TYPE DE RUPTURE DU BARRAGE PRINCIPAL";
      descriptionChampBarr[3] = "COTE DE CRETE DU BARRAGE PRINCIPAL";
      descriptionBarr[1] = descriptionChampBarr;
    }
    descriptionChamp[1] = descriptionBarr;
    // description des sous rubriques seuil
    if (params.seuils != null) {
      Object[] descriptionLesSeuil = new Object[params.seuils.length];
      Object[] descriptionSeuil_i = new Object[2];
      Object[] descriptionChampSeuil_i = new Object[15];
      for (int i = 0; i < params.seuils.length; i++) {
        descriptionSeuil_i[0] = "SEUIL " + (i + 1) + " (2)";
        descriptionChampSeuil_i[0] = "NOM SEUIL " + (i + 1);
        descriptionChampSeuil_i[1] = "TYPE SEUIL " + (i + 1);
        descriptionChampSeuil_i[2] = "NUM BRANCHE SEUIL " + (i + 1);
        descriptionChampSeuil_i[3] = "ABSCISSE SEUIL " + (i + 1);
        descriptionChampSeuil_i[4] = "COTE CRETE SEUIL " + (i + 1);
        descriptionChampSeuil_i[5] = "COTE MOYENNE CRETE " + (i + 1);
        descriptionChampSeuil_i[6] = "COTE RUPTURE SEUIL " + (i + 1);
        descriptionChampSeuil_i[7] = "COEFF DEBIT SEUIL " + (i + 1);
        descriptionChampSeuil_i[8] = "LARGEUR VANNE " + (i + 1);
        descriptionChampSeuil_i[9] = "NUMERO LOI SEUIL " + (i + 1);
        descriptionChampSeuil_i[10] =
                "NOMBRE DE POINTS DE LA LOI SEUIL " + (i + 1);
        descriptionChampSeuil_i[11] = "ABSCISSE EN TRAVERS CRETE " + (i + 1);
        descriptionChampSeuil_i[12] = "COTE CRETE " + (i + 1);
        descriptionChampSeuil_i[13] = "EPAISSEUR SEUIL " + (i + 1);
        descriptionChampSeuil_i[14] = "GRADIENT DE DESCENTE SEUIL " + (i + 1);
        descriptionSeuil_i[1] = descriptionChampSeuil_i.clone();
        descriptionLesSeuil[i] = descriptionSeuil_i.clone();
      }
      descriptionChamp[2] = descriptionLesSeuil;
    }
    // description sous rubrique perte de charges
    Object[] descriptionPerte = new Object[2];
    if (params.pertesCharges != null) {
      descriptionPerte[0] = "PERTES DE CHARGES (2)";
      Object[] descriptionChampPerte = new Object[4];
      descriptionChampPerte[0] = "NOMBRE DE PERTES DE CHARGE SINGULIERES";
      descriptionChampPerte[1] = "NUM BRANCHE DE LA PERTE DE CHARGE SINGULIERE";
      descriptionChampPerte[2] = "ABSCISSE DE LA PERTE DE CHARGE SINGULIERE";
      descriptionChampPerte[3] = "COEFFICIENT DE LA PERTE DE CHARGE SINGULIERE";
      descriptionPerte[1] = descriptionChampPerte;
    }
    descriptionChamp[3] = descriptionPerte;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresCasier(
          Writer fcas,
          SParametresCasier params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("PARAMETRES CASIER (1)");
    Object[] descriptionChamp = new Object[8];
    descriptionChamp[0] = "NOMBRE DE CASIERS";
    descriptionChamp[1] = "CASIERS OPTION DE PLANIMETRAGE";
    descriptionChamp[2] = "CASIERS OPTION DE CALCUL";
    descriptionChamp[3] = "CASIERS FICHIER GEOMETRIE";
    descriptionChamp[4] = "CASIERS COTE INITIALE";
    descriptionChamp[5] = "CASIERS PAS DE PLANIMETRAGE";
    descriptionChamp[6] = "CASIERS NOMBRE DE COTES DE PLANIMETRAGE";
    // description sous rubrique liaison
    Object[] descriptionLiaison = new Object[2];
    if (params.liaisons != null) {
      descriptionLiaison[0] = "LIAISONS (2)";
      Object[] descriptionChampLiaison = new Object[17];
      descriptionChampLiaison[0] = "NOMBRE DE LIAISONS";
      descriptionChampLiaison[1] = "LIAISON TYPE";
      descriptionChampLiaison[2] = "LIAISON NATURE";
      descriptionChampLiaison[3] = "LIAISON COTE";
      descriptionChampLiaison[4] = "LIAISON LARGEUR";
      descriptionChampLiaison[5] = "LIAISON LONGUEUR";
      descriptionChampLiaison[6] = "LIAISON RUGOSITE";
      descriptionChampLiaison[7] = "LIAISON SECTION";
      descriptionChampLiaison[8] = "LIAISON COEFFICIENT PERTE DE CHARGE";
      descriptionChampLiaison[9] = "LIAISON COEFFICIENT DE DEBIT SEUIL";
      descriptionChampLiaison[10] = "LIAISON COEFFICIENT D'ACTIVATION";
      descriptionChampLiaison[11] = "LIAISON COEFFICIENT DE DEBIT ORIFICE";
      descriptionChampLiaison[12] = "LIAISON TYPE ORIFICE";
      descriptionChampLiaison[13] = "LIAISON NUMERO DU CASIER ORIGINE";
      descriptionChampLiaison[14] = "LIAISON NUMERO DU CASIER FIN";
      descriptionChampLiaison[15] = "LIAISON NUMERO DU BIEF ASSOCIE";
      descriptionChampLiaison[16] = "LIAISON ABSCISSE CORRESPONDANTE";
      descriptionLiaison[1] = descriptionChampLiaison;
    }
    descriptionChamp[7] = descriptionLiaison;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresApportDeversoirs(
          Writer fcas,
          SParametresApporDeversoirs params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("APPORTS ET DEVERSOIRS (1)");
    Object[] descriptionChamp = new Object[3];
    // description sous rubrique debit d'apports
    Object[] descriptionQApport = new Object[2];
    if (params.debitsApports != null) {
      descriptionQApport[0] = "DEBITS D'APPORTS (2)";
      Object[] descriptionChampQApport = new Object[6];
      descriptionChampQApport[0] = "NOMBRE DE DEBITS D'APPORTS";
      descriptionChampQApport[1] = "NOM DE L'APPORT";
      descriptionChampQApport[2] = "NUMERO BRANCHE APPORT";
      descriptionChampQApport[3] = "ABSCISSE APPORT";
      descriptionChampQApport[4] = "LONGUEUR APPORT";
      descriptionChampQApport[5] = "NUMERO LOI APPORT";
      descriptionQApport[1] = descriptionChampQApport;
    }
    descriptionChamp[0] = descriptionQApport;
    // description sous rubrique deversoirs lateraux
    Object[] descriptionDeversLat = new Object[2];
    Object[] descriptionChampDeversLat = null;
    descriptionDeversLat[0] = "DEVERSOIRS LATERAUX (2)";

    if (params.deversLate != null) {
      descriptionChampDeversLat = new Object[10];
      descriptionChampDeversLat[0] = "NOMBRE DE DEVERSOIRS";
      descriptionChampDeversLat[1] = "NOM DEVERSOIR ";
      descriptionChampDeversLat[2] = "TYPE DEVERSOIR ";
      descriptionChampDeversLat[3] = "NUM BRANCHE DEVERSOIR ";
      descriptionChampDeversLat[4] = "ABSCISSE DEVERSOIR ";
      descriptionChampDeversLat[5] = "LONGUEUR DEVERSOIR ";
      descriptionChampDeversLat[6] = "COTE CRETE DEVERSOIR ";
      descriptionChampDeversLat[7] = "COEFF DEBIT DEVERSOIR ";
      descriptionChampDeversLat[8] = "NUMERO LOI DEVERSOIR ";
      // description des sous sous rubriques deversoir
      if (params.deversLate.deversoirsV5P2 != null) {
        Object[] descriptionLesDevers =
                new Object[params.deversLate.deversoirsV5P2.length];
        Object[] descriptionDevers_i = new Object[2];
        Object[] descriptionChampDevers_i = new Object[8];
        for (int i = 0; i < params.deversLate.deversoirsV5P2.length; i++) {
          descriptionDevers_i[0] = "DEVERSOIR " + (i + 1) + " (3)";
          descriptionChampDevers_i[0] = "NOM DEVERSOIR " + (i + 1);
          descriptionChampDevers_i[1] = "TYPE DEVERSOIR " + (i + 1);
          descriptionChampDevers_i[2] = "NUM BRANCHE DEVERSOIR " + (i + 1);
          descriptionChampDevers_i[3] = "ABSCISSE DEVERSOIR " + (i + 1);
          descriptionChampDevers_i[4] = "LONGUEUR DEVERSOIR " + (i + 1);
          descriptionChampDevers_i[5] = "COTE CRETE DEVERSOIR " + (i + 1);
          descriptionChampDevers_i[6] = "COEFF DEBIT DEVERSOIR " + (i + 1);
          descriptionChampDevers_i[7] = "NUMERO LOI DEVERSOIR " + (i + 1);
          descriptionDevers_i[1] = descriptionChampDevers_i.clone();
          descriptionLesDevers[i] = descriptionDevers_i.clone();
        }
        descriptionChampDeversLat[9] = descriptionLesDevers;
      }
      descriptionDeversLat[1] = descriptionChampDeversLat;
    }
    descriptionChamp[1] = descriptionDeversLat;
    // description sous rubrique Casier
    Object[] descriptionCasier = new Object[2];
    if (params.apportCasier != null) {
      descriptionCasier[0] = "APPORT CASIER (2)";
      Object[] descriptionChampCasier = new Object[3];
      descriptionChampCasier[0] = "NOMBRE D'APPORTS DE PLUIE";
      descriptionChampCasier[1] = "NUMERO DU CASIER ASSOCIE";
      descriptionChampCasier[2] = "NUMERO DE LA LOI ASSOCIEE";
      descriptionCasier[1] = descriptionChampCasier;
    }
    descriptionChamp[2] = descriptionCasier;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresCalage(
          Writer fcas,
          SParametresCalage params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("CALAGE (1)");
    Object[] descriptionChamp = new Object[2];
    // description sous rubrique frottement
    Object[] descriptionFrot = new Object[2];
    if (params.frottement != null) {
      descriptionFrot[0] = "FROTTEMENT (2)";
      Object[] descriptionChampFrot = new Object[7];
      descriptionChampFrot[0] = "LOI DE FROTTEMENT";
      descriptionChampFrot[1] = "NOMBRE DE ZONES DE FROTTEMENT";
      descriptionChampFrot[2] = "NUMERO DE BRANCHE DE ZONE DE FROTTEMENT";
      descriptionChampFrot[3] = "ABSCISSE DEBUT ZONE DE FROTTEMENT";
      descriptionChampFrot[4] = "ABSCISSE FIN ZONE DE FROTTEMENT";
      descriptionChampFrot[5] = "VALEUR DU COEFFICIENT LIT MINEUR";
      descriptionChampFrot[6] = "VALEUR DU COEFFICIENT LIT MAJEUR";
      descriptionFrot[1] = descriptionChampFrot;
    }
    descriptionChamp[0] = descriptionFrot;
    // description sous rubrique zones de stockage
    Object[] descriptionZonStoc = new Object[2];
    if (params.zoneStockage != null) {
      descriptionZonStoc[0] = "ZONES DE STOCKAGE (2)";
      Object[] descriptionChampZonStoc = new Object[4];
      descriptionChampZonStoc[0] =
              "NOMBRE DE PROFILS COMPORTANT DES ZONES DE STOCKAGE";
      descriptionChampZonStoc[1] = "NUMERO PROFIL STOCKAGE";
      descriptionChampZonStoc[2] = "LIMITE GAUCHE LIT MAJEUR";
      descriptionChampZonStoc[3] = "LIMITE DROITE LIT MAJEUR";
      descriptionZonStoc[1] = descriptionChampZonStoc;
    }
    descriptionChamp[1] = descriptionZonStoc;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresLoisHydrau(
          Writer fcas,
          SParametresLoisHydrau params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("LOIS HYDRAULIQUES (1)");
    Object[] descriptionChamp = new Object[2];
    descriptionChamp[0] = "NOMBRE DE LOIS HYDRAULIQUES";
    // description des sous rubriques loi
    if (params.lois != null) {
      Object[] descriptionLesLoi = new Object[params.lois.length];
      Object[] descriptionLoi_i = new Object[2];
      Object[] descriptionChampLoi_i = new Object[3];
      Object[] descriptionDonneesLoi_i = new Object[2];
      Object[] descriptionChampDonneesLoi_i = new Object[9];
      for (int i = 0; i < params.lois.length; i++) {
        descriptionLoi_i[0] = "LOI NUMERO " + (i + 1) + " (2)";
        descriptionChampLoi_i[0] = "LOI " + (i + 1) + " NOM";
        descriptionChampLoi_i[1] = "LOI " + (i + 1) + " TYPE";
        // description sous sous rubrique donnees loi
        descriptionDonneesLoi_i[0] = "DONNEES LOI " + (i + 1) + " (3)";
        descriptionChampDonneesLoi_i[0] = "LOI " + (i + 1) + " MODE D'ENTREE";
        descriptionChampDonneesLoi_i[1] = "LOI " + (i + 1) + " FICHIER";
        descriptionChampDonneesLoi_i[2] = "LOI " + (i + 1) + " UNITE DE TEMPS";
        descriptionChampDonneesLoi_i[3] = "LOI " + (i + 1) + " NOMBRE DE POINTS";
        descriptionChampDonneesLoi_i[4] = "LOI " + (i + 1) + " TEMPS";
        descriptionChampDonneesLoi_i[5] = "LOI " + (i + 1) + " COTE";
        descriptionChampDonneesLoi_i[6] = "LOI " + (i + 1) + " COTE 2";
        descriptionChampDonneesLoi_i[7] = "LOI " + (i + 1) + " DEBIT";
        descriptionChampDonneesLoi_i[8] =
                "LOI " + (i + 1) + " NOMBRE DE DEBITS DIFFERENTS";
        descriptionDonneesLoi_i[1] = descriptionChampDonneesLoi_i.clone();
        descriptionChampLoi_i[2] = descriptionDonneesLoi_i.clone();
        descriptionLoi_i[1] = descriptionChampLoi_i.clone();
        descriptionLesLoi[i] = descriptionLoi_i.clone();
      }
      descriptionChamp[1] = descriptionLesLoi;
    }
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresCondInit(
          Writer fcas,
          SParametresCondInit params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("CONDITIONS INITIALES (1)");
    Object[] descriptionChamp = new Object[3];
    // description sous rubrique reprise etude
    Object[] descriptionReprise = new Object[2];
    if (params.repriseEtude != null) {
      descriptionReprise[0] = "REPRISE ETUDE (2)";
      Object[] descriptionChampReprise = new Object[3];
      descriptionChampReprise[0] = "REPRISE DE CALCUL";
      descriptionChampReprise[1] = "FICHIER DE REPRISE EN LECTURE";
      descriptionChampReprise[2] = "BINAIRE DU FICHIER DE REPRISE EN LECTURE";
      descriptionReprise[1] = descriptionChampReprise;
    }
    descriptionChamp[0] = descriptionReprise;
    // description sous rubrique ligne d'eau
    Object[] descriptionLigEau = new Object[2];
    if (params.ligneEau != null) {
      descriptionLigEau[0] = "LIGNE D'EAU (2)";
      Object[] descriptionChampLigEau = new Object[11];
      descriptionChampLigEau[0] = "PRESENCE LIGNE D'EAU INITIALE";
      descriptionChampLigEau[1] = "MODE D'ENTREE DE LA LIGNE D'EAU";
      descriptionChampLigEau[2] = "FICHIER LIGNE D'EAU";
      descriptionChampLigEau[3] = "FORMAT LIGNE D'EAU";
      descriptionChampLigEau[4] = "NOMBRE DE POINTS DECRIVANT LA LIGNE D'EAU";
      descriptionChampLigEau[5] = "BRANCHE DE LA LIGNE D'EAU";
      descriptionChampLigEau[6] = "ABSCISSE DE LA LIGNE D'EAU";
      descriptionChampLigEau[7] = "COTE DE LA LIGNE D'EAU";
      descriptionChampLigEau[8] = "DEBIT DE LA LIGNE D'EAU";
      descriptionChampLigEau[9] = "COEF FROTTEMENT MINEUR DE LA LIGNE D'EAU";
      descriptionChampLigEau[10] = "COEF FROTTEMENT MAJEUR DE LA LIGNE D'EAU";
      descriptionLigEau[1] = descriptionChampLigEau;
    }
    descriptionChamp[1] = descriptionLigEau;
    // description sous rubrique zones seches
    Object[] descriptionZoneSeche = new Object[2];
    if (params.zonesSeches != null) {
      descriptionZoneSeche[0] = "ZONES SECHES (2)";
      Object[] descriptionChampZoneSeche = new Object[4];
      descriptionChampZoneSeche[0] = "NOMBRE DE ZONES SECHES";
      descriptionChampZoneSeche[1] = "BRANCHE DE ZONE SECHE";
      descriptionChampZoneSeche[2] = "ABSCISSE DE DEBUT DE ZONE SECHE";
      descriptionChampZoneSeche[3] = "ABSCISSE DE FIN DE ZONE SECHE";
      descriptionZoneSeche[1] = descriptionChampZoneSeche;
    }
    descriptionChamp[2] = descriptionZoneSeche;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresImpressResult(
          Writer fcas,
          SParametresImpressResult params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("IMPRESSIONS - RESULTATS (1)");
    Object[] descriptionChamp = new Object[9];
    descriptionChamp[0] = "TITRE DU CALCUL";
    // description sous rubrique impression
    Object[] descriptionImpress = new Object[2];
    if (params.impression != null) {
      descriptionImpress[0] = "IMPRESSION (2)";
      Object[] descriptionChampImpress = new Object[6];
      descriptionChampImpress[0] = "IMPRESSION DE LA GEOMETRIE";
      descriptionChampImpress[1] = "IMPRESSION DU PLANIMETRAGE";
      descriptionChampImpress[2] = "IMPRESSION DU RESEAU";
      descriptionChampImpress[3] = "IMPRESSION DES LOIS HYDRAULIQUES";
      descriptionChampImpress[4] = "IMPRESSION DE LA LIGNE D'EAU INITIALE";
      descriptionChampImpress[5] = "IMPRESSION CALCUL";
      descriptionImpress[1] = descriptionChampImpress;
    }
    descriptionChamp[1] = descriptionImpress;
    // description sous rubrique pas de stockage et d'impression
    Object[] descriptionPasStoc = new Object[2];
    if (params.pasStockage != null) {
      descriptionPasStoc[0] = "PAS DE STOCKAGE ET D'IMPRESSION (2)";
      Object[] descriptionChampPasStoc = new Object[3];
      descriptionChampPasStoc[0] = "PREMIER PAS DE TEMPS A STOCKER";
      descriptionChampPasStoc[1] = "PAS DE STOCKAGE";
      descriptionChampPasStoc[2] = "PAS D'IMPRESSION";
      descriptionPasStoc[1] = descriptionChampPasStoc;
    }
    descriptionChamp[2] = descriptionPasStoc;
    // description sous rubrique resultats
    Object[] descriptionResult = new Object[2];
    if (params.resultats != null) {
      descriptionResult[0] = "RESULTATS (2)";
      Object[] descriptionChampResult = new Object[4];
      descriptionChampResult[0] = "FICHIER RESULTATS";
      descriptionChampResult[1] = "FICHIER RESULTATS 2";
      descriptionChampResult[2] = "BINAIRE DU FICHIER RESULTATS";
      descriptionChampResult[3] = "POST-PROCESSEUR";
      descriptionResult[1] = descriptionChampResult;
    }
    descriptionChamp[3] = descriptionResult;
    // description sous rubrique listing
    Object[] descriptionListing = new Object[2];
    if (params.listing != null) {
      descriptionListing[0] = "LISTING (2)";
      Object[] descriptionChampListing = new Object[1];
      descriptionChampListing[0] = "FICHIER LISTING";
      descriptionListing[1] = descriptionChampListing;
    }
    descriptionChamp[4] = descriptionListing;
    // description sous rubrique fichier de reprise
    Object[] descriptionFichReprise = new Object[2];
    if (params.fichReprise != null) {
      descriptionFichReprise[0] = "FICHIER DE REPRISE (2)";
      Object[] descriptionChampFichReprise = new Object[2];
      descriptionChampFichReprise[0] = "FICHIER DE REPRISE EN ECRITURE";
      descriptionChampFichReprise[1] =
              "BINAIRE DU FICHIER DE REPRISE EN ECRITURE";
      descriptionFichReprise[1] = descriptionChampFichReprise;
    }
    descriptionChamp[5] = descriptionFichReprise;
    // description sous rubrique rubens
    Object[] descriptionRubens = new Object[2];
    if (params.rubens != null) {
      descriptionRubens[0] = "RUBENS (2)";
      Object[] descriptionChampRubens = new Object[1];
      descriptionChampRubens[0] = "ECART ENTRE BRANCHES";
      descriptionRubens[1] = descriptionChampRubens;
    }
    descriptionChamp[6] = descriptionRubens;
    // description sous rubrique stockage
    Object[] descriptionStockage = new Object[2];
    if (params.stockage != null) {
      descriptionStockage[0] = "STOCKAGE (2)";
      Object[] descriptionChampStockage = new Object[4];
      descriptionChampStockage[0] = "OPTION DE STOCKAGE";
      descriptionChampStockage[1] = "NOMBRE DE SITES";
      descriptionChampStockage[2] = "BRANCHE DU SITE";
      descriptionChampStockage[3] = "ABSCISSE DU SITE";
      descriptionStockage[1] = descriptionChampStockage;
    }
    descriptionChamp[7] = descriptionStockage;
    // description sous rubrique casier
    Object[] descriptionCasier = new Object[2];
    if (params.casier != null) {
      descriptionCasier[0] = "CASIER (2)";
      Object[] descriptionChampCasier = new Object[4];
      descriptionChampCasier[0] = "FICHIER RESULTATS CASIERS";
      descriptionChampCasier[1] = "FICHIER LISTING CASIERS";
      descriptionChampCasier[2] = "FICHIER RESULTATS LIAISONS";
      descriptionChampCasier[3] = "FICHIER LISTING LIAISONS";
      descriptionCasier[1] = descriptionChampCasier;
    }
    descriptionChamp[8] = descriptionCasier;
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresVarCalc(
          Writer fcas,
          SParametresVarCalc params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("VARIABLES CALCULEES (1)");
    Object[] descriptionChamp = new Object[1];
    descriptionChamp[0] = "VARIABLES CALCULEES";
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  private final static void ecritParametresVarStock(
          Writer fcas,
          SParametresVarStock params)
          throws Exception {
    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("VARIABLES STOCKEES (1)");
    Object[] descriptionChamp = new Object[1];
    descriptionChamp[0] = "VARIABLES STOCKEES";
    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  /**
   * Les parametres pour le calage automatique.
   *
   * @param fcas Writer Le fichier cas.
   * @param params SParametresCalageAuto Les parametres de calage auto.
   * @throws Exception
   */
  private final static void ecritParametresCalageAuto(Writer fcas,
          SParametresCalageAuto params)
          throws Exception {

    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("CALAGE AUTOMATIQUE (1)");
    Object[] descriptionChamp = new Object[3];

    // Description sous rubrique parametres
    Object[] descriptionParams = new Object[2];
    if (params.parametres != null) {
      descriptionParams[0] = "PARAMETRES DU CALAGE AUTOMATIQUE (2)";
      Object[] descriptionChampParams = new Object[9];
      descriptionChampParams[0] = "OPTION CALAGE";
      descriptionChampParams[1] = "PAS DE CALCUL DU GRADIENT";
      descriptionChampParams[2] = "NOMBRE D'ITERATIONS";
      descriptionChampParams[3] = "CHOIX DU LIT MINEUR-MAJEUR";
      descriptionChampParams[4] = "PRECISION CONVERGENCE";
      descriptionChampParams[5] = "RO INITIAL";
      descriptionChampParams[6] = "METHODE D'OPTIMISATION";
      descriptionChampParams[7] = "FICHIER RESULTATS CALAGE 1";
      descriptionChampParams[8] = "FICHIER RESULTATS CALAGE 2";
      descriptionParams[1] = descriptionChampParams;
    }
    descriptionChamp[0] = descriptionParams;

    // Description sous rubrique zones de frottement
    Object[] descriptionZones = new Object[2];
    if (params.zones != null) {
      descriptionZones[0] = "ZONES DE FROTTEMENT DU CALAGE AUTOMATIQUE (2)";
      Object[] descriptionChampZones = new Object[5];
      descriptionChampZones[0] = "NOMBRE DE ZONES DE FROTTEMENT POUR LE CALAGE";
      descriptionChampZones[1] = "ABSCISSE DE DEBUT DE ZONE DE CALAGE";
      descriptionChampZones[2] = "ABSCISSE DE FIN DE ZONE DE CALAGE";
      descriptionChampZones[3] = "VALEUR DU COEFFICIENT MINEUR";
      descriptionChampZones[4] = "VALEUR DU COEFFICIENT MAJEUR";
      descriptionZones[1] = descriptionChampZones;
    }
    descriptionChamp[1] = descriptionZones;

    // Description sous rubrique crues
    Object[] descriptionCrues = new Object[2];
    if (params.crues != null) {
      descriptionCrues[0] = "CRUES DU CALAGE AUTOMATIQUE (2)";
      Object[] descriptionChampCrues = new Object[2];
      descriptionChampCrues[0] = "NOMBRE DE CRUES DE CALAGE";

      // Tableau des crues
      Object[] descriptionLesCrues = new Object[params.crues.nbCrues];
      {
        for (int ic = 0; ic < params.crues.nbCrues; ic++) {
          Object[] descriptionLaCrue = new Object[2];
          descriptionLaCrue[0] = "CRUE " + (ic + 1) + " (3)";

          Object[] descriptionChampLaCrue = new Object[9];
          descriptionChampLaCrue[0] = "DEBIT POUR LA CRUE " + (ic + 1);
          descriptionChampLaCrue[1] = "COTE AVAL POUR LA CRUE " + (ic + 1);
          descriptionChampLaCrue[2] = "NOMBRE DE MESURES POUR LA CRUE " + (ic + 1);
          descriptionChampLaCrue[3] = "ABSCISSE DES MESURES POUR LA CRUE " + (ic + 1);
          descriptionChampLaCrue[4] = "COTE DES MESURES POUR LA CRUE " + (ic + 1);
          descriptionChampLaCrue[5] = "PONDERATION POUR LES MESURES CRUE " + (ic + 1);
          descriptionChampLaCrue[6] = "NOMBRE D'APPORTS POUR LA CRUE " + (ic + 1);
          descriptionChampLaCrue[7] = "ABSCISSE DES APPORTS POUR LA CRUE " + (ic + 1);
          descriptionChampLaCrue[8] = "DEBIT DES APPORTS POUR LA CRUE " + (ic + 1);
          descriptionLaCrue[1] = descriptionChampLaCrue;
          descriptionLesCrues[ic] = descriptionLaCrue;
        }
      }
      descriptionChampCrues[1] = descriptionLesCrues;
      descriptionCrues[1] = descriptionChampCrues;
    }
    descriptionChamp[2] = descriptionCrues;

    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  /**
   * Les parametres pour la qualit� d'eau (Tracer).
   *
   * @param fcas Writer Le fichier cas.
   * @param params SParametresTraceur Les parametres de la qualit� d'eau.
   * @throws Exception
   */
  private final static void ecritParametresTracer(Writer fcas,
          SParametresTraceur params)
          throws Exception {

    EdamoxWriter ecritureParametres = new EdamoxWriter(fcas);
    ecritureParametres.setParam(params);
    ecritureParametres.setNomRubrique("OPTION TRACER");


    Object[] descriptionChamp = new Object[9];

    descriptionChamp[0] = "PRESENCE DE TRACEURS";
    descriptionChamp[1] = "NOMBRE DE TRACEURS";

    // Description sous rubrique  convection et diffusion
    Object[] descriptionConvecDiffu = new Object[2];
    if (params.parametresConvecDiffu != null) {
      descriptionConvecDiffu[0] = "CONVECTION - DIFFUSION  (2)";
      Object[] descriptionChampConvecDiffu = new Object[9];
      descriptionChampConvecDiffu[0] = "CONVECTION DES TRACEURS";
      descriptionChampConvecDiffu[1] = "OPTION DE CONVECTION POUR LES TRACEURS";
      descriptionChampConvecDiffu[2] = "ORDRE DU SCHEMA DE CONVECTION VOLUMES FINIS";
      descriptionChampConvecDiffu[3] = "PARAMETRE W DU SCHEMA DE CONVECTION VOLUMES FINIS";
      descriptionChampConvecDiffu[4] = "LIMITEUR DE PENTE DU SCHEMA VOLUMES FINIS";
      descriptionChampConvecDiffu[5] = "DIFFUSION DES TRACEURS";
      descriptionChampConvecDiffu[6] = "OPTION DE CALCUL DE LA DISPERSION POUR LES TRACEURS";
      descriptionChampConvecDiffu[7] = "COEFFICIENT DE DIFFUSION 1 POUR LES TRACEURS";
      descriptionChampConvecDiffu[8] = "COEFFICIENT DE DIFFUSION 2 POUR LES TRACEURS";
      descriptionConvecDiffu[1] = descriptionChampConvecDiffu;
    }
    descriptionChamp[2] = descriptionConvecDiffu;

    // Description sous rubrique qualit� d'eau
    Object[] descriptionQaliteEau = new Object[2];
    if (params.parametresNumQualiteEau != null) {
      descriptionQaliteEau[0] = "QUALITE D'EAU  (2)";
      Object[] descriptionChampQaliteEau = new Object[4];
      descriptionChampQaliteEau[0] = "MODELE DE QUALITE D'EAU";
      descriptionChampQaliteEau[1] = "FICHIER DES PARAMETRES PHYSIQUES TRACER";
      descriptionChampQaliteEau[2] = "FICHIER DES DONNEES METEO TRACER";
      descriptionChampQaliteEau[3] = "FREQUENCE DE COUPLAGE ENTRE HYDRAULIQUE ET TRACER";
      descriptionQaliteEau[1] = descriptionChampQaliteEau;
    }
    descriptionChamp[3] = descriptionQaliteEau;

    // Description sous rubrique impression et resultat
    Object[] descriptionImpressEtResult = new Object[2];
    if (params.parametresImpressTracer != null) {
      descriptionImpressEtResult[0] = "IMPRESSION - RESULTATS (2)";
      Object[] descriptionChampImpressEtResult = new Object[7];
      descriptionChampImpressEtResult[0] = "FICHIER LISTING TRACER";
      descriptionChampImpressEtResult[1] = "IMPRESSION DES CONCENTRATIONS INITIALES";
      descriptionChampImpressEtResult[2] = "IMPRESSION DES LOIS TRACER";
      descriptionChampImpressEtResult[3] = "IMPRESSION DES CONCENTRATIONS SUR LE LISTING";
      descriptionChampImpressEtResult[4] = "IMPRESSION DU BILAN TRACER SUR LE LISTING";
      descriptionChampImpressEtResult[5] = "FICHIER RESULTATS TRACER";
      descriptionChampImpressEtResult[6] = "POST-PROCESSEUR TRACER";
      descriptionImpressEtResult[1] = descriptionChampImpressEtResult;
    }
    descriptionChamp[4] = descriptionImpressEtResult;

    // Description sous rubrique conditions limites
    Object[] descriptionCondLim = new Object[2];
    if (params.parametresCondLimTracer != null) {
      descriptionCondLim[0] = "CONDITIONS AUX LIMITES (2)";
      Object[] descriptionChampCondLim = new Object[2];
      descriptionChampCondLim[0] = "TYPE DE CONDITIONS LIMITES TRACER";
      descriptionChampCondLim[1] = "NUMERO DES LOIS TRACER POUR LES CL";
      descriptionCondLim[1] = descriptionChampCondLim;
    }
    descriptionChamp[5] = descriptionCondLim;

    // Description sous rubrique concentrations initiales
    Object[] descriptionConcInit = new Object[2];
    if (params.parametresConcInitTracer != null) {
      descriptionConcInit[0] = "CONDITIONS INITIALES (2)";
      Object[] descriptionChampConcInit = new Object[7];
      descriptionChampConcInit[0] = "PRESENCE CONCENTRATIONS INITIALES";
      descriptionChampConcInit[1] = "MODE D'ENTREE DES CONCENTRATIONS INITIALES";
      descriptionChampConcInit[2] = "FICHIER DES CONCENTRATIONS INITIALES";
      descriptionChampConcInit[3] = "NOMBRE DE POINTS DECRIVANT LES CONC INITIALES";
      descriptionChampConcInit[4] = "BRANCHE DES CONCENTRATIONS INITIALES";
      descriptionChampConcInit[5] = "ABSCISSE DES CONCENTRATIONS INITIALES";

      // Tableau des concentrations
      int nbTraceurs = params.nbTraceur;// parametresConcInitTracer.concentrations[0].concentrations.length;
      Object[] descriptionLesConcentrations = new Object[nbTraceurs];

      for (int i = 0; i < nbTraceurs; i++) {
        Object[] descriptionLaConcentration = new Object[2];
        descriptionLaConcentration[0] = "Traceur " + (i + 1) + " (3)";

        Object[] descriptionChampLaConcentration = new Object[1];
        descriptionChampLaConcentration[0] = "CONCENTRATIONS INITIALES TRACEUR " + (i + 1);

        descriptionLaConcentration = descriptionChampLaConcentration;
        descriptionLesConcentrations[i] = descriptionLaConcentration;
      }

      descriptionChampConcInit[6] = descriptionLesConcentrations;
      descriptionConcInit[1] = descriptionChampConcInit;
    }
    descriptionChamp[6] = descriptionConcInit;

    // Description sous rubrique sources
    Object[] descriptionSources = new Object[2];
    if (params.parametresSourcesTraceurs != null) {
      descriptionSources[0] = "SOURCES (2)";
      Object[] descriptionChampSources = new Object[7];
      descriptionChampSources[0] = "NOMBRE DE SOURCES DE TRACEURS";
      descriptionChampSources[1] = "NOM DES SOURCES";
      descriptionChampSources[2] = "TYPE DES SOURCES";
      descriptionChampSources[3] = "BRANCHE DES SOURCES";
      descriptionChampSources[4] = "ABSCISSE DES SOURCES";
      descriptionChampSources[5] = "LONGUEUR DES SOURCES";
      descriptionChampSources[6] = "NUMERO DES LOIS TRACER POUR LES SOURCES";
      descriptionSources[1] = descriptionChampSources;
    }
    descriptionChamp[7] = descriptionSources;

    // Description sous rubrique des lois tracer
    Object[] descriptionLoisTracer = new Object[2];
    if (params.parametresLoisTracer != null) {
      descriptionLoisTracer[0] = "LOIS TRACER (2)";
      Object[] descriptionChampLoisTracer = new Object[2];
      descriptionChampLoisTracer[0] = "NOMBRE DE LOIS TRACER";



      // Tableau des Lois traceurs
      Object[] descriptionLesLoisTracer = new Object[params.parametresLoisTracer.nbLoisTracer];
      for (int j = 0; j < params.parametresLoisTracer.nbLoisTracer; j++) {
        Object[] descriptionLoiTracer = new Object[2];
        descriptionLoiTracer[0] = "Loi Tracer " + (j + 1) + " (3)";

        Object[] descriptionChampLoiTracer = new Object[7];
        descriptionChampLoiTracer[0] = "LOI TRACER " + (j + 1) + " NOM";
        descriptionChampLoiTracer[1] = "LOI TRACER " + (j + 1) + " MODE D'ENTREE";
        descriptionChampLoiTracer[2] = "LOI TRACER " + (j + 1) + " FICHIER ";
        descriptionChampLoiTracer[3] = "LOI TRACER " + (j + 1) + " UNITE DE TEMPS ";
        descriptionChampLoiTracer[4] = "LOI TRACER " + (j + 1) + " NOMBRE DE POINTS";
        descriptionChampLoiTracer[5] = "LOI TRACER " + (j + 1) + " TEMPS";


        // Tableau des concentrations
        int nbTraceurs = params.nbTraceur;
        Object[] descriptionLesConcentrations = new Object[nbTraceurs];

        for (int t = 0; t < nbTraceurs; t++) {
          Object[] descriptionLaConcentration = new Object[2];
          descriptionLaConcentration[0] = "Traceur " + (t + 1) + " (4)";

          Object[] descriptionChampLaConcentration = new Object[1];
          descriptionChampLaConcentration[0] = "LOI TRACER " + (j + 1) + " CONCENTRATION TRACEUR " + (t + 1);


          descriptionLaConcentration = descriptionChampLaConcentration;
          descriptionLesConcentrations[t] = descriptionLaConcentration;
        }


        descriptionChampLoiTracer[6] = descriptionLesConcentrations;


        descriptionLoiTracer[1] = descriptionChampLoiTracer;
        descriptionLesLoisTracer[j] = descriptionLoiTracer;
      }



      descriptionChampLoisTracer[1] = descriptionLesLoisTracer;
      descriptionLoisTracer[1] = descriptionChampLoisTracer;
    }
    descriptionChamp[8] = descriptionLoisTracer;

    ecritureParametres.setDescriptionChamp(descriptionChamp);
    ecritureParametres.writeRubrique();
  }

  //******************* .REP ***********************
  @Override
  public SParametresREP parametresREP() {
    return paramsREP_;
  }

  @Override
  public void parametresREP(SParametresREP _paramsREP) {
    paramsREP_ = _paramsREP;
  }

  @Override
  public SParametresTailleMaxFichier parametresTailleMaxFichier() {
    return paramsTailleMaxFichier_;
  }

  @Override
  public void parametresTailleMaxFichier(SParametresTailleMaxFichier _parametresTailleMaxFichier) {
    paramsTailleMaxFichier_ = _parametresTailleMaxFichier;
  }

  static void ecritFichierTexte(File fichier, String contenu)
          throws IOException {
    ecritFichier(fichier, contenu.getBytes());
  }

  static void ecritFichier(File fichier, byte[] contenu) throws IOException {
    System.out.println("Creation du fichier " + fichier.getName());
    FileOutputStream f = new FileOutputStream(fichier);
    f.write(contenu);
    f.close();
  }

  public static void ecritParametresREP(File fichier, SParametresREP params) {
    try {
      ecritFichier(fichier, params.contenu);
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      throw new RuntimeException("Probl�me lors de la g�n�ration du fichier de reprise en lecture\n");
    }
  }

  public static void ecritParametresREPSansLigInit(
          File fichier,
          SParametresREP params) {
    try {
      String chaine = new String(params.contenu);
      int index = chaine.indexOf("FIN"); // le temps final se trouve apr�s FIN
      String chaineSansLigneInitiale = chaine.substring(index + 3);
      ecritFichierTexte(fichier, chaineSansLigneInitiale);
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      throw new RuntimeException("Probl�me lors de la g�n�ration du fichier de reprise en lecture\n");
    }
  }

  public static SParametresREP litParametresREP(File fichier, double tailleMax)
          throws IOException {
    SParametresREP results = new SParametresREP();
    try {
      results.contenu = CtuluLibFile.litFichier(fichier, tailleMax, false);
    } catch (IOException e) {
      if (e.getMessage().lastIndexOf("Le fichier sp�cifi� est introuvable")
              != -1) {
        results = null;
      } else {
        throw new IOException(e.getMessage());
      }
    }
    return results;
  }

  /**
   * Lecture d'un fichier CAS Mascaret et remplissage structure.
   *
   * @param _fcas Le fichier cas
   * @param _fdico Le fichier dictionnaire
   * @param _bnoyau52 Lecture suivant le format 5.2
   * @return La structure contenant les parametres du CAS.
   */
  public static SParametresCAS litParametresCAS(File _fcas, File _fdico, boolean _bnoyau52) throws FichierMascaretException {
    EdamoxReader reader = new EdamoxReader(_fcas, _fdico, _bnoyau52);
    reader.load();

    SParametresCAS params = new SParametresCAS();

    // Parametres g�n�raux
    params.parametresGen = reader.litParametresGen();
    boolean bcasiers = params.parametresGen.presenceCasiers;
//    if (params.parametresModelPhy != null)
    params.parametresModelPhy = reader.litParametresModelPhys();
//    if (params.parametresNum != null)
    params.parametresNum = reader.litParametresNum(bcasiers);
//    if (params.parametresTemp != null)
    params.parametresTemp = reader.litParametresTemp();
//    if (params.parametresGeoReseau != null)
    params.parametresGeoReseau = reader.litParametresGeoReseau();
//    if (params.parametresConfluents != null)
    params.parametresConfluents = reader.litParametresConfluents();
//    if (params.parametresPlanimMaillage != null)
    params.parametresPlanimMaillage = reader.litParametresPlanimMaillage();
//    if (params.parametresSingularite != null)
    params.parametresSingularite = reader.litParametresSingularite();
//    if (params.parametresCasier != null)
    params.parametresCasier = reader.litParametresCasier();
//    if (params.parametresApporDeversoirs != null)
    params.parametresApporDeversoirs = reader.litParametresApporDeversoirs();
//    if (params.parametresCalage != null)
    params.parametresCalage = reader.litParametresCalage();
//    if (params.parametresCalageAuto != null)
    params.parametresCalageAuto = reader.litParametresCalageAuto();
//    if (params.parametresTracer != null)
    params.parametresTracer = reader.litParametresTracer(params.parametresGeoReseau.extrLibres.nb);
//    if (params.parametresLoisHydrau != null)
    params.parametresLoisHydrau = reader.litParametresLoisHydrau();
//    if (params.parametresCondInit != null)
    params.parametresCondInit = reader.litParametresCondInit();
//    if (params.parametresImpressResult != null)
    params.parametresImpressResult = reader.litParametresImpressResult();
//    if (params.parametresVarCalc != null)
    params.parametresVarCalc = reader.litParametresVarCalc();
//    if (params.parametresVarStock != null)
    params.parametresVarStock = reader.litParametresVarStock();
    return params;
  }

  /**
   * Lecture d'un fichier CAS Mascaret et remplissage structure.
   *
   * @param _fcas Le fichier cas
   * @param _fdico Le fichier dictionnaire
   * @param _bnoyau52 Lecture suivant le format 5.2
   * @return La structure contenant les parametres du CAS.
   */
  public static CtuluIOResult<EdamoxXMLContent> litParametresXCAS(File _fcas) {
    EdamoxXmlReaderWriter reader = EdamoxXmlReaderWriter.findReader(_fcas);
    return reader.readXML(_fcas);
  }

  /**
   * Retourne les mots cl�s inconnus lus dans le fichier cas Mascaret.
   */
  public String[] getMotClesInconnus() {
    return new String[0];
  }

  /**
   * Retourne les mots cl�s ignor�s lus dans le fichier cas Mascaret.
   */
  public String[] getMotClesIgnores() {
    return new String[0];
  }

  //******************* .LIG ***********************
  @Override
  public SResultatsTemporelSpatial parametresLigneDEauInitiale() {
    return paramsLigneEauInitiale_;
  }

  @Override
  public void parametresLigneDEauInitiale(SResultatsTemporelSpatial _paramsLigneEauInitiale) {
    paramsLigneEauInitiale_ = _paramsLigneEauInitiale;
  }

  public final static void ecritParametresLigneEauInitiale(
          File fichier,
          SResultatsTemporelSpatial params) {
    try {
      DResultatsMascaret.ecritResultatsRUB(fichier, params, true);
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      throw new RuntimeException("Probl�me lors de la g�n�ration du fichier ligne d'eau initiale \n");
    }
  }

  //******************* .GEO ***********************
  @Override
  public SParametresGEO parametresGEO() {
    return paramsGEO_;
  }

  @Override
  public void parametresGEO(SParametresGEO _paramsGEO) {
    paramsGEO_ = _paramsGEO;
  }

  private final static void ecritParametresProfil(
          FortranWriter fgeo,
          SParametresProfil profil,
          String nomBief,
          int indBief,
          int indProfil,
          boolean ecrireGeoReferencement)
          throws IOException {
    String nomProfil = profil.nom;
    if (nomProfil == null) {
      nomProfil = "prof" + (indBief + 1) + "_" + (indProfil + 1);
    } else if (nomProfil.equals("")) {
      nomProfil = "prof" + (indBief + 1) + "_" + (indProfil + 1);
    }
    if (profil.infoGeoReferencement != null && !profil.infoGeoReferencement.equals("")) {
      fgeo.writeln("PROFIL " + nomBief + " " + nomProfil + " " + profil.absc + " " + profil.infoGeoReferencement);
    } else {
      fgeo.writeln("PROFIL " + nomBief + " " + nomProfil + " " + profil.absc);
    }
    if (ecrireGeoReferencement && profil.avecGeoReferencement) {
      for (int i = 0; i < profil.pts.length; i++) {
        fgeo.writeln(
                profil.pts[i].x + " " + profil.pts[i].y + " " + profil.pts[i].lit + " " + profil.pts[i].cx + " " + profil.pts[i].cy);
      }
    } else {
      System.err.println("sans georef");
      for (int i = 0; i < profil.pts.length; i++) {
        fgeo.writeln(
                profil.pts[i].x + " " + profil.pts[i].y + " " + profil.pts[i].lit);
      }

    }
  }

  private final static void ecritParametresBief(
          FortranWriter fgeo,
          SParametresBief bief,
          int indBief,
          boolean ecrireGeoReferencement)
          throws IOException {
    String nomBief = bief.nom;
    if (nomBief == null) {
      nomBief = "bief" + (indBief + 1);
    } else if (nomBief.equals("")) {
      nomBief = "bief" + (indBief + 1);
    }
    for (int i = 0; i < bief.profils.length; i++) {
      ecritParametresProfil(fgeo, bief.profils[i], nomBief, indBief, i, ecrireGeoReferencement);
    }
  }

  /**
   * Lit un fichier de geometrie de profils et le transfert en structures.
   *
   * @param filename Le fichier sur lequel lire.
   * @return La structure contenant les profils.
   * @throws IOException
   */
  public static SParametresGEO litParametresGEO(File filename) throws FichierMascaretException {
    SParametresGEO params = new SParametresGEO();

    Vector vbiefs = new Vector();
    Vector vprofils = new Vector();
    Vector vptsProfil = new Vector();
    SParametresProfil nouveauProfil = null;
    SParametresBief nouveauBief = null;
    String etat = "debut";
    String nomPremierBief = null;

    int lineNb = 0;
    String line = null;

    try {
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      // debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if ((line.equals("")) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        StringTokenizer st = new StringTokenizer(line);
        String mot1 = st.nextToken();
        String mot2 = st.nextToken();
        if (etat.equals("debut")) {
          if (mot1.equalsIgnoreCase("profil")) {
            // biefs[iProfil].vprofils(new MetierProfil[0]);
            nomPremierBief = mot2;
            nouveauBief = new SParametresBief();
            nouveauBief.nom = nomPremierBief;
            vbiefs.add(nouveauBief);

            nouveauProfil = mascaretEnteteProfil(line);
            vprofils.add(nouveauProfil);
//            vptsProfil=new Vector();
            etat = "entete";
          } else {
            throw new FichierMascaretException(lineNb, line, "debut du fichier incorrect'");
          }
        } else if (etat.equals("entete")) {
          if (mot1.equalsIgnoreCase("profil")) {
            throw new FichierMascaretException(lineNb, line, "profil vide");
//            logMsg+="profil vide: '"+line+"' (ligne "+lineNb+")\n";
//            break;
          } /* else { */
          boolean avecGeoRefrencement = mascaretPointProfil(line, vptsProfil);
          //sI UN SEUL POINT POSSEDE UN GEOREFERENCEMENT ALORS LE PROFILE EST DECLARE GEOREFERENCE
          if (avecGeoRefrencement) {
            nouveauProfil.avecGeoReferencement = true;
          }
          etat = "points";
          // }
        } else if (etat.equals("points")) {

          if (mot1.equalsIgnoreCase("profil")) {
            nouveauProfil.pts = (SParametresPt[]) vptsProfil.toArray(new SParametresPt[0]);
            vptsProfil.clear();

            // Fin du profil, meme bief.
            if (mot2.equals(nomPremierBief)) {
//              mascaretInitIndiceLit(nouveauProfil, ptsProfil);
              nouveauProfil = mascaretEnteteProfil(line);
              vprofils.add(nouveauProfil);
//              ptsProfil=new Vector();
              etat = "entete";
            } // changement de bief
            else {
//              iProfil++;
//              if (biefs.length<=iProfil) {
//                throw new IOException("Trop de biefs dans la g�om�trie : '"+line+"' (ligne "+lineNb+")");
//                logMsg+="Trop de biefs dans la g�om�trie: '"+line+"' (ligne "+lineNb+")\n";
//                break;
//              }
//              biefs[iProfil].vprofils(new MetierProfil[0]);+

              nouveauBief.profils = (SParametresProfil[]) vprofils.toArray(new SParametresProfil[0]);
              vprofils.clear();

              nomPremierBief = mot2;
              nouveauBief = new SParametresBief();
              nouveauBief.nom = nomPremierBief;
              vbiefs.add(nouveauBief);
//              mascaretInitIndiceLit(nouveauProfil, ptsProfil);

              nouveauProfil = mascaretEnteteProfil(line);
              vprofils.add(nouveauProfil);
//              ptsProfil=new Vector();
              etat = "entete";
            }
          } else {
            mascaretPointProfil(line, vptsProfil);
            // etat="points";
          }
        } // fin test etat
      } // fin while( (line=fic.readLine())!=null )
      fic.close();

      nouveauProfil.pts = (SParametresPt[]) vptsProfil.toArray(new SParametresPt[0]);
      nouveauBief.profils = (SParametresProfil[]) vprofils.toArray(new SParametresProfil[0]);

//      mascaretInitIndiceLit(nouveauProfil, ptsProfil);
//      profils.add(nouveauProfil);
//      System.err.println("OK");
//      logMsg+="OK\n";
    } catch (NoSuchElementException e) {
      throw new FichierMascaretException(lineNb, line, "il manque un champ");
//      logMsg+="il manque un champ dans la ligne numero "+lineNb+" '"+line+"'\n";
    } catch (NumberFormatException e) {
      throw new FichierMascaretException(lineNb, line, "format d'un champ incorrect");
//      logMsg+="format d'un champ incorrecte dans la ligne numero "+lineNb+" '"+line+"'\n";
    } catch (IOException e) {
      throw new FichierMascaretException(lineNb, line, e.getMessage());
    }
//    new BuDialogMessage((BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
//        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME).getInformationsSoftware(), logMsg).activate();
    params.biefs = (SParametresBief[]) vbiefs.toArray(new SParametresBief[0]);
    return params;
  }

  private static SParametresProfil mascaretEnteteProfil(String line) throws NoSuchElementException, NumberFormatException {
    StringTokenizer st = new StringTokenizer(line);
    SParametresProfil res = null;
    /* String motCle = */
    st.nextToken();
    /* String nomBief = */
    st.nextToken();
    String nomProfil = st.nextToken();
    double abscisse = Double.parseDouble(st.nextToken());
    res = new SParametresProfil();
    res.nom = nomProfil;
    res.absc = abscisse;

    String infoGeoRef = "";
    try {
      while (st.hasMoreTokens()) {  //boucle de lecture
        infoGeoRef = infoGeoRef + " " + st.nextToken();
      }
    } catch (NoSuchElementException e) {
    } finally {
      //System.err.println(" infoGeoRef"+infoGeoRef);
      res.infoGeoReferencement = infoGeoRef;
      return res;
    }
  }

  private static boolean mascaretPointProfil(String line, Vector ptsProfil) throws NoSuchElementException, NumberFormatException {

    SParametresPt pt;
    boolean avecGeoReferencement = false;
    StringTokenizer st = new StringTokenizer(line);
    double x = Double.parseDouble(st.nextToken());
    double y = Double.parseDouble(st.nextToken());
    String lit = st.nextToken();
    double cx = 0;
    double cy = 0;
    if (st.hasMoreTokens()) {
      try {
        cx = Double.parseDouble(st.nextToken());
        cy = Double.parseDouble(st.nextToken());
        avecGeoReferencement = true;
      } catch (NoSuchElementException e) {
        avecGeoReferencement = false;
      }
    }

    if (avecGeoReferencement) {
      pt = new SParametresPt(x, y, cx, cy, lit);
    } else {
      pt = new SParametresPt(x, y, 0, 0, lit);
    }

    if ((!lit.equals("B")) && (!lit.equals("T"))) {
      throw new NumberFormatException(lit);
    }

    ptsProfil.add(pt);

    return avecGeoReferencement;
  }

  public final static void ecritParametresGEO(
          File fichier,
          SParametresGEO params,
          boolean ecrireGeoReferencement) {
    try {
      FortranWriter fgeo = new FortranWriter(new FileWriter(fichier));
      System.out.println("Creation du fichier " + fichier.getName());
      for (int i = 0; i < params.biefs.length; i++) {
        ecritParametresBief(fgeo, params.biefs[i], i, ecrireGeoReferencement);
      }
      fgeo.close();
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      throw new RuntimeException("Probl�me lors de la g�n�ration du fichier geom�trie (profils)\n");
    }
  }
  //******************* G�om�trie casier ***********************
  // Fichier geometrie contenant la g�m�trie des casiers

  @Override
  public SGeoCasiers casierGEO() {
    return casierGEO_;
  }
  // Fichier geometrie contenant la g�m�trie des casiers

  @Override
  public void casierGEO(SGeoCasiers _casierGEO) {
    casierGEO_ = _casierGEO;
  }

  private final static void ecritParametresCasier(
          FortranWriter fgeo,
          SGeoCasier casier,
          int indCasier)
          throws IOException {
    String nomCasier = casier.nom;
    if (nomCasier == null) {
      nomCasier = "casier" + (indCasier + 1);
    } else if (nomCasier.equals("")) {
      nomCasier = "casier" + (indCasier + 1);
    }
    fgeo.writeln("CASIER " + nomCasier);
    for (int i = 0; i < casier.pts.length; i++) {
      if (casier.pts[i].typePoint == null) {
        fgeo.writeln(
                casier.pts[i].x + " " + casier.pts[i].y + " " + casier.pts[i].z);
      } else {
        fgeo.writeln(
                casier.pts[i].x
                + " "
                + casier.pts[i].y
                + " "
                + casier.pts[i].z
                + " "
                + casier.pts[i].typePoint);
      }
    }
  }

  public final static void ecritCasierGEO(File fichier, SGeoCasiers params) {
    try {
      FortranWriter fgeo = new FortranWriter(new FileWriter(fichier));
      System.out.println("Creation du fichier " + fichier.getName());
      for (int i = 0; i < params.casiers.length; i++) {
        ecritParametresCasier(fgeo, params.casiers[i], i);
      }
      fgeo.close();
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      throw new RuntimeException("Probl�me lors de la g�n�ration du fichier de geom�trie Casier\n");
    }
  }
  //******************* Fichiercas.txt ***********************

  @Override
  public SParametresNCA parametresNCA() {
    return paramsNCA_;
  }

  @Override
  public void parametresNCA(SParametresNCA _paramsNCA) {
    paramsNCA_ = _paramsNCA;
  }

  public final static void ecritParametresNCA(
          File fichier,
          SParametresNCA params) {
    try {
      FortranWriter fnca = new FortranWriter(new FileWriter(fichier));
      System.out.println("Creation du fichier " + fichier.getName());
      fnca.setStringQuoted(true);
      fnca.stringField(0, params.nom);
      fnca.writeFields();
      fnca.close();
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      throw new RuntimeException("Probl�me lors de la g�n�ration du fichier FichierCas.txt\n");
    }
  }
  //******************* Lois hydrauliques ***********************

  @Override
  public SLoiHydraulique[] loisHydrauliques() {
    return loisHydrauliques_;
  }

  @Override
  public void loisHydrauliques(SLoiHydraulique[] _loisHydrauliques) {
    loisHydrauliques_ = _loisHydrauliques;
  }

  public final static void ecritLoisHydrauliques(
          String nomFichierBase,
          boolean deleteOnExit,
          SLoiHydraulique[] params) {
    int nbLoi = params.length;
    File[] fichiersLois = new File[nbLoi];
    for (int i = 0; i < nbLoi; i++) {
      fichiersLois[i] = new File(nomFichierBase + "_" + i + ".loi");
      if (deleteOnExit) {
        fichiersLois[i].deleteOnExit();
      }
      ecritLoiHydraulique(fichiersLois[i], params[i]);
    }
  }

  public final static void ecritLoiHydraulique(
          File fichier,
          SLoiHydraulique params) {
    try {
      PrintWriter fcas = new PrintWriter(new FileWriter(fichier));
      System.out.println("Creation du fichier " + fichier.getName());
      fcas.println("# " + params.nom);
      int nbColonne = params.entetesColonnes.length;
      fcas.print("#");
      for (int i = 0; i < nbColonne; i++) {
        fcas.print(" " + params.entetesColonnes[i]);
      }
      fcas.println();
      if ((params.unitee != null) && (!params.unitee.equals(""))) {
        fcas.println("         " + params.unitee);
      }
      double[] x = params.x;
      double[] y = params.y;
      int nbLigne = x.length;
      if (nbColonne == 2) {
        for (int i = 0; i < nbLigne; i++) {
          fcas.println(" " + x[i] + " " + y[i]);
        }
      } else if (nbColonne == 3) {
        double[] z = params.z;
        for (int i = 0; i < nbLigne; i++) {
          fcas.println(" " + x[i] + " " + y[i] + " " + z[i]);
        }
      }
      fcas.close();
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      System.err.println("IT: " + ex);
    }
  }

//******************* Lois Tracer ***********************
  @Override
  public SLoiTracer[] loisTracer() {
    return loisTracer_;
  }

  @Override
  public void loisTracer(SLoiTracer[] _loisTracer) {
    loisTracer_ = _loisTracer;
  }

  public final static void ecritLoisTracer(
          String nomFichierBase,
          boolean deleteOnExit,
          SLoiTracer[] params) {
    int nbLoi = params.length;
    File[] fichiersLois = new File[nbLoi];
    for (int i = 0; i < nbLoi; i++) {
      fichiersLois[i] = new File(nomFichierBase + "_tracer" + i + ".loi");
      if (deleteOnExit) {
        fichiersLois[i].deleteOnExit();
      }
      ecritLoiTracer(fichiersLois[i], params[i]);
    }
  }

  public final static void ecritLoiTracer(
          File fichier,
          SLoiTracer params) {
    try {
      PrintWriter fcas = new PrintWriter(new FileWriter(fichier));
      System.out.println("Creation du fichier " + fichier.getName());
      fcas.println("# " + params.nom);
      int nbColonne = params.entetesColonnes.length;
      fcas.print("#");
      for (int i = 0; i < nbColonne; i++) {
        fcas.print(" " + params.entetesColonnes[i]);
      }
      fcas.println();
      if ((params.unitee != null) && (!params.unitee.equals(""))) {
        fcas.println("         " + params.unitee);
      }
      double[] tps = params.tps;
      double[][] conc = params.concentrations;
      int nbLigne = tps.length;
      for (int i = 0; i < nbLigne; i++) {
        StringBuffer sb = new StringBuffer(10 * (conc[0].length + 1));
        sb.append(" ");
        sb.append(tps[i]);
        for (int j = 0; j < conc[0].length; j++) {
          sb.append(" ");
          sb.append(conc[i][j]);
        }
        fcas.println(sb.toString());
      }
      fcas.close();
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      System.err.println("IT: " + ex);
    }
  }

//******************* Concentrations initiales ***********************
  @Override
  public SResultatsTemporelSpatial parametresConcentInitiales() {
    return paramsConcentInitiales_;
  }

  @Override
  public void parametresConcentInitiales(SResultatsTemporelSpatial _paramsConcentInitiales) {
    paramsConcentInitiales_ = _paramsConcentInitiales;
  }

  public final static void ecritParametresConcentInitiales(
          File fichier,
          SResultatsTemporelSpatial params) {
    try {
      DResultatsMascaret.ecritResultatsOPT(fichier, params);
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      throw new RuntimeException(
              "Probl�me lors de la g�n�ration du fichier des concentrations initiales \n");
    }
  }

//******************* Fichier meteo ***********************
  @Override
  public SParamMeteoTracer paramMeteoTracer() {
    return paramMeteoTracer_;
  }

  @Override
  public void paramMeteoTracer(SParamMeteoTracer _paramMeteoTracer) {
    paramMeteoTracer_ = _paramMeteoTracer;
  }

  public final static void ecritFichierMeteo(
          File fichier,
          SParamMeteoTracer params) {
    try {
      DParametresMascaret.ecritFichier(fichier, params.paramMeteoTracer);
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      System.err.println("IT: " + ex);
    }
  }

  //******************* Fichier Parametres physiques Tracer ***********************
  @Override
  public SParamPhysTracer[] parametresPhysModele() {
    return parametresPhysModele_;
  }

  @Override
  public void parametresPhysModele(SParamPhysTracer[] _parametresPhysModele) {
    parametresPhysModele_ = _parametresPhysModele;
  }

  public final static void ecritParamsPhysiquesTracer(
          File fichier,
          SParamPhysTracer[] params) {
    try {
      if (params != null) {//TODO correction Fred: que faire si null
        PrintWriter fcas = new PrintWriter(new FileWriter(fichier));
        System.out.println("Creation du fichier " + fichier.getName());
        int nbParams = params.length;
        fcas.println(nbParams + " : NOMBRE DE PARAMETRES PHYSIQUES");
        for (int i = 0; i < nbParams; i++) {

          fcas.println(params[i].valeurParamPhys + " : " + params[i].nomParamPhys);
        }
        fcas.close();
      }
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      System.err.println("IT: " + ex);
    }
  }
}
