/**
 * @file         DResultatsMascaret.java
 * @creation     2000-05-10
 * @modification $Date: 2007-11-20 11:42:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.corba.mascaret.IResultatsMascaret;
import org.fudaa.dodico.corba.mascaret.IResultatsMascaretOperations;
import org.fudaa.dodico.corba.mascaret.SParametresREP;
import org.fudaa.dodico.corba.mascaret.SResultatBief;
import org.fudaa.dodico.corba.mascaret.SResultatsEcran;
import org.fudaa.dodico.corba.mascaret.SResultatsLIS;
import org.fudaa.dodico.corba.mascaret.SResultatsOPT;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
/**
 * Classe qui impl�mente cot� serveur l\u2019interface IResultatsMascaret qui contient
 * les r�sultats du mod�le Mascaret.
 *
 * @version      $Revision: 1.22 $ $Date: 2007-11-20 11:42:57 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe , Axel von Arnim
 */
public class DResultatsMascaret
  extends DResultats
  implements IResultatsMascaret,IResultatsMascaretOperations {
  public final static int RUBENS=0;
  public final static int OPTYCA=1;
  //private static boolean DEBUG= "yes".equals(System.getProperty("fudaa.debug"));
  private SResultatsLIS resultsLIS_;
  private SResultatsLIS resultsDAMOC_;
  private SResultatsLIS resultsCalLIS_;
  private SResultatsTemporelSpatial resultsCal_;
  private SResultatsLIS  resultatsTracerLIS_;
  private SResultatsTemporelSpatial resultatsTracer_;
//  private SResultatsOPT resultsOPT_;
  private SResultatsTemporelSpatial resultatsTemporelSpatial_;
//  private SResultatsRUB resultsRUB_;
  private SResultatsEcran resultsEcran_;
  private SResultatsEcran resultsEcranErreur_;
  private SParametresREP resultsREP_;
  // pour casier
  private SResultatsTemporelSpatial resultsCasier_;
  private SResultatsTemporelSpatial resultsLiaison_;
//  private SResultatsOPT resultsCasierOPT_;
//  private SResultatsRUB resultsCasierRUB_;
//  private SResultatsOPT resultsLiaisonOPT_;
//  private SResultatsRUB resultsLiaisonRUB_;
  private SResultatsLIS listingCasier_;
  private SResultatsLIS listingLiaison_;
  public DResultatsMascaret() {
    super();
  }
  @Override
  public final Object clone() throws CloneNotSupportedException{
    return new DResultatsMascaret();
  }
  @Override
  public String toString() {
    return "string Resultat works\n";
  }

  //************************ LIS ***********************************
  @Override
  public SResultatsLIS resultatsLIS() {
    return resultsLIS_;
  }
  @Override
  public void resultatsLIS(SResultatsLIS _resultsLIS) {
    resultsLIS_= _resultsLIS;
  }
  public static SResultatsLIS litResultatsLIS(File fichier, double tailleMax)
    throws IOException {
    SResultatsLIS results= new SResultatsLIS();
    results.contenu= CtuluLibFile.litFichier(fichier, tailleMax, true);
    return results;
  }
  public static void ecritResultatsLIS(File fichier, SResultatsLIS res)
    throws IOException {
    DParametresMascaret.ecritFichier(fichier, res.contenu);
  }
  //************************ LISTING CALAGE ***********************************
  @Override
  public SResultatsLIS resultatsCalAutoLIS() {
    return resultsCalLIS_;
  }
  @Override
  public void resultatsCalAutoLIS(SResultatsLIS _resultsCalLIS) {
    resultsCalLIS_= _resultsCalLIS;
  }
  public static SResultatsLIS litResultatsCalAutoLIS(File fichier, double tailleMax)
    throws IOException {
    SResultatsLIS results= new SResultatsLIS();
    results.contenu= CtuluLibFile.litFichier(fichier, tailleMax, true);
    return results;
  }
  public static void ecritResultatsCalLIS(File fichier, SResultatsLIS res)
    throws IOException {
    DParametresMascaret.ecritFichier(fichier, res.contenu);
  }
  //******************** RESULTATS CALAGE ***************************************
  @Override
  public SResultatsTemporelSpatial resultatsCalAuto() {
    return resultsCal_;
  }
  @Override
  public void resultatsCalAuto(SResultatsTemporelSpatial _resultsCal) {
    resultsCal_= _resultsCal;
  }
  public static SResultatsTemporelSpatial litResultatsCalAuto(File fichier, double tailleMax, int format)
  throws FichierMascaretException {
	if(format ==OPTYCA)  
		return litResultatsCalAutoOptyca(fichier,tailleMax,format);
		else
    return litResultatsTemporelSpatial(fichier,tailleMax,format);
  }
  //************************ LISTING TRACER ***********************************
  @Override
  public SResultatsLIS resultatsTracerLIS() {
  return resultatsTracerLIS_;
}
  @Override
  public void resultatsTracerLIS(SResultatsLIS _resultatsTracerLIS) {
  resultatsTracerLIS_= _resultatsTracerLIS;
}
public static SResultatsLIS litResultatsTracerLIS(File fichier, double tailleMax)
  throws IOException {
  SResultatsLIS results= new SResultatsLIS();
  results.contenu= CtuluLibFile.litFichier(fichier, tailleMax, true);
  return results;
}
public static void ecritResultatsTracerLIS(File fichier, SResultatsLIS res)
  throws IOException {
  DParametresMascaret.ecritFichier(fichier, res.contenu);
}
//******************** RESULTATS TRACER ***************************************
  @Override
  public SResultatsTemporelSpatial resultatsTracer() {
  return resultatsTracer_;
}
  @Override
  public void resultatsTracer(SResultatsTemporelSpatial _resultatsTracer) {
  resultatsTracer_= _resultatsTracer;
}
public static SResultatsTemporelSpatial litResultatsTracer(File fichier, double tailleMax, int format)
throws FichierMascaretException {
  return litResultatsTemporelSpatial(fichier,tailleMax,format);
}

  //************************ LISTING DAMOCLES ***********************************
  @Override
  public SResultatsLIS resultatsDAMOC() {
    return resultsDAMOC_;
  }
  @Override
  public void resultatsDAMOC(SResultatsLIS _resultsDAMOC) {
    resultsDAMOC_= _resultsDAMOC;
  }
  public static SResultatsLIS litResultatsDAMOC(File fichier, double tailleMax)
    throws IOException {
    SResultatsLIS results= new SResultatsLIS();
    results.contenu= CtuluLibFile.litFichier(fichier, tailleMax, true);
    return results;
  }
  public static void ecritResultatsDAMOC(File fichier, SResultatsLIS res)
    throws IOException {
    DParametresMascaret.ecritFichier(fichier, res.contenu);
  }
  public static void ecritResultats(int format, File fichier, SResultatsTemporelSpatial sres) throws FichierMascaretException {
    switch (format) {
      case RUBENS: ecritResultatsRUB(fichier, sres, false);
        break;
      case OPTYCA: ecritResultatsOPT(fichier, sres);
        break;
    }

  }
  public static void ecritResultatsRUB(File fichier, SResultatsTemporelSpatial sres, boolean entierSur5Carc) throws FichierMascaretException {
    if (!sres.resultatsPermanent) {
      Rubens1DNonPermWriter writer = new Rubens1DNonPermWriter(fichier);
      writer.write(sres);
    } else {
      Rubens1DPermWriter writer = new Rubens1DPermWriter(fichier);
      writer.write(sres, entierSur5Carc);
    }
  }
  public static void ecritResultatsOPT(File fichier, SResultatsTemporelSpatial sres) throws FichierMascaretException {
    Opthyca1DWriter writer = new Opthyca1DWriter(fichier);
    writer.write(sres);
  }
  //************************ OPT ***********************************
  public static void ecritResultatsOPT(File fichier, SResultatsOPT resultat)
    throws IOException {
    if (resultat == null)
      return;
    if (resultat.variables == null)
      return;
    if (resultat.variables.length == 0)
      return;
    try {
      PrintWriter fopt=
        new PrintWriter(new BufferedWriter(new FileWriter(fichier)));
      System.out.println("Creation du fichier " + fichier.getName());
      // entete description variables
      fopt.println("[variables]");
      // description variable
      for (int i= 0; i < resultat.variables.length; i++) {
        fopt.print("\"" + resultat.variables[i].nomLong + "\";");
        fopt.print("\"" + resultat.variables[i].nomCourt + "\";");
        fopt.print("\"" + resultat.variables[i].unite + "\";");
        fopt.println(resultat.variables[i].nbDecimal);
      }
      // entete resultat
      fopt.println("[resultats]");
      // description variable
      for (int i= 0; i < resultat.resultatsPasTemps.length; i++) {
        double tps= resultat.resultatsPasTemps[i].t;
        int numSection= 1;
        for (int j= 0;
          j < resultat.resultatsPasTemps[i].resultatsBief.length;
          j++) {
          String codeBief= " " + (j + 1);
          SResultatBief resuBief=
            resultat.resultatsPasTemps[i].resultatsBief[j];
          for (int k= 0; k < resuBief.resultatsSection.length; k++) {
            String codeSection= " " + (numSection++);
            double absc= resuBief.resultatsSection[k].absc;
            // ecriture du d�but de la ligne
            fopt.print(
              tps + ";\"" + codeBief + "\";\"" + codeSection + "\";" + absc);
            double[] vals= resuBief.resultatsSection[k].valeurs;
            // ecriture des valeurs des diff�rentes variables
            for (int l= 0; l < vals.length; l++) {
              fopt.print(";" + vals[l]);
            }
            fopt.println();
          }
        }
      }
      fopt.close();
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      System.err.println("IT: " + ex);
    }
  }



  //******************** les r�sultats temporels et spatials ******************
  @Override
  public SResultatsTemporelSpatial resultatsTemporelSpatial() {
    return resultatsTemporelSpatial_;
  }
  @Override
  public void resultatsTemporelSpatial(SResultatsTemporelSpatial _resultatsTemporelSpatial) {
    resultatsTemporelSpatial_= _resultatsTemporelSpatial;
  }
  
  
  public static SResultatsTemporelSpatial litResultatsCalAutoOptyca(File fichier, double tailleMax, int format)
     throws FichierMascaretException {
      try {
        SResultatsTemporelSpatial results = new SResultatsTemporelSpatial();
        long tailleMaxOctet = Math.round(tailleMax * 1024);
        long tailleFichier = fichier.length();
        if (tailleFichier > tailleMaxOctet) {
          System.out.println(
              "Fichier "
              + fichier.getName()
              + " trop volumineux : "
              + (tailleFichier / 1024)
              + "Ko");
          return results;
        }
        System.out.println("Lecture de " + fichier.getName());
        System.err.println("litResultatsTemporelSpatialOpthyca("+fichier+")");
        Opthyca1DReader reader = new Opthyca1DReader(fichier);
        return reader.readCalageAuto();
        
        
      }catch (OutOfMemoryError ex) {
          throw new FichierMascaretException(null, "m�moire insuffisante");
        }
        catch (FichierMascaretException ex) {
          throw new FichierMascaretException(null, ex.getMessage());
        }
        catch (Exception ex) {
          ex.printStackTrace();
          throw new FichierMascaretException(null,
                             "Probl�me lors de la lecture de "+fichier.getName());
        }
    }
  
  public static SResultatsTemporelSpatial litResultatsTemporelSpatial(File fichier, double tailleMax, int format)
     throws FichierMascaretException {
      try {
        SResultatsTemporelSpatial results = new SResultatsTemporelSpatial();
        long tailleMaxOctet = Math.round(tailleMax * 1024);
        long tailleFichier = fichier.length();
        if (tailleFichier > tailleMaxOctet) {
          System.out.println(
              "Fichier "
              + fichier.getName()
              + " trop volumineux : "
              + (tailleFichier / 1024)
              + "Ko");
          return results;
        }
        System.out.println("Lecture de " + fichier.getName());
        switch (format) {
          case RUBENS:
            return litResultatsTemporelSpatialRubens(fichier);
          case OPTYCA:
            return litResultatsTemporelSpatialOpthyca(fichier);
          default:
            throw new IllegalArgumentException("format argument :" + format);
        }
      }
      catch (OutOfMemoryError ex) {
        throw new FichierMascaretException(null, "m�moire insuffisante");
      }
      catch (FichierMascaretException ex) {
        throw new FichierMascaretException(null, ex.getMessage());
      }
      catch (Exception ex) {
        ex.printStackTrace();
        throw new FichierMascaretException(null,
                           "Probl�me lors de la lecture de "+fichier.getName());
      }
  }
  public static SResultatsTemporelSpatial litResultatsTemporelSpatialRubens(File fichier)
     throws FichierMascaretException, OutOfMemoryError {
	   Rubens1DReader reader = new Rubens1DReader(fichier);
	   return reader.read();
  }
  public static SResultatsTemporelSpatial litResultatsTemporelSpatialOpthyca(File fichier)
     throws FichierMascaretException, OutOfMemoryError {
   System.err.println("litResultatsTemporelSpatialOpthyca("+fichier+")");
    Opthyca1DReader reader = new Opthyca1DReader(fichier);
    return reader.read();
  }
  //******************** FIN les r�sultats temporels et spatials ******************
  //************************ REP ***********************************
  @Override
  public SParametresREP resultatsREP() {
    return resultsREP_;
  }
  @Override
  public void resultatsREP(SParametresREP _resultsREP) {
    resultsREP_= _resultsREP;
  }
  public static SParametresREP litResultatsREP(File fichier, double tailleMax)
    throws IOException {
    return DParametresMascaret.litParametresREP(fichier, tailleMax);
  }
  public static void ecritResultatsREP(File fichier, SParametresREP reprise)
    throws IOException {
    DParametresMascaret.ecritFichier(fichier, reprise.contenu);
  }
  //************************ ECRAN ***********************************
  @Override
  public SResultatsEcran resultatsEcran() {
    return resultsEcran_;
  }
  @Override
  public void resultatsEcran(SResultatsEcran _resultatsEcran) {
    resultsEcran_= _resultatsEcran;
  }
  public static SResultatsEcran litResultatsEcran(File fichier)
    throws IOException {
    SResultatsEcran results= new SResultatsEcran();
    results.contenu= CtuluLibFile.litFichier(fichier);
    return results;
  }
  public static void ecritResultatsEcran(File fichier, SResultatsEcran res)
    throws IOException {
    DParametresMascaret.ecritFichier(fichier, res.contenu);
  }
  //************************ ECRAN Erreur ***********************************
  @Override
  public SResultatsEcran resultatsEcranErreur() {
    return resultsEcranErreur_;
  }
  @Override
  public void resultatsEcranErreur(SResultatsEcran _resultatsEcranErreur) {
    resultsEcranErreur_= _resultatsEcranErreur;
  }
  public static SResultatsEcran litResultatsEcranErreur(File fichier)
    throws IOException {
    SResultatsEcran results= new SResultatsEcran();
    results.contenu= CtuluLibFile.litFichier(fichier);
    return results;
  }
  public static void ecritResultatsEcranErreur(File fichier, SResultatsEcran res)
    throws IOException {
    DParametresMascaret.ecritFichier(fichier, res.contenu);
  }
  // Pour le module casier
  //
  // r�sultat casier
  @Override
  public SResultatsTemporelSpatial resultatsCasier() {
    return resultsCasier_;
  }
  @Override
  public void resultatsCasier(SResultatsTemporelSpatial _resultatsCasier) {
    resultsCasier_= _resultatsCasier;
  }
  // r�sultat liaison
  @Override
  public SResultatsTemporelSpatial resultatsLiaison() {
    return resultsLiaison_;
  }
  @Override
  public void resultatsLiaison(SResultatsTemporelSpatial resultsLiaison) {
    resultsLiaison_= resultsLiaison;
  }

  // listing casier
  @Override
  public SResultatsLIS casierLIS() {
    return listingCasier_;
  }
  @Override
  public void casierLIS(SResultatsLIS _listingCasier) {
    listingCasier_= _listingCasier;
  }
  // listing liaison
  @Override
  public SResultatsLIS liaisonLIS() {
    return listingLiaison_;
  }
  @Override
  public void liaisonLIS(SResultatsLIS _listingLiaison) {
    listingLiaison_= _listingLiaison;
  }

  public static void main(String arg[]) {
    try {
      SResultatsTemporelSpatial res=
        litResultatsTemporelSpatial(
          new File("permanent7Biefs.rub"),
          Double.MAX_VALUE,RUBENS);
      for (int i= 0; i < res.variables.length; i++) {
        System.out.println(
          "res.variables[" + i + "].nomLong=" + res.variables[i].nomLong);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
