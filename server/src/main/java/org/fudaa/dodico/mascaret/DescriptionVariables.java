/*
 * @file         DescriptionVariables.java
 * @creation     2004-03-01
 * @modification $Date: 2007-11-20 11:42:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.fudaa.dodico.corba.mascaret.SResultatsVariable;
/**
 * Classe utilitaire permettant de g�rer les descriptions de variables utilis�es
 * dans les fichiers r�sultats du code de calcul en utilisant le fichier ��DescriptionVariables.properties��.
 *
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:42:58 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class DescriptionVariables {
  static final String[] VARIABLES={    // Toutes les variables existantes.
    "ZREF",
    "RGC",
    "RDC",
    "KMIN",
    "KMAJ",
    "Z",
    "QMIN",
    "QMAJ",
    "S1",
    "S2",
    "FR",
    "BETA",
    "B1",
    "B2",
    "BS",
    "P1",
    "P2",
    "RH1",
    "RH2",
    "VMIN",
    "VMAJ",
    "TAUF",
    "Y",
    "HMOY",
    "Q2G",
    "Q2D",
    "SS",
    "VOL",
    "VOLS",
    "CHAR",
    "ZMAX",
    "TZMA",
    "VZMX",
    "ZMIN",
    "TZMI",
    "VINF",
    "VSUP",
    "BMAX",
    "TOND",
    "QMAX",
    "TQMA",
    "EMAX"
  };

  public static final int MASCARET=0;
  public static final int CASIER=1;
  public static final int LIAISON=2;
  public static final int TRACER=3;
  public static final int CALAGE_AUTO=4;
  
  private static final ResourceBundle RESOURCE = ResourceBundle.getBundle("org.fudaa.dodico.mascaret.DescriptionVariables");
  
  /**
   * Retourne la description de la variable de nom court donn�es.
   * @param _nomCourt Le nom court de la variable 
   * @return La description sous forme de tableau. [0]: Nom long, [1]: Unit�, [2] : Nombre de d�cimales.
   */
  public static String[] getDescription(String _nomCourt) {
    String[] res=new String[3];
    
    String resource = null;
    try {
      resource = RESOURCE.getString(_nomCourt);
      StringTokenizer stk = new StringTokenizer(resource, ",");
      res[0]=stk.nextToken();
      res[1]=stk.nextToken();
      res[2]=stk.nextToken();
      Integer.parseInt(res[2]); // Pour �tre sur que le nombre de d�cimales est un entier.
    }
    catch (Exception ex) {
      if ("Variable".equals(res[0])) res[0]=_nomCourt;
      res[2]="3";
    }
    return res;
  }

/*  public static IDescriptionVariable[] conversion(SResultatsVariable[] descVars) {
	if (descVars==null) return null;
    IDescriptionVariable[] res= new IDescriptionVariable[descVars.length];
    for (int i= 0; i < descVars.length; i++) {
      if ("".equals(descVars[i].nomLong)) {
        res[i]=creerDescriptionVariable(descVars[i].nomCourt);
      } else {
        res[i]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
        res[i].description(descVars[i].nomLong);
        res[i].nom(descVars[i].nomCourt);
        res[i].nbDecimales(descVars[i].nbDecimal);
        String unite = descVars[i].unite;
        res[i].unite(ConvUnite.convertitUnite(unite));
      }
    }
    return res;
  }*/
  
/*  public  static IDescriptionVariable creerDescriptionVariable(String nomCourt) {
    IDescriptionVariable res = UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    res.nom(nomCourt);

    String resource = null;
    try {
      resource = RESOURCE.getString(nomCourt);
      StringTokenizer stk = new StringTokenizer(resource, ",");
      res.description(stk.nextToken());
      res.unite(ConvUnite.convertitUnite(stk.nextToken()));
      res.nbDecimales(Integer.parseInt(stk.nextToken()));
    }
    catch (Exception ex) {
      if ("Variable".equals(res.description())) res.description(nomCourt);
      res.nbDecimales(3);
    }
    return res;
  }*/

  /**
   * Permet de savoir si la liste contient des noms de variable sp�cifique
   * d'un fichier r�sultat CASIER.
   * @param nomsVar la liste des noms de variable.
   * @return true s'il existe au moins un nom de variable CASIER, false sinon.
   */
  public static final boolean isContientNomsCasier(List nomsVar) {
    Iterator ite = nomsVar.iterator();
    while(ite.hasNext()) {
      int origine = getOrigineVar(ite.next());
      if (origine == CASIER) return true;
    }
    return false;
  }
  /**
   * Permet de savoir si la liste contient des noms de variable sp�cifique
   * d'un fichier r�sultat LIAISON.
   * @param nomsVar la liste des noms de variable.
   * @return true s'il existe au moins un nom de variable LIAISON, false sinon.
   */
  public static final boolean isContientNomsLiaison(List nomsVar) {
    Iterator ite = nomsVar.iterator();
    while(ite.hasNext()) {
      int origine = getOrigineVar(ite.next());
      if (origine == LIAISON) return true;
    }
    return false;
  }
  /**
   * Permet de savoir si la liste contient des noms de variable sp�cifique
   * d'un fichier r�sultat TRACER.
   * @param nomsVar la liste des noms de variable.
   * @return true s'il existe au moins un nom de variable LIAISON, false sinon.
   */
  public static boolean isContientNomsTracer(List nomsVar) {
	  Iterator ite = nomsVar.iterator();
	    while(ite.hasNext()) {
	      int origine = getOrigineVar(ite.next());
	      if (origine == TRACER) return true;
	    }
	    return false;
	}
  
  /**
   * Permet de savoir si la liste contient des noms de variable sp�cifique
   * d'un fichier r�sultat Calage Auto.
   * @param nomsVar la liste des noms de variable.
   * @return true s'il existe au moins un nom de variable Calage Auto, false sinon.
   */
  public static boolean isContientNomsCalageAuto(List nomsVar) {
	  Iterator ite = nomsVar.iterator();
	    while(ite.hasNext()) {
	      int origine = getOrigineVar(ite.next());
	      if (origine == CALAGE_AUTO) return true;
	    }
	    return false;
	}

  static final int getOrigineVar(Object var){
    if (var instanceof String) {
      return getOrigineVar((String) var);
    } else if (var instanceof SResultatsVariable) {
      return getOrigineVar((SResultatsVariable) var);
    } else {
      throw new IllegalArgumentException("var n'est pas une String ni une SResultatsVariable");
    }
  }

  /**
   * Permet de r�cup�rer l'origine de variable (MASCARET, LIAISON ou CASIER).
   * @param nomVar l'identifiant de la variable dans le fichier de properties.
   * @return l'origine de la variable pouvant prendre les valeurs MASCARET, LIAISON,CASIER, ou TRACER.
   */
  static final int getOrigineVar(String nomVar){
    try {
      String type = RESOURCE.getString(nomVar + ".type");
      if ("liaison".equals(type)) return LIAISON;
      else if ("casier".equals(type)) return CASIER;
      else if ("tracer".equals(type)) return TRACER;
      else if ("calageAuto".equals(type)) return CALAGE_AUTO;
      else return MASCARET;
    }
    catch (Exception ex) {
      return MASCARET;
    }
  }

  /**
   * Permet de r�cup�rer l'origine de variable (MASCARET, LIAISON ou CASIER).
   * @param var la variable dont le nom court est l'identifiant dans le fichier de properties.
   * @return l'origine de la variable pouvant prendre les valeurs MASCARET, LIAISON,CASIER, ou TRACER.
   */
  static final int getOrigineVar(SResultatsVariable var){
    try {

      String type = RESOURCE.getString(var.nomCourt + ".type");
      if ("liaison".equals(type)) return LIAISON;
      else if ("casier".equals(type)) return CASIER;
      else if ("tracer".equals(type)) return TRACER;
      else if ("calageAuto".equals(type)) return CALAGE_AUTO;
      else return MASCARET;
    }
    catch (Exception ex) {
      return MASCARET;
    }
  }

  /**
   * Permet de r�cup�rer l'indice de stockage de la variable dans SParametresVarStock.
   * @param nomVar l'identifiant de la variable dans le fichier de properties.
   * @return l'indice de stockage s'il existe, -1 sinon.
   */
  public static final int getIndiceStock(String nomVar){
    try {
      String indiceChaine = RESOURCE.getString(nomVar + ".iStock");
      return Integer.parseInt(indiceChaine);
    }
    catch (Exception ex) {
      return -1;
    }
  }

  /**
   * Permet de savoir si la variable est ind�pendante du temps.
   * @param nomVar l'identifiant de la variable dans le fichier de properties.
   * @return true si la variable est ind�pendante du temps, false sinon.
   */
  public static final boolean isIndependantTps(String nomVar){
    try {
      String valeurChaine = RESOURCE.getString(nomVar + ".IndependantTps");
      if ("Vrai".equalsIgnoreCase(valeurChaine.trim())) return true;
      return false;
    }
    catch (Exception ex) {
      return false;
    }
  }
  /**
   * Permet de savoir si la variable est ind�pendante du temps.
   * @param var la variable dont le nom court est l'identifiant dans le fichier de properties.
   * @return true si la variable est ind�pendante du temps, false sinon.
   */
  public static final boolean isIndependantTps(SResultatsVariable var){
    String nomVar = var.nomCourt;
    return isIndependantTps(nomVar);
  }


}
