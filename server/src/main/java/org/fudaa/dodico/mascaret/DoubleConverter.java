/*
 GPL 2
 */
package org.fudaa.dodico.mascaret;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.apache.commons.lang.StringUtils;
import org.fudaa.dodico.hydraulique1d.conv.ConvH1D_Masc;

/**
 *
 * @author Frederic Deniger
 */
public class DoubleConverter implements Converter {

  com.thoughtworks.xstream.converters.basic.DoubleConverter basicDoubleConverter = new com.thoughtworks.xstream.converters.basic.DoubleConverter();

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    Double values = (Double) source;
    if (values == null || values.isNaN() || ConvH1D_Masc.RIEN == values.doubleValue()) {
      writer.setValue(IntegerConverter.NOTHING_VALUE);
    } else {
      writer.setValue(basicDoubleConverter.toString(values));
    }
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    String value = reader.getValue();
    if (value == null || StringUtils.isBlank(value) || IntegerConverter.NOTHING_VALUE.equals(value)) {
      return ConvH1D_Masc.RIEN;
    }
    return basicDoubleConverter.fromString(value);
  }

  @Override
  public boolean canConvert(Class type) {
    return double.class.equals(type) || Double.class.equals(type);
  }
}
