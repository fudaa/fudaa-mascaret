/*
 * @file         EdamoxWriter.java
 * @creation
 * @modification $Date: 2005-08-16 13:13:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Classe permettant d\u2019�crire le fichier ��cas�� rubriques par rubriques.
 * @version      $Revision: 1.13 $ $Date: 2005-08-16 13:13:30 $ by $Author: deniger $
 * @author       Jean-Marc Lacombe
 */
public class EdamoxWriter extends PrintWriter {
  protected org.omg.CORBA.portable.IDLEntity param;
  protected String nomRubrique= "";
  protected Object[] descriptionChamp;
  private static String lineSeparator= CtuluLibString.LINE_SEP;
  private static final int IRIEN= Integer.MIN_VALUE;
  //private static boolean inverseFields=false;
  public String getLineSeparator() {
    return lineSeparator;
  }
  public void setLineSeparator(String _lineSeparator) {
    lineSeparator= _lineSeparator;
  }
  public EdamoxWriter() {
    this(new OutputStreamWriter(System.out));
  }
  public EdamoxWriter(Writer _writer) {
    super(_writer);
  }
  public void setParam(org.omg.CORBA.portable.IDLEntity _param) {
    param= _param;
  }
  public void setNomRubrique(String _nomRubrique) {
    nomRubrique= _nomRubrique;
  }
  public void setDescriptionChamp(Object[] _descriptionChamp) {
    descriptionChamp= _descriptionChamp;
  }
  public void writeRubrique() throws Exception {
    if (!nomRubrique.equals("")) {
      ecrit("/");
      ecrit("/" + nomRubrique);
      ecrit("/");
    }
    Field vari[], varCourante;
    Object lobj, elt;
    StringBuffer sb =null;
    vari= param.getClass().getFields();
    for (int i= 0; i < vari.length; i++) {
      varCourante= vari[i];
      lobj= varCourante.get(param);
      if (lobj != null) {
        if (lobj.getClass().isArray()) { // LE CHAMP EST UN TABLEAU
          int l= Array.getLength(lobj);

          if (l > 0) {
            if (!(Array.get(lobj, 0)
              instanceof org.omg.CORBA.portable.IDLEntity)) {
              ecrit(descriptionChamp[i].toString() + " = ");
              sb = new StringBuffer(l*20);
            }
          }
          for (int j= 0; j < l; j++) {
            elt= Array.get(lobj, j);
            if (elt instanceof org.omg.CORBA.portable.IDLEntity) {
              Object[] descriptions= (Object[])descriptionChamp[i];
              Object[] descriptParam= (Object[])descriptions[j];
              EdamoxWriter ew= new EdamoxWriter(out);
              ew.setNomRubrique((String)descriptParam[0]);
              ew.setDescriptionChamp((Object[])descriptParam[1]);
              ew.setParam((org.omg.CORBA.portable.IDLEntity)elt);
              ew.writeRubrique();
            } else {
              if (elt instanceof Double) {
                if (!((Double)elt).isNaN())
                  sb.append(elt);
              } else if (elt instanceof Boolean) {
                if (((Boolean)elt).booleanValue())
                  sb.append("VRAI");
                else
                  sb.append("FAUX");
              } else if (elt instanceof String) {
                sb.append("'");
                sb.append(elt);
                sb.append("'");
              } else if (elt instanceof Integer) {
                if (((Integer)elt).intValue() != IRIEN)
                sb.append(elt);
              } else
                sb.append(elt);
              if (j == l - 1) {
                ecrit(sb.toString());
                sb= null;
              } else
                sb.append(";");
            }
          }
        } else { // ce n'est pas un tableau
          if (lobj instanceof org.omg.CORBA.portable.IDLEntity) {
            Object[] descriptParam= (Object[])descriptionChamp[i];
            EdamoxWriter ew= new EdamoxWriter(out);
            ew.setNomRubrique((String)descriptParam[0]);
            ew.setDescriptionChamp((Object[])descriptParam[1]);
            ew.setParam((org.omg.CORBA.portable.IDLEntity)lobj);
            ew.writeRubrique();
          } else { // pas une Structure d�fini dans l'IDL
            String champ= descriptionChamp[i].toString();
            sb  = new StringBuffer();
            if (!champ.equals("")) {
              sb.append(champ + " = ");
              if (lobj instanceof Boolean) {
                if (((Boolean)lobj).booleanValue())
                  ecrit(sb.append("VRAI").toString());
                else
                  ecrit(sb.append("FAUX").toString());
              } else if (lobj instanceof String) {
                sb.append("'");
                sb.append(lobj);
                sb.append("'");
                ecrit(sb.toString());
              } else if (lobj instanceof Integer) {
                if (((Integer)lobj).intValue() != IRIEN)
                  ecrit(sb.append(lobj).toString());
              } else if (lobj instanceof Double) {
                if (!((Double)lobj).isNaN())
                  ecrit(sb.append(lobj).toString());
              } else { // pas un booleen ni une chaine ni entier ni double
                ecrit(sb.append(lobj).toString());
              }
            }
          }
        }
      }
    }
  }
  private final void ecrit(String s) throws IOException {
    for (int i= 0; i < s.length(); i += 72) {
      write(s.substring(i, Math.min(s.length(), i + 72)));
      write(lineSeparator);
    }
  }

}
