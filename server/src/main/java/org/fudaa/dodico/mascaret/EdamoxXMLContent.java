/*
 GPL 2
 */
package org.fudaa.dodico.mascaret;

import org.fudaa.dodico.corba.mascaret.SParametresCAS;

/**
 *
 * @author Frederic Deniger
 */
public class EdamoxXMLContent {

  private SParametresCAS parametresCas;

  public EdamoxXMLContent(SParametresCAS parametresCas) {
    this.parametresCas = parametresCas;
  }

  public EdamoxXMLContent() {
  }


  public SParametresCAS getParametresCas() {
    return parametresCas;
  }

  public void setParametresCas(SParametresCAS parametresCas) {
    this.parametresCas = parametresCas;
  }
}
