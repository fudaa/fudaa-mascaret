/*
 GPL 2
 */
package org.fudaa.dodico.mascaret;

import com.memoire.fu.FuLib;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.io.naming.NameCoder;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.xml.BooleanArrayConverter;
import org.fudaa.ctulu.xml.DoubleArrayConverter;
import org.fudaa.ctulu.xml.ErrorHandlerDefault;
import org.fudaa.ctulu.xml.FloatArrayConverter;
import org.fudaa.ctulu.xml.IntegerArrayConverter;
import org.fudaa.dodico.corba.mascaret.SNoeud;
import org.fudaa.dodico.corba.mascaret.SParametresBiblio;
import org.fudaa.dodico.corba.mascaret.SParametresCAS;
import org.fudaa.dodico.corba.mascaret.SParametresCalageAuto;
import org.fudaa.dodico.corba.mascaret.SParametresConcInitTracer;
import org.fudaa.dodico.corba.mascaret.SParametresConcentrations;
import org.fudaa.dodico.corba.mascaret.SParametresConfluent;
import org.fudaa.dodico.corba.mascaret.SParametresCrueCalageAuto;
import org.fudaa.dodico.corba.mascaret.SParametresDeversLateraux;
import org.fudaa.dodico.corba.mascaret.SParametresDeversoirsV5P2;
import org.fudaa.dodico.corba.mascaret.SParametresExtrLibres;
import org.fudaa.dodico.corba.mascaret.SParametresGeoReseau;
import org.fudaa.dodico.corba.mascaret.SParametresImpress;
import org.fudaa.dodico.corba.mascaret.SParametresLoi;
import org.fudaa.dodico.corba.mascaret.SParametresLoiTracer;
import org.fudaa.dodico.corba.mascaret.SParametresNum;
import org.fudaa.dodico.corba.mascaret.SParametresSeuil;
import org.fudaa.dodico.corba.mascaret.SParametresTraceur;
import org.fudaa.dodico.corba.mascaret.SParametresVarCalc;
import org.fudaa.dodico.corba.mascaret.SParametresVarStock;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *
 * @author Frederic Deniger
 */
public class EdamoxXmlReaderWriter {

  public static final String VERSION_1_0 = "1.0";
  public static final String LAST_VERSION = VERSION_1_0;

  /**
   *
   * @return
   */
  public static EdamoxXmlReaderWriter createReaderWriter() {
    return new EdamoxXmlReaderWriter(LAST_VERSION);
  }

  /**
   *
   * @param xcas will be used to find the version if required.
   * @return
   */
  public static EdamoxXmlReaderWriter findReader(File xcas) {
    //voir XmlVersionFinder
    return new EdamoxXmlReaderWriter("1.0");
  }
  public static final String ENCODING = "ISO-8859-1";
  public static final String ENTETE_DTD = "<!DOCTYPE fichierCas SYSTEM \"mascaret-{version}.dtd\">";
  public static final String ENTETE_XML = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
  private final static Logger LOGGER = Logger.getLogger(EdamoxXmlReaderWriter.class.getName());
  /**
   * La version du fichier
   */
  private final String version;
  /**
   * le nom du fichier xsd a utiliser
   */
  private final String xsdId;
  /**
   * La path complet du fichier xsd
   */
//  private final String xsdPath;
//  private final String dtdFile;
  private final String dtdPath;
  private final String dtdFile;

  public EdamoxXmlReaderWriter(String version) {
    this.version = version;
    this.xsdId = "mascaret";
    dtdFile = xsdId + "-" + version + ".dtd";
    dtdPath = "/dtd/" + dtdFile;
  }

  /**
   * @return the xsdId
   */
  public String getXsdId() {
    return xsdId;
  }

  /**
   * @return the version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @return le path dans jar vers le fichier xsd correspondant
   */
  public final String getDtdValidator() {
    return dtdPath;
  }

  /**
   * Initialisation de xstream
   *
   * @param analyse
   * @return
   */
  protected XStream initXmlParser(final CtuluLog analyse) {
    final XmlFriendlyNameCoder replacer = createReplacer();
    final StaxDriver staxDriver = new StaxDriver(replacer);
    final XStream xstream = new XStream(staxDriver);
    xstream.setMode(XStream.NO_REFERENCES);
    configureXStream(xstream);
    return xstream;
  }

  private XmlFriendlyNameCoder createReplacer() {
    return new XmlFriendlyNameCoder("#", "_");
  }

  public CtuluLog isValide(final File xml) {
    try {
      return isValide(xml.toURI().toURL());
    } catch (MalformedURLException ex) {
      LOGGER.log(Level.SEVERE, null, ex);
    }
    return null;
  }

  public CtuluLog isValide(final String xml) {
    return isValide(getClass().getResource(xml));
  }

  public CtuluLog isValide(final URL xml) {
    return isValide(xml, dtdPath);
  }

  public static class CustomResolver implements EntityResolver {

    URL dtdURL;

    public CustomResolver(URL dtdURL) {
      this.dtdURL = dtdURL;
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
      try {
        return new InputSource(dtdURL.toURI().toString());
      } catch (URISyntaxException ex) {
        Logger.getLogger(EdamoxXmlReaderWriter.class.getName()).log(Level.SEVERE, null, ex);
      }
      return null;
    }
  }

  // TODO a mettre dans un common
  public static CtuluLog isValide(final URL xml, final String dtd) {
    final CtuluLog res = new CtuluLog();
    res.setDesc(CtuluLib.getS("Validation du fichier xml {0}", xml.toString()));
    if (xml == null) {
      res.addSevereError(CtuluLib.getS("Fichier non trouv�: {0}", xml.toString()));
      return res;
    }
    final ErrorHandlerDefault handler = new ErrorHandlerDefault(res);
    InputStream xmlStream = null;
    try {
//      final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      URL dtdURL = null;
      File dtdFile = new File(dtd);
      if (dtdFile.exists()) {
        dtdURL = dtdFile.toURI().toURL();
      } else {
        dtdURL = EdamoxXmlReaderWriter.class.getResource(dtd);
      }
      if (dtdURL == null) {
        res.addSevereError(CtuluLib.getS("La dtd {0} n'a pas �t� trouv�e", dtd));
        return res;
      }
//      schemaFactory.setResourceResolver(new CustomResolver(dtdURL));
//      final Schema schema = schemaFactory.newSchema(dtdURL);
//      final Schema schema = schemaFactory.newSchema(dtdURL);

//      final Validator validator = schemaFactory.newSchema().newValidator();
//      validator.set
//      validator.setErrorHandler(handler);
      xmlStream = xml.openStream();
      InputStreamReader input = new InputStreamReader(xmlStream, ENCODING);

      SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setValidating(true);
      factory.setNamespaceAware(true);
//      factory.setSchema(schema);
      SAXParser parser = factory.newSAXParser();
      XMLReader reader = parser.getXMLReader();
      reader.setEntityResolver(new CustomResolver(dtdURL));
      reader.setErrorHandler(handler);
      reader.parse(new InputSource(input));
//      validator.validate(new SAXSource(new InputSource(input)));
    } catch (final Exception e) {
      LOGGER.log(Level.INFO, "message {0}", e);
      res.addError(CtuluLib.getS("Erreur lors de la validation du fichier xml: {0}", e.getMessage()));
    } finally {
      CtuluLibFile.close(xmlStream);
    }
    return res;
  }

  /**
   * Lit les donn�es dans le fichier f avec les donn�es li�es.
   *
   * @param dataLinked
   * @return
   */
  public final CtuluIOResult<EdamoxXMLContent> read(final URL f) {
    final CtuluLog analyzer = new CtuluLog();
    analyzer.setDesc(CtuluLib.getS("Lecture du fichier {0}", f.getFile()));
    final EdamoxXMLContent d = readDao(f, analyzer);
    final CtuluIOResult<EdamoxXMLContent> res = new CtuluIOResult<EdamoxXMLContent>();
    res.setAnalyze(analyzer);
    res.setSource(d);
    return res;
  }

  /**
   * @param fichier
   * @return
   */
  public EdamoxXMLContent readContent(final File fichier, final CtuluLog analyser) {
    FileInputStream in = null;
    EdamoxXMLContent newData = null;
    try {
      in = new FileInputStream(fichier);
      newData = readContent(in, analyser);
    } catch (final FileNotFoundException e) {
      LOGGER.log(Level.FINE, "readContent", e);
      final String path = fichier == null ? "null" : fichier.getAbsolutePath();
      analyser.addSevereError(CtuluLib.getS("Fichier non trouv�: {0}", path));
    } finally {
      CtuluLibFile.close(in);
    }
    return newData;

  }

  /**
   * @param in
   * @return le dao
   */
  protected EdamoxXMLContent readContent(final InputStream in, final CtuluLog analyser) {
    EdamoxXMLContent newData = null;
    BufferedReader contentRead = null;
    try {
      final XStream parser = initXmlParser(analyser);
      contentRead = new BufferedReader(new InputStreamReader(in, ENCODING));
      newData = (EdamoxXMLContent) parser.fromXML(contentRead);// we not that is a D object.
    } catch (ConversionException conversionException) {
      LOGGER.log(Level.SEVERE, "readContent unknown balise", conversionException);
      analyser.addSevereError(CtuluLib.getS(
              "Une incoh�rence a �t� d�tect�e: le fichier xml est valide mais les balises {0} ne sont pas support�es par l'application",
              StringUtils.substringAfterLast(conversionException.getShortMessage(), ".")));

    } catch (CannotResolveClassException cannotResolveException) {
      LOGGER.log(Level.FINE, "readContent unknown balise", cannotResolveException);
      analyser.addSevereError(CtuluLib.getS(
              "Une incoh�rence a �t� d�tect�e: le fichier xml est valide mais les balises {0} ne sont pas support�es par l'application", StringUtils.
              substringAfterLast(cannotResolveException.getMessage(), ".")));

    } catch (final Exception e) {
      LOGGER.log(Level.SEVERE, "readContent", e);
      analyser.addSevereError(CtuluLib.getS("Une erreur est survenue lors de l'analyse du fichier xml: {0}", e.getMessage()));
    } finally {
      CtuluLibFile.close(contentRead);
    }
    return newData;

  }

  /**
   * @param pathToResource l'adresse du fichier a charger commencant par /
   * @param analyser
   * @param dataLinked
   * @return
   */
  protected EdamoxXMLContent readDao(final String pathToResource, final CtuluLog analyser) {
    return readDao(getClass().getResource(pathToResource), analyser);
  }

  /**
   * @param fichier
   * @return
   */
  public EdamoxXMLContent readDao(final URL url, final CtuluLog analyser) {
    if (url == null) {
      analyser.addSevereError(CtuluLib.getS("L'URL n'est pas renseign�e"));
      return null;
    }

    InputStream in = null;
    EdamoxXMLContent newData = null;
    try {
      in = url.openStream();
      newData = readContent(in, analyser);
    } catch (final IOException e) {
      LOGGER.log(Level.FINE, e.getMessage(), e);
      analyser.addSevereError(CtuluLib.getS("Une erreur est survenue lors de l'analyse du fichier xml: {0}", e.getMessage()));
    } finally {
      CtuluLibFile.close(in);
    }
    return newData;

  }

  /**
   * Lit les donn�es dans le fichier f avec les donn�es li�es.
   *
   * @param validation si vrai valide les donn�es.
   * @return
   */
  public final CtuluIOResult<EdamoxXMLContent> readXML(final File f) {
    CtuluLog analyzer = new CtuluLog();
    analyzer.setDesc(CtuluLib.getS("Lecture du fichier {0}", f.getName()));
    analyzer.setDesc("read.file");
    final EdamoxXMLContent d = readContent(f, analyzer);
    CtuluIOResult<EdamoxXMLContent> res = new CtuluIOResult<EdamoxXMLContent>();
    res.setAnalyze(analyzer);
    res.setSource(d);
    return res;

  }

  /**
   * Lit les donn�es dans le fichier f avec les donn�es li�es.
   *
   * @param dataLinked
   * @return
   */
  public final CtuluIOResult<EdamoxXMLContent> readXML(final String pathToResource) {
    CtuluIOResult<EdamoxXMLContent> res = new CtuluIOResult<EdamoxXMLContent>();
    CtuluLog analyzer = new CtuluLog();
    analyzer.setDesc(CtuluLib.getS("Lecture du fichier {0}", pathToResource));
    final EdamoxXMLContent d = readDao(pathToResource, analyzer);
    res.setSource(d);
    res.setAnalyze(analyzer);
    return res;

  }

  protected boolean writeContent(final File file, final EdamoxXMLContent content, final CtuluLog analyser) {
    FileOutputStream out = null;
    boolean ok = true;
    try {
      out = new FileOutputStream(file);
      out.flush();
      ok = write(out, content, analyser);
    } catch (final IOException e) {
      LOGGER.log(Level.SEVERE, "writeContent " + file.getName(), e);
      ok = false;
    } finally {
      CtuluLibFile.close(out);
    }
    return ok;

  }

  private void configureXStream(XStream xstream) {
    xstream.alias("fichierCas", EdamoxXMLContent.class);
    registerConverters(xstream);

    xstream.alias("structureParametresCAS", SParametresCAS.class);
    xstream.alias("noeud", SNoeud.class);
    xstream.alias("structureParametresConfluent", SParametresConfluent.class);
    xstream.alias("structureParametresSeuil", SParametresSeuil.class);
    xstream.alias("structureParametresLoi", SParametresLoi.class);
    xstream.alias("structureParametresConcentrations", SParametresConcentrations.class);
    xstream.alias("structureParametresDeversoirsV5P2", SParametresDeversoirsV5P2.class);
    xstream.alias("structureParametresCrueCalageAutomatique", SParametresCrueCalageAuto.class);
    xstream.alias("structureSParametresLoiTraceur", SParametresLoiTracer.class);

    xstream.aliasField("parametresCas", EdamoxXMLContent.class, "parametresCas");
    xstream.aliasField("parametresGeneraux", SParametresCAS.class, "parametresGen");
    xstream.aliasField("parametresModelePhysique", SParametresCAS.class, "parametresModelPhy");
    xstream.aliasField("parametresNumeriques", SParametresCAS.class, "parametresNum");
    xstream.aliasField("parametresTemporels", SParametresCAS.class, "parametresTemp");
    xstream.aliasField("parametresGeometrieReseau", SParametresCAS.class, "parametresGeoReseau");
    xstream.aliasField("parametresConfluents", SParametresCAS.class, "parametresConfluents");
    xstream.aliasField("parametresPlanimetrageMaillage", SParametresCAS.class, "parametresPlanimMaillage");
    xstream.aliasField("parametresSingularite", SParametresCAS.class, "parametresSingularite");
    xstream.aliasField("parametresApportDeversoirs", SParametresCAS.class, "parametresApporDeversoirs");
    xstream.aliasField("parametresLoisHydrauliques", SParametresCAS.class, "parametresLoisHydrau");
    xstream.aliasField("parametresConditionsInitiales", SParametresCAS.class, "parametresCondInit");
    xstream.aliasField("parametresImpressionResultats", SParametresCAS.class, "parametresImpressResult");
    xstream.aliasField("parametresVariablesCalculees", SParametresCAS.class, "parametresVarCalc");
    xstream.aliasField("parametresVariablesStockees", SParametresCAS.class, "parametresVarStock");
    xstream.aliasField("parametresTraceur", SParametresCAS.class, "parametresTracer");

    xstream.aliasField("variablesCalculees", SParametresVarCalc.class, "varCalculees");
    xstream.aliasField("variablesStockees", SParametresVarStock.class, "varStockees");

    xstream.aliasField("parametresConvectionDiffusion", SParametresTraceur.class, "parametresConvecDiffu");
    xstream.aliasField("parametresNumeriquesQualiteEau", SParametresTraceur.class, "parametresNumQualiteEau");
    xstream.aliasField("parametresImpressionTraceur", SParametresTraceur.class, "parametresImpressTracer");
    xstream.aliasField("parametresConditionsLimitesTraceur", SParametresTraceur.class, "parametresCondLimTracer");
    xstream.aliasField("parametresConcentrationsInitialesTraceur", SParametresTraceur.class, "parametresConcInitTracer");
    xstream.aliasField("parametresSourcesTraceur", SParametresTraceur.class, "parametresSourcesTraceurs");
    xstream.aliasField("parametresLoisTraceur", SParametresTraceur.class, "parametresLoisTracer");
    xstream.aliasField("bibliotheque", SParametresBiblio.class, "bibliotheques");

    xstream.aliasField("listeBranches", SParametresGeoReseau.class, "branches");
    xstream.aliasField("listeNoeuds", SParametresGeoReseau.class, "noeuds");

    xstream.aliasField("listeCrues", SParametresCalageAuto.class, "crues");
    xstream.aliasField("listeZones", SParametresGeoReseau.class, "zones");

    xstream.aliasField("listeConcentrations", SParametresConcInitTracer.class, "concentrations");
    xstream.aliasField("listeConcentrations", SParametresLoiTracer.class, "concentrations");

    xstream.aliasField("impressionGeometrie", SParametresImpress.class, "geometrie");
    xstream.aliasField("impressionPlanimetrage", SParametresImpress.class, "planimetrage");
    xstream.aliasField("impressionReseau", SParametresImpress.class, "reseau");
    xstream.aliasField("impressionLoiHydraulique", SParametresImpress.class, "loiHydrau");
    xstream.aliasField("impressionligneEauInitiale", SParametresImpress.class, "ligneEauInit");
    xstream.aliasField("impressionCalcul", SParametresImpress.class, "calcul");

    xstream.aliasField("noms", SParametresExtrLibres.class, "Nom");
    xstream.aliasField("noms", SParametresDeversLateraux.class, "nom");

    //ignore
//
    xstream.omitField(SParametresNum.class, "parametresNumeriqueCasier");

  }

  public static void registerConverters(XStream xstream) {
    //ces converter traduisent des tableaux de double, boolean,... en chaine de caractere
    //avec chaque entr�e s�par�s par un espace.
    //Si la lecture de ces lignes est trop compliqu� en fortran, il est possible
    //de supprimer ces converter. Ainsi chaque entr�e sera dans des balises <double>,...
    xstream.registerConverter(new DoubleArrayConverter(IntegerConverter.NOTHING_VALUE));
    xstream.registerConverter(new BooleanArrayConverter(IntegerConverter.NOTHING_VALUE));
    xstream.registerConverter(new IntegerArrayConverter(IntegerConverter.NOTHING_VALUE));
    xstream.registerConverter(new FloatArrayConverter(IntegerConverter.NOTHING_VALUE));
    //pour enlever les caracteres speciaux
    xstream.registerConverter(new StringConverter());
    //pour les NAN
    xstream.registerConverter(new IntegerConverter());
    //pour les NAN
    xstream.registerConverter(new DoubleConverter());
//    xstream.registerConverter(new StringArrayConverter());
  }

  protected static class CustomPrettyPrintWriter extends PrettyPrintWriter {

    public CustomPrettyPrintWriter(Writer writer, char[] lineIndenter, NameCoder nameCoder) {
      super(writer, XML_QUIRKS, lineIndenter, nameCoder);
    }

    @Override
    protected String getNewLine() {
      return CtuluLibString.LINE_SEP;
    }
  }

  /**
   * @param out le flux de sortie
   * @param dao le dao a persister
   * @param analyser le receveur d'information
   * @return
   */
  protected boolean write(final OutputStream out, final EdamoxXMLContent dao, final CtuluLog analyser) {
    boolean isOk = true;
    try {
      final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, ENCODING), 8192 * 4);
      writer.write(ENTETE_XML + CtuluLibString.LINE_SEP);
      writer.write(FuLib.replace(ENTETE_DTD, "{version}", this.version) + CtuluLibString.LINE_SEP);
      final XStream parser = initXmlParser(analyser);
      parser.marshal(dao, new CustomPrettyPrintWriter(writer, new char[]{' ', ' '},
              createReplacer()));

    } catch (final IOException e) {
      LOGGER.log(Level.SEVERE, "", e);
      analyser.addSevereError(CtuluLib.getS("Erreur bloquante rencontr�e lors de l'�criture du fichier xml"));
      isOk = false;
    } finally {
      CtuluLibFile.close(out);
    }
    return isOk;
  }

  /**
   * MEthode qui permet d'ecrire les datas dans le fichier f specifie.
   *
   * @param data
   * @param f
   * @return
   */
  public final CtuluLog writeXML(final EdamoxXMLContent metier, final File f) {
    final CtuluLog analyzer = new CtuluLog();
    f.getParentFile().mkdirs();
    analyzer.setDesc(CtuluLib.getS("Ecriture du fichier {0}", f.getName()));

    if (metier != null) {
//      metier.setXsdName(dtdFile);
      writeContent(f, metier, analyzer);
    }
    return analyzer;
  }

  /**
   * @param metier l'objet metier
   * @param out le flux de sortie qui ne sera pas ferme
   * @param analyser
   * @return true si reussite
   */
  public boolean writeXML(final EdamoxXMLContent metier, final OutputStream out) {
    final CtuluLog analyser = new CtuluLog();
    analyser.setDesc(CtuluLib.getS("Ecriture du fichier {0}", xsdId));
    if (metier != null) {
      return write(out, metier, analyser);
    }
    return false;
  }
}
