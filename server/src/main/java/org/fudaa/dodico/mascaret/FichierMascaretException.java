/*
 * @file         FichierMascaretException.java
 * @creation     2004-02-23
 * @modification $Date: 2007-11-20 11:43:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;
/**
 * Impl�mentation de l'exception "FichierMascaretException" g�n�r�e lorsqu'un probl�me est rencontr� en acc�dant � un fichier.
 * @version      $Revision: 1.2 $ $Date: 2007-11-20 11:43:02 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class FichierMascaretException extends Exception {
  private int numeroLigne_=-1;
  private String ligne_=null;
  public FichierMascaretException() {
    this(-1, null);
  }
  public FichierMascaretException(int numeroLigne) {
    this(numeroLigne, null);
  }
  public FichierMascaretException(String ligne) {
    super();
    ligne_ = ligne;
  }
  public FichierMascaretException(String ligne, String message) {
    super(message);
    ligne_ = ligne;
  }
  public FichierMascaretException(int numeroLigne, String ligne, String message) {
    super(message);
    numeroLigne_ = numeroLigne;
    ligne_ = ligne;
  }
  public FichierMascaretException(int numeroLigne, String ligne) {
    super();
    numeroLigne_ = numeroLigne;
    ligne_ = ligne;
  }
  public String getLigne() {
    return ligne_;
  }
  public int getNumeroLigne() {
    return numeroLigne_;
  }
}
