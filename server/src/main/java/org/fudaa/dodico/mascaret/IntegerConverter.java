/*
 GPL 2
 */
package org.fudaa.dodico.mascaret;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.basic.IntConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.apache.commons.lang.StringUtils;
import org.fudaa.dodico.hydraulique1d.conv.ConvH1D_Masc;

/**
 *
 * @author Frederic Deniger
 */
public class IntegerConverter implements Converter {

  public static String NOTHING_VALUE = "-0";

  IntConverter basicIntConverter = new IntConverter();

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    Integer values = (Integer) source;
    if (values == null || ConvH1D_Masc.IRIEN == values.intValue()) {
      writer.setValue(NOTHING_VALUE);
    } else {
      writer.setValue(basicIntConverter.toString(values));
    }
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    String value = reader.getValue();
    if (value == null || StringUtils.isBlank(value) || NOTHING_VALUE.equals(value)) {
      return ConvH1D_Masc.IRIEN;
    }
    return basicIntConverter.fromString(value);
  }

  @Override
  public boolean canConvert(Class type) {
    return int.class.equals(type) || Integer.class.equals(type);
  }
}
