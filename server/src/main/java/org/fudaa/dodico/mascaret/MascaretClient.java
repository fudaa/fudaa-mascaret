/*
 * @file         MascaretClient.java
 * @creation     2000-06-23
 * @modification $Date: 2007-03-28 15:35:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;

import java.io.File;

import org.fudaa.dodico.corba.lido.SParametresLIG;
import org.fudaa.dodico.corba.mascaret.*;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.corba.usine.IUsine;
import org.fudaa.dodico.objet.ServeurPersonne;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Classe de test qui n\u2019est plus utilis�e.
 *
 * @version $Revision: 1.17 $ $Date: 2007-03-28 15:35:28 $ by $Author: opasteur $
 * @author Jean-Marc Lacombe
 */
public final class MascaretClient {

  private MascaretClient() {}
  static SParametresCAS cas;
  static SParametresGEO geo;
  static SParametresLIG lig;
  static SParametresREP inRep;
  static SResultatsLIS lis;
  static SResultatsOPT outOpt;
  static SResultatsRUB rub;
  static SResultatsEcran ecran;
  static SResultatsMAI mai;
  static SParametresREP outRep;

  public static void main(String[] _args) {
    if (_args.length < 1) {
      System.out.println("Usage: MascaretClient <chemin>");
      System.out.println("       chemin=ou se trouvent les fichiers de parametres");
      System.exit(1);
    }
    /*
     * try { File bidon = new File("bidon"); System.out.println( "chemin :"+bidon.getAbsolutePath()); PrintWriter fbidon =
     * new PrintWriter(new FileWriter(bidon)); fbidon.println("bidon"); fbidon.close(); System.exit(0); } catch
     * (IOException e) {}
     */
    String path = _args[0];
    if (!path.endsWith(File.separator)) path += File.separator;
    System.out.println("Creation de la connexion");
    UsineLib.setAllLocal(true);
    IUsine usine = UsineLib.findUsine();
    final IPersonne sp = ServeurPersonne.createPersonne("test-personne-mascaret", "test-organisme-mascaret");
    ICalculMascaret mascaret;
    System.out.println("MascaretClient...");
    // Recherche du serveur Mascaret
    // do {
    // try {
    // mascaret=(ICalculMascaret)CDodico.findServer("::mascaret::ICalculMascaret");
    // } catch(Exception ex) {
    // mascaret=null;
    // }
    // } while(mascaret==null);
    mascaret = usine.creeMascaretCalculMascaret();
    IConnexion c = mascaret.connexion(sp);
    // CDodico.getBOA().obj_is_ready(mascaret);
    /* SParametresCAS cas= */getCas();
    /* SParametresGEO geo= */getGeo();
    // SParametresLIG lig= new SParametresLIG();
    // SResultatsOPT inOpt= new SResultatsOPT();
    // SParametresREP inRep= new SParametresREP();
    try {
      IParametresMascaret pFic = IParametresMascaretHelper.narrow(mascaret.parametres(c));
      pFic.parametresCAS(cas);
      pFic.parametresGEO(geo);
      IResultatsMascaret rFic = IResultatsMascaretHelper.narrow(mascaret.resultats(c));
      // mascaret.parametres(pFic);
      String nomEtu = "mascaret" + c.numero();
      cas.parametresGen.fichMotsCles = path + nomEtu + ".xcas";
      SParametresNCA ncas = new SParametresNCA(path + nomEtu + ".cxas");
      pFic.parametresNCA(ncas);
      cas.parametresGeoReseau.geometrie.fichier = path + nomEtu + ".geo";
      cas.parametresImpressResult.resultats.fichResultat = path + nomEtu + "_ecr.opt";
      cas.parametresImpressResult.listing.fichListing = path + nomEtu + ".lis";
      cas.parametresImpressResult.fichReprise.fichRepriseEcr = path + nomEtu + "_ecr.rep";
      System.out.println("Voici les donnees temporelles de l'etude " + nomEtu);
      SParametresCAS pCas = new SParametresCAS();
      pCas = pFic.parametresCAS();
      System.out.println("T INITIAL                     =" + pCas.parametresTemp.tempsInit);
      System.out.println("T MAXIMAL                     =" + pCas.parametresTemp.tempsMax);
      System.out.println("PAS DE TEMPS                  =" + pCas.parametresTemp.pasTemps);
      System.out.println("NUMERO DU PREMIER PAS STOCKE  =" + pCas.parametresImpressResult.pasStockage.premPasTpsStock);
      System.out.println("PAS DE TEMPS D IMPRESSION     =" + pCas.parametresImpressResult.pasStockage.pasImpression);
      System.out.println("PAS DE TEMPS DE STOCKAGE      =" + pCas.parametresImpressResult.pasStockage.pasStock);
      /*
       * System.out.println("Donnez le nouveau PAS DE TEMPS: ") ; BufferedReader d = new BufferedReader(new
       * InputStreamReader(System.in)); pCas.parametresTemp.pasTemps = Double.valueOf(d.readLine()).intValue() ;
       * System.out.println("Nouveau PAS DE TEMPS ="+pCas.parametresTemp.pasTemps) ;
       */
      pFic.parametresCAS(pCas);
      mascaret.calcul(c);
      System.out.println("Recuperation des resultats dans le client");
      // SResultatsLIS lis= new SResultatsLIS();
      SResultatsOPT opt = new SResultatsOPT();
      SParametresREP rep = new SParametresREP();
      lis = rFic.resultatsLIS();
      // opt= rFic.resultatsOPT();
      rep = rFic.resultatsREP();
      System.out.println("Listing");
      System.out.println(lis.contenu);
      System.out.println("resultat OPT");
      System.out.println("    variables :");
      for (int i = 0; i < opt.variables.length; i++) {
        System.out.println(opt.variables[i].nomCourt);
      }
      System.out.println("    resultat :");
      for (int i = 0; i < opt.resultatsPasTemps.length; i++) {
        System.out.println(opt.resultatsPasTemps[i].t);
        for (int j = 0; j < opt.resultatsPasTemps[i].resultatsBief.length; j++) {
          for (int k = 0; k < opt.resultatsPasTemps[i].resultatsBief[j].resultatsSection.length; k++) {
            System.out.print("\t" + opt.resultatsPasTemps[i].resultatsBief[j].resultatsSection[k].absc);
            for (int l = 0; l < opt.resultatsPasTemps[i].resultatsBief[j].resultatsSection[k].valeurs.length; l++) {
              System.out.print("\t" + opt.resultatsPasTemps[i].resultatsBief[j].resultatsSection[k].valeurs[l]);
            }
            System.out.println();
          }
        }
      }
      System.out.println("resultat REP");
      System.out.println(rep.contenu);
      System.out.println("Endofclient........");
    } catch (Exception ex) {
      System.err.println(ex);
    }
  }

  public static SParametresCAS getCas() {
    // debut de l'initialisation des parametres fichier
    // cas-------------------------------------------------------------------------------------------
    SParametresCAS parametresCas = new SParametresCAS();
    parametresCas.parametresGen = new SParametresGen(1, 3, "C:\\Program Files\\MASCARET\\Test1\\DONNEES\\Ess2.cas",
        "mascaretV4P1.dico", "/tmp_mnt/home/steph/LIDO/DEV/NEW3/CAS1/princi.f", false,
        "C:\\Program Files\\MASCARET\\Test1\\temp", false, 1, false, new SParametresBiblio("mascaretV4P1.a damoV3P0.a"));
    SParametresModelPhy parametresModelPhy = new SParametresModelPhy();
    parametresModelPhy.perteChargeConf = false;
    parametresModelPhy.compositionLits = 1;
    parametresModelPhy.conservFrotVertical = false;
    parametresModelPhy.elevCoteArrivFront = 0.01;
    parametresModelPhy.interpolLinStrickler = false;
    parametresModelPhy.debordement = new SParametresDebordProgr(false, false);
    parametresCas.parametresModelPhy = parametresModelPhy;
    parametresCas.parametresNum = new SParametresNum(false, 1, true, 0.005, false, false, true,false,
        new SParametresNumCasier());
    parametresCas.parametresTemp = new SParametresTemp(10, 0, 1, 2, 1000, false, 1, 0, 0, 1);
    SParametresGeoReseau parametresGeoReseau = new SParametresGeoReseau();
    parametresGeoReseau.geometrie = new SParametresGeom("C:\\Program Files\\MASCARET\\Test1\\DONNEES\\ge2.txt", 2,
        false);
    int[] vInt0 = { 0 };
    int[] vInt1 = { 1 };
    int[] vInt2 = { 2 };
    double[] vDouble0 = { 0 };
    double[] abscFin = { 18000 };
    parametresGeoReseau.branches = new SParametresBranches(1, vInt1, vDouble0, abscFin, vInt1, vInt2);
    parametresGeoReseau.noeuds = new SParametresNoeuds(0, new SNoeud[0]);
    int[] num = { 1, 2 };
    int[] numExtr = { 1, 2 };
    String[] nom = { "0", "0" };
    int[] typeCond = { 1, 2 };
    int[] numLoi = { 1, 2 };
    parametresGeoReseau.extrLibres = new SParametresExtrLibres(2, num, numExtr, nom, typeCond, numLoi);
    parametresCas.parametresGeoReseau = parametresGeoReseau;
    parametresCas.parametresConfluents = new SParametresConfluents(0, new SParametresConfluent[0]);
    SParametresPlanimMaillage parametresPlanimMaillage = new SParametresPlanimMaillage();
    parametresPlanimMaillage.methodeMaillage = 2;
    double[] valPas = { 0.25 };
    parametresPlanimMaillage.planim = new SParametresPlanim(40, 1, valPas, vInt1, vInt2);
    int[] vInt5 = { 5 };
    SParametresMaillageClavier parametresMaillageClavier = new SParametresMaillageClavier(1, vInt1, vInt0, vDouble0, 1,
        null, null, null, 1, vInt1, vDouble0, abscFin, vInt5);
    parametresPlanimMaillage.maillage = new SParametresMaillage(2, " ", false, " ", parametresMaillageClavier);
    parametresCas.parametresPlanimMaillage = parametresPlanimMaillage;
    SParametresBarrPrincip barrPrincip = new SParametresBarrPrincip(0, 0, 1, 0);
    SParametresSeuil[] seuils = new SParametresSeuil[0];
    double[] vDouble1 = { 1 };
    double[] vDouble2988 = { 2988 };
    SParametresPerteCharge perteCharges = new SParametresPerteCharge(0, vInt1, vDouble2988, vDouble1);
    parametresCas.parametresSingularite = new SParametresSingularite(0, barrPrincip, seuils, perteCharges);
    parametresCas.parametresApporDeversoirs = new SParametresApporDeversoirs();
    parametresCas.parametresApporDeversoirs.debitsApports = new SParametresQApport();
    parametresCas.parametresApporDeversoirs.debitsApports.nbQApport = 0;
    parametresCas.parametresApporDeversoirs.debitsApports.abscisses = null;
    parametresCas.parametresApporDeversoirs.debitsApports.longueurs = null;
    parametresCas.parametresApporDeversoirs.debitsApports.noms = null;
    parametresCas.parametresApporDeversoirs.debitsApports.numBranche = null;
    parametresCas.parametresApporDeversoirs.debitsApports.numLoi = null;
    parametresCas.parametresApporDeversoirs.deversLate = new SParametresDeversLateraux();
    parametresCas.parametresApporDeversoirs.deversLate.nbDeversoirs = 0;
        //parametresCas.parametresApporDeversoirs.deversLate.deversoirsV5P2= null;
	    parametresCas.parametresApporDeversoirs.deversLate.abscisse= null;
	    parametresCas.parametresApporDeversoirs.deversLate.coeffDebit= null;
	    parametresCas.parametresApporDeversoirs.deversLate.coteCrete= null;
	    parametresCas.parametresApporDeversoirs.deversLate.longueur= null;
	    parametresCas.parametresApporDeversoirs.deversLate.nom= null;
	    parametresCas.parametresApporDeversoirs.deversLate.numBranche= null;
	    parametresCas.parametresApporDeversoirs.deversLate.numLoi= null;
    parametresCas.parametresCalage = new SParametresCalage();
    parametresCas.parametresCalage.frottement = new SParametresFrottement();
    parametresCas.parametresCalage.frottement.loi = 1;
    parametresCas.parametresCalage.frottement.nbZone = 1;
    parametresCas.parametresCalage.frottement.numBranche = vInt1;
    parametresCas.parametresCalage.frottement.absDebZone = vDouble0;
    double[] vDouble18000 = { 18000 };
    parametresCas.parametresCalage.frottement.absFinZone = vDouble18000;
    double[] vDouble9999 = { 9999 };
    parametresCas.parametresCalage.frottement.coefLitMin = vDouble9999;
    parametresCas.parametresCalage.frottement.coefLitMaj = vDouble9999;
    parametresCas.parametresCalage.zoneStockage = new SParametresZoneStockage(0, vInt1, vDouble0, vDouble0);
    SParametresLoi[] lois = new SParametresLoi[2];
    double[] tps = { 0, 25, 50, 75, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000 };
    double[] cote = { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    double[] debit = { 2000, 1936.1034, 1872.9678, 1810.5895, 1748.9646, 1509.9228, 1282.63, 1066.8414, 862.3125,
        668.7986, 486.0551, 313.8372, 151.9004, 0, 0 };
    lois[0] = new SParametresLoi(" amont", 1, new SParametresDonneesLoi(2, " ", 1, 15, tps, cote, cote, debit, 0));
    double[] tps2 = { 0, 2000 };
    double[] cote2 = { 15, 15 };
    double[] debit2 = { 0, 1 };
    lois[1] = new SParametresLoi(" aval", 2, new SParametresDonneesLoi(2, " ", 1, 2, tps2, cote2, cote2, debit2, 0));
    parametresCas.parametresLoisHydrau = new SParametresLoisHydrau(2, lois);
    int[] branch = { 1, 1 };
    double[] abs = { 0, 18000 };
    double[] cotes = { 15, 15 };
    double[] debits = { 2000, 2000 };
    SParametresReprEtude repriseEtude = new SParametresReprEtude(false, " ", "STD");
    SParametresLigEau ligneEau = new SParametresLigEau(true, 2, " ", 2, 2, branch, abs, cotes, debits, null, null);
    SParametresZoneSeche zoneSeches = new SParametresZoneSeche(0, null, null, null);
    parametresCas.parametresCondInit = new SParametresCondInit(repriseEtude, ligneEau, zoneSeches);
    SParametresImpress impress = new SParametresImpress(true, true, true, true, true, false);
    SParametresPasStock pasStock = new SParametresPasStock(1, 1000, 1000);
    SParametresResult resultat = new SParametresResult("Test1\\RESULTATS\\optyca.txt", null, "STD", 2);
    SParametresListing listing = new SParametresListing("Test1\\RESULTATS\\listing.txt");
    SParametresFichReprise fichRep = new SParametresFichReprise("repriseecr", "STD");
    SParametresRubens rubens = new SParametresRubens(1);
    SParametresStockage stock = new SParametresStockage(1, 0, vInt1, vDouble0);
    SParametresFichResCasier fichResCasier = new SParametresFichResCasier();
    parametresCas.parametresImpressResult = new SParametresImpressResult(" toto", impress, pasStock, resultat, listing,
        fichRep, rubens, stock, fichResCasier);
    boolean[] varCalcule = { false, false, false, false, false, false, false, true, false, false, true, false, false };
    parametresCas.parametresVarCalc = new SParametresVarCalc(varCalcule);
    boolean[] varStock = { true, false, false, false, false, true, true, false, false, false, false, false, false,
        false, false, false, false, true, false, false, true, false, false, false, false, false, false, false, false,
        false, false, false, false, false, false, false, false, false, false, false, false, false };
    parametresCas.parametresVarStock = new SParametresVarStock(varStock);
    return parametresCas;
  }

  public static SParametresGEO getGeo() {
    SParametresGEO parametresGEO = new SParametresGEO();
    parametresGEO.biefs = new SParametresBief[1];
    parametresGEO.biefs[0].nom = "Bief_1";
    parametresGEO.biefs[0].profils = new SParametresProfil[2];
    SParametresPt[] pts = new SParametresPt[4];
    for (int i = 0; i < pts.length; i++) {
      pts[i] = new SParametresPt();
      pts[i].lit = "B";
    }
    pts[0].x = 0;
    pts[1].x = 0;
    pts[2].x = 200;
    pts[3].x = 200;
    pts[0].y = 20;
    pts[1].y = 10;
    pts[2].y = 10;
    pts[3].y = 20;
    parametresGEO.biefs[0].profils[0] = new SParametresProfil("AMONT_BIEF", 0, pts,false,"");
    parametresGEO.biefs[0].profils[1] = new SParametresProfil("AVAL_BIEF", 18000, pts,false,"");
    return parametresGEO;
  }

  public static SParametresLIG getLig() {
    return lig;
  }

  public static SParametresREP getInRep() {
    return inRep;
  }

  public static SResultatsLIS getLis() {
    return lis;
  }

  public static SResultatsOPT getOutOpt() {
    return outOpt;
  }

  public static SResultatsRUB getRub() {
    return rub;
  }

  public static SResultatsEcran getEcran() {
    return ecran;
  }

  public static SResultatsMAI getMai() {
    return mai;
  }

  public static SParametresREP getOutRep() {
    return outRep;
  }
}
