/**
 * @file         Opthyca1DWriter.java
 * @creation     2000-05-10
 * @modification $Date: 2007-11-20 11:43:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2005 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatialBief;

/**
 * Classe permettant d'�crire un fichier r�sultat au format OPTHYCA et � partir
 * d'un ��SResultatsTemporelSpatial��.
 * @version      $Revision: 1.6 $ $Date: 2007-11-20 11:43:01 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Opthyca1DWriter {
  private PrintWriter fw_;
  public Opthyca1DWriter(File file) throws FichierMascaretException {
    try {
      fw_=new PrintWriter(new BufferedWriter(new FileWriter(file)));
    }
    catch (IOException ex) {
      throw new FichierMascaretException("","Fichier impossible � ouvrir :"+file.getName());
    }
  }
  public void write(SResultatsTemporelSpatial sres) throws FichierMascaretException {
    try {
      fw_.println("[variables]");
      // description variable
      int nbVar = sres.variables.length;
      DecimalFormat[] formaters = new DecimalFormat[nbVar];
      DecimalFormatSymbols formatSymbols=new DecimalFormatSymbols();
      formatSymbols.setDecimalSeparator('.');

      DecimalFormat formaterAbsc = new DecimalFormat("#.#####",formatSymbols);
      formaterAbsc.setGroupingUsed(false);
      formaterAbsc.setDecimalSeparatorAlwaysShown(true);
      formaterAbsc.setMaximumFractionDigits(2);
      formaterAbsc.setMinimumFractionDigits(2);

      DecimalFormat formaterTps = new DecimalFormat("#.#####",formatSymbols);
      formaterTps.setGroupingUsed(false);
      formaterTps.setDecimalSeparatorAlwaysShown(true);
      formaterTps.setMaximumFractionDigits(1);
      formaterTps.setMinimumFractionDigits(1);

      for (int i= 0; i < nbVar; i++) {
        fw_.print("\"" + sres.variables[i].nomLong + "\";");
        fw_.print("\"" + sres.variables[i].nomCourt + "\";");
        fw_.print("\"" + sres.variables[i].unite + "\";");
        int nbDecimal = sres.variables[i].nbDecimal;
        fw_.println(nbDecimal);
        formaters[i] = new DecimalFormat("#.#####",formatSymbols);
        formaters[i].setGroupingUsed(false);
        formaters[i].setDecimalSeparatorAlwaysShown(true);
        formaters[i].setMaximumFractionDigits(nbDecimal);
        formaters[i].setMinimumFractionDigits(nbDecimal);
      }
      // entete resultat
      fw_.println("[resultats]");
      int nbPasTps = sres.pasTemps.length;

      //Pour les r�sultats de casier et de liason
      if (sres.resultatsCasier||sres.resultatsLiaison) {
          int nbBief = sres.resultatsBiefs.length;
          // description variable
          for (int i = 0; i < nbPasTps; i++) {
              double tps = sres.pasTemps[i];
              int numSection = 1;
              for (int j = 0; j < nbBief; j++) {
                  String codeBief = getCodeBief(j + 1);
                  SResultatsTemporelSpatialBief resuBief = sres.resultatsBiefs[j];
                  for (int k = 0; k < resuBief.abscissesSections.length; k++) {
                      String codeSection = getCodeSection(numSection++);
                      double absc = resuBief.abscissesSections[k];
                      // ecriture du d�but de la ligne
                      fw_.print(to9CharFromPt(formaterTps.format(tps)));
                      fw_.print(";\"");
                      fw_.print(codeBief);
                      fw_.print("\"");
                      double[][][] vals = resuBief.valeursVariables;
                      // ecriture des valeurs des diff�rentes variables
                      for (int l = 0; l < nbVar; l++) {
                          fw_.print(";");
                          fw_.print(to9CharFromPt(formaters[l].format(vals[l][i][k])));
                      }
                      fw_.println();
                  }
              }
          }
      } else {
      //Pour les r�sultats de ligne d'eau (bief)
      int nbBief = sres.resultatsBiefs.length;
         // description variable
         for (int i = 0; i < nbPasTps; i++) {
             double tps = sres.pasTemps[i];
             int numSection = 1;
             for (int j = 0; j < nbBief; j++) {
                 String codeBief = getCodeBief(j + 1);
                 SResultatsTemporelSpatialBief resuBief = sres.resultatsBiefs[j];
                 for (int k = 0; k < resuBief.abscissesSections.length; k++) {
                     String codeSection = getCodeSection(numSection++);
                     double absc = resuBief.abscissesSections[k];
                     // ecriture du d�but de la ligne
                     fw_.print(to9CharFromPt(formaterTps.format(tps)));
                     fw_.print(";\"");
                     fw_.print(codeBief);
                     fw_.print("\";\"");
                     fw_.print(codeSection);
                     fw_.print("\";");
                     fw_.print(to9CharFromPt(formaterAbsc.format(absc)));

                     double[][][] vals = resuBief.valeursVariables;
                     // ecriture des valeurs des diff�rentes variables
                     for (int l = 0; l < nbVar; l++) {
                         fw_.print(";");
                         fw_.print(to9CharFromPt(formaters[l].format(vals[l][i][k])));
                     }
                     fw_.println();
                 }
             }
         }
      }
      fw_.close();
    }
    catch (Throwable ex) {
      ex.printStackTrace();
      throw new FichierMascaretException(null,ex.getLocalizedMessage());
    }
  }

  private final static String getCodeBief(int numBief) {
    if (numBief < 10) return "  "+numBief;
    if (numBief < 100) return " "+numBief;
    if (numBief < 1000) return Integer.toString(numBief);
    else return "***";
  }

  private final static String getCodeSection(int numSec) {
    if (numSec < 10) return "   "+numSec;
    if (numSec < 100) return "  "+numSec;
    if (numSec < 1000) return " "+numSec;
    if (numSec < 10000) return Integer.toString(numSec);
    else return "****";
  }
  private final static String to9CharFromPt(String chaine) {
    int indexPt = chaine.indexOf(".");
    if (indexPt >9) {
      return "*********";
    } else if (indexPt == 9) {
      return chaine;
    } else if (indexPt == 8) {
      return " "+chaine;
    } else if (indexPt == 7) {
      return "  "+chaine;
    } else if (indexPt == 6) {
      return "   "+chaine;
    } else if (indexPt == 5) {
      return "    "+chaine;
    } else if (indexPt == 4) {
      return "     "+chaine;
    } else if (indexPt == 3) {
      return "      "+chaine;
    } else if (indexPt == 2) {
      return "       "+chaine;
    } else if (indexPt == 1) {
      return "        "+chaine;
    } else {
      return "      NAN";
    }
  }
  public static void main(String[] args) {
    try {
      String nomFichier = "test3BiefsPetitPermanent.opt";
      if (args.length > 0) {
        File file = new File(args[0]);
        if (file.exists()) {
          nomFichier = args[0];
        }
      }
      File file = new File(nomFichier);
      Opthyca1DReader rubReader = new Opthyca1DReader(file);
      SResultatsTemporelSpatial res = rubReader.read();

      Opthyca1DWriter rubWriter = new Opthyca1DWriter(new File("bis"+nomFichier));
      rubWriter.write(res);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
