/*
 * @file         Rubens1DNonPermReader.java
 * @creation     2004-03-01
 * @modification $Date: 2008-03-04 15:46:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatialBief;
import org.fudaa.dodico.corba.mascaret.SResultatsVariable;
import org.fudaa.dodico.fortran.FortranBinaryInputStream;

/**
 * Classe permettant de lire un fichier r�sultat au format RUBENS non permanent
 * (binaire) et de produire un ��SResultatsTemporelSpatial��.
 * @version      $Revision: 1.13 $ $Date: 2008-03-04 15:46:52 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
class Rubens1DNonPermReader extends Rubens1DReader {
  private FortranBinaryInputStream is_;
  private FileInputStream fis_;
  private final static String NOM_FIN="FIN ";
  public Rubens1DNonPermReader(File file) throws FichierMascaretException {
    super(file);
    try {
      fis_ = new FileInputStream(file());
      is_ = new FortranBinaryInputStream(new BufferedInputStream(fis_), true, ByteOrder.BIG_ENDIAN);
    }
    catch (FileNotFoundException ex) {
      throw new FichierMascaretException("","Fichier introuvable : "+file().getName());
    }
    catch (IOException ex) {
      throw new FichierMascaretException("","Probl�me d'entr�e-sortie sur le fichier : "+file().getName());
    }
  }
  @Override
  public SResultatsTemporelSpatial read() throws FichierMascaretException {
    try {
      CtuluLibMessage.println("premi�re passe");
      CtuluLibMessage.println("lecture ent�te");
      is_.readRecord(); // titre 1
      is_.readRecord(); // titre 2
      is_.readRecord(); // titre 3
      is_.readRecord(); // NOM_IDEB_BIEF
      is_.readRecord(); // NOM_IFIN_BIEF
      is_.readRecord(); // NOM_FIN

      int[] tabNbBief = readIntegerRecord(); // nb de bief
      int nbBief = tabNbBief[0];
      int[] numSectionOrigine = readIntegerRecord();
      CtuluLibMessage.println("numSectionOrigine=",numSectionOrigine);
      int[] numSectionFin = readIntegerRecord();
      CtuluLibMessage.println("numSectionFin=",numSectionFin);
      ArrayList<String> nomsVarIndependanteTemps = new ArrayList<String>(4);
      String nomVar=null;
      while (!NOM_FIN.equals(nomVar)) {
        nomVar = readStringRecord();
        if (!NOM_FIN.equals(nomVar)) {
          nomsVarIndependanteTemps.add(nomVar.trim());
        }
      }
      CtuluLibMessage.println("nomsVarIndependanteTemps="+nomsVarIndependanteTemps);
      int[] tabNbSecEff = readIntegerRecord();
      CtuluLibMessage.println("tabNbSecEff",tabNbSecEff);
      int nbSecEff = tabNbSecEff[0];
      int nbVarIndependanteTemps = nomsVarIndependanteTemps.size();
      float[][] valeurVarIndependante = new float[nbVarIndependanteTemps][];
      for (int i = 0; i < nbVarIndependanteTemps; i++) {
        valeurVarIndependante[i] = readFloatRecord();
      }
      ArrayList<String> nomsVarDependanteTemps = new ArrayList<String>();
      nomVar="";
      while (!NOM_FIN.equals(nomVar)) {
        nomVar = readStringRecord();
        if (!NOM_FIN.equals(nomVar)) {
          nomsVarDependanteTemps.add(nomVar.trim());
        }
      }
      CtuluLibMessage.println("nomsVarDependanteTemps="+nomsVarDependanteTemps);
      int nbVarDependanteTemps=nomsVarDependanteTemps.size();
      CtuluLibMessage.println("lecture donn�es temporelles (passe 1)");
      int nbPasTemps = 0;
      while(is_.available()>10) {
        is_.readRecord(); // numPasTemps
        nbPasTemps++;
        is_.readRecord(); // Temps
        is_.readRecord(); // nbSecEff
        for (int i = 0; i < nbVarDependanteTemps; i++) {
          readFloatRecord();  // valeurs variables d�pendantes temps, n�cessaire
                              // autrement isAvailable n'est pas correcte !
        }
      }
      fis_.close();
      is_.close();
      CtuluLibMessage.println("Fin passe 1");
      CtuluLibMessage.println("nbBief="+nbBief);
      CtuluLibMessage.println("nbSecEff="+nbSecEff);
      CtuluLibMessage.println("nbVarDependanteTemps="+nbVarDependanteTemps);
      CtuluLibMessage.println("nbVarIndependanteTemps="+nbVarIndependanteTemps);
      CtuluLibMessage.println("nbPasTemps="+nbPasTemps);
      CtuluLibMessage.println("nomsVarDependanteTemps="+nomsVarDependanteTemps);
      CtuluLibMessage.println("nomsVarIndependanteTemps="+nomsVarIndependanteTemps);
      CtuluLibMessage.println("numSectionOrigine", numSectionOrigine);
      CtuluLibMessage.println("numSectionFin", numSectionFin);
      CtuluLibMessage.println("valeurVarIndependante", valeurVarIndependante);
      int[] nbSecParBief = new int[nbBief];
      for (int i = 0; i < nbSecParBief.length; i++) {
        nbSecParBief[i]=numSectionFin[i]-numSectionOrigine[i]+1;
      }
      CtuluLibMessage.println("nbSecParBief", nbSecParBief);
      SResultatsTemporelSpatial res = new SResultatsTemporelSpatial();
      res.resultatsPermanent = false;
 
      if (DescriptionVariables.isContientNomsLiaison(nomsVarDependanteTemps)) {
        res.resultatsLiaison = true;
        res.resultatsCasier = false;
        res.resultatsCalageAuto = false;
        res.resultatsTracer = false;
      } else if (DescriptionVariables.isContientNomsCasier(nomsVarIndependanteTemps)) {//nomsVarIndependanteTemps
        res.resultatsLiaison = false;
        res.resultatsCasier = true;
        res.resultatsCalageAuto = false;
        res.resultatsTracer = false;
      } else if (DescriptionVariables.isContientNomsCalageAuto(nomsVarIndependanteTemps)) {//nomsVarIndependanteTemps
          res.resultatsLiaison = false;
          res.resultatsCasier = false;
          res.resultatsCalageAuto = true;
          res.resultatsTracer = false;
      } else if (DescriptionVariables.isContientNomsTracer(nomsVarIndependanteTemps)) {//nomsVarIndependanteTemps
          res.resultatsLiaison = false;
          res.resultatsCasier = false;
          res.resultatsCalageAuto = false;
          res.resultatsTracer = true;
      } else {
        res.resultatsLiaison = false;
        res.resultatsCasier = false;
        res.resultatsCalageAuto = false;
        res.resultatsTracer = false;
      }
      CtuluLibMessage.println("resultatsLiaison="+ res.resultatsLiaison);
      CtuluLibMessage.println("resultatsCasier="+ res.resultatsCasier);

      int nbVarTotal = nbVarDependanteTemps + nbVarIndependanteTemps;
      res.variables = new SResultatsVariable[nbVarTotal-1];
      for (int i = 1; i < nbVarIndependanteTemps; i++) {
        res.variables[i-1] = new SResultatsVariable();
        res.variables[i-1].nomLong ="";
        res.variables[i-1].nomCourt = (String)nomsVarIndependanteTemps.get(i);
        res.variables[i-1].nbDecimal = 4;
        res.variables[i-1].unite = "";
      }
      for (int i = 0; i < nbVarDependanteTemps; i++) {
        int iVar = i+nbVarIndependanteTemps-1;
        res.variables[iVar] = new SResultatsVariable();
        res.variables[iVar].nomLong = "";
        res.variables[iVar].nomCourt = (String)nomsVarDependanteTemps.get(i);
        res.variables[iVar].nbDecimal = 4;
        res.variables[iVar].unite = "";
      }
      
      if (res.resultatsLiaison || res.resultatsCasier) {
        allocationPasse2CasierLiaison(nbBief, nbVarIndependanteTemps,
                                 valeurVarIndependante, nbVarDependanteTemps,
                                 nbPasTemps, nbSecParBief, res);

      } else {
        allocationPasse2Mascaret(nbBief, nbVarIndependanteTemps,
                                 valeurVarIndependante, nbVarDependanteTemps,
                                 nbPasTemps, nbSecParBief, res);
      }


      return res;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      throw new FichierMascaretException(is_.getCurrentPosition(),null,
                                         ex.getMessage());
    }
  }

  /**
   * Effectue l'allocation et la 2�me passe du fichier dans le cas de fichier Mascaret.
   * @param nbBief Nombre de bief.
   * @param nbVarIndependanteTemps Nombre de variable ind�pendante du temps (notament X).
   * @param valeurVarIndependante Les valeurs des variables ind�pendante du temps (notament X).
   * @param nbVarDependanteTemps Nombre de variable d�pendante du temps.
   * @param nbPasTemps Nombre de pas de temps.
   * @param nbSecParBief Contient le nombre de section de calcul pour chaque biefs.
   * @param res Structure de donn�e r�sultat � remplir.
   * @throws IOException
   * @throws FichierMascaretException
   */
  private void allocationPasse2Mascaret(int nbBief,
                                        int nbVarIndependanteTemps,
                                        float[][] valeurVarIndependante,
                                        int nbVarDependanteTemps,
                                        int nbPasTemps, int[] nbSecParBief,
                                        SResultatsTemporelSpatial res)
               throws IOException {
    int nbVarTotal = nbVarDependanteTemps+nbVarIndependanteTemps;
    double[][] abcisseSectionParBief = split(valeurVarIndependante[0], nbSecParBief);

    double[][][] valVarIndepParBief = new double[nbVarIndependanteTemps-1][][];
    for (int i = 1; i < nbVarIndependanteTemps; i++) {
      valVarIndepParBief[i-1] = split(valeurVarIndependante[i], nbSecParBief);
    }

    // allocation
    res.pasTemps = new double[nbPasTemps];
    res.resultatsBiefs = new SResultatsTemporelSpatialBief[nbBief];
    for (int i = 0; i < nbBief; i++) {
      res.resultatsBiefs[i] = new SResultatsTemporelSpatialBief();
      res.resultatsBiefs[i].abscissesSections = abcisseSectionParBief[i];
      res.resultatsBiefs[i].valeursVariables = new double[nbVarTotal-1][nbPasTemps][nbSecParBief[i]];
    }
    // fin allocation

    CtuluLibMessage.println("deuxi�me passe");
    fis_ = new FileInputStream(file());
    is_ = new FortranBinaryInputStream(new BufferedInputStream(fis_),true, ByteOrder.BIG_ENDIAN);
    CtuluLibMessage.println("lecture ent�te");
    is_.readRecord(); // titre 1
    is_.readRecord(); // titre 2
    is_.readRecord(); // titre 3
    is_.readRecord(); // NOM_IDEB_BIEF
    is_.readRecord(); // NOM_IFIN_BIEF
    is_.readRecord(); // NOM_FIN

    is_.readRecord(); // tabNbBief
    is_.readRecord(); // numSectionOrigine
    is_.readRecord(); // numSectionFin

    String nomVar=null;
    while (!NOM_FIN.equals(nomVar)) {
      nomVar = readStringRecord();
    }
    is_.readRecord(); // tabNbSecEff
    for (int i = 0; i < nbVarIndependanteTemps; i++) {
      is_.readRecord(); // valeurVarIndependante
    }
    nomVar="";
    while (!NOM_FIN.equals(nomVar)) {
      nomVar = readStringRecord();
    }
    CtuluLibMessage.println("lecture donn�es temporelles (passe 2)");
    int iPasTemps = -1;
    while(is_.available()>10) {
      is_.readRecord(); // numPasTemps
      iPasTemps++;
      float tps = readFloatRecord()[0]; // le temps
      res.pasTemps[iPasTemps] = tps;
      is_.readRecord(); // nbSecEff, nbSecEff

      for (int i = 0; i < nbBief; i++) {
        for (int j = 0; j < (nbVarIndependanteTemps-1); j++) {
          res.resultatsBiefs[i].valeursVariables[j][iPasTemps] = valVarIndepParBief[j][i];
        }
      }

      for (int i = 0; i < nbVarDependanteTemps; i++) {
        float[] vali = readFloatRecord();
        double[][] valParBief = split(vali,nbSecParBief);
        int iVar = i+nbVarIndependanteTemps-1;
        for (int j = 0; j < nbBief; j++) {
          res.resultatsBiefs[j].valeursVariables[iVar][iPasTemps]=valParBief[j];
        }
      }
    }
    is_.close();
  }

  /**
   * Effectue l'allocation et la 2�me passe du fichier dans le cas de fichier Liaison ou Casier.
   * Dans ces 2 cas, les r�sultats ne sont pas normalement spatiaux, mais en fait le format
   * actuel cr�� artificielement des sections de calcul diff�rentes d'abscisse 1, 2, 3, ...
   * autant qu'il existe de casier ou de liaison.
   * @param nbBief doit normalement �tre �gale � 1.
   * @param nbVarIndependanteTemps Nombre de variable ind�pendante du temps (notament X).
   * @param valeurVarIndependante les valeurs des variables ind�pendante du temps (notament X).
   * @param nbVarDependanteTemps Nombre de variable d�pendante du temps.
   * @param nbPasTemps Nombre de pas de temps.
   * @param nbSecParBief Contient une valeur indiquant le nombre de liaison ou de casier.
   * @param res Structure de donn�e r�sultat � remplir.
   * @throws IOException
   * @throws FichierMascaretException
   */
  private void allocationPasse2CasierLiaison(int nbBief,
                                        int nbVarIndependanteTemps,
                                        float[][] valeurVarIndependante,
                                        int nbVarDependanteTemps,
                                        int nbPasTemps, int[] nbSecParBief,
                                        SResultatsTemporelSpatial res)
                throws IOException, FichierMascaretException {
    if (nbBief != 1) {
      throw new FichierMascaretException(nbBief,
                                   "Fichier rubens Liaison ou Casier invalide");
    }

    int nbCasierLiaison = nbSecParBief[0];

    int nbVarTotal = nbVarDependanteTemps+nbVarIndependanteTemps;



    double[][] valVarIndepParCasierLiaison = new double[nbCasierLiaison][nbVarIndependanteTemps-1];
    for (int i = 1; i < nbVarIndependanteTemps; i++) {
      for (int j = 0; j < nbCasierLiaison; j++) {
        valVarIndepParCasierLiaison[j][i-1] = valeurVarIndependante[i][j];
      }
    }

    double[] absSec = new double[1];
    // allocation
    res.pasTemps = new double[nbPasTemps];
    res.resultatsBiefs = new SResultatsTemporelSpatialBief[nbCasierLiaison];
    for (int i = 0; i < nbCasierLiaison; i++) {
      res.resultatsBiefs[i] = new SResultatsTemporelSpatialBief();
      res.resultatsBiefs[i].abscissesSections = absSec;
      res.resultatsBiefs[i].valeursVariables = new double[nbVarTotal-1][nbPasTemps][1];
    }
    // fin allocation

    CtuluLibMessage.println("deuxi�me passe");
    fis_ = new FileInputStream(file());
    is_ = new FortranBinaryInputStream(new BufferedInputStream(fis_),true);
    CtuluLibMessage.println("lecture ent�te");
    is_.readRecord(); // titre 1
    is_.readRecord(); // titre 2
    is_.readRecord(); // titre 3
    is_.readRecord(); // NOM_IDEB_BIEF
    is_.readRecord(); // NOM_IFIN_BIEF
    is_.readRecord(); // NOM_FIN

    is_.readRecord(); // tabNbBief
    is_.readRecord(); // numSectionOrigine
    is_.readRecord(); // numSectionFin

    String nomVar=null;
    while (!NOM_FIN.equals(nomVar)) {
      nomVar = readStringRecord();
    }
    is_.readRecord(); // tabNbSecEff
    for (int i = 0; i < nbVarIndependanteTemps; i++) {
      is_.readRecord(); // valeurVarIndependante
    }
    nomVar="";
    while (!NOM_FIN.equals(nomVar)) {
      nomVar = readStringRecord();
    }
    CtuluLibMessage.println("lecture donn�es temporelles (passe 2)");
    int iPasTemps = -1;
    while(is_.available()>10) {
      is_.readRecord(); // numPasTemps
      iPasTemps++;
      float tps = readFloatRecord()[0]; // le temps
      res.pasTemps[iPasTemps] = tps;
      is_.readRecord(); // nbSecEff, nbSecEff

      for (int i = 0; i < nbCasierLiaison; i++) {
        for (int j = 0; j < (nbVarIndependanteTemps-1); j++) {
          res.resultatsBiefs[i].valeursVariables[j][iPasTemps][0] = valVarIndepParCasierLiaison[i][j];
        }
      }

      for (int i = 0; i < nbVarDependanteTemps; i++) {
        float[] vali = readFloatRecord();
        int iVar = i+nbVarIndependanteTemps-1;
        for (int j = 0; j < nbCasierLiaison; j++) {
          res.resultatsBiefs[j].valeursVariables[iVar][iPasTemps][0]=vali[j];
        }
      }
    }
    is_.close();
  }
  public static void main(String[] args) {
    try {
      String nomFichier = "TestRubExportModifie.rub";//"mascaret1.rub";//"TestRubExport.rub";
     /* if (args.length > 0) {
        nomFichier = args[0];
      }*/
      File file = new File(nomFichier);
      //CtuluLibMessage.DEBUG = true;
      Rubens1DNonPermReader rr = new Rubens1DNonPermReader(file);
      rr.read();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  private String readStringRecord() throws IOException {
    is_.readRecord();
    int longueur = is_.getSequentialRecordLength();
    return is_.readCharacter(longueur);
  }
  private int[] readIntegerRecord() throws IOException {
    is_.readRecord();
    int longueur = is_.getSequentialRecordLength();
    int nbInt = longueur/4;
    int[] res = new int[nbInt];
    for (int i = 0; i < nbInt; i++) {
      res[i]= is_.readInteger();
    }
    return res;
  }
  private float[] readFloatRecord() throws IOException {
    is_.readRecord();
    int longueur = is_.getSequentialRecordLength();
    int nbFloat = longueur/4;
    float[] res = new float[nbFloat];
    for (int i = 0; i < nbFloat; i++) {
      res[i]= is_.readReal();
    }
    return res;
  }
  /*private double[] readDoubleRecord() throws IOException {
    is_.readRecord();
    int longueur = is_.getSequentialRecordLength();
    int nbDouble = longueur/8;
    double[] res = new double[nbDouble];
    for (int i = 0; i < nbDouble; i++) {
      res[i]= is_.readDoublePrecision();
    }
    return res;
  }*/
  private static double[][] split(float[] tabVal, int nbSecParBief[]) {
    int nbBief = nbSecParBief.length;
    int itabVal=0;
    double[][] res = new double[nbBief][];
    for (int i = 0; i < nbBief; i++) {
      int nbSec = nbSecParBief[i];
      res[i] = new double[nbSec];
      for (int j = 0; j < nbSec; j++) {
        res[i][j] = tabVal[itabVal];
        itabVal++;
      }
    }
    return res;
  }

}
