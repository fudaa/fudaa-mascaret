/*
 * @file         Rubens1DNonPermWriter.java
 * @creation     2004-03-07
 * @modification $Date: 2007-11-20 11:43:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatialBief;
import org.fudaa.dodico.fortran.FortranBinaryOutputStream;

/**
 * Classe permettant d'�crire un fichier r�sultat au format RUBENS non permanent
 * (binaire) � partir d'un ��SResultatsTemporelSpatial��.
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:43:01 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Rubens1DNonPermWriter {
  private FortranBinaryOutputStream os_;
  private FileOutputStream fos_;
  private final static String NOM_FIN      ="FIN ";
  private final static String NOM_IDEB_BIEF="I1  ";
  private final static String NOM_IFIN_BIEF="I2  ";
  private final static String TITRE="                                                                        ";
  private int nbSectionTotal_=0;

  public Rubens1DNonPermWriter(File file) throws FichierMascaretException {
    try {
      fos_ = new FileOutputStream(file);
      os_ = new FortranBinaryOutputStream(new BufferedOutputStream(fos_), true);
    }
    catch (IOException ex) {
      throw new FichierMascaretException("","Probl�me d'entr�e-sortie sur le fichier : "+file.getName());
    }
  }
  public void write(SResultatsTemporelSpatial sres) throws FichierMascaretException {
    try {
      writeStringRecord(TITRE);// titre 1
      writeStringRecord(TITRE);// titre 2
      writeStringRecord(TITRE);// titre 3

      writeStringRecord(NOM_IDEB_BIEF); // NOM_IDEB_BIEF
      writeStringRecord(NOM_IFIN_BIEF); // NOM_IFIN_BIEF
      writeStringRecord(NOM_FIN);       // NOM_FIN

      boolean resultatsMascaret = (!sres.resultatsCasier && !sres.resultatsLiaison);
      if (resultatsMascaret) {
        int nbBief = sres.resultatsBiefs.length;
        writeIntegerRecord(nbBief,nbBief); // NbBief, NbBief
        int[] numSectionOrigine = getNumSectionOrigine(sres.resultatsBiefs);
        writeIntegerRecord(numSectionOrigine); // origine bief
        int[] numSectionFin = getNumSectionFin(sres.resultatsBiefs);
        writeIntegerRecord(numSectionFin); // fin bief

        // Ecriture des noms des variables ind�pendante du temps
        ArrayList<Integer> indicesVarIndependantTps=new ArrayList<Integer>();
        ArrayList<Integer> indicesVarDependantTps=new ArrayList<Integer>();
        int nbVarTotal = sres.variables.length;
        ecritureNomsVarIndepTps(sres, indicesVarIndependantTps,
                                indicesVarDependantTps, nbVarTotal);

        // Valeurs des valeurs ind�pendante du temps
        nbSectionTotal_ = getNbSectionTotal(sres.resultatsBiefs);
        ecritureValeursIndepTps(sres, indicesVarIndependantTps);

        // Ecriture des noms des variables d�pendantes du temps
        int nbVarDependantTps = indicesVarDependantTps.size();
        ecritureNomsvarDepTps(sres, indicesVarDependantTps, nbVarDependantTps);

        // Valeurs des valeurs d�pendantes du temps
        ecritureValeursDepTps(sres, indicesVarDependantTps, nbVarDependantTps);
      } else { // Casier ou Liaison
        //les diff�rents casiers ou liaisons sont �crit dans diff�rentes section de calcul
        writeIntegerRecord(1,1); // NbBief, NbBief
        writeIntegerRecord(1);   // origine section
        int nbCasierLiaison = sres.resultatsBiefs.length;
        writeIntegerRecord(nbCasierLiaison); // fin section
        // Ecriture des noms des variables ind�pendante du temps
        ArrayList<Integer> indicesVarIndependantTps=new ArrayList<Integer>();
        ArrayList<Integer> indicesVarDependantTps=new ArrayList<Integer>();
        int nbVarTotal = sres.variables.length;
        ecritureNomsVarIndepTps(sres, indicesVarIndependantTps,
                                indicesVarDependantTps, nbVarTotal);

        // Valeurs des valeurs ind�pendante du temps
        nbSectionTotal_ = -1;
        ecritureValeursIndepTpsCasierLiaison(sres, indicesVarIndependantTps);

        // Ecriture des noms des variables d�pendante du temps
        int nbVarDependantTps = indicesVarDependantTps.size();
        ecritureNomsvarDepTps(sres, indicesVarDependantTps, nbVarDependantTps);

        // Valeurs des valeurs d�pendante du temps
        ecritureValeursDepTpsCasierLiaison(sres, indicesVarDependantTps, nbVarDependantTps);
      }
      os_.close();
      fos_.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      throw new FichierMascaretException(null,
                                         ex.getMessage());
    }
  }

  private void ecritureValeursDepTps(SResultatsTemporelSpatial sres,
                                     ArrayList<Integer> indicesVarDependantTps,
                                     int nbVarDependantTps) throws IOException {
    for (int i = 0; i < sres.pasTemps.length; i++) {
      writeIntegerRecord(i+1, i+1);
      float tps = (float)sres.pasTemps[i];
      writeFloatRecord(tps,tps);
      writeIntegerRecord(nbSectionTotal_,nbSectionTotal_);
      for (int j = 0; j < nbVarDependantTps; j++) {
        int indiceVar = indicesVarDependantTps.get(j).intValue();
        float[] valeurs = getValeursVar(indiceVar,i,sres.resultatsBiefs);
        writeFloatRecord(valeurs);
      }
    }
  }

  private void ecritureNomsvarDepTps(SResultatsTemporelSpatial sres,
                                     ArrayList<Integer> indicesVarDependantTps,
                                     int nbVarDependantTps) throws IOException {
    for (int i = 0; i < nbVarDependantTps; i++) {
      int indiceVar = indicesVarDependantTps.get(i).intValue();
      String nomVar = sres.variables[indiceVar].nomCourt;
      writeStringRecord(to4Char(nomVar));
    }
    writeStringRecord(NOM_FIN); // �criture de l'indicateur de fin
  }

  private void ecritureValeursIndepTps(SResultatsTemporelSpatial sres,
                                       ArrayList<Integer> indicesVarIndependantTps) throws
      IOException {
    writeIntegerRecord(nbSectionTotal_,nbSectionTotal_); // NbSecEff, NbSecEff
    // D'abord �criture des abscisses
    float[] abscisses = getAbscisse(sres.resultatsBiefs);
    writeFloatRecord(abscisses);
    // Ensuite �criture des autres variables
    int nbVarIndependantTps = indicesVarIndependantTps.size();
    for (int i = 0; i < nbVarIndependantTps; i++) {
      int indiceVar = indicesVarIndependantTps.get(i).intValue();
      float[] valeurs = getValeursVar(indiceVar,0,sres.resultatsBiefs);
      writeFloatRecord(valeurs);
    }
  }

  private void ecritureNomsVarIndepTps(SResultatsTemporelSpatial sres,
                                       ArrayList<Integer> indicesVarIndependantTps,
                                       ArrayList<Integer> indicesVarDependantTps,
                                       int nbVarTotal) throws IOException {
    writeStringRecord(to4Char("X")); // d'abord l'abscisse
    for (int i = 0; i < nbVarTotal; i++) {
      String nomVar = sres.variables[i].nomCourt;
      if (DescriptionVariables.isIndependantTps(nomVar)) {
        writeStringRecord(to4Char(nomVar));
        indicesVarIndependantTps.add(new Integer(i));
      } else {
        indicesVarDependantTps.add(new Integer(i));
      }
    }
    writeStringRecord(NOM_FIN); // �criture de l'indicateur de fin
  }

  private void ecritureValeursIndepTpsCasierLiaison(SResultatsTemporelSpatial
      sres, ArrayList<Integer> indicesVarIndependantTps) throws IOException {
    int nbCasierLiaison = sres.resultatsBiefs.length;
    writeIntegerRecord(nbCasierLiaison,nbCasierLiaison); // NbSecEff, NbSecEff
    // D'abord �criture des abscisses qui sont en fait des numeros de section.
    for (int i = 0; i < nbCasierLiaison; i++) {
      os_.writeReal(/*(float)*/(i+1));
    }
    os_.writeRecord();

    int nbVar = indicesVarIndependantTps.size();
    for (int i = 0; i < nbVar; i++) {
      for (int j = 0; j < nbCasierLiaison; j++) {
        SResultatsTemporelSpatialBief resCasierLiaisoni = sres.resultatsBiefs[j];
        int indiceVar = indicesVarIndependantTps.get(i).intValue();
        float val = (float)resCasierLiaisoni.valeursVariables[indiceVar][0][0];
        os_.writeReal(val);
      }
      os_.writeRecord();
    }
  }

  private void ecritureValeursDepTpsCasierLiaison(SResultatsTemporelSpatial sres,
      ArrayList<Integer> indicesVarIndependantTps, int nbVarDependantTps) throws IOException {
    int nbCasierLiaison = sres.resultatsBiefs.length;
    int nbVar = indicesVarIndependantTps.size();
    int nbPasTps = sres.pasTemps.length;
    for (int i = 0; i < nbPasTps; i++) {
      writeIntegerRecord(i+1, i+1);
      float tps = (float)sres.pasTemps[i];
      writeFloatRecord(tps,tps);
      writeIntegerRecord(nbCasierLiaison,nbCasierLiaison);
      for (int j = 0; j < nbVar; j++) {
        for (int k = 0; k < nbCasierLiaison; k++) {
          SResultatsTemporelSpatialBief resCasierLiaisoni = sres.resultatsBiefs[k];
          int indiceVar = indicesVarIndependantTps.get(j).intValue();
          float val = (float)resCasierLiaisoni.valeursVariables[indiceVar][i][0];
          os_.writeReal(val);
        }
        os_.writeRecord();
      }
    }
  }

  private int[] getNumSectionOrigine(SResultatsTemporelSpatialBief[] tabResBief) {
    int nbBief = tabResBief.length;
    int[] res = new int[nbBief];
    int sommeSection=0;
    for (int i = 0; i < nbBief; i++) {
      res[i] = sommeSection +1;
      sommeSection += tabResBief[i].abscissesSections.length;
    }
    return res;
  }

  private int[] getNumSectionFin(SResultatsTemporelSpatialBief[] tabResBief) {
    int nbBief = tabResBief.length;
    int[] res = new int[nbBief];
    int sommeSection=0;
    for (int i = 0; i < nbBief; i++) {
      sommeSection += tabResBief[i].abscissesSections.length;
      res[i] = sommeSection;
    }
    return res;
  }

  private int getNbSectionTotal(SResultatsTemporelSpatialBief[] tabResBief) {
    int res = 0;
    for (int i = 0; i < tabResBief.length; i++) {
      res += tabResBief[i].abscissesSections.length;
    }
    return res;
  }

  private float[] getAbscisse(SResultatsTemporelSpatialBief[] tabResBief) {
    float[] res = new float[nbSectionTotal_];
    int indiceRes=0;
    for (int i = 0; i < tabResBief.length; i++) {
      SResultatsTemporelSpatialBief resBiefi = tabResBief[i];
      double[] abscisses = resBiefi.abscissesSections;
      for (int j = 0; j < abscisses.length; j++) {
        res[indiceRes] = (float)abscisses[j];
        indiceRes++;
      }
    }
    return res;
  }

  private float[] getValeursVar(int indiceVar, int indicePasTps,
                              SResultatsTemporelSpatialBief[]
                              tabResBief) {
    float[] res = new float[nbSectionTotal_];
    int indiceRes=0;
    for (int i = 0; i < tabResBief.length; i++) {
      SResultatsTemporelSpatialBief resBiefi = tabResBief[i];
      double[] valeurs = resBiefi.valeursVariables[indiceVar][indicePasTps];
      for (int j = 0; j < valeurs.length; j++) {
        res[indiceRes] = (float)valeurs[j];
        indiceRes++;
      }
    }
    return res;
  }
  private void writeStringRecord(String chaine) throws IOException {
    os_.writeCharacter(chaine);
    os_.writeRecord();
  }
  private void writeIntegerRecord(int entier) throws IOException {
    os_.writeInteger(entier);
    os_.writeRecord();
  }
  private void writeIntegerRecord(int entier1, int entier2) throws IOException {
    os_.writeInteger(entier1);
    os_.writeInteger(entier2);
    os_.writeRecord();
  }
  private void writeIntegerRecord(int[] tab) throws IOException {
    for (int i = 0; i < tab.length; i++) {
      os_.writeInteger(tab[i]);
    }
    os_.writeRecord();
  }
  private void writeFloatRecord(float reel1, float reel2) throws IOException {
    os_.writeReal(reel1);
    os_.writeReal(reel2);
    os_.writeRecord();
  }
  private void writeFloatRecord(float[] tab) throws IOException {
    for (int i = 0; i < tab.length; i++) {
      os_.writeReal(tab[i]);
    }
    os_.writeRecord();
  }

  private final static String to4Char(String chaine) {
    int nbChar = chaine.length();
    if (nbChar >4) {
      return chaine;
    } else if (nbChar == 4) {
      return chaine;
    } else if (nbChar == 3) {
      return chaine+" ";
    } else if (nbChar == 2) {
      return chaine+"  ";
    } else if (nbChar == 1) {
      return chaine+"   ";
    } else {
      throw new IllegalArgumentException(
          "completeEspace(String chaine) : chaine vide");
    }
  }
  public static void main(String[] args) {
    try {
      String nomFichier = "mascaret727Mo.opt";
      if (args.length > 0) {
        File file = new File(args[0]);
        if (file.exists()) {
          nomFichier = args[0];
        }
      }
      File file = new File(nomFichier);
      Opthyca1DReader optReader = new Opthyca1DReader(file);
      SResultatsTemporelSpatial res = optReader.read();

      Rubens1DNonPermWriter rubWriter = new Rubens1DNonPermWriter(new File("bis"+nomFichier+".rub"));
      rubWriter.write(res);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
