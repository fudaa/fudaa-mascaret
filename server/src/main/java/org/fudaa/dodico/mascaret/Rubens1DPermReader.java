/*
 * @file         Rubens1DPermReader.java
 * @creation     2004-03-01
 * @modification $Date: 2007-11-20 11:43:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2005 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatialBief;
import org.fudaa.dodico.corba.mascaret.SResultatsVariable;

/**
 * Classe permettant de lire un fichier r�sultat au format RUBENS permanent
 * (ASCII) et de produire un ��SResultatsTemporelSpatial��.
 * @version      $Revision: 1.11 $ $Date: 2007-11-20 11:43:02 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
class Rubens1DPermReader extends Rubens1DReader{
  private LineNumberReader read_;
  private String ligneCourante_ ="";
  private final static String DEBUT_SEPARATEUR_PAS_TEMPS="---------";
  private final static String NOM_FIN="FIN";
  private final static int NB_VALEURS_PAR_LIGNE=5;
  public Rubens1DPermReader(File file) throws FichierMascaretException {
    super(file);
    try {
      FileReader fr = new FileReader(file());
      read_ = new LineNumberReader(fr);
    }
    catch (FileNotFoundException ex) {
      throw new FichierMascaretException("","Fichier introuvable :"+file().getName());
    }
  }
  @Override
  public SResultatsTemporelSpatial read() throws FichierMascaretException {
    try {
      ArrayList listResulatPasTemps = new ArrayList();
      int nbPasTemp=0;
      int nbBief=-1;
      int[] numSectionOrigine=null;
      int[] numSectionFin=null;
      int[] nbSecParBief=null;
      int nbSectionTotal=-1;
      boolean dimensionSpatialIntialise=false;
      while (lectureJusquaProchainPasTps()) {
        nbPasTemp++;
        if (!dimensionSpatialIntialise) {
          nbBief = lectureNbBief();
          numSectionOrigine = new int[nbBief];
          numSectionFin = new int[nbBief];
          lectureNumSection(numSectionOrigine, numSectionFin);
          nbSecParBief = new int[nbBief];
          for (int i = 0; i < nbSecParBief.length; i++) {
            nbSecParBief[i] = numSectionFin[i] - numSectionOrigine[i] + 1;
          }
          nbSectionTotal = numSectionFin[nbBief - 1];
          dimensionSpatialIntialise = true;
        } else {
          readline(); // NBBIEF
          int nbLigneNumSection = (nbBief / 5) + 1; // 5 biefs par ligne
          for (int i = 0; i < nbLigneNumSection; i++) {
            readline(); // Num�ros de section par bief
          }
        }
        Resultat1PasTemps res1PasTps = new Resultat1PasTemps();
        res1PasTps.numPasTemps = nbPasTemp;
        Variable var;
        while((var = lectureVariable(nbSectionTotal,nbSecParBief))!=null) {
          res1PasTps.add(var);
        }
        listResulatPasTemps.add(res1PasTps);
      }

      SResultatsTemporelSpatial res = new SResultatsTemporelSpatial();
      res.resultatsPermanent = true;
      int nbPasTemps = listResulatPasTemps.size();
      if (nbPasTemps > 0) {
        Resultat1PasTemps resultat1erPasTps = (Resultat1PasTemps)listResulatPasTemps.get(0);
        res.resultatsCasier =resultat1erPasTps.isContientVariableCasier();
        res.resultatsLiaison =resultat1erPasTps.isContientVariableLiaison();
        res.pasTemps= new double[nbPasTemps];
        for (int i = 0; i < nbPasTemps; i++) {
          res.pasTemps[i]=i+1;
        }
        Variable abscisse = resultat1erPasTps.getAbscisse();
        int nbVariableDontAbsc = resultat1erPasTps.getNbVar();
        res.variables = new SResultatsVariable[nbVariableDontAbsc-1];
        for (int i = 0; i < res.variables.length; i++) {
          res.variables[i] = new SResultatsVariable();
          res.variables[i].nomLong = "";
          res.variables[i].nomCourt = resultat1erPasTps.get(i+1).nom;
          res.variables[i].nbDecimal = 4;
          res.variables[i].unite = "";

        }
        res.resultatsBiefs = new SResultatsTemporelSpatialBief[nbBief];
        for (int i = 0; i < nbBief; i++) {
          res.resultatsBiefs[i] = new SResultatsTemporelSpatialBief();
          res.resultatsBiefs[i].abscissesSections = abscisse.valeurs[i];
          res.resultatsBiefs[i].valeursVariables = new double[nbVariableDontAbsc-1][nbPasTemp][nbSecParBief[i]];
          for (int j = 1; j < nbVariableDontAbsc; j++) {
            for (int k = 0; k < nbPasTemp; k++) {
              Resultat1PasTemps res1PasTps = (Resultat1PasTemps)listResulatPasTemps.get(k);
              res.resultatsBiefs[i].valeursVariables[j-1][k] = res1PasTps.get(j).valeurs[i];
            }
          }
        }
      }
      return res;
    }
    catch (Throwable ex) {
      ex.printStackTrace();
      throw new FichierMascaretException(read_.getLineNumber(),ligneCourante_,ex.getMessage());
    }
  }

  private String readline() throws IOException {
    ligneCourante_=read_.readLine();
    return ligneCourante_;
  }

  private boolean lectureJusquaProchainPasTps() throws IOException {
    while(readline()!=null){
      if (ligneCourante_.startsWith(DEBUT_SEPARATEUR_PAS_TEMPS) ) {
        return true;
      }
    }
    return false;
  }

  private int lectureNbBief() throws IOException,FichierMascaretException {
    readline();
    int i=ligneCourante_.indexOf("NBBIEF=");
    if (i==-1) {
      throw new FichierMascaretException(read_.getLineNumber() ,ligneCourante_,"NBBIEF= introuvable");
    }
    try {
      return Integer.parseInt(ligneCourante_.substring(i + "NBBIEF=".length()).
                              trim());
    }
    catch (NumberFormatException ex) {
      throw new FichierMascaretException(read_.getLineNumber() ,ligneCourante_,
                                         "Nombre de bief introuvable");
    }
  }
  private void lectureNumSection(int[] numSecOrigine, int[] numSecFin) throws IOException,FichierMascaretException {
     int nbBief  = numSecOrigine.length;
     int nbLigneALire = (nbBief / 5) +1; // 5 biefs trait�s par ligne
     int indiceSection = 0;
     for (int i = 0; i < nbLigneALire; i++) {
       readline();
       int iEgale=ligneCourante_.indexOf("=");
       if (iEgale==-1) {
         throw new FichierMascaretException(read_.getLineNumber() ,ligneCourante_,"= introuvable");
       }
       try {
         String listeNumSection = ligneCourante_.substring(iEgale + 1);
         StringTokenizer st = new StringTokenizer(listeNumSection," ");
         while(st.hasMoreTokens()) {
           numSecOrigine[indiceSection] = Integer.parseInt(st.nextToken());
           numSecFin[indiceSection] = Integer.parseInt(st.nextToken());
           indiceSection++;
         }
       }
       catch (Exception ex) {
         throw new FichierMascaretException(read_.getLineNumber() ,ligneCourante_,
                                            "Num�ros de section introuvable");
       }

     }
   }

   private Variable lectureVariable(int nbSectionTotal, int[] nbSectionPasBief) throws IOException, FichierMascaretException {
     Variable var = new Variable();
     var.nom = readline().trim();
     if (NOM_FIN.equals(var.nom)) {
       return null;
     }
     // calcul du nombre de ligne de fichier � lire pour une variable
     int nbLigneFichier = nbSectionTotal/NB_VALEURS_PAR_LIGNE;
     if ((nbLigneFichier*NB_VALEURS_PAR_LIGNE) != nbSectionTotal) nbLigneFichier++;
     // fin calcul du nombre de ligne de fichier � lire pour une variable

     // lecture dans un string buffer des nbLigneFichier;
     StringBuffer stringBuffer = new StringBuffer();
     for (int i = 0; i < nbLigneFichier; i++) {
       stringBuffer.append(readline());
     }
     String contenuVariable = stringBuffer.toString();
     if (contenuVariable.indexOf("*")!=-1) {
       throw new FichierMascaretException(contenuVariable, "Impossible de lire la variable "+var.nom+" dans le fichier r�sultat Rubens");
     }
     int indexEspace=contenuVariable.indexOf(" ");
     int indexPoint=contenuVariable.indexOf(".");
     int lastIndexPoint=contenuVariable.lastIndexOf(".");

     if ((indexPoint!=-1)&&(indexPoint!=lastIndexPoint)&&(indexEspace==-1)) {
       // il n'y a pas de s�parateur espace : chaque nombre utilise 10 caract�res
       stringBuffer = new StringBuffer();
       int tailleContenuVariable = contenuVariable.length();
       for (int i = 0; i < tailleContenuVariable; i+=10) {
         String uneValeur = contenuVariable.substring(i,Math.min(i+10,tailleContenuVariable));
         stringBuffer.append(uneValeur);
         stringBuffer.append(" ");
       }
       contenuVariable = stringBuffer.toString();
     }

     try {
      // allocation et initialisation en parsant les diff�rents token.
      StringTokenizer stk = new StringTokenizer(contenuVariable, " ");
      int nbBief = nbSectionPasBief.length;
      var.valeurs = new double[nbBief][];
      for (int i = 0; i < nbBief; i++) {
        int nbSectionBiefi = nbSectionPasBief[i];
        var.valeurs[i] = new double[nbSectionBiefi];
        for (int j = 0; j < nbSectionBiefi; j++) {
          var.valeurs[i][j] = Double.parseDouble(stk.nextToken());
        }
      }
    } catch (NumberFormatException ex) {
      throw new FichierMascaretException(contenuVariable, "Impossible de lire la variable "+var.nom+" dans le fichier r�sultat Rubens");
    }
     // fin allocation

     return var;
   }

  public static void main(String[] args) {
    try {
      String nomFichier = "permanent7Biefs.rub";
      if (args.length > 0) {
        nomFichier = args[0];
      }
      File file = new File(nomFichier);
      //CtuluLibMessage.DEBUG = true;
      Rubens1DPermReader rr = new Rubens1DPermReader(file);
      rr.read();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

}

class Variable {
  String nom;
  double[][] valeurs;
  boolean isLiaison() {
    int origine = DescriptionVariables.getOrigineVar(nom);
    if (origine == DescriptionVariables.LIAISON)
      return true;
    //else
      return false;
  }
  boolean isCasier() {
    int origine = DescriptionVariables.getOrigineVar(nom);
    if (origine == DescriptionVariables.CASIER)
      return true;
    //else
      return false;
  }
}
class Resultat1PasTemps {
  int numPasTemps;
  private ArrayList listVar_=new ArrayList();
  void add(Variable var) {
    listVar_.add(var);
  }
  Variable get(int index) {
    return (Variable)listVar_.get(index);
  }
  int getNbVar() {
    return listVar_.size();
  }
  Variable getAbscisse() {
    return get(0);
  }
  boolean isContientVariableCasier() {
    Iterator ite = listVar_.iterator();
    while (ite.hasNext()) {
      Variable var = (Variable)ite.next();
      if (var.isCasier()) return true;
    }
    return false;
  }
  boolean isContientVariableLiaison() {
    Iterator ite = listVar_.iterator();
    while (ite.hasNext()) {
      Variable var = (Variable)ite.next();
      if (var.isLiaison()) return true;
    }
    return false;
  }
}
