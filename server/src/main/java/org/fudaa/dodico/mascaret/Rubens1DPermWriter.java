/*
 * @file         Rubens1DPermWriter.java
 * @creation     2004-03-07
 * @modification $Date: 2007-11-20 11:43:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2005 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatialBief;

/**
 * Classe permettant d'�crire un fichier r�sultat au format RUBENS permanent
 * (ASCII) � partir d'un ��SResultatsTemporelSpatial��.
 * @version      $Revision: 1.9 $ $Date: 2007-11-20 11:43:00 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Rubens1DPermWriter {
  private final static String DEB_LIGNE1="RESULTATS CALCUL,DATE :  ";
  private final static String LIGNE2="FICHIER RESULTAT MASCARET                                               ";
  private final static String LIGNE3="----------------------------------------------------------------------- ";
  private final static String NOM_FIN="FIN";
  private final static String DATE_COURANTE = DateFormat.getInstance().format(new Date());
  private final static String LIGNE1=DEB_LIGNE1+DATE_COURANTE;
  private String ligne4_;
  private String[] lignesSections_;
  private PrintWriter fw_;
  public Rubens1DPermWriter(File file) throws FichierMascaretException {
    try {
      fw_=new PrintWriter(new BufferedWriter(new FileWriter(file)));
    }
    catch (IOException ex) {
      throw new FichierMascaretException("","Fichier impossible � ouvrir :"+file.getName());
    }
  }
  public void write(SResultatsTemporelSpatial sres, boolean entierSur5Carac) throws FichierMascaretException {
    try {
      int nbBief = sres.resultatsBiefs.length;
      int[] numSectionOrigine = getNumSectionOrigine(sres.resultatsBiefs);
      int[] numSectionFin = getNumSectionFin(sres.resultatsBiefs);
      int imax = numSectionFin[nbBief-1];
      double[] abscisses = getAbscisse(sres.resultatsBiefs, imax);
      int nbVarSansX = sres.variables.length;
      int nbPasTemps= sres.pasTemps.length;
      if (entierSur5Carac) {
          ligne4_ = " IMAX  =" + to5Char(imax) + " NBBIEF=" + to5Char(nbBief);
      }
      else {
          ligne4_ = " IMAX  = " + to4Char(imax) + " NBBIEF= " + to4Char(nbBief);
      }
      lignesSections_ = new String[((nbBief-1) / 5)+1]; // 5 biefs par ligne
      for (int i = 0; i < lignesSections_.length; i++) {
        lignesSections_[i] = " I1,I2 =";
      }

      for (int i = 0; i < nbBief; i++) {
        if (entierSur5Carac) {
          lignesSections_[i / 5] += to5Char(numSectionOrigine[i]) + " " +to5Char(numSectionFin[i]);
        }
        else {
          lignesSections_[i / 5] += " " + to4Char(numSectionOrigine[i]) + " " +to4Char(numSectionFin[i]);
        }
      }

      DecimalFormat nf = (DecimalFormat)NumberFormat.getInstance(Locale.US);
      nf.setGroupingUsed(false);
      nf.setGroupingUsed(false);
      nf.setDecimalSeparatorAlwaysShown(true);
      for (int i = 0; i < nbPasTemps; i++) {
        // ent�te
        fw_.println(LIGNE1);
        fw_.println(LIGNE2);
        fw_.println(LIGNE3);
        fw_.println(ligne4_);
        for (int j = 0; j < lignesSections_.length; j++) {
          fw_.println(lignesSections_[j]);
        }

        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        ecritureVar("X", nf, abscisses);

        for (int j = 0; j < nbVarSansX; j++) {
          double[] vals = getValeursVar(j, i, imax, sres.resultatsBiefs);
          String nomVar = sres.variables[j].nomCourt;
          int nbDec = sres.variables[j].nbDecimal;
          nf.setMaximumFractionDigits(nbDec);
          nf.setMinimumFractionDigits(nbDec);
          ecritureVar(nomVar, nf, vals);
        }
        fw_.println(" "+NOM_FIN);
      }
      fw_.close();
    }
    catch (Throwable ex) {
      ex.printStackTrace();
      throw new FichierMascaretException(null,ex.getLocalizedMessage());
    }
  }

  private int[] getNumSectionOrigine(SResultatsTemporelSpatialBief[] tabResBief) {
    int nbBief = tabResBief.length;
    int[] res = new int[nbBief];
    int sommeSection=0;
    for (int i = 0; i < nbBief; i++) {
      res[i] = sommeSection +1;
      sommeSection += tabResBief[i].abscissesSections.length;
    }
    return res;
  }

  private int[] getNumSectionFin(SResultatsTemporelSpatialBief[] tabResBief) {
    int nbBief = tabResBief.length;
    int[] res = new int[nbBief];
    int sommeSection=0;
    for (int i = 0; i < nbBief; i++) {
      sommeSection += tabResBief[i].abscissesSections.length;
      res[i] = sommeSection;
    }
    return res;
  }

  private double[] getAbscisse(SResultatsTemporelSpatialBief[] tabResBief, int nbSectionTotal) {
    double[] res = new double[nbSectionTotal];
    int indiceRes=0;
    for (int i = 0; i < tabResBief.length; i++) {
      SResultatsTemporelSpatialBief resBiefi = tabResBief[i];
      double[] abscisses = resBiefi.abscissesSections;
      for (int j = 0; j < abscisses.length; j++) {
        res[indiceRes] = abscisses[j];
        indiceRes++;
      }
    }
    return res;
  }

  private double[] getValeursVar(int indiceVar, int indicePasTps, int nbSectionTotal,
                              SResultatsTemporelSpatialBief[]
                              tabResBief) {
    double[] res = new double[nbSectionTotal];
    int indiceRes=0;
    for (int i = 0; i < tabResBief.length; i++) {
      SResultatsTemporelSpatialBief resBiefi = tabResBief[i];
      double[] valeurs = resBiefi.valeursVariables[indiceVar][indicePasTps];
      for (int j = 0; j < valeurs.length; j++) {
        res[indiceRes] = valeurs[j];
        indiceRes++;
      }
    }
    return res;
  }

  private void ecritureVar(String nomVar, DecimalFormat nf, double[] valeurs) {
    fw_.println(" "+nomVar);
    int nbColonne=0;
    for (int i = 0; i < valeurs.length; i++) {
      String format = nf.format(valeurs[i]);
      fw_.print(" "+to12Char(format));
      nbColonne++;
      if (nbColonne ==5) {
        fw_.println();
        nbColonne=0;
      }
    }
    if (nbColonne != 0) fw_.println();
  }
/*  private final static String to4ou5Char(int entier) {
    if (entier <10000) return to4Char(entier);
    else return to5Char(entier);
  }*/
  private final static String to4Char(int entier) {
    String chaine = ""+entier;
    int nbChar = chaine.length();
    if (nbChar >4) {
      return "****";
    } else if (nbChar == 4) {
      return chaine;
    } else if (nbChar == 3) {
      return " "+chaine;
    } else if (nbChar == 2) {
      return "  "+chaine;
    } else if (nbChar == 1) {
      return "   "+chaine;
    } else {
      return " NaN";
    }
  }
  private final static String to5Char(int entier) {
    String chaine = ""+entier;
    int nbChar = chaine.length();
    if (nbChar >5) {
      return "*****";
    } else if (nbChar == 5) {
      return chaine;
    } else if (nbChar == 4) {
      return " "+chaine;
    } else if (nbChar == 3) {
      return "  "+chaine;
    } else if (nbChar == 2) {
      return "   "+chaine;
    } else if (nbChar == 1) {
      return "    "+chaine;
    } else {
      return "  NaN";
    }
  }

  private final static String to12Char(String chaine) {
    int nbChar = chaine.length();
    if (nbChar >12) {
      return "************";
    } else if (nbChar == 12) {
      return chaine;
    } else if (nbChar == 11) {
      return " "+chaine;
    } else if (nbChar == 10) {
      return "  "+chaine;
    } else if (nbChar == 9) {
      return "   "+chaine;
    } else if (nbChar == 8) {
      return "    "+chaine;
    } else if (nbChar == 7) {
      return "     "+chaine;
    } else if (nbChar == 6) {
      return "      "+chaine;
    } else if (nbChar == 5) {
      return "       "+chaine;
    } else if (nbChar == 4) {
      return "        "+chaine;
    } else if (nbChar == 3) {
      return "         "+chaine;
    } else if (nbChar == 2) {
      return "          "+chaine;
    } else if (nbChar == 1) {
      return "           "+chaine;
    } else {
      return "         NaN";
    }
  }
  public static void main(String[] args) {
    try {
      String nomFichier = "permanent7Biefs.rub";
      if (args.length > 0) {
        nomFichier = args[0];
      }
      //File file = new File(nomFichier);
      Rubens1DPermReader rubReader = new Rubens1DPermReader(new File(nomFichier));
      SResultatsTemporelSpatial res = rubReader.read();

      Rubens1DPermWriter rubWriter = new Rubens1DPermWriter(new File("bis"+nomFichier));
      rubWriter.write(res, false);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
