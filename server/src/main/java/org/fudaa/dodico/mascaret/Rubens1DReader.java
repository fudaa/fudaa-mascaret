/*
 * @file         Rubens1DReader.java
 * @creation     2004-03-02
 * @modification $Date: 2007-11-20 11:42:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;

/**
 * Classe permettant de d�terminer si le fichier est au format RUBENS permanent
 * ou non-permanent et d\u2019instancier la bonne classe de lecture
 * (��Rubens1DNonPermReader�� ou ��Rubens1DPermReader��).
 * @version      $Revision: 1.6 $ $Date: 2007-11-20 11:42:59 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Rubens1DReader {
  private File file_;
  public Rubens1DReader(File file) {
    file_ = file;
  }
  protected File file() { return file_;}
  protected SResultatsTemporelSpatial read() throws FichierMascaretException {
    if (isCommenceRubensPermament()){
      Rubens1DPermReader reader = new Rubens1DPermReader(file_);
      return reader.read();
    } else {
      Rubens1DNonPermReader reader = new Rubens1DNonPermReader(file_);
      return reader.read();
    }
  }

  private boolean isCommenceRubensPermament() throws FichierMascaretException {
    try {
      LineNumberReader reader = new LineNumberReader(new FileReader(file_));
      String ligne = reader.readLine();
      reader.close();
      if (ligne == null) {
        throw new FichierMascaretException("vide","Fichier Rubens vide");
      }
      if (ligne.indexOf("RESULTATS CALCUL")!=-1) {
        return true;
      }
      else {
        return false;
      }
    }
    catch (IOException ex) {
      throw new FichierMascaretException(1,"","Impossible de savoir si c'est un fichier permanent");
    }
  }
}
