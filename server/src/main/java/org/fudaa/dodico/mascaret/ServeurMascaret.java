/*
 * @file         ServeurMascaret.java
 * @creation     2000-02-16
 * @modification $Date: 2005-06-29 18:08:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mascaret;
import java.util.Date;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Une classe serveur de test pour Mascaret qui n'est plus utilis�e.
 *
 * @version      $Revision: 1.7 $ $Date: 2005-06-29 18:08:18 $ by $Author: jm_lacombe $
 * @author       Guillaume Desnoix
 */
public class ServeurMascaret {
  public static void main(String[] args) {
    String nom=
      (args.length > 0
        ? args[0]
        : CDodico.generateName("::mascaret::ICalculMascaret"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculMascaret.class));
    System.out.println("Mascaret server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
