/*
 GPL 2
 */
package org.fudaa.dodico.mascaret;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import java.text.Normalizer;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Frederic Deniger
 */
public class StringConverter implements Converter {

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    String values = (String) source;
    if (values == null) {
      writer.setValue(StringUtils.EMPTY);
    } else {
      final String clean = Normalizer.normalize(values, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "");
      writer.setValue(clean);
    }
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    return reader.getValue();
  }

  @Override
  public boolean canConvert(Class type) {
    return String.class.equals(type);
  }
}
