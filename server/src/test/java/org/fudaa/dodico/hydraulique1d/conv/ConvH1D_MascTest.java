/*
 GPL 2
 */
package org.fudaa.dodico.hydraulique1d.conv;

import java.io.File;
import java.io.IOException;
import org.fudaa.dodico.corba.mascaret.SParametresCAS;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1dTest;
import org.fudaa.dodico.mascaret.AbstractMascaretTestCase;

/**
 * Debut de test pour la conversion h1d -> mascaret
 *
 * @author Frederic Deniger
 */
public class ConvH1D_MascTest extends AbstractMascaretTestCase {

  /**
   *
   * @throws IOException
   */
  public void testConvert() throws IOException {
    File mascFile = getOpthycaFile(EX_MODMASC);
    MetierEtude1d etude1d = MetierEtude1dTest.readZipFile(mascFile);
    SParametresCAS convertirParametresCas = ConvH1D_Masc.convertirParametresCas(true, etude1d, 0, false);
    assertEquals("mascaret0.cas", convertirParametresCas.parametresGen.fichMotsCles);
  }
}
