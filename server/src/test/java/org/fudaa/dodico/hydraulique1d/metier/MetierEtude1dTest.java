/*
 GPL 2
 */
package org.fudaa.dodico.hydraulique1d.metier;

import org.fudaa.dodico.mascaret.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author Frederic Deniger
 */
public class MetierEtude1dTest extends AbstractMascaretTestCase {

  public MetierEtude1dTest() {
  }

  public void testReadEtude1dPermanent1Bief1Apport() throws IOException {
    File mascFile = getOpthycaFile(PERMANENT1_BIEF1_APPORTMASC);
    assertTrue(mascFile.exists());
    MetierEtude1d metier1d = readZipFile(mascFile);
    assertNotNull(metier1d);
  }

  public void testReadEtude1dExMod() throws IOException {
    File mascFile = getOpthycaFile(EX_MODMASC);
    assertTrue(mascFile.exists());
    MetierEtude1d metier1d = readZipFile(mascFile);
    assertNotNull(metier1d);
  }

  public void testReadEtude1dNonPermanent3Biefs2CasierSings() throws IOException {
    File mascFile = getOpthycaFile(NON_PERMANENT3_BIEFS2_CASIER_SINGSMASC);
    assertTrue(mascFile.exists());
    MetierEtude1d metier1d = readZipFile(mascFile);
    assertNotNull(metier1d);
  }

  public void testReadEtude1dNonPermanent3Biefs3Casier() throws IOException {
    File mascFile = getOpthycaFile(NON_PERMANENT3_BIEFS3_CASIERMASC);
    assertTrue(mascFile.exists());
    MetierEtude1d metier1d = readZipFile(mascFile);
    assertNotNull(metier1d);
  }

  public void testReadEtude1dQualiteeEauAvecSeuil() throws IOException {
    File mascFile = getOpthycaFile(QUALITEE_EAU_AVEC_SEUILMASC);
    assertTrue(mascFile.exists());
    MetierEtude1d metier1d = readZipFile(mascFile);
    assertNotNull(metier1d);
  }

  public void testReadEtude1dOndeSub7biefs3seuils() throws IOException {
    File mascFile = getOpthycaFile(ONDE_SUB7BIEFS3SEUILSMASC);
    assertTrue(mascFile.exists());
    MetierEtude1d metier1d = readZipFile(mascFile);
    assertNotNull(metier1d);
  }

  /**
   * Copied from org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet.
   *
   * @param fichier
   * @return
   */
  public static MetierEtude1d readZipFile(File fichier) {
    ZipFile zf = null;
    MetierEtude1d res = null;
    try {
      zf = new ZipFile(fichier);
      ZipEntry entry;
      entry = zf.getEntry("etude1d.xml");

      //On recupere la version de l'etude
      byte[] b = new byte[1000];
      zf.getInputStream(entry).read(b);
      String DebutEtude = new String(b);
      int indexVersion = DebutEtude.indexOf("<single type=\"String\">");
      String versionEtude = DebutEtude.substring(indexVersion + 22, indexVersion + 25);

      res = new MetierEtude1d().readFrom(zf.getInputStream(entry), false, versionEtude);
    } catch (Exception ex) {
      Logger.getLogger(MetierEtude1dTest.class.getName()).log(Level.SEVERE, "message {0}", ex);
    } finally {
      if (zf != null) {
        try {
          zf.close();
        } catch (IOException ex) {
          Logger.getLogger(MetierEtude1dTest.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return res;
  }
}
