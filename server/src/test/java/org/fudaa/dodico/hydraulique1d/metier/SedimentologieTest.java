package org.fudaa.dodico.hydraulique1d.metier;

import java.io.IOException;

import junit.framework.AssertionFailedError;

import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleEngelundHansen;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleLefort1991;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleLefort2007;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleMeyerPeter;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleRecking2010;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleRecking2011;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleRickenmann;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleSediment;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleSmartJaggi;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleVanRijn;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierParametresSediment;
import org.fudaa.dodico.mascaret.AbstractMascaretTestCase;

import com.memoire.fu.FuLog;

/**
 * Un test case pour les formule de calcul par sédimentologie.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class SedimentologieTest extends AbstractMascaretTestCase {
  HydrauResults hydrauRes_;
  MetierParametresSediment params_;
  double epsilon=1.e-2;
  
  // Issu de la feuille de calcul Excel
  double[][] sedRes=new double[][] {
      //Meyer-Peter 
      {1.90, 1.51, 1.13, 0.90, 0.70, 0.59, 0.36, 0.34, 0.24, 0.19, 0.15, 0.09, 0.05, 0.03, 0.01, 0.00, 0.00, 0.00},
      //Smart et Jaeggi
      {1.76, 1.35, 0.99, 0.77, 0.59, 0.49, 0.29, 0.28, 0.20, 0.16, 0.13, 0.08, 0.05, 0.03, 0.02, 0.00, 0.00, 0.00},
      //Lefort 1991 
      {2.30, 1.76, 1.29, 1.00, 0.77, 0.65, 0.39, 0.38, 0.27, 0.22, 0.18, 0.12, 0.07, 0.05, 0.03, 0.01, 0.01, 0.00},
      //Lefort 2005 
      {1.87, 1.40, 0.99, 0.75, 0.56, 0.46, 0.26, 0.25, 0.17, 0.13, 0.11, 0.07, 0.04, 0.03, 0.02, 0.01, 0.00, 0.00},
      //lefort 2007 
      {1.80, 1.35, 0.96, 0.73, 0.55, 0.45, 0.26, 0.25, 0.17, 0.13, 0.10, 0.07, 0.04, 0.03, 0.01, 0.00, 0.00, 0.00},
      //Engelund Hansen 
      {0.92, 0.61, 0.38, 0.26, 0.17, 0.13, 0.06, 0.06, 0.03, 0.02, 0.02, 0.01, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, 
      //Einstein Brown  
      {2.18, 1.41, 0.85, 0.59, 0.42, 0.33, 0.14, 0.13, 0.06, 0.03, 0.02, 0.01, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, 
      //Rickenmann  
      {1.41, 1.07, 0.77, 0.59, 0.44, 0.35, 0.19, 0.18, 0.11, 0.07, 0.04, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00},
      //Recking 2008  
      {2.02, 1.44, 0.96, 0.69, 0.49, 0.39, 0.21, 0.20, 0.13, 0.09, 0.06, 0.02, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00},
      //Recking 2010 
      {1.24, 0.88, 0.59, 0.43, 0.30, 0.24, 0.13, 0.12, 0.07, 0.03, 0.02, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00},
      //Recking 2011 
      {1.05, 0.73, 0.46, 0.32, 0.20, 0.15, 0.05, 0.05, 0.02, 0.01, 0.01, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00},
      //Van Rijn
      {0.45, 0.32, 0.22, 0.16, 0.11, 0.09, 0.04, 0.04, 0.02, 0.02, 0.01, 0.01, 0.00, 0.00, 0.00, 0.00, 0.00, 0.01}
  };
  double[][] tauRes=new double[][] {
      {274.34, 237.62, 200.39, 174.54, 151.03, 137.08, 104.27, 101.95, 85.56, 75.63, 67.39, 55.32, 42.26, 35.62, 27.98, 18.50, 12.23, 4.66},
      {196.30, 170.03, 143.39, 124.89, 108.07, 98.09, 74.61, 72.95, 61.22, 54.11, 48.22, 39.58, 30.24, 25.49, 20.02, 13.24, 8.75, 3.34},
      {0.85, 0.73, 0.62, 0.54, 0.47, 0.42, 0.32, 0.31, 0.26, 0.23, 0.21, 0.17, 0.13, 0.11, 0.09, 0.06, 0.04, 0.01},
      {0.28, 0.24, 0.21, 0.18, 0.16, 0.14, 0.11, 0.10, 0.09, 0.08, 0.07, 0.06, 0.04, 0.04, 0.03, 0.02, 0.01, 0.00}
  };

  /**
   * 1 seul bief, une seule section, avec plusieurs temps pour simuler un changement de débit.
   * @author Bertrand Marchand (marchand@deltacad.fr)
   */
  class HydrauResults implements MetierResultatsTemporelSpacialI {
    // Les debits, issus de la feuille Excel
    double[] debits = new double[] { 1000, 774, 573, 450, 350, 296, 185, 178, 132, 107, 88, 63, 40, 30, 20, 10, 5, 1 };
    // Les hauteurs, issues de la feuille Excel
    double[] hauteurs = new double[] {
        3.14873081460036, 2.68210790714307, 2.22448059466516, 1.9154924638265, 1.6406299286677, 1.48011332926777, 
        1.11014253111858, 1.08431580617216, 0.903738918268046, 0.795428295616823, 0.706414986443789, 0.57689555253611,
        0.438313744098063, 0.368420603900746, 0.288496806954015, 0.190040156277613, 0.125250658298057, 0.0476277981477428 };
    
    @Override
    public double getValue(MetierDescriptionVariable _var, int _ibief, int _itps, int _isect) {
      // Pente d'energie
      if (_var.nom().equals(MetierDescriptionVariable.PENE.nom())) {
        return 0.01;
      }
      // Largeur du bief
      else if (_var.nom().equals(MetierDescriptionVariable.B1.nom())) {
        return 50;
      }
      // Strickler du bief
      else if (_var.nom().equals(MetierDescriptionVariable.KMIN.nom())) {
        return 32;
      }
      // Debit
      else if (_var.nom().equals(MetierDescriptionVariable.QMIN.nom())) {
        return debits[_itps];
      }
      // Hauteurs
      else if (_var.nom().equals(MetierDescriptionVariable.Y.nom())) {
        return hauteurs[_itps];
      }
      else if (_var.nom().equals(MetierDescriptionVariable.RH1.nom())) {
        double larg=50;
        double h1=hauteurs[_itps];
        return (h1 * larg) / (larg + 2 * h1);
      }
      else if (_var.nom().equals(MetierDescriptionVariable.VMIN.nom())) {
        double larg=50;
        double h1=hauteurs[_itps];
        double qmin=debits[_itps];
        return qmin / (h1 * larg);
      }
      
      // Rayon hydraulique
      throw new AssertionFailedError();
    }

    @Override
    public int getNbBiefs() {
      return 1;
    }

    @Override
    public int getNbTemps() {
      return debits.length;
    }

    @Override
    public int getNbSections(int _ibief) {
      return 1;
    }
  }

  public SedimentologieTest() {
    hydrauRes_=new HydrauResults();
    params_=new MetierParametresSediment();
    params_.setDmoyen(0.02);
    params_.setD30(0.04);
    params_.setD50(0.06);
    params_.setD84(0.1);
    params_.setD90(0.11);
    params_.setRugosite(40);
    params_.setDensiteMateriau(2.65);
    params_.setDensiteApparente(2);
    params_.setTauc(0.055);
    params_.setTEau(17);
  }
  
  public void testTauMoy() throws IOException {
    FuLog.debug("Tau moyen");
    // N'importe quelle formule convient pour ce test.
    MetierFormuleSediment form=new MetierFormuleMeyerPeter();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      MetierFormuleSediment.RetTauMoy ret=form.calculerTaumoy(params_, hydrauRes_, 0, 0, itps);
      assertEquals(ret.tau, tauRes[0][itps],epsilon);
      assertEquals(ret.taueff, tauRes[1][itps],epsilon);
      assertEquals(ret.teta, tauRes[2][itps],epsilon);
      assertEquals(ret.tetaD50, tauRes[3][itps],epsilon);
    }
  }

  public void testFormuleMeyerPeter() throws IOException {
    FuLog.debug("Formule Meyer-Peter");
    MetierFormuleSediment form=new MetierFormuleMeyerPeter();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[0][itps],epsilon);
    }
  }

  public void testFormuleSmartJaggi() throws IOException {
    FuLog.debug("Formule Smart & Jaggi");
    MetierFormuleSediment form=new MetierFormuleSmartJaggi();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[1][itps],epsilon);
    }
  }

  public void testFormuleLefort1991() throws IOException {
    FuLog.debug("Formule Lefort 1991");
    MetierFormuleSediment form=new MetierFormuleLefort1991();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[2][itps],epsilon);
    }
  }

  public void testFormuleLefort2007() throws IOException {
    FuLog.debug("Formule Lefort 2007");
    MetierFormuleSediment form=new MetierFormuleLefort2007();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[4][itps],epsilon);
    }
  }

  public void testFormuleEngelungHansen() throws IOException {
    FuLog.debug("Formule Hangelung & Hansen");
    MetierFormuleSediment form=new MetierFormuleEngelundHansen();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[5][itps],epsilon);
    }
  }

  public void testFormuleRickenmann() throws IOException {
    FuLog.debug("Formule Rickenmann");
    MetierFormuleSediment form=new MetierFormuleRickenmann();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[7][itps],epsilon);
    }
  }

  public void testFormuleRecking2010() throws IOException {
    FuLog.debug("Formule Recking 2010");
    MetierFormuleSediment form=new MetierFormuleRecking2010();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[9][itps],epsilon);
    }
//    double G7=1000;
//    double lb=30;
//    double itrap=0.01;
//    double ABsup=1.e20;
//    double ABinf=1.e-20;
//    double AB=0;
//    double dopti=0.35;
//    double valcible=6.01;
//    double val=valcible+1;
//    
//    while (Math.abs(val) > 10.e-8) {
//      AB=(ABsup+ABinf)/2;
//        val = G7 * (lb - 2 * AB) / (AB * lb * lb * Math.pow((9.81 * AB * (itrap)), 0.5)) - 6.25 - 5.75 * Math.log10(AB / (dopti));
//        FuLog.debug("val=" + val);
//        if (val-valcible>0) {
//          ABinf=AB;
//        }
//        else if (val<valcible-1.e-8) {
//          ABsup=AB;
//        }
//        else {
//          break;
//        }
//    }
//    FuLog.debug("AB=" + AB);
  }

  public void testFormuleRecking2011() throws IOException {
    FuLog.debug("Formule Recking 2011");
    MetierFormuleSediment form=new MetierFormuleRecking2011();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[10][itps],epsilon);
    }
  }

  public void testFormuleVanRijn() throws IOException {
    FuLog.debug("Formule Van Rijn");
    MetierFormuleSediment form=new MetierFormuleVanRijn();
    for (int itps=0; itps<hydrauRes_.getNbTemps(); itps++) {
      double val=form.calculer(params_, hydrauRes_, 0, 0, itps);
      assertEquals(val, sedRes[11][itps],epsilon);
    }
  }
}
