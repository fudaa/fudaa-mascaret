/*
 GPL 2
 */
package org.fudaa.dodico.mascaret;

import java.io.File;
import java.io.IOException;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLibFile;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractMascaretTestCase extends TestCase {

  private static File tempFile;
  public static final String EX_MODMASC = "ExMod.masc";
  public static final String NON_PERMANENT3_BIEFS2_CASIER_SINGSMASC = "NonPermanent3Biefs2CasierSings.masc";
  public static final String NON_PERMANENT3_BIEFS3_CASIERMASC = "NonPermanent3Biefs3Casier.masc";
  public static final String ONDE_SUB7BIEFS3SEUILSMASC = "ondeSub7biefs3seuils.masc";
  public static final String PERMANENT1_BIEF1_APPORTMASC = "permanent1Bief1Apport.masc";
  public static final String QUALITEE_EAU_AVEC_SEUILMASC = "QualiteeEauAvecSeuil.masc";

  public AbstractMascaretTestCase() {
  }

  @Override
  protected void setUp() throws Exception {
    super.setUp();
    tempFile = CtuluLibFile.createTempDir();
  }

  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
    CtuluLibFile.deleteDir(tempFile);
  }

  protected File getOpthycaFile(String filename) throws IOException {
    final String extension = ".masc";
    File target = createTempFile(extension);
    return CtuluLibFile.getFileFromJar("/masc/" + filename, target);
  }

  protected File createTempFile(final String extension) throws IOException {
    return File.createTempFile("test", extension, tempFile);
  }
}
