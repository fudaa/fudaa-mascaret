/*
 GPL 2
 */
package org.fudaa.dodico.mascaret;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.corba.mascaret.SParametresCAS;

/**
 * Test read/write opérations on a artifical built structure with all fields initialized
 *
 * @author Frederic Deniger
 */
public class CompleteStructureTest extends AbstractMascaretTestCase {

  private static void createFile(SParametresCAS parametresCas, File target) {
    EdamoxXmlReaderWriter xmlWriter = new EdamoxXmlReaderWriter("1.0");
    EdamoxXMLContent content = new EdamoxXMLContent(parametresCas);
    xmlWriter.writeXML(content, target);
  }

  /**
   * create a object will all fields initialized
   *
   * @param in
   * @return
   * @throws Exception
   */
  private static Object instanciate(Class in) throws Exception {
    Object newInstance = in.newInstance();
    Field[] declaredFields = in.getDeclaredFields();
    for (Field field : declaredFields) {
      if (!Modifier.isFinal(field.getModifiers())) {
        Class<?> declaringClass = field.getType();
        Object value = doInstanciation(declaringClass);
        field.set(newInstance, value);
      }
    }
    return newInstance;
  }

  /**
   *
   * @param in the class to instanciate. Call #instanciate(Class) for complex objects.
   * @return
   * @throws Exception
   */
  private static Object doInstanciation(Class in) throws Exception {
    if (Boolean.TYPE.equals(in)) {
      return true;
    }
    if (Integer.TYPE.equals(in)) {
      return 1;
    }
    if (Long.TYPE.equals(in)) {
      return 1L;
    }
    if (Float.TYPE.equals(in)) {
      return 1f;
    }
    if (Double.TYPE.equals(in)) {
      return 1d;
    }
    if (String.class.equals(in)) {
      return "test";
    }
    if (boolean[].class.equals(in)) {
      return new boolean[]{true, false};
    }
    if (String[].class.equals(in)) {
      return new String[]{"test1", "test2"};
    }
    if (double[].class.equals(in)) {
      return new double[]{10d, 11d};
    }
    if (float[].class.equals(in)) {
      return new float[]{10.3f, 11.3f};
    }
    if (int[].class.equals(in)) {
      return new int[]{100, 111};
    }
    if (long[].class.equals(in)) {
      return new long[]{1003L, 1111L};
    }
    if (in.isArray()) {
      Object[] res = (Object[]) Array.newInstance(in.getComponentType(), 2);
      for (int i = 0; i < res.length; i++) {
        res[i] = doInstanciation(in.getComponentType());
      }
      return res;
    }
    return instanciate(in);
  }

  /**
   * Write a file containing all paramaters of a study file. Then read it and test that the result is the same than the initial data.
   *
   * @throws Exception
   */
  public void testValidate() throws Exception {
    File tempFile = createTempFile(".xml");
    SParametresCAS res = (SParametresCAS) instanciate(SParametresCAS.class);
    createFile(res, tempFile);
    EdamoxXmlReaderWriter xmlWriter = new EdamoxXmlReaderWriter("1.0");
    CtuluLog valide = xmlWriter.isValide(tempFile);
    if (valide.isNotEmpty()) {
      valide.printResume();
    }
    assertTrue(valide.isEmpty());
    EdamoxXMLContent readContent = xmlWriter.readContent(tempFile, valide);
    assertNotNull(readContent.getParametresCas());
    EdamoxXmlReaderWriterTest.testCompare(res, readContent.getParametresCas());
  }

  /**
   * Just to check the content of the file. //WARN the absolute path must be change.
   *
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    //to be modified: create a xml file in a hardcoded file.
    SParametresCAS res = (SParametresCAS) instanciate(SParametresCAS.class);

    createFile(res, new File("C:\\data\\tmp\\masc.xml"));
  }
}
