/*
 GPL 2
 */
package org.fudaa.dodico.mascaret;

import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.corba.mascaret.SParametresCAS;
import org.fudaa.dodico.hydraulique1d.conv.ConvH1D_Masc;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;

/**
 * Read and Write xml files to check that all data are managed. A masc file is read, then write in xml format. The xml file is read and the read
 * object is compared to the initial one ( from the masc file).
 *
 * @author Frederic Deniger
 */
public class EdamoxXmlReaderWriterTest extends AbstractMascaretTestCase {

  /**
   * Test with ExMod.masc
   *
   * @throws IOException
   */
  public void testWriteExMod() throws IOException {
    testWrite(getOpthycaFile(EX_MODMASC));
  }

  /**
   * ondeSub7biefs3seuils.masc
   *
   * @throws IOException
   */
  public void testWriteOndeSub7biefs3seuils() throws IOException {
    testWrite(getOpthycaFile(ONDE_SUB7BIEFS3SEUILSMASC));
  }

  /**
   * NonPermanent3Biefs2CasierSings.masc
   *
   * @throws IOException
   */
  public void testWriteNonPermanent3Biefs2CasierSings() throws IOException {
    testWrite(getOpthycaFile(NON_PERMANENT3_BIEFS2_CASIER_SINGSMASC));
  }

  /**
   * NonPermanent3Biefs3Casier.masc
   *
   * @throws IOException
   */
  public void testWriteNonPermanent3Biefs3Casier() throws IOException {
    testWrite(getOpthycaFile(NON_PERMANENT3_BIEFS3_CASIERMASC));
  }

  /**
   * permanent1Bief1Apport.masc
   *
   * @throws IOException
   */
  public void testWritePermanent1Bief1Apport() throws IOException {
    testWrite(getOpthycaFile(PERMANENT1_BIEF1_APPORTMASC));
  }

  /**
   * QualiteeEauAvecSeuil.masc
   *
   * @throws IOException
   */
  public void testWriteQualiteeEauAvecSeuil() throws IOException {
    testWrite(getOpthycaFile(QUALITEE_EAU_AVEC_SEUILMASC));
  }

  /**
   * It's not an optimal solution but it works for tests...
   *
   * @param cas1
   * @param cas2
   * @return
   */
  public static void testCompare(SParametresCAS cas1, SParametresCAS cas2) {
    XStream xstream = new XStream();
    EdamoxXmlReaderWriter.registerConverters(xstream);
    xstream.setMode(XStream.NO_REFERENCES);
//    cas2.parametresGen.code=3;
    String cas1ToXml = xstream.toXML(cas1);
    String cas2ToXml = xstream.toXML(cas2);
    cas1ToXml = cas1ToXml.replace('\n', ' ');
    cas2ToXml = cas2ToXml.replace('\n', ' ');
    assertEquals(cas1ToXml, cas2ToXml);
  }

  private void testWrite(File f) throws IOException {
    MetierEtude1d etude1d = org.fudaa.dodico.hydraulique1d.metier.MetierEtude1dTest.readZipFile(f);
    SParametresCAS initContent = ConvH1D_Masc.convertirParametresCas(true, etude1d, "essai", false);
    File target = createTempFile(".xml");
//    File target = new File("C:\\data\\tmp\\toto.xml");
    EdamoxXmlReaderWriter xmlWriter = new EdamoxXmlReaderWriter("1.0");

    EdamoxXMLContent content = new EdamoxXMLContent(initContent);
    CtuluLog writeXML = xmlWriter.writeXML(content, target);
    assertTrue(writeXML.isEmpty());
    CtuluLog valide = xmlWriter.isValide(target);
    if (valide.isNotEmpty()) {
      valide.printResume();
    }
    assertTrue(valide.isEmpty());
    CtuluIOResult<EdamoxXMLContent> readXML = xmlWriter.readXML(target);
    assertTrue(readXML.getAnalyze().isEmpty());
    EdamoxXMLContent readContent = readXML.getSource();
    testCompare(initContent, readContent.getParametresCas());
  }
}
