/*
 GPL 2
 */
package org.fudaa.dodico.mascaret.io;

import java.io.File;
import java.io.IOException;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.corba.mascaret.SResultatsTemporelSpatial;
import org.fudaa.dodico.mascaret.AbstractMascaretTestCase;
import org.fudaa.dodico.mascaret.DResultatsMascaret;
import org.fudaa.dodico.mascaret.Opthyca1DReader;

/**
 *
 * @author Adrien Hadoux
 */
public class DResultatMascaretTestCase extends AbstractMascaretTestCase {

 
  public DResultatMascaretTestCase() {
  }

  
  public void testReadCalage5Crues9iterations() throws Exception {
	  File ficCalOPT = getOpthycaFile("mascaret_5crues_9iter.cal_opt");
	  
	  Opthyca1DReader reader = new Opthyca1DReader(ficCalOPT);
      SResultatsTemporelSpatial temoin = reader.read();
	  
	  SResultatsTemporelSpatial sres = DResultatsMascaret.
              litResultatsCalAuto(ficCalOPT, 2000,
                      DResultatsMascaret.OPTYCA);
	  assertNotNull(temoin);
	  assertNotNull(sres);
	  assertEquals(temoin.pasTemps.length,sres.pasTemps.length);
  }
  

  protected File getOpthycaFile(String filename) throws IOException {
    final String extension = ".cal_opt";
    File target = createTempFile(extension);
    return CtuluLibFile.getFileFromJar("/cal_opt/" + filename, target);
  }

 
}
