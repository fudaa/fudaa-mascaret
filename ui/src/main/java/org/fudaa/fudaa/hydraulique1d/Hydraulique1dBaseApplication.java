/*
 * @file         Hydraulique1dBaseApplication.java
 * @creation     2000-10-30
 * @modification $Date: 2005-08-16 13:33:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;
import com.memoire.bu.BuApplication;
/**
 * Classe fille de ��BuApplication�� et m�re de la classe ��MascaretApplication��.
 * Cette classe cr�e entre autre un champ ��static�� ��FRAME�� de sa propre instance.
 * En pratique, elle ne sert quasiment a rien si ce n\u2019est qu\u2019� respecter le formalisme de Fudaa.
 *
 * @version      $Revision: 1.7 $ $Date: 2005-08-16 13:33:37 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class Hydraulique1dBaseApplication extends BuApplication {
  public static java.awt.Frame FRAME= null;
  public Hydraulique1dBaseApplication() {
    super();
    FRAME= this;
  }
}
