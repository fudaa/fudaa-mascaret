/*
 * @file         Hydraulique1dBaseImplementation.java
 * @creation     2001-09-07
 * @modification $Date: 2007-11-20 11:43:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEventListener;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;

import org.fudaa.fudaa.commun.projet.FudaaParamChangeLog;

/**
 * Classe fille de ��FudaaImplementation�� et m�re de la classe ��MascaretImplementation��. Elle g�re l\u2019activation du menu Fichier/Enregistrer en
 * rep�rant une modification sur le mod�le. De plus, elle d�finit des champs ��static�� d\u2019autorisation de certains menus.
 *
 * @version $Revision: 1.11 $ $Date: 2007-11-20 11:43:05 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public abstract class Hydraulique1dBaseImplementation
        extends FudaaImplementation
        implements H1dObjetEventListener {

  public static int INTERDIT_MENU = -1;
  public static int AUTORISE_NOYAU = 0;
  public static int AUTORISE_RESEAU = 1;
  public static int AUTORISE_PLANIMETRAGE = 2;
  public static int AUTORISE_LOIS = 3;  
  public static int AUTORISE_MAILLAGE = 4;
 
  public static int AUTORISE_COND_INITIALES = 5;
  public static int AUTORISE_PARAM_TEMPOREL = 6;
  public static int AUTORISE_PARAMETRES = 7;
  public static int AUTORISE_PARAM_RESULTAT = 8;
  public static int AUTORISE_CALCULER = 9;
  public static int AUTORISE_RESULTATS_GENERAUX = 10;
  public static int AUTORISE_GRAPHES_RESULTATS = 11;
  protected static int etat = INTERDIT_MENU;
  // H1dObjetEventListener

  @Override
  public void objetCree(H1dObjetEvent e) {
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  @Override
  public void objetSupprime(H1dObjetEvent e) {
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  @Override
  public void objetModifie(H1dObjetEvent e) {
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(
            e.getMessage() + " (" + e.getChamp() + ")");
    setEnabledForAction("ENREGISTRER", true);
  }

  public void setEnableMenu(int _etat) {
  }

  @Override
  protected String _(String _s) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(_s);
  }

  abstract public void setEnableMenuQualiteDEau();
}
