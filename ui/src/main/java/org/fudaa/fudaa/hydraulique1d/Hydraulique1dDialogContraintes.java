/*
 * @file         Hydraulique1dDialogContraintes.java
 * @creation     2000-02-28
 * @modification $Date: 2006-10-19 17:29:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JComponent;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
/**
 * Classe fille de ��BuDialog��.
 * Cette classe permet d\u2019afficher des messages d\u2019avertissement notamment
 * concernant des contraintes � respecter sur les profils, les tableaux de zones ou de sites, etc\u2026
 * @version      $Revision: 1.8 $ $Date: 2006-10-19 17:29:58 $ by $Author: jm_lacombe $
 * @author       Axel von Arnim
 */
public class Hydraulique1dDialogContraintes extends BuDialog {
  public final static int IGNORER= 165;
  public final static int RETOUR= 166;
  JButton btRetour_, btIgnorer_;

  public Hydraulique1dDialogContraintes(Object _message, boolean can_ignore) {
      this((BuCommonInterface)Hydraulique1dBaseApplication.FRAME, ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
                      .getInformationsSoftware(), _message, can_ignore);
  }
  public Hydraulique1dDialogContraintes(String _titre, Object _message, boolean can_ignore) {
      this((BuCommonInterface)Hydraulique1dBaseApplication.FRAME, ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
                      .getInformationsSoftware(), _titre, _message, can_ignore);
  }
  public Hydraulique1dDialogContraintes(BuCommonInterface _parent,
                                        BuInformationsSoftware _isoft,
                                        Object _message, boolean can_ignore) {
    this(_parent, _isoft, "Contraintes", _message, can_ignore);
  }
  public Hydraulique1dDialogContraintes(
    BuCommonInterface _parent,
    BuInformationsSoftware _isoft,
    String _titre,
    Object _message,
    boolean can_ignore) {
    super(_parent, _isoft, BuResource.BU.getString(_titre), _message);
    BuPanel pnb= new BuPanel();
    pnb.setLayout(new FlowLayout(FlowLayout.RIGHT));
    btRetour_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Retour"));
    btRetour_.addActionListener(this);
    getRootPane().setDefaultButton(btRetour_);
    pnb.add(btRetour_);
    if (can_ignore) {
      btIgnorer_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Ignorer"));
      btIgnorer_.addActionListener(this);
      pnb.add(btIgnorer_);
    }
    content_.add(BorderLayout.SOUTH, pnb);
  }
  @Override
  public JComponent getComponent() {
    return null;
  }
  @Override
  public void actionPerformed(ActionEvent ev) {
    JComponent source= (JComponent)ev.getSource();
    if (source == btIgnorer_) {
      reponse_= IGNORER;
    } else if (source == btRetour_) {
      reponse_= RETOUR;
    }
    dispose();
  }
}
