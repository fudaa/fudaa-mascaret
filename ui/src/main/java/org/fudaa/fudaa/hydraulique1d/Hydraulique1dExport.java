/*
 * @file         Hydraulique1dExport.java
 * @creation     2001-04-26
 * @modification $Date: 2007-11-20 11:43:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauPoint;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresReprise;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParamPhysTracer;
import org.fudaa.dodico.mascaret.DParametresMascaret;
import org.fudaa.dodico.mascaret.DResultatsMascaret;
import org.fudaa.dodico.hydraulique1d.conv.ConvH1D_Masc;

import com.memoire.bu.BuFileFilter;
import org.fudaa.ctulu.CtuluLibFile;

/**
 * Classe qui contient des m�thodes ��static�� d\u2019exportation d\u2019un objet m�tier hydraulique1D en un fichier au format natif Mascaret et des
 * m�thodes de s�lection graphique de fichiers d\u2019exportation.
 *
 * @version $Revision: 1.24 $ $Date: 2007-11-20 11:43:05 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dExport {

  public static final void exportProfils_MASCARET(
          File filename,
          MetierBief[] biefs) {
    DParametresMascaret.ecritParametresGEO(
            filename,
            ConvH1D_Masc.convertirParametresGeo(biefs), true);
  }

  public static final void exportGeometrie_CASIER(
          File filename,
          MetierCasier[] casiers) {
    if (casiers == null) {
      return;
    }
    boolean presenceCasier = (casiers.length > 0);
    if (presenceCasier) {
      DParametresMascaret.ecritCasierGEO(
              filename,
              ConvH1D_Masc.convertirParametresGeoCasiers(casiers));
    }
  }

  public static final void exportReprise_MASCARET(
          File filename,
          MetierParametresReprise paramReprise) {
    if (paramReprise == null) {
      return;
    }
    if (paramReprise.contenu() == null) {
      return;
    }
    if (paramReprise.contenu().equals("")) {
      return;
    }
    DParametresMascaret.ecritParametresREP(
            filename,
            ConvH1D_Masc.convertirParametresRep(paramReprise));
  }

  /**
   * Exportation du fichier cas en mode simulation ou mode calage.
   *
   * @param _calage true : Mode calage.
   * @param filename File
   * @param etude MetierEtude1d
   */
  public static final void exportCas_MASCARET(boolean _calage, File filename, MetierEtude1d etude) {
    String nomFichierSansExtension = filename.getAbsolutePath();
    int indexe;
    if ((indexe = nomFichierSansExtension.lastIndexOf('.')) != -1) {
      nomFichierSansExtension = nomFichierSansExtension.substring(0, indexe);
    }
    DParametresMascaret.ecritParametresCAS(
            filename,
            ConvH1D_Masc.convertirParametresCas(_calage, etude, nomFichierSansExtension, false));
  }

  public static final void exportXCas_MASCARET(boolean _calage, File filename, MetierEtude1d etude) {
    File xcasFile = CtuluLibFile.appendExtensionIfNeeded(filename, "xcas");
    String nomSansExtension = CtuluLibFile.getSansExtension(xcasFile.getAbsolutePath());
    DParametresMascaret.ecritParametresXCAS(
            filename,
            ConvH1D_Masc.convertirParametresCas(_calage, etude, nomSansExtension, true));
  }

  public static final void exportLoisHydro_MASCARET(String nomBase, MetierEtude1d etude) {
    DParametresMascaret.ecritLoisHydrauliques(
            nomBase,
            false,
            ConvH1D_Masc.convertirLoisHydrauliques(etude));
  }

  public static final void exportLoisTracer_MASCARET(String nomBase, MetierLoiTracer[] loisTracer, String[][] nomsTracer) {
    DParametresMascaret.ecritLoisTracer(
            nomBase,
            false,
            ConvH1D_Masc.convertirLoisTracer(loisTracer, nomsTracer));
  }

  public static final void exportListing_DAMOCLES(
          File filename,
          byte[] contenu) {
    try {
      if (contenu == null) {
        return;
      }
      DResultatsMascaret.ecritResultatsDAMOC(
              filename,
              ConvH1D_Masc.convertirResultatsLisDamocles(contenu));
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public static final void exportListing(File filename, byte[] contenu) {
    try {
      if (contenu == null) {
        return;
      }
      DResultatsMascaret.ecritResultatsLIS(
              filename,
              ConvH1D_Masc.convertirResultatsLis(contenu));
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public static final void exportRubens(File filename, MetierResultatsTemporelSpatial ires) {
    try {
      if (ires == null) {
        return;
      }
      DResultatsMascaret.ecritResultats(DResultatsMascaret.RUBENS,
              filename,
              ConvH1D_Masc.convertirResultatsTemporelSpatialH1dToMas(ires));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public static final void exportOpthyca(File filename, MetierResultatsTemporelSpatial ires) {
    try {
      if (ires == null) {
        return;
      }
      DResultatsMascaret.ecritResultats(DResultatsMascaret.OPTYCA,
              filename,
              ConvH1D_Masc.convertirResultatsTemporelSpatialH1dToMas(ires));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public static final void exportOpthycaCasier(File filename, MetierEtude1d etude) {
    try {
      MetierResultatsTemporelSpatial ires = etude.resultatsGeneraux().resultatsTemporelCasier();
      if (ires == null) {
        return;
      }
      DResultatsMascaret.ecritResultats(DResultatsMascaret.OPTYCA,
              filename,
              ConvH1D_Masc.convertirResultatsTemporelSpatialH1dToMas(ires));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public static final void exportOpthycaLiaison(
          File filename,
          MetierEtude1d etude) {
    try {
      MetierResultatsTemporelSpatial ires = etude.resultatsGeneraux().resultatsTemporelLiaison();
      if (ires == null) {
        return;
      }
      DResultatsMascaret.ecritResultats(DResultatsMascaret.OPTYCA,
              filename,
              ConvH1D_Masc.convertirResultatsTemporelSpatialH1dToMas(ires));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Exportation du fichier r�sultat de calage automatique.
   *
   * @param filename File Le fichier d'export.
   * @param etude MetierEtude1d L'�tude.
   */
  public static final void exportResuCalageAutoOpthyca(File filename, MetierEtude1d etude) {
    try {
      MetierResultatsTemporelSpatial ires = etude.calageAuto().resultats().resultats();
      if (ires == null) {
        return;
      }

      DResultatsMascaret.ecritResultats(DResultatsMascaret.OPTYCA,
              filename,
              ConvH1D_Masc.convertirResultatsTemporelSpatialH1dToMas(ires));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Exportation du fichier r�sultat de calage automatique au format rubens.
   *
   * @param filename File Le fichier d'export.
   * @param etude MetierEtude1d L'�tude.
   */
  public static final void exportResuCalageAutoRubens(File filename, MetierEtude1d etude) {
    try {
      MetierResultatsTemporelSpatial ires = etude.calageAuto().resultats().resultats();
      if (ires == null) {
        return;
      }

      DResultatsMascaret.ecritResultats(DResultatsMascaret.RUBENS,
              filename,
              ConvH1D_Masc.convertirResultatsTemporelSpatialH1dToMas(ires));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public static final File chooseFile(String _ext) {
    final String ext = _ext;
    File res = null;
    CtuluFileChooser chooser = new CtuluFileChooser();
    chooser.setDialogType(JFileChooser.SAVE_DIALOG);
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
      @Override
      public boolean accept(java.io.File f) {
        return (ext == null)
                ? true
                : (f.getName().endsWith("." + ext) || f.isDirectory());
      }

      @Override
      public String getDescription() {
        return (ext == null)
                ? "*.*"
                : ("Fichiers " + ext.toUpperCase() + " (*." + ext + ")");
      }
    });
    chooser.setCurrentDirectory(Hydraulique1dResource.lastExportDir);
    int returnVal = chooser.showSaveDialog(Hydraulique1dBaseApplication.FRAME);
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    // ajoute �ventuelement l'extension � la fin fu fichier
    int indexSlash = filename.lastIndexOf(java.io.File.separatorChar);
    if (indexSlash > 0) {
      String nomFicSeul = filename.substring(indexSlash + 1);
      if (ext != null && !nomFicSeul.endsWith("." + ext)) {
        filename += "." + ext;
      }
    }

    res = new File(filename);
    if (indexSlash > 0) {
      Hydraulique1dResource.lastExportDir
              = new java.io.File(filename.substring(0, indexSlash));
    } else {
      Hydraulique1dResource.lastExportDir = chooser.getCurrentDirectory();
    }
    return res;
  }

  public static File chooseFile(String[] extentions, String[] descriptions) {
    CtuluFileChooser chooser = new CtuluFileChooser();
    chooser.setDialogType(JFileChooser.SAVE_DIALOG);
    for (int i = 0; (i < extentions.length) && (i < descriptions.length); i++) {
      chooser.addChoosableFileFilter(
              new BuFileFilter(extentions[i], descriptions[i]));
    }
    chooser.setCurrentDirectory(Hydraulique1dResource.lastExportDir);
    int returnVal = chooser.showSaveDialog(Hydraulique1dBaseApplication.FRAME);
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }

    // ajoute �ventuelement l'extension � la fin fu fichier
    int indexSlash = filename.lastIndexOf(java.io.File.separatorChar);
    if (indexSlash > 0) {
      String nomFicSeul = filename.substring(indexSlash + 1);
      String ext = getExtension(chooser.getFileFilter(), extentions);
      if (ext != null) {
        if (!nomFicSeul.endsWith("." + ext)) {
          filename += "." + ext;
        }
      }
    }

    File res = null;
    res = new File(filename);
    if (indexSlash > 0) {
      Hydraulique1dResource.lastExportDir
              = new java.io.File(filename.substring(0, indexSlash));
    } else {
      Hydraulique1dResource.lastExportDir = chooser.getCurrentDirectory();
    }
    return res;
  }

  public static String getExtension(FileFilter filter, String[] extensions) {
    //String res=null;
    String desc = filter.getDescription();
    for (int i = 0; i < extensions.length; i++) {
      if (desc.indexOf("(*." + extensions[i] + ")") != -1) {
        return extensions[i];
      }
    }
    return null;
  }

  /**
   * exportLigneDEauInitiale
   *
   * @param file File
   * @param iLigneEauInitiale MetierLigneEauInitiale
   * @param reseau
   * @param decalage
   * @param EstAbsolu booleen indiquant si les abscisses sonts relatives ou absolues
   * @param ExportRubens booleen indiquant le type d'export voulu (0 : Mascaret ou 1: Rubens par defaut Mascaret)
   */
  public static final void exportLigneDEauInitiale(File file,
          MetierLigneEauInitiale iLigneEauInitiale, MetierReseau reseau, double decalage, boolean EstAbsolu, boolean ExportRubens) {

    try {

      //On recupere la ligne d'eau
      if (iLigneEauInitiale == null) {
        return;
      }
      MetierLigneEauPoint[] tabLigneEauPoint = iLigneEauInitiale.points();

      //conversion du tableau Hydraulique1dLigneLigneDEauTableau[] en MetierResultatsTemporelSpatial
      MetierResultatsTemporelSpatial ires;
      if (ExportRubens && !EstAbsolu) {
        ires = ConvH1D_Masc.convertitDLigneEauPoint_IResultatTemporelSpatial(tabLigneEauPoint, reseau, decalage, true);
      } else {
        ires = ConvH1D_Masc.convertitDLigneEauPoint_IResultatTemporelSpatial(tabLigneEauPoint, reseau, decalage, false);
      }

      exportRubens(file, ires);

    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new RuntimeException(
              Hydraulique1dResource.HYDRAULIQUE1D.getString("Probl�me lors de la conversion en fichier de ligne d'eau initiale") + "\n : "
              + Hydraulique1dResource.HYDRAULIQUE1D.getString("convertion des Parametres")
              + ex.getLocalizedMessage());
    }

  }

  public static final void exportParamsPhysiquesQE(File filename, MetierParamPhysTracer[] iparam) {
    try {
      if (iparam == null) {
        return;
      }
      DParametresMascaret.ecritParamsPhysiquesTracer(filename,
              ConvH1D_Masc.convertirParametresPhysiqueQE(iparam));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
