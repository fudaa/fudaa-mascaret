/*
 * @file         Hydraulique1dExportPanneau.java
 * @creation     2001-04-26
 * @modification $Date: 2007-11-20 11:43:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import javax.swing.BorderFactory;

/**
 * Une boite de selection d'exports pour Hydraulique1d.
 *
 * @version $Revision: 1.18 $ $Date: 2007-11-20 11:43:03 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dExportPanneau
        extends JDialog
        implements ActionListener, WindowListener {
  // Donnees membres publiques

  public final static int CANCEL = 0;
  public final static int OK = 1;
  // Donnees membres privees
  private int status_;
  private File dir_;
  private BuPanel pnChoix_, pnChoixParam_, pnChoixResu_, pnOk_, pnGen_;
  private JCheckBox cbAll_;
  private BuTextField tfDir_;
  private JButton b_ok_, b_cancel_, btDir_;
  private Hashtable cbs_;
  private String[] choix_;
  // Constructeurs

  public Hydraulique1dExportPanneau(Frame _parent) {
    super(_parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Exportation"), true);
    status_ = CANCEL;
    dir_ = Hydraulique1dResource.lastExportDir;
    pnChoixParam_ = new BuPanel();
    pnChoixParam_.setLayout(new BuGridLayout(3, 5, 5, true, true));
    CompoundBorder bdChoixParam =
            new CompoundBorder(
            new EtchedBorder(),
            new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbChoixParam = new TitledBorder(bdChoixParam, Hydraulique1dResource.HYDRAULIQUE1D.getString("Param�tres"));
    pnChoixParam_.setBorder(tbChoixParam);
    int n = 0;
    cbs_ = new Hashtable();
    JCheckBox cb;
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Profils (format Mascaret)"));
    cbs_.put("GEO_MASCA", cb);
    pnChoixParam_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier cas xml (xcas)"));
    cbs_.put("XCAS_MASCA", cb);
    pnChoixParam_.add(cb, n++);
//    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier cas pour simulation"));
//    cbs_.put("CAS_MASCA", cb);
//    pnChoixParam_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier ligne d'eau initiale (Format Mascaret)"));
    cbs_.put("LIG_MASCA", cb);
    pnChoixParam_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichiers des lois hydrau."));
    cbs_.put("LOIS_MASCA", cb);
    pnChoixParam_.add(cb, n++);
    if (CGlobal.AVEC_CALAGE_AUTO) {
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier xcas pour calage"));
      cbs_.put("XCAS_CALAGE", cb);
      pnChoixParam_.add(cb, n++);
    }
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier ligne d'eau initiale (Format Rubens)"));
    cbs_.put("LIG_RUB", cb);
    cb.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Les abscisses relatives seront converties en absolues pour pouvoir etre lues par Rubens"));
    pnChoixParam_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier reprise en lecture"));
    cbs_.put("REP_MASCA", cb);
    pnChoixParam_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier g�om�trie des casiers"));
    cbs_.put("GEO_CASIER", cb);
    pnChoixParam_.add(cb, n++);
    if (CGlobal.AVEC_QUALITE_DEAU) {
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier des lois Tracer"));
      cbs_.put("LOI_TRACER", cb);
      pnChoixParam_.add(cb, n++);
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier des param�tres physiques"));
      cbs_.put("PARAM_PHY", cb);
      pnChoixParam_.add(cb, n++);
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier des concentrations initiales"));
      cbs_.put("CONCS_INITS", cb);
      pnChoixParam_.add(cb, n++);
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier m�t�o"));
      cbs_.put("METEO", cb);
      pnChoixParam_.add(cb, n++);
    }




    pnChoixResu_ = new BuPanel();
    pnChoixResu_.setLayout(new BuGridLayout(3, 5, 5, true, true));
    CompoundBorder bdChoixResu =
            new CompoundBorder(
            new EtchedBorder(),
            new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbChoixResu = new TitledBorder(bdChoixResu, Hydraulique1dResource.HYDRAULIQUE1D.getString("R�sultats"));
    pnChoixResu_.setBorder(tbChoixResu);
    n = 0;
//    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier listing Damocles"));
//    cbs_.put("DAMO_MASCA", cb);
//    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier listing mascaret"));
    cbs_.put("LIST_MASCA", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier reprise en �criture"));
    cbs_.put("REP_ECRI_MASCA", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat mascaret (format Rubens)"));
    cbs_.put("RESU_MASCA", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat mascaret (format Opthyca)"));
    cbs_.put("RESU_OPTYCA", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat casier (format Opthyca)"));
    cbs_.put("RESU_CASIER_OPTYCA", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat casier (format Rubens)"));
    cbs_.put("RESU_CASIER_RUBENS", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier listing casier"));
    cbs_.put("RESU_CASIER_LISTING", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat liaison (format Opthyca)"));
    cbs_.put("RESU_LIAISON_OPTYCA", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat liaison (format Rubens)"));
    cbs_.put("RESU_LIAISON_RUBENS", cb);
    pnChoixResu_.add(cb, n++);
    cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier listing liaison"));
    cbs_.put("RESU_LIAISON_LISTING", cb);
    pnChoixResu_.add(cb, n++);
    if (CGlobal.AVEC_CALAGE_AUTO) {
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat calage (format Opthyca)"));
      cbs_.put("RESU_CALAGE_OPTHYCA", cb);
      pnChoixResu_.add(cb, n++);
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat calage (format Rubens)"));
      cbs_.put("RESU_CALAGE_RUBENS", cb);
      pnChoixResu_.add(cb, n++);
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier listing calage"));
      cbs_.put("LIST_CALAGE", cb);
      pnChoixResu_.add(cb, n++);
    }
    if (CGlobal.AVEC_QUALITE_DEAU) {
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier listing Tracer"));
      cbs_.put("LIST_TRACER", cb);
      pnChoixResu_.add(cb, n++);
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat Tracer (format Rubens)"));
      cbs_.put("RESU_TRACER_RUBENS", cb);
      pnChoixResu_.add(cb, n++);
      cb = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat Tracer (format Opthyca)"));
      cbs_.put("RESU_TRACER_OPTYCA", cb);
      pnChoixResu_.add(cb, n++);
      cb = new JCheckBox(
              Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat Mascaret et Tracer (format Opthyca)"));
      cbs_.put("RESU_TRACER_ET_MASCARET_OPTYCA", cb);
      pnChoixResu_.add(cb, n++);
      cb = new JCheckBox(
              Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier r�sultat Mascaret et Tracer (format Rubens)"));
      cbs_.put("RESU_TRACER_ET_MASCARET_RUBENS", cb);
      pnChoixResu_.add(cb, n++);
    }
    choix_ = new String[cbs_.size()];
    pnChoix_ = new BuPanel();
    pnChoix_.setLayout(new BuVerticalLayout());
    pnChoix_.setBorder(
            new CompoundBorder(
            new EtchedBorder(),
            new EmptyBorder(new Insets(5, 5, 5, 5))));
    n = 0;
    pnChoix_.add(pnChoixParam_, n++);
    pnChoix_.add(pnChoixResu_, n++);
    n = 0;
    pnGen_ = new BuPanel();
    pnGen_.setLayout(new BuVerticalLayout());
    pnGen_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnGen_.add(pnChoix_, n++);
    cbAll_ = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Toute l'�tude"));
    cbAll_.setSelected(false);
    cbAll_.addActionListener(this);
    pnGen_.add(cbAll_, n++);
    BuPanel pnDir = new BuPanel(new BorderLayout(5, 0));
    pnDir.setBorder(BorderFactory.createEmptyBorder(5, 1, 1, 1));
    tfDir_ = new BuTextField();
    tfDir_.setColumns(20);
    pnDir.add(tfDir_);
    btDir_ = new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Parcourir"));
    btDir_.setActionCommand("PARCOURIR");
    btDir_.addActionListener(this);
    pnDir.add(btDir_, BorderLayout.EAST);
    pnDir.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Exporter vers:")), BorderLayout.WEST);
    pnGen_.add(pnDir, n++);
    pnOk_ = new BuPanel();
    b_ok_ = new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    b_ok_.setActionCommand("VALIDER");
    b_ok_.addActionListener(this);
    b_cancel_ = new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    b_cancel_.setActionCommand("ANNULER");
    b_cancel_.addActionListener(this);
    pnOk_.add(b_ok_);
    pnOk_.add(b_cancel_);
    Container content = getContentPane();
    content.setLayout(new BorderLayout());
    content.add(pnGen_, BorderLayout.NORTH);
    content.add(pnOk_, BorderLayout.SOUTH);
    pack();
    setResizable(false);
    setLocationRelativeTo(_parent);
  }
  // Actions

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String action = _evt.getActionCommand();
    Object src = _evt.getSource();
    if (src == cbAll_) {
      Enumeration e = cbs_.elements();
      if (cbAll_.isSelected()) {
        while (e.hasMoreElements()) {
          JCheckBox cb = (JCheckBox) e.nextElement();
          if (cb.isEnabled()) {
            cb.setSelected(true);
          }
        }
      } else {
        while (e.hasMoreElements()) {
          JCheckBox cb = (JCheckBox) e.nextElement();
          cb.setSelected(false);
        }
      }
    } else if (action.equals("ANNULER")) {
      dir_ = Hydraulique1dResource.lastExportDir;
      for (int i = 0; i < choix_.length; i++) {
        choix_[i] = null;
      }
      status_ = CANCEL;
      dispose();
    } else if (action.equals("VALIDER")) {
      Enumeration e = cbs_.keys();
      String key = null;
      int i = 0;
      while (e.hasMoreElements()) {
        key = (String) e.nextElement();
        if (((JCheckBox) cbs_.get(key)).isSelected()) {
          choix_[i++] = key;
        } else {
          choix_[i++] = null;
        }
      }
      String txt = tfDir_.getText();
      if ((txt == null) || ("".equals(txt))) {
        dir_ = null;
        new BuDialogError(
                (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                .getInformationsSoftware(),
                Hydraulique1dResource.HYDRAULIQUE1D.getString("Vous devez choisir un nom de fichier!"))
                .activate();
        return;
      } /*else {*/
      int indSlash = txt.lastIndexOf(File.separator);
      int ind = txt.lastIndexOf('.');
      if ((ind > 0) && (ind > indSlash)) {
        txt = txt.substring(0, ind);
      }
      dir_ = new File(txt);
      //}
      File parentf = dir_.getParentFile();
      if ((parentf != null) && (!parentf.canWrite())) {
        dir_ = null;
        new BuDialogError(
                (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                .getInformationsSoftware(),
                Hydraulique1dResource.HYDRAULIQUE1D.getString("Le r�pertoire ") + parentf + "\n"
                + Hydraulique1dResource.HYDRAULIQUE1D.getString("n'existe pas!"))
                .activate();
        return;
      }
      status_ = OK;
      dispose();
    } else if (action.equals("PARCOURIR")) {
      File file = Hydraulique1dExport.chooseFile(null);
      tfDir_.setText(file == null ? "" : file.getPath());
    }
  }

  public String[] getChoix() {
    return choix_;
  }

  public File getFichier() {
    return dir_;
  }

  public int valeurRetour() {
    return status_;
  }
  // Window events

  @Override
  public void windowActivated(WindowEvent e) {
  }

  @Override
  public void windowClosed(WindowEvent e) {
  }

  @Override
  public void windowClosing(WindowEvent e) {
    dir_ = Hydraulique1dResource.lastExportDir;
    for (int i = 0; i < choix_.length; i++) {
      choix_[i] = null;
    }
    dispose();
  }

  @Override
  public void windowDeactivated(WindowEvent e) {
  }

  @Override
  public void windowDeiconified(WindowEvent e) {
  }

  @Override
  public void windowIconified(WindowEvent e) {
  }

  @Override
  public void windowOpened(WindowEvent e) {
  }
}
