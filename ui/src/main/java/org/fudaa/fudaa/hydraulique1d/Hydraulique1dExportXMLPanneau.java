/*
 * @file         Hydraulique1dExportXMLPanneau.java
 * @creation     2001-07-30
 * @modification $Date: 2006-09-12 08:36:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
/**
 * Boite de dialogue affich� lors de l\u2019exportation des fichiers XML.
 * menu Fichier/Exporter/XML.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-12 08:36:42 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dExportXMLPanneau
  extends JDialog
  implements ActionListener, WindowListener {
  // Donnees membres publiques
  public final static int CANCEL= 0;
  public final static int OK= 1;
  // Donnees membres privees
  private int status_;
  private File dir_;
  private BuPanel pnChoix_, pnOk_, pnGen_;
  private JCheckBox cbAll_;
  private BuTextField tfDir_;
  private JButton b_ok_, b_cancel_, btDir_;
  private Hashtable cbs_;
  private String[] choix_;
  // Constructeurs
  public Hydraulique1dExportXMLPanneau(Frame _parent) {
    super(_parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Exportation"), true);
    status_= CANCEL;
    dir_= Hydraulique1dResource.lastExportDir;
    int n= 0;
    pnChoix_= new BuPanel();
    pnChoix_.setLayout(new BuGridLayout(2, 5, 5, true, true));
    pnChoix_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    cbs_= new Hashtable();
    JCheckBox cb;
    cb= new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("L'�tude sans la partie graphique"));
    cbs_.put("ETUDE1D", cb);
    pnChoix_.add(cb, n++);
    cb= new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("R�seau graphique de l'�tude"));
    cbs_.put("RESEAU", cb);
    pnChoix_.add(cb, n++);
    choix_= new String[cbs_.size()];
    n= 0;
    pnGen_= new BuPanel();
    pnGen_.setLayout(new BuVerticalLayout());
    pnGen_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnGen_.add(pnChoix_, n++);
    cbAll_= new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Toute l'�tude"));
    cbAll_.setSelected(false);
    cbAll_.addActionListener(this);
    pnGen_.add(cbAll_, n++);
    BuPanel pnDir= new BuPanel();
    tfDir_= new BuTextField();
    tfDir_.setColumns(8);
    pnDir.add(tfDir_);
    btDir_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Parcourir"));
    btDir_.setActionCommand("PARCOURIR");
    btDir_.addActionListener(this);
    pnDir.add(btDir_);
    pnGen_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Exporter vers :")), n++);
    pnGen_.add(pnDir, n++);
    pnOk_= new BuPanel();
    b_ok_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    b_ok_.setActionCommand("VALIDER");
    b_ok_.addActionListener(this);
    b_cancel_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    b_cancel_.setActionCommand("ANNULER");
    b_cancel_.addActionListener(this);
    pnOk_.add(b_ok_);
    pnOk_.add(b_cancel_);
    Container content= getContentPane();
    content.setLayout(new BorderLayout());
    content.add(pnGen_, BorderLayout.NORTH);
    content.add(pnOk_, BorderLayout.SOUTH);
    pack();
    setResizable(false);
    setLocationRelativeTo(_parent);
  }
  // Actions
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String action= _evt.getActionCommand();
    Object src= _evt.getSource();
    if (src == cbAll_) {
      Enumeration e= cbs_.elements();
      if (cbAll_.isSelected()) {
        while (e.hasMoreElements()) {
          JCheckBox cb= (JCheckBox)e.nextElement();
          if (cb.isEnabled())
            cb.setSelected(true);
        }
      } else {
        while (e.hasMoreElements()) {
          JCheckBox cb= (JCheckBox)e.nextElement();
          cb.setSelected(false);
        }
      }
    } else if (action.equals("ANNULER")) {
      dir_= Hydraulique1dResource.lastExportDir;
      for (int i= 0; i < choix_.length; i++)
        choix_[i]= null;
      status_= CANCEL;
      dispose();
    } else if (action.equals("VALIDER")) {
      Enumeration e= cbs_.keys();
      String key= null;
      int i= 0;
      while (e.hasMoreElements()) {
        key= (String)e.nextElement();
        if (((JCheckBox)cbs_.get(key)).isSelected())
          choix_[i++]= key;
        else
          choix_[i++]= null;
      }
      String txt= tfDir_.getText();
      if ((txt == null) || ("".equals(txt))) {
        dir_= null;
        new BuDialogError(
          (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
          ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
            .getInformationsSoftware(),
          Hydraulique1dResource.HYDRAULIQUE1D.getString("Vous devez choisir un nom de fichier!"))
          .activate();
        return;
      }/* else {*/
        int indSlash= txt.lastIndexOf(File.separator);
        int ind= txt.lastIndexOf('.');
        if ((ind > 0) && (ind > indSlash))
          txt= txt.substring(0, ind);
        dir_= new File(txt);
      //}
      File parentf= dir_.getParentFile();
      if ((parentf != null) && (!parentf.canWrite())) {
        dir_= null;
        new BuDialogError(
          (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
          ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
            .getInformationsSoftware(),
          Hydraulique1dResource.HYDRAULIQUE1D.getString("Le r�pertoire ") + parentf + "\n" +
          Hydraulique1dResource.HYDRAULIQUE1D.getString("n'existe pas!"))
          .activate();
        return;
      }
      status_= OK;
      dispose();
    } else if (action.equals("PARCOURIR")) {
      File file= Hydraulique1dExport.chooseFile(null);
      tfDir_.setText(file == null ? "" : file.getPath());
    }
  }
  public String[] getChoix() {
    return choix_;
  }
  public File getFichier() {
    return dir_;
  }
  public int valeurRetour() {
    return status_;
  }
  // Window events
  @Override
  public void windowActivated(WindowEvent e) {}
  @Override
  public void windowClosed(WindowEvent e) {}
  @Override
  public void windowClosing(WindowEvent e) {
    dir_= Hydraulique1dResource.lastExportDir;
    for (int i= 0; i < choix_.length; i++)
      choix_[i]= null;
    dispose();
  }
  @Override
  public void windowDeactivated(WindowEvent e) {}
  @Override
  public void windowDeiconified(WindowEvent e) {}
  @Override
  public void windowIconified(WindowEvent e) {}
  @Override
  public void windowOpened(WindowEvent e) {}
}
