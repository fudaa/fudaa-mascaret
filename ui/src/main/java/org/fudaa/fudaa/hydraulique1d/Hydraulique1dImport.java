/*
 * @file         Hydraulique1dImport.java
 * @creation     1999-08-03
 * @modification $Date: 2008-03-04 15:46:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import gnu.trove.TDoubleArrayList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JFileChooser;

import jxl.Sheet;
import jxl.Workbook;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.mascaret.SParametresCAS;
import org.fudaa.dodico.corba.mascaret.SParametresGEO;
import org.fudaa.dodico.corba.mascaret.SParametresLoi;
import org.fudaa.dodico.corba.mascaret.SParametresLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierMethodeMaillage;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSections;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeries;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeriesUnitaire;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierMaillage;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.MetierSite;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierGeometrieCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierNuagePointsCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierPlanimetrageCasier;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParamPhysTracer;
import org.fudaa.dodico.lido.DParametresLido;
import org.fudaa.dodico.mascaret.DParametresMascaret;
import org.fudaa.dodico.mascaret.DResultatsMascaret;
import org.fudaa.dodico.mascaret.FichierMascaretException;
import org.fudaa.dodico.hydraulique1d.conv.ConvMasc_H1D;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneConcentrationsInitialesTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneLaisseTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneLigneDEauTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneZoneFrottementTableau;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuFileFilter;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.mascaret.EdamoxXMLContent;

/**
 * classe qui contient des m�thodes ��static�� d\u2019importation un fichier au format natif Mascaret produisant des objets m�tier hydraulique1D et
 * des m�thodes de s�lection graphique de fichiers d\u2019importation.
 *
 * @version $Revision: 1.43 $ $Date: 2008-03-04 15:46:52 $ by $Author: opasteur $
 * @author Axel von Arnim
 */
public class Hydraulique1dImport {

  public static final String getS(final String _s) {
    return Hydraulique1dResource.getS(_s);
  }

  public static MetierPoint[] pointsXYZ(File filename) {
    MetierPoint[] nouveauTableau = null;
    Vector lignesTableau = new Vector();
    Vector ligneTableau;
    String logMsg = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    int lineNb = 0;
    int motNb = 0;
    String line = null;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if (line.isEmpty()
                || line.startsWith("#")
                || line.startsWith("C")
                || line.startsWith("B")) {
          continue; // ligne suivante
        }
        StringTokenizer st = new StringTokenizer(line);
        ligneTableau = new Vector();
        motNb = 0;
        while (st.hasMoreTokens() && motNb <= 2) {
          String mot = st.nextToken();
          motNb++;
          ligneTableau.add(Double.valueOf(mot));
        }
        if (ligneTableau.size() > 0) {
          lignesTableau.add(ligneTableau);
        }
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      System.err.println("OK");
      logMsg += "OK\n";
    } catch (NumberFormatException e) {
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "' champ numero "
              + motNb);
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "' champ numero "
              + motNb
              + "\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    if (lignesTableau.size() > 0) {
      nouveauTableau = new MetierPoint[lignesTableau.size()];
      for (int i = 0; i < lignesTableau.size(); i++) {
        MetierPoint point = new MetierPoint();
        nouveauTableau[i] = point;
        Vector ligne = (Vector) lignesTableau.get(i);
        nouveauTableau[i].x = ((Double) ligne.get(0)).doubleValue();
        nouveauTableau[i].y = ((Double) ligne.get(1)).doubleValue();
        nouveauTableau[i].z = ((Double) ligne.get(2)).doubleValue();
      }
    }
    return nouveauTableau;
  }

  public static MetierGeometrieCasier[] geometrieCasier(
          File filename,
          boolean single) {
    String etat = "debut";
    ListeGeoCasier listeCasiers = new ListeGeoCasier();
    String logMsg
            = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    int lineNb = 0;
    //int motNb= 0;
    String line = null;
    GeoCasier nouveauCasier = null;
    MetierGeometrieCasier[] tableau = null;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if ((line.isEmpty()) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        StringTokenizer st = new StringTokenizer(line);
        String mot1 = st.nextToken();
        String mot2 = st.nextToken();
        if (etat.equals("debut")) {
          if (mot1.equalsIgnoreCase("casier")) {
            nouveauCasier = new GeoCasier(mot2);
            etat = "entete";
          } else {
            my_perror(
                    getS("debut du fichier incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("debut du fichier incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
        } else if (etat.equals("entete")) {
          if (mot1.equalsIgnoreCase("casier")) {
            my_perror(getS("casier vide") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")");
            logMsg += getS("casier vide") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")\n";
            break;
          } /*else {*/

          nouveauCasier.ajouteUnPoint(line);
          etat = "points";
          //}
        } else if (etat.equals("points")) {
          if (mot1.equalsIgnoreCase("casier")) {
            if (single) {
              my_perror(
                      getS("plusieurs casiers d�finis") + ": '"
                      + line
                      + "' (" + getS("ligne") + " "
                      + lineNb
                      + ")");
              logMsg += getS("plusieurs casiers d�finis") + ": '"
                      + line
                      + "' (" + getS("ligne") + " "
                      + lineNb
                      + ")\n";
              break;
            }/* else {*/

            listeCasiers.ajoutCasier(nouveauCasier);
            nouveauCasier = new GeoCasier(mot2);
            etat = "entete";
            /*}*/
          } else {
            nouveauCasier.ajouteUnPoint(line);
            //          etat="points";
          }
        } // fin test etat
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      listeCasiers.ajoutCasier(nouveauCasier);
      String verifLog = listeCasiers.verifie();
      if (verifLog == null) {
        System.err.println("OK");
        logMsg += "OK\n";
        tableau = listeCasiers.transformeTableauMetier();
      } else {
        System.err.println(verifLog);
        logMsg += verifLog;
      }
    } catch (NoSuchElementException e) {
      my_perror(
              getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (NumberFormatException e) {
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    return tableau;
  }

  public static Hydraulique1dProfilModel[] importProfils(
          File filename,
          String type) {
    Hydraulique1dProfilModel[] imports = null;
    if ("TXT".equals(type)) {
      imports = Hydraulique1dImport.importProfilsTXT(filename);
    } else if ("PRO_LIDO".equals(type)) {
      imports = Hydraulique1dImport.importProfilsPRO_LIDO(filename);
    } else if ("PRO_MASCARET".equals(type)) {
      imports = Hydraulique1dImport.importProfilsPRO_MASCARET(filename);
    } else if ("PRO_RIVICAD".equals(type)) {
      imports = Hydraulique1dImport.importProfilsPRO_RIVICAD(filename);
    } else if ("PRO_EXCEL".equals(type)) {
      imports = Hydraulique1dImport.importProfilsPRO_EXCEL(filename);
    }
    return imports;
  }

  public static double[][] importTableauLoiHydraulique(
          File filename,
          MetierLoiHydraulique loi) {
    double[][] imports = null;
    if ("SEUIL".equals(loi.typeLoi())) {
      imports = Hydraulique1dImport.importLoiSeuil(filename);
    } else {
      StringBuilder sb = new StringBuilder();
      sb.append(getS("R�sultat de l'importation de ")).append(filename.getPath()).append(":\n\n");
      try {
        imports = Hydraulique1dImport.importLoi(filename);
        sb.append("OK\n");
      } catch (IOException _exc) {
        sb.append(_exc.getMessage());
      }
      showMessage(sb.toString());
    }
    return imports;
  }

  public static double[][] importLoiSeuil(File filename) {
    double[][] nouveauTableau = null;
    Vector Zavals = new Vector();
    Vector debits = new Vector();
    Hashtable ZAmonts = new Hashtable();
    String logMsg
            = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    int lineNb = 0;
    int motNb = 0;
    String line = null;
    //String uniteTemps=null; Fred deniger variable inutile
    try {
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if ((line.isEmpty()) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        StringTokenizer st = new StringTokenizer(line);
        motNb = 0;
        Double debit = Double.valueOf(st.nextToken());
        motNb++;
//        Double Zamont = Double.valueOf(st.nextToken());
        Double Zaval = Double.valueOf(st.nextToken());
        motNb++;
//        Double Zaval = Double.valueOf(st.nextToken());
        Double Zamont = Double.valueOf(st.nextToken());
        if (!debits.contains(debit)) {
          debits.add(debit);
        }
        if (!Zavals.contains(Zaval)) {
          Zavals.add(Zaval);
        }
        ZAmonts.put(debit.toString() + " " + Zaval.toString(), Zamont);
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      if (debits.size() * Zavals.size() != ZAmonts.size()) {
        throw new FichierMascaretException();
      }
      nouveauTableau = new double[debits.size() + 1][Zavals.size() + 1];
      nouveauTableau[0][0] = Double.NaN;
      for (int i = 0; i < debits.size(); i++) {
        Double debit = (Double) debits.get(i);
        nouveauTableau[i + 1][0] = debit.doubleValue();
        for (int j = 0; j < Zavals.size(); j++) {
          Double Zaval = (Double) Zavals.get(j);
          if (i == 0) {
            nouveauTableau[0][j + 1] = Zaval.doubleValue();
          }
          nouveauTableau[i + 1][j + 1]
                  = ((Double) ZAmonts.get(debit.toString() + " " + Zaval.toString()))
                  .doubleValue();
        }
      }
      System.err.println("OK");
      logMsg += "OK\n";
    } catch (FichierMascaretException e) {
      logMsg += getS("Le nombre de cote amont doit �tre �gale au produit") + " \n";
      logMsg += getS("du nombre de d�bits diff�rents avec le nombre de cotes avals diff�rentes") + "\n";
      my_perror(getS("Le nombre de cote amont doit �tre �gale au produit")
              + " \n" + getS("du nombre de d�bits diff�rents avec le nombre de cotes avals diff�rentes")
              + "\n");
    } catch (NumberFormatException e) {
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "' champ numero "
              + motNb);
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "' champ numero "
              + motNb
              + "\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    return nouveauTableau;
  }

  public static double[][] importLoi(File filename) throws IOException {
    double[][] nouveauTableau = null;
    Vector<Vector<Double>> lignesTableau = new Vector<Vector<Double>>();
    Vector<Double> ligneTableau;
//    String logMsg =
//            getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    int lineNb = 0;
    int motNb = 0;
    String line = null;
    String uniteTemps = null;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if ((line.isEmpty()) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        StringTokenizer st = new StringTokenizer(line);
        ligneTableau = new Vector<Double>();
        motNb = 0;
        while (st.hasMoreTokens()) { // \"
          String mot = st.nextToken();
          motNb++;
          if ((motNb == 1) && (uniteTemps == null)) {
            if (mot.equalsIgnoreCase("S") || mot.equalsIgnoreCase("\"S\"")) {
              uniteTemps = "S";
            } else if (mot.equalsIgnoreCase("M") || mot.equalsIgnoreCase("\"M\"")) {
              uniteTemps = "M";
            } else if (mot.equalsIgnoreCase("H") || mot.equalsIgnoreCase("\"H\"")) {
              uniteTemps = "H";
            } else if (mot.equalsIgnoreCase("J") || mot.equalsIgnoreCase("\"J\"")) {
              uniteTemps = "J";
            } else {
              ligneTableau.add(Double.valueOf(mot));
            }
          } else {
            ligneTableau.add(Double.valueOf(mot));
          }
        }
        if (ligneTableau.size() > 0) {
          lignesTableau.add(ligneTableau);
        }
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      System.err.println("OK");
//      logMsg += "OK\n";
    } catch (NumberFormatException e) {
      String s = getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "' champ numero "
              + motNb;
      my_perror(s);
      throw new IOException(s);
//    } catch (IOException e) {
//      my_perror(e);
    }
//    showMessage(logMsg);
    int nbLignes = lignesTableau.size();
    if (nbLignes > 0) {
      int nbColonnes = lignesTableau.get(0).size();
      nouveauTableau
              = new double[nbLignes][nbColonnes];
      for (int i = 0; i < nbLignes; i++) {
        for (int j = 0; j < nbColonnes; j++) {
          nouveauTableau[i][j] = lignesTableau.get(i).get(j).doubleValue();
          if ((uniteTemps != null) && (j == 0)) {
            if (uniteTemps.equals("M")) {
              nouveauTableau[i][j] = nouveauTableau[i][j] * 60;
            } // de minute en seconde
            else if (uniteTemps.equals("H")) {
              nouveauTableau[i][j] = nouveauTableau[i][j] * 3600;
            } // de heure en seconde
            else if (uniteTemps.equals("J")) {
              nouveauTableau[i][j] = nouveauTableau[i][j] * 3600 * 24;
            }
            // de heure en seconde
          }
        }
      }
    }
    return nouveauTableau;
  }

  public static MetierProfil[] importProfilsTXT(File filename, MetierBief bief) {
    Vector profils = new Vector();
    BufferedReader fic = null;
    String line = null;
    String logMsg = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    try {
      fic = new BufferedReader(new FileReader(filename));
      line = fic.readLine();
    } catch (IOException e) {
      my_perror(e);
    }
    int lineNb = 0;
    String state = "debut";
    MetierProfil nouveauProfil = null;
    MetierPoint2D[] points = null;
    int nbPoints = 0;
    MetierZoneFrottement zoneF = null;
    //debut lecture
    while (line != null) {
      line = line.trim();
      lineNb++;
      //System.err.println("line "+lineNb+", state="+state);
      if ((!line.isEmpty()) && (!line.startsWith("#"))) {
        if (state.equals("debut")) {
          System.err.println("Importation de " + line);
          logMsg += getS("Importation de ") + line + "\n";
          nouveauProfil = bief.creeProfil();
          // nom du profil
          nouveauProfil.nom(line);
          state = "abscisse";
        } else if (state.equals("abscisse")) {
          try {
            nouveauProfil.abscisse(Double.valueOf(line).doubleValue());
          } catch (NumberFormatException e) {
            my_perror(
                    getS("abscisse incorrecte") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")");
            logMsg += getS("abscisse incorrecte") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
          state = "nbPoints";
        } else if (state.equals("nbPoints")) {
          nbPoints = 0;
          int size = 0;
          try {
            size = Integer.valueOf(line).intValue();
          } catch (NumberFormatException e) {
            my_perror(
                    getS("nombre de points incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("nombre de points incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
          points = new MetierPoint2D[size];
          for (int i = 0; i < points.length; i++) {
            points[i] = new MetierPoint2D();
          }
          if (size > 0) {
            state = "points";
          } else {
            state = "rive"; // on saute les points (y'en a pas)
          }
        } else if (state.equals("points")) {
          StringTokenizer tk = new StringTokenizer(line);
          try {
            points[nbPoints].x = Double.valueOf(tk.nextToken()).doubleValue();
            points[nbPoints].y = Double.valueOf(tk.nextToken()).doubleValue();
          } catch (NoSuchElementException e) {
            my_perror(
                    getS("point mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n  "
                    + getS("(ou bien le nombre de points est inexact)"));
            logMsg += getS("point mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n  "
                    + getS("(ou bien le nombre de points est inexact)")
                    + "\n";
            break;
          } catch (NumberFormatException e) {
            my_perror(
                    getS("point incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n  "
                    + getS("(ou bien le nombre de points est inexact)"));
            logMsg += getS("point incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n  "
                    + getS("(ou bien le nombre de points est inexact)")
                    + "\n";

            break;
          }
          nbPoints++;
          if (nbPoints < points.length) {
            state = "points";
          } else {
            // rajouter un point extremite si necessaire pour
            // que les deux bords du profil soient a la meme cote
            if (points.length > 0) {
              if (points[0].y < points[points.length - 1].y) {
                MetierPoint2D[] tmpPoints = new MetierPoint2D[points.length + 1];
                tmpPoints[0] = new MetierPoint2D();
                tmpPoints[0].y = points[points.length - 1].y;
                tmpPoints[0].x = points[0].x;
                System.arraycopy(points, 0, tmpPoints, 1, points.length);
                points = tmpPoints;
                nbPoints++;
              } else if (points[0].y > points[points.length - 1].y) {
                MetierPoint2D[] tmpPoints = new MetierPoint2D[points.length + 1];
                System.arraycopy(points, 0, tmpPoints, 0, points.length);
                tmpPoints[points.length] = new MetierPoint2D();
                tmpPoints[points.length].y = points[0].y;
                tmpPoints[points.length].x = points[points.length - 1].x;
                points = tmpPoints;
                nbPoints++;
              }
            }
            nouveauProfil.points(points);
            state = "rive";
          }
        } else if (state.equals("rive")) {
          StringTokenizer tk = new StringTokenizer(line);
          try {
            nouveauProfil.indiceLitMinGa(
                    Integer.valueOf(tk.nextToken()).intValue());
            nouveauProfil.indiceLitMinDr(
                    Integer.valueOf(tk.nextToken()).intValue());
          } catch (NoSuchElementException e) {
            my_perror(
                    getS("point de limite de lit mineur mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("point de limite de lit mineur mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          } catch (NumberFormatException e) {
            my_perror(
                    getS("point de limite de lit mineur incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("point de limite de lit mineur incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
          state = "coefStrick";
        } else if (state.equals("coefStrick")) {
          StringTokenizer tk = new StringTokenizer(line);
          try {
            double strickMin = Double.valueOf(tk.nextToken()).doubleValue();
            double strickMaj = Double.valueOf(tk.nextToken()).doubleValue();
            if ((zoneF == null)
                    || (strickMaj != zoneF.coefMajeur())
                    || (strickMin != zoneF.coefMineur())) {
              zoneF = bief.creeZoneFrottement();
              zoneF.abscisseDebut(nouveauProfil.abscisse());
              zoneF.abscisseFin(nouveauProfil.abscisse());
              zoneF.biefRattache(bief);
              zoneF.coefMajeur(strickMaj);
              zoneF.coefMineur(strickMin);
            } else {
              zoneF.abscisseFin(nouveauProfil.abscisse());
            }
          } catch (NoSuchElementException e) {
            my_perror(
                    getS("coefficient de Strickler mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("coefficient de Strickler mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          } catch (NumberFormatException e) {
            my_perror(
                    getS("coefficient de Strickler incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("coefficient de Strickler incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
          state = "fin";
          continue; // ne pas lire la ligne suivante, sinon on la saute!...
        } else if (state.equals("fin")) {
          // on initialise les variables non d�finies dans le fichier
          if (points.length > 0) {
            nouveauProfil.indiceLitMajGa(0);
            nouveauProfil.indiceLitMajDr(points.length - 1);
          }
          profils.add(nouveauProfil);
          System.err.println("OK");
          logMsg += "OK\n";
          state = "debut";
        }
      } // fin if(!line.equals(""))
      try {
        line = fic.readLine();
      } catch (IOException e) {
        my_perror(e);
      }
    }
    try {
      fic.close();
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    MetierProfil[] res = new MetierProfil[profils.size()];
    for (int i = 0; i < res.length; i++) {
      res[i] = (MetierProfil) profils.get(i);
    }
    bief.normaliseProfils(res);
    return res;
  }

  public static Hydraulique1dProfilModel[] importProfilsTXT(File filename) {
    ArrayList profils = new ArrayList();
    BufferedReader fic = null;
    String line = null;
    String logMsg
            = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    try {
      fic = new BufferedReader(new FileReader(filename));
      line = fic.readLine();
    } catch (IOException e) {
      my_perror(e);
    }
    int lineNb = 0;
    String state = "debut";
    Hydraulique1dProfilModel nouveauProfil = null;
    MetierPoint2D[] points = null;
    int nbPoints = 0;
    //MetierZoneFrottement zoneF= null;
    //debut lecture
    while (line != null) {
      line = line.trim();
      lineNb++;
      //System.err.println("line "+lineNb+", state="+state);
      if ((!line.isEmpty()) && (!line.startsWith("#"))) {
        if (state.equals("debut")) {
          System.err.println("Importation de " + line);
          logMsg += getS("Importation de ") + line + "\n";
          nouveauProfil = new Hydraulique1dProfilModel();
          // nom du profil
          nouveauProfil.nom(line);
          state = "abscisse";
        } else if (state.equals("abscisse")) {
          try {
            nouveauProfil.setAbscisse(Double.valueOf(line).doubleValue());
          } catch (NumberFormatException e) {
            my_perror(
                    getS("abscisse incorrecte") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")");
            logMsg += getS("abscisse incorrecte") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
          state = "nbPoints";
        } else if (state.equals("nbPoints")) {
          nbPoints = 0;
          int size = 0;
          try {
            size = Integer.valueOf(line).intValue();
          } catch (NumberFormatException e) {
            my_perror(
                    getS("nombre de points incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("nombre de points incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
          points = new MetierPoint2D[size];
          for (int i = 0; i < points.length; i++) {
            points[i] = new MetierPoint2D();
          }
          if (size > 0) {
            state = "points";
          } else {
            state = "rive"; // on saute les points (y'en a pas)
          }
        } else if (state.equals("points")) {
          StringTokenizer tk = new StringTokenizer(line);
          try {
            points[nbPoints].x = Double.valueOf(tk.nextToken()).doubleValue();
            points[nbPoints].y = Double.valueOf(tk.nextToken()).doubleValue();
          } catch (NoSuchElementException e) {
            my_perror(
                    getS("point mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n  "
                    + getS("(ou bien le nombre de points est inexact)"));
            logMsg += getS("point mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n  "
                    + getS("(ou bien le nombre de points est inexact)")
                    + "\n";
            break;
          } catch (NumberFormatException e) {
            my_perror(
                    getS("point incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n  "
                    + getS("(ou bien le nombre de points est inexact)"));
            logMsg += getS("point incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n  "
                    + getS("(ou bien le nombre de points est inexact)")
                    + "\n";
            break;
          }
          nbPoints++;
          if (nbPoints < points.length) {
            state = "points";
          } else {
            // rajouter un point extremite si necessaire pour
            // que les deux bords du profil soient a la meme cote
            if (points.length > 0) {
              if (points[0].y < points[points.length - 1].y) {
                MetierPoint2D[] tmpPoints = new MetierPoint2D[points.length + 1];
                tmpPoints[0] = new MetierPoint2D();
                tmpPoints[0].y = points[points.length - 1].y;
                tmpPoints[0].x = points[0].x;
                System.arraycopy(points, 0, tmpPoints, 1, points.length);
                points = tmpPoints;
                nbPoints++;
              } else if (points[0].y > points[points.length - 1].y) {
                MetierPoint2D[] tmpPoints = new MetierPoint2D[points.length + 1];
                System.arraycopy(points, 0, tmpPoints, 0, points.length);
                tmpPoints[points.length] = new MetierPoint2D();
                tmpPoints[points.length].y = points[0].y;
                tmpPoints[points.length].x = points[points.length - 1].x;
                points = tmpPoints;
                nbPoints++;
              }
            }
            nouveauProfil.points(points);
            state = "rive";
          }
        } else if (state.equals("rive")) {
          try {
            StringTokenizer tk = new StringTokenizer(line);
            int riveGauche = Integer.valueOf(tk.nextToken()).intValue();
            if (riveGauche >= points.length) {
              throw new NumberFormatException(getS("Doit etre inf�rieur au nombre de point du profil"));
            }
            nouveauProfil.setIndiceLitMinGa(riveGauche);

            int riveDroite = Integer.valueOf(tk.nextToken()).intValue();
            if (riveDroite >= points.length) {
              throw new NumberFormatException(getS("Doit etre inf�rieur au nombre de point du profil"));
            }
            nouveauProfil.setIndiceLitMinDr(riveDroite);

          } catch (NoSuchElementException e) {
            my_perror(
                    getS("point de limite de lit mineur mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("point de limite de lit mineur mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          } catch (NumberFormatException e) {
            my_perror(
                    getS("point de limite de lit mineur incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("point de limite de lit mineur incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
          state = "coefStrick";
        } else if (state.equals("coefStrick")) {
          StringTokenizer tk = new StringTokenizer(line);
          try {
            /*double strickMin= */ Double.valueOf(tk.nextToken()).doubleValue();
            /*double strickMaj= */ Double.valueOf(tk.nextToken()).doubleValue();
            /*            if ((zoneF == null)
             || (strickMaj != zoneF.coefMajeur())
             || (strickMin != zoneF.coefMineur())) {
             zoneF= bief.creeZoneFrottement();
             zoneF.abscisseDebut(nouveauProfil.abscisse());
             zoneF.abscisseFin(nouveauProfil.abscisse());
             zoneF.biefRattache(bief);
             zoneF.coefMajeur(strickMaj);
             zoneF.coefMineur(strickMin);
             } else {
             zoneF.abscisseFin(nouveauProfil.abscisse());
             }*/
          } catch (NoSuchElementException e) {
            my_perror(
                    getS("coefficient de Strickler mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("coefficient de Strickler mal d�fini") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          } catch (NumberFormatException e) {
            my_perror(
                    getS("coefficient de Strickler incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("coefficient de Strickler incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
          state = "fin";
          continue; // ne pas lire la ligne suivante, sinon on la saute!...
        } else if (state.equals("fin")) {
          // on initialise les variables non d�finies dans le fichier
          if (points.length > 0) {
            nouveauProfil.setIndiceLitMajGa(0);
            nouveauProfil.setIndiceLitMajDr(points.length - 1);
          }
          profils.add(nouveauProfil);
          System.err.println("OK");
          logMsg += "OK\n";
          state = "debut";
        }
      } // fin if(!line.equals(""))
      try {
        line = fic.readLine();
      } catch (IOException e) {
        my_perror(e);
      }
    }
    try {
      fic.close();
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    return (Hydraulique1dProfilModel[]) profils.toArray(new Hydraulique1dProfilModel[profils.size()]);
  }

  public static MetierProfil[] importProfilsPRO_LIDO(File filename, MetierBief bief) {
    SParametresPRO pro = null;
    try {
      pro = DParametresLido.litParametresPRO(filename, 0);
    } catch (Exception e) {
      my_perror(e);
      return null;
    }
    System.err.println("Methode de conversion de profils non impl�ment�e");
    convertitProfils(pro, bief);

    return bief.profils();
  }

  public static void convertitProfils(SParametresPRO pro, MetierBief bief) {
    MetierProfil[] profils = new MetierProfil[pro.nbProfils];
    MetierZoneFrottement zoneF = null;
    for (int j = 0; j < pro.nbProfils; j++) {
      MetierProfil p = bief.creeProfil();
      remplitProfil(p, pro.profilsBief[j]);
      double strickMin = pro.profilsBief[j].coefStrickMajMin;
      double strickMaj = pro.profilsBief[j].coefStrickMajSto;
      if ((zoneF == null)
              || (strickMaj != zoneF.coefMajeur())
              || (strickMin != zoneF.coefMineur())) {
        zoneF = bief.creeZoneFrottement();
        zoneF.abscisseDebut(p.abscisse());
        zoneF.abscisseFin(p.abscisse());
        zoneF.biefRattache(bief);
        zoneF.coefMajeur(strickMaj);
        zoneF.coefMineur(strickMin);
      } else {
        zoneF.abscisseFin(p.abscisse());
      }
      profils[j] = p;
    }
  }

  private static void remplitProfil(MetierProfil p, SParametresBiefBlocPRO pPRO) {
    p.nom(pPRO.numProfil);
    p.abscisse(pPRO.abscisse);
    MetierPoint2D[] points = new MetierPoint2D[pPRO.nbPoints];
    boolean stockG = false;
    int indiceLitMinGa = 0,
            indiceLitMinDr = 0,
            indiceLitMajGa = 0,
            indiceLitMajDr = 0;
    for (int i = 0; i < pPRO.nbPoints; i++) {
      points[i] = new MetierPoint2D();
      points[i].x = pPRO.abs[i];
      points[i].y = pPRO.cotes[i];
      if (CGlobal.egale(pPRO.absMajMin[0], pPRO.abs[i])) {
        indiceLitMinGa = i;
      }
      if (CGlobal.egale(pPRO.absMajMin[1], pPRO.abs[i])) {
        indiceLitMinDr = i;
      }
      if (CGlobal.egale(pPRO.absMajSto[0], pPRO.abs[i]) && (!stockG)) {
        stockG = true;
        indiceLitMajGa = i;
      }
      if (CGlobal.egale(pPRO.absMajSto[1], pPRO.abs[i])) {
        indiceLitMajDr = i;
      }
    }
    p.points(points);
    p.indiceLitMajDr(indiceLitMajDr);
    p.indiceLitMinDr(indiceLitMinDr);
    p.indiceLitMinGa(indiceLitMinGa);
    p.indiceLitMajGa(indiceLitMajGa);
    // le planimetrage est fait dans le parsage du CAL
  }

  /**
   * @param fichier File
   * @return Hydraulique1dLigneZoneFrottementTableau[]
   */
  public static Hydraulique1dLigneZoneFrottementTableau[] importFrottementPRO_LIDO(File fichier) {
    String logMsg
            = getS("R�sultat de l'importation de ") + fichier.getPath() + ":\n\n";
    int lineNb = 0;
    String line = null;
    Hydraulique1dLigneZoneFrottementTableau[] tabRes = null;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(fichier));
      String etat = "debut";
      TDoubleArrayList listeAbscDebut = new TDoubleArrayList();
      TDoubleArrayList listeAbscFin = new TDoubleArrayList();
      TDoubleArrayList listeStrickMin = new TDoubleArrayList();
      TDoubleArrayList listeStrickMaj = new TDoubleArrayList();
      double abs = Double.NaN;
      double absPrec = Double.NaN;
      double strickMin = Double.NaN;
      double strickMaj = Double.NaN;
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if (line.isEmpty() || line.startsWith("#") || line.startsWith("-")) {
          continue; // ligne suivante
        }
        int iDX;
        if ((iDX = line.indexOf("DX==>'")) != -1) {
          absPrec = abs;
          abs = Double.parseDouble(line.substring(iDX + "DX==>'".length()).trim());
          if ("debut".equals(etat)) {
            absPrec = abs;
          }
          etat = "new profil";
        }
        int iStric;
        if ((iStric = line.indexOf("STRICKLER ==>'")) != -1) {
          if (line.lastIndexOf("LIT MINEUR") != -1) {
            strickMin = Double.parseDouble(line.substring(iStric + "STRICKLER ==>'".length()).trim());
          } else if (line.lastIndexOf("LIT MAJEUR") != -1) {
            strickMaj = Double.parseDouble(line.substring(iStric + "STRICKLER ==>'".length()).trim());
            etat = "STRICKLER MAJ";
          }
        }
        if ("STRICKLER MAJ".equals(etat)) {
          if (listeAbscDebut.isEmpty()) {
            listeStrickMin.add(strickMin);
            listeStrickMaj.add(strickMaj);
            listeAbscDebut.add(abs);
          } else {
            if ((strickMin != listeStrickMin.get(listeStrickMin.size() - 1))
                    || (strickMaj != listeStrickMaj.get(listeStrickMaj.size() - 1))) {
              listeStrickMin.add(strickMin);
              listeStrickMaj.add(strickMaj);
              listeAbscDebut.add(abs);
              listeAbscFin.add(absPrec);
            }
          }
        }
      } // fin while( (line=fic.readLine())!=null )
      listeAbscFin.add(abs);
      fic.close();
      int nbZone = listeAbscDebut.size();
      tabRes = new Hydraulique1dLigneZoneFrottementTableau[nbZone];
      for (int i = 0; i < nbZone; i++) {
        tabRes[i] = new Hydraulique1dLigneZoneFrottementTableau();
        tabRes[i].abscDebut(listeAbscDebut.get(i));
        tabRes[i].abscFin(listeAbscFin.get(i));
        tabRes[i].coefMin(listeStrickMin.get(i));
        tabRes[i].coefMaj(listeStrickMaj.get(i));
      }
      logMsg += "OK\n";
    } catch (NoSuchElementException e) {
      my_perror(
              getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (NumberFormatException e) {
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    return tabRes;
  }

  /**
   * @param filename File
   * @return Hydraulique1dProfilModel[]
   */
  public static Hydraulique1dProfilModel[] importProfilsPRO_LIDO(File filename) {
    SParametresPRO pro = null;
    try {
      pro = DParametresLido.litParametresPRO(filename, 0);
    } catch (Exception e) {
      my_perror(e);
      return null;
    }
    return convertitProfilsSParametresPRO_ProfilModel(pro);
  }

  public static Hydraulique1dProfilModel[] convertitProfilsSParametresPRO_ProfilModel(SParametresPRO pro) {
    Hydraulique1dProfilModel[] res = new Hydraulique1dProfilModel[pro.nbProfils];
    for (int i = 0; i < res.length; i++) {
      SParametresBiefBlocPRO pPro = pro.profilsBief[i];
      res[i] = new Hydraulique1dProfilModel();
      res[i].numero(i + 1);
      res[i].setAbscisse(pPro.abscisse);
      res[i].nom(pPro.numProfil);
      MetierPoint2D[] points = new MetierPoint2D[pPro.nbPoints];
      int indiceLitMinGa = -1,
              indiceLitMinDr = -1,
              indiceLitMajGa = -1,
              indiceLitMajDr = -1;
      for (int j = 0; j < pPro.nbPoints; j++) {
        points[j] = new MetierPoint2D();
        points[j].x = pPro.abs[j];
        points[j].y = pPro.cotes[j];
        if (CGlobal.egale(pPro.absMajMin[0], pPro.abs[j])) {
          if (indiceLitMinGa == -1) {
            indiceLitMinGa = j;
          } else {
            if (points[indiceLitMinGa].y < points[j].y) {
              indiceLitMinGa = j;
            }
          }
        }
        if (CGlobal.egale(pPro.absMajMin[1], pPro.abs[j])) {
          if (indiceLitMinDr == -1) {
            indiceLitMinDr = j;
          } else {
            if (points[indiceLitMinDr].y < points[j].y) {
              indiceLitMinDr = j;
            }
          }
        }
        if (CGlobal.egale(pPro.absMajSto[0], pPro.abs[j])) {
          if (indiceLitMajGa == -1) {
            indiceLitMajGa = j;
          } else {
            if (points[indiceLitMajGa].y < points[j].y) {
              indiceLitMajGa = j;
            }
          }
        }
        if (CGlobal.egale(pPro.absMajSto[1], pPro.abs[j])) {
          if (indiceLitMajDr == -1) {
            indiceLitMajDr = j;
          } else {
            if (points[indiceLitMajDr].y < points[j].y) {
              indiceLitMajDr = j;
            }
          }
        }
      }

      if (indiceLitMinGa == -1) {
        indiceLitMinGa = 0;
      }
      if (indiceLitMinDr == -1) {
        indiceLitMinDr = 0;
      }
      if (indiceLitMajGa == -1) {
        indiceLitMajGa = 0;
      }
      if (indiceLitMajDr == -1) {
        indiceLitMajDr = 0;
      }

      res[i].points(points);
      res[i].setIndiceLitMajDr(indiceLitMajDr);
      res[i].setIndiceLitMinDr(indiceLitMinDr);
      res[i].setIndiceLitMinGa(indiceLitMinGa);
      res[i].setIndiceLitMajGa(indiceLitMajGa);

    }
    return res;
  }

  public static Hydraulique1dLigneLigneDEauTableau[] importLigneEau_Txt(
          File fichier) {
    ListePtsLigneEau listePts = new ListePtsLigneEau();
    String logMsg
            = getS("R�sultat de l'importation de ") + fichier.getPath() + ":\n\n";
    int lineNb = 0;
    String line = null;
    Hydraulique1dLigneLigneDEauTableau[] tableau = null;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(fichier));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if ((line.isEmpty()) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        listePts.ajoutPt(new PtLigneEau(line));
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      logMsg += "OK\n";
      tableau = listePts.transformeTableauMetier();
    } catch (NoSuchElementException e) {
      my_perror(
              getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (NumberFormatException e) {
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    return tableau;
  }

  public static Hydraulique1dLigneConcentrationsInitialesTableau[] importConcentrationsInitiales_Opt(File fichier, String[] nomVarConcentrations) {
    try {
      MetierResultatsTemporelSpatial ires = ConvMasc_H1D.
              convertirResultatsTemporelSpatialMasToH1d(DResultatsMascaret.
                      litResultatsTemporelSpatialOpthyca(fichier));
      return Hydraulique1dLigneConcentrationsInitialesTableau.convertitResultatsTemporelSpatial_LigneConcentrationsInitialesTableau(ires,
              nomVarConcentrations);
    } catch (OutOfMemoryError ex) {
      showMessage("M�moire insuffisante");
      return null;
    } catch (FichierMascaretException ex) {
      showMessage(ex.getMessage());
      return null;
    }
  }

  public static Hydraulique1dLigneLigneDEauTableau[] importLigneEau_Opt(File fichier) {
    try {
      MetierResultatsTemporelSpatial ires = ConvMasc_H1D.
              convertirResultatsTemporelSpatialMasToH1d(DResultatsMascaret.
                      litResultatsTemporelSpatialOpthyca(fichier));
      return Hydraulique1dLigneLigneDEauTableau.convertitResultatsTemporelSpatial_LigneLigneDEauTableau(ires);
    } catch (OutOfMemoryError ex) {
      showMessage("M�moire insuffisante");
      return null;
    } catch (FichierMascaretException ex) {
      showMessage(ex.getMessage());
      return null;
    }
  }

  public static Hydraulique1dLigneLigneDEauTableau[] importLigneEau_Rub(File fichier) {
    try {
      MetierResultatsTemporelSpatial ires = ConvMasc_H1D.
              convertirResultatsTemporelSpatialMasToH1d(DResultatsMascaret.
                      litResultatsTemporelSpatialRubens(fichier));
      return Hydraulique1dLigneLigneDEauTableau.convertitResultatsTemporelSpatial_LigneLigneDEauTableau(ires);
    } catch (OutOfMemoryError ex) {
      return null;
    } catch (FichierMascaretException ex) {
      return null;
    }
  }

  public static void importProfilsPRO_MASCARET(File filename, MetierBief[] biefs) {
    Vector profils = new Vector();
    Vector ptsProfil = new Vector();
    String logMsg
            = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    int lineNb = 0;
    MetierProfil nouveauProfil = null;
    //int nbPoints=0; variable inutile
    String etat = "debut";
    String nomPremierBief = null;
    String line = null;
    int iProfil = 0;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if ((line.isEmpty()) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        StringTokenizer st = new StringTokenizer(line);
        String mot1 = st.nextToken();
        String mot2 = st.nextToken();
        if (etat.equals("debut")) {
          if (mot1.equalsIgnoreCase("profil")) {
            biefs[iProfil].profils(new MetierProfil[0]);
            nouveauProfil = mascaretEnteteProfil(biefs[iProfil], logMsg, line);
            ptsProfil = new Vector();
            nomPremierBief = mot2;
            etat = "entete";
          } else {
            my_perror(
                    getS("debut du fichier incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("debut du fichier incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
        } else if (etat.equals("entete")) {
          if (mot1.equalsIgnoreCase("profil")) {
            my_perror(getS("profil vide") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")");
            logMsg += getS("profil vide") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")\n";
            break;
          } /*else {*/

          boolean avecGeoref = mascaretPointProfil(logMsg, line, ptsProfil);
          //Si un seul point possede un g�oref alors avecGeoreferencement � true
          if (avecGeoref) {
            nouveauProfil.avecGeoreferencement(avecGeoref);
          }
          etat = "points";
          //}
        } else if (etat.equals("points")) {
          if (mot1.equalsIgnoreCase("profil")) {
            if (mot2.equals(nomPremierBief)) {
              mascaretInitIndiceLit(nouveauProfil, ptsProfil);
              profils.add(nouveauProfil);
              nouveauProfil = mascaretEnteteProfil(biefs[iProfil], logMsg, line);
              ptsProfil = new Vector();
              etat = "entete";
            } else {
              // changement de bief
              iProfil++;
              if (biefs.length <= iProfil) {
                my_perror(getS("Trop de biefs dans la g�om�trie") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")");
                logMsg += getS("Trop de biefs dans la g�om�trie") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")\n";
                break;
              }
              biefs[iProfil].profils(new MetierProfil[0]);
              nomPremierBief = mot2;
              mascaretInitIndiceLit(nouveauProfil, ptsProfil);
              profils.add(nouveauProfil);
              nouveauProfil = mascaretEnteteProfil(biefs[iProfil], logMsg, line);
              ptsProfil = new Vector();
              etat = "entete";
            }
          } else {
            mascaretPointProfil(logMsg, line, ptsProfil);
            //          etat="points";
          }
        } // fin test etat
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      mascaretInitIndiceLit(nouveauProfil, ptsProfil);
      profils.add(nouveauProfil);
      System.err.println("OK");
      logMsg += "OK\n";
    } catch (NoSuchElementException e) {
      my_perror(
              getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (NumberFormatException e) {
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
  }

  private static MetierProfil mascaretEnteteProfil(
          MetierBief bief,
          String logMsg,
          String line)
          throws NoSuchElementException, NumberFormatException {
    StringTokenizer st = new StringTokenizer(line);
    MetierProfil res = null;
    /*String motCle = */
    st.nextToken();
    /*String nomBief = */
    st.nextToken();
    String nomProfil = st.nextToken();
    double abscisse = Double.parseDouble(st.nextToken());
    res = bief.creeProfil();
    res.nom(nomProfil);
    res.abscisse(abscisse);
    String infoGeoRef = "";
    try {
      while (st.hasMoreTokens()) {  //boucle de lecture
        infoGeoRef = infoGeoRef + " " + st.nextToken();
      }
    } catch (NoSuchElementException e) {
    } finally {
      //System.err.println(" infoGeoRef"+infoGeoRef);
      res.infosGeoReferencement(infoGeoRef);
      return res;
    }

  }

  private static boolean mascaretPointProfil(
          String logMsg,
          String line,
          Vector ptsProfil)
          throws NoSuchElementException, NumberFormatException {
    MetierPoint2D pt2D;
    boolean avecGeoReferencement = false;

    StringTokenizer st = new StringTokenizer(line);
    double x = Double.parseDouble(st.nextToken());
    double y = Double.parseDouble(st.nextToken());
    String lit = st.nextToken();
    double cx = 0;
    double cy = 0;
    if (st.hasMoreTokens()) {
      try {
        cx = Double.parseDouble(st.nextToken());
        cy = Double.parseDouble(st.nextToken());
        avecGeoReferencement = true;
      } catch (NoSuchElementException e) {
        avecGeoReferencement = false;
      }
    }
    if (avecGeoReferencement) {
      pt2D = new MetierPoint2D(x, y, cx, cy);
    } else {
      pt2D = new MetierPoint2D(x, y);
    }

    if ((!lit.equals("B")) && (!lit.equals("T"))) {
      throw new NumberFormatException(lit);
    }

    Object[] pt = new Object[2];
    pt[0] = pt2D;
    pt[1] = lit;
    ptsProfil.add(pt);

    return avecGeoReferencement;
  }

  private static void mascaretInitIndiceLit(
          MetierProfil nouveauProfil,
          Vector ptsProfil) {
    int indiceLitMinGa = 0;
    int indiceLitMajGa = 0;
    int nbPts = ptsProfil.size();
    int indiceLitMinDr = nbPts - 1;
    int indiceLitMajDr = nbPts - 1;
    boolean bathyTrouve = false;
    boolean finBathyTrouve = false;
    MetierPoint2D[] pts = new MetierPoint2D[nbPts];
    for (int i = 0; i < nbPts; i++) {
      pts[i] = (MetierPoint2D) ((Object[]) ptsProfil.get(i))[0];
      String lit = (String) ((Object[]) ptsProfil.get(i))[1];
      if ((!bathyTrouve) && (lit.equals("B"))) {
        bathyTrouve = true;
        indiceLitMinGa = i;
      } else if ((!finBathyTrouve) && (bathyTrouve) && (lit.equals("T"))) {
        finBathyTrouve = true;
        indiceLitMinDr = i - 1;
      }
    }
    if (nouveauProfil != null) {
      nouveauProfil.points(pts);
      nouveauProfil.indiceLitMajGa(indiceLitMajGa);
      nouveauProfil.indiceLitMinGa(indiceLitMinGa);
      nouveauProfil.indiceLitMinDr(indiceLitMinDr);
      nouveauProfil.indiceLitMajDr(indiceLitMajDr);
    }
  }

  public static Hydraulique1dProfilModel[] importProfilsPRO_EXCEL(File filename) {
    ArrayList profils = new ArrayList();
    Workbook classeurExcel = null;
    String logMsg = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    try {
      classeurExcel = Workbook.getWorkbook(filename);
      Sheet[] feuilles = classeurExcel.getSheets();
      for (int i = 0; i < feuilles.length; i++) {
        Sheet feuille = feuilles[i];
        int nbColonne = feuille.getColumns();
        int j = 0;

        while (j < nbColonne) {
          if (getS("Intitul� du profil").equalsIgnoreCase(feuille.getCell(j, 0).getContents())) {
            final ImportExcelResult res = Hydraulique1dProfilModel.createFromFeuilleExcel(feuille, j);
            profils.add(res.model);
            j = 1 + res.lastColRead;
          } else {
            logMsg += getS("Format incorrecte de la cellule (") + j + ", 0) " + getS("de la feuille n�") + (i + 1) + ".\n";
            logMsg += getS("Elle doit contenir") + " \"" + getS("Intitul� du profil") + "\"\n";
            throw new Exception(logMsg);
          }
        }
      }
      logMsg += "OK\n";
    } catch (Exception ex) {
      my_perror(ex);
      logMsg = getS("Format du fichier Excel incorrecte:") + "\n"
              + ex.getLocalizedMessage() + "\n";
    }
    if (classeurExcel != null) {
      classeurExcel.close();
    }
    showMessage(logMsg);
    return (Hydraulique1dProfilModel[]) profils.toArray(new Hydraulique1dProfilModel[profils.size()]);
  }

  public static Hydraulique1dProfilModel[] importProfilsPRO_MASCARET(File filename) {
    ArrayList profils = new ArrayList();
    ArrayList ptsProfil = new ArrayList();
    String logMsg
            = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    int lineNb = 0;
    Hydraulique1dProfilModel nouveauProfil = null;
    //int nbPoints=0; variable inutile
    String etat = "debut";
    String nomPremierBief = null;
    String line = null;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if ((line.isEmpty()) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        StringTokenizer st = new StringTokenizer(line);
        String mot1 = st.nextToken();
        String mot2 = st.nextToken();
        if (etat.equals("debut")) {
          if (mot1.equalsIgnoreCase("profil")) {
            nouveauProfil = mascaretEnteteProfil(logMsg, line);
            ptsProfil = new ArrayList();
            nomPremierBief = mot2;
            etat = "entete";
          } else {
            my_perror(
                    getS("debut du fichier incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")");
            logMsg += getS("debut du fichier incorrect") + ": '"
                    + line
                    + "' (" + getS("ligne") + " "
                    + lineNb
                    + ")\n";
            break;
          }
        } else if (etat.equals("entete")) {
          if (mot1.equalsIgnoreCase(" ")) {
            my_perror(getS("profil vide") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")");
            logMsg += getS("profil vide") + ": '" + line + "' (" + getS("ligne") + " " + lineNb + ")\n";
            break;
          } /*else {*/

          boolean avecGeoref = mascaretPointProfil(logMsg, line, ptsProfil);
          //Si un seul point possede un g�oref alors avecGeoreferencement � true
          if (avecGeoref) {
            nouveauProfil.avecGeoreferencement(avecGeoref);
          }
          etat = "points";
          //}
        } else if (etat.equals("points")) {
          if (mot1.equalsIgnoreCase("profil")) {
            if (mot2.equals(nomPremierBief)) {
              mascaretInitIndiceLit(nouveauProfil, ptsProfil);
              profils.add(nouveauProfil);
              nouveauProfil = mascaretEnteteProfil(logMsg, line);
              ptsProfil = new ArrayList();
              etat = "entete";
            } else {
              my_perror(
                      getS("plusieurs biefs d�finis") + ": '"
                      + line
                      + "' (" + getS("ligne") + " "
                      + lineNb
                      + ")");
              logMsg += getS("plusieurs biefs d�finis") + ": '"
                      + line
                      + "' (" + getS("ligne") + " "
                      + lineNb
                      + ")\n";
              break;
            }
          } else {

            boolean avecGeoref = mascaretPointProfil(logMsg, line, ptsProfil);
            //Si un seul point possede un g�oref alors avecGeoreferencement � true
            if (avecGeoref) {
              nouveauProfil.avecGeoreferencement(avecGeoref);
            }
            //          etat="points";
          }
        } // fin test etat
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      mascaretInitIndiceLit(nouveauProfil, ptsProfil);
      profils.add(nouveauProfil);
      System.err.println("OK");
      logMsg += "OK\n";
    } catch (NoSuchElementException e) {
      my_perror(
              getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (NumberFormatException e) {
      e.printStackTrace();
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    return (Hydraulique1dProfilModel[]) profils.toArray(new Hydraulique1dProfilModel[profils.size()]);
  }

  private static Hydraulique1dProfilModel mascaretEnteteProfil(
          String logMsg,
          String line)
          throws NoSuchElementException, NumberFormatException {
    StringTokenizer st = new StringTokenizer(line);
    Hydraulique1dProfilModel res = null;
    /*String motCle = */
    st.nextToken();
    /*String nomBief = */
    st.nextToken();
    String nomProfil = st.nextToken();
    double abscisse = Double.parseDouble(st.nextToken());
    res = new Hydraulique1dProfilModel();
    res.nom(nomProfil);
    res.setAbscisse(abscisse);

    String infoGeoRef = "";
    try {
      while (st.hasMoreTokens()) {  //boucle de lecture
        infoGeoRef = infoGeoRef + " " + st.nextToken();
      }
    } catch (NoSuchElementException e) {
    } finally {
      //System.err.println(" infoGeoRef"+infoGeoRef);
      res.infoGeoreferencement(infoGeoRef);
      return res;
    }
  }

  private static boolean mascaretPointProfil(
          String logMsg,
          String line,
          ArrayList ptsProfil)
          throws NoSuchElementException, NumberFormatException {
    MetierPoint2D pt2D;
    boolean avecGeoReferencement = false;

    StringTokenizer st = new StringTokenizer(line);
    double x = Double.parseDouble(st.nextToken());
    double y = Double.parseDouble(st.nextToken());
    String lit = st.nextToken();
    double cx = 0;
    double cy = 0;
    if (st.hasMoreTokens()) {
      try {
        cx = Double.parseDouble(st.nextToken());
        cy = Double.parseDouble(st.nextToken());
        avecGeoReferencement = true;
      } catch (NoSuchElementException e) {
        avecGeoReferencement = false;
      }
    }
    if (avecGeoReferencement) {
      pt2D = new MetierPoint2D(x, y, cx, cy);
    } else {
      pt2D = new MetierPoint2D(x, y);
    }

    if ((!lit.equals("B")) && (!lit.equals("T"))) {
      throw new NumberFormatException(lit);
    }

    Object[] pt = new Object[2];
    pt[0] = pt2D;
    pt[1] = lit;
    ptsProfil.add(pt);

    return avecGeoReferencement;

  }

  private static void mascaretInitIndiceLit(
          Hydraulique1dProfilModel nouveauProfil,
          ArrayList ptsProfil) {
    int indiceLitMinGa = 0;
    int indiceLitMajGa = 0;
    int nbPts = ptsProfil.size();
    int indiceLitMinDr = nbPts - 1;
    int indiceLitMajDr = nbPts - 1;
    boolean bathyTrouve = false;
    boolean finBathyTrouve = false;
    MetierPoint2D[] pts = new MetierPoint2D[nbPts];
    for (int i = 0; i < nbPts; i++) {
      pts[i] = (MetierPoint2D) ((Object[]) ptsProfil.get(i))[0];
      String lit = (String) ((Object[]) ptsProfil.get(i))[1];
      if ((!bathyTrouve) && (lit.equals("B"))) {
        bathyTrouve = true;
        indiceLitMinGa = i;
      } else if ((!finBathyTrouve) && (bathyTrouve) && (lit.equals("T"))) {
        finBathyTrouve = true;
        indiceLitMinDr = i - 1;
      }
    }
    if (nouveauProfil != null) {
      nouveauProfil.points(pts);
      nouveauProfil.setIndiceLitMajGa(indiceLitMajGa);
      nouveauProfil.setIndiceLitMinGa(indiceLitMinGa);
      nouveauProfil.setIndiceLitMinDr(indiceLitMinDr);
      nouveauProfil.setIndiceLitMajDr(indiceLitMajDr);
    }
  }

  public static MetierProfil[] importProfilsPRO_RIVICAD(File filename, MetierBief bief) {
    SParametresPRO pro = null;
    try {
      pro = DParametresLido.importationLIDO1_1(filename);
    } catch (Exception e) {
      my_perror(e);
      return null;
    }
    if (pro == null) {
      return null;
    }
    System.err.println("Methode de conversion de profils non impl�ment�e");
    convertitProfils(pro, bief);
    return bief.profils();
  }

  /**
   * Import du fichier cas Mascaret.
   *
   * @param _bnoyau52 Lecture suivant le format 5.2
   * @param etude MetierEtude1d
   * @return True : Si l'import s'est correctement pass�.
   */
  public static final boolean importMascaret(File _fcas, File _fgeo, File _fdico, MetierEtude1d etude, boolean _bnoyau52) {

    SParametresCAS params = null;
    try {
      // Lecture des fichiers et transfert dans structure Mascaret
      params = DParametresMascaret.litParametresCAS(_fcas, _fdico, _bnoyau52);
    } catch (FichierMascaretException ex) {
      String origineFile = _fcas.getAbsolutePath();
      if (ex.getLigne().equals("DICO")) {
        origineFile = _fdico.getPath();
      }
      final String message = getS("Fichier '") + origineFile + "'\n" + getS("ligne") + ": " + ex.getNumeroLigne() + "\n" + ex.getMessage();
      showMessage(message);
      return false;
    }
    if (params == null) {
      return false;
    }
    return importMascaretCommon(_fgeo, params, _fcas, etude, _bnoyau52);
  }

  public static final boolean importXCasMascaret(File _fcas, File _fgeo, MetierEtude1d etude, boolean _bnoyau52) {
    CtuluIOResult<EdamoxXMLContent> litParametresXCAS = DParametresMascaret.litParametresXCAS(_fcas);
    if (litParametresXCAS.getAnalyze().containsErrorOrSevereError()) {
      showMessage(CtuluDefaultLogFormatter.asHtml(litParametresXCAS.getAnalyze()));
      return false;
    }
    SParametresCAS params = litParametresXCAS.getSource().getParametresCas();
    if (params == null) {
      return false;
    }
    return importMascaretCommon(_fgeo, params, _fcas, etude, _bnoyau52);
  }

  /**
   * @param filename File
   * @return Hydraulique1dProfilModel[]
   */
  public static Hydraulique1dProfilModel[] importProfilsPRO_RIVICAD(File filename) {
    SParametresPRO pro = null;
    try {
      pro = DParametresLido.importationLIDO1_1(filename);
    } catch (Exception e) {
      my_perror(e);
      return null;
    }
    if (pro == null) {
      return null;
    }
    return convertitProfilsSParametresPRO_ProfilModel(pro);
  }

  public static MetierMaillage importMaillage(
          File fichier,
          MetierParametresGeneraux paramGen,
          MetierReseau reseau) {
    String logMsg
            = getS("R�sultat de l'importation de ") + fichier.getPath() + ":\n\n";
    int lineNb = 0;
    MetierMaillage res = paramGen.creeMaillage();
    String etat = "Type Maillage";
    String line = null;
    int i = 0;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(fichier));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if (etat.equals("Type Maillage")) {
          int indice = line.lastIndexOf('\'');
          int typeMaillage = Integer.parseInt(line.substring(indice + 1).trim());
          switch (typeMaillage) {
            case 1:
              res.methode(EnumMetierMethodeMaillage.SECTIONS_SUR_PROFILS);
              return res;
            case 2:
              res.methode(EnumMetierMethodeMaillage.SECTIONS_PAR_SERIES);
              etat = "Entete Section Par Serie";
              break;
            case 3:
              res.methode(EnumMetierMethodeMaillage.SECTIONS_UTILISATEUR);
              etat = "Entete Section Par Section";
              break;
            case 4:
              res.methode(EnumMetierMethodeMaillage.SECTIONS_LIGNE_EAU_INITIALE);
              return res;
            case 5:
              res.methode(EnumMetierMethodeMaillage.SECTIONS_PAR_SERIES);
              etat = "Entete Section Par Serie aux Profils";
              break;
            default:
              return null;
          }
        } else if (etat.equals("Entete Section Par Serie")) {
          //int indice= line.lastIndexOf("'");
          //Variable inutile
          /*int nbZone = Integer.parseInt(line.substring(indice+1).trim());*/
          res.creeSectionsParSeries();
          MetierDefinitionSectionsParSeries sections
                  = (MetierDefinitionSectionsParSeries) res.sections();
          sections.surProfils(false);
          etat = "Section Par Serie";
          i = 0;
        } else if (etat.equals("Section Par Serie")) {
          StringTokenizer st = new StringTokenizer(line, "'");
          st.nextToken(); // label
          double abscDebZone = Double.parseDouble(st.nextToken().trim());
          st.nextToken(); // label
          double abscFinZone = Double.parseDouble(st.nextToken().trim());
          st.nextToken(); // label
          int nbSection = Integer.parseInt(st.nextToken().trim());
          MetierBief b = reseau.getBiefContenantAbscisse(abscDebZone);
          MetierDefinitionSectionsParSeries sections
                  = (MetierDefinitionSectionsParSeries) res.sections();
          sections.creeSerieALaFin(b);
          sections.unitaires()[i].zone().abscisseDebut(abscDebZone);
          sections.unitaires()[i].zone().abscisseFin(abscFinZone);
          sections.unitaires()[i].nbSections(nbSection);
          i++;
        } else if (etat.equals("Entete Section Par Section")) {
          int indice = line.lastIndexOf('\'');
          int nbSection = Integer.parseInt(line.substring(indice + 1).trim());
          res.creeSectionsParSections();
          MetierDefinitionSectionsParSections sections
                  = (MetierDefinitionSectionsParSections) res.sections();
          sections.unitaires(new MetierSite[nbSection]);
          etat = "Section Par Section";
          i = 0;
        } else if (etat.equals("Section Par Section")) {
          StringTokenizer st = new StringTokenizer(line);
          while (st.hasMoreTokens()) {
            double absc = Double.parseDouble(st.nextToken());
            MetierBief b = reseau.getBiefContenantAbscisse(absc);
            MetierDefinitionSectionsParSections sections
                    = (MetierDefinitionSectionsParSections) res.sections();
            sections.creeSectionALaFin(b);
            sections.unitaires()[i].abscisse(absc);
            sections.unitaires()[i].biefRattache(b);
            i++;
          }
        } else if (etat.equals("Entete Section Par Serie aux Profils")) {
          int indice = line.lastIndexOf('\'');
          int nbZone = Integer.parseInt(line.substring(indice + 1).trim());
          res.creeSectionsParSeries();
          MetierDefinitionSectionsParSeries sections
                  = (MetierDefinitionSectionsParSeries) res.sections();
          sections.surProfils(true);
          sections.unitaires(new MetierDefinitionSectionsParSeriesUnitaire[nbZone]);
          etat = "Section Par Serie aux Profils";
          i = 0;
        } else if (etat.equals("Section Par Serie aux Profils")) {
          StringTokenizer st = new StringTokenizer(line, "'");
          st.nextToken(); // label
          int numProfilDeb = Integer.parseInt(st.nextToken().trim());
          st.nextToken(); // label
          int numProfilFin = Integer.parseInt(st.nextToken().trim());
          st.nextToken(); // label
          double pas = Double.parseDouble(st.nextToken().trim());
          MetierBief b = reseau.getBiefContenantProfilNumero(numProfilDeb);
          MetierDefinitionSectionsParSeries sections
                  = (MetierDefinitionSectionsParSeries) res.sections();
          sections.creeSerieALaFin(b);
          sections.unitaires()[i].zone().abscisseDebut(
                  b.profils()[b.getIndiceProfilNumero(numProfilDeb)].abscisse());
          sections.unitaires()[i].zone().abscisseFin(
                  b.profils()[b.getIndiceProfilNumero(numProfilFin)].abscisse());
          sections.unitaires()[i].pas(pas);
          i++;
        } // fin test etat
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      System.err.println("OK");
      logMsg += "OK\n";
    } catch (NoSuchElementException e) {
      my_perror(
              getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("il manque un champ dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (NumberFormatException e) {
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'");
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + "'\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);
    return res;
  }

  public static List importListeLaisses(MetierReseau reseau) {
    File filename = chooseFile("lai");
    if (filename == null) {
      return null;
    }
    List listeLigne = new ArrayList();
    String logMsg
            = getS("R�sultat de l'importation de ") + filename.getPath() + ":\n\n";
    int lineNb = 0;
    int motNb = 0;
    int nbBief = reseau.biefs().length;
    String line = null;
    try {
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      //debut lecture
      while ((line = fic.readLine()) != null) {
        line = line.trim();
        lineNb++;
        if ((line.isEmpty()) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        StringTokenizer st = new StringTokenizer(line);
        Hydraulique1dLigneLaisseTableau ligneFichier = new Hydraulique1dLigneLaisseTableau();
        motNb = 0;
        Integer numBief = Integer.valueOf(st.nextToken());
        if ((numBief.intValue() < 1) || (numBief.intValue() > nbBief)) {
          logMsg += getS("Numero du bief introuvable :")
                  + numBief
                  + getS(" ligne n�")
                  + lineNb
                  + "\n";
          continue;
        }/* else*/

        ligneFichier.numBief(numBief); // n� bief
        motNb++;
        ligneFichier.absc(Double.valueOf(st.nextToken())); // abscisse
        motNb++;
        ligneFichier.cote(Double.valueOf(st.nextToken())); // cote
        motNb++;
        ligneFichier.nom(st.nextToken()); // nom
        motNb++;
        listeLigne.add(ligneFichier);
      } // fin while( (line=fic.readLine())!=null )
      fic.close();
      System.err.println("OK");
      logMsg += "OK\n";
    } catch (NumberFormatException e) {
      my_perror(
              getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + getS("' champ numero ")
              + motNb);
      logMsg += getS("format d'un champ incorrecte dans la ligne numero ")
              + lineNb
              + " '"
              + line
              + getS("' champ numero ")
              + motNb
              + "\n";
    } catch (IOException e) {
      my_perror(e);
    }
    showMessage(logMsg);

    return listeLigne;
  }

  public static byte[] importFichierSansFormat(File fichier) {
    String logMsg = getS("R�sultat de l'importation de ") + fichier.getPath() + ":\n\n";
    byte[] res = null;
    try {
      res = CtuluLibFile.litFichier(fichier);
      logMsg += "OK\n";
    } catch (FileNotFoundException ex) {
      logMsg += "\n " + getS("Fichier introuvable.");
    } catch (IOException ex) {
      logMsg += "\n " + getS("Erreur d'entr�e-sortie.");
    } catch (OutOfMemoryError ex) {
      logMsg += "\n M�moire insuffisante.";
    }
    showMessage(logMsg);
    return res;
  }

  public static byte[] importFichierReprise(File fichier, double[] conteneurTpsFinal) {
    String logMsg = getS("R�sultat de l'importation de ") + fichier.getPath() + ":\n\n";
    byte[] res = null;
    try {
      res = CtuluLibFile.litFichier(fichier);
      double tpsFinal = extraitTempsFinalFichierReprise(res);
      if (tpsFinal == Double.POSITIVE_INFINITY) {
        res = null;
        logMsg += "\n " + getS("Format incorrect, impossible de trouver le temps final.");
      } else {
        conteneurTpsFinal[0] = tpsFinal;
        logMsg += "OK\n";
      }

    } catch (FileNotFoundException ex) {
      logMsg += "\n " + getS("Fichier introuvable.");
    } catch (IOException ex) {
      logMsg += "\n " + getS("Erreur d'entr�e-sortie.");
    } catch (OutOfMemoryError ex) {
      logMsg += "\n M�moire insuffisante.";
    }
    showMessage(logMsg);
    return res;
  }

  private static double extraitTempsFinalFichierReprise(byte[] contenu) {
    String chaine = new String(contenu);
    int index = chaine.indexOf("FIN"); // le temps final se trouve apr�s FIN
    String chaineSansLigneInitiale = chaine.substring(index + 3);
    StringTokenizer st = new StringTokenizer(chaineSansLigneInitiale);
    try {
      return Double.parseDouble(st.nextToken());
    } catch (Throwable ex) {
      return Double.POSITIVE_INFINITY;
    }
  }

  public static File chooseFile(String extention, String description) {
    String[] extentions = new String[1];
    extentions[0] = extention;
    String[] descriptions = new String[1];
    descriptions[0] = description;
    return chooseFile(extentions, descriptions);
  }

  public static File chooseFile(String[] extentions, String[] descriptions) {
    CtuluFileChooser chooser = new CtuluFileChooser();
    chooser.setDialogType(JFileChooser.OPEN_DIALOG);

    for (int i = 0; (i < extentions.length) && (i < descriptions.length); i++) {
      chooser.addChoosableFileFilter(
              new BuFileFilter(extentions[i], descriptions[i]));
    }
    chooser.setCurrentDirectory(Hydraulique1dResource.lastImportDir);
    int returnVal = chooser.showOpenDialog(Hydraulique1dBaseApplication.FRAME);
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    int indexSlash = filename.lastIndexOf(java.io.File.separatorChar);
    File res = null;
    res = new File(filename);
    if (indexSlash > 0) {
      Hydraulique1dResource.lastImportDir
              = new java.io.File(filename.substring(0, indexSlash));
    } else {
      Hydraulique1dResource.lastImportDir = chooser.getCurrentDirectory();
    }
    return res;
  }

  public static File chooseFile(String[] extentions) {
    CtuluFileChooser chooser = new CtuluFileChooser();
    chooser.setDialogType(JFileChooser.OPEN_DIALOG);

    for (int i = 0; (i < extentions.length); i++) {
      chooser.addChoosableFileFilter(
              new BuFileFilter(extentions[i]));
    }
    chooser.setCurrentDirectory(Hydraulique1dResource.lastImportDir);
    int returnVal = chooser.showOpenDialog(Hydraulique1dBaseApplication.FRAME);
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    int indexSlash = filename.lastIndexOf(java.io.File.separatorChar);
    File res = null;
    res = new File(filename);
    if (indexSlash > 0) {
      Hydraulique1dResource.lastImportDir
              = new java.io.File(filename.substring(0, indexSlash));
    } else {
      Hydraulique1dResource.lastImportDir = chooser.getCurrentDirectory();
    }
    return res;
  }

  public static File chooseFile(String _ext) {
    final String ext = _ext;
    File res = null;
    CtuluFileChooser chooser = new CtuluFileChooser();
    chooser.setDialogType(JFileChooser.OPEN_DIALOG);
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
      @Override
      public boolean accept(java.io.File f) {
        return (ext == null)
                ? true
                : (f.getName().endsWith("." + ext) || f.isDirectory());
      }

      @Override
      public String getDescription() {
        return (ext == null)
                ? "*.*"
                : (getS("Fichiers ") + ext.toUpperCase() + " (*." + ext + ")");
      }
    });
    chooser.setCurrentDirectory(Hydraulique1dResource.lastImportDir);
    int returnVal = chooser.showOpenDialog(Hydraulique1dBaseApplication.FRAME);
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    int indexSlash = filename.lastIndexOf(java.io.File.separatorChar);
    res = new File(filename);
    if (indexSlash > 0) {
      Hydraulique1dResource.lastImportDir
              = new java.io.File(filename.substring(0, indexSlash));
    } else {
      Hydraulique1dResource.lastImportDir = chooser.getCurrentDirectory();
    }
    return res;
  }

  public static final MetierParamPhysTracer[] importParamsPhysiquesQE(File filename) {
    String line = "";
    try {
      int nbParams = 0;
      int numLigne = 0;
      MetierParamPhysTracer[] iparam = null;
      BufferedReader fic = new BufferedReader(new FileReader(filename));
      //debut lecture

      while ((line = fic.readLine()) != null) {
        line = line.trim();

        if ((line.isEmpty()) || (line.startsWith("#"))) {
          continue; // ligne suivante
        }
        numLigne++;
        StringTokenizer st = new StringTokenizer(line, ":");
        if (numLigne == 1) {
          nbParams = (int) Double.parseDouble(st.nextToken().trim());
          iparam = new MetierParamPhysTracer[nbParams];

        } else {
          MetierParamPhysTracer ligneFichier = new MetierParamPhysTracer();
          ligneFichier.valeurParamPhys(Double.parseDouble(st.nextToken().trim()));
          ligneFichier.nomParamPhys(st.nextToken().trim());
          iparam[numLigne - 2] = ligneFichier;
          if (nbParams == numLigne - 1) {
            break;
          }
        }
      }

      if (iparam == null) {
        return new MetierParamPhysTracer[0];
      }

      return iparam;
    } catch (OutOfMemoryError ex) {
      showMessage("M�moire insuffisante");
      return null;
    } catch (NumberFormatException e) {
      showMessage(getS("Format incorrecte dans la ligne :") + line);
      return null;
    } catch (IOException e) {
      showMessage(getS("Erreur d'Entr�e/Sortie dans la ligne :") + line);
      return null;
    }
  }

  private static void my_perror(String msg) {
    System.err.println(msg);
  }

  private static void my_perror(Exception e) {
    e.printStackTrace();
  }

  private static void showMessage(final String message) {
    new BuDialogMessage(
            (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
            ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
            .getInformationsSoftware(), message)
            .activate();
  }

  private static boolean importMascaretCommon(File _fgeo, SParametresCAS params, File _fcas, MetierEtude1d etude, boolean _bnoyau52) {
    String file = "";
    try {

      File geo = _fgeo;
      // Lecture des profils. Le fichier g�ometrie doit exister
      if (geo == null) {
        try {
          String fgeoPath = params.parametresGeoReseau.geometrie.fichier;
          geo = new File(fgeoPath);
          if (!geo.exists()) {
            geo = new File(_fcas.getParentFile(), fgeoPath);
          }
          if (!geo.exists()) {
            showMessage(Hydraulique1dResource.getS("Le fichier {0} n'existe pas", fgeoPath));
          }
        } catch (Exception e) {
          showMessage(getS("Impossible de lire le chemin du fichier de g�om�trie"));
          FuLog.warning(e);
          return false;
        }
      }
      file = geo.getPath();
      SParametresGEO pargeo = DParametresMascaret.litParametresGEO(geo);
      MetierBief[] biefsGeo = ConvMasc_H1D.convertirGEO(pargeo);

      // Lecture des lois hydrauliques sur fichiers
      StringBuffer sbLoi = new StringBuffer();
      SParametresLoi[] lois = params.parametresLoisHydrau.lois;
      for (int i = 0; i < lois.length; i++) {
        if (lois[i].donnees.modeEntree == 1) {
          try {
            double[][] vals = importLoi(CtuluLibFile.getAbsolutePath(_fcas.getParentFile(), lois[i].donnees.fichier));
            vals = CtuluLibArray.transpose(vals);

            lois[i].donnees.modeEntree = 2;
            lois[i].donnees.uniteTps = 1;
            lois[i].donnees.nbPoints = vals[0].length;

            switch (lois[i].type) {
              case 1: // 1->"HYDROGRAMME Q(T)"
                lois[i].donnees.tps = vals[0];
                lois[i].donnees.debit = vals[1];
                break;

              case 2: // 2->"LIMNIGRAMME Z(T)"
                lois[i].donnees.tps = vals[0];
                lois[i].donnees.cote = vals[1];
                break;

              case 3: // 3->"LIMNHYDROGRAMME Z,Q(T)"
                lois[i].donnees.tps = vals[0];
                lois[i].donnees.cote = vals[1];
                lois[i].donnees.debit = vals[2];
                break;

              case 4: // 4->"COURBE DE TARAGE Z=f(Q)"
                lois[i].donnees.debit = vals[0];
                lois[i].donnees.cote = vals[1];
                break;

              case 5: // 5->"COURBE DE TARAGE Q=f(Z)"
                lois[i].donnees.cote = vals[0];
                lois[i].donnees.debit = vals[1];
                break;

              case 6: // 6->"SEUIL Zam=f(Zav,Q)
                lois[i].donnees.debit = vals[0];
                lois[i].donnees.cote = vals[1];
                lois[i].donnees.cote2 = vals[2];
                break;

              case 7: // 7->"OUVERTURE VANNE Zinf,Zsup=f(T)
                lois[i].donnees.tps = vals[0];
                lois[i].donnees.cote = vals[1];
                lois[i].donnees.cote2 = vals[2];
                break;
            }
          } catch (IOException exc) {
            sbLoi.append(exc.getMessage()).append("<br>");
          }
        }
      }

      // Lecture des lois tracer sur fichiers
      SParametresLoiTracer[] loits = params.parametresTracer.parametresLoisTracer.loisTracer;
      for (int i = 0; i < loits.length; i++) {
        if (loits[i].modeEntree == 1) {
          try {
            double[][] vals = importLoi(CtuluLibFile.getAbsolutePath(_fcas.getParentFile(), loits[i].fichier));
            vals = CtuluLibArray.transpose(vals);

            loits[i].modeEntree = 2;
            loits[i].uniteTps = 1;
            loits[i].nbPoints = vals[0].length;
            loits[i].tps = vals[0];

            for (int t = 0; t < params.parametresTracer.nbTraceur; t++) {
              loits[i].concentrations[t].concentrations = vals[t + 1];
            }
          } catch (IOException exc) {
            sbLoi.append(exc.getMessage()).append("<br>");
          }
        }
      }

      // transfert Mascaret -> Hydraulique1d
      StringBuffer sb = ConvMasc_H1D.convertirCas(params, etude, biefsGeo);
      if (sb.length() != 0) {
        showMessage("<HTML><body><b>" + getS("RESULTAT DE L'IMPORT") + "</b>:<br><br>" + sbLoi.toString() + sb.toString());
      }
    } // Pb de lecture geo
    catch (FichierMascaretException ex) {
      final String message = getS("Fichier '") + file + "'\n" + getS("ligne") + ": " + ex.getNumeroLigne() + "\n" + ex.getMessage();
      showMessage(message);
      return false;
    }

    // Version du noyau
    etude.paramGeneraux().noyauV5P2(_bnoyau52);

    return true;
  }
}

class PtLigneEau {

  int numBief;
  double x;
  double z;
  double q;

  PtLigneEau(String ligne)
          throws NoSuchElementException, NumberFormatException {
    StringTokenizer st = new StringTokenizer(ligne);
    numBief = Integer.parseInt(st.nextToken());
    x = Double.parseDouble(st.nextToken());
    z = Double.parseDouble(st.nextToken());
    q = Double.parseDouble(st.nextToken());
  }

  PtLigneEau(int _numBief, double _x, double _z, double _q) {
    numBief = _numBief;
    x = _x;
    z = _z;
    q = _q;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof PtLigneEau) {
      PtLigneEau pt = (PtLigneEau) o;
      return ((pt.numBief == numBief) && (pt.x == x) && (pt.q == q) && (pt.z == z));
    } /*else*/

    return false;
  }

  Hydraulique1dLigneLigneDEauTableau transformeMetier() {
    Hydraulique1dLigneLigneDEauTableau pt = new Hydraulique1dLigneLigneDEauTableau();
    pt.iBief(numBief);
    pt.absc(x);
    pt.cote(z);
    pt.debit(q);
    return pt;
  }
}

class ListePtsLigneEau {

  private ArrayList points;

  ListePtsLigneEau() {
    points = new ArrayList();
  }

  void ajoutPt(PtLigneEau pt) {
    points.add(pt);
  }

  /**
   * @return le tableau d'objet metier corba.
   */
  Hydraulique1dLigneLigneDEauTableau[] transformeTableauMetier() {
    int taille = points.size();
    Hydraulique1dLigneLigneDEauTableau[] res = new Hydraulique1dLigneLigneDEauTableau[taille];
    for (int i = 0; i < res.length; i++) {
      res[i] = ((PtLigneEau) points.get(i)).transformeMetier();
    }
    return res;
  }
}

class PtGeoCasier {

  double x;
  double y;
  double z;
  String type = null;

  PtGeoCasier(double _x, double _y, double _z, String _type) {
    x = _x;
    y = _y;
    z = _z;
    type = _type;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof PtGeoCasier) {
      PtGeoCasier pt = (PtGeoCasier) o;
      return ((pt.x == x) && (pt.y == y) && (pt.z == z));
    } /*else*/

    return false;
  }
}

class GeoCasier {

  ArrayList points;
  ArrayList pointsFrontieres;
  String nom;
  private double pas = Double.NaN;

  GeoCasier(String _nom) {
    nom = _nom;
    points = new ArrayList();
    pointsFrontieres = new ArrayList();
  }

  void ajouteUnPoint(String ligne)
          throws NoSuchElementException, NumberFormatException {
    StringTokenizer st = new StringTokenizer(ligne);
    double x = Double.parseDouble(st.nextToken());
    double y = Double.parseDouble(st.nextToken());
    double z = Double.parseDouble(st.nextToken());
    String type = null;
    if (st.hasMoreTokens()) {
      type = st.nextToken();
    }
    PtGeoCasier pt = new PtGeoCasier(x, y, z, type);
    if ("F".equalsIgnoreCase(type)) {
      pointsFrontieres.add(pt);
    } else {
      points.add(pt);
    }
  }

  boolean isPlanimetrage() {
    boolean res = false;
    if (points.size() > 0) {
      PtGeoCasier pt0 = (PtGeoCasier) points.get(0);
      return (pt0.type == null);
    }
    return res;
  }

  boolean isSemiPoint() {
    boolean res = false;
    if (points.size() > 0) {
      PtGeoCasier pt0 = (PtGeoCasier) points.get(0);
      return (pt0.type != null);
    }
    return res;
  }

  String verifie() {
    String res = "";
    if (isPlanimetrage()) {
      PtGeoCasier pt0 = (PtGeoCasier) points.get(0);
      if (pt0.z != 0.) {
        res = Hydraulique1dImport.getS("Le volume du 1er point doit �tre nul") + "\n";
      }
      if (points.size() > 1) {
        PtGeoCasier pt1 = (PtGeoCasier) points.get(1);
        pas = pt1.x - pt0.x;
        Iterator ite = points.iterator();
        PtGeoCasier ptPrec = pt0;
        ite.next();
        while (ite.hasNext()) {
          PtGeoCasier pt = (PtGeoCasier) ite.next();
          double p = pt.x - ptPrec.x;
          if (p <= 0) {
            res += Hydraulique1dImport.getS("Les cotes doivent �tre rang�es dans l'ordre croissant") + "\n";
          }
          if (Math.abs(p - pas) > 0.000001) {
            res += Hydraulique1dImport.getS("L'�cart entre les cotes n'est pas constant") + "\n";
            continue;
          }
          ptPrec = pt;
        }
      } else {
        res += Hydraulique1dImport.getS("Le planim�trage ne contient q'un seul point") + "\n";
      }
    } else if (isSemiPoint()) {
      if (pointsFrontieres.size() < 4) {
        res = Hydraulique1dImport.getS("Le nombre de points fronti�re doit �tre au moins �gale � 4") + "\n";
      } else if (points.size() < 1) {
        res += Hydraulique1dImport.getS("Le nombre de points int�rieur doit �tre au moins �gale � 1") + "\n";
      }
      PtGeoCasier pt0 = (PtGeoCasier) pointsFrontieres.get(0);
      PtGeoCasier ptLast
              = (PtGeoCasier) pointsFrontieres.get(pointsFrontieres.size() - 1);
      if (!pt0.equals(ptLast)) {
        res += Hydraulique1dImport.getS("Le premier et de dernier point fronti�re doit �tre identique") + "\n";
      }
    } else {
      res = Hydraulique1dImport.getS("Format du Casier inconnu") + "\n";
    }
    return res;
  }

  MetierGeometrieCasier transformeMetier() {
    if (!verifie().isEmpty()) {
      return null;
    }
    if (isPlanimetrage()) {
      MetierPlanimetrageCasier geo
              = new MetierPlanimetrageCasier();
      geo.setPasPlanimetrage(pas);
      PtGeoCasier pt0 = (PtGeoCasier) points.get(0);
      geo.coteInitiale(pt0.x);
      MetierPoint2D[] pts = new MetierPoint2D[points.size()];
      for (int i = 0; i < pts.length; i++) {
        pts[i] = new MetierPoint2D();
        pts[i].x = ((PtGeoCasier) points.get(i)).y;
        pts[i].y = ((PtGeoCasier) points.get(i)).z;
      }
      geo.setPointsPlanimetrage(pts);
      return geo;
    } else if (isSemiPoint()) {
      MetierNuagePointsCasier geo
              = new MetierNuagePointsCasier();
      MetierPoint[] ptsF = new MetierPoint[pointsFrontieres.size()];
      for (int i = 0; i < ptsF.length; i++) {
        ptsF[i] = new MetierPoint();
        ptsF[i].x = ((PtGeoCasier) pointsFrontieres.get(i)).x;
        ptsF[i].y = ((PtGeoCasier) pointsFrontieres.get(i)).y;
        ptsF[i].z = ((PtGeoCasier) pointsFrontieres.get(i)).z;
      }
      geo.setPointsFrontiere(ptsF);
      MetierPoint[] pts = new MetierPoint[points.size()];
      for (int i = 0; i < pts.length; i++) {
        pts[i] = new MetierPoint();
        pts[i].x = ((PtGeoCasier) points.get(i)).x;
        pts[i].y = ((PtGeoCasier) points.get(i)).y;
        pts[i].z = ((PtGeoCasier) points.get(i)).z;
      }
      geo.setPointsInterieur(pts);
      return geo;
    } else {
      return null;
    }
  }
}

class ListeGeoCasier {

  private ArrayList casiers;

  ListeGeoCasier() {
    casiers = new ArrayList();
  }

  void ajoutCasier(GeoCasier casier) {
    casiers.add(casier);
  }

  GeoCasier getCasier(int i) {
    return (GeoCasier) casiers.get(i);
  }

  /**
   * @return null si pas de probl�me, sinon retourne le message d'erreur
   */
  String verifie() {
    Iterator ite = casiers.iterator();
    String res = "";
    while (ite.hasNext()) {
      GeoCasier geoCasier = (GeoCasier) ite.next();
      String mssgErreur = geoCasier.verifie();
      if (!mssgErreur.isEmpty()) {
        res += "\n" + Hydraulique1dImport.getS("Probl�me sur le Casier") + " '"
                + geoCasier.nom
                + "' :\n"
                + mssgErreur;
      }
    }
    if (res.isEmpty()) {
      return null;
    }
    //else
    return res;
  }

  /**
   * @return le tableau d'objet metier corba.
   */
  MetierGeometrieCasier[] transformeTableauMetier() {
    int taille = casiers.size();
    MetierGeometrieCasier[] res = new MetierGeometrieCasier[taille];
    for (int i = 0; i < res.length; i++) {
      res[i] = ((GeoCasier) casiers.get(i)).transformeMetier();
    }
    return res;
  }
}
