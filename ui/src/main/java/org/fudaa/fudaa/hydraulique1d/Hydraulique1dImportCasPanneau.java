/**
 *
 */
package org.fudaa.fudaa.hydraulique1d;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.UIManager;

import java.awt.Dimension;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;

import java.awt.Color;
import javax.swing.JRadioButton;

import org.fudaa.fudaa.commun.impl.FudaaGuiLib;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuGridLayout;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author bmarchan
 *
 */
public class Hydraulique1dImportCasPanneau extends JDialog {

  private JPanel pnAll = null;
  private JPanel pnCas = null;
  private JPanel pnMain = null;
  private JPanel pnButtons = null;
  private JButton btOk = null;
  private JButton btAnnuler = null;
  private JLabel lbCas = null;
  private JTextField tfCas = null;
  private JButton btCas = null;
  private JPanel pnDico = null;
  private JRadioButton rbDico61 = null;
//  private JPanel pnGeo = null;
  private JLabel lbGeo = null;
  private JRadioButton rdGeoFromCas = null;
  private JRadioButton rdGeoFromUser = null;
  private JTextField tfGeo = null;
  private JButton btGeo = null;
  private JRadioButton rbDico52 = null;
  /**
   * Le frame parent
   */
  private Frame parent_ = null;
  /**
   * Le retour
   */
  private int rep_ = CANCEL;
  /**
   * Valeur de retour pour annuler
   */
  public final static int CANCEL = 0;
  /**
   * Valeur de retour pour Valider
   */
  public final static int OK = 1;

  /**
   * Constructeur
   *
   */
  public Hydraulique1dImportCasPanneau(Frame _f) {
    super(_f);
    parent_ = _f;
    initialize();
    customize();
    this.setLocationRelativeTo(_f);
  }

  /**
   * This method initializes this
   *
   */
  private void initialize() {
    this.setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Import Mascaret (cas et geo)"));
    this.setContentPane(getPnAll());
    this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    this.setModal(true);
  }

  private void customize() {
    ButtonGroup bgDico = new ButtonGroup();
    bgDico.add(rbDico61);
    bgDico.add(rbDico52);
    pack();
  }

  /**
   * This method initializes pnAll
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnAll() {
    if (pnAll == null) {
      pnAll = new JPanel();
      pnAll.setLayout(new BorderLayout());
      pnAll.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      pnAll.add(getPnMain(), BorderLayout.CENTER);
      pnAll.add(getPnButtons(), BorderLayout.SOUTH);
    }
    return pnAll;
  }

  /**
   * This method initializes pnCas
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnCasAndGeo() {
    if (pnCas == null) {
      BuGridLayout lyCas = new BuGridLayout(3, 5, 5);
      pnCas = new JPanel();
      pnCas.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      pnCas.setLayout(lyCas);
      pnCas.add(lbCas);
      pnCas.add(getTfCas());
      pnCas.add(getBtCas());
      lbGeo = new JLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier GEO")+": ");
      rdGeoFromCas = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Utiliser celui d�fini dans le fichier cas"));
      pnCas.add(lbGeo);
      pnCas.add(rdGeoFromCas);
      pnCas.add(new JLabel());
      rdGeoFromUser = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Sp�cifier un fichier GEO"));
      pnCas.add(new JLabel());
      pnCas.add(rdGeoFromUser);
      pnCas.add(new JLabel());
      pnCas.add(new JLabel());
      pnCas.add(getTfGeo());
      pnCas.add(getBtGeo());
      rdGeoFromCas.setSelected(true);
      ButtonGroup bg = new ButtonGroup();
      bg.add(rdGeoFromCas);
      bg.add(rdGeoFromUser);
      ActionListener updateState = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          updateGeoState();
        }
      };
      rdGeoFromCas.addActionListener(updateState);
      rdGeoFromUser.addActionListener(updateState);
      updateGeoState();
    }
    return pnCas;
  }

  public void updateGeoState() {
    getTfGeo().setEnabled(rdGeoFromUser.isSelected());
    getBtGeo().setEnabled(rdGeoFromUser.isSelected());
  }

  /**
   * This method initializes pnMain
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnMain() {
    if (pnMain == null) {
      lbCas = new JLabel();
      lbCas.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier CAS") + ":");
      pnMain = new JPanel();
      pnMain.setLayout(new BoxLayout(getPnMain(), BoxLayout.Y_AXIS));
      pnMain.add(getPnCasAndGeo(), null);
      pnMain.add(getPnDico(), null);
    }
    return pnMain;
  }

  /**
   * This method initializes pnButtons
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnButtons() {
    if (pnButtons == null) {
      pnButtons = new JPanel();
      pnButtons.setLayout(new FlowLayout());
      pnButtons.add(getBtOk(), null);
      pnButtons.add(getBtAnnuler(), null);
    }
    return pnButtons;
  }

  /**
   * This method initializes btOk
   *
   * @return javax.swing.JButton
   */
  private JButton getBtOk() {
    if (btOk == null) {
      btOk = new JButton();
      btOk.setText("Ok");
      btOk.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          btOk_actionPerformed(e);
        }
      });
    }
    return btOk;
  }

  /**
   * This method initializes btAnnuler
   *
   * @return javax.swing.JButton
   */
  private JButton getBtAnnuler() {
    if (btAnnuler == null) {
      btAnnuler = new JButton();
      btAnnuler.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
      btAnnuler.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          btAnnuler_actionPerformed(e);
        }
      });
    }
    return btAnnuler;
  }

  /**
   * This method initializes tfCas
   *
   * @return javax.swing.JTextField
   */
  private JTextField getTfCas() {
    if (tfCas == null) {
      tfCas = new JTextField();
      tfCas.setPreferredSize(new Dimension(250, 20));
    }
    return tfCas;
  }

  /**
   * This method initializes btCas
   *
   * @return javax.swing.JButton
   */
  private JButton getBtCas() {
    if (btCas == null) {
      btCas = new JButton();
      btCas.setText("...");
      btCas.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          btCas_actionPerformed(e);
        }
      });
    }
    return btCas;
  }

  protected void btCas_actionPerformed(ActionEvent evt) {
    FileFilter filter = new BuFileFilter("cas", "Mascaret");
    File fichier = FudaaGuiLib.ouvrirFileChooser("Ouvrir", filter, parent_, false, null);
    if (fichier == null) {
      return;
    }

    tfCas.setText(fichier.getPath());
    tfGeo.setText(fichier.getPath().substring(0, fichier.getPath().lastIndexOf('.')) + ".geo");
  }

  protected void btGeo_actionPerformed(ActionEvent evt) {
    FileFilter filter = new BuFileFilter("geo", Hydraulique1dResource.HYDRAULIQUE1D.getString("Geometrie mascaret"));
    File fichier = FudaaGuiLib.ouvrirFileChooser(Hydraulique1dResource.HYDRAULIQUE1D.getString("Ouvrir"), filter, parent_, false, null);
    if (fichier == null) {
      return;
    }

    tfGeo.setText(fichier.getPath());
  }

  protected void btOk_actionPerformed(ActionEvent evt) {
    if (!isOk()) {
      return;
    }
    rep_ = OK;
    dispose();
  }

  protected void btAnnuler_actionPerformed(ActionEvent evt) {
    rep_ = CANCEL;
    dispose();
  }

  /**
   * This method initializes pnDico
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnDico() {
    if (pnDico == null) {
      GridLayout gridLayout = new GridLayout();
      gridLayout.setColumns(1);
      gridLayout.setRows(0);
      pnDico = new JPanel();
      pnDico.setLayout(gridLayout);
      pnDico.setBorder(BorderFactory.createTitledBorder(null, Hydraulique1dResource.HYDRAULIQUE1D.getString("Dictionnaire pour l'import"), TitledBorder.DEFAULT_JUSTIFICATION,
              TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), Color.black));
      pnDico.add(getRbDico61(), null);
      pnDico.add(getRbDico52(), null);
    }
    return pnDico;
  }

  /**
   * This method initializes rbDico61
   *
   * @return javax.swing.JRadioButton
   */
  private JRadioButton getRbDico61() {
    if (rbDico61 == null) {
      rbDico61 = new JRadioButton();
      rbDico61.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Le dictionnaire 8.0 install�"));
      rbDico61.setSelected(true);
    }
    return rbDico61;
  }

  /**
   * This method initializes tfGeo
   *
   * @return javax.swing.JTextField
   */
  private JTextField getTfGeo() {
    if (tfGeo == null) {
      tfGeo = new JTextField();
    }
    return tfGeo;
  }

  /**
   * This method initializes btGeo
   *
   * @return javax.swing.JButton
   */
  private JButton getBtGeo() {
    if (btGeo == null) {
      btGeo = new JButton();
      btGeo.setText("...");
      btGeo.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          btGeo_actionPerformed(e);
        }
      });
    }
    return btGeo;
  }

  /**
   * Retourne la reponse.
   */
  public int getReponse() {
    return rep_;
  }

  /**
   * Retourne le fichier Mascaret
   */
  public File getFichierCas() {
    return new File(tfCas.getText());
  }

  /**
   * @return le fichier de g�ometrie. null si le fichier doit �tre issu du fichier cas.
   */
  public File getFichierGeo() {
    if (rdGeoFromCas.isSelected()) {
      return null;
    }
    return new File(tfGeo.getText());
  }

  /**
   * Retourne true si fichier dictionnaire 6.1
   */
  public boolean isDico61() {
    return rbDico61.isSelected();
  }

  /**
   * Retourne true si fichier dictionnaire 5.2
   */
  public boolean isDico52() {
    return rbDico52.isSelected();
  }

  /**
   * Controle la validit� du dialogue.
   */
  private boolean isOk() {
    if (!new File(tfCas.getText()).canRead()) {
      JOptionPane.showMessageDialog(this, Hydraulique1dResource.HYDRAULIQUE1D.getString("Le fichier cas n'existe pas ou est illisible"),
              Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier invalide"), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (rdGeoFromUser.isSelected() && !new File(tfGeo.getText()).canRead()) {
      JOptionPane.showMessageDialog(this, Hydraulique1dResource.HYDRAULIQUE1D.getString("Le fichier de g�ometrie n'existe pas ou est illisible"),
              Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier invalide"), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  /**
   * This method initializes rbDico52
   *
   * @return javax.swing.JRadioButton
   */
  private JRadioButton getRbDico52() {
    if (rbDico52 == null) {
      rbDico52 = new JRadioButton();
      rbDico52.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Le dictionnaire 5.2 install�"));
    }
    return rbDico52;
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception _exc) {
    }
    new Hydraulique1dImportCasPanneau(null).setVisible(true);
    System.exit(0);
  }
}  //  @jve:decl-index=0:visual-constraint="122,72"
