/**
 *
 */
package org.fudaa.fudaa.hydraulique1d;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.UIManager;

import java.awt.Dimension;
import javax.swing.filechooser.FileFilter;

import javax.swing.JRadioButton;

import org.fudaa.fudaa.commun.impl.FudaaGuiLib;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuGridLayout;

import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author bmarchan
 *
 */
public class Hydraulique1dImportXCasPanneau extends JDialog {

  private JPanel pnAll = null;
  private JPanel pnCas = null;
  private JPanel pnMain = null;
  private JPanel pnButtons = null;
  private JButton btOk = null;
  private JButton btAnnuler = null;
  private JLabel lbCas = null;
  private JTextField tfCas = null;
  private JButton btCas = null;
  private JLabel lbGeo = null;
  private JTextField tfGeo = null;
  private JButton btGeo = null;
  private JRadioButton rdGeoFromCas = null;
  private JRadioButton rdGeoFromUser = null;
  /**
   * Le frame parent
   */
  private Frame parent_ = null;
  /**
   * Le retour
   */
  private int rep_ = CANCEL;
  /**
   * Valeur de retour pour annuler
   */
  public final static int CANCEL = 0;
  /**
   * Valeur de retour pour Valider
   */
  public final static int OK = 1;

  /**
   * Constructeur
   *
   */
  public Hydraulique1dImportXCasPanneau(Frame _f) {
    super(_f);
    parent_ = _f;
    initialize();
    customize();
    this.setLocationRelativeTo(_f);
  }

  /**
   * This method initializes this
   *
   */
  private void initialize() {
    this.setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Import Mascaret XML (.xcas)"));
    this.setContentPane(getPnAll());
    this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    this.setModal(true);
  }

  private void customize() {
    pack();
  }

  /**
   * This method initializes pnAll
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnAll() {
    if (pnAll == null) {
      pnAll = new JPanel();
      pnAll.setLayout(new BorderLayout());
      pnAll.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      pnAll.add(getPnMain(), BorderLayout.CENTER);
      pnAll.add(getPnButtons(), BorderLayout.SOUTH);
    }
    return pnAll;
  }

  /**
   * This method initializes pnCas
   *
   * @return javax.swing.JPanel
   */
  /**
   * This method initializes pnCas
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnCasAndGeo() {
    if (pnCas == null) {
      BuGridLayout lyCas = new BuGridLayout(3, 5, 5);
      pnCas = new JPanel();
      pnCas.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      pnCas.setLayout(lyCas);
      pnCas.add(lbCas);
      pnCas.add(getTfCas());
      pnCas.add(getBtCas());
      lbGeo = new JLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier GEO") + ": ");
      rdGeoFromCas = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Utiliser celui d�fini dans le fichier cas"));
      pnCas.add(lbGeo);
      pnCas.add(rdGeoFromCas);
      pnCas.add(new JLabel());
      rdGeoFromUser = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Sp�cifier un fichier GEO"));
      pnCas.add(new JLabel());
      pnCas.add(rdGeoFromUser);
      pnCas.add(new JLabel());
      pnCas.add(new JLabel());
      pnCas.add(getTfGeo());
      pnCas.add(getBtGeo());
      rdGeoFromCas.setSelected(true);
      ButtonGroup bg = new ButtonGroup();
      bg.add(rdGeoFromCas);
      bg.add(rdGeoFromUser);
      ActionListener updateState = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          updateGeoState();
        }
      };
      rdGeoFromCas.addActionListener(updateState);
      rdGeoFromUser.addActionListener(updateState);
      updateGeoState();
    }
    return pnCas;
  }

  public void updateGeoState() {
    getTfGeo().setEnabled(rdGeoFromUser.isSelected());
    getBtGeo().setEnabled(rdGeoFromUser.isSelected());
  }

  /**
   * This method initializes pnMain
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnMain() {
    if (pnMain == null) {
      lbCas = new JLabel();
      lbCas.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier") + " XCAS :");
      pnMain = new JPanel();
      pnMain.setLayout(new BoxLayout(getPnMain(), BoxLayout.Y_AXIS));
      pnMain.add(getPnCasAndGeo(), null);
    }
    return pnMain;
  }

  /**
   * This method initializes pnButtons
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPnButtons() {
    if (pnButtons == null) {
      pnButtons = new JPanel();
      pnButtons.setLayout(new FlowLayout());
      pnButtons.add(getBtOk(), null);
      pnButtons.add(getBtAnnuler(), null);
    }
    return pnButtons;
  }

  /**
   * This method initializes btOk
   *
   * @return javax.swing.JButton
   */
  private JButton getBtOk() {
    if (btOk == null) {
      btOk = new JButton();
      btOk.setText("Ok");
      btOk.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          btOk_actionPerformed(e);
        }
      });
    }
    return btOk;
  }

  /**
   * This method initializes btAnnuler
   *
   * @return javax.swing.JButton
   */
  private JButton getBtAnnuler() {
    if (btAnnuler == null) {
      btAnnuler = new JButton();
      btAnnuler.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
      btAnnuler.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          btAnnuler_actionPerformed(e);
        }
      });
    }
    return btAnnuler;
  }

  /**
   * This method initializes tfCas
   *
   * @return javax.swing.JTextField
   */
  private JTextField getTfCas() {
    if (tfCas == null) {
      tfCas = new JTextField();
      tfCas.setPreferredSize(new Dimension(250, 20));
    }
    return tfCas;
  }

  /**
   * This method initializes btCas
   *
   * @return javax.swing.JButton
   */
  private JButton getBtCas() {
    if (btCas == null) {
      btCas = new JButton();
      btCas.setText("...");
      btCas.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          btCas_actionPerformed(e);
        }
      });
    }
    return btCas;
  }

  protected void btCas_actionPerformed(ActionEvent evt) {
    FileFilter filter = new BuFileFilter("xcas", "Mascaret");
    File fichier = FudaaGuiLib.ouvrirFileChooser("Ouvrir", filter, parent_, false, null);
    if (fichier == null) {
      return;
    }

    tfCas.setText(fichier.getPath());
    tfGeo.setText(fichier.getPath().substring(0, fichier.getPath().lastIndexOf('.')) + ".geo");
  }

  protected void btGeo_actionPerformed(ActionEvent evt) {
    FileFilter filter = new BuFileFilter("geo", Hydraulique1dResource.HYDRAULIQUE1D.getString("Geometrie mascaret"));
    File fichier = FudaaGuiLib.ouvrirFileChooser(Hydraulique1dResource.HYDRAULIQUE1D.getString("Ouvrir"), filter, parent_, false, null);
    if (fichier == null) {
      return;
    }

    tfGeo.setText(fichier.getPath());
  }

  protected void btOk_actionPerformed(ActionEvent evt) {
    if (!isOk()) {
      return;
    }
    rep_ = OK;
    dispose();
  }

  protected void btAnnuler_actionPerformed(ActionEvent evt) {
    rep_ = CANCEL;
    dispose();
  }

  /**
   * This method initializes tfGeo
   *
   * @return javax.swing.JTextField
   */
  private JTextField getTfGeo() {
    if (tfGeo == null) {
      tfGeo = new JTextField();
    }
    return tfGeo;
  }

  /**
   * This method initializes btGeo
   *
   * @return javax.swing.JButton
   */
  private JButton getBtGeo() {
    if (btGeo == null) {
      btGeo = new JButton();
      btGeo.setText("...");
      btGeo.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          btGeo_actionPerformed(e);
        }
      });
    }
    return btGeo;
  }

  /**
   * Retourne la reponse.
   */
  public int getReponse() {
    return rep_;
  }

  /**
   * Retourne le fichier Mascaret
   */
  public File getFichierXCas() {
    return new File(tfCas.getText());
  }

  /**
   * Retourne le fichier de g�ometrie
   */
  public File getFichierGeo() {
    if (rdGeoFromCas.isSelected()) {
      return null;
    }
    return new File(tfGeo.getText());
  }

  /**
   * Controle la validit� du dialogue.
   */
  private boolean isOk() {
    if (!new File(tfCas.getText()).canRead()) {
      JOptionPane.showMessageDialog(this, Hydraulique1dResource.HYDRAULIQUE1D.getString("Le fichier cas n'existe pas ou est illisible"),
              Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier invalide"), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (!new File(tfGeo.getText()).canRead()) {
      JOptionPane.showMessageDialog(this, Hydraulique1dResource.HYDRAULIQUE1D.getString("Le fichier de g�ometrie n'existe pas ou est illisible"),
              Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier invalide"), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception _exc) {
    }
    new Hydraulique1dImportXCasPanneau(null).setVisible(true);
    System.exit(0);
  }
}  //  @jve:decl-index=0:visual-constraint="122,72"
