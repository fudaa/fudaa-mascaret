/*
 * @file         Hydraulique1dImportXMLPanneau.java
 * @creation     2001-10-02
 * @modification $Date: 2006-09-12 08:36:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
/**
 * Boite de dialogue affich� lors de l\u2019importation des fichiers XML.
 * menu Fichier/Importer/XML.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-12 08:36:42 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dImportXMLPanneau
  extends JDialog
  implements ActionListener, WindowListener {
  // Donnees membres publiques
  public final static int CANCEL= 0;
  public final static int OK= 1;
  // Donnees membres privees
  private int status_;
  private File fichierEtude_;
  private File fichierReseau_;
  private BuPanel pnFichierEtude_, pnFichierReseau_, pnOk_, pnGen_;
  private BuTextField tfFichierEtude_, tfFichierReseau_;
  private JButton b_ok_, b_cancel_, btFichierEtude_, btFichierReseau_;
  // Constructeurs
  public Hydraulique1dImportXMLPanneau(Frame _parent) {
    super(_parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Importation"), true);
    status_= CANCEL;
    //    File dir_=Hydraulique1dResource.lastImportDir;
    pnFichierEtude_= new BuPanel();
    pnFichierEtude_.setLayout(new BuHorizontalLayout());
    pnFichierEtude_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Importer l'�tude � partir de :")));
    tfFichierEtude_= BuTextField.createFileField();
    tfFichierEtude_.setColumns(8);
    pnFichierEtude_.add(tfFichierEtude_);
    btFichierEtude_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Parcourir"));
    btFichierEtude_.setActionCommand("PARCOURIR_ETUDE");
    btFichierEtude_.addActionListener(this);
    pnFichierEtude_.add(btFichierEtude_);
    pnFichierReseau_= new BuPanel();
    pnFichierReseau_.setLayout(new BuHorizontalLayout());
    pnFichierReseau_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Importer le r�seau � partir de :")));
    tfFichierReseau_= BuTextField.createFileField();
    tfFichierReseau_.setColumns(8);
    pnFichierReseau_.add(tfFichierReseau_);
    btFichierReseau_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Parcourir"));
    btFichierReseau_.setActionCommand("PARCOURIR_RESEAU");
    btFichierReseau_.addActionListener(this);
    pnFichierReseau_.add(btFichierReseau_);
    int n= 0;
    pnGen_= new BuPanel();
    pnGen_.setLayout(new BuVerticalLayout());
    pnGen_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnGen_.add(pnFichierEtude_, n++);
    pnGen_.add(pnFichierReseau_, n++);
    pnOk_= new BuPanel();
    b_ok_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    b_ok_.setActionCommand("VALIDER");
    b_ok_.addActionListener(this);
    b_cancel_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    b_cancel_.setActionCommand("ANNULER");
    b_cancel_.addActionListener(this);
    pnOk_.add(b_ok_);
    pnOk_.add(b_cancel_);
    Container content= getContentPane();
    content.setLayout(new BorderLayout());
    content.add(pnGen_, BorderLayout.NORTH);
    content.add(pnOk_, BorderLayout.SOUTH);
    pack();
    setResizable(false);
    setLocationRelativeTo(_parent);
  }
  // Actions
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String action= _evt.getActionCommand();
    //    Object src=_evt.getSource(); inutile
    if (action.equals("ANNULER")) {
      status_= CANCEL;
      dispose();
    } else if (action.equals("VALIDER")) {
      String txtEtude= tfFichierEtude_.getText();
      if ((txtEtude == null) || ("".equals(txtEtude))) {
        new BuDialogError(
          (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
          ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
            .getInformationsSoftware(),
          Hydraulique1dResource.HYDRAULIQUE1D.getString("Vous devez choisir un nom de fichier d'�tude !"))
          .activate();
        return;
      } /*else {*/
        int indSlash= txtEtude.lastIndexOf(File.separator);
        int ind= txtEtude.lastIndexOf('.');
        if ((ind > 0) && (ind > indSlash))
          txtEtude= txtEtude.substring(0, ind);
        Hydraulique1dResource.lastImportDir= new File(txtEtude);
      //}
      File parentEtude= fichierEtude_.getParentFile();
      if ((parentEtude != null) && (!parentEtude.canWrite())) {
        fichierEtude_= null;
        new BuDialogError(
          (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
          ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
            .getInformationsSoftware(),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Le r�pertoire ") + parentEtude + "\n"
          + Hydraulique1dResource.HYDRAULIQUE1D.getString("n'existe pas!"))
          .activate();
        return;
      }
      status_= OK;
      dispose();
    } else if (action.equals("PARCOURIR_ETUDE")) {
      fichierEtude_= Hydraulique1dImport.chooseFile("xml");
      tfFichierEtude_.setText(
        fichierEtude_ == null ? "" : fichierEtude_.getPath());
    } else if (action.equals("PARCOURIR_RESEAU")) {
      fichierReseau_= Hydraulique1dImport.chooseFile("xml");
      tfFichierReseau_.setText(
        fichierReseau_ == null ? "" : fichierReseau_.getPath());
    }
  }
  public File getFichierEtude() {
    return fichierEtude_;
  }
  public File getFichierReseau() {
    return fichierReseau_;
  }
  public int valeurRetour() {
    return status_;
  }
  // Window events
  @Override
  public void windowActivated(WindowEvent e) {}
  @Override
  public void windowClosed(WindowEvent e) {}
  @Override
  public void windowClosing(WindowEvent e) {
    dispose();
  }
  @Override
  public void windowDeactivated(WindowEvent e) {}
  @Override
  public void windowDeiconified(WindowEvent e) {}
  @Override
  public void windowIconified(WindowEvent e) {}
  @Override
  public void windowOpened(WindowEvent e) {}
}
