/*
 * @file         Hydraulique1dPoint.java
 * @creation     2004-08-17
 * @modification $Date: 2007-11-20 11:43:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
/**
 * Mod�le de donn�e utilis� pour repr�senter un point d\u2019un profil.
 * Ce mod�le est utilis� par la classe ��Hydraulique1dProfilModel��.
 * @version      $Revision: 1.5 $ $Date: 2007-11-20 11:43:04 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class Hydraulique1dPoint implements Cloneable {
  private Double X_=null;
  private Double Y_=null;
  //Coordonn�es de g�oreferencement
  private Double CX_=null;
  private Double CY_ = null;
  private boolean georefInitialized;


  /**
   * Constructeur par d�faut.
   */
  public Hydraulique1dPoint() {
    this(null, null);
  }

  /**
   * Constructeur d'un MetierPoint2D.
   * @param pt le point 2D.
   */
  public Hydraulique1dPoint(MetierPoint2D pt) {
    this(pt.x, pt.y,pt.cx,pt.cy);
  }
  /**
   * Constructeur d'un point � partir 2 r�els primitifs.
   * @param x premier �l�ment.
   * @param y deuxi�me �l�ment .
   */
  public Hydraulique1dPoint(double x, double y) {
	    this(doubleValue(x), doubleValue(y));
	  }
  public Hydraulique1dPoint(double x, double y, double cx, double cy) {
    this(doubleValue(x), doubleValue(y),doubleValue(cx), doubleValue(cy));
  }

  /**
   * Constructeur d'un point � partir 2 r�els non primitifs.
   * @param x premier �l�ment.
   * @param y deuxi�me �l�ment .
   */
  public Hydraulique1dPoint(Double x, Double y) {
    X_= x;
    Y_=y;
    CX_=new Double(0);
    CY_=new Double(0);
  }
  public Hydraulique1dPoint(Double x, Double y,Double cx, Double cy) {
	    X_= x;
	    Y_=y;
	    CX_=cx;
    CY_ = cy;
    georefInitialized = true;
	  }

  public MetierPoint2D toSPoint2D() {
    MetierPoint2D pt2D = new MetierPoint2D(x(),y(),Cx(),Cy());
    return pt2D;
  }
  /**
   * @return la valeur du premier �lement de coordon�e(double non primitif).
   */
  public Double X() {
    return X_;
  }
  /**
   * @return la valeur du premier �lement de coordon�e(double primitif).
   */
  public double x() {
    return doubleValue(X_);
  }
  /**
   * Initialise le premier �lement de coordon�e.
   * @param X La nouvelle valeur du premier �lement (peut �tre null).
   */
  public void X(Double X) {
    X_=X;
  }
  /**
   * Initialise le premier �lement de coordon�e.
   * @param x La nouvelle valeur du premier �lement.
   */
  public void x(double x) {
    X_= doubleValue(x);
  }
  /**
   * @return la valeur du deuxi�me �lement de coordon�e(double non primitif).
   */
  public Double Y() {
    return Y_;
  }
  /**
   * @return la valeur du deuxi�me �lement de coordon�e (double primitif).
   */
  public double y() {
    return doubleValue(Y_);
  }
  /**
   * Initialise le deuxi�me �lement de coordon�e.
   * @param Y La nouvelle valeur du deuxi�me �lement (peut �tre null).
   */
  public void Y(Double Y) {
    Y_=Y;
  }
  /**
   * Initialise le deuxi�me �lement de coordon�e.
   * @param y La nouvelle valeur du deuxi�me �lement.
   */
  public void y(double y) {
    Y_= doubleValue(y);
  }

  /**
   * @return la valeur du premier �lement de coordon�e(double non primitif).
   */
  public Double CX() {
    return CX_;
  }
  /**
   * @return la valeur du premier �lement de coordon�e(double primitif).
   */
  public double Cx() {
    return doubleValue(CX_);
  }
  /**
   * Initialise le premier �lement de coordon�e.
   * @param X La nouvelle valeur du premier �lement (peut �tre null).
   */
  public void CX(Double CX) {
    CX_ = CX;
    georefInitialized = true;
  }
  /**
   * Initialise le premier �lement de coordon�e.
   * @param x La nouvelle valeur du premier �lement.
   */
  public void Cx(double Cx) {
    CX_ = doubleValue(Cx);
    georefInitialized = true;
  }
  /**
   * @return la valeur du deuxi�me �lement de coordon�e (double non primitif).
   */
  public Double CY() {
    return CY_;
  }
  /**
   * @return la valeur du deuxi�me �lement de coordon�e(double primitif).
   */
  public double Cy() {
    return doubleValue(CY_);
  }
  /**
   * Initialise le deuxi�me �lement de coordon�e.
   * @param Y La nouvelle valeur du deuxi�me �lement (peut �tre null).
   */
  public void CY(Double CY) {
    CY_ = CY;
    georefInitialized = true;
  }
  /**
   * Initialise le deuxi�me �lement de coordon�e.
   * @param y La nouvelle valeur du deuxi�me �lement.
   */
  public void Cy(double Cy) {
    CY_ = doubleValue(Cy);
    georefInitialized = true;
  }

  public boolean isGeorefInitialized() {
    return georefInitialized;
  }


  /**
   * @return true si le point est nul (une de ses coordonnees n'est pas renseigne).
   */
  public boolean isNull() {
    return X()==null || Y()==null;
  }

  @Override
  public Hydraulique1dPoint clone() {
    Hydraulique1dPoint clone = new Hydraulique1dPoint(X_, Y_,CX_, CY_);
    return clone;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Hydraulique1dPoint) {
      Hydraulique1dPoint pt = (Hydraulique1dPoint) obj;
      return ((x() == pt.x())&&(y() == pt.y()));
    }
    else if (obj instanceof MetierPoint2D) {
      MetierPoint2D pt = (MetierPoint2D) obj;
      return ((x() == pt.x)&&(y() == pt.y));
    }
    else return false;
  }

  @Override
  public String toString() {
    return "("+X_+", "+Y_+")";
  }

  final static double doubleValue(Object x) {
    if (x == null) return Double.POSITIVE_INFINITY;
    return ((Double)x).doubleValue();
  }
  final static Double doubleValue(double x) {
    if (x == Double.POSITIVE_INFINITY) return null;
    return new Double(x);
  }
}
