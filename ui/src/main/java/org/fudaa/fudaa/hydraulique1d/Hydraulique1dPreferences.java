/*
 * @file         Hydraulique1dPreferences.java
 * @creation     1999-12-03
 * @modification $Date: 2006-09-12 08:36:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;
import com.memoire.bu.BuPreferences;
/**
 * Classe fille de ��BuPreferences��.
 * Elle cr�e un champ ��static�� ��HYDRAULIQUE1D�� de sa propre instance.
 * En pratique, elle ne sert quasiment a rien.
 * @version      $Revision: 1.6 $ $Date: 2006-09-12 08:36:42 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public class Hydraulique1dPreferences extends BuPreferences {
  public final static Hydraulique1dPreferences HYDRAULIQUE1D=
    new Hydraulique1dPreferences();
}
