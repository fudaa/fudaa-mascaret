/*
 * @file         Hydraulique1dProfilDataEvent.java
 * @creation     2004-08-17
 * @modification $Date: 2007-02-21 16:33:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import java.util.*;

/**
 * Un evenenement de changement de points de profils.
 */
public class Hydraulique1dProfilDataEvent extends EventObject {

  public static final int GLOBAL_MODIFICATION=0;
  public static final int MODIFICATION=1;
  public static final int INSERTION=2;
  public static final int SUPPRESSION=3;

  /** Les points modifi�s */
  private int[] isels_;

  /**
   * Construction de l'evenement
   * @param _src La source.
   * @param _type Le type d'�venement.
   * @param _src Les indices des points modifi�s.
   */
  public Hydraulique1dProfilDataEvent(Object _src, int[] _isels) {
    super(_src);
    isels_=_isels;
  }

  /**
   * Retourne les indices des points modifi�s.
   */
  public int[] getIndices() {
    return isels_;
  }
}
