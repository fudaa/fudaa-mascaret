/*
 * @file         Hydraulique1dProfilDataListener.java
 * @creation     2004-08-17
 * @modification $Date: 2007-02-21 16:33:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import java.util.*;

/**
 * Un listener pour les evenenement de modification de points de profils.
 */
public interface Hydraulique1dProfilDataListener extends EventListener {

  /**
   * La modification des points.
   */
  public void pointModified(Hydraulique1dProfilDataEvent _evt);

  /**
   * La suppression des points.
   */
  public void pointDeleted(Hydraulique1dProfilDataEvent _evt);

  /**
   * L'insersion de points.
   */
  public void pointInserted(Hydraulique1dProfilDataEvent _evt);
  }

