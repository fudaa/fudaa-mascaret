package org.fudaa.fudaa.hydraulique1d;

/**
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public interface Hydraulique1dProfilI {

  /**
   * @return L'indice de point avec le Z mini
   */
  public int getIndiceZMin();

  /**
   * @param _idxPoint L'indice du point pour lequel on cherche l'abscisse.
   * @return L'abscisse curviligne du point. Si l'indice n'existe pas, -1 est retourne.
   */
  public double getCurv(int _idxPoint);
  
  /**
   * @return L'abscisse curviligne de l'intersection du profil avec l'axe hydraulique.
   */
  public double getCurvAxeHydrauliqueOnProfil();

  /**
   * @param _idxPoint L'indice du point pour lequel on cherche l'ordonnee.
   * @return L'ordonnee du point.
   */
  public double getZ(int _idxPoint);

  /**
   * @return L'indice de rive droite sur la liste des points.
   */
  public int getIndiceLitMinDr();

  /**
   * @return L'indice de rive gauche sur la liste des points.
   */
  public int getIndiceLitMinGa();

  /**
   * @return L'indice de la limite stockage droit sur la liste des points.
   */
  public int getIndiceLitMajDr();

  /**
   * @return L'indice de limite stockage gauche sur la liste des points.
   */
  public int getIndiceLitMajGa();

  /**
   * @return L'indice du point min.
   */
  public int getIndiceXMin();

  /**
   * @return L'indice du point max.
   */
  public int getIndiceXMax();

}