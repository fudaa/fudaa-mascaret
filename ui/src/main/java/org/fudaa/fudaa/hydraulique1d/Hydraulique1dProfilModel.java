
/*
 * @file         Hydraulique1dProfilModel.java
 * @creation     2004-08-17
 * @modification $Date: 2007-11-20 11:43:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import jxl.Cell;
import jxl.CellType;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;

import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.dodico.hydraulique1d.util.CourbeLissable;
import org.fudaa.dodico.hydraulique1d.util.Hydraulique1DUtils;

/**
 * Mod�le de donn�e utilis� pour repr�senter un profil. Il peut �tre cr�er � partir d'un objet m�tier Corba ��DProfil��.
 *
 * C'est ce mod�le de donn�e qui est utilis� dans les �diteurs de bief et de profil.
 *
 * @version $Revision: 1.15 $ $Date: 2007-11-20 11:43:05 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe, Bertrand Marchand
 */
public final class Hydraulique1dProfilModel implements Cloneable, Hydraulique1dProfilI, CourbeLissable {

  public static final int LIGNE_GEO_REF = 6;
  /**
   * Insertion : Mask zone vide
   */
  public static final int MASK_ZONE_VIDE = 0;
  /**
   * Insertion : Mask zone stockage gauche
   */
  public static final int MASK_ZONE_STK_GA = 1 << 1;
  /**
   * Insertion : Mask zone lit majeur gauche
   */
  public static final int MASK_ZONE_MAJ_GA = 1 << 2;
  /**
   * Insertion : Mask zone lit mineur
   */
  public static final int MASK_ZONE_MIN = 1 << 3;
  /**
   * Insertion : Mask zone lit majeur droit
   */
  public static final int MASK_ZONE_MAJ_DR = 1 << 4;
  /**
   * Insertion : Mask zone stockage droit
   */
  public static final int MASK_ZONE_STK_DR = 1 << 5;

  private static int ord = 0;

  /**
   * Les points du profil courant. La liste peut contenir des points vides
   */
  private List<Hydraulique1dPoint> listePoints_ = new ArrayList<Hydraulique1dPoint>();
  /**
   * Indices des points s�lectionn�s
   */
  private Vector<Integer> viselects_ = new Vector<Integer>();
  /**
   * Clipboard contenant les points copies/coupes
   */
  private Vector<Hydraulique1dPoint> vclip_ = new Vector<Hydraulique1dPoint>();
  /**
   * Abscisse du profil
   */
  private double abscisse_;
  /**
   * Niveau d'eau profil
   */
  private double niveauEau_ = Double.NaN;
  /**
   * Niveau d'eau min
   */
  private double niveauEauMin_ = Double.NaN;
  /**
   * Niveau d'eau max
   */
  private double niveauEauMax_ = Double.NaN;
  /**
   * Niveau d'eau profil au pas de temps t=0
   */
  private double niveauEauInitial_ = Double.NaN;
  /**
   * Numero du profil
   */
  private int numero_;
  /**
   * Nom du profil
   */
  private String nom_;
  /**
   * Point limite de stockage gauche
   */
  private Hydraulique1dPoint ptLitMinGa_ = null;
  /**
   * Point limite de rive gauche
   */
  private Hydraulique1dPoint ptLitMajGa_ = null;
  /**
   * Point limite de rive droite
   */
  private Hydraulique1dPoint ptLitMinDr_ = null;
  /**
   * Point limite de stockage droit
   */
  private Hydraulique1dPoint ptLitMajDr_ = null;

  /**
   * Cache des indices selectionnes non nuls
   */
  private int[] selectedIndicesNonNuls_ = null;
  /**
   * Cache des indices non nuls.
   */
  private int[] indicesNonNuls_ = null;

  /**
   * Les auditeurs pour le data modifi�.
   */
  private HashSet<Hydraulique1dProfilDataListener> hlisteners_ = new HashSet<Hydraulique1dProfilDataListener>();
  /**
   * Les auditeurs pour la selection modifi�e.
   */
  private HashSet<Hydraulique1dProfilSelectionListener> hsellisteners_ = new HashSet<Hydraulique1dProfilSelectionListener>();

  private boolean avecGeoreferencement_;
  private String infoGeoreferencement_;
  private final ProfilGeoRefData geoRefData = new ProfilGeoRefData();

  // constructeurs
  public Hydraulique1dProfilModel() {
    numero_ = ord++;
    nom_ = Hydraulique1dResource.HYDRAULIQUE1D.getString("Profil ") + numero_;
    abscisse_ = 0.;
    ptLitMinDr_ = null;
    ptLitMinGa_ = null;
    ptLitMajDr_ = null;
    ptLitMajGa_ = null;
    avecGeoreferencement_ = false;
    infoGeoreferencement_ = "";
  }

  public Hydraulique1dProfilModel(MetierProfil iprofil) {
    numero_ = iprofil.numero();
    nom_ = iprofil.nom();
    abscisse_ = iprofil.abscisse();
    points(iprofil.points());
    setIndiceLitMajGa(iprofil.indiceLitMajGa());
    setIndiceLitMinGa(iprofil.indiceLitMinGa());
    setIndiceLitMinDr(iprofil.indiceLitMinDr());
    setIndiceLitMajDr(iprofil.indiceLitMajDr());
    avecGeoreferencement_ = iprofil.avecGeoreferencement();
    infoGeoreferencement_ = iprofil.infosGeoReferencement();
    geoRefData.initWith(this);
  }

  @Override
  public Hydraulique1dProfilModel clone() {
    Hydraulique1dProfilModel clone = new Hydraulique1dProfilModel();
    clone.numero(ord++);
    clone.nom(nom_);
    clone.setAbscisse(abscisse_);
    int nbPoint = listePoints_.size();
    clone.listePoints_ = new ArrayList<Hydraulique1dPoint>(nbPoint);
    for (int i = 0; i < nbPoint; i++) {
      clone.listePoints_.add(listePoints_.get(i).clone());
    }
    clone.setIndiceLitMajGa(getIndiceLitMajGa());
    clone.setIndiceLitMinGa(getIndiceLitMinGa());
    clone.setIndiceLitMinDr(getIndiceLitMinDr());
    clone.setIndiceLitMajDr(getIndiceLitMajDr());
    clone.avecGeoreferencement(avecGeoreferencement_);
    clone.infoGeoreferencement(infoGeoreferencement_);
    return clone;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o instanceof Hydraulique1dProfilModel) {
      Hydraulique1dProfilModel p = (Hydraulique1dProfilModel) o;
      if (abscisse_ != p.getAbscisse()) {
        return false;
      }
      if (!avecGeoreferencement_ == p.avecGeoreferencement()) {
        return false;
      }
      if (!infoGeoreferencement_.equals(p.infoGeoreferencement())) {
        return false;
      }
      if (getIndiceLitMajDr() != p.getIndiceLitMajDr()) {
        return false;
      }
      if (getIndiceLitMajGa() != p.getIndiceLitMajGa()) {
        return false;
      }
      if (getIndiceLitMinDr() != p.getIndiceLitMinDr()) {
        return false;
      }
      if (getIndiceLitMinGa() != p.getIndiceLitMinGa()) {
        return false;
      }
      if (!nom_.equals(p.nom())) {
        return false;
      }
      return egale(p.listePoints_, listePoints_);
    } else if (o instanceof MetierProfil) {
      MetierProfil p = (MetierProfil) o;
      if (abscisse_ != p.abscisse()) {
        return false;
      }
      if (!avecGeoreferencement_ == p.avecGeoreferencement()) {
        return false;
      }
      if (!infoGeoreferencement_.equals(p.infosGeoReferencement())) {
        return false;
      }
      if (getIndiceLitMajDr() != p.indiceLitMajDr()) {
        return false;
      }
      if (getIndiceLitMajGa() != p.indiceLitMajGa()) {
        return false;
      }
      if (getIndiceLitMinDr() != p.indiceLitMinDr()) {
        return false;
      }
      if (getIndiceLitMinGa() != p.indiceLitMinGa()) {
        return false;
      }
      if (!nom_.equals(p.nom())) {
        return false;
      }
      return egale(p.points(), listePoints_);
    } else {
      return false;
    }
  }

  public final static void miseAZeroProfil(Hydraulique1dProfilModel[] profils) {
    if (profils == null) {
      return;
    }
    for (int i = 0; i < profils.length; i++) {
      profils[i].setAbscisse(0.);
    }
  }

  public final static void decalageAbscisseProfils(Hydraulique1dProfilModel[] profils, double offset) {
    if (profils == null) {
      return;
    }
    for (int i = 0; i < profils.length; i++) {
      profils[i].setAbscisse(profils[i].getAbscisse() + offset);
    }
  }

  public final static void initCompteurOrdinal(int compteurOrd) {
    ord = compteurOrd;
  }

  // attributs
  public int numero() {
    return numero_;
  }

  public void numero(int s) {
    if (numero_ == s) {
      return;
    }
    numero_ = s;
  }

  public boolean avecGeoreferencement() {
    return avecGeoreferencement_;
  }

  public void avecGeoreferencement(boolean b) {
    if (avecGeoreferencement_ == b) {
      return;
    }
    avecGeoreferencement_ = b;
  }

  public String infoGeoreferencement() {
    return infoGeoreferencement_;
  }

  public void infoGeoreferencement(String s) {
    infoGeoreferencement_ = s;
  }

  public String nom() {
    return nom_;
  }

  public void nom(String s) {
    nom_ = s;
  }

  public double getAbscisse() {
    return abscisse_;
  }

  public void setAbscisse(double s) {
    abscisse_ = s;
  }

  public double getNiveauEau() {
    return niveauEau_;
  }

  public void setNiveauEau(double niveauEau) {
    niveauEau_ = niveauEau;
  }

  public double getNiveauEauMin() {
    return niveauEauMin_;
  }

  public void setNiveauEauMin(double niveauEauMin) {
    niveauEauMin_ = niveauEauMin;
  }

  public double getNiveauEauMax() {
    return niveauEauMax_;
  }

  public void setNiveauEauMax(double niveauEauMax) {
    niveauEauMax_ = niveauEauMax;
  }

  /**
   * Retourne la liste des points, contenant les nuls.
   *
   * @return La liste.
   */
  public List<Hydraulique1dPoint> getListePoints() {
    return listePoints_;
  }

  @Override
  public int getIndiceXMax() {
    int[] idxNonNull = getIndicesNonNuls();

    if (idxNonNull.length == 0) {
      return -1;
    }
    return idxNonNull[idxNonNull.length - 1];
  }

  @Override
  public int getIndiceXMin() {
    int[] idxNonNull = getIndicesNonNuls();

    if (idxNonNull.length == 0) {
      return -1;
    }
    return idxNonNull[0];
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilI#getIndiceZMin()
   */
  @Override
  public int getIndiceZMin() {
    int idx = -1;
    double zmin = Double.POSITIVE_INFINITY;

    int[] idxNonNull = getIndicesNonNuls();
    for (int i = 0; i < idxNonNull.length; i++) {
      if (listePoints_.get(idxNonNull[i]).y() < zmin) {
        zmin = listePoints_.get(idxNonNull[i]).y();
        idx = idxNonNull[i];
      }
    }

    return idx;
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilI#getCurv(int)
   */
  @Override
  public double getCurv(int _idxPoint) {
    if (_idxPoint == -1) {
      return -1;
    }

    return listePoints_.get(_idxPoint).x();
  }

  /**
   * Non utilis�
   */
  @Override
  public double getCurvAxeHydrauliqueOnProfil() {
    return -1;
  }

  @Override
  public double getZ(int _idxPoint) {
    if (_idxPoint == -1) {
      return -1;
    }

    return listePoints_.get(_idxPoint).y();
  }

  /**
   * @param _ind L'indice du point.
   * @return le point d'indice donn�e. Null si hors bornes.
   */
  public Hydraulique1dPoint getPoint(int _ind) {
    if (_ind < 0 || _ind >= listePoints_.size()) {
      return null;
    }
    return (Hydraulique1dPoint) listePoints_.get(_ind);
  }

  /**
   * Retourne les indices des points non nuls sur la liste des points.
   */
  public int[] getIndicesNonNuls() {
    if (indicesNonNuls_ == null) {
      int nb = 0;
      int[] rtmp = new int[listePoints_.size()];
      for (int i = 0; i < listePoints_.size(); i++) {
        if (!((Hydraulique1dPoint) listePoints_.get(i)).isNull()) {
          rtmp[nb++] = i;
        }
      }

      indicesNonNuls_ = new int[nb];
      System.arraycopy(rtmp, 0, indicesNonNuls_, 0, nb);
    }

    return indicesNonNuls_;
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilI#getIndiceLitMinDr()
   */
  @Override
  public int getIndiceLitMinDr() {
    // Retourne le point le plus a droite qui est non nul.
    if (ptLitMinDr_ == null) {
      int[] inds = getIndicesNonNuls();
      if (inds.length > 0) {
        int indicePt = inds[inds.length - 1];
        ptLitMinDr_ = (Hydraulique1dPoint) listePoints_.get(indicePt);
        return indicePt;
      }
      return -1;
    }
    int indicePt = listePoints_.indexOf(ptLitMinDr_);
    if (indicePt == -1) {
      indicePt = listePoints_.size() - 1;
      ptLitMinDr_ = (Hydraulique1dPoint) listePoints_.get(indicePt);
    }
    return indicePt;
  }

  public void setIndiceLitMinDr(int s) {
    int nbPoint = listePoints_.size();
    if (nbPoint == 0) {
      ptLitMinDr_ = null;
      ptLitMajDr_ = null;
      ptLitMinGa_ = null;
      ptLitMinDr_ = null;
    } else if (nbPoint == 1) {
      Hydraulique1dPoint pt1 = (Hydraulique1dPoint) listePoints_.get(0);
      ptLitMinDr_ = pt1;
      ptLitMajDr_ = pt1;
      ptLitMinGa_ = pt1;
      ptLitMinDr_ = pt1;
    } else {
      s = Math.min(nbPoint - 1, s);
      s = Math.max(0, s);
      ptLitMinDr_ = (Hydraulique1dPoint) listePoints_.get(s);
      if (s < getIndiceLitMinGa()) {
        ptLitMajGa_ = ptLitMinDr_;
      }
      if (s < getIndiceLitMajGa()) {
        ptLitMajGa_ = ptLitMinDr_;
      }
      if (s > getIndiceLitMajDr()) {
        ptLitMajDr_ = ptLitMinDr_;
      }
    }
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilI#getIndiceLitMinGa()
   */
  @Override
  public int getIndiceLitMinGa() {
    // Retourne le point le plus a gauche qui est non nul.
    if (ptLitMinGa_ == null) {
      int[] inds = getIndicesNonNuls();
      if (inds.length > 0) {
        int indicePt = inds[0];
        ptLitMinGa_ = (Hydraulique1dPoint) listePoints_.get(indicePt);
        return indicePt;
      }
      return -1;
    }
    int indicePt = listePoints_.indexOf(ptLitMinGa_);
    if (indicePt == -1) {
      indicePt = 0;
      ptLitMinGa_ = (Hydraulique1dPoint) listePoints_.get(indicePt);
    }
    return indicePt;
  }

  public void setIndiceLitMinGa(int s) {
    int nbPoint = listePoints_.size();
    if (nbPoint == 0) {
      ptLitMinDr_ = null;
      ptLitMajDr_ = null;
      ptLitMinGa_ = null;
      ptLitMinDr_ = null;
    } else if (nbPoint == 1) {
      Hydraulique1dPoint pt1 = (Hydraulique1dPoint) listePoints_.get(0);
      ptLitMinDr_ = pt1;
      ptLitMajDr_ = pt1;
      ptLitMinGa_ = pt1;
      ptLitMinDr_ = pt1;
    } else {
      s = Math.min(nbPoint - 1, s);
      s = Math.max(0, s);
      ptLitMinGa_ = (Hydraulique1dPoint) listePoints_.get(s);
      if (s > getIndiceLitMinDr()) {
        ptLitMinDr_ = ptLitMinGa_;
      }
      if (s > getIndiceLitMajDr()) {
        ptLitMajDr_ = ptLitMinGa_;
      }
      if (s < getIndiceLitMajGa()) {
        ptLitMajGa_ = ptLitMinGa_;
      }
    }
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilI#getIndiceLitMajDr()
   */
  @Override
  public int getIndiceLitMajDr() {
    // Retourne le point le plus a droite qui est non nul.
    if (ptLitMajDr_ == null) {
      int[] inds = getIndicesNonNuls();
      if (inds.length > 0) {
        int indicePt = inds[inds.length - 1];
        ptLitMajDr_ = (Hydraulique1dPoint) listePoints_.get(indicePt);
        return indicePt;
      }
      return -1;
    }
    int indicePt = listePoints_.indexOf(ptLitMajDr_);
    if (indicePt == -1) {
      indicePt = listePoints_.size() - 1;
      ptLitMajDr_ = (Hydraulique1dPoint) listePoints_.get(indicePt);
    }
    return indicePt;
  }

  public void setIndiceLitMajDr(int s) {
    int nbPoint = listePoints_.size();
    if (nbPoint == 0) {
      ptLitMinDr_ = null;
      ptLitMajDr_ = null;
      ptLitMinGa_ = null;
      ptLitMinDr_ = null;
    } else if (nbPoint == 1) {
      Hydraulique1dPoint pt1 = (Hydraulique1dPoint) listePoints_.get(0);
      ptLitMinDr_ = pt1;
      ptLitMajDr_ = pt1;
      ptLitMinGa_ = pt1;
      ptLitMinDr_ = pt1;
    } else {
      s = Math.min(nbPoint - 1, s);
      s = Math.max(0, s);
      ptLitMajDr_ = (Hydraulique1dPoint) listePoints_.get(s);
      if (s < getIndiceLitMinGa()) {
        ptLitMinGa_ = ptLitMajDr_;
      }
      if (s < getIndiceLitMajGa()) {
        ptLitMajGa_ = ptLitMajDr_;
      }
      if (s < getIndiceLitMinDr()) {
        ptLitMinDr_ = ptLitMajDr_;
      }
    }
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilI#getIndiceLitMajGa()
   */
  @Override
  public int getIndiceLitMajGa() {
    // Retourne le point le plus a gauche qui est non nul.
    if (ptLitMajGa_ == null) {
      int[] inds = getIndicesNonNuls();
      if (inds.length > 0) {
        int indicePt = inds[0];
        ptLitMajGa_ = (Hydraulique1dPoint) listePoints_.get(indicePt);
        return indicePt;
      }
      return -1;
    }
    int indicePt = listePoints_.indexOf(ptLitMajGa_);
    if (indicePt == -1) {
      indicePt = 0;
      ptLitMajGa_ = (Hydraulique1dPoint) listePoints_.get(indicePt);
    }
    return indicePt;
  }

  /**
   * D�finit l'indice pour la limite stockage gauche
   *
   * @param s L'indice.
   */
  public void setIndiceLitMajGa(int s) {
    int nbPoint = listePoints_.size();
    if (nbPoint == 0) {
      ptLitMinDr_ = null;
      ptLitMajDr_ = null;
      ptLitMinGa_ = null;
      ptLitMinDr_ = null;
    } else if (nbPoint == 1) {
      Hydraulique1dPoint pt1 = (Hydraulique1dPoint) listePoints_.get(0);
      ptLitMinDr_ = pt1;
      ptLitMajDr_ = pt1;
      ptLitMinGa_ = pt1;
      ptLitMinDr_ = pt1;
    } else {
      s = Math.min(nbPoint - 1, s);
      s = Math.max(0, s);
      ptLitMajGa_ = (Hydraulique1dPoint) listePoints_.get(s);
      if (s > getIndiceLitMinGa()) {
        ptLitMinGa_ = ptLitMajGa_;
      }
      if (s > getIndiceLitMajDr()) {
        ptLitMajDr_ = ptLitMajGa_;
      }
      if (s > getIndiceLitMinDr()) {
        ptLitMinDr_ = ptLitMajGa_;

      }
    }
  }

  /**
   * Retourne l'indice correspondant a la limite mini pour une zone donn�e.
   */
  private int getIndiceMinZone(int _maskZone) {
    switch (_maskZone) {
      case MASK_ZONE_STK_GA:
        return 0;
      case MASK_ZONE_MAJ_GA:
        return getIndiceLitMajGa();
      case MASK_ZONE_MIN:
        return getIndiceLitMinGa();
      case MASK_ZONE_MAJ_DR:
        return getIndiceLitMinDr();
      case MASK_ZONE_STK_DR:
        return getIndiceLitMajDr();
    }

    return -1;
  }

  /**
   * Definit l'indice correspondant a la limite mini pour une zone donn�e.
   */
  private void setIndiceMinZone(int _maskZone, int _ind) {
    switch (_maskZone) {
      case MASK_ZONE_STK_GA:
        break;
      case MASK_ZONE_MAJ_GA:
        setIndiceLitMajGa(_ind);
        break;
      case MASK_ZONE_MIN:
        setIndiceLitMinGa(_ind);
        break;
      case MASK_ZONE_MAJ_DR:
        setIndiceLitMinDr(_ind);
        break;
      case MASK_ZONE_STK_DR:
        setIndiceLitMajDr(_ind);
        break;
    }
  }

  /**
   * Retourne l'indice correspondant a la limite maxi pour une zone donn�e.
   */
  private int getIndiceMaxZone(int _maskZone) {
    switch (_maskZone) {
      case MASK_ZONE_STK_GA:
        return getIndiceLitMajGa();
      case MASK_ZONE_MAJ_GA:
        return getIndiceLitMinGa();
      case MASK_ZONE_MIN:
        return getIndiceLitMinDr();
      case MASK_ZONE_MAJ_DR:
        return getIndiceLitMajDr();
      case MASK_ZONE_STK_DR:
        return listePoints_.size() - 1;
    }

    return -1;
  }

  /**
   * Definit l'indice correspondant a la limite maxi pour une zone donn�e.
   */
  private void setIndiceMaxZone(int _maskZone, int _ind) {
    switch (_maskZone) {
      case MASK_ZONE_STK_GA:
        setIndiceLitMajGa(_ind);
        break;
      case MASK_ZONE_MAJ_GA:
        setIndiceLitMinGa(_ind);
        break;
      case MASK_ZONE_MIN:
        setIndiceLitMinDr(_ind);
        break;
      case MASK_ZONE_MAJ_DR:
        setIndiceLitMajDr(_ind);
        break;
      case MASK_ZONE_STK_DR:
        break;
    }
  }

  /**
   * Supprime les points nuls de la liste.
   */
  public void supprimePointsNuls() {
    clearSelection();
    indicesNonNuls_ = null;

    for (int i = listePoints_.size() - 1; i >= 0; i--) {
      Hydraulique1dPoint pt = (Hydraulique1dPoint) listePoints_.get(i);
      if (pt.isNull()) {
        listePoints_.remove(i);
      }
    }
  }

  public MetierPoint2D[] pointsNonNuls() {
    List<MetierPoint2D> listePtNonNuls = new ArrayList<MetierPoint2D>(listePoints_.size());
    int nbPoints = listePoints_.size();
    for (int i = 0; i < nbPoints; i++) {
      Hydraulique1dPoint pt = (Hydraulique1dPoint) listePoints_.get(i);
      if (!pt.isNull()) {
        listePtNonNuls.add(pt.toSPoint2D());
      }
    }
    return listePtNonNuls.toArray(new MetierPoint2D[listePtNonNuls.size()]);
  }

  /**
   * @deprecated Utiliser setPoints.
   */
  public void points(MetierPoint2D[] s) {
    Hydraulique1dPoint[] pts;
    if (s == null) {
      pts = new Hydraulique1dPoint[0];
    } else {
      pts = new Hydraulique1dPoint[s.length];
    }
    for (int i = 0; i < pts.length; i++) {
      pts[i] = new Hydraulique1dPoint(s[i]);
    }
    setPoints(pts);

    if (listePoints_.size() <= 1) {
      setIndiceLitMinDr(0);
    }
  }

  /**
   * Affecte les points.
   *
   * @param _pts Les points.
   */
  public void setPoints(Hydraulique1dPoint[] _pts) {
    clearSelection();
    indicesNonNuls_ = null;

    listePoints_.clear();
    listePoints_.addAll(Arrays.asList(_pts));

    fireDataChanged(Hydraulique1dProfilDataEvent.GLOBAL_MODIFICATION, null);
  }

  /**
   * @return Le tableau de points.
   */
  public Hydraulique1dPoint[] getPoints() {
    return (Hydraulique1dPoint[]) listePoints_.toArray(new Hydraulique1dPoint[listePoints_.size()]);
  }

  public final static ImportExcelResult createFromFeuilleExcel(Sheet feuille, int colonneOrigine) {
    try {
      int col0 = colonneOrigine;
      int col1 = col0 + 1;

      Hydraulique1dProfilModel p = new Hydraulique1dProfilModel();

      int lig = 0;
      p.nom(feuille.getCell(col1, lig++).getContents());
      p.setAbscisse(getCellValue(feuille.getCell(col1, lig++)));
      boolean newFormat = false;
      boolean geoRef = false;
      if (feuille.getCell(col1, LIGNE_GEO_REF).getType() == CellType.NUMBER) {
        newFormat = true;
        int geoRefMarqueur = (int) getCellValue(feuille, col1, LIGNE_GEO_REF);
        if (geoRefMarqueur == 1) {
          geoRef = true;
        }

      }
      p.avecGeoreferencement(geoRef);

      // lecture des points avant les indices des Lit mineurs, majeurs gauche et droits.
      boolean fini = false;
      List<Hydraulique1dPoint> listPoints = new ArrayList<Hydraulique1dPoint>();
      int l = 7;
      if (newFormat) {
        l++;
      }
      while (!fini) {
        if (feuille.getRows() > l) {
          if (feuille.getCell(col0, l).getType() == CellType.NUMBER) {
            double x = getCellValue(feuille, col0, l);
            double y = getCellValue(feuille, col1, l);
            Hydraulique1dPoint pt = new Hydraulique1dPoint(x, y);
            if (geoRef) {
              pt.CX(getCellValue(feuille, col1 + 1, l));
              pt.CY(getCellValue(feuille, col1 + 2, l));
            }
            listPoints.add(pt);
            l++;
          } else {
            fini = true;
          }
        } else {
          fini = true;
        }
      }
      p.listePoints_ = listPoints;

      // Lecture des indices des Lit mineurs, majeurs gauche et droits.
      p.setIndiceLitMajGa(((int) getCellValue(feuille, col1, lig++)) - 1);
      p.setIndiceLitMinGa(((int) getCellValue(feuille, col1, lig++)) - 1);
      p.setIndiceLitMinDr(((int) getCellValue(feuille, col1, lig++)) - 1);
      p.setIndiceLitMajDr(((int) getCellValue(feuille, col1, lig++)) - 1);
      ImportExcelResult res = new ImportExcelResult();
      res.model = p;
      res.lastColRead = col1;//TODO
      if (geoRef) {
        res.lastColRead = col1 + 2;
      }
      return res;
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new IllegalArgumentException(ex.getLocalizedMessage());
    }

  }

  static double getCellValue(Sheet feuille, int col, int lig) {
    return getCellValue(feuille.getCell(col, lig));
  }

  static double getCellValue(Cell cellule) {
    if (cellule.getType() == CellType.NUMBER) {
      NumberCell numberCellule = (NumberCell) cellule;
      return numberCellule.getValue();
    }
    throw new IllegalArgumentException(Hydraulique1dResource.HYDRAULIQUE1D.getString("La cellule (")
            + cellule.getRow() + ", " + cellule.getColumn()
            + Hydraulique1dResource.HYDRAULIQUE1D.getString(") ne contient pas une valeur num�rique"));

  }

  public int ecritSurFeuilleExcel(WritableSheet feuille, int colonneOrigine) {
    int col0 = colonneOrigine;
    int col1 = col0 + 1;
    int col2 = col1 + 1;
    int col3 = col2 + 1;

    try {

      WritableFont arial10ptBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
      WritableCellFormat arial10BoldFormatNoBorder = new WritableCellFormat(arial10ptBold);

      WritableCellFormat arial10BoldFormatBorder = new WritableCellFormat(arial10ptBold);
      arial10BoldFormatBorder.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.DOUBLE);

      WritableCellFormat doubleRight = new WritableCellFormat();
      doubleRight.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.DOUBLE);

      WritableCellFormat doubleLeft = new WritableCellFormat();
      doubleLeft.setBorder(jxl.format.Border.LEFT, jxl.format.BorderLineStyle.DOUBLE);

      WritableCellFormat normal = new WritableCellFormat();

      // Ent�te gauche
      int lig = 0;
      feuille.addCell(new Label(col0, lig++, Hydraulique1dResource.HYDRAULIQUE1D.getString("Intitul� du profil"), arial10BoldFormatNoBorder));
      feuille.addCell(new Label(col0, lig++, Hydraulique1dResource.HYDRAULIQUE1D.getString("Abscisse du profil"), arial10BoldFormatNoBorder));
      feuille.addCell(new Label(col0, lig++, Hydraulique1dResource.HYDRAULIQUE1D.getString("indice Zone Stock. gau."), arial10BoldFormatNoBorder));
      feuille.addCell(new Label(col0, lig++, Hydraulique1dResource.HYDRAULIQUE1D.getString("indice berge gauche"), arial10BoldFormatNoBorder));
      feuille.addCell(new Label(col0, lig++, Hydraulique1dResource.HYDRAULIQUE1D.getString("indice berge droite"), arial10BoldFormatNoBorder));
      feuille.addCell(new Label(col0, lig++, Hydraulique1dResource.HYDRAULIQUE1D.getString("indice Zone Stock. dr."), arial10BoldFormatNoBorder));
      feuille.addCell(new Label(col0, lig++, Hydraulique1dResource.HYDRAULIQUE1D.getString("G�or�f�renc�"), arial10BoldFormatNoBorder));
      feuille.addCell(new Label(col0, lig++, Hydraulique1dResource.HYDRAULIQUE1D.getString("Abscisses"), arial10BoldFormatNoBorder));
      int nbLigneEntete = lig;
      // Ent�te droite
      lig = 0;
      boolean geoRef = avecGeoreferencement();
      WritableCellFormat col1Format = doubleRight;
      if (geoRef) {
        col1Format = normal;
      }
      feuille.addCell(new Label(col1, lig++, nom(), col1Format));
      feuille.addCell(new jxl.write.Number(col1, lig++, getAbscisse(), col1Format));
      feuille.addCell(new jxl.write.Number(col1, lig++, getIndiceLitMajGa() + 1, col1Format));
      feuille.addCell(new jxl.write.Number(col1, lig++, getIndiceLitMinGa() + 1, col1Format));
      feuille.addCell(new jxl.write.Number(col1, lig++, getIndiceLitMinDr() + 1, col1Format));
      feuille.addCell(new jxl.write.Number(col1, lig++, getIndiceLitMajDr() + 1, col1Format));
      feuille.addCell(new jxl.write.Number(col1, lig++, avecGeoreferencement() ? 1 : 0, col1Format));
      feuille.addCell(new Label(col1, lig++, "Cotes", geoRef ? arial10BoldFormatNoBorder : col1Format));
      if (geoRef) {
        for (int i = 0; i < nbLigneEntete; i++) {
          feuille.addCell(new Label(col3, i, "", doubleRight));
        }
        feuille.addCell(new Label(col2, lig - 1, "X (georef)", arial10BoldFormatNoBorder));
        feuille.addCell(new Label(col3, lig - 1, "Y (georef)", arial10BoldFormatBorder));
      }

      // les cotes et abscisses
      MetierPoint2D[] points = pointsNonNuls();
      for (int i = 0; i < points.length; i++) {
        feuille.addCell(new jxl.write.Number(col0, i + nbLigneEntete, points[i].x, doubleLeft));
        feuille.addCell(new jxl.write.Number(col1, i + nbLigneEntete, points[i].y, col1Format));
        if (geoRef) {
          feuille.addCell(new jxl.write.Number(col2, i + nbLigneEntete, points[i].cx, normal));
          feuille.addCell(new jxl.write.Number(col3, i + nbLigneEntete, points[i].cy, doubleRight));
        }
      }
      feuille.setColumnView(col0, 23);
      feuille.setColumnView(col1, 15);
      if (avecGeoreferencement_) {
        feuille.setColumnView(col2, 23);
        feuille.setColumnView(col3, 23);
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    if (avecGeoreferencement_) {
      return col3;
    }
    return col1;
  }

  private final static boolean egale(MetierPoint2D[] p, List<Hydraulique1dPoint> liste) {
    if (p == null || liste == null) {
      return false;
    }

    int length = p.length;
    if (liste.size() != length) {
      return false;
    }

    for (int i = 0; i < length; i++) {
      Hydraulique1dPoint p2 = liste.get(i);
      if (!((p[i].x == p2.x()) && (p[i].y == p2.y()) && (p[i].cx == p2.Cx()) && (p[i].cy == p2.Cy()))) {
        return false;
      }
    }

    return true;
  }

  private final static boolean egale(List<Hydraulique1dPoint> l1, List<Hydraulique1dPoint> l2) {
    if (l1 == null || l2 == null) {
      return false;
    }

    int length = l1.size();
    if (l2.size() != length) {
      return false;
    }

    for (int i = 0; i < length; i++) {
      Hydraulique1dPoint p1 = l1.get(i);
      Hydraulique1dPoint p2 = l2.get(i);
      if (!((p1.x() == p2.x()) && (p1.y() == p2.y()) && (p1.Cx() == p2.Cx()) && (p1.Cy() == p2.Cy()))) {
        return false;
      }
    }

    return true;
  }

  //--- Gestion du buffer de s�lection  ------------------------------------------------/
  /**
   * Retourne les indices des points non nuls selectionn�s sur la liste des points.
   */
  public int[] getSelectedNonNuls() {
    if (selectedIndicesNonNuls_ == null ) {
      int nb = 0;
      int[] rtmp = new int[viselects_.size()];
      for (int i = 0; i < viselects_.size(); i++) {
        int isel = ((Integer) viselects_.get(i)).intValue();
        if (!((Hydraulique1dPoint) listePoints_.get(isel)).isNull()) {
          rtmp[nb++] = isel;
        }
      }

      selectedIndicesNonNuls_ = new int[nb];
      System.arraycopy(rtmp, 0, selectedIndicesNonNuls_, 0, nb);
    }
    return selectedIndicesNonNuls_;
  }

  /**
   * Nettoye les points s�lectionn�s.
   */
  public void clearSelection() {
    selectedIndicesNonNuls_ = null;
    viselects_.clear();
    fireSelectionChanged();
  }

  /**
   * Selectionne tous les points.
   */
  public void setAllSelected() {
    selectedIndicesNonNuls_ = null;

    int nb = listePoints_.size();
    viselects_.clear();
    for (int i = 0; i < nb; i++) {
      viselects_.add(new Integer(i));
    }
    fireSelectionChanged();
  }

  /**
   * Ajoute un point a la selection. Seulement si non pr�sent.
   *
   * @param _isel L'indice du point selectionn�.
   */
  public void addToSelection(int _isel) {
    selectedIndicesNonNuls_ = null;

    if (viselects_.indexOf(new Integer(_isel)) != -1) {
      return;
    }
    viselects_.add(new Integer(_isel));
    fireSelectionChanged();
  }

  /**
   * Supprime un point de la selection
   *
   * @param _isel L'indice du point selectionn�.
   */
  public void removeFromSelection(int _isel) {
    selectedIndicesNonNuls_ = null;

    viselects_.remove(new Integer(_isel));
    fireSelectionChanged();
  }

  /**
   * Definit les points selectionn�s
   *
   * @param _isels Les points s�lectionn�s
   */
  public void setSelected(int[] _isels) {
    selectedIndicesNonNuls_ = null;

    viselects_.clear();
    for (int i = 0; i < _isels.length; i++) {
      viselects_.add(new Integer(_isels[i]));
    }
    fireSelectionChanged();
  }

  /**
   * Retourne les points selectionn�s.
   */
  public int[] getSelected() {
    int[] r = new int[viselects_.size()];
    for (int i = 0; i < r.length; i++) {
      r[i] = ((Integer) viselects_.get(i)).intValue();
    }
    return r;
  }

  /**
   * Renvoie si le point est d�j� selectionn�.
   *
   * @return true si le point est selectionn�. false sinon.
   */
  public boolean isSelected(int _ind) {
    for (int i = 0; i < viselects_.size(); i++) {
      if (((Integer) viselects_.get(i)).intValue() == _ind) {
        return true;
      }
    }
    return false;
  }

  /**
   * Copie les points points d'indices donn�s vers le clipboard et les supprime.
   *
   * @param _inds Les indices de points s�lectionn�s.
   */
  public void cut(int[] _inds) {
    Arrays.sort(_inds);
    clearSelection();
    indicesNonNuls_ = null;

    vclip_.clear();
    for (int i = 0; i < _inds.length; i++) {
      vclip_.add(listePoints_.remove(_inds[i] - i));
    }
    fireDataChanged(Hydraulique1dProfilDataEvent.SUPPRESSION, _inds);
  }

  /**
   * Copie les points points d'indices donn�s vers le clipboard sans les supprimer.
   *
   * @param _inds Les indices de points s�lectionn�s.
   */
  public void copy(int[] _inds) {
    Arrays.sort(_inds);

    vclip_.clear();
    for (int i = 0; i < _inds.length; i++) {
      vclip_.add(listePoints_.get(_inds[i]).clone()); // Clone : Le point stock� n'est jamais modifi�.
    }
  }

  /**
   * Copy les points du clipboard juste apr�s le dernier point s�lectionn�. Si aucun point n'est s�lectionn�, la copie est faite � la fin du profil.
   *
   * @param _inds Les indices de points s�lectionn�s, apr�s lesquels les points doivent �tre copi�s.
   */
  public void paste(int[] _inds) {
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    if (isClibboardContientAppliExtern(clipboard)) {
      vclip_.removeAllElements();
      String chaine;
      ArrayList<List<String>> tab;

      try {
        chaine = (String) clipboard.getContents(null).getTransferData(DataFlavor.stringFlavor);
        tab = convertClipboardToTable(chaine, "\n", "\t ", ',');
        if (tab.isEmpty()) {
          return;
        }
        for (Iterator<List<String>> iter = tab.iterator(); iter.hasNext();) {
          List<String> lignei = iter.next();
          if (lignei.size() == 2) {
            double xi = Double.parseDouble(lignei.get(0).toString().trim());
            double yi = Double.parseDouble(lignei.get(1).toString().trim());
            vclip_.add(new Hydraulique1dPoint(xi, yi));
          } else if (lignei.size() >= 3) {
            double xi = Double.parseDouble(lignei.get(1).toString().trim());
            double yi = Double.parseDouble(lignei.get(2).toString().trim());
            vclip_.add(new Hydraulique1dPoint(xi, yi));
          }

        }
      } catch (Exception ex) {
        ex.getMessage();
        System.err.println("Impossible de parser le clipboard system pour le copier dans le tableau du profil");
      }

    }
    if (vclip_.isEmpty()) {
      return;
    }
    clearSelection();
    indicesNonNuls_ = null;

    int indDest;
    if (_inds.length == 0) {  // Aucun point selectionn� : Copie � la fin.
      indDest = listePoints_.size() - 1;
    } else {  // Des points sont s�lectionn�s  : On copie juste apr�s.
      Arrays.sort(_inds);
      indDest = _inds[_inds.length - 1];
    }

    // Point origine du bloc
    Hydraulique1dPoint po;
    int indOrigNonNul = 0;
    while ((po = (Hydraulique1dPoint) vclip_.get(indOrigNonNul)).isNull() && indOrigNonNul < vclip_.size() - 1) {
      indOrigNonNul++;
    }

    // Position de destination du bloc. Si point nul, on prend le pr�c�dent.
    double dx = 0;
    if (indDest >= 0) {
      Hydraulique1dPoint pd;
      int indDestNonNul = indDest;
      while ((pd = getPoint(indDestNonNul)).isNull() && indDestNonNul > 0) {
        indDestNonNul--;
      }

      if (!pd.isNull() && !po.isNull()) {
        dx = pd.x() + 1 - po.x();
      }
    }

    // Nouveaux points.
    for (int i = 0; i < vclip_.size(); i++) {
      Hydraulique1dPoint pi = (Hydraulique1dPoint) vclip_.get(i);
      listePoints_.add(indDest + 1 + i, new Hydraulique1dPoint(pi.X() == null ? null : new Double(pi.x() + dx), pi.Y(), pi.CX(), pi.CY()));
    }

    // Selection de ces nouveaux points
    int[] isels = new int[vclip_.size()];
    for (int i = 0; i < vclip_.size(); i++) {
      isels[i] = indDest + 1 + i;
    }

    fireDataChanged(Hydraulique1dProfilDataEvent.INSERTION, isels);
    setSelected(isels);
  }

  public void addSelectionListener(Hydraulique1dProfilSelectionListener _listener) {
    hsellisteners_.add(_listener);
  }

  public void removeSelectionListener(Hydraulique1dProfilSelectionListener _listener) {
    hsellisteners_.remove(_listener);
  }

  private void fireSelectionChanged() {
    for (Iterator<Hydraulique1dProfilSelectionListener> i = hsellisteners_.iterator(); i.hasNext();) {
      i.next().pointSelectionChanged(new Hydraulique1dProfilSelectionEvent(this, getSelected()));
    }
  }

  // ---Modifications des points d'indice donn�es du profil ----------------------------------------/
  /**
   * Modification des coordonn�es d'un point donn�e.
   *
   * @param _ind L'indice du point � modifier.
   * @param _x La coordonn�e X du point a modifier. Peut etre nulle.
   * @param _y La coordonn�e Y du point a modifier. Peut etre nulle.
   */
  public void modifiePoint(int _ind, Double _x, Double _y) {
    if (_ind < 0 || _ind > listePoints_.size()) {
      return;
    }
    indicesNonNuls_ = null;

    Hydraulique1dPoint pt = (Hydraulique1dPoint) listePoints_.get(_ind);
    pt.X(_x);
    pt.Y(_y);

    if (_x == null || _y == null) {
      if (ptLitMajGa_ == pt) {
        ptLitMajGa_ = null;
      }
      if (ptLitMinGa_ == pt) {
        ptLitMinGa_ = null;
      }
      if (ptLitMinDr_ == pt) {
        ptLitMinDr_ = null;
      }
      if (ptLitMajDr_ == pt) {
        ptLitMajDr_ = null;
      }
    }

    fireDataChanged(Hydraulique1dProfilDataEvent.MODIFICATION, new int[]{_ind});
  }

  /**
   * Cr�e un point vide � la suite de celui d'indice donn�.
   *
   * @param _ind L'indice du point apr�s lequel est cr�e le nouveau point. Si -1, le nouveau point est cr�e � la fin.
   */
  public void creeEmptyPoint(int _ind) {
    if (_ind < 0 || _ind > listePoints_.size()) {
      _ind = listePoints_.size();
    } else {
      _ind++;
    }
    indicesNonNuls_ = null;

    listePoints_.add(_ind, new Hydraulique1dPoint());
    fireDataChanged(Hydraulique1dProfilDataEvent.INSERTION, new int[]{_ind});
  }

  /**
   * D�place les points d'indices donn�s d'une translation suivant x et y.
   *
   * @param _inds Les indices des points � d�placer.
   * @param _tx Translation suivant x
   * @param _ty Translation suivant y
   */
  public void movePoints(int[] _inds, double _tx, double _ty) {
    List<Hydraulique1dPoint> pts = listePoints_;
    for (int i = 0; i < _inds.length; i++) {
      Hydraulique1dPoint pt = pts.get(_inds[i]);
      pt.x(pt.x() + _tx);
      pt.y(pt.y() + _ty);
    }
    fireDataChanged(Hydraulique1dProfilDataEvent.MODIFICATION, _inds);
  }

  /**
   * Copie les points d'indices donn�es. Les points a copier forment un bloc, a recopier juste apr�s le point _indDest.
   *
   * @param _inds Les indices des points � copier.
   * @param _indDest L'indice a partir duquel les points sont copi�s.
   */
  public void copiePoints(int[] _inds, int _indDest) {
    if (_inds.length == 0) {
      return;
    }
    clearSelection();
    indicesNonNuls_ = null;

    Arrays.sort(_inds); // Tri dans l'ordre, pour copier sur le dernier point.

    // Point origine du bloc
    Hydraulique1dPoint po = (Hydraulique1dPoint) listePoints_.get(_inds[0]);
    // Point destination du bloc
    Hydraulique1dPoint pe = (Hydraulique1dPoint) listePoints_.get(_indDest);
    double dx = pe.x() + 1 - po.x();

    // Nouveaux points.
    for (int i = 0; i < _inds.length; i++) {
      Hydraulique1dPoint pi = (Hydraulique1dPoint) listePoints_.get(_inds[i]);
      listePoints_.add(_indDest + 1 + i, new Hydraulique1dPoint(pi.x() + dx, pi.y()));
    }

    // Selection de ces nouveaux points
    for (int i = 0; i < _inds.length; i++) {
      _inds[i] = _indDest + 1 + i;
    }
    fireDataChanged(Hydraulique1dProfilDataEvent.INSERTION, _inds);
    setSelected(_inds);
  }

  /**
   * Supprime les points d'indices donn�s.
   *
   * @param _inds Les indice des points � d�placer.
   * @param indice int[]
   */
  public void supprimePoints(int[] _inds) {
    if (_inds.length == 0) {
      return;
    }
    clearSelection(); // On supprime la s�lection.
    indicesNonNuls_ = null;

    Arrays.sort(_inds); // Tri dans l'ordre, pour supprimer les points depuis le plus lointain vers le plus proche.

    for (int i = _inds.length - 1; i >= 0; i--) {
      int nbPoints = listePoints_.size();
      if (_inds[i] < 0 || _inds[i] >= nbPoints) {
        continue;
      }

      Hydraulique1dPoint ptASupprimer = (Hydraulique1dPoint) listePoints_.get(_inds[i]);
      if (nbPoints == 1) {
        ptLitMajDr_ = null;
        ptLitMinDr_ = null;
        ptLitMajGa_ = null;
        ptLitMinGa_ = null;
        listePoints_.remove(_inds[i]);
        return;
      }
      if (ptLitMajDr_ == ptASupprimer) {
        if (ptLitMajDr_ == listePoints_.get(nbPoints - 1)) {
          // le point a supprimer est le dernier point et la limite LitMajDr
          // La limite LitMajDr devient l'avant dernier point.
          ptLitMajDr_ = (Hydraulique1dPoint) listePoints_.get(nbPoints - 2);
        } else {
          // le point a supprimer et limite LitMajDr n'est pas le dernier point.
          // La limite LitMajDr devient le point imm�diatement � droite.
          int indicePt = listePoints_.indexOf(ptLitMajDr_);
          ptLitMajDr_ = (Hydraulique1dPoint) listePoints_.get(indicePt + 1);
        }

      }
      if (ptLitMinDr_ == ptASupprimer) {
        if (ptLitMinDr_ == listePoints_.get(nbPoints - 1)) {
          // le point a supprimer est le dernier point et la limite LitMinDr
          // La limite LitMajDr devient l'avant dernier point.
          ptLitMinDr_ = (Hydraulique1dPoint) listePoints_.get(nbPoints - 2);
        } else {
          // le point a supprimer et limite LitMinDr n'est pas le dernier point.
          // La limite LitMinDr devient le point imm�diatement � droite.
          int indicePt = listePoints_.indexOf(ptLitMinDr_);
          ptLitMinDr_ = (Hydraulique1dPoint) listePoints_.get(indicePt + 1);
        }
      }
      if (ptLitMinGa_ == ptASupprimer) {
        int indicePt = listePoints_.indexOf(ptLitMinGa_);
        if (indicePt > 0) {
          // Ce n'est pas le premier point.
          // La limite LitMinGa devient le point imm�diatement � gauche.
          ptLitMinGa_ = (Hydraulique1dPoint) listePoints_.get(indicePt - 1);
        } else {
          // C'est le premier point.
          // La limite LitMinGa devient le 2eme point imm�diatement � droite.
          ptLitMinGa_ = (Hydraulique1dPoint) listePoints_.get(1);
        }
      }
      if (ptLitMajGa_ == ptASupprimer) {
        int indicePt = listePoints_.indexOf(ptLitMajGa_);
        if (indicePt > 0) {
          // Ce n'est pas le premier point.
          // La limite LitMajGa devient le point imm�diatement � gauche.
          ptLitMajGa_ = (Hydraulique1dPoint) listePoints_.get(indicePt - 1);
        } else {
          // C'est le premier point.
          // La limite LitMajGa devient le 2eme point imm�diatement � droite.
          ptLitMajGa_ = (Hydraulique1dPoint) listePoints_.get(1);
        }
      }
      listePoints_.remove(_inds[i]);
    }
    fireDataChanged(Hydraulique1dProfilDataEvent.SUPPRESSION, _inds);
  }

  public void addDataListener(Hydraulique1dProfilDataListener _listener) {
    hlisteners_.add(_listener);
  }

  public void removeDataListener(Hydraulique1dProfilDataListener _listener) {
    hlisteners_.remove(_listener);
  }

  public void fireDataChanged(int _type, int[] _inds) {
    geoRefData.update(this);
    for (Iterator<Hydraulique1dProfilDataListener> i = hlisteners_.iterator(); i.hasNext();) {
      Hydraulique1dProfilDataListener l = i.next();

      if (_type == Hydraulique1dProfilDataEvent.MODIFICATION) {
        l.pointModified(new Hydraulique1dProfilDataEvent(this, _inds));
      } else if (_type == Hydraulique1dProfilDataEvent.GLOBAL_MODIFICATION) {
        l.pointModified(new Hydraulique1dProfilDataEvent(this, _inds));
      } else if (_type == Hydraulique1dProfilDataEvent.INSERTION) {
        l.pointInserted(new Hydraulique1dProfilDataEvent(this, _inds));
      } else {
        l.pointDeleted(new Hydraulique1dProfilDataEvent(this, _inds));
      }
    }
  }

  // ---Modifications globales des points du profil ----------------------------------------/
  /**
   * Insertion d'un profil dans une zone du profil courant.
   *
   * @param _p profil a inserer.
   * @param _maskZones Le mask des zones a inserer.
   * @param _coteMin La cote min des points a inserer. Si null, pas de cote min.
   * @param _coteMax La cote max des points a inserer. Si null, pas de cote max.
   */
  public void inserer(Hydraulique1dProfilModel _p, int _maskZones, Double _coteMin, Double _coteMax) {
    clearSelection();
    indicesNonNuls_ = null;

    // On travaille sur un profil de points temporaire.
    Hydraulique1dProfilModel prfTmp = new Hydraulique1dProfilModel();
    List<Hydraulique1dPoint> lptsTmp = prfTmp.getListePoints();

    int iOrigMin;
    int iOrigMax;
    double tx;
    int[] maskZones;

    //--- Pour les zones � droite du lit mineur et le lit mineur �galement.
    maskZones = new int[]{MASK_ZONE_MIN, MASK_ZONE_MAJ_DR, MASK_ZONE_STK_DR};
    iOrigMin = getIndiceMinZone(maskZones[0]);
    tx = Double.NaN;

    for (int izone = 0; izone < maskZones.length; izone++) {

      // Ce n'est pas une zone a inserer.
      if ((maskZones[izone] & _maskZones) == 0) {
        iOrigMin = getIndiceMinZone(maskZones[izone]);
        iOrigMax = getIndiceMaxZone(maskZones[izone]);
        if (izone == maskZones.length - 1) {
          iOrigMax++; // Derniere zone � droite. On prend le dernier point.
        }
        // Ajout des points.
        int iTmpMin = lptsTmp.size();
        for (int i = iOrigMin; i < iOrigMax; i++) {
          lptsTmp.add(listePoints_.get(i));
        }
        int iTmpMax = lptsTmp.size();

        // Limites de la zone.
        prfTmp.setIndiceMinZone(maskZones[izone], iTmpMin);
        prfTmp.setIndiceMaxZone(maskZones[izone], iTmpMax);

        iOrigMin = iOrigMax;
        tx = Double.NaN;
      } // C'est une zone a inserer.
      else {
        iOrigMax = getIndiceMaxZone(maskZones[izone]);

        // Determination des points debut/fin de la zone importee.
        int iImpMin = _p.getIndiceMinZone(maskZones[izone]);
        int iImpMax = _p.getIndiceMaxZone(maskZones[izone]);
        if (izone == maskZones.length - 1) {
          iImpMax++; // Derniere zone � droite. On prend le dernier point.
        }
        Hydraulique1dPoint ptOrigMin = getPoint(iOrigMin);
        Hydraulique1dPoint ptOrigMax = getPoint(iOrigMax);
        Hydraulique1dPoint ptImpMin = _p.getPoint(iImpMin);
        if ((ptOrigMin != null) && (ptOrigMax != null)) {
          if (Double.isNaN(tx)) {
            tx = ptImpMin.x() - ptOrigMin.x();
          }
        } else {
          tx = 0;
        }
        // Ajout des points
        int iTmpMin = lptsTmp.size();
        for (int i = iImpMin; i < iImpMax; i++) {
          Hydraulique1dPoint ptImp = _p.getPoint(i);
          if (ptOrigMax != null) {
            if (ptImp.x() - tx > ptOrigMax.x()) {
              break;
            }
          }
          // Cas particulier de la zone lit mineur : Les points ne sont ajout�s que s'ils sont compris
          // entre les cotes min et max.
          if (maskZones[izone] != MASK_ZONE_MIN
                  || (_coteMin == null || ptImp.y() >= _coteMin.doubleValue())
                  && (_coteMax == null || ptImp.y() <= _coteMax.doubleValue())) {

            lptsTmp.add(new Hydraulique1dPoint(ptImp.x() - tx, ptImp.y()));
          }
        }
        int iTmpMax = lptsTmp.size();

        // Limites de la zone.
        prfTmp.setIndiceMinZone(maskZones[izone], iTmpMin);
        prfTmp.setIndiceMaxZone(maskZones[izone], iTmpMax);
      }
    }

    //--- Pour les zones � gauche du lit mineur. On parcourt a l'envers.
    maskZones = new int[]{MASK_ZONE_MAJ_GA, MASK_ZONE_STK_GA};
    iOrigMax = getIndiceMaxZone(maskZones[0]);
    tx = Double.NaN;

    for (int izone = 0; izone < maskZones.length; izone++) {

      // Ce n'est pas une zone a inserer.
      if ((maskZones[izone] & _maskZones) == 0) {
        iOrigMin = getIndiceMinZone(maskZones[izone]);
        iOrigMax = getIndiceMaxZone(maskZones[izone]);

        // Ajout des points.
        for (int i = iOrigMax - 1; i >= iOrigMin; i--) {
          lptsTmp.add(0, listePoints_.get(i));
        }

        // Limites de la zone.
        prfTmp.setIndiceMinZone(maskZones[izone], 0);

        iOrigMax = iOrigMin;
        tx = Double.NaN;
      } // C'est une zone a inserer.
      else {
        iOrigMin = getIndiceMinZone(maskZones[izone]);

        // Determination des points debut/fin de la zone importee.
        int iImpMin = _p.getIndiceMinZone(maskZones[izone]);
        int iImpMax = _p.getIndiceMaxZone(maskZones[izone]);

        Hydraulique1dPoint ptOrigMin = getPoint(iOrigMin);
        Hydraulique1dPoint ptOrigMax = getPoint(iOrigMax);
        Hydraulique1dPoint ptImpMax = _p.getPoint(iImpMax);
        if (ptOrigMax != null) {
          if (Double.isNaN(tx)) {
            tx = ptImpMax.x() - ptOrigMax.x();
          }
        } else {
          tx = 0;
        }
        // Ajout des points
        for (int i = iImpMax - 1; i >= iImpMin; i--) {
          Hydraulique1dPoint ptImp = _p.getPoint(i);
          if (ptOrigMin != null) {
            if (ptImp.x() - tx < ptOrigMin.x()) {
              break;
            }
          }
          lptsTmp.add(0, new Hydraulique1dPoint(ptImp.x() - tx, ptImp.y()));
        }

        // Limites de la zone.
        prfTmp.setIndiceMinZone(maskZones[izone], 0);
      }
    }

    // Mise � jour des points du profil courant.
    listePoints_ = lptsTmp;

    // Mise ajour des limites
    setIndiceLitMajGa(prfTmp.getIndiceLitMajGa());
    setIndiceLitMinGa(prfTmp.getIndiceLitMinGa());
    setIndiceLitMinDr(prfTmp.getIndiceLitMinDr());
    setIndiceLitMajDr(prfTmp.getIndiceLitMajDr());

    this.supprimePointsNuls();
    fireDataChanged(Hydraulique1dProfilDataEvent.GLOBAL_MODIFICATION, null);
  }

  /**
   * Inverse le profil suivant un axe vertical.
   */
  public void inverserProfil() {
    clearSelection(); // On supprime la s�lection.
    indicesNonNuls_ = null;
    supprimePointsNuls(); // On enleve les points nuls, problematique des les m�thodes globales.

    int nb = listePoints_.size();
    if (nb == 0) {
      return;
    }

    int indLitMajGa = getIndiceLitMajGa();
    int indLitMinGa = getIndiceLitMinGa();
    int indLitMinDr = getIndiceLitMinDr();
    int indLitMajDr = getIndiceLitMajDr();

    double xmin = ((Hydraulique1dPoint) listePoints_.get(0)).x();
    double xmax = ((Hydraulique1dPoint) listePoints_.get(nb - 1)).x();

    // Inversion du profil
    for (int i = 0; i < nb; i++) {
      Hydraulique1dPoint pt = (Hydraulique1dPoint) listePoints_.get(i);
      pt.x(xmax + xmin - pt.x());
    }
    for (int i = 0; i < nb / 2; i++) {
      Hydraulique1dPoint pt1 = listePoints_.get(i);
      Hydraulique1dPoint pt2 = listePoints_.get(nb - i - 1);
      listePoints_.set(i, pt2);
      listePoints_.set(nb - i - 1, pt1);
    }

    // Les limites gauches deviennent droites et inverse.
    setIndiceLitMajGa(nb - indLitMajDr - 1);
    setIndiceLitMinGa(nb - indLitMinDr - 1);
    setIndiceLitMinDr(nb - indLitMinGa - 1);
    setIndiceLitMajDr(nb - indLitMajGa - 1);

    fireDataChanged(Hydraulique1dProfilDataEvent.GLOBAL_MODIFICATION, null);
  }

  @Override
  public int getNbPoint() {
    return this.getPoints().length;
  }

  @Override
  public double getX(int _ipt) {
    return this.getCurv(_ipt);
  }

  @Override
  public double getY(int _ipt) {
    return this.getZ(_ipt);
  }

  @Override
  public boolean canModifyY(int _ipt) {
    return (_ipt != this.getIndiceLitMajGa() && _ipt != this.getIndiceLitMinGa() && _ipt != this.getIndiceLitMinDr() && _ipt != this.
            getIndiceLitMajDr());
  }

  @Override
  public void setY(int _ipt, double _y) {
    this.getPoint(_ipt).y(_y);
  }

  /**
   * Lisse le profil suivant une m�thode m�diane/moyenne glissante.
   *
   * @param _meth La m�thode de lissage (0: Mediane, 1: Moyenne)
   * @param _l Largeur de la fenetre pour la m�diane/moyenne glissante
   */
  public void lisser(int _meth, double _l) {
    int nb = listePoints_.size();
    if (nb == 0) {
      return;
    }

    clearSelection(); // On supprime la s�lection.
    indicesNonNuls_ = null;
    supprimePointsNuls(); // On enleve les points nuls, problematique des les m�thodes globales.

    Hydraulique1DUtils.lisser(this, _l, _meth, true);

    fireDataChanged(Hydraulique1dProfilDataEvent.GLOBAL_MODIFICATION, null);
  }

  /**
   * Filtre le profil suivant une m�thode de distance tol�r�e � la corde.
   *
   * @param _d La distance � la corde
   */
  public void filtrer(double _dst) {
    clearSelection(); // On supprime la s�lection.
    indicesNonNuls_ = null;
    supprimePointsNuls(); // On enleve les points nuls, problematique des les m�thodes globales.

    int nb;

    boolean bsup = true;
    while (bsup && (nb = listePoints_.size()) >= 3) {
      nb = listePoints_.size();
      // Calcul des distances � la corde pour tous les points sauf extr�mit�s, dans un tableau tri�.
      TreeMap<Double, Integer> hdst2pts = new TreeMap<Double, Integer>();
      for (int i = 1; i < nb - 1; i++) {
        Hydraulique1dPoint pp = listePoints_.get(i - 1);
        Hydraulique1dPoint pi = listePoints_.get(i);
        Hydraulique1dPoint ps = listePoints_.get(i + 1);

        /* Droite ax+by+c=0 passant par les points pi-1 et pi+1 */
        double aa = pp.y() - ps.y();
        double bb = ps.x() - pp.x();
        double cc = pp.x() * ps.y() - ps.x() * pp.y();

        /* Distance du point a la droite orientee */
        double dst = Math.abs(aa * pi.x() + bb * pi.y() + cc) / Math.sqrt(aa * aa + bb * bb);
        hdst2pts.put(new Double(dst), new Integer(i));
      }

      // Suppression de tous les points dont la distance est inf�rieure � _dst
      bsup = false;
      for (Iterator<Double> i = hdst2pts.keySet().iterator(); i.hasNext();) {
        Double dst = i.next();
        if (dst.doubleValue() > _dst) {
          break;
        }
        int ind = ((Integer) hdst2pts.get(dst)).intValue();

        if (ind != getIndiceLitMajGa() && ind != getIndiceLitMinGa() && ind != getIndiceLitMinDr() && ind != getIndiceLitMajDr()
                && listePoints_.get(ind - 1) != null && listePoints_.get(ind + 1) != null) {
          bsup = true;
          listePoints_.set(ind, null);
        }
      }

      // Des points ont �t� supprim�s : On r�duit la liste de points.
      if (bsup) {
        for (int i = listePoints_.size() - 1; i >= 0; i--) {
          if (listePoints_.get(i) == null) {
            listePoints_.remove(i);
          }
        }
      }
    }
    fireDataChanged(Hydraulique1dProfilDataEvent.GLOBAL_MODIFICATION, null);
  }

  /**
   * Retourne Vrai si le presse papier contient des donn�es provenant d'un aplication autre que Fudaa-Mascaret On suppose que, dans ce cas, le presse
   * papier contient des donn�es au format html.
   *
   * @param clipboard Clipboard
   * @return boolean
   */
  static private boolean isClibboardContientAppliExtern(Clipboard clipboard) {
    DataFlavor[] tabDataFlavor = clipboard.getAvailableDataFlavors();
    for (int i = 0; i < tabDataFlavor.length; i++) {
      String subType = tabDataFlavor[i].getSubType();
      if ("html".equalsIgnoreCase(subType)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Convertit la chaine contenut dans le presse papier en une liste de liste de cellule
   *
   * @param _chaine La chaine contenant le presse papier (clipboard)
   * @param _lineseparator String
   * @param _cellseparator String
   * @param _localDecimalSeparator char
   * @return ArrayList liste contenant des ArrayList de cellule (String)
   */
  private static ArrayList<List<String>> convertClipboardToTable(final String _chaine, final String _lineseparator,
          final String _cellseparator, final char _localDecimalSeparator) {
    // ajouter les traitements des exceptions lev�es par les deux methodes de parse.
    final ArrayList<List<String>> tab = new ArrayList<List<String>>();
    if (_chaine != null) {
      // Fred: on va remplacer le \r et \f par rien
      String chaine = _chaine.replace('\r', ' ');
      chaine = _chaine.replace('\f', ' ');
      final ArrayList<String> listLine = parseLineFromClipboard(chaine, _lineseparator);
      for (int i = 0; i < listLine.size(); i++) {
        tab.add(parseCellFromClipboard(listLine.get(i), _cellseparator, _localDecimalSeparator));
      }
    }
    return tab;
  }

  /**
   * parse une chaine contenant des s�parateurs de ligne en une ArrayList de ligne (String)
   *
   * @param _chaine String
   * @param _lineseparator String
   * @return ArrayList
   */
  private static ArrayList<String> parseLineFromClipboard(final String _chaine, final String _lineseparator) {
    final ArrayList<String> listLigne = new ArrayList<String>();

    final StringTokenizer st = new StringTokenizer(_chaine, _lineseparator);
    while (st.hasMoreTokens()) {
      listLigne.add(st.nextToken());
    }
    return listLigne;
  }

  /**
   * @param _line String
   * @param _cellseparator String
   * @param _localDecimalSeparator char
   * @return ArrayList
   */
  private static ArrayList<String> parseCellFromClipboard(final String _line, final String _cellseparator,
          final char _localDecimalSeparator) {
    final ArrayList<String> listCell = new ArrayList<String>(50);

    final StringTokenizer st = new StringTokenizer(_line, _cellseparator, false);
    while (st.hasMoreTokens()) {
      String cell = st.nextToken().trim();
      listCell.add(cell.replace(_localDecimalSeparator, '.'));
    }
    return listCell;
  }

public double getNiveauEauInitial_() {
	return niveauEauInitial_;
}

public void setNiveauEauInitial_(double niveauEauInitial_) {
	this.niveauEauInitial_ = niveauEauInitial_;
}

}
