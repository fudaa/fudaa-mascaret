/*
 * @file         Hydraulique1dProfilSelectionEvent.java
 * @creation     2004-08-17
 * @modification $Date: 2007-02-21 16:33:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import java.util.*;

/**
 * Un evenenement de changement de selection de points de profils.
 */
public class Hydraulique1dProfilSelectionEvent extends EventObject {
  private int[] isels_;

  /**
   * Construction de l'evenement
   * @param _src La source.
   * @param _src Les indices des points selectionnés.
   */
  public Hydraulique1dProfilSelectionEvent(Object _src, int[] _isels) {
    super(_src);
    isels_=_isels;
  }

  /**
   * Retourne les indices selectionnés.
   */
  public int[] getIndices() {
    return isels_;
  }

}
