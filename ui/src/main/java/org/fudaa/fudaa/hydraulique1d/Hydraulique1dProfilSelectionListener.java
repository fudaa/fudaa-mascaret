/*
 * @file         Hydraulique1dProfilSelectionListener.java
 * @creation     2004-08-17
 * @modification $Date: 2007-02-21 16:33:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import java.util.*;

/**
 * Un listener pour les evenenement de changement de selection de points de profils.
 */
public interface Hydraulique1dProfilSelectionListener extends EventListener {

  /**
   * Le changement de selection
   */
  public void pointSelectionChanged(Hydraulique1dProfilSelectionEvent _evt);
}

