/*
 * @file         Hydraulique1dResource.java
 * @creation     1999-01-17
 * @modification $Date: 2006-09-12 08:36:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;

import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * Classe fille de ��BuRessource��. Elle cr�e entre autre un champ ��static�� ��HYDRAULIQUE1D�� de sa propre instance. En pratique, elle ne sert
 * quasiment a rien si ce n\u2019est qu\u2019� respecter le formalisme de Fudaa.
 *
 * @version $Revision: 1.7 $ $Date: 2006-09-12 08:36:42 $ by $Author: opasteur $
 * @author Axel von Arnim
 */
public class Hydraulique1dResource extends FudaaResource {
//  public final static Hydraulique1dResource HYDRAULIQUE1D=
//    new Hydraulique1dResource();

  public final static Hydraulique1dResource HYDRAULIQUE1D = new Hydraulique1dResource(FudaaResource.FUDAA);

  protected Hydraulique1dResource(final BuResource _parent) {
    super(_parent);
  }
  public final static int COULEURS[] =
          new int[]{
    0xCC0000,
    0x00CC00,
    0x0000CC,
    0xCCCC00,
    0x00CCCC,
    0xCC00CC,
    0xCC8800,
    0x88CC00,
    0x8800CC,
    0x88CC00,
    0xCC8800,
    0xCC0088,
    0xCC8888,
    0x88CC88,
    0x8888CC};

  public static final class PROFIL {

    public final static int PROFILS = 1;
    public final static int CALAGE = 2;
    public final static int PROFILS_SANS_J = 3;
  }
  public static java.io.File lastExportDir =
          new java.io.File(System.getProperty("user.dir"));
  public static java.io.File lastImportDir =
          new java.io.File(System.getProperty("user.dir"));
  private static BuAssistant assistant_;

  public static void setAssistant(BuAssistant ass) {
    assistant_ = ass;
  }

  public static BuAssistant getAssistant() {
    return assistant_;
  }
  private static int fractionDigits_ = 4;

  public static void setFractionDigits(int d) {
    fractionDigits_ = d;
  }

  public static int getFractionDigits() {
    return fractionDigits_;
  }

  public static final class PLANIMETRAGE {

    public final static int NBVALEURS_PLANI = 50;
  }

  /**
   * Traduit et retourne la chaine traduite, avec ou sans valeurs � ins�rer.
   *
   * @param _s La chaine � traduire.
   * @param _vals Les valeurs, de n'importe quelle type.
   * @return La chaine traduite.
   */
  public static final String getS(String _s, Object ... _vals) {
    String r = HYDRAULIQUE1D.getString(_s);
    if (r == null) {
      return r;
    }

    for (int i=0; i<_vals.length; i++) {
      r = FuLib.replace(r, "{"+i+"}", _vals[i].toString());
    }
    return r;
  }
}
