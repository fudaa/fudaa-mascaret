/*
 GPL 2
 */

package org.fudaa.fudaa.hydraulique1d;

/**
 *
 * @author Frederic Deniger
 */
public class ImportExcelResult {

  public int lastColRead;
  public Hydraulique1dProfilModel model;

}
