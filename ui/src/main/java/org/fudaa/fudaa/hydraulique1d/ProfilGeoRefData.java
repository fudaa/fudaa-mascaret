/*
 GPL 2
 */
package org.fudaa.fudaa.hydraulique1d;

import java.text.DecimalFormat;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;
import static org.fudaa.ctulu.CtuluLib.getDecimalFormat;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilGeoRefData {

  private double absMin;
  private double absMax;
  private double xMin;
  private double xMax;
  private double yMin;
  private double yMax;
  private boolean initialized = false;
  private static DecimalFormat format = CtuluLib.getDecimalFormat(3);

  protected void update(Hydraulique1dProfilModel model) {
    if (!model.avecGeoreferencement() || !initialized) {
      return;
    }
    final List<Hydraulique1dPoint> listePoints = model.getListePoints();
    final int nb = listePoints.size();
    for (int i = 0; i < nb; i++) {
      updatePt(listePoints.get(i));

    }

  }

  private static Double getFormattedDouble(double in) {
    return Double.valueOf(format.format(in));
  }

  private void updatePt(Hydraulique1dPoint pt) {
    if (pt.X() == null) {
      return;
    }
    if (absMin == absMax) {
      pt.CX(xMin);
      pt.CY(yMin);
    } else {
      double k = (pt.x() - absMin) / (absMax - absMin);

      pt.CX(getFormattedDouble(xMin + k * (xMax - xMin)));
      pt.CY(getFormattedDouble(yMin + k * (yMax - yMin)));
    }
  }

  public void setInitialized(boolean initialized) {
    this.initialized = initialized;
  }

  protected void initWith(Hydraulique1dProfilModel model) {
    if (!model.avecGeoreferencement()) {
      return;
    }
    initialized = true;
    final List<Hydraulique1dPoint> listePoints = model.getListePoints();
    int nb = listePoints.size();
    Hydraulique1dPoint ptMin = null;
    Hydraulique1dPoint ptMax = null;
    for (int i = 0; i < nb; i++) {
      Hydraulique1dPoint pt = listePoints.get(i);
      if (!pt.isGeorefInitialized()) {
        continue;
      }
      if (ptMin == null) {
        ptMin = pt;
      } else if (pt.x() < ptMin.x()) {
        ptMin = pt;
      }
      if (ptMax == null) {
        ptMax = pt;
      } else if (pt.x() > ptMax.x()) {
        ptMax = pt;
      }
    }
    absMin = ptMin.x();
    absMax = ptMax.x();
    xMin = ptMin.Cx();
    yMin = ptMin.Cy();
    xMax = ptMax.Cx();
    yMax = ptMax.Cy();
  }

}
