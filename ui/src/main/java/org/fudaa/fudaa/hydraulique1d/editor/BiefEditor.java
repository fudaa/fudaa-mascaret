package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.dodico.hydraulique1d.Hydraulique1dResource;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Bief;

import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuList;
import com.memoire.bu.BuResource;

/**
 * 
 * @author Adrien Hadoux
 *
 */
public class BiefEditor extends BuInternalFrame {



	
	private static final long serialVersionUID = 1L;
	private JButton btEditer_=new JButton();
	
	public BiefEditor(final MetierBief[] datas) {
		super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Profils du Bief"),true,true,true,true);


		getContentPane().setLayout(new BorderLayout());


		final ListModel<MetierBief> model = new ListModel<MetierBief>() {

			@Override
			public int getSize() {
				return datas.length;
			}

			@Override
			public MetierBief getElementAt(int index) {
				return datas[index];
			}

			@Override
			public void addListDataListener(ListDataListener l) {
				// TODO Auto-generated method stub

			}

			@Override
			public void removeListDataListener(ListDataListener l) {
				// TODO Auto-generated method stub

			}
		};

		final BuList listBiefs = new BuList(model);
		getContentPane().add(new JScrollPane(listBiefs), BorderLayout.CENTER);
		listBiefs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listBiefs.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getFirstIndex() > -1) {
					edit(model.getElementAt(e.getFirstIndex()));
				}
				
			}
		});
		
		btEditer_.setText(BuResource.BU.getString("Editer"));
		btEditer_.setIcon(BuResource.BU.getIcon("editer"));
		btEditer_.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(listBiefs.getSelectedIndex() > -1) {
					edit(model.getElementAt(listBiefs.getSelectedIndex()));
				}
				
			}
		});
		getContentPane().add(btEditer_, BorderLayout.SOUTH);
		
	}


	public void edit(MetierBief bief) {

		Hydraulique1dIHM_Bief ihmBief
		= Hydraulique1dIHMRepository.getInstance().BIEF();
		ihmBief.setBief(bief);
		
		ihmBief.editer();
	}


}
