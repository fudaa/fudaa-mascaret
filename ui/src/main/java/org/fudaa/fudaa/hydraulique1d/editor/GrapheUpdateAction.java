package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
/**
 * Action to update automatically the results when user perform a change.
 * @author Adrien
 *
 */
public class GrapheUpdateAction implements ActionListener,ListSelectionListener{

	
	public boolean isAuto = true;
	private Hydraulique1dGraphesResultatsEditor pane;
	
	public GrapheUpdateAction(Hydraulique1dGraphesResultatsEditor p) {
		pane = p;
		pane.getPnHydro().getLstBiefCasierLiaison_().addListSelectionListener(this);
		pane.getPnHydro().getLstPasTpsSection_().addListSelectionListener(this);
		pane.getPnHydro().getLstVar().addListSelectionListener(this);
		pane.getPnHydro().getCheckboxMajAuto().addActionListener(this);
		
		activatePnQeEvent();
		
	}
	
	public void activatePnQeEvent() {
		if(pane.getPnQE() !=null){
			pane.getPnQE().getLstBiefCasierLiaison_().addListSelectionListener(this);
			pane.getPnQE().getLstPasTpsSection_().addListSelectionListener(this);
			pane.getPnQE().getLstVar().addListSelectionListener(this);
			pane.getPnQE().getCheckboxMajAuto().addActionListener(this);
		}
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
			if(e.getSource() == pane.getPnHydro().getCheckboxMajAuto())
				isAuto = pane.getPnHydro().getCheckboxMajAuto().isSelected();
			else if(pane.getPnQE()!= null && e.getSource() == pane.getPnQE().getCheckboxMajAuto())
				isAuto = pane.getPnQE().getCheckboxMajAuto().isSelected();
			else if(isAuto){
				//-- do the automatic update --//
				pane.visualiser(true,true);
				
			}
	}


	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(isAuto){
			//-- do the automatic update --//
			pane.visualiser(true,true);
			
		}
		
	}

	

}
