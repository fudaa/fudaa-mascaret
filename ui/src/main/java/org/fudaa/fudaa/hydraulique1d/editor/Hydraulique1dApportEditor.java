/*
 * @file         Hydraulique1dApportEditor.java
 * @creation     2000-11-29
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'un apport de d�bit (MetierApport).<br>
 * Appeler si l'utilisateur clic sur une singularit� de type "Hydraulique1dReseauApport".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().APPORT2().editer().<br>
 * @version      $Revision: 1.18 $ $Date: 2007-11-20 11:42:45 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dApportEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuButton btDefinirLoi_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("DEFINIR UNE LOI"));
  private BuTextField tfNumero_, tfAbscisse_, tfLongueur_, tfNom_;
  private BuLabel lbAbscisse_;
  private Hydraulique1dListeLoiCombo cmbNomLoi_;
  private BuPanel pnNumero_, pnApport_, pnNomLoi_, pnAbscisse_, pnNom_;
  private BuPanel pnTypeApport_;
  private BuRadioButton rbLineique_, rbPonctuel_;
  private BuVerticalLayout loApport_, loNomLoi_;
  private BuHorizontalLayout loNumero_, loAbscisse_, loTypeApport_,loNom_;
  private MetierApport apport_;
  private MetierDonneesHydrauliques donneesHydro_;
  private MetierBief bief_;
  public Hydraulique1dApportEditor() {
    this(null);
  }
  public Hydraulique1dApportEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition d�bit d'apport"));
    cmbNomLoi_ = new Hydraulique1dListeLoiCombo(Hydraulique1dListeLoiCombo.HYDROGRAMME);
    apport_= null;
    loNumero_= new BuHorizontalLayout(5, false, false);
    loAbscisse_= new BuHorizontalLayout(5, false, false);
    loNom_= new BuHorizontalLayout(5, false, false);
    loTypeApport_= new BuHorizontalLayout(5, false, false);
    loApport_= new BuVerticalLayout(5, false, false);
    loNomLoi_= new BuVerticalLayout(5, false, false);
    Container pnMain_= getContentPane();
    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loNumero_);
    pnAbscisse_= new BuPanel();
    pnAbscisse_.setLayout(loAbscisse_);
    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    pnTypeApport_= new BuPanel();
    pnTypeApport_.setLayout(loTypeApport_);
    pnNomLoi_= new BuPanel();
    pnNomLoi_.setLayout(loNomLoi_);
    pnApport_= new BuPanel();
    pnApport_.setLayout(loApport_);
    pnApport_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    int textSize= 5;
    BuLabel lbTypeApport= new BuLabel(getS("Type d'apport"));
    Dimension dimLabel= lbTypeApport.getPreferredSize();
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(true);
    BuLabel lbNumero= new BuLabel(getS("N� d�bit"));
    lbNumero.setPreferredSize(dimLabel);
    pnNumero_.add(lbNumero, 0);
    pnNumero_.add(tfNumero_, 1);
    textSize= 10;
    tfAbscisse_= BuTextField.createDoubleField();
    tfAbscisse_.setColumns(textSize);
    tfAbscisse_.setEditable(true);
    BuLabel lbAbscisse= new BuLabel(getS("Abscisse"));
    lbAbscisse.setPreferredSize(dimLabel);
    pnAbscisse_.add(lbAbscisse, 0);
    pnAbscisse_.add(tfAbscisse_, 1);
    lbAbscisse_= new BuLabel("   ");
    pnAbscisse_.add(lbAbscisse_, 2);
    tfNom_= new BuTextField();
    //tfNom_.setColumns(textSize);
    tfNom_.setEditable(true);
    BuLabel lbNom = new BuLabel(getS("Nom"));
    lbNom.setPreferredSize(dimLabel);
    pnNom_.add(lbNom, 0);
    pnNom_.add(tfNom_, 1);
    tfLongueur_= BuTextField.createDoubleField();
    tfLongueur_.setColumns(textSize);
    pnTypeApport_.add(lbTypeApport, 0);
    pnTypeApport_.add(constructPanelApport(), 1);
    pnNomLoi_.add(new BuLabel(getS("nom de la loi de type")+
                              "'"+getS("Hydrogramme")
                              +" Q(t)'"), 0);
    pnNomLoi_.add(cmbNomLoi_, 1);
    btDefinirLoi_.setActionCommand("DEFINIR_LOI_HYDROGRAMME");
    btDefinirLoi_.addActionListener(this);
    pnNomLoi_.add(btDefinirLoi_, 2);
    pnApport_.add(pnNumero_, 0);
    pnApport_.add(pnAbscisse_, 1);
    pnApport_.add(pnNom_, 2);
    pnApport_.add(pnTypeApport_, 3);
    pnApport_.add(pnNomLoi_, 4);
    pnMain_.add(pnApport_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("LINEIQUE".equals(cmd)) {
      tfLongueur_.setEnabled(true);
    }
    else if ("PONCTUEL".equals(cmd)) {
      tfLongueur_.setEnabled(false);
    }
    else if ("ANNULER".equals(cmd)) {
      fermer();
    }
    else if (cmd.startsWith("DEFINIR_LOI")) {
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().setQualiteDEau(false);
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer();
    }
    else if ("VALIDER".equals(cmd)) {
      if (rbLineique_.isSelected()) {
        Double longueur = (Double) tfLongueur_.getValue();
        if (longueur == null) {
          showBuError(getS("Longueur vide pas autoris�e"), true);
          return;
        }
        else if (longueur.doubleValue() < 0) {
          showBuError(getS("Longueur n�gative pas autoris�e"), true);
          return;
        }
      }
      if (cmbNomLoi_.getValeurs() == null) {
        showBuError(getS("Aucune loi s�lectionn�e"), true);
        System.err.println(getS("Aucune loi selectionnee"));
        return;
      }
      if (getValeurs()) {
        firePropertyChange("singularite", null, apport_);
      }
      fermer();
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _app) {
    if (!(_app instanceof MetierApport))
      return;
    if (_app == apport_)
      return;
    apport_= (MetierApport)_app;
    setValeurs();
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (apport_ == null)
      return changed;
    double longu= 0;
    if (rbLineique_.isSelected()) {
      longu = ( (Double) tfLongueur_.getValue()).doubleValue();
    }
    if (longu != apport_.longueur()) {
      apport_.longueur(longu);
      changed= true;
    }
    int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != apport_.numero()) {
      apport_.numero(nnum);
      changed= true;
    }
    String nom= tfNom_.getText();
    if (!nom.equals(apport_.nom())) {
      apport_.nom(nom);
      changed = true;
    }
    double absc= ((Double)tfAbscisse_.getValue()).doubleValue();
    if (absc != apport_.abscisse()) {
      apport_.abscisse(absc);
      changed= true;
    }
    MetierLoiHydraulique loi= cmbNomLoi_.getValeurs();
    if (loi != apport_.loi()) {
      apport_.loi(loi);
      changed= true;
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(apport_.numero()));
    tfAbscisse_.setValue(new Double(apport_.abscisse()));
    tfNom_.setValue(apport_.nom());
    double longueur = apport_.longueur();
    if (longueur > 0) {
      tfLongueur_.setValue(new Double(apport_.longueur()));
      rbLineique_.setSelected(true);
      tfLongueur_.setEnabled(true);
    } else {
      rbPonctuel_.setSelected(true);
      tfLongueur_.setEnabled(false);
    }
    cmbNomLoi_.initListeLoi();
    if (apport_.loi() != null)
      cmbNomLoi_.setValeurs(apport_.loi());
    String textAbsc= "";
    if (bief_ != null) {
      textAbsc= "du bief n�" + (bief_.indice()+1);
      if ((bief_.extrAmont().profilRattache() != null)
        && (bief_.extrAval().profilRattache() != null))
        textAbsc=
          textAbsc
            + " entre "
            + bief_.extrAmont().profilRattache().abscisse()
            + " et "
            + bief_.extrAval().profilRattache().abscisse();
      else
        textAbsc= textAbsc + " ("+getS("abscisses des extremit�s inconnues")+")";
    } else
      textAbsc= "bief inconnu";
    lbAbscisse_.setText(textAbsc);
  }
  // ObjetEventListener
  @Override
  public void objetCree(H1dObjetEvent e) {
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
/*    System.out.println("objetModifie e="+e.enChaine());
    System.out.println("e.getSource().getClass()="+e.getSource().getClass());
    System.out.println("e.getMessage()="+e.getMessage());
    System.out.println("e.getChamp()="+e.getChamp());*/
    MetierHydraulique1d src= e.getSource();
    String champ= e.getChamp();
    if (src == null)
      return;
    if ((src instanceof MetierDonneesHydrauliques)&&("lois".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
    if ((src instanceof MetierLoiHydrogramme)&&("nom".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
  }

  public void setDonneesHydrauliques(MetierDonneesHydrauliques _donneesHydro) {
    donneesHydro_= _donneesHydro;
    cmbNomLoi_.setDonneesHydro(donneesHydro_);
  }
  public void setBiefParent(MetierBief _bief) {
    bief_= _bief;
  }
  private BuPanel constructPanelApport() {
    BuPanel panel= new BuPanel();
    panel.setLayout(new GridBagLayout());
    panel.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    rbLineique_= new BuRadioButton(getS("Lin�ique"));
    rbLineique_.addActionListener(this);
    rbLineique_.setActionCommand("LINEIQUE");
    rbPonctuel_= new BuRadioButton(getS("Ponctuel"));
    rbPonctuel_.addActionListener(this);
    rbPonctuel_.setActionCommand("PONCTUEL");
    ButtonGroup groupeRadioBouton= new ButtonGroup();
    groupeRadioBouton.add(rbLineique_);
    groupeRadioBouton.add(rbPonctuel_);
    rbPonctuel_.setSelected(true);
    Insets inset3_5= new Insets(3, 5, 0, 0);
    Insets inset3_0= new Insets(3, 0, 0, 0);
    // radio bouton Lin�ique
    panel.add(
      rbLineique_,
      new GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // ligne horizontale
    BuPanel pnLigneHorizontal= new BuPanel();
    pnLigneHorizontal.setPreferredSize(new Dimension(30, 5));
    pnLigneHorizontal.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    panel.add(
      pnLigneHorizontal,
      new GridBagConstraints(
        1,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_0,
        10,
        0));
    // label  "Nom de la loi de type "hydrogramme Q(t)"
    BuLabel lb= new BuLabel(getS("Longueur"));
    panel.add(
      lb,
      new GridBagConstraints(
        2,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
      panel.add(
      tfLongueur_,
      new GridBagConstraints(
        3,
        0,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    // radio bouton ponctuel
    panel.add(
      rbPonctuel_,
      new GridBagConstraints(
        0,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    return panel;
  }
}
