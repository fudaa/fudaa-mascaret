/*
 * @file         Hydraulique1dBiefEditor.java
 * @creation     1999-04-27
 * @modification $Date: 2008-03-04 15:46:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BPanneauEditorAction;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;
import org.fudaa.fudaa.hydraulique1d.graphe3D.PanelGraphe3D;
import org.fudaa.fudaa.hydraulique1d.graphe3D.PanelGraphe3Dzyx;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Profil;
import org.fudaa.fudaa.hydraulique1d.tableau.ActionDoubleClickEvent;
import org.fudaa.fudaa.hydraulique1d.tableau.ActionDoubleClickListener;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauProfilsModel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur d'un bief (MetierBief).<br>
 * Appeler si l'utilisateur clic sur un bief du r�seau de type "Hydraulique1dReseauBiefCourbe".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().BIEF().editer().<br>
 *
 * @version $Revision: 1.23 $ $Date: 2008-03-04 15:46:52 $ by $Author: opasteur $
 * @author Axel von Arnim
 */
public class Hydraulique1dBiefEditor extends Hydraulique1dCustomizerImprimable
        implements ActionDoubleClickListener, BuCutCopyPasteInterface {

  protected Hydraulique1dTableauProfilsModel modele_;
  protected Hydraulique1dTableauReel tableau_;
  private MetierBief ibief_;
  private MetierEtude1d ietude_;

  
  private BuTabbedPane tabbedPane = new BuTabbedPane();
  private Hydraulique1dPlanimetrageEditor planimetrage = new Hydraulique1dPlanimetrageEditor();
  private BuPanel panelPrfils = new BuPanel(new BorderLayout());
  private PanelGraphe3D panel3d = new PanelGraphe3D(); 
  /*private PanelGraphe3Dzyx panel3dxyz = new PanelGraphe3Dzyx();*/ 
  private JComboBox<MetierBief> listBiefs;
  
  public Hydraulique1dBiefEditor(String titre) {
    super(titre);
    modele_ = new Hydraulique1dTableauProfilsModel();
    tableau_ = new Hydraulique1dTableauReel(modele_);
    JScrollPane sp = new JScrollPane(tableau_);
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(tabbedPane,BorderLayout.CENTER);
    panelPrfils.add(sp,BorderLayout.CENTER);
    tabbedPane.addTab(getS("Profils du Bief"), panelPrfils);
    tabbedPane.addTab(getS("Planimetrage"), planimetrage.getContentPane());
    tabbedPane.addTab(getS("Profil 3d"), panel3d);
    //tabbedPane.addTab(getS("Profil 3d 2"), panel3dxyz);
    
    
    listBiefs = new JComboBox<MetierBief>();
    JPanel panelListeBiefs = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    panelListeBiefs.add(listBiefs);
    getContentPane().add(panelListeBiefs, BorderLayout.NORTH);
    
    
    setActionPanel(EbliPreferences.DIALOG.CREER | EbliPreferences.DIALOG.SUPPRIMER
            | EbliPreferences.DIALOG.EDITER | EbliPreferences.DIALOG.IMPORTER
            | EbliPreferences.DIALOG.EXPORTER);
    
    pnAction_.addCustomButton(getS("DUPLIQUER"), "DUPLIQUER");
    pnAction_.addCustomButton(getS("TRIER"), "TRIER");
    tableau_.addActionDoubleClickListener(this);
    setNavPanel(EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  BPanneauEditorAction pnAction_ ;
  
  public void setActionPanel(final int _mode) {	   
	    pnAction_ = new BPanneauEditorAction(_mode);
	    pnAction_.addActionListener(this);
	    panelPrfils.add(pnAction_,BorderLayout.SOUTH);	 
	    
	  }
 
  

  /**
   * Invoquer lors du "double-clic" sur une ligne de tableau.
   *
   * @param event L'�v�nement int�grant une r�f�rence vers le tableau et la ligne.
   */
  @Override
  public void actionDoubleClick(ActionDoubleClickEvent event) {
    editer();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    
	String cmd = _evt.getActionCommand();
    if (cmd.equalsIgnoreCase("CREER")) {
      tableau_.ajouterLigne();
    } else if (cmd.equalsIgnoreCase("SUPPRIMER")) {
      tableau_.supprimeLignesSelectionnees();
    } else if (cmd.equalsIgnoreCase("VALIDER")) {
      boolean res = getValeurs();
      if(res)
    	  fermer();
    } else if (cmd.equalsIgnoreCase("EDITER")) {
      editer();
    } else if (cmd.equalsIgnoreCase("IMPORTER")) {
      importer();
    } else if (cmd.equalsIgnoreCase("EXPORTER")) {
      exporter();
    } else if (cmd.equalsIgnoreCase("DUPLIQUER")) {
      dupliquer();
    } else if (cmd.equalsIgnoreCase("TRIER")) {
      trier();
    }
    super.actionPerformed(_evt);
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierBief) {
      ibief_ = (MetierBief) _n;
      modele_.setModeleMetier(ibief_);
    } else if (_n instanceof MetierEtude1d) {
      ietude_ = (MetierEtude1d) _n;
    }
  }

  @Override
  public void setValeurs() {
    modele_.setValeurs();
    try{
    if(planimetrage != null)
    	planimetrage.setValeurs();
    if(panel3d != null)
    	panel3d.feedGraph();
    }catch(Exception e) {
    	e.printStackTrace();
    }
   /* if(panel3dxyz != null)
    	panel3dxyz.feedGraph();
    */
  }

  @Override
  public boolean getValeurs() {
	  boolean resProfil =  modele_.getValeurs();
	  //-- get values from all tab and dsplay specifics tab if errors occurs --//
	  if(!resProfil)
		  tabbedPane.setSelectedIndex(0);
		  
	  boolean resPlanim = planimetrage.validatePlanimetrage(false);
	  if(!resPlanim) {
		  tabbedPane.setSelectedIndex(1);
	      return false;
	  }
    return true;
  }

  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface printable). Le format <code>_format</code> sera celui donne par la
   * methode <code>Pageable.getPageFormat(int)</code>.
   *
   * @param _g Graphics
   * @param _format PageFormat
   * @param _numPage int
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe, sinon <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _numPage) {
    return EbliPrinter.printComponent(_g, _format, tableau_, true, _numPage);
  }

  @Override
  public void cut() {
    tableau_.cut();
  }

  @Override
  public void copy() {
    tableau_.copy();
  }

  @Override
  public void paste() {
    tableau_.paste();
  }

  @Override
  public void duplicate() {
    dupliquer();
  }

  public void editer() {
    String message = "";
    int[] indexes = tableau_.getSelectedRows();
    Hydraulique1dProfilModel[] tabProfilsBief = modele_.getTabProfilModel();
    //if (tabProfilsBief.length == 0) return;
    Hydraulique1dIHM_Profil ihmProfil = new Hydraulique1dIHM_Profil(ietude_);
    ihmProfil.setProfilsModeleBief(tabProfilsBief);
    Hydraulique1dProfilModel[] tabProfSelected = modele_.getTabProfilModel(indexes);
    if (tabProfSelected.length == 0) {//aucune ligne selectionn�e
      if (tabProfilsBief.length > 0) {//S'il y a des profils dans le tableau
        if (indexes.length == 0) {
          ihmProfil.setProfilModeleCourant(tabProfilsBief[0]);
        } else {
          message = getS("Les lignes s�l�ctionn�es sont vides") + "\n"
                  + getS("Pour cr�er un profil vous devez d'abord remplir une ligne (intitul� et abscisse)");
        }
      } else {//S'il n'y a pas de profils dans le tableau
        message = getS("Aucun profil � �diter") + "\n"
                + getS("Pour cr�er un profil vous devez d'abord remplir une ligne (intitul� et abscisse)");
      }
    } else {//au moins une ligne selectionn�e
      ihmProfil.setProfilModeleCourant(tabProfSelected[0]);
    }


    /* if (tabProfSelected.length >0) {
     ihmProfil.setProfilModeleCourant(tabProfSelected[0]);
     } else {
     if (tabProfilsBief.length > 0) {
     ihmProfil.setProfilModeleCourant(tabProfilsBief[0]);
     }
     }*/
    if (!message.isEmpty()) {
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(), message).activate();
    } else {
      ihmProfil.editer();
    }
  }

  public void exporter() {
    String geoRef = "georef";
    String xls = "xls";
    String[] ext = {xls, geoRef};
    String[] desc = {getS("Geom�trie au format Excel"), getS("Fichier georef")};

    File fToSave = Hydraulique1dExport.chooseFile(ext, desc);
    if (fToSave == null) {
      return;
    }

    FudaaCommonImplementation fci = (FudaaCommonImplementation) ((Hydraulique1dBaseApplication) Hydraulique1dBaseApplication.FRAME)
            .getImplementation();
    String userExt = StringUtils.lowerCase(CtuluLibFile.getExtension(fToSave.getName()));
    if (userExt == null || (!userExt.equals(geoRef) && !userExt.equals(xls))) {
      fci.error(getS("L'extension du fichier choisi n'est pas connu"));
    }
    if (fToSave.exists()) {
      boolean b = fci.question(getS("Confirmation"), getS("Le fichier ") + fToSave.getName() + " " + getS("existe d�j�. Voulez-vous l'�craser?"));
      if (!b) {
        return;
      }
    }
    if (userExt.equals(xls)) {
      exportToExcel(fToSave, fci);
    } else {
      exportToGeoRef(fToSave, fci);
    }
  }

  private void exportToGeoRef(File fToSave, FudaaCommonImplementation fci) {
    final MetierProfil[] currentProfils = modele_.getCurrentMetierProfil();
    MetierBief bief = new MetierBief();
    if (ibief_ != null) {
      bief.numero(ibief_.numero());
    }
    bief.profils(currentProfils);
    Hydraulique1dExport.exportProfils_MASCARET(fToSave, new MetierBief[]{bief});

  }

  private void exportToExcel(File fToSave, FudaaCommonImplementation fci) {
    try {
      WritableWorkbook classeur = Workbook.createWorkbook(fToSave);
      Hydraulique1dProfilModel[] profils = modele_.getTabProfilModel();

      int nbProfil = profils.length;
      int reste = nbProfil % 100;
      int nbFeuille = -1;
      if (reste == 0) {
        nbFeuille = nbProfil / 100;
      } else {
        nbFeuille = (nbProfil / 100) + 1;
      }

      WritableSheet feuille = null;
      int col = 0;
      for (int i = 0; i < nbProfil; i++) {
        int iFeuille = i / 100;
        int nbFeuilleDsClasseur = classeur.getSheets().length;

        if (iFeuille >= (nbFeuilleDsClasseur)) {
          col = 0;
          feuille = classeur.createSheet(ibief_.getInfos()[0] + " " + (iFeuille + 1)
                  + " sur " + nbFeuille, iFeuille);
        }
        col = 1 + profils[i].ecritSurFeuilleExcel(feuille, col);
      }

      classeur.write();
      classeur.close();
    } catch (Exception ex) {
      ex.printStackTrace();
      fci.error(getS("Erreur lors de l'exportation Excel :") + ex.getLocalizedMessage());
    }
  }

  public void importer() {
    ImportChooser c = new ImportChooser(Hydraulique1dBaseApplication.FRAME);
    c.show();
    final int choice = c.getChoice();
    if (choice == 0) {
      return;
    }
    File file;
    String[] extensionsFichierGeometrie = {"geo", "pro"};
    String[] extensionsFichierGeometrieMasc = {"geo", "pro", "georef"};
    switch (choice) {
      case ImportChooser.TXT:
        file = Hydraulique1dImport.chooseFile("txt");
        break;
      case ImportChooser.PRO_LIDO:
        file = Hydraulique1dImport.chooseFile(extensionsFichierGeometrie);
        break;
      case ImportChooser.PRO_MASCARET:
        file = Hydraulique1dImport.chooseFile(extensionsFichierGeometrieMasc);
        break;
      case ImportChooser.PRO_RIVICAD:
        file = Hydraulique1dImport.chooseFile("lid");
        break;
      case ImportChooser.PRO_EXCEL:
        file = Hydraulique1dImport.chooseFile("xls");
        file = CtuluLibFile.appendExtensionIfNeeded(file, "xls");
        break;
      default:
        file = null;
    }

    /*File file=
     Hydraulique1dImport.chooseFile(
     choice == ImportChooser.TXT
     ? "txt"
     : choice == ImportChooser.PRO_LIDO
     ? "pro"
     : choice == ImportChooser.PRO_MASCARET
     ? "pro"
     : choice == ImportChooser.PRO_RIVICAD
     ? "lid"
     : choice == ImportChooser.PRO_EXCEL
     ? "xls"
     : null);*/
    if (file == null) {
      return;
    }
    Hydraulique1dProfilModel[] profils
            = Hydraulique1dImport.importProfils(
                    file,
                    choice == ImportChooser.TXT
                    ? "TXT"
                    : choice == ImportChooser.PRO_LIDO
                    ? "PRO_LIDO"
                    : choice == ImportChooser.PRO_MASCARET
                    ? "PRO_MASCARET"
                    : choice == ImportChooser.PRO_RIVICAD
                    ? "PRO_RIVICAD"
                    : choice == ImportChooser.PRO_EXCEL
                    ? "PRO_EXCEL"
                    : "");
    if ((profils == null)) {
      new BuDialogError(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              getS("ERREUR") + ": " + getS("l'importation de profils") + "\n" + getS("a �chou�."))
              .activate();
      return;
    }
    if ((profils.length == 0)) {
      new BuDialogError(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              getS("ERREUR") + ": " + getS("aucun profil n'est") + "\n" + getS("disponible dans cette import!"))
              .activate();
      return;
    }
    if (c.isMiseAZero()) {
      Hydraulique1dProfilModel.miseAZeroProfil(profils);
    } else if (c.isOffset()) {
      double offset = c.getOffset();
      Hydraulique1dProfilModel.decalageAbscisseProfils(profils, offset);
    }
    modele_.setProfilsModel(profils);
  }

  public void dupliquer() {
    int[] indexes = tableau_.getSelectedRows();
    modele_.dupliquer(indexes);
  }

  public void trier() {
    modele_.trier();
  }

public Hydraulique1dPlanimetrageEditor getPlanimetrage() {
	return planimetrage;
}

public void setPlanimetrage(Hydraulique1dPlanimetrageEditor planimetrage) {
	this.planimetrage = planimetrage;
}



public PanelGraphe3D getPanel3d() {
	return panel3d;
}



public void setPanel3d(PanelGraphe3D panel3d) {
	this.panel3d = panel3d;
}



public JComboBox<MetierBief> getListBiefs() {
	return listBiefs;
}



public void setListBiefs(JComboBox<MetierBief> listBiefs) {
	this.listBiefs = listBiefs;
}


/*
public PanelGraphe3Dzyx getPanel3dxyz() {
	return panel3dxyz;
}



public void setPanel3dxyz(PanelGraphe3Dzyx panel3dxyz) {
	this.panel3dxyz = panel3dxyz;
}*/
}

class ImportChooser extends JDialog implements ActionListener {

  public final static int TXT = 0x01;
  public final static int PRO_LIDO = 0x02;
  public final static int PRO_MASCARET = 0x03;
  public final static int PRO_RIVICAD = 0x04;
  public final static int PRO_EXCEL = 0x05;
  private int choice_;
  private JRadioButton cbTxt_, cbProLido_, cbProMascaret_, cbProRivicad_, cbProExcel_;
  private JCheckBox cbBief_, cbMiseAZero_, cbOffset_;
  private BuTextField tfOffset_;

  public ImportChooser(Frame parent) {
    super(parent, true);
    choice_ = 0;
    getContentPane().setLayout(new BuVerticalLayout());
    BuPanel pn = new BuPanel();
    pn.setBorder(
            new CompoundBorder(
                    new EmptyBorder(new Insets(5, 5, 5, 5)),
                    new EtchedBorder()));
    pn.setLayout(new BuVerticalLayout());
    cbTxt_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format texte (.txt)"));
    cbTxt_.addActionListener(this);
    pn.add(cbTxt_, 0);
    cbProLido_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Lido 2.0 (.pro ou .geo)"));
    cbProLido_.addActionListener(this);
    pn.add(cbProLido_, 1);
    cbProMascaret_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Macaret 5.0 (.pro, .geo ou .georef)"));
    cbProMascaret_.addActionListener(this);
    pn.add(cbProMascaret_, 2);
    cbProRivicad_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Rivicad (.lid)"));
    cbProRivicad_.addActionListener(this);
    pn.add(cbProRivicad_, 3);
    cbProExcel_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format profils Excel (.xls)"));
    cbProExcel_.addActionListener(this);
    pn.add(cbProExcel_, 4);
    int n = 0;
    getContentPane().add(pn, n++);
    cbMiseAZero_ = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mise � 0 des abscisses"));
    cbMiseAZero_.addActionListener(this);
    cbMiseAZero_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    cbMiseAZero_.setSelected(false);
    getContentPane().add(cbMiseAZero_, n++);
    cbOffset_ = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("D�caler les abscisses"));
    cbOffset_.addActionListener(this);
    cbOffset_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    cbOffset_.setSelected(false);
    getContentPane().add(cbOffset_, n++);
    tfOffset_ = BuTextField.createDoubleField();
    tfOffset_.setValue(new Double(0.));
    tfOffset_.setEnabled(false);
    getContentPane().add(tfOffset_, n++);
    pack();
    setLocationRelativeTo(parent);
    //pack();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Object src = e.getSource();
    if (src == cbTxt_) {
      choice_ = TXT;
      dispose();
    } else if (src == cbProLido_) {
      choice_ = PRO_LIDO;
      dispose();
    } else if (src == cbProMascaret_) {
      choice_ = PRO_MASCARET;
      dispose();
    } else if (src == cbProRivicad_) {
      choice_ = PRO_RIVICAD;
      dispose();
    } else if (src == cbProExcel_) {
      choice_ = PRO_EXCEL;
      dispose();
    } else if (src == cbBief_) {
      if (cbBief_.isSelected()) {
        cbMiseAZero_.setSelected(false);
      }
    } else if (src == cbMiseAZero_) {
      if (cbMiseAZero_.isSelected()) {
        cbBief_.setSelected(false);
        cbOffset_.setSelected(false);
        tfOffset_.setEnabled(false);
      }
    } else if (src == cbOffset_) {
      if (cbOffset_.isSelected()) {
        cbMiseAZero_.setSelected(false);
        tfOffset_.setEnabled(true);
      } else {
        tfOffset_.setEnabled(false);
      }
    }
  }

  public int getChoice() {
    return choice_;
  }

  public boolean isNouveauBief() {
    return cbBief_.isSelected();
  }

  public boolean isMiseAZero() {
    return cbMiseAZero_.isSelected();
  }

  public boolean isOffset() {
    return cbOffset_.isSelected();
  }

  public double getOffset() {
    Double val = (Double) tfOffset_.getValue();
    if (val == null) {
      return 0.;
    }
    return val.doubleValue();
  }
}
