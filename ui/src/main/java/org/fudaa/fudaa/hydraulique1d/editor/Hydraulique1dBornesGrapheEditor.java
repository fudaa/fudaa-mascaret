/*
 * @file         Hydraulique1dLissageProfilEditor.java
 * @creation     1999-12-28
 * @modification $Date: 2007-02-21 16:33:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur pour le choix de la m�thode de lissage.
 * @author Bertrand Marchand
 * @version 1.0
 */
public class Hydraulique1dBornesGrapheEditor extends JDialog {
  private JPanel pnPrinc_=new JPanel();
  private Border bdpnPrinc_;
  private JPanel pnBoutons_=new JPanel();
  private BorderLayout lyThis_=new BorderLayout();
  private BuButton btOk_=new BuButton();
  private BuButton btAnnuler_=new BuButton();
  private BuGridLayout lyPrinc_=new BuGridLayout(7,5,2);
  private JLabel lbXmin_=new JLabel();
  private JLabel lbXminUnite_=new JLabel();
  private JLabel lbYmin_=new JLabel();
  private BuTextField tfYmin_=BuTextField.createDoubleField();
  private JLabel lbYminUnite_=new JLabel();
  public int reponse=0;
  private BuTextField tfXmin_=BuTextField.createDoubleField();
  private JLabel lbSep1_=new JLabel();
  private JLabel lbXmax_=new JLabel();
  private BuTextField tfXmax_=BuTextField.createDoubleField();
  private JLabel lbXmaxUnite_=new JLabel();
  private JLabel lbSep2_=new JLabel();
  private JLabel lbYmax_=new JLabel();
  private BuTextField tfYmax_=BuTextField.createDoubleField();
  private JLabel lbYmaxUnite_=new JLabel();
  public Hydraulique1dBornesGrapheEditor(Frame parent) {
    super(parent);
    setModal(true);
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    customize();
    pack();
    setLocationRelativeTo(parent);
  }

  private void jbInit() throws Exception {
    bdpnPrinc_=BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
                                               BorderFactory.createEmptyBorder(5, 5, 5, 5));
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fixer les bornes du graphe"));
    this.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        this_windowClosed(e);
      }
    }); this.getContentPane().setLayout(lyThis_);
    pnPrinc_.setBorder(bdpnPrinc_);
    pnPrinc_.setLayout(lyPrinc_);
    btOk_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    btOk_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btOk_actionPerformed(e);
      }
    });
    btAnnuler_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    btAnnuler_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btAnnuler_actionPerformed(e);
      }
    });
    lbXmin_.setText("Xmin :");
    lbYmin_.setText("Ymin :");
    lbYminUnite_.setText("m");
    tfXmin_.setPreferredSize(new Dimension(70, 20));
    lbXminUnite_.setText("m");
    tfYmin_.setMinimumSize(new Dimension(70, 20));
    tfYmin_.setPreferredSize(new Dimension(70, 20));
    lbSep1_.setPreferredSize(new Dimension(20, 0));
    lbXmax_.setText("Xmax :");
    tfXmax_.setPreferredSize(new Dimension(70, 20));
    lbXmaxUnite_.setText("m");
    lbYmax_.setText("Ymax :");
    lbYmaxUnite_.setText("m");
    lbSep2_.setPreferredSize(new Dimension(20, 0));
    tfYmax_.setPreferredSize(new Dimension(70, 20));
    pnPrinc_.add(lbXmin_);
    pnPrinc_.add(tfXmin_);
    pnPrinc_.add(lbXminUnite_);
    pnPrinc_.add(lbSep1_);
    pnPrinc_.add(lbXmax_);
    pnPrinc_.add(tfXmax_);
    pnPrinc_.add(lbXmaxUnite_);
    pnPrinc_.add(lbYmin_);
    pnPrinc_.add(tfYmin_);
    pnPrinc_.add(lbYminUnite_);
    pnPrinc_.add(lbSep2_);
    pnPrinc_.add(lbYmax_);
    pnPrinc_.add(tfYmax_);
    pnPrinc_.add(lbYmaxUnite_);
    pnBoutons_.add(btOk_);
    pnBoutons_.add(btAnnuler_);
    this.getContentPane().add(pnBoutons_, java.awt.BorderLayout.SOUTH);
    this.getContentPane().add(pnPrinc_, java.awt.BorderLayout.CENTER);
  }

  private void customize() {
    btOk_.setIcon(BuResource.BU.getIcon("valider"));
    btAnnuler_.setIcon(BuResource.BU.getIcon("annuler"));
  }

  public void btOk_actionPerformed(ActionEvent e) {
    reponse=1;
    setVisible(false);
  }

  public void btAnnuler_actionPerformed(ActionEvent e) {
    reponse=0;
    setVisible(false);
  }

  public void this_windowClosed(WindowEvent e) {
    reponse=0;
    setVisible(false);
  }

  /**
   * Definit les bornes min/max.
   * @param bornes Les bornes, dans l'ordre xmin,xmax,ymin,ymax. null si un pb avec une valeur.
   */
  public void setBornes(double[] _bornes) {
    tfXmin_.setText(new DecimalFormat("0.00").format(_bornes[0]));
    tfXmax_.setText(new DecimalFormat("0.00").format(_bornes[1]));
    tfYmin_.setText(new DecimalFormat("0.00").format(_bornes[2]));
    tfYmax_.setText(new DecimalFormat("0.00").format(_bornes[3]));
  }

  /**
   * Retourne les bornes min/max.
   * @return
   */
  public double[] getBornes() {
    double[] r=null;
    try {
      double xmin=Double.parseDouble(tfXmin_.getText());
      double xmax=Double.parseDouble(tfXmax_.getText());
      double ymin=Double.parseDouble(tfYmin_.getText());
      double ymax=Double.parseDouble(tfYmax_.getText());
      r=new double[]{xmin,xmax,ymin,ymax};
    }
    catch (NumberFormatException _exc) {}

    return r;
  }

  /**
   * Pour tester l'ergonomie
   */
  public static void main(String[] _args) {
    Hydraulique1dBornesGrapheEditor ed=new Hydraulique1dBornesGrapheEditor(null);
    ed.setVisible(true);
    System.exit(0);
  }
}
