package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierConcentrationInitiale;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresModeleQualiteEau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.dodico.hydraulique1d.conv.ConvH1D_Masc;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneConcentrationsInitialesTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauConcInitsModel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur des concentrations initiales .<br>
 * Appeler si l'utilisateur clic sur le bouton "Concentrations initiales" du menu qualit� d'eau.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().CONCENTRATIONS_INITIALES().editer().<br>
 * @version      $Revision: 1.5 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Olivier Pasteur
 */
public class Hydraulique1dConcentrationsInitialesEditor extends
        Hydraulique1dCustomizerImprimable implements ActionListener {
    private BuPanel pnLigneEau_, pnBas_;
    private BuBorderLayout loLigneEau_;
    private BuVerticalLayout loBas_;
    private BuScrollPane spLigneEau_;
    //private BuTextField tfNom_;
    private Hydraulique1dTableauReel tabConcInits_;
    private MetierEtude1d etude_;
    private MetierConcentrationInitiale[] concInits_;
    private MetierParametresModeleQualiteEau modeleQE_;
    public Hydraulique1dConcentrationsInitialesEditor() {
        this(null);
    }

    public Hydraulique1dConcentrationsInitialesEditor(BDialogContent parent) {
        super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Concentrations initiales"));
        concInits_ = null;
        modeleQE_ = null;
        etude_ = null;
        loBas_ = new BuVerticalLayout(5, true, true);
        loLigneEau_ = new BuBorderLayout(5, 5);
        Container pnMain_ = getContentPane();
        pnBas_ = new BuPanel();
        pnBas_.setLayout(loBas_);
        pnLigneEau_ = new BuPanel();
        pnLigneEau_.setLayout(loLigneEau_);
        pnLigneEau_.setBorder(
                new CompoundBorder(
                        new EtchedBorder(),
                        new EmptyBorder(new Insets(5, 5, 5, 5))));
        Hydraulique1dTableauConcInitsModel model = new
                Hydraulique1dTableauConcInitsModel();
        tabConcInits_ = new Hydraulique1dTableauReel(model);
        spLigneEau_ = new BuScrollPane(tabConcInits_);
        spLigneEau_.setBorder(
                new CompoundBorder(
                        new EtchedBorder(),
                        new EmptyBorder(new Insets(5, 5, 5, 5))));
        setActionPanel(
                EbliPreferences.DIALOG.CREER
                | EbliPreferences.DIALOG.SUPPRIMER
                | EbliPreferences.DIALOG.IMPORTER
                | EbliPreferences.DIALOG.EXPORTER);

        pnLigneEau_.add(spLigneEau_, BorderLayout.CENTER);
        pnLigneEau_.add(pnBas_, BorderLayout.SOUTH);
        pnMain_.add(pnLigneEau_, BorderLayout.CENTER);
        setNavPanel(EbliPreferences.DIALOG.VALIDER |
                    EbliPreferences.DIALOG.ANNULER);
        pack();
    }

  @Override
    public void actionPerformed(ActionEvent _evt) {
        String cmd = _evt.getActionCommand();
        if ("VALIDER".equals(cmd)) {
            if (getValeurs()) {
                firePropertyChange("concentrationsInitiales", null, concInits_);
            }
            fermer();
        } else if ("CREER".equals(cmd)) {
            creer();
        } else if ("SUPPRIMER".equals(cmd)) {
            supprimer();
        } else if ("IMPORTER".equals(cmd)) {
            importer();
        } else if ("EXPORTER".equals(cmd)) {
            exporter();
        } else {
            super.actionPerformed(_evt);
        }
    }

  @Override
    public void setObject(MetierHydraulique1d _n) {
        if (_n instanceof MetierEtude1d) {
            MetierEtude1d etude = (MetierEtude1d) _n;
            if (etude == etude_) {
                return;
            }
            etude_ = etude;
            MetierConcentrationInitiale[] concInits = etude_.qualiteDEau().
                                                 concentrationsInitiales();
            if (concInits == concInits_) {
                return;
            }
            concInits_ = concInits;
            MetierParametresModeleQualiteEau modeleQE = etude_.qualiteDEau().
                    parametresModeleQualiteEau();
            if (modeleQE == modeleQE_) {
                return;
            }
            modeleQE_ = modeleQE;
            setValeurs();
        } else {
            return;
        }

    }

    /**
     * La methode centrale qui permet d'imprimer (idem que celle de l'interface
     * printable). Le format <code>_format</code> sera celui donne par la methode
     * <code>Pageable.getPageFormat(int)</code>.
     * @param _g Graphics
     * @param _format PageFormat
     * @param _numPage int
     * @return <code>Printable.PAGE_EXISTS</code> si la page existe, sinon
     *   <code>Printable.NO_SUCH_PAGE</code>.
     */
  @Override
    public int print(Graphics _g, PageFormat _format, int _numPage) {
        return EbliPrinter.printComponent(_g, _format, tabConcInits_, true,
                                          _numPage);
    }

    public void cut() {
        tabConcInits_.cut();
    }

    public void copy() {
        tabConcInits_.copy();
    }

    public void paste() {
        tabConcInits_.paste();
    }

    public void duplicate() {
        tabConcInits_.duplicate();
    }

  @Override
    protected boolean getValeurs() {
        boolean changed = false;
        Hydraulique1dTableauConcInitsModel model = (
                Hydraulique1dTableauConcInitsModel) tabConcInits_.getModel();
        if (model.getValeurs()) {
            changed = true;
        }
        return changed;
    }

  @Override
    protected void setValeurs() {
        Hydraulique1dTableauConcInitsModel model = (
                Hydraulique1dTableauConcInitsModel) tabConcInits_.getModel();
        model.setValeurs(etude_.qualiteDEau());

    }


    private void importer() {
        File file = Hydraulique1dImport.chooseFile("conc");
        if (file == null) {
            return;
        }

        String[] nomVarConcs = new String[modeleQE_.nbTraceur()];
        for (int i = 0; i < modeleQE_.vvNomTracer().length; i++) {
            nomVarConcs[i]= modeleQE_.vvNomTracer()[i][0];
        }
        Hydraulique1dLigneConcentrationsInitialesTableau[] concInitsPoint =
                Hydraulique1dImport.importConcentrationsInitiales_Opt(file,nomVarConcs);
        if ((concInitsPoint == null)) {
            new BuDialogError(
                    (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                    ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                    .getInformationsSoftware(),
                    getS("ERREUR")+": "+getS("l'importation des concentrations initiales")+"\n" +
                    getS("a �chou�."))
                    .activate();
            return;
        }
        if ((concInitsPoint.length == 0)) {
            new BuDialogError(
                    (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                    ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                    .getInformationsSoftware(),
                    getS("ERREUR")+": "+getS("aucun point n'est")+"\n" +
                    getS("disponible dans cette import!"))
                    .activate();
            return;
        }
        estimationNumeroBief(concInitsPoint);
        ((Hydraulique1dTableauConcInitsModel) tabConcInits_.getModel()).
                setTabLignes(concInitsPoint);
    }

    private void exporter() {

        File fichier = Hydraulique1dExport.chooseFile("conc");
        if (fichier == null) {
            return;
        }

        //Conversion
        if (fichier == null) {
            return;
        }
        List listePts = ((Hydraulique1dTableauConcInitsModel) tabConcInits_.
                         getModel()).getListePtsComplets();
        MetierConcentrationInitiale[] concs = new MetierConcentrationInitiale[listePts.
                                         size()];
        Notifieur.getNotifieur().setEventMuet(true);
        for (int i = 0; i < concs.length; i++) {
            concs[i] = new MetierConcentrationInitiale();
            ((Hydraulique1dLigneConcentrationsInitialesTableau) listePts.get(i)).
                    setDConcentrationInitiale(concs[i]);
        }

        MetierResultatsTemporelSpatial ires = ConvH1D_Masc.convertitDConcentrationInitiale_IResultatTemporelSpatial(concs,
                modeleQE_.vvNomTracer());
        Notifieur.getNotifieur().setEventMuet(false);

        Hydraulique1dExport.exportOpthyca(fichier, ires);
    }


    private void estimationNumeroBief(Hydraulique1dLigneConcentrationsInitialesTableau[]
                                      pts) {

        Arrays.sort(pts);

        if (etude_.paramGeneraux().profilsAbscAbsolu()) {
            return;
        }

        boolean presenceAbscissesDouteuses = false;
        for (int i = 0; i < pts.length; i++) {
            MetierBief b = etude_.reseau().getBiefContenantAbscisse(pts[i].
                    ListeDoubles()[0]);
            if (b == null) {
                presenceAbscissesDouteuses = true;
            } else {
                if (pts[i].entier() != null) {
                    if (pts[i].i() != (b.indice() + 1)) {
                        presenceAbscissesDouteuses = true;
                    }
                } else {
                    return;
                }
            }
        }

        int choix = JOptionPane.showConfirmDialog(pnLigneEau_, getS("Certaines abscisses n'appartiennent pas au bief indiqu� !")+"\n"+
                                                  getS(" Souhaitez-vous effectuer une conversion en abscisses relatives ?"),
                                                  getS("Attention !"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE);
        if (choix == JOptionPane.YES_OPTION) {

            //on essaye de convertir en relatif
            if (presenceAbscissesDouteuses) {
                double abscisseRelative;
                double abscisseAbsolueDebutBief = 0;
                int biefCourant = -9999;
                for (int i = 0; i < pts.length; i++) {
                    if (biefCourant != pts[i].i()) {
                        biefCourant = pts[i].i();
                        abscisseAbsolueDebutBief = pts[i].ListeDoubles()[0];
                    }
                    abscisseRelative = etude_.reseau().getAbscisseRelative(pts[i].i(),
                            pts[i].ListeDoubles()[0], abscisseAbsolueDebutBief);
                    pts[i].ListeDoubles(0, abscisseRelative);
                }
            }

        }
    }
    private void creer() {
        tabConcInits_.ajouterLigne();
    }

    private void supprimer() {
        tabConcInits_.supprimeLignesSelectionnees();
    }
}
