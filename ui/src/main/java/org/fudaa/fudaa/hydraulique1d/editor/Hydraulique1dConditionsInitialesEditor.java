/*
 * @file         Hydraulique1dConditionsInitialesEditor.java
 * @creation     2000-12-12
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierConditionsInitiales;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresReprise;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des conditions générales (MetierConditionsInitiales).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Conditions Initiales".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().CONDITION_INITIALE().editer().<br>
 * @version      $Revision: 1.14 $ $Date: 2007-11-20 11:42:49 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dConditionsInitialesEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  public BuButton btZonesSeches_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("ZONES SECHES"));
  public BuButton btLigneInitiale_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("LIGNE D'EAU INITIALE"));
  public BuButton btFichierReprise_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("FICHIER REPRISE"));
  public BuButton btVisuInit_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("VISUALISER"));
  BuTextField tfFichierReprise_, tfLigInitiale_;
  BuRadioButton rbRepriseOui_,
    rbRepriseNon_,
    rbLigInitialeOui_,
    rbLigInitialeNon_;
  BuPanel pnCondInitiales_, pnLigneEau_, pnRepReprise_, pnBtVisuReprise_;
  BuPanel pnRepRepriseOui_,
    pnRepRepriseNon_,
    pnRepLigInitiale_,
    pnRepLigInitialeOui_,
    pnRepLigInitialeOuiNomBt_;
  BuPanel pnLigRepriseOui_,
    pnLigRepriseNon_,
    pnLigLigInitialeOui_,
    pnRepLigInitialeOuiNom_;
  BuVerticalLayout loCondInitiales_,
    loRepReprise_,
    loRepLigInitiale_,
    loRepLigInitialeOuiNomBt_;
  BuHorizontalLayout loLigneEau_,
    loRepRepriseOui_,
    loRepRepriseNon_,
    loRepLigInitialeOui_,
    loRepLigInitialeOuiNom_;
  FlowLayout loBtVisuReprise_;
  private MetierConditionsInitiales conditionsInitiales_;
  private MetierParametresGeneraux parametresGeneraux_;
  public Hydraulique1dConditionsInitialesEditor() {
    this(null);
  }
  public Hydraulique1dConditionsInitialesEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Conditions Initiales"));
    conditionsInitiales_= null;
    loLigneEau_= new BuHorizontalLayout(5, false, true);
    loRepRepriseOui_= new BuHorizontalLayout(5, false, true);
    loRepRepriseNon_= new BuHorizontalLayout(5, false, true);
    loRepLigInitialeOui_= new BuHorizontalLayout(5, false, true);
    loRepLigInitialeOuiNom_= new BuHorizontalLayout(5, false, true);
    loCondInitiales_= new BuVerticalLayout(5, false, true);
    loRepReprise_= new BuVerticalLayout(5, false, true);
    loRepLigInitiale_= new BuVerticalLayout(5, false, true);
    loRepLigInitialeOuiNomBt_= new BuVerticalLayout(5, false, true);
    loBtVisuReprise_= new FlowLayout(FlowLayout.CENTER, 10, 10);
    Container pnMain_= getContentPane();
    pnLigneEau_= new BuPanel();
    pnLigneEau_.setLayout(loLigneEau_);
    CompoundBorder bdLigneEau=
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbLigneEau= new TitledBorder(bdLigneEau, Hydraulique1dResource.HYDRAULIQUE1D.getString("Ligne d'eau"));
    pnLigneEau_.setBorder(tbLigneEau);
    pnRepReprise_= new BuPanel();
    pnRepReprise_.setLayout(loRepReprise_);
    pnRepReprise_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnRepRepriseOui_= new BuPanel();
    pnRepRepriseOui_.setLayout(loRepRepriseOui_);
    pnRepRepriseNon_= new BuPanel();
    pnRepRepriseNon_.setLayout(loRepRepriseNon_);
    pnRepLigInitiale_= new BuPanel();
    pnRepLigInitiale_.setLayout(loRepLigInitiale_);
    pnRepLigInitiale_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnRepLigInitialeOui_= new BuPanel();
    pnRepLigInitialeOui_.setLayout(loRepLigInitialeOui_);
    pnRepLigInitialeOuiNomBt_= new BuPanel();
    pnRepLigInitialeOuiNomBt_.setLayout(loRepLigInitialeOuiNomBt_);
    pnRepLigInitialeOuiNom_= new BuPanel();
    pnRepLigInitialeOuiNom_.setLayout(loRepLigInitialeOuiNom_);
    pnLigLigInitialeOui_= new BuPanel();
    pnLigLigInitialeOui_.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    pnLigRepriseOui_= new BuPanel();
    pnLigRepriseOui_.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    pnLigRepriseNon_= new BuPanel();
    pnLigRepriseNon_.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    pnBtVisuReprise_= new BuPanel();
    pnBtVisuReprise_.setLayout(loBtVisuReprise_);
    pnCondInitiales_= new BuPanel();
    pnCondInitiales_.setLayout(loCondInitiales_);
    pnCondInitiales_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    rbRepriseOui_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Oui"));
    rbRepriseOui_.setActionCommand("REPRISE_OUI");
    rbRepriseOui_.addActionListener(this);
    tfFichierReprise_= new BuTextField("");
    tfFichierReprise_.setColumns(8);
    tfFichierReprise_.setEditable(false);
    btFichierReprise_.setActionCommand("FICHIER_REPRISE");
    int n= 0;
    pnRepRepriseOui_.add(rbRepriseOui_, n++);
    pnRepRepriseOui_.add(pnLigRepriseOui_, n++);
    pnRepRepriseOui_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier de reprise")), n++);
    pnRepRepriseOui_.add(tfFichierReprise_, n++);
    pnRepRepriseOui_.add(btFichierReprise_, n++);
    rbLigInitialeOui_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Oui"));
    rbLigInitialeOui_.setActionCommand("LIGNE_INITIALE_OUI");
    rbLigInitialeOui_.addActionListener(this);
    tfLigInitiale_= new BuTextField("");
    tfLigInitiale_.setColumns(8);
    tfLigInitiale_.setEditable(false);
    n= 0;
    pnRepLigInitialeOuiNom_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nom ligne d'eau")), n++);
    pnRepLigInitialeOuiNom_.add(tfLigInitiale_, n++);
    btLigneInitiale_.setActionCommand("LIGNE_INITIALE");
    n= 0;
    pnRepLigInitialeOuiNomBt_.add(pnRepLigInitialeOuiNom_, n++);
    pnRepLigInitialeOuiNomBt_.add(btLigneInitiale_, n++);
    n= 0;
    pnRepLigInitialeOui_.add(rbLigInitialeOui_, n++);
    pnRepLigInitialeOui_.add(pnLigLigInitialeOui_, n++);
    pnRepLigInitialeOui_.add(pnRepLigInitialeOuiNomBt_, n++);
    rbLigInitialeNon_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Non"));
    rbLigInitialeNon_.setActionCommand("LIGNE_INITIALE_NON");
    rbLigInitialeNon_.addActionListener(this);
    n= 0;
    pnRepLigInitiale_.add(pnRepLigInitialeOui_, n++);
    pnRepLigInitiale_.add(rbLigInitialeNon_, n++);
    rbRepriseNon_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Non"));
    rbRepriseNon_.setActionCommand("REPRISE_NON");
    rbRepriseNon_.addActionListener(this);
    n= 0;
    pnRepRepriseNon_.add(rbRepriseNon_, n++);
    pnRepRepriseNon_.add(pnLigRepriseNon_, n++);
    pnRepRepriseNon_.add(
      new BuLabelMultiLine(Hydraulique1dResource.HYDRAULIQUE1D.getString("Présence d'une")+" \n"+
                           Hydraulique1dResource.HYDRAULIQUE1D.getString("ligne d'eau initiale ?")),
      n++);
    pnRepRepriseNon_.add(pnRepLigInitiale_, n++);
    n= 0;
    pnRepReprise_.add(pnRepRepriseOui_, n++);
    pnRepReprise_.add(pnRepRepriseNon_, n++);
    n= 0;
    pnLigneEau_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Reprise de calcul ?")), n++);
    pnLigneEau_.add(pnRepReprise_, n++);
    btZonesSeches_.setActionCommand("ZONES_SECHES");
    btVisuInit_.setActionCommand("VISU_COND_INIT");
    n= 0;
    pnBtVisuReprise_.add(btZonesSeches_, n++);
    pnBtVisuReprise_.add(btVisuInit_, n++);
    int larg= pnLigneEau_.getPreferredSize().width;
    int haut= pnBtVisuReprise_.getPreferredSize().height;
    pnBtVisuReprise_.setPreferredSize(new Dimension(larg, haut));
    n= 0;
    pnCondInitiales_.add(pnLigneEau_, n++);
    pnCondInitiales_.add(pnBtVisuReprise_, n++);
    BuScrollPane scrlpEditor= new BuScrollPane(pnCondInitiales_);
    pnMain_.add(scrlpEditor, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
    if (e.getSource() instanceof MetierLigneEauInitiale) {
      if ("nom".equals(e.getChamp()) ){
        MetierLigneEauInitiale iLigne= (MetierLigneEauInitiale)e.getSource();
        tfLigInitiale_.setText(iLigne.nom());
      }
    }
    else if (e.getSource() instanceof MetierParametresReprise) {
      if ("fichier".equals(e.getChamp()) ){
        MetierParametresReprise rep = (MetierParametresReprise)e.getSource();
        tfFichierReprise_.setText(rep.fichier());
      }
    }
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("ANNULER".equals(cmd)) {
      fermer();
    } else if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("conditionsInitiales", null, conditionsInitiales_);
      }
      fermer();
    } else if ("REPRISE_OUI".equals(cmd)) {
      rbRepriseOui_.setSelected(true);
      tfFichierReprise_.setEnabled(true);
      btFichierReprise_.setEnabled(true);
      rbRepriseNon_.setSelected(false);
      rbLigInitialeOui_.setEnabled(false);
      rbLigInitialeNon_.setEnabled(false);
      tfLigInitiale_.setEnabled(false);
      btLigneInitiale_.setEnabled(false);
    } else if ("REPRISE_NON".equals(cmd)) {
      rbRepriseOui_.setSelected(false);
      tfFichierReprise_.setEnabled(false);
      btFichierReprise_.setEnabled(false);
      rbRepriseNon_.setSelected(true);
      rbLigInitialeOui_.setEnabled(true);
      rbLigInitialeNon_.setEnabled(true);
      if (rbLigInitialeOui_.isSelected()) {
        tfLigInitiale_.setEnabled(true);
        btLigneInitiale_.setEnabled(true);
      } else {
        tfLigInitiale_.setEnabled(false);
        btLigneInitiale_.setEnabled(false);
      }
    } else if ("LIGNE_INITIALE_OUI".equals(cmd)) {
      rbLigInitialeOui_.setSelected(true);
      tfLigInitiale_.setEnabled(true);
      btLigneInitiale_.setEnabled(true);
      rbLigInitialeNon_.setSelected(false);
    } else if ("LIGNE_INITIALE_NON".equals(cmd)) {
      rbLigInitialeOui_.setSelected(false);
      tfLigInitiale_.setEnabled(false);
      btLigneInitiale_.setEnabled(false);
      rbLigInitialeNon_.setSelected(true);
    }
  }
  public BuTextField getTextFieldFichierReprise() {
    return tfFichierReprise_;
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _iobjet) {
    if (_iobjet instanceof MetierParametresGeneraux) {
        parametresGeneraux_ = (MetierParametresGeneraux) _iobjet;
    }
    else if (_iobjet instanceof MetierConditionsInitiales) {
        conditionsInitiales_ = (MetierConditionsInitiales) _iobjet;
        setValeurs();
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (conditionsInitiales_ == null)
      return changed;
    if (rbRepriseOui_.isSelected()) {
      if (conditionsInitiales_.paramsReprise() == null) {
        changed= true;
        conditionsInitiales_.creeParamsReprise();
      }
    } else { // pas reprise
      if (conditionsInitiales_.paramsReprise() != null) {
        changed= true;
        conditionsInitiales_.paramsReprise(null);
      }
    }
    if (rbLigInitialeOui_.isSelected()) {
      if (conditionsInitiales_.ligneEauInitiale() == null) {
        changed= true;
        conditionsInitiales_.creeLigneInitiale();
      } else {
        String nom= conditionsInitiales_.ligneEauInitiale().nom();
        if (!nom.equals(tfLigInitiale_.getText())) {
          changed= true;
          conditionsInitiales_.ligneEauInitiale().nom(tfLigInitiale_.getText());
        }
      }
    } else if (rbRepriseNon_.isSelected()) { //pas de ligne d'eau initiale
      if (conditionsInitiales_.ligneEauInitiale() != null) {
        changed= true;
        conditionsInitiales_.ligneEauInitiale(null);
      }
    }
    return changed;
  }
  @Override
  protected void setValeurs() {

     if (parametresGeneraux_.regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
         rbRepriseOui_.setEnabled(true);
         rbRepriseNon_.setEnabled(true);
         rbLigInitialeOui_.setEnabled(true);
         rbLigInitialeNon_.setEnabled(true);
     }
     if (parametresGeneraux_.regime().value() == EnumMetierRegime._FLUVIAL_NON_PERMANENT) {
         conditionsInitiales_.paramsReprise(null);
         rbRepriseOui_.setSelected(false);
         rbRepriseNon_.setSelected(true);
         rbRepriseOui_.setEnabled(false);
         rbRepriseNon_.setEnabled(false);
         rbLigInitialeOui_.setEnabled(true);
         rbLigInitialeNon_.setEnabled(true);

     }
     if (parametresGeneraux_.regime().value() == EnumMetierRegime._FLUVIAL_PERMANENT) {
         conditionsInitiales_.paramsReprise(null);
         rbRepriseOui_.setSelected(false);
         rbRepriseNon_.setSelected(true);
         rbRepriseOui_.setEnabled(false);
         rbRepriseNon_.setEnabled(false);
         conditionsInitiales_.ligneEauInitiale(null);
         rbLigInitialeOui_.setSelected(false);
         rbLigInitialeNon_.setSelected(true);
         rbLigInitialeOui_.setEnabled(false);
         rbLigInitialeNon_.setEnabled(false);
     }


    if (conditionsInitiales_.paramsReprise() != null) {
        rbRepriseOui_.setSelected(true);
        rbRepriseNon_.setSelected(false);
        tfFichierReprise_.setText(conditionsInitiales_.paramsReprise().fichier());
        tfFichierReprise_.setEnabled(true);
        btFichierReprise_.setEnabled(true);
        rbLigInitialeOui_.setEnabled(false);
        rbLigInitialeNon_.setEnabled(false);
        tfLigInitiale_.setEnabled(false);
        btLigneInitiale_.setEnabled(false);

    }  else {
        rbRepriseOui_.setSelected(false);
        rbRepriseNon_.setSelected(true);
        tfFichierReprise_.setEnabled(false);
        btFichierReprise_.setEnabled(false);
        tfFichierReprise_.setText("");

        if (conditionsInitiales_.ligneEauInitiale() == null) {
            rbLigInitialeOui_.setSelected(false);
            rbLigInitialeNon_.setSelected(true);
            tfLigInitiale_.setText("");
            tfLigInitiale_.setEnabled(false);
            btLigneInitiale_.setEnabled(false);
        } else {
            rbLigInitialeOui_.setEnabled(true);
            rbLigInitialeNon_.setEnabled(true);
            rbLigInitialeOui_.setSelected(true);
            rbLigInitialeNon_.setSelected(false);
            tfLigInitiale_.setText(conditionsInitiales_.ligneEauInitiale().nom());
            tfLigInitiale_.setEnabled(true);
            btLigneInitiale_.setEnabled(true);

        }
    }
  }
}
