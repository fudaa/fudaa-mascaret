/*
 * @file         Hydraulique1dCrueCalagePanel.java
 * @creation     2000-11-29
 * @modification $Date: 2007-11-20 11:42:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierApportCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierMesureCrueCalageAuto;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneReelTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReelModel;

import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Panneau d'�dition d'une crue pour le calage.<br>
 * @version      $Revision: 1.4 $ $Date: 2007-11-20 11:42:43 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class Hydraulique1dCrueCalagePanel  extends BuPanel {
  private JPanel pnTableaux = new JPanel();
  private BorderLayout lyThis = new BorderLayout();
  private GridLayout lyTableau = new GridLayout();
  private BuPanel pnLimites = new BuPanel();
  private BuHorizontalLayout lyLimites = new BuHorizontalLayout();
  private BuLabel lbQAmont = new BuLabel();
  private BuTextField tfQAmont = BuTextField.createDoubleField();
  private BuLabel lbZAval = new BuLabel();
  private BuTextField tfZAval = BuTextField.createDoubleField();
  private JScrollPane spApports = new JScrollPane();
  private JScrollPane spMesures = new JScrollPane();
  private JPanel pnApports = new JPanel();
  private JPanel pnMesures = new JPanel();
  private BorderLayout lyApports = new BorderLayout();
  private BorderLayout lyMesures = new BorderLayout();
  private Hydraulique1dTableauReel tbApports=new Hydraulique1dTableauReel();
  private Hydraulique1dTableauReel tbMesures=new Hydraulique1dTableauReel();

  private MetierReseau reseau_;

  public Hydraulique1dCrueCalagePanel() {
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    tbApports.setModel(new Hydraulique1dTableauReelModel(new String[]{Hydraulique1dResource.HYDRAULIQUE1D.getString("Abscisse (m)"),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("D�bit (m3/s)")},20));
    tbMesures.setModel(new Hydraulique1dTableauReelModel(new String[]{Hydraulique1dResource.HYDRAULIQUE1D.getString("Abscisse (m)"),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("Cote (m)"),Hydraulique1dResource.HYDRAULIQUE1D.getString("Pond�ration")},20));
  }

  private void jbInit() throws Exception {
    this.setLayout(lyThis);
    pnTableaux.setLayout(lyTableau);
    lyTableau.setColumns(2);
    pnLimites.setLayout(lyLimites);
    lbQAmont.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Q amont"));
    lyLimites.setHgap(5);
    tfQAmont.setPreferredSize(new Dimension(70, 20));
    tfQAmont.setText("");
    lbZAval.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Z aval"));
    tfZAval.setPreferredSize(new Dimension(70, 20));
    pnLimites.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnApports.setLayout(lyApports);
    pnApports.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.
      white, new Color(165, 163, 151)), Hydraulique1dResource.HYDRAULIQUE1D.getString("Apports")));
    pnMesures.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.
      white, new Color(165, 163, 151)), Hydraulique1dResource.HYDRAULIQUE1D.getString("Mesures")));
    pnMesures.setLayout(lyMesures);
    spApports.setPreferredSize(new Dimension(250, 250));
    spMesures.setPreferredSize(new Dimension(250, 250));
    this.add(pnTableaux, java.awt.BorderLayout.CENTER);
    pnTableaux.add(pnApports);
    pnTableaux.add(pnMesures);
    pnMesures.add(spMesures, java.awt.BorderLayout.CENTER);
    spMesures.getViewport().add(tbMesures);
    this.add(pnLimites, java.awt.BorderLayout.NORTH);
    pnLimites.add(lbQAmont);
    pnLimites.add(tfQAmont);
    pnLimites.add(lbZAval);
    pnLimites.add(tfZAval);
    pnApports.add(spApports, java.awt.BorderLayout.CENTER);
    spApports.getViewport().add(tbApports);
  }

  /**
   * Cr�ation d'un nouvel apport
   */
  public void ajouterApport() {
    Hydraulique1dTableauReelModel md=(Hydraulique1dTableauReelModel)tbApports.getModel();
    md.ajouterLigne();
  }

  /**
   * Suppression de l'apport s�lectionn�.
   */
  public void supprimerApport() {
    Hydraulique1dTableauReelModel md=(Hydraulique1dTableauReelModel)tbApports.getModel();
    int[] sels=tbApports.getSelectedRows();
    for (int i=sels.length-1; i>=0; i--) {
      md.supprimerLigne(sels[i]);
    }
  }

  /**
   * Cr�ation d'une nouvelle mesure
   */
  public void ajouterMesure() {
    Hydraulique1dTableauReelModel md=(Hydraulique1dTableauReelModel)tbMesures.getModel();
    md.ajouterLigne();
  }

  /**
   * Suppression de l'apport s�lectionn�.
   */
  public void supprimerMesure() {
    Hydraulique1dTableauReelModel md=(Hydraulique1dTableauReelModel)tbMesures.getModel();
    int[] sels=tbMesures.getSelectedRows();
    for (int i=sels.length-1; i>=0; i--) {
      md.supprimerLigne(sels[i]);
    }
  }

  /**
   * Controle que le panneau est correctement rempli.
   * @return Un message indiquant l'origine du pb. null si pas de pb.
   */
  public String isValide() {
    if (tfQAmont.getValue()==null) return Hydraulique1dResource.HYDRAULIQUE1D.getString("Q amont non rempli");
    if (tfZAval.getValue()==null) return Hydraulique1dResource.HYDRAULIQUE1D.getString("Z aval non rempli");

    Hydraulique1dTableauReelModel md;
    java.util.List ls;
    double smin=reseau_.biefs()[0].getXOrigine();
    double smax=reseau_.biefs()[0].getXFin();

    // Apports

    md=(Hydraulique1dTableauReelModel)tbApports.getModel();
    ls=md.getListePtsComplets();
    // Les lignes avec certaines valeurs non remplies contiennent aussi les lignes avec toutes les valeurs remplies.
    if (md.getListePtsPasToutVide().size()!=ls.size())
      return Hydraulique1dResource.HYDRAULIQUE1D.getString("Tableau des apports")+" : "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Certaines lignes ne sont pas enti�rement remplies");

    // Limites de bief. Les abscisses doivent �tre inclus dans le bief.
    for (int i=0; i<ls.size(); i++) {
      Hydraulique1dLigneReelTableau lig=(Hydraulique1dLigneReelTableau)ls.get(i);
      double s=lig.getValue(0).doubleValue();
      if(s<smin || s>smax)
        return Hydraulique1dResource.HYDRAULIQUE1D.getString("Tableau des apports")+" : "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Certaines abscisses sont hors bief");
    }

    // Mesures

    md=(Hydraulique1dTableauReelModel)tbMesures.getModel();
    // Les lignes avec certaines valeurs non remplies contiennent aussi les lignes avec toutes les valeurs remplies.
    ls=md.getListePtsComplets();
    if (md.getListePtsPasToutVide().size()!=ls.size())
      return Hydraulique1dResource.HYDRAULIQUE1D.getString("Tableau des mesures")+" : "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Certaines lignes ne sont pas enti�rement remplies");
    if (ls.size()<1)
      return Hydraulique1dResource.HYDRAULIQUE1D.getString("Tableau des mesures")+" : "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Aucune donn�e");

    // Limites de bief. Les abscisses doivent �tre inclus dans le bief.
    for (int i=0; i<ls.size(); i++) {
      Hydraulique1dLigneReelTableau lig=(Hydraulique1dLigneReelTableau)ls.get(i);
      double s=lig.getValue(0).doubleValue();
      if(s<smin || s>smax)
        return Hydraulique1dResource.HYDRAULIQUE1D.getString("Tableau des mesures")+" : "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Certaines abscisses sont hors bief");
    }

    return null;
  }

  /**
   * Controle que le panneau est totalement vide.
   */
  public boolean isVide() {
    if (tfQAmont.getValue()!=null) return false;
    if (tfZAval.getValue()!=null) return false;

    Hydraulique1dTableauReelModel md;
    md=(Hydraulique1dTableauReelModel)tbApports.getModel();
    if (!md.getListePtsPasToutVide().isEmpty()) return false;

    md=(Hydraulique1dTableauReelModel)tbMesures.getModel();
    if (!md.getListePtsPasToutVide().isEmpty()) return false;

    return true;
  }

  /**
   * Affecte le reseau.
   */
  public void setReseau(MetierReseau _res) {
    reseau_=_res;
  }

  /**
   * Affecte la crue.
   */
  public void setCrue(MetierCrueCalageAuto _crue) {
    // Crue nulle : Clear du panneau.
    if (_crue==null) {
      tfQAmont.setValue(null);
      tfZAval.setValue(null);

      Hydraulique1dTableauReelModel md;
      double[][] vals=new double[0][0];

      // Apports
      md=(Hydraulique1dTableauReelModel)tbApports.getModel();
      md.setTabDouble(vals);
      // Mesures
      md=(Hydraulique1dTableauReelModel)tbMesures.getModel();
      md.setTabDouble(vals);

      return;
    }

    tfQAmont.setValue(new Double(_crue.debitAmont()));
    tfZAval.setValue(new Double(_crue.coteAval()));

    Hydraulique1dTableauReelModel md;
    double[][] vals;

    // Apports
    md=(Hydraulique1dTableauReelModel)tbApports.getModel();
    MetierApportCrueCalageAuto[] apports=_crue.apports();
    vals=new double[apports.length][2];
    for (int i=0; i<apports.length; i++) {
      vals[i][0]=apports[i].abscisse();
      vals[i][1]=apports[i].debit();
    }
    md.setTabDouble(vals);

    // Mesures
    md=(Hydraulique1dTableauReelModel)tbMesures.getModel();
    MetierMesureCrueCalageAuto[] mesures=_crue.mesures();
    vals=new double[mesures.length][3];
    for (int i=0; i<mesures.length; i++) {
      vals[i][0]=mesures[i].abscisse();
      vals[i][1]=mesures[i].cote();
      vals[i][2]=mesures[i].coefficient();
    }
    md.setTabDouble(vals);
  }

  /**
   * Retourne une crue.
   */
  public MetierCrueCalageAuto getCrue() {
    if (isValide()!=null) return null;

    MetierCrueCalageAuto crue=new MetierCrueCalageAuto();
    crue.debitAmont(((Double)tfQAmont.getValue()).doubleValue());
    crue.coteAval(((Double)tfZAval.getValue()).doubleValue());

    Hydraulique1dTableauReelModel md;
    double[][] vals;

    // Apports
    md=(Hydraulique1dTableauReelModel)tbApports.getModel();
    md.trier();
    vals=md.getTabDoubleComplet();
    MetierApportCrueCalageAuto[] apports=new MetierApportCrueCalageAuto[vals.length];
    for (int i=0; i<apports.length; i++) {
      apports[i]=new MetierApportCrueCalageAuto();
      apports[i].abscisse(vals[i][0]);
      apports[i].debit(vals[i][1]);
    }
    crue.apports(apports);

    // Mesures
    md=(Hydraulique1dTableauReelModel)tbMesures.getModel();
    md.trier();
    vals=md.getTabDoubleComplet();
    MetierMesureCrueCalageAuto[] mesures=new MetierMesureCrueCalageAuto[vals.length];
    for (int i=0; i<mesures.length; i++) {
      mesures[i]=new MetierMesureCrueCalageAuto();
      mesures[i].abscisse(vals[i][0]);
      mesures[i].cote(vals[i][1]);
      mesures[i].coefficient(vals[i][2]);
    }
    crue.mesures(mesures);

    return crue;
  }

}
