/*
 * @file         Hydraulique1dCruesCalageEditor.java
 * @creation     2000-12-07
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCrueCalageAuto;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuResource;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur des crues de calage.<br>
 * Appel� si l'utilisateur clic sur le menu "Calage/Crues Calage".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().CRUES_CALAGE().editer().<br>
 * @version      $Revision: 1.4 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class Hydraulique1dCruesCalageEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {

  private MetierEtude1d param_;
  private JTabbedPane tpPrincipal_;
  private int icrueCourante_;

  public Hydraulique1dCruesCalageEditor() {
    this(null);
  }

  public Hydraulique1dCruesCalageEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Donn�es de crues"));

    tpPrincipal_=new JTabbedPane();
    tpPrincipal_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Crue")+" 1",new Hydraulique1dCrueCalagePanel());
    tpPrincipal_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Crue")+" 2",new Hydraulique1dCrueCalagePanel());
    tpPrincipal_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Crue")+" 3",new Hydraulique1dCrueCalagePanel());
    tpPrincipal_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Crue")+" 4",new Hydraulique1dCrueCalagePanel());
    tpPrincipal_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Crue")+" 5",new Hydraulique1dCrueCalagePanel());
    tpPrincipal_.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        tpPrincipal_stateChanged(e);
      }
    });
    getContentPane().add(tpPrincipal_,BorderLayout.CENTER);
    icrueCourante_=tpPrincipal_.getSelectedIndex();

    param_= null;
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Cr�er apport"),BuResource.BU.getIcon("creer"),"CREER_APPORT");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Supprimer apport"),BuResource.BU.getIcon("detruire"),"SUPPRIMER_APPORT");
    JLabel lbDummy=new JLabel();
    lbDummy.setPreferredSize(new Dimension(50,10));  // S�parateur de boutons.
    getActionPanel().add(lbDummy);
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Cr�er mesure"),BuResource.BU.getIcon("creer"),"CREER_MESURE");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Supprimer mesure"),BuResource.BU.getIcon("detruire"),"SUPPRIMER_MESURE");
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }

  /**
   * On autorise de passer a une autre crue si :
   * - La courante est correctement remplie.
   * - La courante est vide, mais l'indice courant est sup�rieur � l'indice s�lectionn�.
   * @param _evt ChangeEvent
   */
  private void tpPrincipal_stateChanged(ChangeEvent _evt) {
    int icrueSelected=tpPrincipal_.getSelectedIndex();

    for (int i=0; i<icrueSelected; i++) {
      Hydraulique1dCrueCalagePanel crue=(Hydraulique1dCrueCalagePanel)tpPrincipal_.getComponent(i);
      String mes=crue.isValide();
      if (mes!=null) {
        JOptionPane.showMessageDialog(this,
         Hydraulique1dResource.HYDRAULIQUE1D.getString("Crue ")+(i+1)+" : "+mes+".\n"+
         Hydraulique1dResource.HYDRAULIQUE1D.getString("Vous ne pouvez d�finir la crue ")+(icrueSelected+1)+".",
         Hydraulique1dResource.HYDRAULIQUE1D.getString("Erreur de mise en donn�es"),
          JOptionPane.ERROR_MESSAGE);

        tpPrincipal_.setSelectedIndex(icrueCourante_);
        break;
      }
    }
    icrueCourante_=tpPrincipal_.getSelectedIndex();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (!isValide()) return;
      if (getValeurs()) {
        firePropertyChange("crues", null, param_);
      }
      fermer();
    }
    else if ("CREER_APPORT".equals(cmd)) {
      Hydraulique1dCrueCalagePanel pn=
        (Hydraulique1dCrueCalagePanel)tpPrincipal_.getSelectedComponent();
      pn.ajouterApport();
    }
    else if ("SUPPRIMER_APPORT".equals(cmd)) {
      Hydraulique1dCrueCalagePanel pn=
        (Hydraulique1dCrueCalagePanel)tpPrincipal_.getSelectedComponent();
      pn.supprimerApport();
    }
    else if ("CREER_MESURE".equals(cmd)) {
      Hydraulique1dCrueCalagePanel pn=
        (Hydraulique1dCrueCalagePanel)tpPrincipal_.getSelectedComponent();
      pn.ajouterMesure();
    }
    else if ("SUPPRIMER_MESURE".equals(cmd)) {
      Hydraulique1dCrueCalagePanel pn=
        (Hydraulique1dCrueCalagePanel)tpPrincipal_.getSelectedComponent();
      pn.supprimerMesure();
    }
    else {
//      Notifieur.getNotifieur().setEventMuet(true);
//      MetierCrueCalageAuto ap=new MetierCrueCalageAuto();
//      Notifieur.getNotifieur().setEventMuet(false);
//      UsineLib.findUsine().supprimeHydraulique1dCrueCalageAuto(ap);

      super.actionPerformed(_evt);
    }
  }

  @Override
  protected boolean getValeurs() {
//    boolean changed= false;

    Vector vcrues=new Vector();
    for (int i=0; i<tpPrincipal_.getComponentCount(); i++) {
      Hydraulique1dCrueCalagePanel pn=(Hydraulique1dCrueCalagePanel)tpPrincipal_.getComponent(i);
      if (pn.isVide()) break;
      vcrues.add(pn.getCrue());
    }

    param_.calageAuto().crues((MetierCrueCalageAuto[])vcrues.toArray(new MetierCrueCalageAuto[vcrues.size()]));

    return true;
  }

  @Override
  public void setObject(MetierHydraulique1d _param) {
    if (!(_param instanceof MetierEtude1d)) return;
    if (_param==param_) return;

    param_= (MetierEtude1d)_param;

    for (int i=0; i<tpPrincipal_.getComponentCount(); i++) {
      Hydraulique1dCrueCalagePanel pn=(Hydraulique1dCrueCalagePanel)tpPrincipal_.getComponent(i);
      pn.setReseau(param_.reseau());
    }
  }

  @Override
  protected void setValeurs() {
    MetierCrueCalageAuto[] crues=param_.calageAuto().crues();

    for (int i=0; i<tpPrincipal_.getComponentCount(); i++) {
      Hydraulique1dCrueCalagePanel pn=
        (Hydraulique1dCrueCalagePanel)tpPrincipal_.getComponent(i);
      pn.setCrue(i<crues.length ? crues[i]:null);
    }
  }

  /**
   * Controle que les panneaux sont corrects
   */
  private boolean isValide() {
    for (int i=0; i<tpPrincipal_.getComponentCount(); i++) {
      Hydraulique1dCrueCalagePanel pn=(Hydraulique1dCrueCalagePanel)tpPrincipal_.getComponent(i);
      if (pn.isVide()) break;
      String mes=pn.isValide();
      if (mes!=null) {
        JOptionPane.showMessageDialog(this,
         Hydraulique1dResource.HYDRAULIQUE1D.getString("Crue ")+(i+1)+" : "+mes+".",
         Hydraulique1dResource.HYDRAULIQUE1D.getString("Erreur de mise en donn�es"),
          JOptionPane.ERROR_MESSAGE);
        return false;
      }
    }
    return true;
  }
}
