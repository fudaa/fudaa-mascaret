/*
 * @file         Hydraulique1dCustomizer.java
 * @creation     1999-04-30
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEventListener;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.IDialogInterface;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuCommonInterface;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Classe abstraite, m�re de tous les �diteurs et �couteur de tous les �v�nements m�tiers du mod�le hydraulique1D.<br>
 * Ainsi, toutes les classes filles sont �galement �couteurs des �v�nements m�tiers et devront r�-impl�menter les m�thodes suivantes suivant le besoins�:
 * <ul>
 *     <li> ��void objetCree(H1dObjetEvent e)��
 *     <li> ��void objetSupprime(H1dObjetEvent e)��
 *     <li> ��void objetModifie(H1dObjetEvent e)��
 * </ul><br><br>
 *
 * Les m�thodes abstraites � impl�menter sont�:
 * <ul>
 *     <li> ��void setObject(MetierHydraulique1d _n)�� qui permet de transf�rer le ou les objets Corba m�tier qui sont �dit�s.
 *     <li> ��void setValeurs()�� qui permet de actualiser l\u2019affichage � partir des objets m�tier pr�alablement transf�rer par la m�thode pr�c�dente. Cette m�thode est appel� par la m�thode ��show��.
 *     <li> ��boolean getValeurs()�� qui permet de mettre � jour le ou les objet m�tier � partir des modifications �ventuelles que l\u2019utilisateur aura effectu� sur les composants d\u2019affichages. De plus elle doit retourner le bool�en Faux si aucuns changements n\u2019a �t� effectu�s.
 * </ul><br><br>
 *
 * Une autre caract�ristique de ��Hydraulique1dCustomizer�� est qu\u2019il h�rite de la classe ��BDialogContent�� du projet Ebli.<br><br>
 *
 * Cette classe qui est un simple panneau, doit �tre consid�rer, en faite, comme une fen�tre qui peut, selon le besoin de l\u2019utilisateur,
 * �tre soit une fen�tre interne (��BuInternalFrame��), soit ou une fen�tre autonome (��JDialog��).<br>
 * De plus, en bas de la fen�tre, il existe 2 sous panneaux�: le panneau de navigation (le plus bas) et le panneau d\u2019action (au-dessus). <br>
 * Le premier permet d\u2019avoir des boutons aux choix pour valider, fermer, reculer, avancer, terminer et annuler. <br>
 * Le deuxi�me permet d\u2019avoir des boutons aux choix pour cr�er, supprimer, �diter, voir, importer et exporter.
 *
 * @version      $Revision: 1.14 $ $Date: 2007-11-20 11:42:45 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public abstract class Hydraulique1dCustomizer
  extends BDialogContent
  implements H1dObjetEventListener {
  protected Hydraulique1dCustomizer(BuCommonInterface app, String _title) {
    super(app, _title);
  }
  protected Hydraulique1dCustomizer(String _title) {
    super((BuCommonInterface)Hydraulique1dBaseApplication.FRAME, _title);
  }
  protected Hydraulique1dCustomizer(
    BuCommonInterface app,
    String _title,
    JComponent _message) {
    super(app, _title, _message);
  }
  protected Hydraulique1dCustomizer(String _title, JComponent _message) {
    super(
      (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
      _title,
      _message);
  }
  protected Hydraulique1dCustomizer(
    BuCommonInterface app,
    BDialogContent _parent,
    String _title) {
    super(app, _parent, _title);
  }
  protected Hydraulique1dCustomizer(BDialogContent _parent, String _title) {
    super(
      (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
      _parent,
      _title);
  }
  protected Hydraulique1dCustomizer(
    BuCommonInterface app,
    BDialogContent _parent,
    String _title,
    JComponent _message) {
    super(app, _parent, _title, _message);
  }
  protected Hydraulique1dCustomizer(
    BDialogContent _parent,
    String _title,
    JComponent _message) {
    super(
      (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
      _parent,
      _title,
      _message);
  }
  @Override
  protected void baseInit() {
    super.baseInit();
    setClosable(false);
    setModal(false);
    //
/**    evtSupport_= DObjetEventListenerSupport.createEventSupport();
    evtSupport_.clientListener(this);
    UsineLib.findUsine().addObjetEventListener((DHydraulique1dEventListenerSupport)evtSupport_.tie());
*/
    Notifieur.getNotifieur().addObjetEventListener(this);
  }
  public abstract void setObject(MetierHydraulique1d _n);
  @Override
  public void fermer() {
    super.fermer();
    firePropertyChange("fermer", null, null);
  }
  @Override
  public void show() {
    setValeurs();
    BuCommonImplementation fci = ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME).getImplementation();
    IDialogInterface dialog = super.getDialog();
    if (dialog instanceof JInternalFrame) {
        fci.activateInternalFrame((JInternalFrame) dialog);
    }
    else if (dialog instanceof JDialog) {
        JDialog dial = (JDialog) dialog;
        try {
            dial.toFront();
        } catch (Throwable ex) {
            System.out.println("JDialog.toFront() indisponible pour java < 1.5");
        }
    }

    super.show();
  }
  @Override
  public void objetCree(H1dObjetEvent e) {
//    System.out.println("objetCree(H1dObjetEvent e) e.getSource().enChaine()="+e.getSource().enChaine());
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
//    System.out.println("Hydraulique1dCustomizer");
//    System.out.println("objetSupprime(H1dObjetEvent e) e.getSource().enChaine()="+e.getSource().enChaine());
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
//    System.out.println("Hydraulique1dCustomizer");
//    System.out.println("objetModifie(H1dObjetEvent e) e.getSource().enChaine()="+e.getSource().enChaine());
  }

  /**
   * Transfert les donn�es de l'objet m�tier vers les composants graphique de l'�diteur.
   */
  protected abstract void setValeurs();

  /**
   * Transfert les donn�es de l'�diteur vers l'objet m�tier.
   * @return VRAI s'il y a au moins une modification par rapport � l'objet m�tier.
   */
  protected abstract boolean getValeurs();
  
  @Override
  protected String getS(final String _s) {
    return this.getS(_s,new Object[0]);
  }

  protected String getS(final String _s, Object... _vals) {
    return Hydraulique1dResource.getS(_s, _vals);
  }
}
