/*
 * @file         Hydraulique1dCustomizerImprimable.java
 * @creation     2001-12-07
 * @modification $Date: 2007-02-07 09:56:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.Map;

import javax.swing.JComponent;

import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.impression.EbliPageFormat;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.ebli.impression.EbliPageableDelegate;
import org.fudaa.ebli.impression.EbliPrinter;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import org.fudaa.ctulu.CtuluLibImage;
/**
 * Classe m�re abstraite de tous les �diteurs.<br>
 * Elle permet d\u2019ajouter � sa classe m�re des possibilit�s d\u2019impression et d\u2019exportation d\u2019image.
 * @version      $Id: Hydraulique1dCustomizerImprimable.java,v 1.12 2007-02-07 09:56:21 deniger Exp $
 * @author       Jean-Marc Lacombe
 */
public abstract class Hydraulique1dCustomizerImprimable
  extends Hydraulique1dCustomizer
  implements EbliPageable, CtuluImageProducer {
  private EbliPageableDelegate delegueImpression_;
  protected Hydraulique1dCustomizerImprimable(String _title) {
    super(_title);
    initImpression();
  }
  protected Hydraulique1dCustomizerImprimable(
    String _title,
    JComponent _message) {
    super(_title, _message);
    initImpression();
  }
  protected Hydraulique1dCustomizerImprimable(
    BDialogContent _parent,
    String _title) {
    super(_parent, _title);
    initImpression();
  }
  protected Hydraulique1dCustomizerImprimable(
    BuCommonInterface app,
    BDialogContent _parent,
    String _title) {
    super(app, _parent, _title);
    initImpression();
  }
  protected Hydraulique1dCustomizerImprimable(
    BuCommonInterface app,
    String _title) {
    super(app, _title);
    initImpression();
  }
  protected Hydraulique1dCustomizerImprimable(
    BDialogContent _parent,
    String _title,
    JComponent _message) {
    super(_parent, _title, _message);
    initImpression();
  }
  private final void initImpression() {
    delegueImpression_= new EbliPageableDelegate(this);
  }
  @Override
  public Printable getPrintable(int _i) {
    return delegueImpression_.getPrintable(_i);
  }
  @Override
  public PageFormat getPageFormat(int _i) {
    return delegueImpression_.getPageFormat(_i);
  }
  @Override
  public EbliPageFormat getDefaultEbliPageFormat() {
    return delegueImpression_.getDefaultEbliPageFormat();
  }
  @Override
  public int getNumberOfPages() {
    return 1;
  }
  @Override
  public int print(Graphics _g, PageFormat _format, int _page) {
    return EbliPrinter.printComponent(_g,_format,getContentPane(),true,_page);
  }
  /**
   * @return l'image produite dans la taille courante du composant.
   */
  @Override
  public BufferedImage produceImage(Map _params) {
    //Fred: peut etre remplacer le tout par CtuluLibImage.produceImage(getContentPane());
    BufferedImage i = new BufferedImage(getContentPane().getWidth(), getContentPane().getHeight(),
        BufferedImage.TYPE_3BYTE_BGR);
    getContentPane().paint(i.createGraphics());
    return i;
  }
  
  @Override
  public BufferedImage produceImage(int _w, int _h, Map _params) {
    return CtuluLibImage.produceImageForComponent(getContentPane(), _w, _h,_params);
  }
  @Override
  public Dimension getDefaultImageDimension(){
    return getContentPane().getSize();
  }
  /**
   * Renvoie les informations sur le document.
   */
  @Override
  public BuInformationsDocument getInformationsDocument() {
    BuInformationsDocument id_= new BuInformationsDocument();
    return id_;
  }
}
