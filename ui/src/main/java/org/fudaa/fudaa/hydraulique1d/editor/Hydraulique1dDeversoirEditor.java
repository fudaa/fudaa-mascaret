/*
 * @file         Hydraulique1dDeversoirEditor.java
 * @creation     2000-11-29
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementZCoefQ;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;
import org.fudaa.fudaa.hydraulique1d.reseau.Hydraulique1dReseauDeversoir;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'un d�versoir lat�ral (MetierDeversoir).<br>
 * Appeler si l'utilisateur clic sur une singularit� de type "Hydraulique1dReseauDeversoir".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().DEVERSOIR().editer().<br>
 * @version      $Revision: 1.20 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dDeversoirEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuButton btDefinirLoi_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("DEFINIR UNE LOI"));
  private BuTextField tfNumero_, tfAbscisse_, tfLongueur_, tfNom_;
  private BuTextField tfZ_, tfQ_;
  private BuRadioButton rbLoiTarage_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi de tarage"));
  private BuRadioButton rbZQ_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Cote cr�te, coeff. Q"));
  private BuLabel lbAbscisse_;
  private Hydraulique1dListeLoiCombo cmbNomLoi_;
  private BuPanel pnNumero_, pnDeversoir_, pnDeversoirBase_, pnNomLoi_, pnAbscisse_, pnNom_;
  private BuPanel pnLongueur_, pnComportementZQ_, pnZQ_, pnLigZQ_, pnValZQ_;
  private BuPanel pnComportementLoi_, pnLoiTarage_, pnLigLoiTarage_, pnComportement_;
  private BuVerticalLayout loDeversoir_, loDeversoirBase_, loNomLoi_, loComportement_;
  private BuHorizontalLayout loNumero_, loAbscisse_,loNom_, loLongueur_, loZQ_, loLoiTarage_;
  private BuHorizontalLayout loComportementZQ_, loComportementLoi_;
  private BuGridLayout loValZQ_;
  private MetierDeversoir deversoir_;
  private MetierDonneesHydrauliques donneesHydro_;
  private MetierBief bief_;

  private Hydraulique1dReseauDeversoir deversoirGraphique_;
  public Hydraulique1dDeversoirEditor() {
    this(null);
  }
  public Hydraulique1dDeversoirEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition d'un d�versoir"));
    cmbNomLoi_ = new Hydraulique1dListeLoiCombo(Hydraulique1dListeLoiCombo.TARAGE);
    deversoir_= null;
    loNumero_= new BuHorizontalLayout(5, false, false);
    loAbscisse_= new BuHorizontalLayout(5, false, false);
    loNom_= new BuHorizontalLayout(5, false, false);
    loLongueur_= new BuHorizontalLayout(5, false, false);
    loComportementZQ_= new BuHorizontalLayout(5, false, false);
    loComportementLoi_= new BuHorizontalLayout(5, false, false);
    loZQ_= new BuHorizontalLayout(2, false, false);
    loLoiTarage_= new BuHorizontalLayout(2, false, false);
    loValZQ_= new BuGridLayout(2, 5, 5);
    loComportement_= new BuVerticalLayout(5, false, false);
    loDeversoir_= new BuVerticalLayout(5, false, false);
    loDeversoirBase_= new BuVerticalLayout(5, false, false);
    loNomLoi_= new BuVerticalLayout(5, false, false);
    Container pnMain_= getContentPane();
    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loNumero_);
    pnAbscisse_= new BuPanel();
    pnAbscisse_.setLayout(loAbscisse_);
    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    pnLongueur_= new BuPanel();
    pnLongueur_.setLayout(loLongueur_);
    pnNomLoi_= new BuPanel();
    pnNomLoi_.setLayout(loNomLoi_);
    pnNomLoi_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnComportement_= new BuPanel();
    pnComportement_.setLayout(loComportement_);
    CompoundBorder bdComportement=
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5)));
    TitledBorder tb= new TitledBorder(bdComportement, Hydraulique1dResource.HYDRAULIQUE1D.getString("Comportement"));
    pnComportement_.setBorder(tb);
    pnComportementZQ_= new BuPanel();
    pnComportementZQ_.setLayout(loComportementZQ_);
    pnZQ_= new BuPanel();
    pnZQ_.setLayout(loZQ_);
    Dimension tailleRb= rbLoiTarage_.getPreferredSize();
    pnLigZQ_= new BuPanel();
    pnLigZQ_.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    Dimension taillepnLigZQ= pnLigZQ_.getPreferredSize();
    taillepnLigZQ.height= tailleRb.height;
    pnLigZQ_.setPreferredSize(taillepnLigZQ);
    pnLigLoiTarage_= new BuPanel();
    pnLigLoiTarage_.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    Dimension taillepnLigLoiTarage= pnLigLoiTarage_.getPreferredSize();
    taillepnLigLoiTarage.height= tailleRb.height;
    pnLigLoiTarage_.setPreferredSize(taillepnLigLoiTarage);
    pnValZQ_= new BuPanel();
    pnValZQ_.setLayout(loValZQ_);
    pnValZQ_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnComportementLoi_= new BuPanel();
    pnComportementLoi_.setLayout(loComportementLoi_);
    pnLoiTarage_= new BuPanel();
    pnLoiTarage_.setLayout(loLoiTarage_);
    pnDeversoirBase_= new BuPanel();
    pnDeversoirBase_.setLayout(loDeversoirBase_);
    pnDeversoir_= new BuPanel();
    pnDeversoir_.setLayout(loDeversoir_);
    pnDeversoir_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    int textSize= 5;
    BuLabel lbNumero= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("N� d�versoir"));
    Dimension dimLabel= lbNumero.getPreferredSize();
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(true);
    pnNumero_.add(lbNumero, 0);
    pnNumero_.add(tfNumero_, 1);
    textSize= 10;
    tfAbscisse_= BuTextField.createDoubleField();
    tfAbscisse_.setColumns(textSize);
    tfAbscisse_.setEditable(true);
    BuLabel lbAbscisse= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Abscisse"));
    lbAbscisse.setPreferredSize(dimLabel);
    pnAbscisse_.add(lbAbscisse, 0);
    pnAbscisse_.add(tfAbscisse_, 1);
    lbAbscisse_= new BuLabel("   ");
    pnAbscisse_.add(lbAbscisse_, 2);
    tfNom_= new BuTextField();
    //tfNom_.setColumns(textSize);
    tfNom_.setEditable(true);
    BuLabel lbNom = new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nom"));
    lbNom.setPreferredSize(dimLabel);
    pnNom_.add(lbNom, 0);
    pnNom_.add(tfNom_, 1);
    tfLongueur_= BuTextField.createDoubleField();
    tfLongueur_.setColumns(textSize);
    tfLongueur_.setEditable(true);
    BuLabel lbLongueur= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Longueur"));
    lbLongueur.setPreferredSize(dimLabel);
    pnLongueur_.add(lbLongueur, 0);
    pnLongueur_.add(tfLongueur_, 1);
    pnDeversoirBase_.add(pnNumero_, 0);
    pnDeversoirBase_.add(pnAbscisse_, 1);
    pnDeversoirBase_.add(pnNom_, 2);
    pnDeversoirBase_.add(pnLongueur_, 3);
    pnZQ_.add(rbZQ_, 0);
    pnZQ_.add(pnLigZQ_, 1);
    tfZ_= BuTextField.createDoubleField();
    tfZ_.setColumns(textSize);
    tfZ_.setEditable(true);
    tfQ_= BuTextField.createDoubleField();
    tfQ_.setColumns(textSize);
    tfQ_.setEditable(true);
    pnValZQ_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Cote cr�te")), 0);
    pnValZQ_.add(tfZ_, 1);
    pnValZQ_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Coeff. de d�bit")), 2);
    pnValZQ_.add(tfQ_, 3);
    pnComportementZQ_.add(pnZQ_, 0);
    pnComportementZQ_.add(pnValZQ_, 1);
    pnLoiTarage_.add(rbLoiTarage_, 0);
    pnLoiTarage_.add(pnLigLoiTarage_, 1);
    pnNomLoi_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("nom de la loi de type tarage")+" 'Z=f(Q)'"), 0);
    pnNomLoi_.add(cmbNomLoi_, 1);
    btDefinirLoi_.setActionCommand("DEFINIR_LOI_TARAGE");
    btDefinirLoi_.addActionListener(this);
    pnNomLoi_.add(btDefinirLoi_, 2);
    pnComportementLoi_.add(pnLoiTarage_, 0);
    pnComportementLoi_.add(pnNomLoi_, 1);
    pnComportement_.add(pnComportementZQ_, 0);
    pnComportement_.add(pnComportementLoi_, 1);
    pnDeversoir_.add(pnDeversoirBase_, 0);
    pnDeversoir_.add(pnComportement_, 1);
    rbLoiTarage_.addActionListener(this);
    rbLoiTarage_.setActionCommand("LOI_TARAGE");
    rbZQ_.addActionListener(this);
    rbZQ_.setActionCommand("ZQ");
    pnMain_.add(pnDeversoir_, BorderLayout.CENTER);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("LOI_TARAGE".equals(cmd)) {
      rbLoiTarage_.setSelected(true);
      rbZQ_.setSelected(false);
      activationComportementZQ(false);
    }
    else if ("ZQ".equals(cmd)) {
      rbLoiTarage_.setSelected(false);
      rbZQ_.setSelected(true);
      activationComportementZQ(true);
    }
    else if (cmd.startsWith("DEFINIR_LOI")) {
        Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().setQualiteDEau(false);
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer();
    }
    else if ("ANNULER".equals(cmd)) {
      fermer();
    }
    else if ("VALIDER".equals(cmd)) {
      if ((!rbZQ_.isSelected())&&(cmbNomLoi_.getValeurs() == null)) {
        showBuError(Hydraulique1dResource.HYDRAULIQUE1D.getString("Aucune loi s�lectionn�e"), true);
        System.err.println(Hydraulique1dResource.HYDRAULIQUE1D.getString("Aucune loi selectionnee"));
        return;
      }
      if (getValeurs()) {
        firePropertyChange("singularite", null, deversoir_);
      }
      fermer();
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _dev) {
    if (!(_dev instanceof MetierDeversoir))
      return;
    if (_dev == deversoir_)
      return;
    deversoir_= (MetierDeversoir)_dev;
    setValeurs();
  }
  public void setDeversoirGraphique(Hydraulique1dReseauDeversoir _deversoir) {
    deversoirGraphique_= _deversoir;
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (deversoir_ == null)
      return changed;
    int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != deversoir_.numero()) {
      deversoir_.numero(nnum);
      changed= true;
    }
    double absc= ((Double)tfAbscisse_.getValue()).doubleValue();
    if (absc != deversoir_.abscisse()) {
      deversoir_.abscisse(absc);
      changed= true;
    }
    String nom= tfNom_.getText();
    if (!nom.equals(deversoir_.nom())) {
      deversoir_.nom(nom);
      changed = true;
    }
    double longu= ((Double)tfLongueur_.getValue()).doubleValue();
    if (longu != deversoir_.longueur()) {
      deversoir_.longueur(longu);
      changed= true;
    }
    if (rbZQ_.isSelected()) {
      if (deversoir_ instanceof MetierDeversoirComportementLoi) {
        deversoir_=
          bief_.transformeDeversoirLoi2ZQ(
            (MetierDeversoirComportementLoi)deversoir_);
        deversoirGraphique_.putData("singularite", deversoir_);
        changed= true;
      }
      MetierDeversoirComportementZCoefQ dZQ=
        (MetierDeversoirComportementZCoefQ)deversoir_;
      double debit= ((Double)tfQ_.getValue()).doubleValue();
      if (debit != dZQ.coefQ()) {
        dZQ.coefQ(debit);
        changed= true;
      }
      double cote= ((Double)tfZ_.getValue()).doubleValue();
      if (cote != dZQ.coteCrete()) {
        dZQ.coteCrete(cote);
        changed= true;
      }
    } else { // le bouton comportement par loi est s�lectionn�
      if (deversoir_ instanceof MetierDeversoirComportementZCoefQ) {
        deversoir_=
          bief_.transformeDeversoirZQ2Loi(
            (MetierDeversoirComportementZCoefQ)deversoir_);
        deversoirGraphique_.putData("singularite", deversoir_);
        changed= true;

      }
      MetierDeversoirComportementLoi dLoi= (MetierDeversoirComportementLoi)deversoir_;
      //MetierLoiTarage loi= (MetierLoiTarage)cmbNomLoi_.getValeurs();
      MetierLoiHydraulique loi= cmbNomLoi_.getValeurs();
      if (loi != dLoi.loi()) {
        dLoi.loi(loi);
        changed= true;
      }
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(deversoir_.numero()));
    tfAbscisse_.setValue(new Double(deversoir_.abscisse()));
    tfNom_.setValue(deversoir_.nom());
    tfLongueur_.setValue(new Double(deversoir_.longueur()));
    String textAbsc= "";
    if (bief_ != null) {
      textAbsc= getS("du bief n�") + (bief_.indice()+1);
      if ((bief_.extrAmont().profilRattache() != null)
        && (bief_.extrAval().profilRattache() != null))
        textAbsc=
          textAbsc
            + getS(" entre ")
            + bief_.extrAmont().profilRattache().abscisse()
            + getS(" et ")
            + bief_.extrAval().profilRattache().abscisse();
      else
        textAbsc= textAbsc + " ("+getS("abscisses des extremit�s inconnues")+")";
    } else
      textAbsc= getS("bief inconnu");
    lbAbscisse_.setText(textAbsc);
    if (deversoir_ instanceof MetierDeversoirComportementZCoefQ) {
      MetierDeversoirComportementZCoefQ deversoirZQ_=
        (MetierDeversoirComportementZCoefQ)deversoir_;
      tfZ_.setValue(new Double(deversoirZQ_.coteCrete()));
      tfQ_.setValue(new Double(deversoirZQ_.coefQ()));
      rbZQ_.setSelected(true);
      rbLoiTarage_.setSelected(false);
      activationComportementZQ(true);
    }
    cmbNomLoi_.initListeLoi();
    if (deversoir_ instanceof MetierDeversoirComportementLoi) {
      MetierDeversoirComportementLoi deversoirLoi_=
        (MetierDeversoirComportementLoi)deversoir_;
      if (deversoirLoi_.loi() != null)
        cmbNomLoi_.setValeurs(deversoirLoi_.loi());
      tfZ_.setValue(new Double(0));
      tfQ_.setValue(new Double(0));
      rbZQ_.setSelected(false);
      rbLoiTarage_.setSelected(true);
      activationComportementZQ(false);
    }
  }
  // ObjetEventListener
  @Override
  public void objetCree(H1dObjetEvent e) {
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
	  MetierHydraulique1d src= e.getSource();
    String champ= e.getChamp();
    if (src == null)
      return;
    if ((src instanceof MetierDonneesHydrauliques)&&("lois".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
    if ((src instanceof MetierLoiTarage)&&("nom".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
  }
  public void setDonneesHydrauliques(MetierDonneesHydrauliques _donneesHydro) {
    donneesHydro_= _donneesHydro;
    cmbNomLoi_.setDonneesHydro(donneesHydro_);
  }
  public void setBiefParent(MetierBief _bief) {
    bief_= _bief;
  }
  private void activationComportementZQ(boolean zq) {
    if (zq) {
      pnValZQ_.setEnabled(true);
      tfQ_.setEnabled(true);
      tfZ_.setEnabled(true);
      if (tfQ_.getValue() == null)
        tfQ_.setValue(new Double(0));
      if (tfZ_.getValue() == null)
        tfZ_.setValue(new Double(0));
      pnNomLoi_.setEnabled(false);
      cmbNomLoi_.setEnabled(false);
      btDefinirLoi_.setEnabled(false);
    } else {
      pnNomLoi_.setEnabled(true);
      cmbNomLoi_.setEnabled(true);
      btDefinirLoi_.setEnabled(true);
      pnValZQ_.setEnabled(false);
      tfQ_.setEnabled(false);
      tfZ_.setEnabled(false);
    }
  }
}
