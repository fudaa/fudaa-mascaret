/*
 * @file         Hydraulique1dExtremLibreEditor.java
 * @creation     2000-11-25
 * @modification $Date: 2007-11-20 11:42:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierCondLimiteImposee;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierLimiteCalcule;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLimite;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimniHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierTypeCondLimiteTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierLimiteQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresModeleQualiteEau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur d'une extr�mit� libre (MetierExtremite).<br>
 * Appeler si l'utilisateur clic sur l'extremite libre d'un bief du r�seau de type "Hydraulique1dReseauExtremLibre".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().EXTREMITE_LIBRE().editer().<br>
 * @version      $Revision: 1.26 $ $Date: 2007-11-20 11:42:42 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe, Olivier Pasteur
 */
public class Hydraulique1dExtremLibreEditor
  extends Hydraulique1dCustomizerImprimable
   {
  private BuButton btDefinirLoi_= new BuButton(getS("DEFINIR UNE LOI"));
  private BuButton btDefinirLoiTracer_= new BuButton(getS("DEFINIR UNE LOI"));
  private BuTextField tfNumero_;
  private BuRadioButton rbCalcul_= new BuRadioButton(getS("par calcul"));
  private BuRadioButton rbLoi_= new BuRadioButton(getS("par loi"));
  private BuRadioButton rbEvacLibre_= new BuRadioButton(getS("Evacuation libre"));
  private BuRadioButton rbHNormal_= new BuRadioButton(getS("Hauteur normale"));
  private BuRadioButton rbCote_= new BuRadioButton(getS("Cote impos�e"));
  private BuRadioButton rbDebit_= new BuRadioButton(getS("Debit impos�"));
  private BuRadioButton rbCoteDebit_= new BuRadioButton(getS("Cote et Debit impos�s"));
  private BuRadioButton rbCondNeumann_= new BuRadioButton(getS("Neumann"));
  private BuRadioButton rbCondDirichlet_= new BuRadioButton(getS("Dirichlet"));
  private Hydraulique1dListeLoiCombo cmbNomLoi_,cmbNomLoiTracer_;
  private BuPanel pnExtremLibre_, pnNumero_, pnHydro_, pnQualiteEau_, pnNomLoi_, pnNomLoiTracer_, pnCondLimite_,pnLimnihydrogramme_,pnTypeCondTracer_;
  private BuPanel pnCondLimCalcul_, pnCondLimFournie_, pnRbCalcul_, pnLigne_,pnLoi_,pnRbLimni_,pnRbTypeCondTracer_;
  private BuVerticalLayout loExtremLibre_, loHydro_, loQualiteEau_, loNomLoi_,loNomLoiTracer_, loRbLimni_,loRbTypeCondTracer_;
  private BuHorizontalLayout loNumero_, loCondLimite_, loRbCalcul_,loLoi_,loLimnihydrogramme_,loTypeCondTracer_;
  private BuBorderLayout loCondLimCalcul_;
  private MetierExtremite extremite_;
  private MetierLimite limite_;
  private MetierLimiteQualiteDEau limiteQualiteDEau_;
  private MetierDonneesHydrauliques donneesHydro_;
  private MetierParametresModeleQualiteEau paramModeleQualiteDEau_;

  public Hydraulique1dExtremLibreEditor() {
    this(null);
  }
  public Hydraulique1dExtremLibreEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition d'extremit� libre"));
    cmbNomLoi_ = new Hydraulique1dListeLoiCombo(Hydraulique1dListeLoiCombo.HYDRAULIQUE);
    cmbNomLoi_.addActionListenerCombo(this);
    //TODO mettre type de loi TRACER
    cmbNomLoiTracer_ = new Hydraulique1dListeLoiCombo(Hydraulique1dListeLoiCombo.TRACER);
    cmbNomLoiTracer_.addActionListenerCombo(this);
    extremite_= null;
    loNumero_= new BuHorizontalLayout(5, false, false);
    loCondLimite_= new BuHorizontalLayout(2, false, false);
    loRbCalcul_= new BuHorizontalLayout(2, false, false);
    loHydro_= new BuVerticalLayout(5, false, false);
    loQualiteEau_= new BuVerticalLayout(5, false, false);
    loNomLoi_= new BuVerticalLayout(5, false, false);
    loCondLimCalcul_= new BuBorderLayout(5, 5);
    loLimnihydrogramme_ = new BuHorizontalLayout(2, false, false);
    loLoi_ =new BuHorizontalLayout(2, false, false);
    loRbLimni_=new BuVerticalLayout(2, false, false);
    loTypeCondTracer_ = new BuHorizontalLayout(2, false, false);
    loRbTypeCondTracer_=new BuVerticalLayout(2, false, false);
    loNomLoiTracer_= new BuVerticalLayout(5, false, false);
    loExtremLibre_= new BuVerticalLayout(5, false, false);
    Container pnMain_= getContentPane();
    pnExtremLibre_= new BuPanel();
    pnExtremLibre_.setLayout(loExtremLibre_);
    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loNumero_);
    pnCondLimite_= new BuPanel();
    pnCondLimite_.setLayout(loCondLimite_);
    pnLimnihydrogramme_ = new BuPanel();
    pnLimnihydrogramme_.setLayout(loLimnihydrogramme_);
    pnRbLimni_ = new BuPanel();
    pnRbLimni_.setLayout(loRbLimni_);
    pnNomLoi_= new BuPanel();
    pnNomLoi_.setLayout(loNomLoi_);
    pnLigne_= new BuPanel();
    Dimension dim= rbCalcul_.getPreferredSize();
    dim.width= 25;
    pnLigne_.setPreferredSize(dim);
    pnLigne_.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    pnRbCalcul_= new BuPanel();
    pnRbCalcul_.setLayout(loRbCalcul_);
    pnCondLimCalcul_= new BuPanel();
    pnCondLimCalcul_.setLayout(loCondLimCalcul_);
    pnCondLimCalcul_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnCondLimFournie_= new BuPanel();
    pnCondLimFournie_.setLayout(loCondLimCalcul_);
    pnCondLimFournie_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnHydro_= new BuPanel();
    pnHydro_.setLayout(loHydro_);
    CompoundBorder bdHydro  = new CompoundBorder(new EtchedBorder(),
    new EmptyBorder(new Insets(5, 5, 5, 5)));
    TitledBorder tbHydro= new TitledBorder(bdHydro, Hydraulique1dResource.HYDRAULIQUE1D.getString("Hydraulique"));
    pnHydro_.setBorder(tbHydro);


    int textSize= 6;
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEnabled(false);
    pnNumero_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("N� extr�mit�")), 0);
    pnNumero_.add(tfNumero_, 1);
    pnRbCalcul_.add(rbCalcul_, 0);
    pnRbCalcul_.add(pnLigne_, 1);
    pnCondLimCalcul_.add(pnRbCalcul_, BorderLayout.NORTH);
    pnCondLimCalcul_.add(rbLoi_, BorderLayout.SOUTH);
    pnCondLimFournie_.add(rbEvacLibre_, BorderLayout.NORTH);
    pnCondLimFournie_.add(rbHNormal_, BorderLayout.SOUTH);
    Dimension dpn= pnCondLimCalcul_.getPreferredSize();
    BuLabel lbCondLimite= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Condition limite fournie"));
    Dimension dlb= lbCondLimite.getPreferredSize();
    dlb.height= dpn.height;
    lbCondLimite.setPreferredSize(dlb);
    pnCondLimite_.add(lbCondLimite, 0);
    pnCondLimite_.add(pnCondLimCalcul_, 1);
    pnCondLimite_.add(pnCondLimFournie_, 2);

    ButtonGroup bG = new ButtonGroup();
    bG.add(rbCote_);
    bG.add(rbDebit_);
    bG.add(rbCoteDebit_);
    rbDebit_.setSelected(true);
    pnRbLimni_.add(rbCote_, 0);
    pnRbLimni_.add(rbDebit_, 1);
    pnRbLimni_.add(rbCoteDebit_, 2);
    pnRbLimni_.setBorder(
            new LineChoiceBorder(false, false, false, true, false, false));
    Dimension dpn2= pnRbLimni_.getPreferredSize();
    BuLabel lbLimni = new BuLabel(" "+Hydraulique1dResource.HYDRAULIQUE1D.getString("avec")+" ");
    Dimension dlb2= lbLimni.getPreferredSize();
    dlb2.height = dpn2.height;
    lbLimni.setPreferredSize(dlb2);
    pnLimnihydrogramme_.add(lbLimni,0);
    pnLimnihydrogramme_.add(pnRbLimni_,1);

    pnNomLoi_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nom de la loi")), 0);
    pnNomLoi_.add(cmbNomLoi_, 1);
    btDefinirLoi_.setActionCommand("DEFINIR_LOI_HYDRAULIQUE");
    btDefinirLoi_.addActionListener(this);
    pnNomLoi_.add(btDefinirLoi_, 2);

    pnLoi_= new BuPanel();
    pnLoi_.setLayout(loLoi_);
    pnLoi_.add(pnNomLoi_,0);
    pnLoi_.add(pnLimnihydrogramme_, 1);


    rbCalcul_.addActionListener(this);
    rbCalcul_.setActionCommand("PAR_CALCUL");
    rbEvacLibre_.addActionListener(this);
    rbEvacLibre_.setActionCommand("EVACUATION_LIBRE");
    rbHNormal_.addActionListener(this);
    rbHNormal_.setActionCommand("HAUTEUR_NORMALE");
    rbLoi_.addActionListener(this);
    rbLoi_.setActionCommand("PAR_LOI");
    pnHydro_.add(pnNumero_, 0);
    pnHydro_.add(pnCondLimite_, 1);
    pnHydro_.add(pnLoi_, 2);


    //Panel Qualit� d'eau
    pnNomLoiTracer_ = new BuPanel();
    pnNomLoiTracer_.setLayout(loNomLoiTracer_);
    pnTypeCondTracer_ = new BuPanel();
    pnTypeCondTracer_.setLayout(loTypeCondTracer_);
    pnRbTypeCondTracer_ = new BuPanel();
    pnRbTypeCondTracer_.setLayout(loRbTypeCondTracer_);
    pnQualiteEau_= new BuPanel();
    pnQualiteEau_.setLayout(loQualiteEau_);
    Dimension dimHydro= pnHydro_.getPreferredSize();
    pnQualiteEau_.setPreferredSize(dimHydro);
    CompoundBorder bdQualiteDEau  = new CompoundBorder(new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5)));
    TitledBorder tbQualiteDEau= new TitledBorder(bdQualiteDEau, Hydraulique1dResource.HYDRAULIQUE1D.getString("Qualit� d'eau"));
    pnQualiteEau_.setBorder(tbQualiteDEau);

   ButtonGroup bG2 = new ButtonGroup();
   bG2.add(rbCondNeumann_);
   bG2.add(rbCondDirichlet_);
   rbCondNeumann_.setSelected(true);
   pnRbTypeCondTracer_.add(rbCondNeumann_, 0);
   pnRbTypeCondTracer_.add(rbCondDirichlet_, 1);
   pnRbTypeCondTracer_.setBorder(
           new LineChoiceBorder(false, false, false, true, false, false));
   Dimension dpn3= pnRbTypeCondTracer_.getPreferredSize();
   BuLabel lbTypeCond = new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Type de condition")+" ");
   Dimension dlb3= lbTypeCond.getPreferredSize();
   dlb3.height = dpn3.height;
   lbTypeCond.setPreferredSize(dlb3);
   pnTypeCondTracer_.add(lbTypeCond,0);
   pnTypeCondTracer_.add(pnRbTypeCondTracer_,1);

   pnNomLoiTracer_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nom de la loi")+" "), 0);
   pnNomLoiTracer_.add(cmbNomLoiTracer_, 1);
   btDefinirLoiTracer_.setActionCommand("DEFINIR_LOI_TRACER");
   btDefinirLoiTracer_.addActionListener(this);
   pnNomLoiTracer_.add(btDefinirLoiTracer_, 2);

   pnQualiteEau_.add(pnTypeCondTracer_,0);
   pnQualiteEau_.add(pnNomLoiTracer_,1);
   pnQualiteEau_.add(btDefinirLoiTracer_,2);


   //tous
    pnExtremLibre_.add(pnHydro_, 0);
    if (CGlobal.AVEC_QUALITE_DEAU) {
    	pnExtremLibre_.add(pnQualiteEau_, 1);
    }
    pnMain_.add(pnExtremLibre_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();

    if ("VALIDER".equals(cmd)) {
      //DEBUG: Test de deniger pour eviter une sale erreur:
      //rbLoi est selectionne et aucune loi choisi
      if (rbLoi_.isSelected()) {
        //System.out.println("loi selectionne");
        if (cmbNomLoi_.getValeurs() == null) {
          showBuError(getS("Aucune loi s�lectionn�e"), true);
          System.err.println(getS("Aucune loi selectionnee"));
          return;
        }
      }
      if (getValeurs()) {
        firePropertyChange("object", null, extremite_);
      }
      fermer();
    }
    else if (cmd.startsWith("DEFINIR_LOI_TRACER")) {
     Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().setQualiteDEau(true);
     Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer();
    }
    else if (cmd.startsWith("DEFINIR_LOI")) {
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().setQualiteDEau(false);
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer();
    }
    else if ("PAR_CALCUL".equals(cmd)) {
      rbCalcul_.setSelected(true);
      rbLoi_.setSelected(false);
      if (!rbEvacLibre_.isEnabled()) {
        rbEvacLibre_.setSelected(true);
        rbHNormal_.setSelected(false);
      }
      rbEvacLibre_.setEnabled(true);
      rbHNormal_.setEnabled(true);
      btDefinirLoi_.setEnabled(false);
      cmbNomLoi_.setEnabled(false);
      CtuluLibSwing.griserPanel(pnLimnihydrogramme_,false);
    }
    else if ("PAR_LOI".equals(cmd)) {
      rbLoi_.setSelected(true);
      rbCalcul_.setSelected(false);
      rbEvacLibre_.setEnabled(false);
      rbHNormal_.setEnabled(false);
      btDefinirLoi_.setEnabled(true);
      cmbNomLoi_.setEnabled(true);
      CtuluLibSwing.griserPanel(pnLimnihydrogramme_,true);
      MetierLoiHydraulique loi= cmbNomLoi_.getValeurs();
      if (loi instanceof MetierLoiLimniHydrogramme){
          CtuluLibSwing.griserPanel(pnLimnihydrogramme_,true);
      }

      else
          CtuluLibSwing.griserPanel(pnLimnihydrogramme_,false);


    }
    else if ("EVACUATION_LIBRE".equals(cmd)) {
      if (rbCalcul_.isSelected()) {
        rbEvacLibre_.setSelected(true);
        rbHNormal_.setSelected(false);
      }
    }
    else if ("HAUTEUR_NORMALE".equals(cmd)) {
      if (rbCalcul_.isSelected()) {
        rbHNormal_.setSelected(true);
        rbEvacLibre_.setSelected(false);
      }
    }
    else if ("COMBO_LOI".equals(cmd)) {
        System.out.println("actionPerformed combo_loi"+cmd);
        MetierLoiHydraulique loi= cmbNomLoi_.getValeurs();
        System.out.println("loi="+loi);
        if (loi instanceof MetierLoiLimniHydrogramme){
            System.out.println("loi limnihydro"+cmd);
            CtuluLibSwing.griserPanel(pnLimnihydrogramme_,true);
        }

        else

            CtuluLibSwing.griserPanel(pnLimnihydrogramme_,false);
    }
    else {
      super.actionPerformed(_evt);
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d o) {
    if (o instanceof MetierExtremite) {
      if (o == extremite_)
        return;
      extremite_= (MetierExtremite)o;
      limite_= extremite_.conditionLimite();
      limiteQualiteDEau_= extremite_.conditionLimiteQualiteDEau();
    } else if (o instanceof MetierDonneesHydrauliques) {
      donneesHydro_= (MetierDonneesHydrauliques)o;
      cmbNomLoi_.setDonneesHydro(donneesHydro_);
      cmbNomLoiTracer_.setDonneesHydro(donneesHydro_);
    } else if (o instanceof MetierParametresModeleQualiteEau) {
        paramModeleQualiteDEau_= (MetierParametresModeleQualiteEau)o;
        System.out.println(getS("Presence de qualite d'eau")+" :"+paramModeleQualiteDEau_.presenceTraceurs());

    }

  }
  @Override
  protected boolean getValeurs() {
      boolean changed = false;
      if (extremite_ == null)
          return changed;
      int nnum = ((Integer) tfNumero_.getValue()).intValue();
      if (nnum != extremite_.numero()) {
          extremite_.numero(nnum);
          changed = true;
      }
      if (rbCalcul_.isSelected()) {
          if (extremite_.conditionLimite().loi() != null) {
              extremite_.conditionLimite().loi(null);
              if (rbEvacLibre_.isSelected())
                  limite_.typeLimiteCalcule(EnumMetierLimiteCalcule.EVACUATION_LIBRE);
              else
                  limite_.typeLimiteCalcule(EnumMetierLimiteCalcule.HAUTEUR_NORMALE);
              changed = true;
          } else { // loi pas null et condition limite fournie par calcul
              if (rbEvacLibre_.isSelected()
                  && (limite_.typeLimiteCalcule().value()
                      != EnumMetierLimiteCalcule._EVACUATION_LIBRE)) {
                  limite_.typeLimiteCalcule(EnumMetierLimiteCalcule.EVACUATION_LIBRE);
                  changed = true;
              }
              if (rbHNormal_.isSelected()
                  && (limite_.typeLimiteCalcule().value()
                      != EnumMetierLimiteCalcule._HAUTEUR_NORMALE)) {
                  limite_.typeLimiteCalcule(EnumMetierLimiteCalcule.HAUTEUR_NORMALE);
                  changed = true;
              }
          }
      } else { // condition limite fournie par loi
          MetierLoiHydraulique loi = cmbNomLoi_.getValeurs();
          if (loi != limite_.loi()) {
              limite_.loi(loi);
              changed = true;
          }
          if (limite_.loi() instanceof MetierLoiLimniHydrogramme) {
              if (rbDebit_.isSelected()
                  && (limite_.condLimiteImposee().value()
                      != EnumMetierCondLimiteImposee._DEBIT)) {
                  limite_.condLimiteImposee(EnumMetierCondLimiteImposee.DEBIT);
                  changed = true;
              }
              if (rbCote_.isSelected()
                  && (limite_.condLimiteImposee().value()
                      != EnumMetierCondLimiteImposee._COTE)) {
                  limite_.condLimiteImposee(EnumMetierCondLimiteImposee.COTE);
                  changed = true;
              }
              if (rbCoteDebit_.isSelected()
                  && (limite_.condLimiteImposee().value()
                      != EnumMetierCondLimiteImposee._COTE_DEBIT)) {
                  limite_.condLimiteImposee(EnumMetierCondLimiteImposee.COTE_DEBIT);
                  changed = true;
              }
          }
      }

      if (paramModeleQualiteDEau_.presenceTraceurs()){

          MetierLoiTracer loi = (MetierLoiTracer) cmbNomLoiTracer_.getValeurs();
          if (loi != limiteQualiteDEau_.loi()) {
              limiteQualiteDEau_.loi(loi);
              changed = true;
          }
          if (rbCondDirichlet_.isSelected() &&
              (limiteQualiteDEau_.typeCondLimiteTracer().value() !=
               EnumMetierTypeCondLimiteTracer._DIRICHLET)) {
              limiteQualiteDEau_.typeCondLimiteTracer(EnumMetierTypeCondLimiteTracer.
                      DIRICHLET);
              changed = true;
          }
          if (rbCondNeumann_.isSelected() &&
              (limiteQualiteDEau_.typeCondLimiteTracer().value() !=
               EnumMetierTypeCondLimiteTracer._NEUMANN)) {
              limiteQualiteDEau_.typeCondLimiteTracer(EnumMetierTypeCondLimiteTracer.
                      NEUMANN);
              changed = true;
          }
      }
               return changed;
  }


  @Override
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(extremite_.numero()));
    cmbNomLoi_.initListeLoi();
    if (limite_.typeLimiteCalcule().value()
      == EnumMetierLimiteCalcule._EVACUATION_LIBRE) {
      rbEvacLibre_.setSelected(true);
      rbHNormal_.setSelected(false);
    } else { // hauteur normal
      rbEvacLibre_.setSelected(false);
      rbHNormal_.setSelected(true);
    }
    if (limite_.loi() == null) {
      rbLoi_.setSelected(false);
      rbCalcul_.setSelected(true);
      btDefinirLoi_.setEnabled(false);
      cmbNomLoi_.setEnabled(false);
      rbEvacLibre_.setEnabled(true);
      rbHNormal_.setEnabled(true);
    } else { // il y a une loi
      rbLoi_.setSelected(true);
      //Correction bogue
      rbCalcul_.setSelected(false);
      cmbNomLoi_.setValeurs(limite_.loi());
      btDefinirLoi_.setEnabled(true);
      cmbNomLoi_.setEnabled(true);
      rbEvacLibre_.setEnabled(false);
      rbHNormal_.setEnabled(false);
      if(limite_.condLimiteImposee().value() == EnumMetierCondLimiteImposee._DEBIT)
          rbDebit_.setSelected(true);
      else if(limite_.condLimiteImposee().value() == EnumMetierCondLimiteImposee._COTE)
          rbCote_.setSelected(true);
      else
          rbCoteDebit_.setSelected(true);
    }
    //Qualit� d'eau
    //grise ou degrise le panneau selon qu'il y a presence de qualite d'eau ou non
    cmbNomLoiTracer_.initListeLoi();
    cmbNomLoiTracer_.setValeurs((MetierLoiHydraulique) limiteQualiteDEau_.loi());
    CtuluLibSwing.griserPanel(pnQualiteEau_,paramModeleQualiteDEau_.presenceTraceurs());
    if(paramModeleQualiteDEau_.presenceTraceurs()){
        if (limiteQualiteDEau_.typeCondLimiteTracer().value()== EnumMetierTypeCondLimiteTracer._DIRICHLET) {
            rbCondDirichlet_.setSelected(true);
            rbCondNeumann_.setSelected(false);
        } else { // hauteur normal
            rbCondDirichlet_.setSelected(false);
            rbCondNeumann_.setSelected(true);
        }
        /*if (limiteQualiteDEau_.loi() == null) {
            btDefinirLoiTracer_.setEnabled(false);
            cmbNomLoiTracer_.setEnabled(false);
        } else { // il y a une loi
            btDefinirLoiTracer_.setEnabled(true);
            cmbNomLoiTracer_.setEnabled(true);
        }*/
    }

  }
  /**
   * DEBUG:deniger un evt creation peut arriver avant que la loi soit reellement
   * creee: d'ou erreur.
   */
  @Override
  public void objetCree(H1dObjetEvent e) {
    /* MetierHydraulique1d src=e.getSource();
    if( src==null ) return;
    //FIXME: deniger voir commentaire ci-dessus
    if (src instanceof DLoiHydraulique) {
      // System.out.println("obj cree");

      initListeLoi();
    } */
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
    /* MetierHydraulique1d src=e.getSource();
    if( src==null ) return;
    if (src instanceof DLoiHydraulique) {
      // System.out.println("obj supprime");
      //FIXME: deniger
      initListeLoi();
    } */
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
    MetierHydraulique1d src= e.getSource();
    String champ= e.getChamp();
    if (src == null)
      return;
    if ((src instanceof MetierDonneesHydrauliques)&&("lois".equals(champ))) {
      cmbNomLoi_.initListeLoi();
      cmbNomLoiTracer_.initListeLoi();
    }
    if ((src instanceof MetierLoiHydraulique)&&("nom".equals(champ))) {
      cmbNomLoi_.initListeLoi();
      cmbNomLoiTracer_.initListeLoi();
    }
  }

 /* private static void griserPanel(Container pn, boolean b){
      pn.setEnabled(b);
     Component[] tabCompoments = pn.getComponents();
     for (int i = 0; i < tabCompoments.length; i++) {
         Component c = tabCompoments[i];
         c.setEnabled(b);
         if (c instanceof Container) griserPanel((Container)c,b);
     }
 }*/

}
