/*
 * @file         Hydraulique1dFiltrageProfilEditor.java
 * @creation     1999-12-28
 * @modification $Date: 2007-02-21 16:33:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur pour le filtrage du profil.
 * @author Bertrand Marchand
 * @version 1.0
 */
public class Hydraulique1dFiltrageProfilEditor extends JDialog {
  public static final int LISSAGE_MOYENNE=0;
  public static final int LISSAGE_MEDIANE=1;

  private JPanel pnPrinc_=new JPanel();
  private Border bdpnPrinc_;
  private JPanel pnBoutons_=new JPanel();
  private BorderLayout lyThis_=new BorderLayout();
  private BuButton btOk_=new BuButton();
  private BuButton btAnnuler_=new BuButton();
  private BuGridLayout lyPrinc_=new BuGridLayout(3,5,2);
  private JLabel lbDistance_=new JLabel();
  private BuTextField tfDistance_=BuTextField.createDoubleField();
  private JLabel lbDistanceUnite_=new JLabel();
  public int reponse=0;

  public Hydraulique1dFiltrageProfilEditor(Frame parent) {
    super(parent);
    setModal(true);
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    customize();
    pack();
    setLocationRelativeTo(parent);
  }

  private void jbInit() throws Exception {
    bdpnPrinc_=BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
                                               BorderFactory.createEmptyBorder(5, 5, 5, 5));
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Filtrage du profil"));
    this.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        this_windowClosed(e);
      }
    }); this.getContentPane().setLayout(lyThis_);
    pnPrinc_.setBorder(bdpnPrinc_);
    pnPrinc_.setLayout(lyPrinc_);
    btOk_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    btOk_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btOk_actionPerformed(e);
      }
    });
    btAnnuler_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    btAnnuler_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btAnnuler_actionPerformed(e);
      }
    });
    lbDistance_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Distance maxi tol�r�e � la corde")+" :");
    lbDistanceUnite_.setText("m");
    tfDistance_.setPreferredSize(new Dimension(70, 20));
    tfDistance_.setText("0.1");
    this.getContentPane().add(pnPrinc_, java.awt.BorderLayout.CENTER);
    pnPrinc_.add(lbDistance_);
    pnPrinc_.add(tfDistance_);
    pnPrinc_.add(lbDistanceUnite_);
    pnBoutons_.add(btOk_);
    pnBoutons_.add(btAnnuler_);
    this.getContentPane().add(pnBoutons_, java.awt.BorderLayout.SOUTH);
  }

  private void customize() {
    btOk_.setIcon(BuResource.BU.getIcon("valider"));
    btAnnuler_.setIcon(BuResource.BU.getIcon("annuler"));
  }

  public void btOk_actionPerformed(ActionEvent e) {
    reponse=1;
    setVisible(false);
  }

  public void btAnnuler_actionPerformed(ActionEvent e) {
    reponse=0;
    setVisible(false);
  }

  public void this_windowClosed(WindowEvent e) {
    reponse=0;
    setVisible(false);
  }

  /**
   * Retourne la distance � la corde tol�r�e.
   * @return La distance
   */
  public double getDistance() {
    double r=0;
    try {
      r=Double.parseDouble(tfDistance_.getText());
    }
    catch (NumberFormatException _exc) {}

    return r;
  }

  /**
   * Pour tester l'ergonomie
   */
  public static void main(String[] _args) {
    Hydraulique1dFiltrageProfilEditor ed=new Hydraulique1dFiltrageProfilEditor(null);
    ed.setVisible(true);
    System.exit(0);
  }
}
