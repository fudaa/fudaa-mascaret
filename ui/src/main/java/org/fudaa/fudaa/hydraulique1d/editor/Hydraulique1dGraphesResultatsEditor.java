/*
 * @file         Hydraulique1dGraphesResultatsEditor.java
 * @creation     2001-09-25
 * @modification $Date: 2008-04-22 14:34:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLaisse;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCrueCalageAuto;
import org.fudaa.ebli.animation.EbliAnimationAction;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BPanneauNavigation;
import org.fudaa.ebli.graphe.Contrainte;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.GrapheComponent;
import org.fudaa.ebli.graphe.Valeur;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilLongPane;
import org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilPane;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dAbstractGraphe;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheResuBief;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuList;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTaskOperation;
import com.memoire.fu.FuLib;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Editeur des r�sultats sous forme graphiques (trac� des courbes) (MetierResultatsTemporelSpatial).<br> Appeler si l'utilisateur clic sur le menu
 * "Mascaret/Graphes".<br> Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().GRAPHES_RESULTATS().editer().<br>
 *
 * @version $Revision: 1.46 $ $Date: 2008-04-22 14:34:16 $ by $Author: jm_lacombe $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dGraphesResultatsEditor
extends Hydraulique1dCustomizerImprimable {

	JButton btAvancer_, btAvancerVite_, btReculer_, btReculerVite_/*, btAnimation_*/;
	JButton btProfAvancer_, btProfAvancerVite_, btProfReculer_, btProfReculerVite_;
	JButton btAnim_;
	JButton btCustom_;
	JButton btExportExcel_;
	JButton btVoir_;
	JButton btExport_;
	private BuTabbedPane onglets_;
	private PanneauGraphesResultats pnHydro;
	private PanneauGraphesResultats pnQE = null;
	private Hydraulique1dProfilPane pnProfils_;
	private Hydraulique1dProfilLongPane pnProfilsLong_;

	private MetierResultatsTemporelSpatial resultTempoSpatial_;
	private MetierResultatsTemporelSpatial resultatsQE_;
	MetierDonneesHydrauliques donnneHyd_;
	MetierReseau reseau_;
	private EbliAnimationAction aniAction_;
	private GrapheUpdateAction actionUpdateAuto;
	private final boolean  resultCalage ;
	protected MetierCalageAuto donneeCalageAuto;
	
	public Hydraulique1dGraphesResultatsEditor() {
		this(null, false);
	}
	public Hydraulique1dGraphesResultatsEditor(BDialogContent parent){
		this(parent, false);
	}
	
	public Hydraulique1dGraphesResultatsEditor(BDialogContent parent, final boolean resultCalage) {
		super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Graphes Resultats"));
		this.resultCalage = resultCalage;
		resultTempoSpatial_ = null;

		setActionPanel(BPanneauNavigation.AUCUN);
		buildNavigation();

		//  Create a tabbed pane
		onglets_ = new BuTabbedPane();
		onglets_.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				switchNavigation();
			}
		});

		pnHydro = new PanneauGraphesResultats(this);
		actionUpdateAuto = new GrapheUpdateAction(this);
		onglets_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("R�sultats hydrauliques"), pnHydro);

		pnProfils_ = new Hydraulique1dProfilPane(Hydraulique1dProfilPane.VOIR, Hydraulique1dProfilPane.PROFILS);
		
		pnProfilsLong_ = new Hydraulique1dProfilLongPane(Hydraulique1dProfilPane.VOIR,Hydraulique1dProfilPane.PROFILS);
		if(!resultCalage) {
			onglets_.addTab(Hydraulique1dResource.getS("Profils en travers"), pnProfils_);
			onglets_.addTab(Hydraulique1dResource.getS("Profils en long"), pnProfilsLong_);
		}
		

		getContentPane().add(onglets_/*, BuBorderLayout.CENTER*/);

		aniAction_ = new EbliAnimationAction(new GrapheAnimation(this));
		final String tooltip = Hydraulique1dResource.getS("S�lectionner au moins une variable");
		aniAction_.setUnableToolTip(tooltip);
		aniAction_.setEnabled(false);
		btAnim_.setEnabled(false);
		btAnim_.setToolTipText("<html>" + btAnim_.getToolTipText() + "<br>" + tooltip + "</html>");
		aniAction_.setDefaultToolTip(btAnim_.getToolTipText());
		pnHydro.getLstVar().getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				
					aniAction_.setEnabled(!pnHydro.getLstVar().isSelectionEmpty());
					btAnim_.setEnabled(!pnHydro.getLstVar().isSelectionEmpty());
				
					
			}
		});

		
		
		
		setNavPanel(BPanneauNavigation.FERMER);
		pack();
	}

	private void buildNavigation() {
		this.setActionPanel(EbliPreferences.DIALOG.VOIR | EbliPreferences.DIALOG.EXPORTER);

		btVoir_ = getActionPanel().getCustomAction("VOIR");
		btExport_ = getActionPanel().getCustomAction("EXPORTER");
		btExportExcel_ = (JButton) addAction("Excel", "EXCEL");
		btCustom_ = (JButton) addAction(Hydraulique1dResource.getS("Personnaliser"), "PERSONNALISER");
		btReculerVite_ = (JButton) addAction(Hydraulique1dResource.getS("Reculer vite"), BuResource.BU.getIcon("reculervite"), "POINTRECULERVITE");
		btReculer_ = (JButton) addAction(Hydraulique1dResource.getS("Reculer"), BuResource.BU.getIcon("reculer"), "POINTRECULER");
		btAvancer_ = (JButton) addAction(Hydraulique1dResource.getS("Avancer"), BuResource.BU.getIcon("avancer"), "POINTAVANCER");
		btAvancerVite_ = (JButton) addAction(Hydraulique1dResource.getS("Avancer vite"), BuResource.BU.getIcon("avancervite"), "POINTAVANCERVITE");
		btAnim_ = (JButton) addAction(Hydraulique1dResource.getS("Animation"), BuResource.BU.getIcon("video"), "ANIM");

		btProfReculerVite_ = (JButton) addAction(Hydraulique1dResource.getS("Reculer vite"), BuResource.BU.getIcon("reculervite"), "PROFRECULERVITE");
		btProfReculer_ = (JButton) addAction(Hydraulique1dResource.getS("Reculer"), BuResource.BU.getIcon("reculer"), "PROFRECULER");
		btProfAvancer_ = (JButton) addAction(Hydraulique1dResource.getS("Avancer"), BuResource.BU.getIcon("avancer"), "PROFAVANCER");
		btProfAvancerVite_ = (JButton) addAction(Hydraulique1dResource.getS("Avancer vite"), BuResource.BU.getIcon("avancervite"), "PROFAVANCERVITE");
	}

	private void switchNavigation() {
		boolean btravers = onglets_.getSelectedComponent() == pnProfils_ ||onglets_.getSelectedComponent() == pnProfilsLong_;

		btVoir_.setVisible(!btravers);
		btExport_.setVisible(!btravers);
		btExportExcel_.setVisible(!btravers && FuLib.isWindows());
		btCustom_.setVisible(!btravers);
		btReculerVite_.setVisible(!btravers);
		btReculer_.setVisible(!btravers);
		btAvancer_.setVisible(!btravers);
		btAvancerVite_.setVisible(!btravers);
		btAnim_.setVisible(!btravers);


		if(onglets_.getSelectedComponent() == pnProfilsLong_) {
			btravers = false;
			btVoir_.setVisible(true);
			pnProfilsLong_.fireDatasetProfilsChanged();
		}
		btProfReculerVite_.setVisible(btravers);
		btProfReculer_.setVisible(btravers);
		btProfAvancer_.setVisible(btravers);
		btProfAvancerVite_.setVisible(btravers);

		if(onglets_.getSelectedComponent() == pnQE) {
			if(aniAction_ != null)
				aniAction_.setEnabled(!pnQE.getLstVar().isSelectionEmpty());
			btAnim_.setEnabled(!pnQE.getLstVar().isSelectionEmpty());	
		} else if(onglets_.getSelectedComponent() == pnHydro) {
			if(aniAction_ != null)
				aniAction_.setEnabled(!pnHydro.getLstVar().isSelectionEmpty());
			btAnim_.setEnabled(!pnHydro.getLstVar().isSelectionEmpty());	
		} 

	}

	public void updateNavigation() {
		btProfAvancer_.setEnabled(pnProfils_.getProfilCourant() != pnProfils_.getProfilsModeleBief().length - 1);
		btProfAvancerVite_.setEnabled(pnProfils_.getProfilCourant() != pnProfils_.getProfilsModeleBief().length - 1);
		btProfReculer_.setEnabled(pnProfils_.getProfilCourant() != 0);
		btProfReculerVite_.setEnabled(pnProfils_.getProfilCourant() != 0);
	}

	@Override
	public void actionPerformed(ActionEvent _evt) {
		String cmd = _evt.getActionCommand();
		if ("FERMER".equals(cmd)) {
			fermer();
		} /*else if ("SPATIAL".equals(cmd))
     spatial();
     else if ("TEMPOREL".equals(cmd))
     temporel();*/ else if ("PERSONNALISER".equals(cmd)) {
    	 personnaliser();
     } else if ("VOIR".equals(cmd)) {
    	 visualiser(true);
     } else if ("EXPORTER".equals(cmd)) {
    	 exporter();
     } else if ("EXCEL".equals(cmd)) {
    	 lanceExcel();
     } else if ("POINTRECULERVITE".equals(cmd)) {
    	 deplaceVisu(-5);
     } else if ("POINTRECULER".equals(cmd)) {
    	 deplaceVisu(-1);
     } else if ("POINTAVANCER".equals(cmd)) {
    	 deplaceVisu(1);
     } else if ("POINTAVANCERVITE".equals(cmd)) {
    	 deplaceVisu(5);
     } else if ("PROFRECULERVITE".equals(cmd)) {
    	 pnProfils_.setProfilCourant(pnProfils_.getProfilCourant() - 5);
     } else if ("PROFRECULER".equals(cmd)) {
    	 pnProfils_.setProfilCourant(pnProfils_.getProfilCourant() - 1);
     } else if ("PROFAVANCER".equals(cmd)) {
    	 pnProfils_.setProfilCourant(pnProfils_.getProfilCourant() + 1);
     } else if ("PROFAVANCERVITE".equals(cmd)) {
    	 pnProfils_.setProfilCourant(pnProfils_.getProfilCourant() + 5);
     } else if ("ANIM".equals(cmd)) {
    	 aniAction_.changeAll();
     }

		updateNavigation();
	}

	@Override
	protected boolean getValeurs() {
		boolean changed = false;
		return changed;
	}

	public String[] getEnabledActions() {
		String[] r = new String[]{
				"IMPRIMER", "PREVISUALISER", "MISEENPAGE"};
		return r;
	}

	public void setResultatsCasier(MetierResultatsTemporelSpatial resultatsCasier) {
		/*if (resultatsCasier == resultatsCasier_)
     return;
     resultatsCasier_= resultatsCasier;*/
		pnHydro.setResultatsCasier(resultatsCasier);
	}

	public void setResultatsLiaison(MetierResultatsTemporelSpatial resultatsLiaison) {
		/*if (resultatsLiaison == resultatsLiaison_)
     return;
     resultatsLiaison_= resultatsLiaison;*/
		pnHydro.setResultatsLiaison(resultatsLiaison);
	}

	public void setResultatsQE(MetierResultatsTemporelSpatial resultatsQE) {
		if (!CGlobal.AVEC_QUALITE_DEAU) {
			resultatsQE_ = null;
			return;
		}
		if (resultatsQE == resultatsQE_) {
			return;
		}
		resultatsQE_ = resultatsQE;
	}

	@Override
	public void setObject(MetierHydraulique1d _n) {
		if (_n instanceof MetierResultatsTemporelSpatial) {
			MetierResultatsTemporelSpatial resultTempoSpatial
			= (MetierResultatsTemporelSpatial) _n;

			if (resultTempoSpatial.resultatsCasier()) {
				/*if (resultTempoSpatial == resultatsCasier_)
         return;
         resultatsCasier_ = resultTempoSpatial;*/
				pnHydro.setResultatsCasier(resultTempoSpatial);
			} else if (resultTempoSpatial.resultatsLiaison()) {
				/*if (resultTempoSpatial == resultatsLiaison_)
         return;
         resultatsLiaison_ = resultTempoSpatial;*/
				pnHydro.setResultatsLiaison(resultTempoSpatial);
			} //Utiliser uniquement (je pense) � l'ouverture d'un fichier r�sultats sans projet
			else if (resultTempoSpatial.resultatsTracer()) {
				if (resultTempoSpatial == resultatsQE_) {
					return;
				}
				resultatsQE_ = resultTempoSpatial;
				//panHydro.setResultats(resultTempoSpatial);
				pnQE.setResultats(resultTempoSpatial);
				
				
				
			} else {
				/*if (resultTempoSpatial == resultTempoSpatial_) {
					return;
				}*/
				resultTempoSpatial_ = resultTempoSpatial;
				pnHydro.setResultats(resultTempoSpatial_);
			}
			setValeurs();

		}
		if (_n instanceof MetierDonneesHydrauliques) {
			MetierDonneesHydrauliques donnneHyd = (MetierDonneesHydrauliques) _n;
			if (donnneHyd == donnneHyd_) {
				return;
			}
			donnneHyd_ = donnneHyd;
		}
		if (_n instanceof MetierReseau) {
			MetierReseau reseau = (MetierReseau) _n;
			if (reseau == reseau_) {
				return;
			}
			reseau_ = reseau;
		}
		if (_n instanceof MetierEtude1d) {
			pnProfils_.setEtude((MetierEtude1d) _n);
			pnProfilsLong_.setEtude((MetierEtude1d) _n);
			updateNavigation();
		}
		if (_n instanceof MetierCalageAuto) {
			donneeCalageAuto = (MetierCalageAuto) _n;		
			
		}
	}

	public PanneauGraphesResultats getSelectedProfLongPanel() {
		//((PanneauGraphesResultats) onglets_.getSelectedComponent().); => retourne le scrollPane
		if (onglets_.getSelectedComponent() == pnHydro) {
			return pnHydro;
		} else if (onglets_.getSelectedComponent() == pnQE) {
			return pnQE;
		} else {
			return null;
		}
	}

	public Hydraulique1dGrapheResuBief getSelectedGraph() {
		return getSelectedProfLongPanel().bgraphe_;
	}

	public BuList getListePasTpsSection() {
		return getSelectedProfLongPanel().lstPasTpsSection_;
	}

	public BuList getListeBiefCasierLiaison() {
		return getSelectedProfLongPanel().lstBiefCasierLiaison_;
	}

	/**
	 * La methode centrale qui permet d'imprimer (idem que celle de l'interface printable). Le format <code>_format</code> sera celui donne par la
	 * methode <code>Pageable.getPageFormat(int)</code>.
	 *
	 * @return <code>Printable.PAGE_EXISTS</code> si la page existe, sinon <code>Printable.NO_SUCH_PAGE</code>.
	 */
	@Override
	public int print(Graphics _g, PageFormat _format, int _page) {
		if (onglets_.getSelectedComponent() == pnQE) {
			return pnQE.bgraphe_.print(_g, _format, _page);
		} else if (onglets_.getSelectedComponent() == pnHydro) {
			return pnHydro.bgraphe_.print(_g, _format, _page);
		} else if (onglets_.getSelectedComponent() == pnProfils_) {
			return pnProfils_.print(_g, _format, _page);
		}else if (onglets_.getSelectedComponent() == pnProfilsLong_) {
			return pnProfilsLong_.print(_g, _format, _page);
		}
		return Printable.NO_SUCH_PAGE;
	}

	/**
	 * Set les valeurs et modifie l'interface en d�terminant le nombre d'onglets necessaires La diff�rence entre r�sultat temporels ou it�ratifs (calcul
	 * ou calage auto) est trait� dans le changeUI de la classe PanneauGrapheResultats
	 */
	@Override
	protected void setValeurs() {

		if (!CGlobal.AVEC_QUALITE_DEAU) {
			resultatsQE_ = null;
		}

		//S'il ya des resultats de qualit� d'eau...
		if (resultatsQE_ != null) {
			try {
				//On essaye la fusion
				MetierResultatsTemporelSpatial resFus = MetierResultatsTemporelSpatial.fusionnerDResultatsTemporelSpatial(resultTempoSpatial_, resultatsQE_);

				//Si la fusion a reussie ...
				//On fournit les resultats fusionn�s au pannel hydraulique et on modifie le titre de l'onglet
				pnHydro.setResultats(resFus);
				onglets_.setTitleAt(0, getS("R�sultats hydrauliques et qualit� d'eau"));
				//Si on arrive a fusionner les resultats , on supprime l'onglet QE eventuellment cr�� lors d'un calcul pr�c�dent avec fusion impossible
				if (pnQE != null) {
					onglets_.remove(pnQE);
				}

			} catch (Exception e) {

				//Si la fusion est impossible
				System.out.println(getS("Fusion des resultats Tracer et Mascaret impossible") + " :" + e.getMessage());
				//On cr�� le panel
				if (pnQE == null) {
					pnQE = new PanneauGraphesResultats(this);
					actionUpdateAuto.activatePnQeEvent();
					onglets_.insertTab(getS("R�sultats qualit� d'eau"), null, pnQE, null, 1);
					//On modifie le titre de l'onglet hydro
					onglets_.setTitleAt(0, getS("R�sultats hydrauliques"));
				}

				if (resultatsQE_ == null || resultatsQE_.resultatsBiefs() == null) {
					System.out.println(getS("Erreur dans la fonction SetValeur de l'editeur de r�sultat")
							+ " : " + getS("pas de resultats tracer disponibles !"));
				}

				//On fournit les valeurs de QE au panel QE, et on set les valeurs
				pnQE.setResultats(resultatsQE_);
				pnQE.setValeurs();
				pnQE.setResultatsLiaison(null);//Jamais de casier avec la qualit� d'eau
				pnQE.setResultatsCasier(null);

				
				if(pnQE != null) {
					pnQE.getLstVar().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
						@Override
						public void valueChanged(ListSelectionEvent e) {
							aniAction_.setEnabled(!pnQE.getLstVar().isSelectionEmpty());
							btAnim_.setEnabled(!pnQE.getLstVar().isSelectionEmpty());						
						}
					});
				}
				
				//e.printStackTrace();
			} finally {       //Dans tous les cas...
				//On set les valeurs hydrauliques et les profils si disponible
				pnHydro.setValeurs();
			}
		} else {
			//Le cas sans qualit� d'eau.........
			pnHydro.setValeurs();
			onglets_.setTitleAt(0, getS("R�sultats hydrauliques"));
		}

	}

	protected void personnaliser() {
		BuCommonInterface app
		= (BuCommonInterface) Hydraulique1dBaseApplication.FRAME;
		BDialogContent dial
		= new BDialogContent(
				app,
				this,
				getS("PERSONNALISER LE GRAPHE"),
				getSelectedGraph().getPersonnaliseurGraphe());
		dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
		dial.pack();
		dial.show();
	}

	public void visualiser(boolean calculBorne) {
		visualiser(calculBorne,false);
	}
	
	public void visualiser(boolean calculBorne, boolean silentMode) {
		if (onglets_.getSelectedComponent() == pnHydro) {
			pnHydro.visualiser(calculBorne, silentMode);
		} else if (onglets_.getSelectedComponent() == pnQE) {
			pnQE.visualiser(calculBorne, silentMode);
		}else if (onglets_.getSelectedComponent() == pnProfilsLong_) {
			pnProfilsLong_.visualiser(calculBorne, silentMode);	
		}
	}

	/**
	 * @return l'image produite dans la taille courante du composant.
	 */
	@Override
	public BufferedImage produceImage(Map _params) {
		return getSelectedGraph().produceImage(null);
	}

	@Override
	public BufferedImage produceImage(int _w, int _h, Map _params) {
		return getSelectedGraph().produceImage(_w, _h, null);
	}

	@Override
	public Dimension getDefaultImageDimension() {
		return getSelectedGraph().getDefaultImageDimension();
	}

	protected void lanceExcel() {
		try {
			visualiser(true);
			getSelectedGraph().lanceExcel();
		} catch (Exception ex) {
			ex.printStackTrace();
			FudaaCommonImplementation fci = (FudaaCommonImplementation) ((Hydraulique1dBaseApplication) Hydraulique1dBaseApplication.FRAME)
					.getImplementation();
			fci.error(getS("Ex�cution d'Excel"), getS("Un probl�me est survenu au lancement d'Excel"), false);
		}
	}

	protected void exporter() {
		final GrapheSaveFileChooser fileC = new GrapheSaveFileChooser();
		fileC.setDialogTitle(getS("Exportation du graphique"));
		if (fileC.showOpenDialog(this) != JFileChooser.APPROVE_OPTION) {
			return;
		}
		final File f = fileC.getSelectedFile();
		if (f == null) {
			return;
		}
		BuCommonImplementation bci = ((Hydraulique1dBaseApplication) Hydraulique1dBaseApplication.FRAME)
				.getImplementation();
		new BuTaskOperation(bci, getS("Exportation du graphique")) {
			@Override
			public void act() {
				setProgression(10);
				BufferedImage i = getSelectedGraph().produceImage(null);
				setProgression(20);
				getSelectedGraph().paintComponents(i.createGraphics());

				setProgression(40);
				String fmt = fileC.getSelectedFormat();
				File fToSave = f;
				if (!f.getName().toLowerCase().endsWith("." + fmt)) {
					fToSave = new File(f.getAbsolutePath() + "." + fmt);
				}
				FudaaCommonImplementation fci = (FudaaCommonImplementation) ((Hydraulique1dBaseApplication) Hydraulique1dBaseApplication.FRAME)
						.getImplementation();

				if (fToSave.exists()) {
					boolean b = fci.question(getS("Confirmation"), getS("Le fichier " + fToSave.getName() + " existe d�j�. Voulez-vous l'�craser?"));
					if (!b) {
						return;
					}
				}
				if ("txt".equals(fileC.getSelectedFormat())) {
					getSelectedGraph().exportTxt(fToSave);
				} else if ("xls".equals(fileC.getSelectedFormat())) {
					getSelectedGraph().exportExcel(fToSave);
				} else {
					if (CtuluImageExport.export(i, fToSave, fileC.getSelectedFormat(),
							fci)) {
						fci.setMainMessageAndClear(getS("Enregistrement image r�ussi"));
					}
				}
			}
		}.start();

	}

	protected List<GrapheComponent> getListLaisse(final int[] selectedVars) {
		List<GrapheComponent> listeLaisse = new ArrayList<GrapheComponent>();
		if (getSelectedProfLongPanel().cbLaisse_.isSelected()) {
			
			
			if(donnneHyd_ !=null && donnneHyd_.laisses() != null) {
			MetierLaisse[] laisses = donnneHyd_.laisses();
			//Si Profil spatial
			if (getSelectedProfLongPanel().rbSpatial_.isSelected()) {
				Vector<Valeur> vLaissesBief = new Vector<Valeur>();
				for (int j = getListeBiefCasierLiaison().getMinSelectionIndex(); j <= getListeBiefCasierLiaison().getMaxSelectionIndex(); j++) {
					if (j < reseau_.biefs().length) {
						//On recherche les laisses rattach�es au bief indiceBief
						for (int i = 0; i < laisses.length; i++) {
							if (laisses[i].site().biefRattache()
									== reseau_.biefs()[j]) {
								MetierLaisse l = laisses[i];
								Valeur v = new Valeur();
								v.s_ = l.site().abscisse();
								v.v_ = l.cote();
								v.titre_ = l.nom();
								vLaissesBief.add(v);
							}
						}
					}
				}
				
			
				CourbeDefault res = new CourbeDefault();
				res.marqueurs_ = true;
				res.titre_ = "laisse";
				res.type_ = "nuage";
				res.valeurs_ = vLaissesBief;
				listeLaisse.add(res);
				//Si profil temporel
			} else {
				NumberFormat nf2 = NumberFormat.getInstance(Locale.US);
				nf2.setMaximumFractionDigits(Hydraulique1dResource.getFractionDigits());
				nf2.setGroupingUsed(false);

				for (int j = getListeBiefCasierLiaison().getMinSelectionIndex(); j <= getListeBiefCasierLiaison().getMaxSelectionIndex(); j++) {
					if (j < reseau_.biefs().length) {
						//On recherche les laisses rattach�es au bief indiceBief
						for (int i = 0; i < laisses.length; i++) {
							if (laisses[i].site().biefRattache()
									== reseau_.biefs()[j]) {
								MetierLaisse l = laisses[i];
								Contrainte ContrainteLaisse = new Contrainte();
								ContrainteLaisse.titre_ = "Laisse " + l.nom() + " Z=" + nf2.format(l.cote());
								ContrainteLaisse.vertical_ = true;
								ContrainteLaisse.type_ = "max";
								ContrainteLaisse.v_ = l.cote();
								listeLaisse.add(ContrainteLaisse);
							}
						}
					}
				}

			}
		} else if(donneeCalageAuto != null) {
			//-- adrien hadoux - laisses de crues afficher uniquement celui correspondant � la variable affich�e --//
			
			int indiceVar=0;
			for(int var:selectedVars) {
				int nbCrues = donneeCalageAuto.crues().length;
				Vector<Valeur> vLaissesBief = new Vector<Valeur>();
			
				//-- adrien: pour retrouver le bon indice de valeur de crue � afficher,
				//-- il faut prendre le num�ro de variable modulo le nombre de crue
				//-- ainsi, par exemple, si on choisit le DEBIT TOTAL(2) qui est � la 6�me position,
				//-- on prend bien la valeur de la crue N� 2 -> 6%5 =1
				int indiceCrueValue = var%nbCrues;
				Valeur v = new Valeur();
				v.s_ = donneeCalageAuto.crues()[indiceCrueValue].debitAmont();
				v.v_ = donneeCalageAuto.crues()[indiceCrueValue].coteAval();
				v.titre_ = "" + (indiceCrueValue+1);
				vLaissesBief.add(v);

				CourbeDefault res = new CourbeDefault();
				res.marqueurs_ = true;
				res.titre_ = getS("Cote de la Crue")+" "+(indiceCrueValue+1);
				res.type_ = "nuage";
				//res.type_ = "courbe";
				res.aspect_.texte_ = Hydraulique1dAbstractGraphe.PALETTE[indiceVar % Hydraulique1dAbstractGraphe.PALETTE.length];
				//res.aspect_.surface_ = Hydraulique1dAbstractGraphe.PALETTE[indiceVar % Hydraulique1dAbstractGraphe.PALETTE.length];
				res.setAspectContour(Hydraulique1dAbstractGraphe.PALETTE[indiceVar % Hydraulique1dAbstractGraphe.PALETTE.length]);
				res.valeurs_ = vLaissesBief;
				listeLaisse.add(res);
				indiceVar++;
				}
			}
		}
		
		return listeLaisse;
	}

	private void deplaceVisu(int delta) {
		int indexeCourant = getListePasTpsSection().getSelectedIndex();
		int nouveauIndexe = indexeCourant + delta;
		int indexeMax = getListePasTpsSection().getModel().getSize() - 1;
		nouveauIndexe = Math.max(0, nouveauIndexe);
		nouveauIndexe = Math.min(indexeMax, nouveauIndexe);
		if (nouveauIndexe != indexeCourant) {
			getListePasTpsSection().setSelectedIndex(nouveauIndexe);
			visualiser(false);
		}
	}

	/**
	 * @param resBiefs DResultatsTemporelSpatialBief[]
	 * @param indicesVar int[]
	 * @param indicesPasTps int[]
	 * @return double[][]
	 */
	/*private final static double[][] getTableauResultatsSpatial(
   DResultatsTemporelSpatialBief[] resBiefs,
   int[] indicesVar,
   int[] indicesPasTps) {
   int nbCourbe= indicesVar.length * indicesPasTps.length;
   int nbBief = resBiefs.length;
   int nbSectionTotal = 0;
   ArrayList listeRes = new ArrayList(nbBief);
   for (int i = 0; i < nbBief; i++) {
   DResultatsTemporelSpatialBief resBief = resBiefs[i];
   double[][] resB= new double[nbCourbe + 1][];
   // l'abscisse : les sections
   resB[0]= resBief.abscissesSections();
   nbSectionTotal +=resB[0].length;
   // les ordonn�es
   double[][][] vals= resBief.valeursVariables();
   int indiceCourbe= 1;
   for (int j= 0; j < indicesVar.length; j++) {
   for (int k= 0; k < indicesPasTps.length; k++) {
   resB[indiceCourbe]= vals[indicesVar[j]][indicesPasTps[k]];
   indiceCourbe++;
   }
   }
   listeRes.add(resB);
   }
   double[][] res= new double[nbCourbe + 1][nbSectionTotal];
   for (int i = 0; i < res.length; i++) {
   int indiceDest = 0;
   for (int j = 0; j < nbBief; j++) {
   double[][] resB = (double[][])listeRes.get(j);
   double[] resBCbi = resB[i];
   int nbSectionsBief = resBCbi.length;
   System.arraycopy(resBCbi,0,res[i],indiceDest,nbSectionsBief);
   indiceDest += nbSectionsBief;
   }
   }
   return res;
   }*/
	/**
	 * @param resBiefs DResultatsTemporelSpatialBief[]
	 * @param indicesVar int[]
	 * @param indicesSection int[]
	 * @return double[][]
	 */
	/*private final static double[][] getTableauResultatsTemporels(
   DResultatsTemporelSpatialBief[] resBiefs,
   int[] indicesVar,
   int[] indicesSection,
   double[] pasTemps) {

   int nbPasTemps= pasTemps.length;
   int nbCourbe= indicesVar.length * indicesSection.length;
   double[][] res= new double[nbCourbe + 1][nbPasTemps];
   // l'abscisse : les pas de temps
   res[0]= pasTemps;
   // les ordonn�es
   double[][][] vals= resBiefs[0].valeursVariables();
   for (int i= 0; i < nbPasTemps; i++) {
   int indiceCourbe= 1;
   for (int j= 0; j < indicesVar.length; j++) {
   for (int k= 0; k < indicesSection.length; k++) {
   res[indiceCourbe][i]= vals[indicesVar[j]][i][indicesSection[k]];
   indiceCourbe++;
   }
   }
   }
   return res;
   }*/
	//#################################################################################################
	//#################################################################################################
	//#################################################################################################
	//#################################################################################################
	class GrapheAnimation implements EbliAnimationSourceInterface {

		private Hydraulique1dGraphesResultatsEditor editeur_;

		GrapheAnimation(Hydraulique1dGraphesResultatsEditor editeur) {
			editeur_ = editeur;
		}

		/**
		 * @return le nombre de pas de temps a parcourir
		 */
		@Override
		public int getNbTimeStep() {
			editeur_.getListePasTpsSection().getModel().getSize();
			return editeur_.getListePasTpsSection().getModel().getSize();
		}

		/**
		 * @param _idx indice du pas de temps [0;getNbTimeStep()[
		 * @return representation de ce pas de temps
		 */
		@Override
		public String getTimeStep(int _idx) {
			return editeur_.getListePasTpsSection().getModel().getElementAt(_idx).toString();
		}

		@Override
		public double getTimeStepValueSec(int _idx) {
			try {
				return Double.parseDouble(getTimeStep(_idx));
			} catch (NumberFormatException numberFormatException) {
			}
			return 0;
		}

		/**
		 * @param _idx l'indice a afficher
		 */
		@Override
		public void setTimeStep(int _idx) {
			editeur_.getListePasTpsSection().setSelectedIndex(_idx);
			editeur_.visualiser(false);
		}

		@Override
		public String getTitle() {
			return editeur_.getTitle();
		}

		/**
		 * Permet d'avertir le client que la video est en cours
		 *
		 * @param _b true si video en cours
		 */
		@Override
		public void setVideoMode(boolean _b) {
		}

		/**
		 * @return le composant contenant l'affichage (optionnel)
		 */
		@Override
		public Component getComponent() {
			return null;

		}

		/**
		 * @return l'image produite dans la taille courant du composant.
		 */
		@Override
		public BufferedImage produceImage(Map _params) {
			return editeur_.produceImage(null);
		}

		@Override
		public BufferedImage produceImage(int _w, int _h, Map _params) {
			return editeur_.produceImage(_w, _h, null);
		}

		/**
		 * @return les dimensions de l'image produite
		 */
		@Override
		public Dimension getDefaultImageDimension() {
			return editeur_.getSelectedGraph().getDefaultImageDimension();
		}
	}

	public PanneauGraphesResultats getPnHydro() {
		return pnHydro;
	}

	public void setPnHydro(PanneauGraphesResultats pnHydro) {
		this.pnHydro = pnHydro;
	}
	public PanneauGraphesResultats getPnQE() {
		return pnQE;
	}
	public void setPnQE(PanneauGraphesResultats pnQE) {
		this.pnQE = pnQE;
	}
}

//#################################################################################################
//#################################################################################################
//#################################################################################################
//#################################################################################################
class GrapheSaveFileChooser extends CtuluFileChooser {

	Map<BuFileFilter, String> filterFormat_;

	/**
	 *
	 */
	public GrapheSaveFileChooser() {
		super(true);
		super.setDialogType(SAVE_DIALOG);
		setFileHidingEnabled(false);
		setAcceptAllFileFilterUsed(false);
		BuFileFilter[] f = new BuFileFilter[CtuluImageExport.FORMAT_LIST.size() + 2];
		filterFormat_ = new HashMap<BuFileFilter, String>(f.length);
		int pngIDx = 0;
		for (int i = 0; i < (f.length - 2); i++) {
			String fmt = CtuluImageExport.FORMAT_LIST.get(i);
			if ("png".equals(fmt)) {
				pngIDx = i;
			}
			f[i] = new BuFileFilter(fmt, BuResource.BU.getString("Image") + CtuluLibString.ESPACE + fmt);
			filterFormat_.put(f[i], fmt);
			addChoosableFileFilter(f[i]);
		}
		String fmt = "txt";
		f[f.length - 2] = new BuFileFilter(fmt,
				Hydraulique1dResource.HYDRAULIQUE1D.getString("Texte (s�parateur") + " : " + "tabulation)" + CtuluLibString.ESPACE + fmt);
		filterFormat_.put(f[f.length - 2], fmt);
		addChoosableFileFilter(f[f.length - 2]);
		fmt = "xls";
		f[f.length - 1] = new BuFileFilter(fmt, Hydraulique1dResource.HYDRAULIQUE1D.getString("Feuille Excel") + CtuluLibString.ESPACE + fmt);
		filterFormat_.put(f[f.length - 1], fmt);
		addChoosableFileFilter(f[f.length - 1]);
		setFileFilter(f[pngIDx]);
	}

	public String getSelectedFormat() {
		return filterFormat_.get(getFileFilter());
	}

	/**
	 * Ne fait rien.
	 *
	 * @see javax.swing.JFileChooser#setDialogType(int)
	 */
	@Override
	public void setDialogType(int _dialogType) {
	}

	@Override
	protected JDialog createDialog(Component _parent) throws HeadlessException {
		return super.createDialog(_parent);
	}
}
