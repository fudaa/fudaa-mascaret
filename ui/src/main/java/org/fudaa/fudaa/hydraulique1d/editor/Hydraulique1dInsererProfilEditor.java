/*
 * @file         Hydraulique1dLissageProfilEditor.java
 * @creation     1999-12-28
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur pour le choix de la m�thode de lissage.
 * @author Bertrand Marchand
 * @version 1.0
 */
public class Hydraulique1dInsererProfilEditor extends JDialog {
  public static final int LISSAGE_MOYENNE=1;
  public static final int LISSAGE_MEDIANE=0;

  private JPanel pnPrinc_=new JPanel();
  private JPanel pnBoutons_=new JPanel();
  private BorderLayout lyThis_=new BorderLayout();
  private BuButton btOk_=new BuButton();
  private BuButton btAnnuler_=new BuButton();
  private BuGridLayout lyPrinc_=new BuGridLayout(3,5,2);
  public int reponse=0;
  private JPanel pnFormats=new JPanel();
  private Border bdpnFormats_=new TitledBorder("Format");
  private JPanel pnZones_=new JPanel();
  private Border bdpnZones_=new TitledBorder(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zones � remplacer"));
  private BuGridLayout lyFormats_=new BuGridLayout();
  private JRadioButton rbExcel_=new JRadioButton();
  private JRadioButton rbRivicad_=new JRadioButton();
  private JRadioButton rbMascaret_=new JRadioButton();
  private JRadioButton rbTexte_=new JRadioButton();
  private JRadioButton rbLido_=new JRadioButton();
  private BuGridLayout lyZones_=new BuGridLayout();
  private JCheckBox cbZoneMin_=new JCheckBox();
  private JCheckBox cbZoneStkGauche_=new JCheckBox();
  private JCheckBox cbZoneStkDroit_=new JCheckBox();
  private JCheckBox cbZoneMajGauche_=new JCheckBox();
  private JCheckBox cbZoneMajDroit_=new JCheckBox();
  private JPanel pnCotes_=new JPanel();
  private BuGridLayout lyCotes_=new BuGridLayout();
  private JLabel lbDummy1_=new JLabel();
  private JCheckBox cbCoteInf_=new JCheckBox();
  private BuTextField tfCoteInf_=BuTextField.createDoubleField();
  private JLabel lbCoteInfUnite_=new JLabel();
  private JLabel lbDummy2_=new JLabel();
  private JCheckBox cbCoteSup_=new JCheckBox();
  private BuTextField tfCoteSup_=BuTextField.createDoubleField();
  private JLabel lbCoteSupUnite_=new JLabel();

  /** Le dialogue de choix d'un fichier de profils */
//  private JFileChooser fc=new JFileChooser();
  /** Le profil import� */
  private Hydraulique1dProfilModel prof_=null;
  /** Le fichier selectionn� */
  private File file_=null;
  /** Le type de fichier selectionn�. */
  private int typeFile_=-1;
  /** Les extensions des fichiers d'import */
  private final static String[] EXT_FILES_={"txt","pro","pro","lid","xls"};

  public Hydraulique1dInsererProfilEditor(Frame parent) {
    super(parent);
    setModal(true);
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    customize();
    pack();
    setLocationRelativeTo(parent);
  }

  private void jbInit() throws Exception {
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Insertion d'un profil"));
    this.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        this_windowClosed(e);
      }
    }); this.getContentPane().setLayout(lyThis_);
    pnPrinc_.setLayout(lyPrinc_);
    btOk_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    btOk_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btOk_actionPerformed(e);
      }
    });
    btAnnuler_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    btAnnuler_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btAnnuler_actionPerformed(e);
      }
    });
    lyPrinc_.setColumns(1);
    pnFormats.setBorder(bdpnFormats_);
    pnFormats.setLayout(lyFormats_);
    pnZones_.setBorder(bdpnZones_);
    pnZones_.setLayout(lyZones_);
    rbExcel_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format profils Excel (.xls)"));
    rbExcel_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbExcel_actionPerformed(e);
      }
    });
    rbRivicad_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Rivicad (.lid)"));
    rbRivicad_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbRivicad_actionPerformed(e);
      }
    });
    rbMascaret_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Mascaret 5.0 (.pro)"));
    rbMascaret_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbMascaret_actionPerformed(e);
      }
    });
    rbTexte_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format texte (.txt)"));
    rbTexte_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbTexte_actionPerformed(e);
      }
    });
    rbLido_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Lido (.pro)"));
    rbLido_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbLido_actionPerformed(e);
      }
    });
    lyFormats_.setColumns(1);
    lyZones_.setColumns(1);
    cbZoneMin_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Lit mineur"));
    cbZoneMin_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateState();
      }
    });
    cbZoneMin_.setSelected(true);
    cbZoneStkGauche_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zone de stockage gauche"));
    cbZoneStkGauche_.setSelected(true);
    cbZoneStkDroit_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zone de stockage droite"));
    cbZoneStkDroit_.setSelected(true);
    cbZoneMajGauche_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Lit majeur gauche"));
    cbZoneMajGauche_.setSelected(true);
    cbZoneMajDroit_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Lit majeur droit"));
    cbZoneMajDroit_.setSelected(true);
    pnCotes_.setLayout(lyCotes_);
    lyCotes_.setColumns(4);
    lyCotes_.setHgap(3);
    cbCoteInf_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Uniquement les points de cote inf�rieure �"));
    cbCoteInf_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateState();
      }
    });
    lbCoteInfUnite_.setText("m");
    cbCoteSup_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Uniquement les points de cote sup�rieure �"));
    cbCoteSup_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateState();
      }
    });
    lbCoteSupUnite_.setText("m");
    lbDummy1_.setPreferredSize(new Dimension(12, 0));
    tfCoteSup_.setPreferredSize(new Dimension(70, 20));
    tfCoteInf_.setPreferredSize(new Dimension(70, 20));
    lbDummy2_.setPreferredSize(new Dimension(12, 0));
    this.getContentPane().add(pnPrinc_, java.awt.BorderLayout.CENTER);
    pnPrinc_.add(pnFormats);
    pnFormats.add(rbTexte_);
    pnFormats.add(rbLido_);
    pnFormats.add(rbMascaret_);
    pnFormats.add(rbRivicad_);
    pnFormats.add(rbExcel_);
    pnPrinc_.add(pnZones_);
    pnZones_.add(cbZoneMin_);
    pnZones_.add(pnCotes_);
    pnCotes_.add(lbDummy1_);
    pnCotes_.add(cbCoteInf_);
    pnCotes_.add(tfCoteInf_);
    pnCotes_.add(lbCoteInfUnite_);
    pnCotes_.add(lbDummy2_);
    pnCotes_.add(cbCoteSup_);
    pnCotes_.add(tfCoteSup_);
    pnCotes_.add(lbCoteSupUnite_);
    pnZones_.add(cbZoneMajDroit_);
    pnZones_.add(cbZoneMajGauche_);
    pnZones_.add(cbZoneStkDroit_);
    pnZones_.add(cbZoneStkGauche_);
    pnBoutons_.add(btOk_);
    pnBoutons_.add(btAnnuler_);
    this.getContentPane().add(pnBoutons_, java.awt.BorderLayout.SOUTH);
  }

  private void customize() {
    btOk_.setIcon(BuResource.BU.getIcon("valider"));
    btAnnuler_.setIcon(BuResource.BU.getIcon("annuler"));
    updateState();
  }

  private void btOk_actionPerformed(ActionEvent e) {
    reponse=1;
    prof_=obtainProfilFromFile();
    if (prof_!=null) setVisible(false);
  }

  private void btAnnuler_actionPerformed(ActionEvent e) {
    reponse=0;
    setVisible(false);
  }

  private void this_windowClosed(WindowEvent e) {
    reponse=0;
    setVisible(false);
  }

  private void rbTexte_actionPerformed(ActionEvent e) {
    typeFile_=0;
    file_=Hydraulique1dImport.chooseFile(EXT_FILES_[typeFile_]);
    updateState();
  }

  private void rbLido_actionPerformed(ActionEvent e) {
    typeFile_=1;
    file_=Hydraulique1dImport.chooseFile(EXT_FILES_[typeFile_]);
    updateState();
  }

  private void rbMascaret_actionPerformed(ActionEvent e) {
    typeFile_=2;
    file_=Hydraulique1dImport.chooseFile(EXT_FILES_[typeFile_]);
    updateState();
  }

  private void rbRivicad_actionPerformed(ActionEvent e) {
    typeFile_=3;
    file_=Hydraulique1dImport.chooseFile(EXT_FILES_[typeFile_]);
    updateState();
  }

  private void rbExcel_actionPerformed(ActionEvent e) {
    typeFile_=4;
    file_=Hydraulique1dImport.chooseFile(EXT_FILES_[typeFile_]);
    updateState();
  }

  private Hydraulique1dProfilModel obtainProfilFromFile() {
    String[] tpFiles={"TXT","PRO_LIDO","PRO_MASCARET","PRO_RIVICAD","PRO_EXCEL"};

    Hydraulique1dProfilModel[] profils=Hydraulique1dImport.importProfils(file_,tpFiles[typeFile_]);
    if ((profils == null)) {
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR")+": "+
        Hydraulique1dResource.HYDRAULIQUE1D.getString("l'importation de profils")+"\n" +
        Hydraulique1dResource.HYDRAULIQUE1D.getString("a �chou�."))
        .activate();
      return null;
    }
    if ((profils.length == 0)) {
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR")+": "+
        Hydraulique1dResource.HYDRAULIQUE1D.getString("aucun profil n'est")+"\n" +
        Hydraulique1dResource.HYDRAULIQUE1D.getString("disponible dans cet import!"))
        .activate();
      return null;
    }

    return profils[0];
  }

  private void updateState() {
    CtuluLibSwing.griserPanel(pnCotes_,cbZoneMin_.isSelected());
    if (cbZoneMin_.isSelected()) {
      tfCoteInf_.setEnabled(cbCoteInf_.isSelected());
      tfCoteSup_.setEnabled(cbCoteSup_.isSelected());
    }

    btOk_.setEnabled(file_!=null);

    rbTexte_   .setSelected(typeFile_==0 && file_!=null);
    rbLido_    .setSelected(typeFile_==1 && file_!=null);
    rbMascaret_.setSelected(typeFile_==2 && file_!=null);
    rbRivicad_ .setSelected(typeFile_==3 && file_!=null);
    rbExcel_   .setSelected(typeFile_==4 && file_!=null);
  }

  /**
   * @return Le profil lu.
   */
  public Hydraulique1dProfilModel getProfil() {
    return prof_;
  }

  /**
   * Efface le profil temporaire.
   */
  public void clearProfil() {
    file_=null;
    prof_=null;
    updateState();
  }

  /**
   * @return le masque de zone.
   */
  public int getMasqueZones() {
    int mask=Hydraulique1dProfilModel.MASK_ZONE_VIDE;
    if (cbZoneMin_.isSelected())       mask|=Hydraulique1dProfilModel.MASK_ZONE_MIN;
    if (cbZoneMajDroit_.isSelected())  mask|=Hydraulique1dProfilModel.MASK_ZONE_MAJ_DR;
    if (cbZoneMajGauche_.isSelected()) mask|=Hydraulique1dProfilModel.MASK_ZONE_MAJ_GA;
    if (cbZoneStkDroit_.isSelected())  mask|=Hydraulique1dProfilModel.MASK_ZONE_STK_DR;
    if (cbZoneStkGauche_.isSelected()) mask|=Hydraulique1dProfilModel.MASK_ZONE_STK_GA;

    return mask;
  }

  /**
   * @return La valeur de cote maxi pour les points du lit mineur a inserer. Null: Cette valeur n'est pas prise en compte.
   */
  public Double getCoteMax() {
    if (!cbZoneMin_.isSelected() || !cbCoteInf_.isSelected()) return null;

    Double r=null;
    try {
      r=new Double(tfCoteInf_.getText());
    }
    catch (NumberFormatException _exc) {}

    return r;
  }

  /**
   * @return La valeur de cote mini pour les points du lit mineur a inserer. Null: Cette valeur n'est pas prise en compte.
   */
  public Double getCoteMin() {
    if (!cbZoneMin_.isSelected() || !cbCoteSup_.isSelected()) return null;

    Double r=null;
    try {
      r=new Double(tfCoteSup_.getText());
    }
    catch (NumberFormatException _exc) {}

    return r;
  }

  /**
   * Pour tester l'ergonomie
   */
  public static void main(String[] _args) {
    Hydraulique1dInsererProfilEditor ed=new Hydraulique1dInsererProfilEditor(null);
    ed.setVisible(true);
    System.exit(0);
  }
}
