/*
 * @file         Hydraulique1dSitesEditor.java
 * @creation     2004-07-06
 * @modification $Date: 2007-11-20 11:42:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.event.ActionEvent;

import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauLaisseModel;

import com.memoire.bu.BuButton;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur des laisses de crues (MetierLaisse).<br>
 * Appeler si l'utilisateur clic sur le bouton "LAISSES DE CRUES" de l'�diteur des param�tres g�n�raux.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().LAISSE().editer().<br>
 * @version      $Revision: 1.7 $ $Date: 2007-11-20 11:42:50 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dLaissesEditor
    extends Hydraulique1dSitesEditor {

  BuButton btImporterCalage;

  public Hydraulique1dLaissesEditor() {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Laisses de crues"), new Hydraulique1dTableauLaisseModel(),
          EbliPreferences.DIALOG.CREER | EbliPreferences.DIALOG.SUPPRIMER |
          EbliPreferences.DIALOG.IMPORTER);
    if (CGlobal.AVEC_CALAGE_AUTO){
    btImporterCalage=(BuButton)addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Importer donn�es calage"),"IMPORTER_CALAGE");
    btImporterCalage.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Extrait les laisses de crues des donn�es hydraulique du calage"));
    btImporterCalage.addActionListener(this);
    }
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd = _evt.getActionCommand();
    if (cmd.equalsIgnoreCase("IMPORTER")) {
       ( (Hydraulique1dTableauLaisseModel) modele_).importer();
    }
    else if (cmd.equalsIgnoreCase("IMPORTER_CALAGE")) {
       ((Hydraulique1dTableauLaisseModel)modele_).transfererCruesCalage();
    }
    else {
      super.actionPerformed(_evt);
    }
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierReseau) {
      modele_.setReseau( (MetierReseau) _n);
    }
    else if (_n instanceof MetierDonneesHydrauliques) {
       ( (Hydraulique1dTableauLaisseModel) modele_).setModelMetier( (
          MetierDonneesHydrauliques) _n);
    }
    else if (_n instanceof MetierCalageAuto) {
      MetierCalageAuto cal=(MetierCalageAuto)_n;
      ((Hydraulique1dTableauLaisseModel)modele_).setCalageAuto(cal);
    }
  }

  /**
   * Override : Pour mettre a jour l'UI.
   */
  @Override
  public void setValeurs() {
	  if (CGlobal.AVEC_CALAGE_AUTO){
    btImporterCalage.setEnabled(
      ((Hydraulique1dTableauLaisseModel)modele_).isImportationCruesPossible());
	  }
    super.setValeurs();
  }
}
