/*
 * @file         Hydraulique1dLigneInitialeEditor.java
 * @creation     2000-12-22
 * @modification $Date: 2007-11-20 11:42:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauPoint;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.dodico.hydraulique1d.conv.ConvH1D_Masc;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneLigneDEauTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauLigneEauModel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur de la ligne d'eau initiale (MetierLigneEauInitiale).<br>
 * Appeler si l'utilisateur clic sur le bouton "LIGNE D'EAU INITIALE" de l'�diteur des conditions initiales.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().LIGNE_INITIALE().editer().<br>
 * @version      $Revision: 1.23 $ $Date: 2007-11-20 11:42:50 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dLigneInitialeEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuButton btRepriseResultat_;
  private BuPanel pnLigneEau_, pnBas_, pnNom_;
  private BuBorderLayout loLigneEau_, loNom_;
  private BuVerticalLayout loBas_;
  private BuScrollPane spLigneEau_;
  private BuTextField tfNom_;
  private Hydraulique1dTableauReel tabLigneEau_;
  private MetierLigneEauInitiale ligneEau_;
  private MetierEtude1d etude_;
  private MetierResultatsGeneraux resultatsGeneraux_;
  public Hydraulique1dLigneInitialeEditor() {
    this(null);
  }
  public Hydraulique1dLigneInitialeEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Ligne d'eau initiale"));
    ligneEau_= null;
    loBas_= new BuVerticalLayout(5, true, true);
    loLigneEau_= new BuBorderLayout(5, 5);
    loNom_= new BuBorderLayout(5, 5);
    Container pnMain_= getContentPane();
    pnBas_= new BuPanel();
    pnBas_.setLayout(loBas_);
    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    pnLigneEau_= new BuPanel();
    pnLigneEau_.setLayout(loLigneEau_);
    pnLigneEau_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    Hydraulique1dTableauLigneEauModel tabLigneEauModel= new Hydraulique1dTableauLigneEauModel();
    tabLigneEau_= new Hydraulique1dTableauReel(tabLigneEauModel);
    spLigneEau_= new BuScrollPane(tabLigneEau_);
    spLigneEau_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    setActionPanel(
      EbliPreferences.DIALOG.CREER
        | EbliPreferences.DIALOG.SUPPRIMER
        | EbliPreferences.DIALOG.IMPORTER
        | EbliPreferences.DIALOG.EXPORTER);
    btRepriseResultat_ = (BuButton)addAction(getS("REPRISE DERNIER RESULTAT"),"REPRISE_RESULTAT");
    btRepriseResultat_.setToolTipText(getS("R�cup�re la derni�re ligne d'eau du calcul pr�c�dent"));
    tfNom_= new BuTextField();
    tfNom_.setEditable(true);
    pnNom_.add(new BuLabel(getS("Nom de la ligne d'eau")+" : "), BorderLayout.WEST);
    pnNom_.add(tfNom_, BorderLayout.CENTER);
    int n= 0;
    pnBas_.add(pnNom_, n++);
    pnLigneEau_.add(spLigneEau_, BorderLayout.CENTER);
    pnLigneEau_.add(pnBas_, BorderLayout.SOUTH);
    pnMain_.add(pnLigneEau_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("ligneEauInitiale", null, ligneEau_);
      }
      fermer();
    } else if ("CREER".equals(cmd)) {
      creer();
    } else if ("SUPPRIMER".equals(cmd)) {
      supprimer();
    } else if ("IMPORTER".equals(cmd)) {
      importer();
    } else if ("EXPORTER".equals(cmd)) {
      exporter();
    } else if ("REPRISE_RESULTAT".equals(cmd)) {
      repriseResultat();
    } else {
      super.actionPerformed(_evt);
    }
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierLigneEauInitiale) {
      MetierLigneEauInitiale ligneEau= (MetierLigneEauInitiale)_n;
      if (ligneEau == ligneEau_)
        return;
      ligneEau_= ligneEau;
      ((Hydraulique1dTableauLigneEauModel)tabLigneEau_.getModel()).setModelMetier(ligneEau_);
      setValeurs();
    }
    if (_n instanceof MetierEtude1d) {
      etude_= (MetierEtude1d)_n;
    }
    if (_n instanceof MetierResultatsGeneraux) {
      resultatsGeneraux_= (MetierResultatsGeneraux)_n;
    }
  }
  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   * @param _g Graphics
   * @param _format PageFormat
   * @param _numPage int
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe, sinon
   *   <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _numPage) {
    return EbliPrinter.printComponent(_g, _format, tabLigneEau_, true, _numPage);
  }
  public void cut() {
    tabLigneEau_.cut();
  }
  public void copy() {
    tabLigneEau_.copy();
  }
  public void paste() {
    tabLigneEau_.paste();
  }
  public void duplicate() {
    tabLigneEau_.duplicate();
  }

  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    String nom= tfNom_.getText();
    if (!nom.equals(ligneEau_.nom())) {
      ligneEau_.nom(nom);
      changed= true;
    }
    Hydraulique1dTableauLigneEauModel tabModel=(Hydraulique1dTableauLigneEauModel)tabLigneEau_.getModel();
    if (tabModel.getValeurs()) {
      changed= true;
    }
    return changed;
  }

  @Override
  protected void setValeurs() {
    tfNom_.setText(ligneEau_.nom());
    ((Hydraulique1dTableauLigneEauModel)tabLigneEau_.getModel()).setValeurs();
    if (resultatsGeneraux_ == null)
      btRepriseResultat_.setEnabled(false);
    else if (resultatsGeneraux_.resultatsTemporelSpatial() == null)
      btRepriseResultat_.setEnabled(false);
    else if (resultatsGeneraux_.resultatsTemporelSpatial().pasTemps() == null)
      btRepriseResultat_.setEnabled(false);
    else if (resultatsGeneraux_.resultatsTemporelSpatial().pasTemps().length == 0)
      btRepriseResultat_.setEnabled(false);
    else
      btRepriseResultat_.setEnabled(true);
  }

  /**
   * R�cup�ration de la ligne d'eau initiale � partir du r�sultat courant au dernier pas de temps.
   */
  private void repriseResultat() {
    if (resultatsGeneraux_ == null) return;
    if (resultatsGeneraux_.resultatsTemporelSpatial() == null) return;
    MetierResultatsTemporelSpatial resTempoSpatial = resultatsGeneraux_.resultatsTemporelSpatial();
    Hydraulique1dLigneLigneDEauTableau[] tabLigne =
        Hydraulique1dLigneLigneDEauTableau.
        convertitResultatsTemporelSpatial_LigneLigneDEauTableau(resTempoSpatial);
    estimationNumeroBief(tabLigne);
    ((Hydraulique1dTableauLigneEauModel)tabLigneEau_.getModel()).setTabLignes(tabLigne);
    if (resultatsGeneraux_.resultatsTemporelCasier() == null) return;
    MetierResultatsTemporelSpatial resCasiers = resultatsGeneraux_.resultatsTemporelCasier();
    int choix = JOptionPane.showConfirmDialog(pnLigneEau_,
         getS("Souhaitez-vous �galement importer la cote initiale des casiers ?"),
         getS("Attention !"),
          JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
    if (choix==JOptionPane.NO_OPTION) return;
    int indiceCote= resCasiers.getIndiceVariable("ZCAS");
    if (indiceCote==-1)  indiceCote= resCasiers.getIndiceVariable("COTE");
    if (indiceCote==-1){
        JOptionPane.showMessageDialog(pnLigneEau_, getS("Pas de variable cote (ni ZCAS ni COTE) dans les r�sultats. Abandon de l'import des cotes initiales des casiers !")
                                      , getS("Erreur !"), JOptionPane.ERROR_MESSAGE);
        return;
    }
     int dernierPasTemps= resCasiers.resultatsBiefs()[0].valeursVariables()[indiceCote].length-1;
    MetierCasier[] casiers = etude_.reseau().casiers();
    if (casiers.length!=resCasiers.resultatsBiefs().length) {
    JOptionPane.showMessageDialog(pnLigneEau_,
     getS("Le nombre de casier dans les r�sultats ne correspond pas au nombre de casier dans le mod�le. Abandon de l'import des cotes initiales des casiers !")
     ,getS("Erreur !")
     ,JOptionPane.ERROR_MESSAGE);
    }else{
        for (int i = 0; i < casiers.length; i++) {
            casiers[i].coteInitiale(resCasiers.resultatsBiefs()[i].valeursVariables()[indiceCote][dernierPasTemps][0]);
        }
    }


  }


  /**
   * tester l'import Optyca et Rubens
   */
  private void importer() {
    MyImportChooser c= new MyImportChooser(Hydraulique1dBaseApplication.FRAME);
    c.show();
    final int choice= c.getChoice();
    if (choice == 0)
      return;
    String extension= "";
    if (choice == MyImportChooser.LID_PERM)
      extension= "lig";
    else if (choice == MyImportChooser.OPTY)
      extension= "opt";
    else if (choice == MyImportChooser.RUB)
      extension= "rub";
    else
      extension= "txt";
    File file= Hydraulique1dImport.chooseFile(extension);
    if (file == null)
      return;
    Hydraulique1dLigneLigneDEauTableau[] ligneEauPoint= null;
    if (choice == MyImportChooser.LID_PERM) {
      ligneEauPoint=
        Hydraulique1dImport.importLigneEau_Rub(file);
    } else if (choice == MyImportChooser.TXT) {
      ligneEauPoint= Hydraulique1dImport.importLigneEau_Txt(file);
    } else if (choice == MyImportChooser.OPTY) {
      ligneEauPoint= Hydraulique1dImport.importLigneEau_Opt(file);
    } else if (choice == MyImportChooser.RUB) {
      ligneEauPoint= Hydraulique1dImport.importLigneEau_Rub(file);
    }
    if ((ligneEauPoint == null)) {
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        getS("ERREUR")+": "+getS("l'importation de la ligne d'eau") + "\n" +
        getS("a �chou�."))
        .activate();
      return;
    }
    if ((ligneEauPoint.length == 0)) {
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        getS("ERREUR")+": "+getS("aucun point n'est")+"\n" +
        getS("disponible dans cette import!"))
        .activate();
      return;
    }
    estimationNumeroBief(ligneEauPoint);
    ((Hydraulique1dTableauLigneEauModel)tabLigneEau_.getModel()).setTabLignes(ligneEauPoint);
    tfNom_.setText(file.getName());
  }
  private void exporter() {

  boolean formatRubens = false;
  if (!etude_.paramGeneraux().profilsAbscAbsolu()) {

    MyExportChooser c= new MyExportChooser(Hydraulique1dBaseApplication.FRAME);
    c.show();
    final int choice= c.getChoice();
    if (choice == 0)
      return;
    formatRubens = (choice==MyExportChooser.RUB);


  }

    File fichier = Hydraulique1dExport.chooseFile( "lig");
    if (fichier==null) return;

    //Conversion
    if (fichier == null) return;
    List listePts = ((Hydraulique1dTableauLigneEauModel)tabLigneEau_.getModel()).getListePtsComplets();
    MetierLigneEauPoint[] lignes = new MetierLigneEauPoint[listePts.size()];
    Notifieur.getNotifieur().setEventMuet(true);
    for (int i = 0; i < lignes.length; i++) {
      lignes[i] = new MetierLigneEauPoint();
      lignes[i].numeroBief(((Hydraulique1dLigneLigneDEauTableau)listePts.get(i)).iBief());
      lignes[i].abscisse(((Hydraulique1dLigneLigneDEauTableau)listePts.get(i)).absc());
      lignes[i].cote(((Hydraulique1dLigneLigneDEauTableau)listePts.get(i)).cote());
      lignes[i].debit(((Hydraulique1dLigneLigneDEauTableau)listePts.get(i)).debit());
    }


    MetierResultatsTemporelSpatial ires = ConvH1D_Masc.convertitDLigneEauPoint_IResultatTemporelSpatial(lignes,
                           etude_.reseau(), etude_.paramResultats().decalage(),formatRubens);
    Notifieur.getNotifieur().setEventMuet(false);

    Hydraulique1dExport.exportRubens(fichier,ires);
  }


  private void estimationNumeroBief(Hydraulique1dLigneLigneDEauTableau[] pts) {

    Arrays.sort(pts);

    if (etude_.paramGeneraux().profilsAbscAbsolu()) return;

    boolean presenceAbscissesDouteuses = false;
    for (int i= 0; i < pts.length; i++) {
      MetierBief b= etude_.reseau().getBiefContenantAbscisse(pts[i].absc());
      if (b == null){
        presenceAbscissesDouteuses = true;
      }else {
        if (pts[i].indiceBief() != null) {
          if (pts[i].iBief() != (b.indice() + 1)) {
            presenceAbscissesDouteuses = true;
          }
        } else {
          return;
        }
      }
    }

    int choix = JOptionPane.showConfirmDialog(pnLigneEau_, getS("Certaines abscisses n'appartiennent pas au bief indiqu� !")+"\n"+
                                              getS(" Souhaitez-vous effectuer une conversion en abscisses relatives ?"),
                                               getS("Attention !"),
                                               JOptionPane.YES_NO_OPTION,
                                               JOptionPane.QUESTION_MESSAGE);
     if (choix == JOptionPane.YES_OPTION) {

         if (presenceAbscissesDouteuses) { //on essaye de convertir en relatif

             double abscisseRelative;
             double abscisseAbsolueDebutBief = 0;
             int biefCourant = -9999;
             for (int i = 0; i < pts.length; i++) {
                 if (biefCourant != pts[i].iBief()) {
                     biefCourant = pts[i].iBief();
                     abscisseAbsolueDebutBief = pts[i].absc();
                 }
                 abscisseRelative = etude_.reseau().getAbscisseRelative(pts[i].
                         iBief(), pts[i].absc(), abscisseAbsolueDebutBief);
                 pts[i].absc(abscisseRelative);
             }
         }
     }
  }
  private void creer() {
    tabLigneEau_.ajouterLigne();
  }
  private void supprimer() {
    tabLigneEau_.supprimeLignesSelectionnees();
  }
}

class MyExportChooser extends JDialog implements ActionListener {
  public final static int MASC= 0x01;
  public final static int RUB= 0x02;
  private int choice_;
  private JRadioButton cbMasc_, cbRub_;
  public MyExportChooser(Frame parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Choix du format d'exportation"),true);
    choice_= 0;
    getContentPane().setLayout(new BuVerticalLayout());
    String mssg="";
    mssg+=Hydraulique1dResource.HYDRAULIQUE1D.getString("Attention !");
    mssg+="\n";
    mssg+=Hydraulique1dResource.HYDRAULIQUE1D.getString("Mod�le en abscisse relative !");
    mssg+="\n  \n";
    mssg+=Hydraulique1dResource.HYDRAULIQUE1D.getString("Choisissez le format d'exportation selon vos besoins");
    BuLabelMultiLine pnMessage = new BuLabelMultiLine(mssg);

    BuPanel pn= new BuPanel();
    pn.setBorder(
      new CompoundBorder(
        new EmptyBorder(new Insets(5, 5, 5, 5)),
        new EtchedBorder()));
    pn.setLayout(new BuVerticalLayout());
    cbMasc_= new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Lido permanent pour Mascaret")+" : "+
                              Hydraulique1dResource.HYDRAULIQUE1D.getString("pas de conversion des abscisses"));
    cbMasc_.addActionListener(this);
    pn.add(cbMasc_, 0);
    cbRub_= new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Lido permanent pour Rubens")+" : "+
                             Hydraulique1dResource.HYDRAULIQUE1D.getString("conversion des abscisses en absolue"));
    cbRub_.addActionListener(this);
    pn.add(cbRub_, 1);
    int n= 0;
    getContentPane().add(pnMessage, n++);
    getContentPane().add(pn, n++);
    pack();
    setLocationRelativeTo(parent);
  }
  @Override
  public void actionPerformed(ActionEvent e) {
    Object src= e.getSource();
    if (src == cbMasc_) {
      choice_= MASC;
      dispose();
    } else if (src == cbRub_) {
      choice_= RUB;
      dispose();
    }
  }
  public int getChoice() {
    return choice_;
  }
}


class MyImportChooser extends JDialog implements ActionListener {
  public final static int OPTY= 0x01;
  public final static int LID_PERM= 0x02;
  public final static int TXT= 0x03;
  public final static int RUB= 0x04;
  private int choice_;
  private JRadioButton cbOpty_, cbLido_, cbTxt_, cbRub_;
  public MyImportChooser(Frame parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Choix du format d'importation"), true);
    choice_= 0;
    getContentPane().setLayout(new BuVerticalLayout());
    BuPanel pn= new BuPanel();
    pn.setBorder(
      new CompoundBorder(
        new EmptyBorder(new Insets(5, 5, 5, 5)),
        new EtchedBorder()));
    pn.setLayout(new BuVerticalLayout());
    cbOpty_= new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format optyca (.opt)"));
    cbOpty_.addActionListener(this);
    pn.add(cbOpty_, 0);
    cbLido_= new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format Lido 2.0 (.lig)"));
    cbLido_.addActionListener(this);
    pn.add(cbLido_, 1);
    cbTxt_= new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format texte (.txt)"));
    cbTxt_.addActionListener(this);
    pn.add(cbTxt_, 2);
    cbRub_= new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Format rubens (.rub)"));
    cbRub_.addActionListener(this);
    pn.add(cbRub_, 3);
    int n= 0;
    getContentPane().add(pn, n++);
    pack();
    setLocationRelativeTo(parent);
  }
  @Override
  public void actionPerformed(ActionEvent e) {
    Object src= e.getSource();
    if (src == cbOpty_) {
      choice_= OPTY;
      dispose();
    } else if (src == cbLido_) {
      choice_= LID_PERM;
      dispose();
    } else if (src == cbTxt_) {
      choice_= TXT;
      dispose();
    } else if (src == cbRub_) {
      choice_= RUB;
      dispose();
    }
  }
  public int getChoice() {
    return choice_;
  }
}
