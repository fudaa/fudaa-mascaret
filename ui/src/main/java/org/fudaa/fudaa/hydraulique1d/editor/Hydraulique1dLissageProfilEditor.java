/*
 * @file         Hydraulique1dLissageProfilEditor.java
 * @creation     1999-12-28
 * @modification $Date: 2007-02-21 16:33:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import org.fudaa.dodico.hydraulique1d.util.Hydraulique1DUtils;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;

/**
 * Editeur pour le choix de la m�thode de lissage.
 * @author Bertrand Marchand
 * @version 1.0
 */
public class Hydraulique1dLissageProfilEditor extends JDialog {

  private JPanel pnPrinc_=new JPanel();
  private Border bdpnPrinc_;
  private JPanel pnBoutons_=new JPanel();
  private BorderLayout lyThis_=new BorderLayout();
  private BuButton btOk_=new BuButton();
  private BuButton btAnnuler_=new BuButton();
  private BuGridLayout lyPrinc_=new BuGridLayout(3,5,2);
  private JLabel lbMethode_=new JLabel();
  private JComboBox coMethode_=new JComboBox();
  private JLabel lbDummy_=new JLabel();
  private JLabel lbLargeur_=new JLabel();
  private BuTextField tfLargeur_=BuTextField.createDoubleField();
  private JLabel lbLargeurUnite_=new JLabel();
  private int[] methods_=new int[]{Hydraulique1DUtils.LISSAGE_MEDIANE,Hydraulique1DUtils.LISSAGE_MOYENNE};
  public int reponse=0;

  public Hydraulique1dLissageProfilEditor(Frame parent) {
    super(parent);
    setModal(true);
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    customize();
    pack();
    setLocationRelativeTo(parent);
  }
  
  public void setChoosableMethods(int... _methods) {
    methods_=_methods;
    
    coMethode_.removeAllItems();
    for (int i=0; i<_methods.length; i++) {
      if (_methods[i]==Hydraulique1DUtils.LISSAGE_MEDIANE) {
        coMethode_.addItem(Hydraulique1dResource.HYDRAULIQUE1D.getString("Par m�diane glissante"));
      }
      else {
        coMethode_.addItem(Hydraulique1dResource.HYDRAULIQUE1D.getString("Par moyenne glissante"));
      }
    }
  }

  private void jbInit() throws Exception {
    bdpnPrinc_=BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
                                               BorderFactory.createEmptyBorder(5, 5, 5, 5));
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Lissage du profil"));
    this.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        this_windowClosed(e);
      }
    }); this.getContentPane().setLayout(lyThis_);
    pnPrinc_.setBorder(bdpnPrinc_);
    pnPrinc_.setLayout(lyPrinc_);
    btOk_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    btOk_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btOk_actionPerformed(e);
      }
    });
    btAnnuler_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    btAnnuler_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btAnnuler_actionPerformed(e);
      }
    });
    lbMethode_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("M�thode de lissage")+" :");
    lbLargeur_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Largeur de la fen�tre de lissage")+" :");
    lbLargeurUnite_.setText("m");
    tfLargeur_.setText("1.0");
    this.getContentPane().add(pnPrinc_, java.awt.BorderLayout.CENTER);
    pnPrinc_.add(lbMethode_);
    pnPrinc_.add(coMethode_);
    pnPrinc_.add(lbDummy_);
    pnPrinc_.add(lbLargeur_);
    pnPrinc_.add(tfLargeur_);
    pnPrinc_.add(lbLargeurUnite_);
    pnBoutons_.add(btOk_);
    pnBoutons_.add(btAnnuler_);
    this.getContentPane().add(pnBoutons_, java.awt.BorderLayout.SOUTH);
  }

  private void customize() {
    coMethode_.addItem(Hydraulique1dResource.HYDRAULIQUE1D.getString("Par m�diane glissante"));
    coMethode_.addItem(Hydraulique1dResource.HYDRAULIQUE1D.getString("Par moyenne glissante"));
    btOk_.setIcon(BuResource.BU.getIcon("valider"));
    btAnnuler_.setIcon(BuResource.BU.getIcon("annuler"));
  }

  public void btOk_actionPerformed(ActionEvent e) {
    reponse=1;
    setVisible(false);
  }

  public void btAnnuler_actionPerformed(ActionEvent e) {
    reponse=0;
    setVisible(false);
  }

  public void this_windowClosed(WindowEvent e) {
    reponse=0;
    setVisible(false);
  }

  /**
   * Retourne le type de m�thode utilis�e.
   * @return La m�thode utilis�e.
   */
  public int getMethode() {
    return methods_[coMethode_.getSelectedIndex()];
  }

  /**
   * Retourne la largeur de fenetre
   * @return La largeur.
   */
  public double getLargeur() {
    double r=0;
    try {
      r=Double.parseDouble(tfLargeur_.getText());
    }
    catch (NumberFormatException _exc) {}

    return r;
  }

  /**
   * Pour tester l'ergonomie
   */
  public static void main(String[] _args) {
    Hydraulique1dLissageProfilEditor ed=new Hydraulique1dLissageProfilEditor(null);
    ed.setVisible(true);
    System.exit(0);
  }
}
