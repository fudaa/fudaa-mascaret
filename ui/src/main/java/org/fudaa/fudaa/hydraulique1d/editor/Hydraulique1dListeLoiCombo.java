/*
 * @file         Hydraulique1dListeLoiCombo.java
 * @creation     2004-22-03
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.event.ActionListener;

import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuPanel;
/**
 * Panneau contenant une liste de lois hydrauliques selectionnable.<br>
 * Ce composant liste les lois hydrauliques disponible dans les donn�es hydrauliques suivant sont type.<br>
 * On peut filtrer suivant le type de lois (HYDROGRAMME, LIMNIGRAMME,
 * OUVERTURE_VANNE, SEUIL, GEOMETRIQUE et TARAGE) avec la m�thode setTypeLois.<br>
 * On selectionne une loi avec la m�thode setValeurs.<br>
 * On r�cup�re la loi s�lectionn�e par l'utilisateur par la m�thode getValeurs.<br>
 * @version      $Revision: 1.6 $ $Date: 2007-11-20 11:42:45 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dListeLoiCombo extends BuPanel {
  public static final int HYDRAULIQUE=0;
  public static final int HYDROGRAMME=1;
  public static final int LIMNIGRAMME=2;
  public static final int OUVERTURE_VANNE=3;
  public static final int SEUIL=4;
  public static final int GEOMETRIQUE=5;
  public static final int TARAGE=6;
  public static final int TRACER=7;
  private int typeLois_=HYDRAULIQUE;
  private BuComboBox cmbNomLoi_;
  private MetierDonneesHydrauliques donneesHydro_;

  /**
   * Constructeur, n�cessite le type de loi � filter.
   * @param typeLois Hydraulique1dListeLoiCombo.HYDRAULIQUE ou
   * Hydraulique1dListeLoiCombo.HYDROGRAMME ou Hydraulique1dListeLoiCombo.LIMNIGRAMME etc...
   */
  public Hydraulique1dListeLoiCombo(int typeLois) {
    typeLois_ = typeLois;
    cmbNomLoi_= new BuComboBox();
    cmbNomLoi_.setActionCommand("COMBO_LOI");
    super.getMinimumSize().width = 150;
    super.setLayout(new BuBorderLayout());
    super.add(cmbNomLoi_, BuBorderLayout.CENTER);
  }
  public void setDonneesHydro(MetierDonneesHydrauliques donneesHydro) {
    donneesHydro_ = donneesHydro;
  }
  public MetierLoiHydraulique getValeurs() {
    LoiHydrauliqueDelegue loiDelegue=
        (LoiHydrauliqueDelegue)cmbNomLoi_.getSelectedItem();
    if (loiDelegue == null) return null;
    return loiDelegue.loi();
  }
  public void setTypeLois(int typeLois) {
    typeLois_ = typeLois;
  }
  public void setValeurs(MetierLoiHydraulique loi) {
    cmbNomLoi_.setSelectedItem(new LoiHydrauliqueDelegue(loi));
  }
  public void initListeLoi() {
    cmbNomLoi_.removeAllItems();
    MetierLoiHydraulique[] lois= loisDisponibles();
    for (int i= 0; i < lois.length; i++) {
      cmbNomLoi_.addItem(new LoiHydrauliqueDelegue(lois[i]));
    }
  }
  @Override
  public void setEnabled(boolean enable) {
    super.setEnabled(enable);
    cmbNomLoi_.setEnabled(enable);
  }
  private MetierLoiHydraulique[] loisDisponibles() {
    if (donneesHydro_==null) return new MetierLoiHydraulique[0];
    switch (typeLois_) {
      case HYDRAULIQUE: return donneesHydro_.getToutesLoisSaufTracer();
      case HYDROGRAMME: return donneesHydro_.getLoisHydrogramme();
      case LIMNIGRAMME: return donneesHydro_.getLoisLimnigramme();
      case OUVERTURE_VANNE: return donneesHydro_.getLoisOuvertureVanne();
      case SEUIL: return donneesHydro_.getLoisSeuil();
      case GEOMETRIQUE: return donneesHydro_.getLoisGeometrique();
      case TARAGE: return donneesHydro_.getLoisTarage();
      case TRACER: return donneesHydro_.getLoisTracer();
      default:return new MetierLoiHydraulique[0];
    }
 }

 public void addActionListenerCombo(ActionListener l) {
     cmbNomLoi_.addActionListener(l);
    }
}
class LoiHydrauliqueDelegue {
  private MetierLoiHydraulique loi_;
  LoiHydrauliqueDelegue(MetierLoiHydraulique loi) {
    loi_= loi;
  }
  @Override
  public String toString() {
    return loi_.nom();
  }
  public MetierLoiHydraulique loi() {
    return loi_;
  }
  @Override
  public boolean equals(Object o) {
    if (o instanceof LoiHydrauliqueDelegue) {
      LoiHydrauliqueDelegue l= (LoiHydrauliqueDelegue)o;
      return (l.loi_ == loi_);
    } else if (o instanceof MetierLoiHydraulique) {
      MetierLoiHydraulique l= (MetierLoiHydraulique)o;
      return (l == loi_);
    } else
      return false;
  }

}
