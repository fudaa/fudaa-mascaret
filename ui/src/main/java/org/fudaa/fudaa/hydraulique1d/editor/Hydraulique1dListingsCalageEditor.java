/*
 * @file         Hydraulique1dListingsCalageEditor.java
 * @creation     2001-09-25
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierResultatsCalageAuto;
import org.fudaa.ebli.commun.EbliListModelLargeString;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des listings issus du calcul calage (MetierResultatsCalageAuto).<br>
 * Appel� si l'utilisateur clic sur le menu "Calage/R�sultats G�n�raux".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().LISTINGS_CALAGE().editer().<br>
 * @version      $Revision: 1.5 $ $Date: 2007-11-20 11:42:45 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public final class Hydraulique1dListingsCalageEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {

  BuTabbedPane tpResultats_;
  BuScrollPane spEcran_;
//  BuScrollPane spDamocle_;
  BuScrollPane spMascaret_;
  BuScrollPane spCalage_;
//  JList lsDamocle_;
  JList lsMascaret_;
  JList lsCalage_;
  JTextPane taEcran_;

  private MetierResultatsCalageAuto res_;

  public Hydraulique1dListingsCalageEditor() {
    this(null);
  }
  public Hydraulique1dListingsCalageEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("R�sultats du calage"));
    res_= null;
    tpResultats_= new BuTabbedPane();
    tpResultats_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));

    Font fontNew= new Font("DialogInput", Font.PLAIN, 12);
    taEcran_= new JTextPane();
    taEcran_.setEditable(false);
    taEcran_.setFont(fontNew);
    StyledDocument doc = taEcran_.getStyledDocument();
    Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
    Style normal = doc.addStyle("normal", def);
    StyleConstants.setFontFamily(def,"DialogInput");

    Style s = doc.addStyle("erreur", normal);
    StyleConstants.setForeground(s,Color.red);

    spEcran_= new BuScrollPane(taEcran_);

//    lsDamocle_= new JList();
//    lsDamocle_.setFont(fontNew);
//    spDamocle_= new BuScrollPane(lsDamocle_);

    lsMascaret_= new JList();
    lsMascaret_.setFont(fontNew);
    spMascaret_= new BuScrollPane(lsMascaret_);

    lsCalage_= new JList();
    lsCalage_.setFont(fontNew);
    spCalage_= new BuScrollPane(lsCalage_);

    tpResultats_.setPreferredSize(new Dimension(400, 600));
    tpResultats_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Messages � l'�cran"),spEcran_);
//    tpResultats_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Listing Damocl�s"),  spDamocle_);
    tpResultats_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Listing Mascaret"),  spMascaret_);
    tpResultats_.addTab(Hydraulique1dResource.HYDRAULIQUE1D.getString("Listing Calage"),    spCalage_);
    tpResultats_.setSelectedIndex(0);
    getContentPane().add(tpResultats_, BorderLayout.CENTER);

    setNavPanel(EbliPreferences.DIALOG.FERMER);
    pack();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("FERMER".equals(cmd)) {
      fermer();
    }
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierResultatsCalageAuto))  return;

    MetierResultatsCalageAuto res= (MetierResultatsCalageAuto)(_n);
    if (res==res_) return;

    res_=res;
  }

  public void setMessagesEcran(byte[] messageEcran) {
    StyledDocument doc = taEcran_.getStyledDocument();
    try {
      doc.remove(0, doc.getLength());
      if ( (messageEcran == null) || (messageEcran.length == 0)) {
        System.out.println("messageEcran vide");
      }
      else {
        System.out.println("messageEcran pas vide");
        doc.insertString(0, new String(messageEcran), doc.getStyle("normal"));
      }
    }
    catch (BadLocationException ex) {
      ex.printStackTrace();
    }
  }

  public void setMessagesEcranErreur(byte[] messageEcranErreur) {
    if ((messageEcranErreur != null) && (messageEcranErreur.length != 0)) {
      StyledDocument doc = taEcran_.getStyledDocument();
      try {
        doc.insertString(doc.getLength(), new String(messageEcranErreur), doc.getStyle("erreur"));
      }
      catch (BadLocationException ex) {
        ex.printStackTrace();
      }
    }
    else {
      System.out.println("messageEcranErreur vide");
    }
  }

//  public void setListingDamocle(byte[] listingDamoc) {
//    if ((listingDamoc == null) || (listingDamoc.length == 0)) {
//      tpResultats_.getComponent(1).setEnabled(false);
//    }
//    EbliListModelLargeString modele = new EbliListModelLargeString(listingDamoc);
//    lsDamocle_.setModel(modele);
//  }

  public void setListingMascaret(byte[] listing) {
    if ((listing == null) || (listing.length == 0)) {
      tpResultats_.getComponent(1).setEnabled(false);
    }
    EbliListModelLargeString modele = new EbliListModelLargeString(listing);
    lsMascaret_.setModel(modele);
  }

  public void setListingCalage(byte[] _lis) {
    if ((_lis == null) || (_lis.length == 0)) {
      tpResultats_.getComponent(2).setEnabled(false);
    }
    EbliListModelLargeString modele = new EbliListModelLargeString(_lis);
    lsCalage_.setModel(modele);
  }

  @Override
  protected boolean getValeurs() {
    return false;
  }

  @Override
  protected void setValeurs() {
    setMessagesEcran(res_.messagesEcran());
    setMessagesEcranErreur(res_.messagesEcranErreur());
//    setListingDamocle(res_.listingDamocles());
    setListingMascaret(res_.listingMascaret());
    setListingCalage(res_.listingCalage());
  }
}
