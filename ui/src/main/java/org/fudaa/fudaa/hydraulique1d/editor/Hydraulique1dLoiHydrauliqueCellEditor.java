/*
 * @file         Hydraulique1dLoiHydrauliqueCellEditor.java
 * @creation     1999-10-10
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JToolTip;
import javax.swing.JWindow;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Plus utilis�.
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class Hydraulique1dLoiHydrauliqueCellEditor
  extends JComboBox
  implements TableCellEditor {
  private MetierLoiHydraulique[] lois_;
  JWindow popup_;
  public Hydraulique1dLoiHydrauliqueCellEditor() {
    super();
    lois_= null;
    MyLoiHydrauliqueRenderer r= new MyLoiHydrauliqueRenderer();
    final JToolTip t= new JToolTip();
    popup_= new JWindow();
    //popup_.setTitle(null);
    popup_.getContentPane().setLayout(new BorderLayout());
    //popup_.setBorder(new EmptyBorder(new Insets(0,0,0,0)));
    popup_.getContentPane().add(BorderLayout.CENTER, t);
    r.addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent e) {
        if ("selected".equals(e.getPropertyName())) {
          if (e.getNewValue() instanceof MetierLoiHydraulique) {
            MetierLoiHydraulique item= (MetierLoiHydraulique)e.getNewValue();
            t.setTipText(item.nom());
          } else {
            t.setTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Cr�er une nouvelle loi"));
          }
          popup_.invalidate();
          popup_.doLayout();
          popup_.setSize(t.getPreferredSize());
          Point p= getLocationOnScreen();
          p.y += (1 + ((Integer)e.getOldValue()).intValue())
            * (getHeight() + 1);
          p.x += getWidth();
          popup_.setLocation(p);
          if (popup_.isShowing())
            popup_.repaint();
          else {
            popup_.pack();
            popup_.show();
            popup_.toFront();
          }
          popup_.setLocation(p);
        }
      }
    });
    this.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (popup_.isShowing())
          popup_.hide();
      }
    });
    setRenderer(r);
    addItem(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nouveau"));
  }
  @Override
  public void hidePopup() {
    System.err.println("hidePopup");
    if (popup_.isShowing())
      popup_.hide();
    super.hidePopup();
  }
  public void setLoisHydrauliques(MetierLoiHydraulique[] o) {
    if ((o == lois_) || (o == null))
      return;
    //DLoiHydraulique[] vp= lois_;
    lois_= o;
    removeAllItems();
    for (int i= 0; i < lois_.length; i++) {
      addItem(lois_[i]);
      System.err.println(lois_[i].numero() + " added");
    }
    addItem(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nouveau"));
    // firePropertyChange("loiclm", vp, lois_);
  }
  // cellEditor
  @Override
  public Component getTableCellEditorComponent(
    JTable table,
    Object value,
    boolean isSelected,
    int row,
    int column) {
    if (column == 1) {
      if (lois_ != null) {
        setSelectedItem(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nouveau"));
        int num= ((Integer)value).intValue();
        for (int i= 0; i < lois_.length; i++)
          if (lois_[i].numero() == num)
            setSelectedIndex(i);
      }
      return this;
    }
    return new JLabel("???");
  }
  private Vector list_= new Vector();
  @Override
  public void addCellEditorListener(CellEditorListener l) {
    if (!list_.contains(l))
      list_.add(l);
  }
  @Override
  public void removeCellEditorListener(CellEditorListener l) {
    if (list_.contains(l))
      list_.remove(l);
  }
  @Override
  public void cancelCellEditing() {
    for (int i= 0; i < list_.size(); i++)
      ((CellEditorListener)list_.get(i)).editingCanceled(new ChangeEvent(this));
  }
  @Override
  public Object getCellEditorValue() {
    return getSelectedItem();
  }
  @Override
  public boolean isCellEditable(EventObject e) {
    return true;
  }
  @Override
  public boolean shouldSelectCell(EventObject e) {
    return true;
  }
  @Override
  public boolean stopCellEditing() {
    for (int i= 0; i < list_.size(); i++)
      ((CellEditorListener)list_.get(i)).editingStopped(new ChangeEvent(this));
    return true;
  }
}
class MyLoiHydrauliqueRenderer extends JLabel implements ListCellRenderer {
  public MyLoiHydrauliqueRenderer() {
    setOpaque(true);
  }
  @Override
  public Component getListCellRendererComponent(
    JList list,
    Object value,
    int index,
    boolean isSelected,
    boolean cellHasFocus) {
    if (value instanceof String)
      setText((String)value);
    else {
      MetierLoiHydraulique item= (MetierLoiHydraulique)value;
      setText("" + item.numero());
    }
    setBackground(
      isSelected
        ? UIManager.getColor("List.selectionBackground")
        : UIManager.getColor("List.background"));
    setForeground(
      isSelected
        ? UIManager.getColor("List.selectionForeground")
        : UIManager.getColor("List.foreground"));
    if (isSelected)
      firePropertyChange("selected", new Integer(index), value);
    return this;
  }
}
