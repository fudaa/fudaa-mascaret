/*
 * @file         Hydraulique1dLoiHydrauliqueChooser.java
 * @creation     1999-10-09
 * @modification $Date: 2006-09-08 16:04:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.Container;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;

import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Boite de dialogue permettant de s�lectionner le type de loi � cr�er.<br>
 * Utiliser par le classe Hydraulique1dIHM_LoiHydraulique2.
 * @version      $Revision: 1.8 $ $Date: 2006-09-08 16:04:37 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public class Hydraulique1dLoiHydrauliqueChooser extends BDialog {

  public static final int MASCARET= 6;
  JRadioButton btTarage_, btLimni_, btHydro_, btLimniHydro_, btRegulation_;
  JRadioButton btSeuil_, btOuvertureVanne_, btGeometrique_;
  BuGridLayout loBoutons_;


  int permanent_;
  public Hydraulique1dLoiHydrauliqueChooser(int permanent) {
    super((BuCommonInterface)Hydraulique1dBaseApplication.FRAME);
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi hydraulique"));
    setLocationRelativeTo(Hydraulique1dBaseApplication.FRAME);
    permanent_= permanent;
    init();
  }
  public Hydraulique1dLoiHydrauliqueChooser(
    IDialogInterface parent,
    int permanent) {
    super((BuCommonInterface)Hydraulique1dBaseApplication.FRAME, parent);
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi hydraulique"));
    setLocationRelativeTo(parent.getComponent());
    permanent_= permanent;
    init();
  }
  private void init() {

    Container content= getContentPane();
    loBoutons_= new BuGridLayout(2, 5, 5, true, true);
    loBoutons_.setCfilled(false);
    content.setLayout(loBoutons_);

    int n = 0;
    btHydro_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Hydrogramme Q(t)"));
    btHydro_.setActionCommand("HYDRO");
    content.add(btHydro_, n++);
    btLimni_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Limnigramme Z(t)"));
    btLimni_.setActionCommand("LIMNI");
    content.add(btLimni_, n++);
    btLimniHydro_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Limnihydrogramme Z,Q(t)"));
    btLimniHydro_.setActionCommand("LIMNIHYDRO");
    content.add(btLimniHydro_, n++);
    btTarage_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Courbe de tarage (Z,Q)"));
    btTarage_.setActionCommand("TARAGE");
    content.add(btTarage_, n++);
    btRegulation_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("R�gulation (Qam,Zav)"));
    btRegulation_.setActionCommand("REGULATION");
    content.add(btRegulation_, n++);
    btSeuil_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Seuil Zam")+"="+
                                Hydraulique1dResource.HYDRAULIQUE1D.getString("f(Zav,Q)"));
    btSeuil_.setActionCommand("SEUIL");
    content.add(btSeuil_, n++);
    btOuvertureVanne_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Ouverture vanne Zinf,Zsup")+"=f(t)");
    btOuvertureVanne_.setActionCommand("VANNE");
    content.add(btOuvertureVanne_, n++);
    btGeometrique_ = new JRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Profil de cr�te"));
    btGeometrique_.setActionCommand("GEOMETRIQUE");
    content.add(btGeometrique_, n++);


    pack();
  }
  public void addActionListener(ActionListener l) {
    btHydro_.addActionListener(l);
      btLimni_.addActionListener(l);
      btTarage_.addActionListener(l);
      btLimniHydro_.addActionListener(l);
      btRegulation_.addActionListener(l);
      btSeuil_.addActionListener(l);
      btOuvertureVanne_.addActionListener(l);
      btGeometrique_.addActionListener(l);
  }
  public void removeActionListener(ActionListener l) {
    btHydro_.removeActionListener(l);
      btLimni_.removeActionListener(l);
      btTarage_.removeActionListener(l);
      btLimniHydro_.removeActionListener(l);
      btRegulation_.removeActionListener(l);
      btSeuil_.removeActionListener(l);
      btOuvertureVanne_.removeActionListener(l);
    }
}
