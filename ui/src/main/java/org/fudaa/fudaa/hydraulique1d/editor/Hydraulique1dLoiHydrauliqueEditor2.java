/*
 * @file         Hydraulique1dLoiHydrauliqueEditor2.java
 * @creation     2001-03-26
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.io.File;
import java.util.Iterator;

import javax.swing.ButtonGroup;
import javax.swing.JSplitPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresModeleQualiteEau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneReelTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReelModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'une loi hydraulique (DLoiHydraulique).<br>
 * Appeler si l'utilisateur clic sur le bouton "EDITER" du gestionnaire des lois hydrauliques.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().LOI_HYDRAULIQUE2().editer().<br>
 * @version      $Revision: 1.21 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dLoiHydrauliqueEditor2
  extends Hydraulique1dCustomizerImprimable implements BuCutCopyPasteInterface {
  private BuPanel  pnGauche_, pnNom_, pnTarageAmont_, pnTemps_;
  private JSplitPane sptLoi_;
  private BuBorderLayout loNom_;
  private BuVerticalLayout loGauche_;
  private BuHorizontalLayout loTarageAmont_,loTemps_;
  private BuScrollPane spLoi_;
  private BuTextField tfNom_;
  private BuRadioButton rbAmont_, rbAval_, rbSeconde_, rbMinute_, rbHeure_, rbJour_;
  private BuLabel lbType_;
  private Hydraulique1dGrapheTableau graphe_;
  private String[] titreCourbes_= null;
  private LoiHydrauliqueTable table_;
  private LoiHydrauliqueTableModel tableModel_;
  private MetierLoiHydraulique loi_;
  private MetierDonneesHydrauliques donneesHydrauliques_;
  private MetierParametresModeleQualiteEau modeleQE_;
  private boolean estNouvelleLoi_;
  public Hydraulique1dLoiHydrauliqueEditor2() {
    this(null);
  }
  public Hydraulique1dLoiHydrauliqueEditor2(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi hydraulique"));
    donneesHydrauliques_ = null;
    estNouvelleLoi_ = false;
    loi_= null;
    modeleQE_=null;
    loGauche_= new BuVerticalLayout(5, true, true);
    loTarageAmont_= new BuHorizontalLayout();
    loTemps_= new BuHorizontalLayout();
    loNom_= new BuBorderLayout(5, 5);
    Container pnMain_= getContentPane();
    pnGauche_= new BuPanel();
    pnGauche_.setLayout(loGauche_);

    // panel des 2 radio bouton "Z=f(Q)" et "Q=f(Z)"
    pnTarageAmont_ = new BuPanel();
    pnTarageAmont_.setLayout(loTarageAmont_);
    rbAmont_ = new BuRadioButton("Z=f(Q)");
    rbAval_ = new BuRadioButton("Q=f(Z)");
    ButtonGroup groupeRadioBouton= new ButtonGroup();
    groupeRadioBouton.add(rbAmont_);
    groupeRadioBouton.add(rbAval_);
    pnTarageAmont_.add(rbAmont_);
    pnTarageAmont_.add(rbAval_);

    tableModel_= new LoiHydrauliqueTableModel();

    // panel des 4 radio bouton "sec", "min", "heure" et "Jour"
    pnTemps_ = new BuPanel();
    pnTemps_.setLayout(loTemps_);
    rbSeconde_ = new BuRadioButton("sec.");
    rbSeconde_.addActionListener(tableModel_);
    rbSeconde_.setActionCommand("SECONDE");
    rbMinute_ = new BuRadioButton("min.");
    rbMinute_.addActionListener(tableModel_);
    rbMinute_.setActionCommand("MINUTE");
    rbHeure_ = new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("heure"));
    rbHeure_.addActionListener(tableModel_);
    rbHeure_.setActionCommand("HEURE");
    rbJour_ = new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("jour"));
    rbJour_.addActionListener(tableModel_);
    rbJour_.setActionCommand("JOUR");
    ButtonGroup groupeRadioTemps= new ButtonGroup();
    groupeRadioTemps.add(rbSeconde_);
    groupeRadioTemps.add(rbMinute_);
    groupeRadioTemps.add(rbHeure_);
    groupeRadioTemps.add(rbJour_);
    groupeRadioTemps.setSelected(rbSeconde_.getModel(),true);

    pnTemps_.add(rbSeconde_);
    pnTemps_.add(rbMinute_);
    pnTemps_.add(rbHeure_);
    pnTemps_.add(rbJour_);

    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    graphe_= new Hydraulique1dGrapheTableau();
    table_= new LoiHydrauliqueTable(tableModel_);
    table_.setPreferredScrollableViewportSize(new Dimension(250, 70));
    table_.addPropertyChangeListener(graphe_);
    table_.getModel().addTableModelListener(graphe_);
    spLoi_= new BuScrollPane(table_);
    spLoi_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    tfNom_= new BuTextField();
    tfNom_.setEditable(true);
    tfNom_.addActionListener(this);
    tfNom_.setActionCommand("NOM");
    pnNom_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nom de loi")+" : "), BorderLayout.WEST);
    pnNom_.add(tfNom_, BorderLayout.CENTER);
    lbType_= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi Hydraulique"));
    int n= 0;
    pnGauche_.add(lbType_, n++);
    pnGauche_.add(pnNom_, n++);
    pnGauche_.add(pnTarageAmont_, n++);
    pnGauche_.add(pnTemps_, n++);
    pnGauche_.add(spLoi_, n++);
    sptLoi_= new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnGauche_, graphe_);
    sptLoi_.setBorder(
      new CompoundBorder(
      new EtchedBorder(),
      new EmptyBorder(new Insets(5, 5, 5, 5))));

    sptLoi_.setOneTouchExpandable(true);
    pnMain_.add(sptLoi_, BorderLayout.CENTER);
    setActionPanel(
      EbliPreferences.DIALOG.CREER
        | EbliPreferences.DIALOG.SUPPRIMER
        | EbliPreferences.DIALOG.IMPORTER);
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("D�selectionner"), "DESELECTIONNER");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Editer Axes"), "EDITER_AXES");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Personnaliser Graphe"), "PERSONNALISER_GRAPHE");
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("loi", null, loi_);
      }
      fermer();
    } else if ("ANNULER".equals(cmd)) {
      if (estNouvelleLoi_) {
          MetierLoiHydraulique[] tabLoi= new MetierLoiHydraulique[1];
          tabLoi[0]= loi_;
    	  donneesHydrauliques_.supprimeLois(tabLoi);
    	  firePropertyChange("donneesHydrauliques", null, donneesHydrauliques_);
      }
      fermer();
    } else if ("CREER".equals(cmd)) {
      creer();
    } else if ("SUPPRIMER".equals(cmd)) {
      supprimer();
    } else if ("IMPORTER".equals(cmd)) {
      importer();
    } else if ("DESELECTIONNER".equals(cmd)) {
      table_.clearSelection();
    } else if ("NOM".equals(cmd)) {
      graphe_.setLabels(tfNom_.getText(), null, null, null, null);
    } else if ("EDITER_AXES".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          "EDITER LES AXES",
          graphe_.getEditeurAxes());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else if ("PERSONNALISER_GRAPHE".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          Hydraulique1dResource.HYDRAULIQUE1D.getString("PERSONNALISER LE GRAPHE"),
          graphe_.getPersonnaliseurGraphe());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    }
  }
  @Override
  public boolean getValeurs() {
    boolean changed= false;
    String nom= tfNom_.getText();
    if (!nom.equals(loi_.nom())) {
      loi_.nom(nom);
      changed= true;
    }
    if (loi_ instanceof MetierLoiTarage) {
      MetierLoiTarage loiTarage = (MetierLoiTarage)loi_;
      boolean amont = rbAmont_.isSelected();
      if (amont != loiTarage.amont()) {
        loiTarage.amont(amont);
        changed= true;
      }
    }
    double[][] tabRes = tableModel_.getTabDouble1ereColSec();
    loi_.setPoints(tabRes);
    changed= true;
    return changed;
  }


  @Override
  public void setObject(MetierHydraulique1d _n) {
      if (_n instanceof MetierDonneesHydrauliques) {
    	  MetierDonneesHydrauliques donneesHydrauliques = (MetierDonneesHydrauliques) _n;
          if (donneesHydrauliques == donneesHydrauliques_)
              return;
          donneesHydrauliques_ = donneesHydrauliques;
      } else if (_n instanceof MetierLoiHydraulique) {
          MetierLoiHydraulique loi = (MetierLoiHydraulique) _n;
          if (loi == loi_)
              return;
          loi_ = loi;
      } else if (_n instanceof MetierParametresModeleQualiteEau) {
          MetierParametresModeleQualiteEau modeleQE = (MetierParametresModeleQualiteEau) _n;
          if (modeleQE == modeleQE_)
              return;
          modeleQE_ = modeleQE;
          setDescriptionTableau();
          setValeurs();
          table_.addPropertyChangeListener(graphe_);

      } else {
          return;
      }

  }
  @Override
  public void setValeurs() {
    tfNom_.setText(loi_.nom());
    String type= loi_.typeLoi();
    if ("Tarage".equals(type)) {
      pnTarageAmont_.setVisible(true);
      pnTemps_.setVisible(false);
      MetierLoiTarage loiTarage = (MetierLoiTarage) loi_;
      rbAmont_.setSelected(loiTarage.amont());
      rbAval_.setSelected(!loiTarage.amont());
    } else {
      pnTarageAmont_.setVisible(false);
      if ("Regulation".equals(type)) {
        pnTemps_.setVisible(false);
      }
      else if ("Geometrique".equals(type)) {
        pnTemps_.setVisible(false);
      }
      else {
        pnTemps_.setVisible(true);
      }
    }
    setDescriptionGraphe();
    double[][] tab= loi_.pointsToDoubleArray();
    graphe_.affiche(CtuluLibArray.transpose(tab) , titreCourbes_);
    tableModel_.setTabDouble(tab);
  }
  public String[] getEnabledActions() {
    String[] r= new String[] { "IMPRIMER", "PREVISUALISER", "MISEENPAGE" };
    return r;
  }
  protected void creer() {
    table_.ajouterLigne();
  }
  protected void supprimer() {
    table_.supprimeLignesSelectionnees();
  }
  protected void importer() {
    File file= Hydraulique1dImport.chooseFile("loi");
    if (file == null)
      return;
    double[][] tableauLoi=
      Hydraulique1dImport.importTableauLoiHydraulique(file, loi_);
    if ((tableauLoi == null)) {
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR")+": "+getS("l'importation de la loi")+"\n"
        + Hydraulique1dResource.HYDRAULIQUE1D.getString("a �chou�."))
        .activate();
      return;
    }
    if ((tableauLoi.length == 0)) {
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR")+": "+getS("aucun point n'est")+"\n" +
        Hydraulique1dResource.HYDRAULIQUE1D.getString("disponible dans cette import!"))
        .activate();
      return;
    }
    tableModel_.setTabDouble(tableauLoi);
  }
  public void setEstNouvelleLoi(boolean b) {
	  estNouvelleLoi_ = b;
  }
  protected void setDescriptionTableau() {
    String type= loi_.typeLoi();
    if ("Tarage".equals(type)) {
      lbType_.setText(getS("Courbe de tarage"));
      tableModel_.columnNames= new String[2];
      tableModel_.columnNames[0]= getS("D�bit (m3/s)");
      tableModel_.columnNames[1]= getS("Cote (m)");
    } else if ("Limnigramme".equals(type)) {
      lbType_.setText(getS("Limnigramme"));
      tableModel_.columnNames= new String[2];
      tableModel_.columnNames[0]= getS("Temps");
      tableModel_.columnNames[1]= getS("Cote (m)");
    } else if ("Hydrogramme".equals(type)) {
      lbType_.setText(getS("Hydrogramme"));
      tableModel_.columnNames= new String[2];
      tableModel_.columnNames[0]= getS("Temps");
      tableModel_.columnNames[1]= getS("D�bit (m3/s)");
    } else if ("LimniHydrogramme".equals(type)) {
      lbType_.setText(getS("Limnihydrogramme"));
      tableModel_.columnNames= new String[3];
      tableModel_.columnNames[0]= getS("Temps");
      tableModel_.columnNames[1]= getS("Cote (m)");
      tableModel_.columnNames[2]= getS("D�bit (m3/s)");
    } else if ("Regulation".equals(type)) {
      lbType_.setText(getS("Loi r�gulation"));
      tableModel_.columnNames= new String[2];
      tableModel_.columnNames[0]= getS("D�bit amont (m3/s)");
      tableModel_.columnNames[1]= getS("Cote aval (m)");
    } else if ("Geometrique".equals(type)) {
      lbType_.setText(getS("Profil cr�te"));
      tableModel_.columnNames= new String[2];
      tableModel_.columnNames[0]= getS("Distance (m)");
      tableModel_.columnNames[1]= getS("Cote (m)");
    } else if ("OuvertureVanne".equals(type)) {
      lbType_.setText(getS("Ouverture Vanne"));
      tableModel_.columnNames= new String[3];
      tableModel_.columnNames[0]= getS("Temps");
      tableModel_.columnNames[1]= getS("Cote inf�rieur (m)");
      tableModel_.columnNames[2]= getS("Cote sup�rieur (m)");
  } else if ("Tracer".equals(type)) {
    int nbTraceurs = modeleQE_.nbTraceur();
    lbType_.setText(getS("Loi tracer"));
    tableModel_.columnNames= new String[nbTraceurs+1];
    tableModel_.columnNames[0]= getS("Temps");
    for (int i = 0; i < nbTraceurs; i++) {
        tableModel_.columnNames[i+1]= "C� "+ modeleQE_.vvNomTracer()[i][0];
    }

  }

  }
  protected void setDescriptionGraphe() {
    String type= loi_.typeLoi();
    if ("Tarage".equals(type)) {
      graphe_.setLabels(tfNom_.getText(), "Q", "Z", "m3/s", "m");
    } else if ("Limnigramme".equals(type)) {
      graphe_.setLabels(tfNom_.getText(), "t", "Z", "", "m");
    } else if ("Hydrogramme".equals(type)) {
      graphe_.setLabels(tfNom_.getText(), "t", "Q", "", "m3/s");
    } else if ("LimniHydrogramme".equals(type)) {
      graphe_.setLabels(tfNom_.getText(), "t", "Z Q", "", "m m3/s");
      titreCourbes_= new String[2];
      titreCourbes_[0]= getS("Cote");
      titreCourbes_[1]= getS("D�bit");
    } else if ("Regulation".equals(type)) {
      graphe_.setLabels(tfNom_.getText(), "Q", "Z", "m3/s", "m");
    } else if ("Geometrique".equals(type)) {
      graphe_.setLabels(tfNom_.getText(), "D", "Z", "m", "m");
    } else if ("OuvertureVanne".equals(type)) {
      graphe_.setLabels(tfNom_.getText(), "t", "Z", "", "m");
      titreCourbes_= new String[2];
      titreCourbes_[0]= "Zinf";
      titreCourbes_[1]= "Zsup";
  }  else if ("Tracer".equals(type)) {
      graphe_.setLabels(tfNom_.getText(), "t", "C�", "", "");
      int nbTraceurs = modeleQE_.nbTraceur();
      titreCourbes_ = new String[nbTraceurs];
      for (int i = 0; i < nbTraceurs; i++) {
          titreCourbes_[i] = modeleQE_.vvNomTracer()[i][0];
      }
  }
    graphe_.setName(tfNom_.getText());
  }
  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   *
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe,
   *         sinon <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _page) {
    return graphe_.print(_g, _format, _page);
  }
  @Override
  public void cut() {
    table_.cut();
  }
  @Override
  public void copy() {
    table_.copy();
  }
  @Override
  public void paste() {
    table_.paste();
  }
  @Override
  public void duplicate() {
    table_.duplicate();
  }


}
class LoiHydrauliqueTableModel extends Hydraulique1dTableauReelModel implements ActionListener {
  String[] columnNames= { getS("Temps"), getS("Cote (m)"), getS("D�bit (m3/s)") };
  private String tempsPrecedent_="SECONDE";
  @Override
  public int getColumnCount() {
    return columnNames.length;
  }
  @Override
  public String getColumnName(int col) {
    return columnNames[col];
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd = _evt.getActionCommand();
    if ("SECONDE".equals(cmd)) {
      if ("SECONDE".equals(tempsPrecedent_))return;
      double coef = 1;
      if ("MINUTE".equals(tempsPrecedent_)) coef = 60.;
      if ("HEURE".equals(tempsPrecedent_)) coef = 60. * 60.;
      if ("JOUR".equals(tempsPrecedent_)) coef = 60. * 60. * 24.;
      multiplieColonneTemps(coef);
      tempsPrecedent_ = "SECONDE";
      return;
    }
    else if ("MINUTE".equals(cmd)) {
      if ("MINUTE".equals(tempsPrecedent_))return;
      double coef = 1;
      if ("SECONDE".equals(tempsPrecedent_)) coef = 1. / 60.D;
      if ("HEURE".equals(tempsPrecedent_)) coef = 60.;
      if ("JOUR".equals(tempsPrecedent_)) coef = 60. * 24.;
      multiplieColonneTemps(coef);
      tempsPrecedent_ = "MINUTE";
      return;
    }
    else if ("HEURE".equals(cmd)) {
      if ("HEURE".equals(tempsPrecedent_))return;
      double coef = 1;
      if ("SECONDE".equals(tempsPrecedent_)) coef = 1. / (60.D * 60.D);
      if ("MINUTE".equals(tempsPrecedent_)) coef = 1. / 60.D;
      if ("JOUR".equals(tempsPrecedent_)) coef = 24;
      multiplieColonneTemps(coef);
      tempsPrecedent_ = "HEURE";
      return;
    }
    else if ("JOUR".equals(cmd)) {
      if ("JOUR".equals(tempsPrecedent_))return;
      double coef = 1;
      if ("SECONDE".equals(tempsPrecedent_)) coef = 1. / (60D * 60D * 24D);
      if ("MINUTE".equals(tempsPrecedent_)) coef = 1. / (60D * 24D);
      if ("HEURE".equals(tempsPrecedent_)) coef = 1. / 24D;
      multiplieColonneTemps(coef);
      tempsPrecedent_ = "JOUR";
      return;
    }
  }
  public void multiplieColonneTemps(double coef) {
     Iterator ite = super.listePts_.iterator();
    while(ite.hasNext()) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
      if (lig.X() != null) {
        lig.x(lig.x()*coef);
      }
    }
    fireTableDataChanged();
  }
  /**
   * Retourne un tableau de r�els avec la 1ere colonne en seconde.
   * Si les colonnes suivants sont vides, la valeur sera Double.POSITIVE_INFINITY.
   * @return Le tableau de r�els [indice colonne][indice ligne].
   */
  public double[][] getTabDouble1ereColSec() {
    double[][] tab = getTabDouble();
    double coef = 1;
    if ("SECONDE".equals(tempsPrecedent_)) return tab;
    else if ("MINUTE".equals(tempsPrecedent_)) {
      coef = 60.;
    }
    else if ("HEURE".equals(tempsPrecedent_)) {
      coef = 60.*60.;
    }
    else if ("JOUR".equals(tempsPrecedent_)) {
      coef = 60.*60.*24.;
    }
    if (tab.length > 0) {
      int nbLigne = tab.length;
      for (int i = 0; i < nbLigne; i++) {
        tab[i][0] = tab[i][0] * coef;
      }
    }

    return tab;
  }
}
class LoiHydrauliqueTable extends Hydraulique1dTableauReel {
//  private Double[] longValues= { new Double(88888), new Double(88888), new Double(88888)};
  private static Double longValue =  new Double(88888);
  public LoiHydrauliqueTable(LoiHydrauliqueTableModel model) {
    super(model);
  }
  public void firePropertyChange(
    String property,
    double[][] vp,
    double[][] v) {
    super.firePropertyChange(property, vp, v);
  }
  @Override
  public Object getLongValues(int col) {
    return longValue;
  }
}
