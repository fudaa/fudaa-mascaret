/*
 * @file         Hydraulique1dLoiHydrauliqueLibraryEditor.java
 * @creation     1999-12-27
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BPanneauEditorAction;
import org.fudaa.ebli.dialog.BPanneauNavigation;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheTableau;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dTableauLoiSeuilGraphe;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_LoiHydraulique2;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Gestionnaires des lois hydrauliques dans les donn�es hydrauliques (MetierDonneesHydrauliques).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Lois hydrauliques".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer().<br>
 * @version      $Revision: 1.15 $ $Date: 2007-11-20 11:42:49 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class Hydraulique1dLoiHydrauliqueLibraryEditor
  extends Hydraulique1dCustomizerImprimable
  implements PropertyChangeListener {
  public final static int VALIDATION= 112334;
  private MetierDonneesHydrauliques donneesHydro_;
  private MetierEtude1d etude_;
  MyThumbnailPanel[] pnCL_;
  JScrollPane spCL_;
  BuPanel pnCLGen_;
  MyStatusPanel pnStatus_;
  Vector selectedLois_;
  boolean qualiteDEau_ = false;
  public Hydraulique1dLoiHydrauliqueLibraryEditor() {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Gestionnaire de lois"));
    init();
  }

  public Hydraulique1dLoiHydrauliqueLibraryEditor(BDialogContent _parent) {
    super(_parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Gestionnaire de lois"));
    init();
  }
  private void init() {

    if ((donneesHydro_ == null) || (donneesHydro_.lois() == null)) {
      setSize(new Dimension(300, 200));
      return;
    }
    selectedLois_= new Vector();
    Container content= getContentPane();
    pnStatus_= new MyStatusPanel();
    pnCLGen_= new BuPanel();
    BuGridLayout lo= new BuGridLayout(3, 0, 0, false, false);
    lo.setCfilled(false);
    pnCLGen_.setLayout(lo);
    MetierLoiHydraulique[] lois = null;
    if (qualiteDEau_) {
        lois = donneesHydro_.getLoisTracer();
    }
    else {
        lois = donneesHydro_.getToutesLoisSaufTracer();
    }
    pnCL_= new MyThumbnailPanel[lois.length];

    for (int i= 0; i < lois.length; i++) {
      pnCL_[i]= new MyThumbnailPanel(lois[i], pnStatus_);
      pnCL_[i].setActionCommand("THUMB" + i);
      pnCL_[i].addActionListener(this);
      pnCLGen_.add(pnCL_[i], i);
    }
    spCL_= new JScrollPane(pnCLGen_);
    spCL_.setBorder(
      new CompoundBorder(
        new EmptyBorder(
          new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)),
        spCL_.getBorder()));
    spCL_.setPreferredSize(
      new Dimension(
        pnCLGen_.getPreferredSize().width + 30,
        pnCLGen_.getPreferredSize().width + 30));
    BuPanel pnGen= new BuPanel();
    pnGen.setLayout(new BuVerticalLayout(5, true, false));
    pnGen.add(spCL_, 0);
    pnGen.add(pnStatus_, 1);
    content.add(BorderLayout.CENTER, pnGen);
    setNavPanel(BPanneauNavigation.FERMER);
    //-- aha -2.1.4 - change icon valider au lieu de fermer --//
    getNavPanel().getBtNavFermer().setIcon(BuResource.BU.getIcon("valider"));
    setActionPanel(
      BPanneauEditorAction.SUPPRIMER
        | BPanneauEditorAction.CREER
        | BPanneauEditorAction.EDITER);
    pack();
  }
  private void reinit() {
    pnCLGen_.removeAll();
    MetierLoiHydraulique[] lois = null;
    if (qualiteDEau_) {
        lois = donneesHydro_.getLoisTracer();
    }
    else {
        lois = donneesHydro_.getToutesLoisSaufTracer();
    }
    pnCL_= new MyThumbnailPanel[lois.length];
    for (int i= 0; i < lois.length; i++) {
      pnCL_[i]= new MyThumbnailPanel(lois[i], pnStatus_);
      pnCL_[i].setActionCommand("THUMB" + i);
      pnCL_[i].addActionListener(this);
      pnCLGen_.add(pnCL_[i], i);
    }
    spCL_.setPreferredSize(
      new Dimension(
        pnCLGen_.getPreferredSize().width + 30,
        Math.max(200, pnCLGen_.getPreferredSize().width + 30)));
    pack();
  }
  public boolean restore() {
    return false;
  }
  @Override
  public void setObject(MetierHydraulique1d o) {
    if (o instanceof MetierDonneesHydrauliques) {
      MetierDonneesHydrauliques vp= donneesHydro_;
      donneesHydro_= (MetierDonneesHydrauliques)o;
      init();
      firePropertyChange("donneesHydro", vp, donneesHydro_);
    } else if (o instanceof MetierEtude1d) {
      etude_= (MetierEtude1d)o;
      setObject(etude_.donneesHydro());
    }
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    int id= _evt.getID();
    if (_evt.getSource() instanceof MyThumbnailPanel) {
      MyThumbnailPanel src= (MyThumbnailPanel)_evt.getSource();
      if (id == MyThumbnailPanel.SELECTION) {
        if (src.isSelected()) {
          selectedLois_.add(src.getLoi());
        } else {
          selectedLois_.remove(src.getLoi());
        }
      } else if (id == MyThumbnailPanel.VALIDATION) {
        for (int i= 0; i < pnCL_.length; i++)
          pnCL_[i].setSelected(false);
        selectedLois_.clear();
        src.setSelected(true);
        selectedLois_.add(src.getLoi());
        reponse_= VALIDATION;
        if (etude_ != null) {
            Hydraulique1dIHM_LoiHydraulique2 ihmLoiHydraulique =
                    new Hydraulique1dIHM_LoiHydraulique2(etude_);
            ihmLoiHydraulique.setQualiteDEau(qualiteDEau_);
          ihmLoiHydraulique.addPropertyChangeListener(this);
            ihmLoiHydraulique.editer(src.getLoi());
        }
        for (int i= 0; i < pnCL_.length; i++)
          pnCL_[i].setSelected(false);
        selectedLois_.clear();
      }
    } else if ("SUPPRIMER".equals(cmd)) {
      MetierLoiHydraulique[] lois= getSelectedLois();
      for (int i= 0; i < lois.length; i++) {
        MetierLoiHydraulique[] tabLoi= new MetierLoiHydraulique[1];
        tabLoi[0]= lois[i];
        donneesHydro_.supprimeLois(tabLoi);
        selectedLois_.clear();
        reinit();
      }
      for (int i= 0; i < pnCL_.length; i++)
        pnCL_[i].setSelected(false);
    } else if ("CREER".equals(cmd)) {
      if (etude_ != null) {
          Hydraulique1dIHM_LoiHydraulique2 ihmLoiHydraulique =
                  new Hydraulique1dIHM_LoiHydraulique2(etude_);
          ihmLoiHydraulique.setQualiteDEau(qualiteDEau_);
          ihmLoiHydraulique.addPropertyChangeListener(this);
          ihmLoiHydraulique.editer();

      }
    } else if ("EDITER".equals(cmd)) {
      if (etude_ != null) {
          Hydraulique1dIHM_LoiHydraulique2 ihmLoiHydraulique =
                  new Hydraulique1dIHM_LoiHydraulique2(etude_);
          ihmLoiHydraulique.setQualiteDEau(qualiteDEau_);
          ihmLoiHydraulique.addPropertyChangeListener(this);
          MetierLoiHydraulique[] lois = getSelectedLois();
          for (int i = 0; i < pnCL_.length; i++)
              pnCL_[i].setSelected(false);
          for (int i = 0; i < lois.length; i++) {
              ihmLoiHydraulique.editer(lois[i]);
          }
          selectedLois_.clear();

      }
      }
    super.actionPerformed(_evt);
  }
  @Override
  public void propertyChange(PropertyChangeEvent e) {
    if (e.getPropertyName().indexOf("loi") != -1) {
      if (pnCLGen_ == null)
        return;
      reinit();
    }
  }
  public MetierLoiHydraulique[] getSelectedLois() {
    MetierLoiHydraulique[] res= new MetierLoiHydraulique[selectedLois_.size()];
    for (int i= 0; i < selectedLois_.size(); i++) {
      res[i]= (MetierLoiHydraulique)selectedLois_.get(i);
    }
    return res;
  }
  @Override
  protected void setValeurs() {
    reinit();
  }
  @Override
  protected boolean getValeurs() {
    return false;
  }
  public void setQualiteDEau(boolean b) {
      qualiteDEau_ = b;
  }
}

class MyStatusPanel extends BuPanel {
  BuLabel lbTypeCL_, lbNomCL_, lbNumeroCL_, lbNbPointsCL_;
  public MyStatusPanel() {
    setLayout(new BuGridLayout(2, 1, 1, true, false));
    setBorder(new TitledBorder(Hydraulique1dResource.HYDRAULIQUE1D.getString("Description")));
    int m= 0;
    add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Num�ro")+": "), m++);
    lbNumeroCL_= new BuLabel();
    add(lbNumeroCL_, m++);
    add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nom")+": "), m++);
    lbNomCL_= new BuLabel();
    add(lbNomCL_, m++);
    add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Type")+": "), m++);
    lbTypeCL_= new BuLabel();
    add(lbTypeCL_, m++);
    add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Points")+": "), m++);
    lbNbPointsCL_= new BuLabel();
    add(lbNbPointsCL_, m++);
    doLayout();
  }
  public void setLoi(MetierLoiHydraulique loi) {
    if (loi == null) {
      lbNumeroCL_.setText("");
      lbNomCL_.setText("");
      lbTypeCL_.setText("");
      lbNbPointsCL_.setText("");
    } else {
      lbNumeroCL_.setText("" + (loi.indice()+1));
      lbNomCL_.setText("" + loi.nom());
      lbTypeCL_.setText("" + loi.typeLoi());
      lbNbPointsCL_.setText("" + loi.nbPoints());
    }
  }
}
class MyThumbnailPanel extends BuPanel implements MouseListener {
  public final static int SELECTION= 1;
  public final static int VALIDATION= 2;
  MyStatusPanel status_;
  MetierLoiHydraulique loi_;
  Hydraulique1dGrapheTableau graphe_;
  boolean selected_;
  Vector listeners;
  String actionCmd_;
  public MyThumbnailPanel(MetierLoiHydraulique loi, MyStatusPanel s) {
    loi_= loi;
    double[][] valeurs;
    if (loi_ instanceof MetierLoiSeuil) {
      graphe_= new Hydraulique1dTableauLoiSeuilGraphe();
      MetierLoiSeuil lseuil= (MetierLoiSeuil)loi_;
      //      valeurs = Hydraulique1dLoiSeuilEditor2.trans(lseuil.arrayZaval());
      valeurs= lseuil.arrayZaval();
    } else {
      graphe_= new Hydraulique1dGrapheTableau();
      valeurs= CtuluLibArray.transpose(loi_.pointsToDoubleArray()) ;
    }
    graphe_.setLabels("", "", "", "", "");
    graphe_.setThumbnail(true, 64, 64);
    graphe_.affiche(valeurs, null);
    add(graphe_);
    status_= s;
    addMouseListener(this);
    listeners= new Vector();
    actionCmd_= "THUMB" + loi_.numero();
  }
  public MetierLoiHydraulique getLoi() {
    return loi_;
  }
  public void setSelected(boolean s) {
    if (selected_ == s)
      return;
    selected_= s;
    setBackground(
      selected_
        ? UIManager.getColor("MenuItem.selectionBackground")
        : UIManager.getColor("*.background"));
  }
  public boolean isSelected() {
    return selected_;
  }
  @Override
  public void mouseClicked(MouseEvent e) {
    switch (e.getClickCount()) {
      case 1 :
        {
          setSelected(!selected_);
          fireActionEvent(new ActionEvent(this, SELECTION, getActionCommand()));
          break;
        }
      case 2 :
        {
          fireActionEvent(
            new ActionEvent(this, VALIDATION, getActionCommand()));
          break;
        }
    }
  }
  @Override
  public void mouseEntered(MouseEvent e) {
    status_.setLoi(loi_);
  }
  @Override
  public void mouseExited(MouseEvent e) {
    status_.setLoi(null);
  }
  @Override
  public void mousePressed(MouseEvent e) {}
  @Override
  public void mouseReleased(MouseEvent e) {}
  public void addActionListener(ActionListener l) {
    if (!listeners.contains(l))
      listeners.add(l);
  }
  public void removeActionListener(ActionListener l) {
    if (listeners.contains(l))
      listeners.remove(l);
  }
  private void fireActionEvent(ActionEvent e) {
    for (int i= 0; i < listeners.size(); i++) {
      ((ActionListener)listeners.get(i)).actionPerformed(e);
    }
  }
  public void setActionCommand(String cmd) {
    actionCmd_= cmd;
  }
  public String getActionCommand() {
    return actionCmd_;
  }


}
