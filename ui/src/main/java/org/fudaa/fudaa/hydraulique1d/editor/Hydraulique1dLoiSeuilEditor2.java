/*
 * @file         Hydraulique1dLoiSeuilEditor2.java
 * @creation     2001-11-20
 * @modification $Date: 2007-11-20 11:42:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.print.PageFormat;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReelModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'une loi seuil (MetierSeuil).<br>
 * Appeler lors de la cr�ation d'une loi "Seuil Zam=f(Zav,Q)" � partir du "gestionnaire de lois hydrauliques".<br>
 * Lancer par l'ihm helper Hydraulique1dIHM_LoiHydraulique2 au niveau de la classe Hydraulique1dLoiHydrauliqueLibraryEditor.<br>
 * @version      $Revision: 1.21 $ $Date: 2007-11-20 11:42:47 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dLoiSeuilEditor2
  extends Hydraulique1dCustomizerImprimable
  implements BuCutCopyPasteInterface, TableModelListener, FocusListener
{
  private BuPanel pnGauche_, pnNom_;
  private JSplitPane sptLoi_;
  private BuBorderLayout loNom_;
  private BuVerticalLayout loGauche_;
  private BuTextField tfNom_;
  private BuLabel lbType_;
  private Hydraulique1dGrapheTableau graphe_;

  private Hydraulique1dTableauReel tableDebit_, tableZAval_, tableZAmont_;
  private TableauDebitModel tableDebitModel_;
  private TableauZavalModel tableZAvalModel_;
  private TableauZamontModel tableZAmontModel_;
  private MetierLoiSeuil loi_;
  private String typeGraphe= "ZAVAL";
  private MetierDonneesHydrauliques donneesHydrauliques_;
  private boolean estNouvelleLoi_;
  public Hydraulique1dLoiSeuilEditor2() {
    this(null);
  }
  public Hydraulique1dLoiSeuilEditor2(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi seuil"));
    donneesHydrauliques_= null;
    estNouvelleLoi_=false;
    loi_= null;
    loGauche_= new BuVerticalLayout(5, true, true);
    loNom_= new BuBorderLayout(5, 5);
    Container pnMain_= getContentPane();
    pnGauche_= new BuPanel();
    pnGauche_.setLayout(loGauche_);
    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    graphe_= new Hydraulique1dGrapheTableau();

    tableDebitModel_ = new TableauDebitModel();
    tableDebit_ = new Hydraulique1dTableauReel(tableDebitModel_);
    tableDebit_.addFocusListener(this);
    tableDebit_.setPreferredScrollableViewportSize(new Dimension(20, 120));
    JScrollPane spDebit= new JScrollPane(tableDebit_);
    spDebit.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    spDebit.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    tableZAvalModel_ = new TableauZavalModel();
    tableZAval_ = new Hydraulique1dTableauReel(tableZAvalModel_);
    tableZAval_.addFocusListener(this);
    tableZAval_.setPreferredScrollableViewportSize(new Dimension(140, 20));
    JScrollPane spZAval= new JScrollPane(tableZAval_);
    spZAval.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    spZAval.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

    tableZAmontModel_ = new TableauZamontModel();
    tableZAmont_ = new Hydraulique1dTableauReel(tableZAmontModel_);
    tableZAmont_.addFocusListener(this);
    tableZAmont_.setPreferredScrollableViewportSize(new Dimension(140, 140));
    JScrollPane spZAmont= new JScrollPane(tableZAmont_);
    spZAmont.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    spZAmont.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

    tableZAmontModel_.addTableModelListener(this);
    tableZAvalModel_.addTableModelListener(this);
    tableDebitModel_.addTableModelListener(this);

    BuPanel panelTableaux = new BuPanel();

    panelTableaux.setLayout(new GridBagLayout());
    Insets insets = new Insets(0, 0, 0, 0);
    panelTableaux.add(spZAval, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 140, 20));
    panelTableaux.add(spDebit, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 40, 120));
    panelTableaux.add(spZAmont,   new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0
            ,GridBagConstraints.SOUTHEAST, GridBagConstraints.BOTH, insets, 140, 120));


    tfNom_= new BuTextField();
    tfNom_.setEditable(true);
    tfNom_.addActionListener(this);
    tfNom_.setActionCommand("NOM");
    pnNom_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nom de loi")+" : "), BorderLayout.WEST);
    pnNom_.add(tfNom_, BorderLayout.CENTER);
    lbType_= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi seuil"));
    int n= 0;
    pnGauche_.add(lbType_, n++);
    pnGauche_.add(pnNom_, n++);
    pnGauche_.add(panelTableaux, n++);
    Dimension minimumSize= new Dimension(200, 100);
    pnGauche_.setMinimumSize(minimumSize);
    graphe_.setMinimumSize(minimumSize);
    sptLoi_= new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnGauche_, graphe_);
    sptLoi_.setOneTouchExpandable(true);
    sptLoi_.setDividerLocation(300);
    sptLoi_.setPreferredSize(new Dimension(600, 300));
    sptLoi_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    pnMain_.add(sptLoi_);
    addAction(
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Creer Colonne"),
      BuResource.BU.getIcon("ajouter"),
      "CREERCOLONNE");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Creer Ligne"), BuResource.BU.getIcon("creer"), "CREERLIGNE");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Supprimer"), BuResource.BU.getIcon("detruire"), "SUPPRIMER");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Importer"), BuResource.BU.getIcon("importer"), "IMPORTER");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Transposer Graphe"), "TRANSPOSER_AXES");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Editer Axes"), "EDITER_AXES");
    addAction(Hydraulique1dResource.HYDRAULIQUE1D.getString("Personnaliser Graphe"), "PERSONNALISER_GRAPHE");
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void tableChanged(TableModelEvent evt) {
    setValeursGraphe();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("loi", null, loi_);
      }
      fermer();
    } else if ("ANNULER".equals(cmd)) {
    	  if (estNouvelleLoi_) {
              MetierLoiHydraulique[] tabLoi= new MetierLoiHydraulique[1];
              tabLoi[0]= loi_;
        	  donneesHydrauliques_.supprimeLois(tabLoi);
        	  firePropertyChange("donneesHydrauliques", null, donneesHydrauliques_);
          }
      fermer();
    } else if ("CREERLIGNE".equals(cmd)) {
      creerLigne();
    } else if ("CREERCOLONNE".equals(cmd)) {
      creerColonne();
    } else if ("SUPPRIMER".equals(cmd)) {
      supprimer();
    } else if ("IMPORTER".equals(cmd)) {
      importer();
    } else if ("NOM".equals(cmd)) {
      graphe_.setLabels(tfNom_.getText(), null, null, null, null);
    } else if ("EDITER_AXES".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          Hydraulique1dResource.HYDRAULIQUE1D.getString("EDITER LES AXES"),
          graphe_.getEditeurAxes());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else if ("PERSONNALISER_GRAPHE".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          Hydraulique1dResource.HYDRAULIQUE1D.getString("PERSONNALISER LE GRAPHE"),
          graphe_.getPersonnaliseurGraphe());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else if ("TRANSPOSER_AXES".equals(cmd)) {
      if (typeGraphe.equals("ZAVAL"))
        typeGraphe= "DEBIT";
      else
        typeGraphe= "ZAVAL";
      setValeursGraphe();
    }
  }
  @Override
  public void cut() {
    if (tableDebit_.hasFocus()) tableDebit_.cut();
    if (tableZAval_.hasFocus()) tableZAval_.cut();
    if (tableZAmont_.hasFocus()) tableZAmont_.cut();
  }
  @Override
  public void copy() {
    if (tableDebit_.hasFocus()) tableDebit_.copy();
    if (tableZAval_.hasFocus()) tableZAval_.copy();
    if (tableZAmont_.hasFocus()) tableZAmont_.copy();
  }
  @Override
  public void paste() {
    if (tableDebit_.hasFocus()) tableDebit_.paste();
    if (tableZAval_.hasFocus()) tableZAval_.paste();
    if (tableZAmont_.hasFocus()) tableZAmont_.paste();
  }
  @Override
  public void duplicate() {
    if (tableDebit_.hasFocus()) tableDebit_.duplicate();
    if (tableZAval_.hasFocus()) tableZAval_.duplicate();
    if (tableZAmont_.hasFocus()) tableZAmont_.duplicate();
  }
  /**
   * Impl�mentation de FocusListener.
   * Invoked when a component gains the keyboard focus.
   * @param e L'�v�nement.
   */
  @Override
  public void focusGained(FocusEvent e) {
    if (!tableZAmont_.hasFocus()) {
      tableZAmont_.getSelectionModel().clearSelection();
    }
    if (!tableDebit_.hasFocus()) {
      tableDebit_.getSelectionModel().clearSelection();
    }
    if (!tableZAval_.hasFocus()) {
      tableZAval_.getSelectionModel().clearSelection();
    }
  }

  /**
   * Impl�mentation de FocusListener.
   * Invoked when a component loses the keyboard focus.
   * @param e L'�v�nement.
   */
  @Override
  public void focusLost(FocusEvent e) {

  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    String nom= tfNom_.getText();
    if (!nom.equals(loi_.nom())) {
      loi_.nom(nom);
      changed= true;
    }
    double[] q = tableDebitModel_.getTab1DDoubleComplet();
    boolean debitChanged  = !Arrays.equals(q,loi_.q());
    double[] zAval = tableZAvalModel_.getTab1DDoubleComplet();
    boolean zAvalChanged  = !Arrays.equals(zAval,loi_.zAval());
    double[][] zAmont = tableZAmontModel_.getTabDoubleComplet();
    boolean zAmontChanged = !CtuluLibArray.equals(zAmont,loi_.zAmont());

    if (debitChanged || zAvalChanged || zAmontChanged) {
      double[][] pts= new double[q.length+1][zAval.length+1];
      for (int i = 0; i < q.length; i++) {
        pts[i+1][0] = q[i];
      }
      System.arraycopy(zAval, 0, pts[0], 1, zAval.length);
      for (int i = 0; i < zAmont.length; i++) {
        System.arraycopy(zAmont[i], 0, pts[i+1], 1, zAmont[i].length);
      }
      loi_.setPoints(pts);
      changed = true;
    }
    if (estNouvelleLoi_) changed=true;
    return changed;
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
	  if (_n instanceof MetierDonneesHydrauliques) {
    	  MetierDonneesHydrauliques donneesHydrauliques = (MetierDonneesHydrauliques) _n;
          if (donneesHydrauliques == donneesHydrauliques_)
              return;
          donneesHydrauliques_ = donneesHydrauliques;
      }
	  else if (_n instanceof MetierLoiSeuil) {
	    MetierLoiSeuil loi= (MetierLoiSeuil)_n;
	    if (loi == loi_)
	      return;
	    loi_= loi;
	    setDescriptionTableau();
	    setValeurs();
	  } else{
    	return;
    }
  }

  public void setEstNouvelleLoi(boolean b) {
	  estNouvelleLoi_ = b;
  }

  final static double[][] arrayZaval(double[] q, double[] zAval, double[][] zAmont) {
    double[][] tab = new double[zAval.length+1][q.length];

    for (int i = 0; i < tab.length; i++) {
      for (int j = 0; j < tab[i].length; j++) {
        if (i==0) {
          tab[0][j] =q[j]; // l'abscisse
        }
        else {
          tab[i][j] = zAmont[j][i-1];
        }
      }
    }
    return tab;
  }
  final static double[][] arrayDebit(double[] q, double[] zAval, double[][] zAmont) {
    double[][] tab = new double[q.length+1][zAval.length];

    for (int i = 0; i < tab.length; i++) {
      for (int j = 0; j < tab[i].length; j++) {
        if (i==0) {
          tab[0][j] =zAval[j]; // l'abscisse
        }
        else {
          tab[i][j] = zAmont[i-1][j];
        }
      }
    }
    return tab;

  }
  @Override
  protected void setValeurs() {
    // D�bit
    double[][] tQ = new double[loi_.q().length][1];
    String[] columnNamesQ ={Hydraulique1dResource.HYDRAULIQUE1D.getString("D�bit")};
    for (int i = 0; i < tQ.length; i++) {
      tQ[i][0] = loi_.q()[i];
    }
    tableDebitModel_.setColumnNames(columnNamesQ);
    tableDebitModel_.setTabDouble(tQ);

    // Cote aval
    if (loi_.zAval().length == 0) {
      double[][] tZAval = new double[1][1];
      tZAval[0][0] = 0;
      String[] columnNamesZAval ={Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+" 1"};
      tableZAvalModel_.setColumnNames(columnNamesZAval);
      tableZAvalModel_.setTabDouble(tZAval);
    } else {
      double[][] tZAval = new double[1][];
      String[] columnNamesZAval = new String[loi_.zAval().length];
      for (int i = 0; i < loi_.zAval().length; i++) {
        columnNamesZAval[i]=Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+" "+(i+1);
      }
      tZAval[0] = loi_.zAval();
      tableZAvalModel_.setColumnNames(columnNamesZAval);
      tableZAvalModel_.setTabDouble(tZAval);
    }

    // Cote amont
    if (loi_.zAmont().length == 0) {
      double[][] tZAmont = new double[1][1];
      tZAmont[0][0] = 0;
      String[] columnNamesZAmont = {Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" 1"};
      tableZAmontModel_.setColumnNames(columnNamesZAmont);
      tableZAmontModel_.setTabDouble(tZAmont);
    }
    else if (loi_.zAmont()[0].length == 0) {
      double[][] tZAmont = new double[1][1];
      tZAmont[0][0] = 0;
      String[] columnNamesZAmont = {Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" 1"};
      tableZAmontModel_.setColumnNames(columnNamesZAmont);
      tableZAmontModel_.setTabDouble(tZAmont);
    }
    else {
      double[][] tZAmont = loi_.zAmont();
      String[] columnNamesZAmont;
      columnNamesZAmont = new String[loi_.zAmont()[0].length];
      for (int i = 0; i < columnNamesZAmont.length; i++) {
        columnNamesZAmont[i]=Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" "+(i+1);
      }
      tableZAmontModel_.setColumnNames(columnNamesZAmont);
      tableZAmontModel_.setTabDouble(tZAmont);
    }
    setValeursGraphe();
  }
  protected void setValeursGraphe() {
    tfNom_.setText(loi_.nom());
    setDescriptionGraphe();
    java.util.List listeValeurs = new ArrayList();
    java.util.List listeNoms = new ArrayList();
    int[] indicesZaval= tableZAvalModel_.getIndiceNonNull();
    int[] indicesDebit= tableDebitModel_.getIndiceNonNull();
    if (typeGraphe.equals("ZAVAL")) {
      listeValeurs.add(tableDebitModel_.getDebits()); // abcisses
      for (int i = 0; i < indicesZaval.length; i++) {
        double[] zAmont = tableZAmontModel_.getTabParZaval(indicesZaval[i],indicesDebit);
        if (zAmont != null) {
          listeValeurs.add(zAmont);
          listeNoms.add(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+(indicesZaval[i]+1));
        }

      }
    } else { // typeGraphe.equals("DEBIT")
      listeValeurs.add(tableZAvalModel_.getZavals()); // abcisses
      for (int i = 0; i < indicesDebit.length; i++) {
        double[] zAmont = tableZAmontModel_.getTabParQ(indicesDebit[i],
            indicesZaval);
        if (zAmont != null) {
          listeValeurs.add(zAmont);
          listeNoms.add("Q" + (indicesDebit[i]+1));
        }
      }
    }
    double[][] valeurs = (double[][])listeValeurs.toArray(new double[listeValeurs.size()][]);
    String[] nomCourbes = (String[])listeNoms.toArray(new String[listeNoms.size()]);
//    graphe_.affiche(CtuluLibArray.transpose(valeurs),nomCourbes);
    graphe_.affiche(valeurs,nomCourbes);

  }
  protected void setDescriptionGraphe() {
    if (typeGraphe.equals("ZAVAL"))
      graphe_.setLabels(tfNom_.getText(), "Q", Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont"), "m3/s", "m");
    else
      graphe_.setLabels(tfNom_.getText(), Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval"),
                        Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont"), "m", "m");
    graphe_.setName(tfNom_.getText());
  }

  public String[] getEnabledActions() {
    String[] r= new String[] { "IMPRIMER", "PREVISUALISER", "MISEENPAGE" };
    return r;
  }
  protected void creerLigne() {
    ListSelectionModel rowSM= tableDebit_.getSelectionModel();
    if (!rowSM.isSelectionEmpty()) {
      int indiceSelect = rowSM.getMinSelectionIndex();
      tableDebitModel_.ajouterLigne(indiceSelect);
      tableZAmontModel_.ajouterLigne(indiceSelect);
    }
    else {
      tableDebitModel_.ajouterLigne();
      tableZAmontModel_.ajouterLigne();
    }
  }
  protected void creerColonne() {
    int[] index= tableZAval_.getSelectedColumns();
    if (index.length != 0) {
      for (int i = 0; i < index.length; i++) {
        tableZAvalModel_.ajouterColonne(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+" "+(index[i]+1),index[i]);
        tableZAmontModel_.ajouterColonne(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" "+(index[i]+1),index[i]);
      }
      for (int i = index[index.length-1]; i < tableZAvalModel_.getColumnCount(); i++) {
        tableZAvalModel_.setColumnName(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+" "+(i+1),i);
        tableZAmontModel_.setColumnName(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" "+(i+1),i);
      }
    }
    else {
      tableZAvalModel_.ajouterColonne(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+" "+(tableZAvalModel_.getColumnCount()+1));
      tableZAmontModel_.ajouterColonne(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" "+(tableZAmontModel_.getColumnCount()+1));
    }
  }
  protected void supprimer() {
    int[] index= tableZAval_.getSelectedColumns();
    if (index.length != 0) {
      for (int i = (index.length-1); i >=0 ; i--) {
        // suppression d'une colonne
        tableZAvalModel_.supprimerColonne(index[i]);
        tableZAmontModel_.supprimerColonne(index[i]);
      }
      for (int i = 0; i < tableZAvalModel_.getColumnCount(); i++) {
        tableZAvalModel_.setColumnName(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+" "+(i+1),i);
        tableZAmontModel_.setColumnName(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" "+(i+1),i);
      }
    }
    else {
      index= tableDebit_.getSelectedRows();
      if (index.length != 0) {
        for (int i = (index.length-1); i >=0 ; i--) {
          tableDebitModel_.supprimerLigne(index[i]);
          tableZAmontModel_.supprimerLigne(index[i]);
        }
      }
      else {
        tableDebitModel_.supprimerLigne(tableDebitModel_.getRowCount()-1);
        tableZAmontModel_.supprimerLigne(tableZAmontModel_.getRowCount()-1);
      }
    }
  }


  protected void importer() {
    File file= Hydraulique1dImport.chooseFile("loi");
    if (file == null)
      return;
    double[][] tableauLoi= Hydraulique1dImport.importLoiSeuil(file);
    if ((tableauLoi == null)) {
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR")+": "+getS("l'importation de la loi")+"\n" + Hydraulique1dResource.HYDRAULIQUE1D.getString("a �chou�."))
        .activate();
      return;
    }
    if ((tableauLoi.length == 0)) {
      new BuDialogError(
          (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
          ( (BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
          Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR")+": "+getS("ucun point n'est")+"\n" +
          Hydraulique1dResource.HYDRAULIQUE1D.getString("disponible dans cette import!"))
          .activate();
      return;

    }

    // extraction du d�bit
    double[][] tabDebit = new double[tableauLoi.length -1][1];
    for (int i = 0; i < tabDebit.length; i++) {
      tabDebit[i][0]=tableauLoi[i+1][0];
    }
    tableDebitModel_.setTabDouble(tabDebit);

    // extraction de la cote aval
    double[][] tabZAval = new double[1][tableauLoi[0].length -1];
    String[] namesZAval = new String[tabZAval[0].length];
    String[] namesZAmont = new String[tabZAval[0].length];
    for (int i = 0; i < tabZAval[0].length; i++) {
      tabZAval[0][i]=tableauLoi[0][i+1];
      namesZAval[i] = Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+" "+(i+1);
      namesZAmont[i] = Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" "+(i+1);
    }
    tableZAvalModel_.setColumnNames(namesZAval);
    tableZAvalModel_.setTabDouble(tabZAval);

    // extraction de la cote amont
    double[][] tabZAmont = new double[tableauLoi.length-1][tableauLoi[0].length -1];
    for (int i = 0; i < tabZAmont.length; i++) {
      for (int j = 0; j < tabZAmont[i].length; j++) {
        tabZAmont[i][j]=tableauLoi[i+1][j+1];
      }
    }
    tableZAmontModel_.setColumnNames(namesZAmont);
    tableZAmontModel_.setTabDouble(tabZAmont);

    setValeursGraphe();
  }
  protected void setDescriptionTableau() {
    lbType_.setText(getS("Courbe Seuil Zamont")+" = "+getS("f(Q,Zaval)"));
  }
  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   *
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe,
   *         sinon <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _page) {
    return graphe_.print(_g, _format, _page);
  }
}
class TableauDebitModel extends Hydraulique1dTableauReelModel {
  final static String[] colNamesQ={Hydraulique1dResource.HYDRAULIQUE1D.getString("D�bit")};
  TableauDebitModel() {
    super(colNamesQ,0);
  }
  int[] getIndiceNonNull() {
    try {
      ArrayList listeTmp = new ArrayList(getRowCount());
      for (int i = 0; i < getRowCount(); i++) {
        if (getValueAt(i, 0) != null) {
          listeTmp.add(new Integer(i));
        }
      }
      Integer[] tab = (Integer[]) listeTmp.toArray(new Integer[listeTmp.size()]);
      int[] res = new int[tab.length];
      for (int i = 0; i < res.length; i++) {
        res[i] = tab[i].intValue();
      }
      return res;
    }
    catch (IndexOutOfBoundsException ex) {
      return new int[0];
    }
  }

  double[] getDebits() {
    int[] indices = getIndiceNonNull();
    double[] res = new double[indices.length];
    for (int i = 0; i < res.length; i++) {
      res[i] = super.valueAt(indices[i],0);
    }
    return res;
  }
}
class TableauZavalModel extends Hydraulique1dTableauReelModel {
  final static String[] colNamesZAval={Hydraulique1dResource.HYDRAULIQUE1D.getString("Zaval")+" 1"};
  TableauZavalModel() {
    super(colNamesZAval,0);
  }
  int[] getIndiceNonNull() {
    try {
      ArrayList listeTmp = new ArrayList(getColumnCount());
      for (int i = 0; i < getColumnCount(); i++) {
        if (getValueAt(0, i) != null) {
          listeTmp.add(new Integer(i));
        }
      }
      Integer[] tab = (Integer[]) listeTmp.toArray(new Integer[listeTmp.size()]);
      int[] res = new int[tab.length];
      for (int i = 0; i < res.length; i++) {
        res[i] = tab[i].intValue();
      }
      return res;
    }
    catch (IndexOutOfBoundsException ex) {
      return new int[0];
    }
  }
  double[] getZavals() {
    int[] indices = getIndiceNonNull();
    double[] res = new double[indices.length];
    for (int i = 0; i < res.length; i++) {
      res[i] = super.valueAt(0, indices[i]);
    }
    return res;
  }
}
class TableauZamontModel extends Hydraulique1dTableauReelModel {
  final static String[] colNamesZAmont={Hydraulique1dResource.HYDRAULIQUE1D.getString("Zamont")+" 1"};
  TableauZamontModel() {
    super(colNamesZAmont,0);
  }
  double[] getTabParZaval(int iZaval, int[] iQ) {
    try {
      double[] res = new double[iQ.length];
      for (int i = 0; i < res.length; i++) {
        Double val = (Double) getValueAt(iQ[i], iZaval);
        if (val == null) {
          return null;
        }
        res[i] = val.doubleValue();
      }
      return res;
    }
    catch (IndexOutOfBoundsException ex) {
      return null;
    }

  }
  double[] getTabParQ(int iQ, int[] iZaval) {
    try {
      double[] res = new double[iZaval.length];
      for (int i = 0; i < res.length; i++) {
        Double val = (Double) getValueAt(iQ, iZaval[i]);
        if (val == null) {
          return null;
        }
        res[i] = val.doubleValue();
      }
      return res;
    }
    catch (IndexOutOfBoundsException ex) {
      return null;
    }
  }
}
