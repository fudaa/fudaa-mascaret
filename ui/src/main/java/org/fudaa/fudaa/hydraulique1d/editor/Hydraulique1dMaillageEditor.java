/*
 * @file         Hydraulique1dMaillageEditor.java
 * @creation     2000-12-19
 * @modification $Date: 2007-11-20 11:42:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JComboBox;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierMethodeMaillage;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSections;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeries;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierMaillage;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_SectionsParSections;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_SectionsParSerie2;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur du maillage (DMaillage).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Maillage".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().MAILLAGE2().editer().<br>
 * @version      $Revision: 1.10 $ $Date: 2007-11-20 11:42:43 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dMaillageEditor extends Hydraulique1dCustomizerImprimable {
  MetierMaillage maillage_;
  MetierEtude1d etude_;
  BuPanel pnMethode_, pnMaillage_, pnEditer_;
  BuHorizontalLayout loMethode_;
  FlowLayout loEditer_;
  BuVerticalLayout loMaillage_;
  JComboBox cmbMethode_;
  BuButton btEditer_;
  final static String SECTIONS_SUR_PROFILS=
    Hydraulique1dResource.HYDRAULIQUE1D.getString("Sections uniquement sur les profils");
  final static String SECTIONS_UTILISATEUR=
    Hydraulique1dResource.HYDRAULIQUE1D.getString("Sections d�finies, une � une, par l'utilisateur");
  final static String SECTIONS_PAR_SERIES=
    Hydraulique1dResource.HYDRAULIQUE1D.getString("Sections positionn�es � partir d'une carte des tailles de mailles");
  final static String SECTIONS_LIGNE_EAU_INITIALE=
    Hydraulique1dResource.HYDRAULIQUE1D.getString("Sections reprises de la ligne d'eau initiale");
  public Hydraulique1dMaillageEditor() {
    this(null);
  }
  public Hydraulique1dMaillageEditor(BDialogContent parent) {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Maillage"));
    maillage_= null;
    loMaillage_= new BuVerticalLayout(5, true, true);
    loMethode_= new BuHorizontalLayout(5, false, false);
    loEditer_= new FlowLayout();
    pnMaillage_= new BuPanel();
    pnMaillage_.setLayout(loMaillage_);
    pnMaillage_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    pnMethode_= new BuPanel();
    pnMethode_.setLayout(loMethode_);
    pnEditer_= new BuPanel();
    pnEditer_.setLayout(loEditer_);
    cmbMethode_=
      new JComboBox(
        new String[] {
          SECTIONS_SUR_PROFILS,
          SECTIONS_UTILISATEUR,
          SECTIONS_PAR_SERIES,
          SECTIONS_LIGNE_EAU_INITIALE });
    cmbMethode_.addActionListener(this);
    int n= 0;
    pnMethode_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("M�thode de calcul")), n++);
    pnMethode_.add(cmbMethode_, n++);
    btEditer_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("EDITER SECTIONS"));
    btEditer_.addActionListener(this);
    btEditer_.setActionCommand("EDITER_SECTION");
    pnEditer_.add(btEditer_, 0);
    n= 0;
    pnMaillage_.add(pnMethode_, n++);
    pnMaillage_.add(pnEditer_, n++);
    getContentPane().add(BorderLayout.CENTER, pnMaillage_);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    setActionPanel(EbliPreferences.DIALOG.IMPORTER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    Object src= _evt.getSource();
    if ("IMPORTER".equals(cmd)) {
      importer();
      setValeurs();
    } else if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("maill", null, maillage_);
      }
      fermer();
    } else if ("EDITER_SECTION".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("maill", null, maillage_);
      }
      if (maillage_.sections() instanceof MetierDefinitionSectionsParSections) {
        Hydraulique1dIHM_SectionsParSections ihmSectionsParSections=
          new Hydraulique1dIHM_SectionsParSections(etude_);
        ihmSectionsParSections.editer();
      } else if (
        maillage_.sections() instanceof MetierDefinitionSectionsParSeries) {
        Hydraulique1dIHM_SectionsParSerie2 ihmSectionsParSerie=
          new Hydraulique1dIHM_SectionsParSerie2(etude_);
        ihmSectionsParSerie.editer();
      }
    } else if (src == cmbMethode_) {
      String itemSectionne= (String)cmbMethode_.getSelectedItem();
      if (itemSectionne.equals(SECTIONS_SUR_PROFILS)) {
        btEditer_.setEnabled(false);
      } else if (itemSectionne.equals(SECTIONS_UTILISATEUR)) {
        btEditer_.setEnabled(true);
      } else if (itemSectionne.equals(SECTIONS_PAR_SERIES)) {
        btEditer_.setEnabled(true);
      } else if (itemSectionne.equals(SECTIONS_LIGNE_EAU_INITIALE)) {
        btEditer_.setEnabled(false);
      }
    } else  {
      super.actionPerformed(_evt);
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierMaillage) {
      maillage_= (MetierMaillage)_n;
      setValeurs();
    } else if (_n instanceof MetierEtude1d) {
      etude_= (MetierEtude1d)_n;
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (maillage_.methode() == null)
      return changed;
    String itemSectionne= (String)cmbMethode_.getSelectedItem();
    if (itemSectionne.equals(SECTIONS_SUR_PROFILS)) {
      if (maillage_.methode().value()
        != EnumMetierMethodeMaillage._SECTIONS_SUR_PROFILS) {
        maillage_.methode(EnumMetierMethodeMaillage.SECTIONS_SUR_PROFILS);
        changed= true;
      }
    } else if (itemSectionne.equals(SECTIONS_UTILISATEUR)) {
      if (maillage_.methode().value()
        != EnumMetierMethodeMaillage._SECTIONS_UTILISATEUR) {
        maillage_.methode(EnumMetierMethodeMaillage.SECTIONS_UTILISATEUR);
        maillage_.creeSectionsParSections();
        changed= true;
      }
    } else if (itemSectionne.equals(SECTIONS_PAR_SERIES)) {
      if (maillage_.methode().value()
        != EnumMetierMethodeMaillage._SECTIONS_PAR_SERIES) {
        maillage_.methode(EnumMetierMethodeMaillage.SECTIONS_PAR_SERIES);
        maillage_.creeSectionsParSeries();
        changed= true;
      }
    } else if (itemSectionne.equals(SECTIONS_LIGNE_EAU_INITIALE)) {
      if (maillage_.methode().value()
        != EnumMetierMethodeMaillage._SECTIONS_LIGNE_EAU_INITIALE) {
        maillage_.methode(EnumMetierMethodeMaillage.SECTIONS_LIGNE_EAU_INITIALE);
        changed= true;
      }
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    switch (maillage_.methode().value()) {
      case EnumMetierMethodeMaillage._SECTIONS_SUR_PROFILS :
        cmbMethode_.setSelectedIndex(0);
        btEditer_.setEnabled(false);
        break;
      case EnumMetierMethodeMaillage._SECTIONS_UTILISATEUR :
        cmbMethode_.setSelectedIndex(1);
        btEditer_.setEnabled(true);
        break;
      case EnumMetierMethodeMaillage._SECTIONS_PAR_SERIES :
        cmbMethode_.setSelectedIndex(2);
        btEditer_.setEnabled(true);
        break;
      case EnumMetierMethodeMaillage._SECTIONS_LIGNE_EAU_INITIALE :
        cmbMethode_.setSelectedIndex(3);
        btEditer_.setEnabled(false);
        break;
    }
  }

  protected void importer() {
    System.out.println("DEBUT importer()");
    File file= Hydraulique1dImport.chooseFile("mail");
    System.out.println("file=" + file);
    if (file == null)
      return;
    MetierMaillage maillageImporter=
      Hydraulique1dImport.importMaillage(
        file,
        etude_.paramGeneraux(),
        etude_.reseau());
    if ((maillageImporter == null)) {
      System.out.println("maillageImporter==null");
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR")+": "+getS("l'importation du maillage")+"\n" +
        Hydraulique1dResource.HYDRAULIQUE1D.getString("a �chou�."))
        .activate();
    } else {
      System.out.println("maillageImporter!=null");
      etude_.paramGeneraux().maillage(maillageImporter);
    }
    System.out.println("FIN importer()");
  }
}
