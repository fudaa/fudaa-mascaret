/*
 * @file         Hydraulique1dMeteoEditor.java
 * @creation     2006-07-05
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresGenerauxQualiteDEau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauChainesModel;

import com.memoire.bu.BuScrollPane;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur du fichier meteo (Qualit� d'eau).<br>
 * Appel� si l'utilisateur clic sur le bouton "Meteo".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().METEO().editer().<br>
 * @version      1.0
 * @author       Olivier Pasteur
 */

public class Hydraulique1dMeteoEditor extends Hydraulique1dCustomizerImprimable
  implements ActionListener {

  private BuScrollPane spMeteo_;
  private Hydraulique1dTableau tabMeteo_;
  private Hydraulique1dTableauChainesModel tabModel_;
  private byte[] paramMeteo_;
  MetierParametresGenerauxQualiteDEau paramsGeneraux_;

  public Hydraulique1dMeteoEditor() {
    this(null);
  }
  public Hydraulique1dMeteoEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Param�tres M�t�o"));
    paramsGeneraux_= null;

    String[] colonneNom= {Hydraulique1dResource.HYDRAULIQUE1D.getString("Param�tres m�t�o")};
    tabModel_ = new Hydraulique1dTableauChainesModel(colonneNom,1);
    tabMeteo_= new Hydraulique1dTableau(tabModel_);
    spMeteo_= new BuScrollPane(tabMeteo_);

    getContentPane().add(spMeteo_, BorderLayout.CENTER);
    setActionPanel(
            EbliPreferences.DIALOG.IMPORTER
             | EbliPreferences.DIALOG.EXPORTER | EbliPreferences.DIALOG.SUPPRIMER);

    setNavPanel(EbliPreferences.DIALOG.VALIDER |
                 EbliPreferences.DIALOG.ANNULER);
    pack();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
      String cmd = _evt.getActionCommand();
      if ("VALIDER".equals(cmd)) {
          if (getValeurs()) {
              firePropertyChange("Param�tres M�t�o", null, paramMeteo_);
          }
          fermer();
      } else if ("SUPPRIMER".equals(cmd)) {
    	  tabMeteo_.supprimeLignesSelectionnees();

      } else if ("IMPORTER".equals(cmd)) {
          importer();
      } else if ("EXPORTER".equals(cmd)) {
          exporter();
      } else {
          super.actionPerformed(_evt);
        }
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierParametresGenerauxQualiteDEau))  return;
    paramsGeneraux_= (MetierParametresGenerauxQualiteDEau)_n;
    setValeurs();
  }



  @Override
  protected boolean getValeurs() {
    paramsGeneraux_.paramMeteoTracer(tabModel_.getByte());
    return true;
  }

  @Override
  protected void setValeurs() {

  if (paramsGeneraux_.paramMeteoTracer()==null || paramsGeneraux_.paramMeteoTracer().length==0){
      String[] colonneNom= {Hydraulique1dResource.HYDRAULIQUE1D.getString("Veuillez importer le fichier m�t�o !")};
      tabModel_.setColumnNames(colonneNom);
  }
        tabModel_.setLargeTabByte(paramsGeneraux_.paramMeteoTracer());

  }

  private void importer() {
        File fichier = Hydraulique1dImport.chooseFile("met");
        if (fichier == null) {
            return;
        }
        try{
            byte[] donneesMeteo = CtuluLibFile.litFichier(fichier);
            tabModel_.setLargeTabByte(donneesMeteo);
            String[] colonneNom= {Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier m�t�o")};
            tabModel_.setColumnNames(colonneNom);
        } catch (IOException ex){
            System.out.println(Hydraulique1dResource.HYDRAULIQUE1D.getString("Erreur lors de la lecture du fichier M�t�o")+" : "+ex.getMessage());
            FudaaCommonImplementation fci = (FudaaCommonImplementation)
      ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation();
            fci.error(Hydraulique1dResource.HYDRAULIQUE1D.getString("Erreur lors de la lecture du fichier M�t�o")+" : "+ex.getMessage());

        }
    }

    private void exporter() {
        File fichier = Hydraulique1dExport.chooseFile("met");
        //Conversion
        if (fichier == null) {
            return;
        }
        Hydraulique1dExport.exportListing(fichier,tabModel_.getByte());
    }

}
