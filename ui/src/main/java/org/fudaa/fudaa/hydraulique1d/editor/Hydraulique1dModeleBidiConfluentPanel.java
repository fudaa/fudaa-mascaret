package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;

import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

public class Hydraulique1dModeleBidiConfluentPanel extends BuPanel {
  private MetierNoeud noeud_;
  private BuButton btEstimation_;
  private BuTextField[][] tfPoints1_= new BuTextField[3][3];
  private BuTextField[][] tfPoints2_= new BuTextField[3][4];

  private BuLabel[] lbExtremites1_= new BuLabel[3];
  private BuLabel[] lbExtremites2_= new BuLabel[3];

  public Hydraulique1dModeleBidiConfluentPanel() {
    super();
    Border bdEtched = BorderFactory.createEtchedBorder();
    setBorder(BorderFactory.createTitledBorder(bdEtched,Hydraulique1dResource.HYDRAULIQUE1D.getString("Modélisation bidimentionnelle locale")));
    super.setLayout(new BorderLayout());

    BuPanel pnCoordNormales = new BuPanel(new BorderLayout());
    pnCoordNormales.setBorder(BorderFactory.createTitledBorder(bdEtched,Hydraulique1dResource.HYDRAULIQUE1D.getString("Coordonnées des normales extérieures")));
    BuLabel lbImageConfluentNormales=new BuLabel();
    lbImageConfluentNormales.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("hydraulique1dconfluentnormales.gif"));
    lbImageConfluentNormales.setHorizontalAlignment(BuLabel.CENTER);
    pnCoordNormales.add(lbImageConfluentNormales, BorderLayout.NORTH);

    BuPanel pnCoordPtsExtremites = new BuPanel(new BorderLayout());
    pnCoordPtsExtremites.setBorder(BorderFactory.createTitledBorder(bdEtched,Hydraulique1dResource.HYDRAULIQUE1D.getString("Coordonnées des points des extrémités des profils")));
    BuLabel lbImageConfluentExtremitees=new BuLabel();
    lbImageConfluentExtremitees.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("hydraulique1dconfluentextremitees.gif"));
    lbImageConfluentExtremitees.setHorizontalAlignment(BuLabel.CENTER);
    pnCoordPtsExtremites.add(lbImageConfluentExtremitees, BorderLayout.NORTH);

    BuLabelMultiLine lbNord = new BuLabelMultiLine(Hydraulique1dResource.HYDRAULIQUE1D.getString("La modélisation bidimensionnelle nécessite la saisie des coordonnées des normales")+"      \n"+
                                                 Hydraulique1dResource.HYDRAULIQUE1D.getString("Elles peuvent être éventuellement estimées par les coordonnées des points des extrémités"));
    lbNord.setHorizontalAlignment(BuLabelMultiLine.CENTER);

    BuPanel pnBtEstimation = new BuPanel(new FlowLayout());
    btEstimation_ = new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Estimation des coordonnées des normales"));
    btEstimation_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        estimParam();
      }
    });
    pnBtEstimation.add(btEstimation_);

    super.add(lbNord, BorderLayout.NORTH);
    super.add(pnCoordNormales, BorderLayout.WEST);
    super.add(pnBtEstimation, BorderLayout.SOUTH);
    super.add(pnCoordPtsExtremites, BorderLayout.EAST);

    BuPanel pnExtremite = initialisationPanneauExtremite();

    pnCoordPtsExtremites.add(pnExtremite, BorderLayout.CENTER);

    BuPanel pnNormale = initialisationPanneauNormales();

    pnCoordNormales.add(pnNormale, BorderLayout.CENTER);

  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    btEstimation_.setEnabled(enabled);
    for (int i = 0; i < tfPoints1_.length; i++) {
      for (int j = 0; j < tfPoints1_[i].length; j++) {
        tfPoints1_[i][j].setEnabled(enabled);
      }
    }
    for (int i = 0; i < tfPoints2_.length; i++) {
      for (int j = 0; j < tfPoints2_[i].length; j++) {
        tfPoints2_[i][j].setEnabled(enabled);
      }
    }

  }

  /**
   * initialisationPanneauNormales
   *
   * @return BuPanel
   */
  private BuPanel initialisationPanneauNormales() {
    BuGridLayout loExtremites= new BuGridLayout(5, 5, 5, true, false);
    BuPanel pnNormale= new BuPanel(loExtremites);

    pnNormale.add(new BuLabel(""), 0);
    pnNormale.add(new BuLabel(""), 1);
    pnNormale.add(new BuLabel("X", SwingConstants.CENTER), 2);
    pnNormale.add(new BuLabel("Y", SwingConstants.CENTER), 3);
    pnNormale.add(new BuLabel("\u03b1", SwingConstants.CENTER), 4);
    LineBorder lineBorder = new LineBorder(Color.black);
    for (int i = 0; i < lbExtremites1_.length; i++) {
      lbExtremites1_[i] = new BuLabel(""+(i+1));
      lbExtremites1_[i].setBorder(lineBorder);
    }
    for (int i = 0; i < tfPoints1_.length; i++) {
      for (int j = 0; j < tfPoints1_[i].length; j++) {
        tfPoints1_[i][j] = BuTextField.createDoubleField();
        tfPoints1_[i][j].setColumns(7);
        tfPoints1_[i][j].setEditable(true);
      }
    }
    String extremite = Hydraulique1dResource.HYDRAULIQUE1D.getString("Extremité");
    pnNormale.add(new BuLabel(extremite), 5);
    pnNormale.add(lbExtremites1_[0], 6);
    for (int i = 0; i < 3; i++)
      pnNormale.add(tfPoints1_[0][i], 7 + i);
    pnNormale.add(new BuLabel(extremite), 10);
    pnNormale.add(lbExtremites1_[1], 11);
    for (int i = 0; i < 3; i++)
      pnNormale.add(tfPoints1_[1][i], 12 + i);
    pnNormale.add(new BuLabel(extremite), 15);
    pnNormale.add(lbExtremites1_[2], 16);
    for (int i = 0; i < 3; i++)
      pnNormale.add(tfPoints1_[2][i], 17 + i);

    return pnNormale;
  }

  private BuPanel initialisationPanneauExtremite() {
    BuGridLayout loExtremites= new BuGridLayout(6, 5, 5, true, false);
    BuPanel pnExtremite= new BuPanel(loExtremites);

    pnExtremite.add(new BuLabel(""), 0);
    pnExtremite.add(new BuLabel(""), 1);
    pnExtremite.add(new BuLabel("Pt1 X", SwingConstants.CENTER), 2);
    pnExtremite.add(new BuLabel("Pt1 Y", SwingConstants.CENTER), 3);
    pnExtremite.add(new BuLabel("Pt2 X", SwingConstants.CENTER), 4);
    pnExtremite.add(new BuLabel("Pt2 Y", SwingConstants.CENTER), 5);
    LineBorder lineBorder = new LineBorder(Color.black);
    for (int i = 0; i < lbExtremites2_.length; i++) {
      lbExtremites2_[i] = new BuLabel(""+(i+1));
      lbExtremites2_[i].setBorder(lineBorder);
    }
    for (int i= 0; i < tfPoints2_.length; i++) {
      for (int j= 0; j < tfPoints2_[i].length; j++) {
        tfPoints2_[i][j]= BuTextField.createDoubleField();
        tfPoints2_[i][j].setColumns(5);
        tfPoints2_[i][j].setEditable(true);
      }
    }
    String extremite = Hydraulique1dResource.HYDRAULIQUE1D.getString("Extremité");
    pnExtremite.add(new BuLabel(extremite), 6);
    pnExtremite.add(lbExtremites2_[0], 7);
    for (int i = 0; i < 4; i++)
      pnExtremite.add(tfPoints2_[0][i], 8 + i);

    pnExtremite.add(new BuLabel(extremite), 12);
    pnExtremite.add(lbExtremites2_[1], 13);
    for (int i = 0; i < 4; i++)
      pnExtremite.add(tfPoints2_[1][i], 14 + i);

    pnExtremite.add(new BuLabel(extremite), 18);
    pnExtremite.add(lbExtremites2_[2], 19);
    for (int i = 0; i < 4; i++)
      pnExtremite.add(tfPoints2_[2][i], 20 + i);

    return pnExtremite;
  }
  public void setNoeudMetier(MetierNoeud noeud) {
    noeud_ = noeud;
    if (noeud_ !=null) {

      for (int i= 0; i < lbExtremites1_.length; i++) {
        lbExtremites1_[i].setText(" " + noeud_.extremites()[i].numero() + " ");
        if (noeud_.extremites()[i].pointMilieu() != null) {
          tfPoints1_[i][0].setValue(new Double(noeud_.extremites()[i].pointMilieu().x));
          tfPoints1_[i][1].setValue(new Double(noeud_.extremites()[i].pointMilieu().y));
          tfPoints1_[i][2].setValue(new Double(noeud_.extremites()[i].angle()));
        }
      }

      for (int i= 0; i < lbExtremites2_.length; i++) {
        lbExtremites2_[i].setText(" " + noeud_.extremites()[i].numero() + " ");
        if (noeud_.extremites()[i].point1() != null) {
          tfPoints2_[i][0].setValue(new Double(noeud_.extremites()[i].point1().x));
          tfPoints2_[i][1].setValue(new Double(noeud_.extremites()[i].point1().y));
          tfPoints2_[i][2].setValue(new Double(noeud_.extremites()[i].point2().x));
          tfPoints2_[i][3].setValue(new Double(noeud_.extremites()[i].point2().y));
        }
      }
    }
  }
  public MetierNoeud getNoeudMetier() {
    if (! super.isEnabled()) return noeud_;
    MetierExtremite[] extremites = noeud_.extremites();
    for (int i = 0; i < 3; i++) {
      Double ptiX     = (Double)tfPoints1_[i][0].getValue();
      Double ptiY     = (Double)tfPoints1_[i][1].getValue();
      Double ptiAngle = (Double)tfPoints1_[i][2].getValue();
      if ((ptiX != null) && (ptiY != null) && (ptiAngle != null)) {
        if (extremites[i].pointMilieu() == null) extremites[i].creePoints();
        extremites[i].pointMilieu(new MetierPoint2D(ptiX.doubleValue(),ptiY.doubleValue()));
        extremites[i].angle(ptiAngle.doubleValue());
      }

      Double ptiPt1X     = (Double)tfPoints2_[i][0].getValue();
      Double ptiPt1Y     = (Double)tfPoints2_[i][1].getValue();
      Double ptiPt2X     = (Double)tfPoints2_[i][2].getValue();
      Double ptiPt2Y     = (Double)tfPoints2_[i][3].getValue();
      if ((ptiPt1X != null) && (ptiPt1Y != null) && (ptiPt2X != null)&& (ptiPt2Y != null)) {
        if (extremites[i].point1() == null) extremites[i].creePoints();
        extremites[i].point1(new MetierPoint2D(ptiPt1X.doubleValue(),ptiPt1Y.doubleValue()));
        extremites[i].point2(new MetierPoint2D(ptiPt2X.doubleValue(),ptiPt2Y.doubleValue()));
      }
    }
    noeud_.extremites(extremites);
    return noeud_;
  }

  public boolean isChanged() {
    if (! super.isEnabled()) return false;
    boolean changed= false;
    for (int i = 0; i < 3; i++) {
      MetierExtremite extremite = noeud_.extremites()[i];

      Double ptiX     = (Double)tfPoints1_[i][0].getValue();
      Double ptiY     = (Double)tfPoints1_[i][1].getValue();
      if (! isEgale(extremite.pointMilieu(), ptiX, ptiY)) return true;

      Double ptiPt1X  = (Double)tfPoints2_[i][0].getValue();
      Double ptiPt1Y  = (Double)tfPoints2_[i][1].getValue();
      if (! isEgale(extremite.point1(), ptiPt1X, ptiPt1Y)) return true;

      Double ptiPt2X  = (Double)tfPoints2_[i][2].getValue();
      Double ptiPt2Y  = (Double)tfPoints2_[i][3].getValue();
      if (! isEgale(extremite.point2(), ptiPt2X, ptiPt2Y)) return true;

      Double ptiAngle = (Double)tfPoints1_[i][2].getValue();
      if (ptiAngle == null) return true;
      if (extremite.angle() != ptiAngle.doubleValue()) return true;
    }
    return changed;
  }

  private boolean isEgale(MetierPoint2D pt, Double x, Double y) {
    if ((pt == null) && (x==null) && (y==null)) return true;
    if ((pt == null) && (x!=null) && (y==null)) return false;
    if ((pt == null) && (x==null) && (y!=null)) return false;
    if ((pt == null) && (x!=null) && (y!=null)) return false;
    if ((pt != null) && (x==null) && (y==null)) return false;
    if ((pt != null) && (x!=null) && (y==null)) return false;
    if ((pt != null) && (x==null) && (y!=null)) return false;

    if (pt.x != x.doubleValue()) return false;
    if (pt.y != y.doubleValue()) return false;

    return true;
  }

  // Hydraulique1dNoeudTransParamEditor
  private void estimParam() {
    try {
      MetierPoint2D[][] ptsExtremitees = new MetierPoint2D[3][2];
      double pt01x = ((Double) tfPoints2_[0][0].getValue()).doubleValue();
      double pt01y = ((Double) tfPoints2_[0][1].getValue()).doubleValue();
      double pt02x = ((Double) tfPoints2_[0][2].getValue()).doubleValue();
      double pt02y = ((Double) tfPoints2_[0][3].getValue()).doubleValue();
      ptsExtremitees[0][0] = new MetierPoint2D(pt01x, pt01y);
      ptsExtremitees[0][1] = new MetierPoint2D(pt02x, pt02y);

      double pt11x = ((Double) tfPoints2_[1][0].getValue()).doubleValue();
      double pt11y = ((Double) tfPoints2_[1][1].getValue()).doubleValue();
      double pt12x = ((Double) tfPoints2_[1][2].getValue()).doubleValue();
      double pt12y = ((Double) tfPoints2_[1][3].getValue()).doubleValue();
      ptsExtremitees[1][0] = new MetierPoint2D(pt11x, pt11y);
      ptsExtremitees[1][1] = new MetierPoint2D(pt12x, pt12y);

      double pt21x = ((Double) tfPoints2_[2][0].getValue()).doubleValue();
      double pt21y = ((Double) tfPoints2_[2][1].getValue()).doubleValue();
      double pt22x = ((Double) tfPoints2_[2][2].getValue()).doubleValue();
      double pt22y = ((Double) tfPoints2_[2][3].getValue()).doubleValue();
      ptsExtremitees[2][0] = new MetierPoint2D(pt21x, pt21y);
      ptsExtremitees[2][1] = new MetierPoint2D(pt22x, pt22y);

      MetierPoint2D[] ptsMilieu = new MetierPoint2D[3];
      for (int i = 0; i < ptsMilieu.length; i++) {
        ptsMilieu[i] = milieu(ptsExtremitees[i][0], ptsExtremitees[i][1]);
      }

      MetierPoint2D bary = barycentre(ptsMilieu[0], ptsMilieu[1], ptsMilieu[2]);
      double[] angles = new double[3];
      for (int i = 0; i < 3; i++) {
        MetierPoint2D na = normalAbs(new MetierPoint2D(ptsExtremitees[i][1].x -
            ptsExtremitees[i][0].x,
            ptsExtremitees[i][1].y - ptsExtremitees[i][0].y));
        MetierPoint2D nExt = normalExterieur(na, bary, ptsMilieu[i]);
        angles[i] = angleAxeX(nExt);
      }
      if (isContienNaN(ptsMilieu, angles)) {
        new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR : le calcul des coordonnées des")+ " \n" +
        Hydraulique1dResource.HYDRAULIQUE1D.getString("normales extérieures a échoué."))
        .activate();

      }
      else {
        for (int i = 0; i < 3; i++) {
          tfPoints1_[i][0].setValue(Double.valueOf(ptsMilieu[i].x));
          tfPoints1_[i][1].setValue(Double.valueOf(ptsMilieu[i].y));
          tfPoints1_[i][2].setValue(Double.valueOf(angles[i]));
        }
      }
    }
    catch (Exception ex) {
      new BuDialogError(
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
        ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("ERREUR : l'estimation des coordonnées des")+" \n" +
        Hydraulique1dResource.HYDRAULIQUE1D.getString("normales extérieures a échoué."))
        .activate();

    }
  }
  private boolean isContienNaN( MetierPoint2D[] ptsMilieu, double[] angles) {
    boolean contienNaN = false;
    for (int i = 0; i < angles.length; i++) {
      if (Double.isNaN(ptsMilieu[i].x)) return true;
      if (Double.isNaN(ptsMilieu[i].y)) return true;
      if (Double.isNaN(angles[i])) return true;
    }
    return contienNaN;
  }
  private MetierPoint2D milieu(MetierPoint2D pt1, MetierPoint2D pt2) {
    return new MetierPoint2D((pt1.x + pt2.x) / 2, (pt1.y + pt2.y) / 2);
  }
  private MetierPoint2D barycentre(MetierPoint2D pt1, MetierPoint2D pt2, MetierPoint2D pt3) {
    return new MetierPoint2D(
      (pt1.x + pt2.x + pt3.x) / 3,
      (pt1.y + pt2.y + pt3.y) / 3);
  }
  private MetierPoint2D normalAbs(MetierPoint2D vec) {
    return new MetierPoint2D(Math.abs(vec.y), Math.abs(vec.x));
  }
  private MetierPoint2D normalExterieur(
    MetierPoint2D normalAbs,
    MetierPoint2D bary,
    MetierPoint2D milieu) {
    MetierPoint2D mg= new MetierPoint2D(bary.x - milieu.x, bary.y - milieu.y);
    MetierPoint2D normalExt= normalAbs;
    if (mg.x > 0)
      normalExt.x= normalExt.x * -1;
    if (mg.y > 0)
      normalExt.y= normalExt.y * -1;
    return normalExt;
  }
  private double angleAxeX(MetierPoint2D normalExt) {
    double angle= Math.toDegrees(Math.atan(normalExt.y / normalExt.x));
    if (normalExt.x == 0) {
      if (normalExt.y == 0) {
        angle= Double.NaN;
      } else if (normalExt.y > 0)
        angle= 90;
      else if (normalExt.y < 0)
        angle= -90;
    } else if (normalExt.x < 0)
      angle= angle + 180;
    return angle;
  }
}
