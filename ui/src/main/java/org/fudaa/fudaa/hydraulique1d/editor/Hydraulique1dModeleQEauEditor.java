/*
 * @file         Hydraulique1dModeleQEauEditor.java
 * @creation     2006-03-18
 * @modification $Date: 2007-11-20 11:42:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierModeleQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresModeleQualiteEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresQualiteDEau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauChainesModel;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur de s�lection du du modele de qualite d'eau (MetierParametresModeleQualiteEau ).<br>
 * Appeler si l'utilisateur clic sur le menu "Qualite d'eau/Modele de qualite d'eau".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().MODELE_QUALITE_EAU().editer().<br>
 * @version      $Revision: 1.13 $ $Date: 2007-11-20 11:42:43 $ by $Author: bmarchan $
 * @author       Olivier Pasteur
 */
public class Hydraulique1dModeleQEauEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {

final static String[][] TRACEURS_TRANSPORT_PUR = { {"TRA1", "Traceur 1"}
};
final static String[][] TRACEURS_O2 = { {"O2", Hydraulique1dResource.HYDRAULIQUE1D.getString("Oxyg�ne dissous")}, {"LOR",
                                    Hydraulique1dResource.HYDRAULIQUE1D.getString("Charge organique")}, {"NH4",
                                    Hydraulique1dResource.HYDRAULIQUE1D.getString("Charge ammoniacale")}
};
final static String[][] TRACEURS_BIOMASS = { {"PHY",
                                         Hydraulique1dResource.HYDRAULIQUE1D.getString("Biomasse phytoplanctonique")},
                                         {"PO4",
                                         Hydraulique1dResource.HYDRAULIQUE1D.getString("Phosphore min�ral assimilable")},
                                         {"POR",
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Phosphore min�ral non assimilable")}, {"NO3",
                                         Hydraulique1dResource.HYDRAULIQUE1D.getString("Azote min�ral assimilable")},
                                         {"NOR",
                                         Hydraulique1dResource.HYDRAULIQUE1D.getString("Azote min�ral non assimilable")}
};
final static String[][] TRACEURS_EUTRO = { {"PHY",
                                       Hydraulique1dResource.HYDRAULIQUE1D.getString("Biomasse phytoplanctonique")},
                                       {"PO4",
                                       Hydraulique1dResource.HYDRAULIQUE1D.getString("Phosphore min�ral assimilable")},
                                       {"POR",
      Hydraulique1dResource.HYDRAULIQUE1D.getString("phosphore min�ral non assimilable")}, {"NO3",
                                       Hydraulique1dResource.HYDRAULIQUE1D.getString("Azote min�ral assimilable")},
                                       {"NOR",
                                       Hydraulique1dResource.HYDRAULIQUE1D.getString("Azote min�ral non assimilable")},
                                       {"NH4", Hydraulique1dResource.HYDRAULIQUE1D.getString("Charge ammoniacale")},
                                       {"LOR", Hydraulique1dResource.HYDRAULIQUE1D.getString("Charge organique")}, {"O2",
                                       Hydraulique1dResource.HYDRAULIQUE1D.getString("Oxyg�ne dissous")}
};
;
final static String[][] TRACEURS_MICROPOL = { {"MES",
                                          Hydraulique1dResource.HYDRAULIQUE1D.getString("Mati�res en suspension")},
                                          {"SED", Hydraulique1dResource.HYDRAULIQUE1D.getString("s�diments")}, {"C_EAU",
                                          Hydraulique1dResource.HYDRAULIQUE1D.getString("Concentration dans l'eau")},
                                          {"C_MES",
                                          Hydraulique1dResource.HYDRAULIQUE1D.getString("Concentration dans les MES")},
                                          {"C_SED",
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Concentration dans les s�diments")}
};
;
final static String[][] TRACEURS_THERMIC = { {"T_EAU",
                                         Hydraulique1dResource.HYDRAULIQUE1D.getString("Temp�rature de l'eau")}
};
final static Hashtable NOMS_TRACEURS;
final static Hashtable NOMBRES_TRACEURS;

final static String TRANSPORT_PUR = "TRANSPORT_PUR";
final static String O2 = "O2";
final static String BIOMASS = "BIOMASS";
final static String EUTRO = "EUTRO";
final static String MICROPOL = "MICROPOL";
final static String THERMIC = "THERMIC";

JComboBox cmbModeleQE_, cmnNbTraceurs_;
BuCheckBox chkActivation_;
Hydraulique1dTableau tabNomTraceurs_;
JScrollPane spNomsTraceurs_;
BuPanel pnActivationQE_, pnModele_, pnNbTraceur_, pnModeleQEau_;
BuVerticalLayout loModeleQEau_;
BuHorizontalLayout loActivationQE_, loModele_, loNbTraceur_;
private boolean bModeleInitialementVide;
private MetierParametresQualiteDEau paramQE_;
private MetierParametresModeleQualiteEau paramModeleQE_;
private MetierDonneesHydrauliques donneesHydro_;
private MetierReseau reseau_;
static {
  NOMS_TRACEURS = new Hashtable(6);
  NOMBRES_TRACEURS = new Hashtable(6);
  NOMS_TRACEURS.put(TRANSPORT_PUR, TRACEURS_TRANSPORT_PUR);
  NOMS_TRACEURS.put(O2, TRACEURS_O2);
  NOMS_TRACEURS.put(BIOMASS, TRACEURS_BIOMASS);
  NOMS_TRACEURS.put(EUTRO, TRACEURS_EUTRO);
  NOMS_TRACEURS.put(MICROPOL, TRACEURS_MICROPOL);
  NOMS_TRACEURS.put(THERMIC, TRACEURS_THERMIC);
  NOMBRES_TRACEURS.put(TRANSPORT_PUR, new Integer(1));
  NOMBRES_TRACEURS.put(O2, new Integer(3));
  NOMBRES_TRACEURS.put(BIOMASS, new Integer(5));
  NOMBRES_TRACEURS.put(EUTRO, new Integer(8));
  NOMBRES_TRACEURS.put(MICROPOL, new Integer(5));
  NOMBRES_TRACEURS.put(THERMIC, new Integer(1));
}

public Hydraulique1dModeleQEauEditor() {
  this(null);
}

public Hydraulique1dModeleQEauEditor(BDialogContent parent) {
  super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Choix du mod�le de qualit� d'eau"));
  paramModeleQE_ = null;
  donneesHydro_=null;
  reseau_=null;
  paramQE_ = null;
  bModeleInitialementVide = true;
  loModeleQEau_ = new BuVerticalLayout(5, false, false);
  loActivationQE_ = new BuHorizontalLayout(5, false, false);
  loModele_ = new BuHorizontalLayout(5, false, false);
  loNbTraceur_ = new BuHorizontalLayout(5, false, false);

  Container pnMain_ = getContentPane();
  pnActivationQE_ = new BuPanel();
  pnActivationQE_.setLayout(loActivationQE_);
  pnActivationQE_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
  chkActivation_ = new BuCheckBox();
  chkActivation_.addActionListener(this);
  chkActivation_.setActionCommand("ACTIVATION_QE");
  pnActivationQE_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Activation de la qualit� d'eau ")), 0);
  pnActivationQE_.add(chkActivation_, 1);

  pnModele_ = new BuPanel();
  pnModele_.setLayout(loModele_);
  pnModele_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));

  cmbModeleQE_ = new JComboBox(new String[] {TRANSPORT_PUR, O2, BIOMASS,
                               EUTRO, MICROPOL, THERMIC});
  cmbModeleQE_.addActionListener(this);
  cmbModeleQE_.setActionCommand("COMBO_MODELE");
  pnModele_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mod�le de qualit� d'eau ")), 0);
  pnModele_.add(cmbModeleQE_, 1);

  pnNbTraceur_ = new BuPanel();
  pnNbTraceur_.setLayout(loNbTraceur_);
  pnNbTraceur_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
  cmnNbTraceurs_ = new JComboBox(new String[] {"1", "2", "3", "4", "5",
                                 "6", "7", "8", "9", "10"});
  cmnNbTraceurs_.addActionListener(this);
  cmnNbTraceurs_.setActionCommand("NB_TRACEURS");
  pnNbTraceur_.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nombre de traceurs ")), 0);
  pnNbTraceur_.add(cmnNbTraceurs_, 1);

  String[] tabNoms_ = {Hydraulique1dResource.HYDRAULIQUE1D.getString("Noms des traceurs"),
                      "            "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Description")+"            "};
  Hydraulique1dTableauChainesModel modele = new
          Hydraulique1dTableauChainesModel(tabNoms_, 0);
  String[][] tabTraceurs_ = { {"TRA1", Hydraulique1dResource.HYDRAULIQUE1D.getString("Traceur")+" 1"}
  };
  modele.setTabString(tabTraceurs_);
  tabNomTraceurs_ = new Hydraulique1dTableau(modele);
  spNomsTraceurs_ = new JScrollPane(tabNomTraceurs_);
  spNomsTraceurs_.setVerticalScrollBarPolicy(JScrollPane.
          VERTICAL_SCROLLBAR_ALWAYS);
  spNomsTraceurs_.setPreferredSize(new Dimension(400, 200));

  spNomsTraceurs_.setBorder(BorderFactory.createCompoundBorder(
          BorderFactory.createEtchedBorder(),
          BorderFactory.createEmptyBorder(5, 5, 5, 5)));
  pnModeleQEau_ = new BuPanel();
  pnModeleQEau_.setLayout(loModeleQEau_);
  pnModeleQEau_.setBorder(BorderFactory.createCompoundBorder(
          BorderFactory.createEtchedBorder(),
          BorderFactory.createEmptyBorder(5, 5, 5, 5)));
  pnModeleQEau_.add(pnActivationQE_);
  pnModeleQEau_.add(pnModele_);
  pnModeleQEau_.add(pnNbTraceur_);
  pnModeleQEau_.add(spNomsTraceurs_);

  pnMain_.add(pnModeleQEau_, BorderLayout.CENTER);
  setNavPanel(EbliPreferences.DIALOG.VALIDER |
              EbliPreferences.DIALOG.ANNULER);
  pack();
}

  @Override
  public void actionPerformed(ActionEvent _evt) {
  String cmd = _evt.getActionCommand();
  if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
          firePropertyChange("object", null, paramModeleQE_);
      }
      //ce champs n'est � vrai que lors de la premiere ouverture de l'editeur
      bModeleInitialementVide=false;
      fermer();
  } else if ("ACTIVATION_QE".equals(cmd)) {
	  System.out.println("ACTIVATION_QE");
	  if (chkActivation_.isSelected()) {
		  if (avertissementActivationQE()){
	          CtuluLibSwing.griserPanel(pnModele_, true);
	          if (cmbModeleQE_.getSelectedItem().equals(TRANSPORT_PUR)) {
	              CtuluLibSwing.griserPanel(pnNbTraceur_, true);
	              CtuluLibSwing.griserPanel(spNomsTraceurs_, true);
	          }
		  }else chkActivation_.setSelected(false);
	  } else {
		  if (avertissementDesactivationQE()){
	          CtuluLibSwing.griserPanel(pnModele_, false);
	          CtuluLibSwing.griserPanel(pnNbTraceur_, false);
	          CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
		  } else chkActivation_.setSelected(true);
      }
  } else if ("COMBO_MODELE".equals(cmd)) {
      String itemSectionne = (String) cmbModeleQE_.getSelectedItem();
      if (itemSectionne.equals(TRANSPORT_PUR)) {
          CtuluLibSwing.griserPanel(spNomsTraceurs_, true);
          CtuluLibSwing.griserPanel(pnNbTraceur_, true);
          cmnNbTraceurs_.setSelectedIndex(((Integer) NOMBRES_TRACEURS.get(
                  TRANSPORT_PUR)).intValue() - 1);
          ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
                  setValeurs((String[][]) NOMS_TRACEURS.get(TRANSPORT_PUR));

      } else if (itemSectionne.equals(O2)) {
          cmnNbTraceurs_.setSelectedIndex(((Integer) NOMBRES_TRACEURS.get(
                  O2)).intValue() - 1);
          CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
          ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
                  setValeurs((String[][]) NOMS_TRACEURS.get(O2));
          CtuluLibSwing.griserPanel(pnNbTraceur_, false);

      } else if (itemSectionne.equals(BIOMASS)) {
          cmnNbTraceurs_.setSelectedIndex(((Integer) NOMBRES_TRACEURS.get(
                  BIOMASS)).intValue() - 1);
          CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
          ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
                  setValeurs((String[][]) NOMS_TRACEURS.get(BIOMASS));
          CtuluLibSwing.griserPanel(pnNbTraceur_, false);

      } else if (itemSectionne.equals(EUTRO)) {
          cmnNbTraceurs_.setSelectedIndex(((Integer) NOMBRES_TRACEURS.get(
                  EUTRO)).intValue() - 1);
          CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
          ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
                  setValeurs((String[][]) NOMS_TRACEURS.get(EUTRO));
          CtuluLibSwing.griserPanel(pnNbTraceur_, false);

      } else if (itemSectionne.equals(MICROPOL)) {
          cmnNbTraceurs_.setSelectedIndex(((Integer) NOMBRES_TRACEURS.get(
                  MICROPOL)).intValue() - 1);
          CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
          ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
                  setValeurs((String[][]) NOMS_TRACEURS.get(MICROPOL));
          CtuluLibSwing.griserPanel(pnNbTraceur_, false);

      } else if (itemSectionne.equals(THERMIC)) {
          cmnNbTraceurs_.setSelectedIndex(((Integer) NOMBRES_TRACEURS.get(
                  THERMIC)).intValue() - 1);
          CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
          ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
                  setValeurs((String[][]) NOMS_TRACEURS.get(THERMIC));
          CtuluLibSwing.griserPanel(pnNbTraceur_, false);

      }
      //NOMBRES_TRACEURS.
  } else if ("NB_TRACEURS".equals(cmd)) {
      int nbTraceurs = cmnNbTraceurs_.getSelectedIndex() + 1;
      ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
              reDimentionner(nbTraceurs);
      String[][] tab = new String[nbTraceurs][2];
      for (int i = 0; i < tab.length; i++) {
          tab[i][0] = "TRA" + (i + 1);
          tab[i][1] = Hydraulique1dResource.HYDRAULIQUE1D.getString("Traceur")+" " + (i + 1);
      }
      ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
              setValeurs(tab);

  } else
      super.actionPerformed(_evt);
}


  @Override
  protected boolean getValeurs() {

  boolean changed = false;

  if (paramModeleQE_ == null)
      return changed;

  //Modele de qualite d'eau
  String itemSectionne = (String) cmbModeleQE_.getSelectedItem();
  boolean bConfirmationEffectuee = false;
  if (itemSectionne.equals(TRANSPORT_PUR)) {
      if (paramModeleQE_.modeleQualiteEau().value() !=
          EnumMetierModeleQualiteDEau._TRANSPORT_PUR) {
          boolean bConfirm = avertissementPerteDonnees();
          bConfirmationEffectuee = true;
          if  (bConfirm){
              paramModeleQE_.modeleQualiteEau(EnumMetierModeleQualiteDEau.
                                              TRANSPORT_PUR);
              changed = true;
          } else {
              setValeurs();
              return false;
              }
      }
  } else if (itemSectionne.equals(O2)) {
      if (paramModeleQE_.modeleQualiteEau().value() !=
          EnumMetierModeleQualiteDEau._O2) {
          boolean bConfirm = avertissementPerteDonnees();
          bConfirmationEffectuee = true;
          if  (bConfirm){
              paramModeleQE_.modeleQualiteEau(EnumMetierModeleQualiteDEau.O2);
              changed = true;
          } else {
              setValeurs();
              return false;
          }

      }
  } else if (itemSectionne.equals(BIOMASS)) {
      if (paramModeleQE_.modeleQualiteEau().value() !=
          EnumMetierModeleQualiteDEau._BIOMASS) {
          boolean bConfirm = avertissementPerteDonnees();
          bConfirmationEffectuee = true;
          if  (bConfirm){
              paramModeleQE_.modeleQualiteEau(EnumMetierModeleQualiteDEau.BIOMASS);
              changed = true;
          } else {
              setValeurs();
              return false;
          }

      }
  } else if (itemSectionne.equals(EUTRO)) {
      if (paramModeleQE_.modeleQualiteEau().value() !=
          EnumMetierModeleQualiteDEau._EUTRO) {
          boolean bConfirm = avertissementPerteDonnees();
          bConfirmationEffectuee = true;
          if  (bConfirm){
              paramModeleQE_.modeleQualiteEau(EnumMetierModeleQualiteDEau.EUTRO);
              changed = true;
          } else {
              setValeurs();
              return false;
          }

      }
  } else if (itemSectionne.equals(MICROPOL)) {
      if (paramModeleQE_.modeleQualiteEau().value() !=
          EnumMetierModeleQualiteDEau._MICROPOL) {
          boolean bConfirm = avertissementPerteDonnees();
          bConfirmationEffectuee = true;
          if  (bConfirm){
              paramModeleQE_.modeleQualiteEau(EnumMetierModeleQualiteDEau.MICROPOL);
              changed = true;
          } else {
              setValeurs();
              return false;
          }

      }
  } else if (itemSectionne.equals(THERMIC)) {
      if (paramModeleQE_.modeleQualiteEau().value() !=
          EnumMetierModeleQualiteDEau._THERMIC) {
          boolean bConfirm = avertissementPerteDonnees();
          bConfirmationEffectuee = true;
          if  (bConfirm){
              paramModeleQE_.modeleQualiteEau(EnumMetierModeleQualiteDEau.THERMIC);
              changed = true;
          } else {
              cmbModeleQE_.setSelectedIndex(paramModeleQE_.modeleQualiteEau().value());
          }

      }
  }


  //Nombre de traceurs
  int nbTraceurs = cmnNbTraceurs_.getSelectedIndex() + 1;
  if (nbTraceurs != paramModeleQE_.nbTraceur()) {

	  if (!bConfirmationEffectuee) {
		  boolean bConfirm = avertissementPerteDonnees();
	      if  (bConfirm){
	          paramModeleQE_.nbTraceur(nbTraceurs);
	          changed = true;
	          } else {
              setValeurs();
              return false;
	          }
	  }else /*si confirmation deja faite on accepte le nouveau nombre de traceur*/{
		  paramModeleQE_.nbTraceur(nbTraceurs);
          changed = true;
	  }

  }

  //si changement de modele ou de nombre de traceur on remet � jour les autres donn�es
  //L'activation de la QE et les noms des traceur doivent se trouver apres cela
  if (changed) {
	  paramQE_.initialisationSelonModeleQE();
	  donneesHydro_.supprimeLois(donneesHydro_.getLoisTracer());
  }



  //Activation de la qualit� d'eau
  if (chkActivation_.isSelected()) {
      if (!paramModeleQE_.presenceTraceurs()) {
          paramModeleQE_.presenceTraceurs(true);
          changed = true;
      }
  } else { //si la QE est desactivee
      if (paramModeleQE_.presenceTraceurs()) {
          paramModeleQE_.presenceTraceurs(false);
          changed = true;
      }
  }

  //Noms des traceurs
  Hydraulique1dTableauChainesModel modele = (
          Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel();
  String[][] tab = modele.getValeursTableau();
  if (!Arrays.equals(tab, paramModeleQE_.vvNomTracer())) {
      paramModeleQE_.vvNomTracer(tab);
      changed = true;
  }

  return changed;
}

  @Override
  public void setObject(MetierHydraulique1d _n) {

  if (_n instanceof MetierParametresQualiteDEau) {
      MetierParametresQualiteDEau param =(MetierParametresQualiteDEau)_n;

      if (param == paramQE_)
          return;
      paramQE_ = param;
      paramModeleQE_ = paramQE_.parametresModeleQualiteEau();


  } else if(_n instanceof MetierDonneesHydrauliques) {
          MetierDonneesHydrauliques donneeHydro =(MetierDonneesHydrauliques)_n;
          if (donneeHydro == donneesHydro_)
              return;
          donneesHydro_ = donneeHydro;

  } else if(_n instanceof MetierReseau) {
	  MetierReseau reseau =(MetierReseau)_n;
      if (reseau == reseau_)
          return;
      reseau_ = reseau;
      setValeurs();

      }else {
      return;
  }
}

  @Override
  protected void setValeurs() {

	if (paramModeleQE_.modeleQualiteEau()==null){
	  paramModeleQE_.modeleQualiteEau(EnumMetierModeleQualiteDEau.TRANSPORT_PUR);
	  paramQE_.initialisationSelonModeleQE();
	}
  switch (paramModeleQE_.modeleQualiteEau().value()) {
  case EnumMetierModeleQualiteDEau._TRANSPORT_PUR:
      cmbModeleQE_.setSelectedIndex(0);
      CtuluLibSwing.griserPanel(spNomsTraceurs_, true);
      CtuluLibSwing.griserPanel(pnNbTraceur_, true);
      break;
  case EnumMetierModeleQualiteDEau._O2:
      cmbModeleQE_.setSelectedIndex(1);
      CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
      CtuluLibSwing.griserPanel(pnNbTraceur_, false);
      break;
  case EnumMetierModeleQualiteDEau._BIOMASS:
      cmbModeleQE_.setSelectedIndex(2);
      CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
      CtuluLibSwing.griserPanel(pnNbTraceur_, false);
      break;
  case EnumMetierModeleQualiteDEau._EUTRO:
      cmbModeleQE_.setSelectedIndex(3);
      CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
      CtuluLibSwing.griserPanel(pnNbTraceur_, false);
      break;
  case EnumMetierModeleQualiteDEau._MICROPOL:
      cmbModeleQE_.setSelectedIndex(4);
      CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
      CtuluLibSwing.griserPanel(pnNbTraceur_, false);
      break;
  case EnumMetierModeleQualiteDEau._THERMIC:
      cmbModeleQE_.setSelectedIndex(5);
      CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
      CtuluLibSwing.griserPanel(pnNbTraceur_, false);
      break;
  }

  if (paramModeleQE_.nbTraceur() <= 0) {
      paramModeleQE_.nbTraceur(1);
  }
  cmnNbTraceurs_.setSelectedIndex(paramModeleQE_.nbTraceur() - 1);
  ((Hydraulique1dTableauChainesModel) tabNomTraceurs_.getModel()).
          setValeurs(paramModeleQE_.vvNomTracer());

  if (paramModeleQE_.presenceTraceurs()) {
      chkActivation_.setSelected(true);
      if (paramModeleQE_.modeleQualiteEau().value() ==
          EnumMetierModeleQualiteDEau._TRANSPORT_PUR) {
          CtuluLibSwing.griserPanel(pnModele_, true);
          CtuluLibSwing.griserPanel(pnNbTraceur_, true);
          CtuluLibSwing.griserPanel(spNomsTraceurs_, true);
      }
  } else {
      chkActivation_.setSelected(false);
      CtuluLibSwing.griserPanel(pnModele_, false);
      CtuluLibSwing.griserPanel(pnNbTraceur_, false);
      CtuluLibSwing.griserPanel(spNomsTraceurs_, false);
  }
}


/*Fonction supprimant toutes les donn�es dependantes d'un modele de qualit� d'eau*/
private boolean avertissementPerteDonnees() {
    MetierLoiTracer[] loiTracers = donneesHydro_.getLoisTracer();
    boolean bLoisTracer = false;
    boolean bConcInits = false;
    boolean bOptionTraceur = false;
    boolean bParamsPhys = false;
    boolean bParamsMeteo = false;
    String message=Hydraulique1dResource.HYDRAULIQUE1D.getString("Si vous changez le mod�le de qualit� d'eau, les donn�es suivantes associ�es � l'ancien mod�le seront supprim�es")
                   +" :\n";
    if (!((loiTracers==null) || (loiTracers.length==0))) {
        bLoisTracer= true;
        message=message+ "- "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Les lois Tracer")+"\n";
    }
     if (!((paramQE_.concentrationsInitiales()==null) || (paramQE_.concentrationsInitiales().length==0))) {
         bConcInits = true;
         message=message+ "- "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Les concentrations initiales")+"\n";
     }
     if (!((paramQE_.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers()==null) || (paramQE_.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers().length==0))) {
         bOptionTraceur = true;
         message = message + "- "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Les options de diffusion et de convection des traceurs")+"\n";
     }
     if (!((paramQE_.parametresGenerauxQualiteDEau().paramsPhysTracer()==null) || (paramQE_.parametresGenerauxQualiteDEau().paramsPhysTracer().length==0))) {
    bParamsPhys = true;
    message = message + "- "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Les param�tres physiques des traceurs")+"\n";
    }
     if (!((paramQE_.parametresGenerauxQualiteDEau().paramMeteoTracer()==null) || (paramQE_.parametresGenerauxQualiteDEau().paramMeteoTracer().length==0))) {
    	 bParamsMeteo = true;
	    message = message + "- "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Les param�tres m�t�o")+"\n";
	    }

     if (bModeleInitialementVide) return true;
     if (!bLoisTracer && !bConcInits && !bOptionTraceur && !bParamsPhys && !bParamsMeteo) return true;
     message=message+ " "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Souhaitez-vous vraiment changer de mod�le ?");
    int choix = JOptionPane.showConfirmDialog(pnActivationQE_,message,Hydraulique1dResource.HYDRAULIQUE1D.getString("Attention !"),
      JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
    boolean confirm = false;
    if (choix==JOptionPane.YES_OPTION){
        confirm = true;
    }
       return confirm;
}

/**Fonction avertissant de l'incompatibilit� entre Tracer et les casiers*/
private boolean avertissementActivationQE() {
   String message ="";
   boolean confirm = true;
   if (reseau_.casiers()!=null && reseau_.casiers().length!=0) {
     message=message+ " "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Attention la qualit� d'eau n'est pas compatible avec l'utilisation de casiers !")+
             "\n"+Hydraulique1dResource.HYDRAULIQUE1D.getString("Souhaitez-vous malgr�s tout activer le module de qualit� d'eau ?");
    int choix = JOptionPane.showConfirmDialog(pnActivationQE_,message,Hydraulique1dResource.HYDRAULIQUE1D.getString("Attention !"),
      JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
	if (choix==JOptionPane.NO_OPTION){
	    confirm = false;
	}
   }
       return confirm;
}

/**Fonction avertissant de l'inutilit� des sources en l'absence de QE*/
private boolean avertissementDesactivationQE() {
   String message ="";
   boolean confirm = true;
   if (reseau_.sources()!=null && reseau_.sources().length!=0) {
     message+=" "+Hydraulique1dResource.HYDRAULIQUE1D.getString("Les sources pr�sentes dans votre reseau ne sont pas pris en compte en dehors du module de qualit� d'eau !");
     message+="\n";
     message+=Hydraulique1dResource.HYDRAULIQUE1D.getString("Souhaitez-vous malgr�s tout d�sactiver le module de qualit� d'eau ?");
    int choix = JOptionPane.showConfirmDialog(pnActivationQE_,message,Hydraulique1dResource.HYDRAULIQUE1D.getString("Attention !"),
      JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
	if (choix==JOptionPane.NO_OPTION){
	    confirm = false;
	}
   }
       return confirm;
}

/*  private void verifieContraintes() {
    String message="";
    if (paramModeleQE_.regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
      String itemSectionne= (String)cmbModeleQE_.getSelectedItem();
      if (!itemSectionne.equals(TRANSCRITIQUE)) {
        // Passage du torrentiel au fluvial
        if (paramTempo_.pasTempsVariable()) {
          message+="* Passage en pas de temps fixe (Param�tres temporels)";
          paramTempo_.pasTempsVariable(false);
        }
        if (paramModeleQE_.ondeSubmersion()) {
          message+="\n \n* Sortie du calcul d'onde de submersion (Param�tres g�n�raux)";
          paramModeleQE_.ondeSubmersion(false);
        }
        if (paramModeleQE_.traitementImpliciteFrottements()) {
          message+="\n \n* Suppression du traitement implicite des frottements \n(Param�tres g�n�raux avanc�s)";
          paramModeleQE_.traitementImpliciteFrottements(false);
        }
        if (paramModeleQE_.implicitationNoyauTrans()) {
          message+="\n \n* Suppression de l'implicitation du noyau de calcul \ntranscritique (Param�tres g�n�raux avanc�s)";
          paramModeleQE_.implicitationNoyauTrans(false);
        }
        if (!paramModeleQE_.perteChargeAutoElargissement()) {
          message+="\n \n* Activation des pertes de charge automatique en cas \nd'�largissement (Param�tres g�n�raux avanc�s)";
          paramModeleQE_.perteChargeAutoElargissement(true);
        }
      }
    }

    if (!message.equals("")) {
      new BuDialogMessage(
      (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
      ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
         .getInformationsSoftware(),message).activate();
    }
  }*/
}
