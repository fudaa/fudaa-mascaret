/*
 * @file         Hydraulique1dNoeudFluvEditor.java
 * @creation     1999-04-27
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'un noeud du r�seau avec le noyau fluvial (DNoeud).<br>
 * Appeler si l'utilisateur clic sur un noeud (ou confluent) de type "Hydraulique1dReseauNoeud".<br>
 * Lancer dans la classe Hydraulique1dReseauMouseAdapter sans utiliser l'ihm helper.<br>
 * @version      $Revision: 1.10 $ $Date: 2007-11-20 11:42:49 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dNoeudFluvEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  BuTextField tfNumero_;
  BuLabel[] lbExtremites_= new BuLabel[5];
  BuPanel pnNumero_, pnNoeud_, pnExtremite_, pnPerteChargeBtParam_;
  BuGridLayout loExtremite_;
  BuHorizontalLayout loNumero_;
  BorderLayout loNoeud_;
  BuCheckBox btPerteChargeAuto_;
  private MetierNoeud noeud_;
  private MetierParametresGeneraux paramGeneraux_;
  private Hydraulique1dModeleBidiConfluentPanel pnModeleBidi_;

  public Hydraulique1dNoeudFluvEditor() {
    this(null);
  }
  public Hydraulique1dNoeudFluvEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition de noeud fluviale"));
    noeud_= null;
    loExtremite_= new BuGridLayout(5, 5, 5, true, false);
    loNumero_= new BuHorizontalLayout(5, false, false);
    loNoeud_= new BorderLayout();
    Container pnMain_= getContentPane();
    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loNumero_);
    pnNumero_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnExtremite_= new BuPanel();
    pnExtremite_.setLayout(loExtremite_);
    pnExtremite_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnNoeud_= new BuPanel();
    pnNoeud_.setLayout(loNoeud_);
    pnNoeud_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    int textSize= 10;
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(true);
    pnNumero_.add(new BuLabel(getS("N� du Confluent")), 0);
    pnNumero_.add(tfNumero_, 1);
    for (int i= 0; i < lbExtremites_.length; i++) {
      pnExtremite_.add(new BuLabel(getS("Extr�mit�")), i);
    }
    LineBorder lineBorder= new LineBorder(Color.black);
    for (int i= 0; i < lbExtremites_.length; i++) {
      lbExtremites_[i]= new BuLabel("");
      lbExtremites_[i].setBorder(lineBorder);
      pnExtremite_.add(lbExtremites_[i], i + lbExtremites_.length);
    }
    btPerteChargeAuto_ = new BuCheckBox(getS("Pertes de charge automatiques aux confluents"));
    btPerteChargeAuto_.setActionCommand("Perte de charge automatique");
    pnNoeud_.add(pnNumero_, BorderLayout.NORTH);
    pnNoeud_.add(pnExtremite_, BorderLayout.CENTER);
    pnMain_.add(pnNoeud_, BorderLayout.CENTER);
    pnPerteChargeBtParam_ = new BuPanel(new BorderLayout());
    Border bdEtched = BorderFactory.createEtchedBorder();
    pnPerteChargeBtParam_.setBorder(BorderFactory.createTitledBorder(bdEtched,getS("Utilisation des abaques uniquement avec 3 confluents")));
    pnPerteChargeBtParam_.add(btPerteChargeAuto_, BorderLayout.NORTH);
    pnModeleBidi_ = new Hydraulique1dModeleBidiConfluentPanel();
    pnPerteChargeBtParam_.add(pnModeleBidi_, BorderLayout.CENTER);
    pnMain_.add(pnPerteChargeBtParam_, BorderLayout.SOUTH);
    btPerteChargeAuto_.addActionListener(this);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("object", null, noeud_);
      }
      fermer();
    }
    else if("Perte de charge automatique".equals(cmd)) {
      enabledBoutons();
    }
    else {
      super.actionPerformed(_evt);
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierParametresGeneraux) {
      paramGeneraux_ = (MetierParametresGeneraux)_n;
    }
    else if (_n instanceof MetierNoeud) {
      noeud_= (MetierNoeud)_n;
      setValeurs();
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (noeud_ == null)
      return changed;
    int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != noeud_.numero()) {
      noeud_.numero(nnum);
      changed= true;
    }
    boolean perteCh= btPerteChargeAuto_.isSelected();
    if (perteCh != paramGeneraux_.perteChargeConfluents()) {
      paramGeneraux_.perteChargeConfluents(perteCh);
      changed= true;
    }

    if (btPerteChargeAuto_.isEnabled() && perteCh) { // Dans ce cas, il faut tenir compte de la mod�lisation bidimentionnelle
      if (pnModeleBidi_.isChanged()) {
        changed= true;
        noeud_ = pnModeleBidi_.getNoeudMetier();
      }
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    if (noeud_.extremites().length == 3) {
      pnModeleBidi_.setNoeudMetier(noeud_);
    }
    tfNumero_.setValue(new Integer(noeud_.numero()));
    for (int i= 0; i < lbExtremites_.length; i++) {
      if (i < noeud_.extremites().length)
        lbExtremites_[i].setText("" + noeud_.extremites()[i].numero());
      else {
        lbExtremites_[i].setText(" ");
        lbExtremites_[i].setBorder(BorderFactory.createEmptyBorder());
      }
    }
    btPerteChargeAuto_.setSelected(paramGeneraux_.perteChargeConfluents());
    enabledBoutons();
  }

  private void enabledBoutons() {
    if (noeud_.extremites().length != 3) {
      btPerteChargeAuto_.setEnabled(false);
      pnModeleBidi_.setEnabled(false);
    }
    else {
      btPerteChargeAuto_.setEnabled(true);
      if (btPerteChargeAuto_.isSelected()) {
        pnModeleBidi_.setEnabled(true);
      }
      else {
        pnModeleBidi_.setEnabled(false);
      }
    }
  }
}
