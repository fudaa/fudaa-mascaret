/*
 * @file         Hydraulique1dNoeudTransEditor.java
 * @creation     2000-11-23
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'un noeud du r�seau avec le noyau transcritique (DNoeud).<br>
 * Appeler si l'utilisateur clic sur un noeud (ou confluent) de type "Hydraulique1dReseauNoeud".<br>
 * Lancer dans la classe Hydraulique1dReseauMouseAdapter sans utiliser l'ihm helper.<br>
 * @version      $Revision: 1.11 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dNoeudTransEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuTextField tfNumero_;
  private BuPanel pnNumero_, pnNoeud_;
  private BuHorizontalLayout loNumero_;
  private BuBorderLayout loNoeud_;
  private MetierNoeud noeud_;
  private Hydraulique1dModeleBidiConfluentPanel pnModeleBidi_;

  public Hydraulique1dNoeudTransEditor() {
    this(null);
  }
  public Hydraulique1dNoeudTransEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition de noeud transcritique"));
    noeud_= null;
    loNumero_= new BuHorizontalLayout(5, false, false);
    loNoeud_= new BuBorderLayout();
    Container pnMain_= getContentPane();
    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loNumero_);
    pnNumero_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnNoeud_= new BuPanel();
    pnNoeud_.setLayout(loNoeud_);
    pnNoeud_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    int textSize= 6;
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(true);
    pnNumero_.add(new BuLabel(getS("N� du Confluent")), 0);
    pnNumero_.add(tfNumero_, 1);

    pnNoeud_.add(pnNumero_, BorderLayout.NORTH);
    pnModeleBidi_ = new Hydraulique1dModeleBidiConfluentPanel();
    pnNoeud_.add(pnModeleBidi_, BorderLayout.CENTER);
    pnMain_.add(pnNoeud_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("object", null, noeud_);
      }
      fermer();
    }
    else {
      super.actionPerformed(_evt);
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierNoeud))
      return;
    if (_n == noeud_)
      return;
    noeud_= (MetierNoeud)_n;
    setValeurs();
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (noeud_ == null)
      return changed;
    int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != noeud_.numero()) {
      noeud_.numero(nnum);
      changed= true;
    }
    if (pnModeleBidi_.isChanged()) {
      changed= true;
      noeud_ = pnModeleBidi_.getNoeudMetier();
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(noeud_.numero()));
    pnModeleBidi_.setNoeudMetier(noeud_);
  }
}
