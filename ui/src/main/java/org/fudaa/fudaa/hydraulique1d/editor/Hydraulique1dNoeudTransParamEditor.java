/*
 * @file         Hydraulique1dNoeudTransParamEditor.java
 * @creation     2000-11-24
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des param�tres du confluent avec le noyau transcritique (DNoeud).<br>
 * Appeler si l'utilisateur clic sur le bouton "PARAMETRES" de l'�diteur d'un confluent avec le noyau transcritique.<br>
 * Lancer dans la classe Hydraulique1dNoeudTransEditor sans utiliser l'ihm helper.<br>
 * @version      $Revision: 1.10 $ $Date: 2007-11-20 11:42:49 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dNoeudTransParamEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  BuButton btEstim_= new BuButton(getS("Estimation"));
  BuLabel[] lbExtremites_= new BuLabel[3];
  BuTextField[][] tfPoints_= new BuTextField[3][4];
  BuPanel pnNoeud_, pnExtremite_, pnEstim_;
  BuGridLayout loExtremites_;
  BuBorderLayout loNoeud_, loEstim_;
  private MetierNoeud noeud_;
  public Hydraulique1dNoeudTransParamEditor() {
    this(null);
  }
  public Hydraulique1dNoeudTransParamEditor(BDialogContent parent) {
    super(
      parent,
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition des param�tres d'un confluent avec calcul transcritique"));
    noeud_= null;
    loExtremites_= new BuGridLayout(5, 5, 5, true, false);
    loEstim_= new BuBorderLayout(5, 5);
    loNoeud_= new BuBorderLayout();
    Container pnMain_= getContentPane();
    pnExtremite_= new BuPanel();
    pnExtremite_.setLayout(loExtremites_);
    pnExtremite_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnNoeud_= new BuPanel();
    pnNoeud_.setLayout(loNoeud_);
    pnNoeud_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    pnEstim_= new BuPanel();
    pnEstim_.setLayout(loEstim_);
    pnEstim_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    int textSize= 10;
    pnExtremite_.add(new BuLabel(""), 0);
    pnExtremite_.add(new BuLabel(""), 1);
    pnExtremite_.add(new BuLabel("X", SwingConstants.CENTER), 2);
    pnExtremite_.add(new BuLabel("Y", SwingConstants.CENTER), 3);
    pnExtremite_.add(new BuLabel("\u03b1", SwingConstants.CENTER), 4);
    LineBorder lineBorder= new LineBorder(Color.black);
    for (int i= 0; i < lbExtremites_.length; i++) {
      lbExtremites_[i]= new BuLabel("");
      lbExtremites_[i].setBorder(lineBorder);
    }
    for (int i= 0; i < tfPoints_.length; i++) {
      for (int j= 0; j < tfPoints_[i].length; j++) {
        tfPoints_[i][j]= BuTextField.createDoubleField();
        tfPoints_[i][j].setColumns(textSize);
        tfPoints_[i][j].setEditable(true);
      }
    }
    String extremite = Hydraulique1dResource.HYDRAULIQUE1D.getString("Extremit�");
    pnExtremite_.add(new BuLabel(extremite), 5);
    pnExtremite_.add(lbExtremites_[0], 6);
    for (int i= 0; i < 3; i++)
      pnExtremite_.add(tfPoints_[0][i], 7 + i);
    pnExtremite_.add(new BuLabel(extremite), 10);
    pnExtremite_.add(lbExtremites_[1], 11);
    for (int i= 0; i < 3; i++)
      pnExtremite_.add(tfPoints_[1][i], 12 + i);
    pnExtremite_.add(new BuLabel(extremite), 15);
    pnExtremite_.add(lbExtremites_[2], 16);
    for (int i= 0; i < 3; i++)
      pnExtremite_.add(tfPoints_[2][i], 17 + i);
    btEstim_.setActionCommand("ESTIM_PARAMETRES");
    btEstim_.addActionListener(this);
    pnEstim_.add(btEstim_, BuBorderLayout.CENTER);
    pnNoeud_.add(pnExtremite_, BuBorderLayout.CENTER);
    pnNoeud_.add(pnEstim_, BorderLayout.SOUTH);
    pnMain_.add(pnNoeud_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("object", null, noeud_);
      }
      fermer();
    } else if ("ESTIM_PARAMETRES".equals(cmd)) {
      estimParam();
      firePropertyChange("object", null, noeud_);
    } else {
      super.actionPerformed(_evt);
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierNoeud))
      return;
    if (_n == noeud_)
      return;
    noeud_= (MetierNoeud)_n;
    setValeurs();
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (noeud_ == null)
      return changed;
    double val0, val1, val2;
    for (int i= 0; i < lbExtremites_.length; i++) {
      val0= ((Double)tfPoints_[i][0].getValue()).doubleValue();
      if (val0 != noeud_.extremites()[i].pointMilieu().x) {
        noeud_.extremites()[i].pointMilieu().x= val0;
        changed= true;
      }
      val1= ((Double)tfPoints_[i][1].getValue()).doubleValue();
      if (val1 != noeud_.extremites()[i].pointMilieu().y) {
        noeud_.extremites()[i].pointMilieu().y= val1;
        changed= true;
      }
      val2= ((Double)tfPoints_[i][2].getValue()).doubleValue();
      if (val2 != noeud_.extremites()[i].angle()) {
        noeud_.extremites()[i].angle(val2);
        changed= true;
      }
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    for (int i= 0; i < lbExtremites_.length; i++) {
      lbExtremites_[i].setText("  " + noeud_.extremites()[i].numero() + "  ");
      tfPoints_[i][0].setValue(
        new Double(noeud_.extremites()[i].pointMilieu().x));
      tfPoints_[i][1].setValue(
        new Double(noeud_.extremites()[i].pointMilieu().y));
      tfPoints_[i][2].setValue(new Double(noeud_.extremites()[i].angle()));
    }
  }
  // Hydraulique1dNoeudTransParamEditor
  protected void estimParam() {
    MetierExtremite[] extr= noeud_.extremites();
    for (int i= 0; i < extr.length; i++) {
      extr[i].pointMilieu(milieu(extr[i].point1(), extr[i].point2()));
    }
    MetierPoint2D bary=
      barycentre(
        extr[0].pointMilieu(),
        extr[1].pointMilieu(),
        extr[2].pointMilieu());
    for (int i= 0; i < extr.length; i++) {
      MetierPoint2D na=
        normalAbs(
          new MetierPoint2D(
            extr[i].point2().x - extr[i].point1().x,
            extr[i].point2().y - extr[i].point1().y));
      MetierPoint2D nExt= normalExterieur(na, bary, extr[i].pointMilieu());
      extr[i].angle(angleAxeX(nExt));
    }
    setValeurs();
  }
  private MetierPoint2D milieu(MetierPoint2D pt1, MetierPoint2D pt2) {
    return new MetierPoint2D((pt1.x + pt2.x) / 2, (pt1.y + pt2.y) / 2);
  }
  private MetierPoint2D barycentre(MetierPoint2D pt1, MetierPoint2D pt2, MetierPoint2D pt3) {
    return new MetierPoint2D(
      (pt1.x + pt2.x + pt3.x) / 3,
      (pt1.y + pt2.y + pt3.y) / 3);
  }
  private MetierPoint2D normalAbs(MetierPoint2D vec) {
    return new MetierPoint2D(Math.abs(vec.y), Math.abs(vec.x));
  }
  private MetierPoint2D normalExterieur(
    MetierPoint2D normalAbs,
    MetierPoint2D bary,
    MetierPoint2D milieu) {
    MetierPoint2D mg= new MetierPoint2D(bary.x - milieu.x, bary.y - milieu.y);
    MetierPoint2D normalExt= normalAbs;
    if (mg.x > 0)
      normalExt.x= normalExt.x * -1;
    if (mg.y > 0)
      normalExt.y= normalExt.y * -1;
    return normalExt;
  }
  private double angleAxeX(MetierPoint2D normalExt) {
    double angle= Math.toDegrees(Math.atan(normalExt.y / normalExt.x));
    if (normalExt.x == 0) {
      if (normalExt.y == 0) {
        angle= Double.NaN;
      } else if (normalExt.y > 0)
        angle= 90;
      else if (normalExt.y < 0)
        angle= -90;
    } else if (normalExt.x < 0)
      angle= angle + 180;
    return angle;
  }
}
