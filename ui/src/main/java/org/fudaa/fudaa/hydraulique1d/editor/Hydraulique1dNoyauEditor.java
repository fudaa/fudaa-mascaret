/*
 * @file         Hydraulique1dNoyauEditor.java
 * @creation     2000-11-22
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierCritereArret;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresTemporels;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresModeleQualiteEau;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAmont;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCompositionLits;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur de s�lection du noyau de calcul (DParametresGeneraux).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Noyau".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().NOYAU().editer().<br>
 * @version      $Revision: 1.27 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dNoyauEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  final static String TRANSCRITIQUE= Hydraulique1dResource.HYDRAULIQUE1D.getString("Transcritique");
  final static String FLUVIALNP= Hydraulique1dResource.HYDRAULIQUE1D.getString("Fluvial non permanent");
  final static String FLUVIALP= Hydraulique1dResource.HYDRAULIQUE1D.getString("Fluvial permanent");
  final static String PERMANENT= Hydraulique1dResource.HYDRAULIQUE1D.getString("Permanent");
  final static String MASCARET_5_2= "MASCARET 5.2";
  final static String MASCARET_8 = "MASCARET 8  ";
  JComboBox cmbRegime_;
  BuComboBox cmbVersion_;
  BuLabel lbVersion_;
  BuPanel pnVersion_, pnRegime_, pnNoyau_;
  BorderLayout loNoyau_;
  BuHorizontalLayout loVersion_, loRegime_;
  private MetierParametresModeleQualiteEau paramModeleQE_;
  private MetierParametresTemporels paramTempo_;
  private MetierParametresGeneraux param_;
  private MetierReseau reseau_;
  public Hydraulique1dNoyauEditor() {
    this(null);
  }
  public Hydraulique1dNoyauEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Choix du noyau de calcul"));
    paramModeleQE_ = null;
    param_= null;
    reseau_= null;
    loNoyau_= new BorderLayout();
    loVersion_= new BuHorizontalLayout(5, false, false);
    loRegime_= new BuHorizontalLayout(5, false, false);
    Container pnMain_= getContentPane();
    pnVersion_= new BuPanel();
    pnVersion_.setLayout(loVersion_);
    pnVersion_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnRegime_= new BuPanel();
    pnRegime_.setLayout(loRegime_);
    pnRegime_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnNoyau_= new BuPanel();
    pnNoyau_.setLayout(loNoyau_);
    pnNoyau_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    if (CGlobal.AVEC_NOYAU_5P2) {
      cmbVersion_ = new BuComboBox(new String[]{MASCARET_8, MASCARET_5_2});
    }
    else {
      cmbVersion_ = new BuComboBox(new String[]{MASCARET_8});
      cmbVersion_.setEnabled(false);
      lbVersion_ = new BuLabel(MASCARET_8);
      lbVersion_.setBorder(new LineBorder(Color.black));
    }
    pnVersion_.add(new BuLabel(getS("Version du code")), 0);
    if (CGlobal.AVEC_NOYAU_5P2) {
      pnVersion_.add(cmbVersion_, 1);
    }
    else {
      pnVersion_.add(lbVersion_, 1);
    }

    cmbRegime_=
      new JComboBox(new String[] { TRANSCRITIQUE, FLUVIALNP, PERMANENT });
    pnRegime_.add(new BuLabel(getS("Noyau de calcul")), 0);
    pnRegime_.add(cmbRegime_, 1);
    pnNoyau_.add(pnVersion_, BorderLayout.NORTH);
    pnNoyau_.add(pnRegime_, BorderLayout.CENTER);
    pnMain_.add(pnNoyau_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();

    cmbVersion_.addActionListener(this);

  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (_evt.getSource() == cmbVersion_) {
      boolean isAvecItemPermanent = cmbRegime_.getItemAt(2).equals(PERMANENT);
      if (cmbVersion_.getSelectedItem().equals(MASCARET_5_2)) {
        if (isAvecItemPermanent) {
          cmbRegime_.insertItemAt(FLUVIALP,2);
          cmbRegime_.removeItemAt(3);
        }
      }
      else { // Version du code : MASCARET_7_0
        if (!isAvecItemPermanent) {
          cmbRegime_.insertItemAt(PERMANENT,2);
          cmbRegime_.removeItemAt(3);
        }
      }
    }
    else if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("object", null, param_.regime());
      }
      fermer();
    }
    else super.actionPerformed(_evt);
  }
  @Override
  protected boolean getValeurs() {
    verifieContraintes();
    boolean changed= false;

    boolean isMasc5_2 = cmbVersion_.getSelectedItem().equals(MASCARET_5_2);
    if (isMasc5_2 != param_.noyauV5P2()) {
        param_.noyauV5P2(isMasc5_2);
        changed= true;
    }

    if (param_.regime() == null)
      return changed;
    String itemSectionne= (String)cmbRegime_.getSelectedItem();
    if (itemSectionne.equals(TRANSCRITIQUE)) {
      if (param_.regime().value() != EnumMetierRegime._TRANSCRITIQUE) {
        param_.regime(EnumMetierRegime.TRANSCRITIQUE);
        changed= true;
      }
    } else if (itemSectionne.equals(FLUVIALNP)) {
      if (param_.regime().value() != EnumMetierRegime._FLUVIAL_NON_PERMANENT) {
        param_.regime(EnumMetierRegime.FLUVIAL_NON_PERMANENT);
        changed= true;
      }
    } else if (itemSectionne.equals(FLUVIALP)||itemSectionne.equals(PERMANENT)) {
      if (param_.regime().value() != EnumMetierRegime._FLUVIAL_PERMANENT) {
        param_.regime(EnumMetierRegime.FLUVIAL_PERMANENT);
        changed= true;
      }
    }
    return changed;
  }


  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierParametresGeneraux) {
      MetierParametresGeneraux param = (MetierParametresGeneraux)_n;
      if (param == param_)
        return;
      param_ = param;
      setValeurs();
    }
    else if (_n instanceof MetierParametresTemporels) {
      paramTempo_ = (MetierParametresTemporels)_n;
    } else if (_n instanceof MetierReseau) {
      reseau_ = (MetierReseau)_n;
    }else if (_n instanceof MetierParametresModeleQualiteEau) {
    	paramModeleQE_ = (MetierParametresModeleQualiteEau)_n;
    }

  }
  @Override
  protected void setValeurs() {

    if (CGlobal.AVEC_NOYAU_5P2) {
      if (param_.noyauV5P2()) {
        cmbVersion_.setSelectedItem(MASCARET_5_2);
      } else {
        cmbVersion_.setSelectedItem(MASCARET_8);
      }
    }
    else {
      cmbVersion_.setSelectedItem(MASCARET_8);
    }
    switch (param_.regime().value()) {
      case EnumMetierRegime._TRANSCRITIQUE :
        cmbRegime_.setSelectedIndex(0);
        break;
      case EnumMetierRegime._FLUVIAL_NON_PERMANENT :
        cmbRegime_.setSelectedIndex(1);
        break;
      case EnumMetierRegime._FLUVIAL_PERMANENT :
        cmbRegime_.setSelectedIndex(2);
        break;
    }
  }
  private void verifieContraintes() {
    String message="";

    String itemSectionne= (String)cmbRegime_.getSelectedItem();
    if (!param_.noyauV5P2() && cmbVersion_.getSelectedItem().equals(MASCARET_5_2)){
    	if (param_.optimisationNoyauTrans()) {
            message += "\n \n* Mascaret V5P2 : ";
            message +=getS("Suppression de l'optimisation du noyau de calcul");
            message +=" \n";
            message +=getS("transcritique (Param�tres g�n�raux avanc�s)");
            param_.optimisationNoyauTrans(false);
        }
    	if (paramTempo_.critereArret().equals(EnumMetierCritereArret.COTE_MAX)) {
            message += "\n \n* Mascaret V5P2 : ";
            message +=getS("D�sactivation du crit�re d'arret par cote max");
            paramTempo_.critereArret(EnumMetierCritereArret.TEMPS_MAX);
        }
    	if (reseau_.deversoirs().length>5) {
            message += "\n \n* Mascaret V5P2 : ";
            message += getS("Cette version du code ne supporte pas plus de 5 d�versoirs !");
        }
        if (CGlobal.AVEC_QUALITE_DEAU) {
	           if (paramModeleQE_.presenceTraceurs()){
	        	   paramModeleQE_.presenceTraceurs(false);
	                message +="\n \n* ";
                        message +=getS("D�sactivation de la qualit� d'eau")+" \n";
	            }
        }

    }
    if (param_.regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
      if (!itemSectionne.equals(TRANSCRITIQUE)) {
        // Passage du torrentiel au fluvial
        if (paramTempo_.pasTempsVariable()) {
          message+="* "+getS("Passage en pas de temps fixe (Param�tres temporels)");
          paramTempo_.pasTempsVariable(false);
        }
        if (param_.ondeSubmersion()) {
          message+="\n \n* "+getS("Sortie du calcul d'onde de submersion (Param�tres g�n�raux)");
          param_.ondeSubmersion(false);
        }
        if (param_.traitementImpliciteFrottements()) {
          message+="\n \n* "+getS("Suppression du traitement implicite des frottements");
          message+="\n"+getS("(Param�tres g�n�raux avanc�s)");
          param_.traitementImpliciteFrottements(false);
        }
        if (param_.implicitationNoyauTrans()) {
          message+="\n \n* "+getS("Suppression de l'implicitation du noyau de calcul")+" \n"+getS("transcritique (Param�tres g�n�raux avanc�s)");
          param_.implicitationNoyauTrans(false);
        }
        if (param_.optimisationNoyauTrans()) {
            message += "\n \n* "+getS("Suppression de l'optimisation du noyau de calcul")+" \n"+getS("transcritique (Param�tres g�n�raux avanc�s)");
            param_.optimisationNoyauTrans(false);
        }
        if (!param_.perteChargeAutoElargissement()) {
          message+="\n \n* "+getS("Activation des pertes de charge automatique en cas")+" \n"+getS("d'�largissement (Param�tres g�n�raux avanc�s)");
          param_.perteChargeAutoElargissement(true);
        }
      }
    } else {
        if (itemSectionne.equals(TRANSCRITIQUE) ) { // passage en Transcritique
            if (reseau_!=null && reseau_.biefs()!=null && reseau_.biefs().length!=0){
                message +=
                        "\n \n* "+getS("Suppression des zones de stockage dans les profils")+" \n";
            }
/*           if (CGlobal.AVEC_QUALITE_DEAU) {
	           if (paramModeleQE_.presenceTraceurs()){
	        	   paramModeleQE_.presenceTraceurs(false);
	                message +=
	                        "\n \n* "+getS("D�sactivation de la qualit� d'eau")+" \n";
	            }
           }*/
           if (param_.compositionLits().value() == EnumMetierTypeCompositionLits._FOND_BERGE) {
             param_.compositionLits(EnumMetierTypeCompositionLits.MINEUR_MAJEUR);
             message += "\n \n* "+getS("Passage du mod�le de composition des lits au mod�le Debord")+"\n  "+getS("(Param�tres g�n�raux avanc�s)");
           }
            //Verification des types de seuil
            String numSeuilsInterdits = "";
            int nb =0;
            MetierSingularite[] singularites = reseau_.singularites();
            for (int i =0; i <singularites.length ; i++) {
                if ((singularites[i] instanceof MetierSeuil) && !((singularites[i] instanceof MetierSeuilLoi) || (singularites[i] instanceof MetierSeuilTarageAmont))){
                    nb++;
                    if (nb==1) numSeuilsInterdits = ""+singularites[i].numero();
                    if (nb==2) numSeuilsInterdits = singularites[i].numero()+" et " +numSeuilsInterdits;
                    if (nb>2) numSeuilsInterdits = singularites[i].numero() +", "+ numSeuilsInterdits;

                }
            }
            if (nb==1) {
                message += "\n \n* "+getS("Attention le seuil n�") +numSeuilsInterdits+getS(" est incompatible avec le noyau transcritique !");
            } else if (nb>1) {
                message += "\n \n* "+getS("Attention les seuils n�") +numSeuilsInterdits+getS(" sont incompatibles avec le noyau transcritique !");

            }

        }

    }

    if (!message.isEmpty()) {
      new BuDialogMessage(
      (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
      ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
         .getInformationsSoftware(),message).activate();
    }
  }
}
