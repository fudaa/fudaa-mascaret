/*
 * @file         Hydraulique1dParametresCalageEditor.java
 * @creation     2000-12-07
 * @modification $Date: 2007-11-20 11:42:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.EnumMetierMethodeOpt;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.EnumMetierTypeLit;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierParametresCalageAuto;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur des param�tres de calage.<br>
 * Appel� si l'utilisateur clic sur le menu "Calage/Param�tres Calage".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PARAM_CALAGE().editer().<br>
 * @version      $Revision: 1.4 $ $Date: 2007-11-20 11:42:47 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class Hydraulique1dParametresCalageEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {

  private MetierParametresCalageAuto param_;

  private BuGridLayout lyCenter_;
  private BuLabel lbPasGradient_;
  //private BuTextField tfPasGradient_;
  private BuLabel lbNbIter_;
  private BuTextField tfNbIter_;
  private BuLabel lbPrecision_;
  private BuTextField tfPrecision_;
  private BuLabel lbRoInit_;
  //private BuTextField tfRoInit_;
  private BuLabel lbTypeLit_;
  private BuComboBox coTypeLit_;
  private BuLabel lbMethode_;
  //private BuComboBox coMethode_;
  private BuPanel pnCenter_;
  private BuPanel pnPrincipal_;
  private BorderLayout lyPrincipal_;

  public Hydraulique1dParametresCalageEditor() {
    this(null);
  }

  public Hydraulique1dParametresCalageEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition des param�tres de calage"));

    lbPasGradient_=new BuLabel(getS("Pas du gradient"));
    //tfPasGradient_=BuTextField.createDoubleField();
    //tfPasGradient_.setColumns(7);
    lbNbIter_=new BuLabel(getS("Nb it�rations maximum"));
    tfNbIter_=BuTextField.createIntegerField();
    lbPrecision_=new BuLabel(getS("Pr�cision convergence"));
    tfPrecision_=BuTextField.createDoubleField();
    lbRoInit_=new BuLabel(getS("Ro initial"));
    //tfRoInit_=BuTextField.createDoubleField();
    lbTypeLit_=new BuLabel(getS("Choix du lit"));
    coTypeLit_=new BuComboBox(new Object[]{getS("Lit mineur"),getS("Lit majeur"),getS("Lit mineur + Lit majeur")});
    lbMethode_=new BuLabel("M�thode");
    //coMethode_=new BuComboBox(new Object[]{getS("Descente optimale")/*,"Casier Newton","Algorithme g�n�tique"*/});

    lyCenter_=new BuGridLayout(2,5,2);
    pnCenter_=new BuPanel();
    pnCenter_.setBorder(BorderFactory.createEmptyBorder(0,0,5,0)); // Pour des pb d'affichage.
    pnCenter_.setLayout(lyCenter_);
    //pnCenter_.add(lbPasGradient_);
    //pnCenter_.add(tfPasGradient_);
    //pnCenter_.add(lbRoInit_);
    //pnCenter_.add(tfRoInit_);
    pnCenter_.add(lbNbIter_);
    pnCenter_.add(tfNbIter_);
    pnCenter_.add(lbTypeLit_);
    pnCenter_.add(coTypeLit_);
    pnCenter_.add(lbPrecision_);
    pnCenter_.add(tfPrecision_);
    //pnCenter_.add(lbMethode_);
    //pnCenter_.add(coMethode_);

    Border bd=BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
    pnPrincipal_=new BuPanel();
    pnPrincipal_.setBorder(BorderFactory.createTitledBorder(bd,getS("Calage")));
    lyPrincipal_=new BorderLayout(5,0);
    pnPrincipal_.setLayout(lyPrincipal_);
    pnPrincipal_.add(pnCenter_,BorderLayout.CENTER);
    getContentPane().add(pnPrincipal_,BorderLayout.CENTER);

    param_= null;
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      String mes=isValide();
      if (mes!=null) {
        JOptionPane.showMessageDialog(this,
         mes+".",
         getS("Erreur de mise en donn�es"),
          JOptionPane.ERROR_MESSAGE);
      }
      else {
        if (getValeurs()) firePropertyChange("parametres", null, param_);
        fermer();
      }
    }
    else {
      super.actionPerformed(_evt);
    }
  }

  /**
   * Controle que le panneau est correctement rempli.
   * @return Un message indiquant l'origine du pb. null si pas de pb.
   */
  public String isValide() {
   // if (tfPasGradient_.getValue()==null) return getS("Pas du gradient")+" : "+getS("Champ invalide");
   // double pasGradient=((Double)tfPasGradient_.getValue()).doubleValue();
   // if (pasGradient<=0) return getS("Pas du gradient")+" : "+getS("La valeur doit �tre strictement positive");

    if (tfNbIter_.getValue()==null) return getS("Nb it�rations maximum")+" : "+getS("Champ invalide");
    int nbMaxIterations=((Integer)tfNbIter_.getValue()).intValue();
    if (nbMaxIterations<=0) return getS("Nb it�rations maximum")+" : "+getS("La valeur doit �tre strictement positive");

    if (tfPrecision_.getValue()==null) return getS("Pr�cision convergence")+" : "+getS("Champ invalide");
    double precision=((Double)tfPrecision_.getValue()).doubleValue();
    if (precision<=0) return getS("Pr�cision convergence")+" : "+getS("La valeur doit �tre strictement positive");

    /*if (tfRoInit_.getValue()==null) return getS("Ro initial")+" : "+getS("Champ invalide");
    double roInit=((Double)tfRoInit_.getValue()).doubleValue();
    if (roInit<=0) return getS("Ro initial")+" : "+getS("La valeur doit �tre strictement positive");
    */
    return null;
  }

  @Override
  protected boolean getValeurs() {
    boolean changed= false;

   /* double pasGradient=((Double)tfPasGradient_.getValue()).doubleValue();
    if (pasGradient!=param_.pasGradient()) {
      param_.pasGradient(pasGradient);
      changed=true;
    }*/
    int nbMaxIterations=((Integer)tfNbIter_.getValue()).intValue();
    if (nbMaxIterations!=param_.nbMaxIterations()) {
      param_.nbMaxIterations(nbMaxIterations);
      changed=true;
    }
    double precision=((Double)tfPrecision_.getValue()).doubleValue();
    if (precision!=param_.precision()) {
      param_.precision(precision);
      changed=true;
    }
    /*
    double roInit=((Double)tfRoInit_.getValue()).doubleValue();
    if (roInit!=param_.roInit()) {
      param_.roInit(roInit);
      changed=true;
    }
    */
    EnumMetierTypeLit typeLit;
    if (coTypeLit_.getSelectedIndex()==0) typeLit=EnumMetierTypeLit.MINEUR;
    else            
    	if (coTypeLit_.getSelectedIndex()==1) 
    		typeLit=EnumMetierTypeLit.MAJEUR;
    	else
    		typeLit=EnumMetierTypeLit.MINEUR_MAJEUR;
    if (!typeLit.equals(param_.typeLit())) {
    	param_.typeLit(typeLit);
    	changed=true;
    }
    /*EnumMetierMethodeOpt methodeOpt;
    if (coMethode_.getSelectedIndex()==0)      methodeOpt=EnumMetierMethodeOpt.DESCENTE_OPTIMALE;
    else if (coMethode_.getSelectedIndex()==1) methodeOpt=EnumMetierMethodeOpt.CASIER_NEWTON;
    else                                       methodeOpt=EnumMetierMethodeOpt.ALGO_GENETIQUE;
    if (!methodeOpt.equals(param_.methodeOpt())) {
      param_.methodeOpt(methodeOpt);
      changed=true;
    }*/

    return changed;
  }

  @Override
  public void setObject(MetierHydraulique1d _param) {
    if (!(_param instanceof MetierParametresCalageAuto)) return;
    if (_param==param_) return;

    param_= (MetierParametresCalageAuto)_param;
    setValeurs();
  }

  @Override
  protected void setValeurs() {
   // tfPasGradient_.setValue(new Double(param_.pasGradient()));
    tfNbIter_.setValue(new Integer(param_.nbMaxIterations()));
    tfPrecision_.setValue(new Double(param_.precision()));

   // tfRoInit_.setValue(new Double(param_.roInit()));
    if (param_.typeLit().equals(EnumMetierTypeLit.MINEUR))
      coTypeLit_.setSelectedIndex(0);
    else
    	if (param_.typeLit().equals(EnumMetierTypeLit.MAJEUR))
      coTypeLit_.setSelectedIndex(1);
    	else
    		coTypeLit_.setSelectedIndex(2);
    /*if (param_.methodeOpt().equals(EnumMetierMethodeOpt.DESCENTE_OPTIMALE))
      coMethode_.setSelectedIndex(0);
    else if (param_.methodeOpt().equals(EnumMetierMethodeOpt.CASIER_NEWTON))
      coMethode_.setSelectedIndex(1);
    else
      coMethode_.setSelectedIndex(2);
      */
  }
}
