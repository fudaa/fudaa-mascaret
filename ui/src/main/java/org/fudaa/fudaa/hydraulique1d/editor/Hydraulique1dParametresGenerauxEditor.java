/*
 * @file         Hydraulique1dParametresGenerauxEditor.java
 * @creation     2000-12-07
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeFrottement;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.ebli.commun.EbliButtonYesNo;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGenerauxCasier;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_ZonesFrottement;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.MascaretYesNoButton;

/**
 * Editeur des param�tres g�n�raux (DParametresGeneraux).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Param�tres G�n�raux".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PARAM_GENERAUX().editer().<br>
 *
 * @version $Revision: 1.16 $ $Date: 2007-11-20 11:42:45 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dParametresGenerauxEditor
extends Hydraulique1dCustomizerImprimable
implements ActionListener {

	BuPanel pnParamGeneraux_,
	pnOptionsAvances_,
	pnBarragePrincipal_,
	pnParamModelisation_;
	BuPanel pnCalculOnde_,
	pnOndeOui_,
	pnTypeRupture_,
	pnTypeFrot_,
	pnBarrageOndeSub_;
	BuPanel pnBoutonsLaissFrotCasier_,
	pnParamPhysiques_,
	pnParamNumerique_, 
	pnTypeDebordement_,
	pnFrotParois_;
	BuPanel pnPerteCharge_, pnActivationCasier_;
	BuGridLayout loTypeRupture_;
	BuGridLayout layoutPanelPhysique_;
	GridLayout loBoutonsLaissFrotCasier_;
	BuHorizontalLayout loBarragePrincipal_,
	loOndeOui_,
	loTypeFrot_,
	loBarrageOndeSub_;
	BuVerticalLayout loTypeDebordement_,
	loFrotParois_,
	loPerteCharge_,
	loActivationCasier_,
	loAbscAbsolue_,
	loParamGeneraux_,
	loCalculOnde_;
	FlowLayout loOptionsAvances_;
	BuBorderLayout loParamModelisation_;
	BuRadioButton rbOndeNon_, rbOndeOui_, rbProgressive_, rbInstantanee_;
	BuRadioButton rbFrotParoisOui_,
	rbFrotParoisNon_,
	rbPerteChargeOui_,
	rbPerteChargeNon_,
	rbActivationCasierOui_,
	rbActivationCasierNon_;
	
	BuTextField tfAbscisse_, tfCoteEau_;
	BuCheckBox cbLitMajeur_, cbZoneStockage_;
	JComboBox cmbTypeFrot_, cmbCoef_;
	//  public BuButton btLaisseCrue_, btZonesFrot_, btZonesStock_;
	public BuButton btZonesFrot_;//, btCasier_;
	BuButton btOptionsAvances_;
	private MetierParametresGeneraux param_;
	private boolean hasCasiers;

	 private MascaretYesNoButton btTermesNonHydrostatique_,apportDebit_;
	
	public Hydraulique1dParametresGenerauxNumeriquesEditor panelDataNumeriquesAvance;
	
	public Hydraulique1dParametresGenerauxEditor() {
		this(null);
	}
	public Hydraulique1dParametresGenerauxEditor(BDialogContent parent) {
		super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition des param�tres g�n�raux"));
		

		param_ = null;
		loTypeRupture_ = new BuGridLayout(1, 5, 2);
		layoutPanelPhysique_ = new BuGridLayout(2, 2, 5, true, true);
		loBoutonsLaissFrotCasier_ = new GridLayout(1, 3, 5, 5);
		loBarragePrincipal_ = new BuHorizontalLayout(5, true, true);
		loBarrageOndeSub_ = new BuHorizontalLayout(5, true, true);
		loOndeOui_ = new BuHorizontalLayout(1, true, true);
		loTypeFrot_ = new BuHorizontalLayout();
		loTypeDebordement_ = new BuVerticalLayout(1, true, true);
		loFrotParois_ = new BuVerticalLayout(1, true, true);
		loPerteCharge_ = new BuVerticalLayout(1, true, true);
		loActivationCasier_ = new BuVerticalLayout(1, true, true);
		loAbscAbsolue_ = new BuVerticalLayout(1, true, true);

		loCalculOnde_ = new BuVerticalLayout(1, true, true);
		loOptionsAvances_ = new FlowLayout();
		loParamModelisation_ = new BuBorderLayout(5, 5);
		Container pnMain_ = getContentPane();
		pnCalculOnde_ = new BuPanel();
		pnCalculOnde_.setLayout(loCalculOnde_);
		pnCalculOnde_.setBorder(
				new LineChoiceBorder(false, false, false, true, false, false));
		pnFrotParois_ = new BuPanel();
		pnFrotParois_.setLayout(loFrotParois_);
		pnFrotParois_.setBorder(
				new LineChoiceBorder(false, false, false, true, false, false));
		pnTypeFrot_ = new BuPanel();
		pnTypeFrot_.setLayout(loTypeFrot_);
		pnOndeOui_ = new BuPanel();
		pnOndeOui_.setLayout(loOndeOui_);
		pnOptionsAvances_ = new BuPanel();
		pnOptionsAvances_.setLayout(loOptionsAvances_);
		pnBoutonsLaissFrotCasier_ = new BuPanel();
		pnBoutonsLaissFrotCasier_.setLayout(loBoutonsLaissFrotCasier_);
		pnParamPhysiques_ = new BuPanel();
		pnParamNumerique_ = new BuPanel();
		pnParamPhysiques_.setLayout(layoutPanelPhysique_);
		pnParamNumerique_.setLayout(new BuGridLayout(2, 2, 5, true, true));
		pnParamModelisation_ = new BuPanel();
		pnParamModelisation_.setLayout(loParamModelisation_);
		CompoundBorder bdParamModelisation
		= new CompoundBorder(
				new EtchedBorder(),
				new EmptyBorder(new Insets(2, 2, 2, 2)));
		TitledBorder tbParamModelisation
		= new TitledBorder(bdParamModelisation, getS("Param�tres de mod�lisation"));
		pnParamModelisation_.setBorder(tbParamModelisation);
		pnBarrageOndeSub_ = new BuPanel();
		pnBarrageOndeSub_.setLayout(loBarrageOndeSub_);
		pnBarragePrincipal_ = new BuPanel();
		pnBarragePrincipal_.setLayout(loBarragePrincipal_);
		CompoundBorder bdBarragePrincipal
		= new CompoundBorder(
				new EtchedBorder(),
				new EmptyBorder(new Insets(2, 2, 2, 2)));
		TitledBorder tbBarragePrincipal
		= new TitledBorder(bdBarragePrincipal, getS("Barrage Principal"));
		//pnBarragePrincipal_.setBorder(tbBarragePrincipal);
		pnPerteCharge_ = new BuPanel();
		pnPerteCharge_.setLayout(loPerteCharge_);
		pnPerteCharge_.setBorder(
				new LineChoiceBorder(false, false, false, true, false, false));
		pnActivationCasier_ = new BuPanel();
		pnActivationCasier_.setLayout(loActivationCasier_);
		pnActivationCasier_.setBorder(
				new LineChoiceBorder(false, false, false, true, false, false));

		
		pnTypeDebordement_ = new BuPanel();
		pnTypeDebordement_.setLayout(loTypeDebordement_);
		pnTypeDebordement_.setBorder(
				new LineChoiceBorder(false, false, false, true, false, false));
		pnTypeRupture_ = new BuPanel();
		pnTypeRupture_.setLayout(loTypeRupture_);
		pnTypeRupture_.setBorder(
				new LineChoiceBorder(false, false, false, true, false, false));
		pnParamGeneraux_ = new BuPanel(new BorderLayout());
		//pnParamGeneraux_.setLayout(loParamGeneraux_);
		BuTabbedPane tabPane = new BuTabbedPane();
		pnParamGeneraux_.add(tabPane, BorderLayout.CENTER);
		final BuPanel pnParamPhysiquesGeneral = new BuPanel(new BorderLayout()); 
		final BuPanel pnParamNumeriqueGeneral = new BuPanel(new BorderLayout()); 
   
		pnParamPhysiquesGeneral.add(pnParamPhysiques_,BorderLayout.CENTER);
		pnParamNumeriqueGeneral.add(pnParamNumerique_,BorderLayout.CENTER);

		tabPane.addTab(getS("Param�tres physiques"), pnParamPhysiquesGeneral);
		tabPane.addTab(getS("Param�tres num�riques"), pnParamNumeriqueGeneral);

		pnParamGeneraux_.setBorder(
				new CompoundBorder(
						new EtchedBorder(),
						new EmptyBorder(new Insets(5, 5, 5, 5))));
		String[] items = {"Strickler", "Ch�zy", "Colebrook", "Bazin"};
		cmbTypeFrot_ = new JComboBox(items);
		pnTypeFrot_.add(cmbTypeFrot_);
		
		String[] itemsCoeff = {Hydraulique1dResource.HYDRAULIQUE1D.getString("Coefficients de Strickler K"),
				Hydraulique1dResource.HYDRAULIQUE1D.getString("Coefficients de Manning n")};
		cmbCoef_ = new JComboBox(itemsCoeff);
		
		cbLitMajeur_ = new BuCheckBox(getS("Lit majeur"));
		cbZoneStockage_ = new BuCheckBox(getS("Zone de Stockage"));
		int n = 0;
		pnTypeDebordement_.add(cbLitMajeur_, n++);
		pnTypeDebordement_.add(cbZoneStockage_, n++);
		rbFrotParoisOui_ = new BuRadioButton(getS("Oui"));
		rbFrotParoisOui_.setActionCommand("PAROIS_FROT");
		rbFrotParoisOui_.addActionListener(this);
		rbFrotParoisNon_ = new BuRadioButton(getS("Non"));
		rbFrotParoisNon_.setActionCommand("PAS_PAROIS_FROT");
		rbFrotParoisNon_.addActionListener(this);
		n = 0;
		pnFrotParois_.add(rbFrotParoisOui_, n++);
		pnFrotParois_.add(rbFrotParoisNon_, n++);
		rbPerteChargeOui_ = new BuRadioButton(getS("Oui"));
		rbPerteChargeOui_.setActionCommand("PERTE_CHARGE");
		rbPerteChargeOui_.addActionListener(this);
		rbPerteChargeNon_ = new BuRadioButton(getS("Non"));
		rbPerteChargeNon_.setActionCommand("PAS_PERTE_CHARGE");
		rbPerteChargeNon_.addActionListener(this);

		rbActivationCasierOui_ = new BuRadioButton(getS("Oui"));
		rbActivationCasierOui_.setActionCommand("ACTIVATION_CASIER");
		rbActivationCasierOui_.addActionListener(this);
		rbActivationCasierNon_ = new BuRadioButton(getS("Non"));
		rbActivationCasierNon_.setActionCommand("PAS_ACTIVATION_CASIER");
		rbActivationCasierNon_.addActionListener(this);

		n = 0;
		pnPerteCharge_.add(rbPerteChargeOui_, n++);
		pnPerteCharge_.add(rbPerteChargeNon_, n++);
		n = 0;
		pnActivationCasier_.add(rbActivationCasierOui_, n++);
		pnActivationCasier_.add(rbActivationCasierNon_, n++);

		

		//-- contenu du panel physique --//
		n = 0;
		pnParamPhysiques_.add(
				new BuLabel(getS("Type de d�bordement progressif"), SwingConstants.RIGHT),
				n++);
		pnParamPhysiques_.add(pnTypeDebordement_, n++);
		BuLabelMultiLine lbmActivationCasier
		= new BuLabelMultiLine(getS("Activation des casiers"));
		lbmActivationCasier.setHorizontalAlignment(SwingConstants.RIGHT);
		lbmActivationCasier.setVerticalAlignment(SwingConstants.CENTER);
		pnParamPhysiques_.add(lbmActivationCasier, n++);
		pnParamPhysiques_.add(pnActivationCasier_, n++);
		BuLabelMultiLine lbmPert
		= new BuLabelMultiLine(getS("Pertes de charge automatiques") + "\n" + getS("aux confluents ?"));
		lbmPert.setHorizontalAlignment(SwingConstants.RIGHT);
		lbmPert.setVerticalAlignment(SwingConstants.CENTER);
		pnParamPhysiques_.add(lbmPert, n++);
		pnParamPhysiques_.add(pnPerteCharge_, n++);
		
		BuLabelMultiLine label = new BuLabelMultiLine(getS("Calcul d'une onde") + "\n" + getS("de submersion ?"));
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setVerticalAlignment(SwingConstants.CENTER);
		
		pnParamPhysiques_.add(label,n++);
		pnParamPhysiques_.add(pnCalculOnde_, n++);
		
		
		 label = new BuLabelMultiLine(getS("Barrage Principal") + "\n" + getS("type de rupture"));
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setVerticalAlignment(SwingConstants.CENTER);
		pnParamPhysiques_.add(label,n++);
		pnParamPhysiques_.add(pnBarragePrincipal_, n++);

		
		 btTermesNonHydrostatique_ = new MascaretYesNoButton();
		 label = new BuLabelMultiLine(getS("Termes non hydrostatiques")+ "\n"
		            + getS("pour le noyau Transcritique ?"));
		 label.setHorizontalAlignment(SwingConstants.RIGHT);
			label.setVerticalAlignment(SwingConstants.CENTER);
		 pnParamPhysiques_.add(label,n++);
		 pnParamPhysiques_.add(btTermesNonHydrostatique_,n++);
		 
		 
		 apportDebit_ = new MascaretYesNoButton();
		  apportDebit_.setSelected(false);
		  label = new BuLabelMultiLine(getS("Apport de d�bit dans la quantit� de mouvement"));
		  label.setHorizontalAlignment(SwingConstants.RIGHT);
			label.setVerticalAlignment(SwingConstants.CENTER);
		  pnParamPhysiques_.add(label,n++);
			 pnParamPhysiques_.add(apportDebit_,n++);
		//-- voutons --//
		btZonesFrot_ = new BuButton(getS("ZONES DE FROTTEMENT"));
		
		btZonesFrot_.setActionCommand("ZONES_DE_FROTTEMENT");
		btZonesFrot_.setPreferredSize(new Dimension(150, 70));
		btZonesFrot_.setHorizontalAlignment(SwingConstants.CENTER);
		BuPanel panelBtnPhys = new BuPanel(new FlowLayout());
		panelBtnPhys.add(btZonesFrot_);
		
		//pnParamPhysiques_.add(new BuLabelMultiLine());
		//pnParamPhysiques_.add(panelBtnPhys);
		pnParamPhysiquesGeneral.add(panelBtnPhys,BorderLayout.SOUTH);

		
		
		

		//-- contenu du panel num�rique --//
		n = 0;
		rbProgressive_ = new BuRadioButton(getS("progressive"));
		rbProgressive_.setActionCommand("PROGRESSIVE");
		rbProgressive_.addActionListener(this);
		rbInstantanee_ = new BuRadioButton(getS("instantan�e"));
		rbInstantanee_.setActionCommand("INSTANTANEE");
		rbInstantanee_.addActionListener(this);
		tfAbscisse_ = BuTextField.createDoubleField();
		tfAbscisse_.setColumns(7);
		tfAbscisse_.setEditable(true);
		tfCoteEau_ = BuTextField.createDoubleField();
		tfCoteEau_.setColumns(7);
		tfCoteEau_.setEditable(true);
		n = 0;
		pnTypeRupture_.add(rbProgressive_, n++);
		pnTypeRupture_.add(rbInstantanee_, n++);
		
		BuPanel panelOfGrid = new BuPanel(new FlowLayout(FlowLayout.LEFT));
		panelOfGrid.add(new BuLabel(getS("Abscisse")));
		panelOfGrid.add(tfAbscisse_);		
		pnTypeRupture_.add(panelOfGrid, n++);
		
		panelOfGrid = new BuPanel(new FlowLayout(FlowLayout.LEFT));
		panelOfGrid.add(new BuLabel(getS("Cote plan d'eau")));
		panelOfGrid.add(tfCoteEau_);
		
		pnTypeRupture_.add(panelOfGrid, n++);
		rbOndeOui_ = new BuRadioButton(getS("Oui"));
		rbOndeOui_.setActionCommand("ONDE");
		rbOndeOui_.addActionListener(this);
		n = 0;
		pnOndeOui_.add(rbOndeOui_, n++);
		rbOndeNon_ = new BuRadioButton(getS("Non"));
		rbOndeNon_.setActionCommand("PAS_ONDE");
		rbOndeNon_.addActionListener(this);
		n = 0;
		pnCalculOnde_.add(pnOndeOui_, n++);
		pnCalculOnde_.add(rbOndeNon_, n++);
		n = 0;
		//pnBarragePrincipal_.add(new BuLabel(getS("type de rupture")), n++);
		pnBarragePrincipal_.add(pnTypeRupture_, n++);


		activateDataNumeriqueAvance();
		pnParamNumeriqueGeneral.add(panelDataNumeriquesAvance,BorderLayout.SOUTH);
		
		//-- complete le panel de data numeriques avanc�s --//
		 pnMain_.add(pnParamGeneraux_, BorderLayout.CENTER);
		setNavPanel(EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
		//pack();
	}
	
	/**
	 * Active le panel de data numeriques avanc�s.
	 * @author Adrien
	 */
	public void activateDataNumeriqueAvance() {
		if(panelDataNumeriquesAvance == null) {
			panelDataNumeriquesAvance = new Hydraulique1dParametresGenerauxNumeriquesEditor();
			//param_.ondeSubmersion(rbOndeOui_.isSelected());
			rbOndeOui_.addActionListener(panelDataNumeriquesAvance);
			rbOndeNon_.addActionListener(panelDataNumeriquesAvance);
			//panelDataNumeriquesAvance.setObject(param_);
			
		}
	}

	@Override
	public void actionPerformed(ActionEvent _evt) {
		String cmd = _evt.getActionCommand();
		if ("VALIDER".equals(cmd)) {
			if (getValeurs()) {
				firePropertyChange("parametresGeneraux", null, param_);
			}
			fermer();
		} else if ("OPTIONS_AVANCEES".equals(cmd)) {
			panelDataNumeriquesAvance = new Hydraulique1dParametresGenerauxNumeriquesEditor(this);
			param_.ondeSubmersion(rbOndeOui_.isSelected());
			rbOndeOui_.addActionListener(panelDataNumeriquesAvance);
			rbOndeNon_.addActionListener(panelDataNumeriquesAvance);
			panelDataNumeriquesAvance.setObject(param_);
			
		} else if ("ONDE".equals(cmd)) {
			rbOndeOui_.setSelected(true);
			rbOndeNon_.setSelected(false);
		} else if ("PAS_ONDE".equals(cmd)) {
			rbOndeNon_.setSelected(true);
			rbOndeOui_.setSelected(false);
		} else if ("PROGRESSIVE".equals(cmd)) {
			rbProgressive_.setSelected(true);
			rbInstantanee_.setSelected(false);
			tfAbscisse_.setEnabled(false);
			tfCoteEau_.setEnabled(false);
		} else if ("INSTANTANEE".equals(cmd)) {
			rbInstantanee_.setSelected(true);
			rbProgressive_.setSelected(false);
			tfAbscisse_.setEnabled(true);
			tfCoteEau_.setEnabled(true);
		} else if ("PAROIS_FROT".equals(cmd)) {
			rbFrotParoisOui_.setSelected(true);
			rbFrotParoisNon_.setSelected(false);
		} else if ("PAS_PAROIS_FROT".equals(cmd)) {
			rbFrotParoisNon_.setSelected(true);
			rbFrotParoisOui_.setSelected(false);
		} else if ("PERTE_CHARGE".equals(cmd)) {
			rbPerteChargeOui_.setSelected(true);
			rbPerteChargeNon_.setSelected(false);
		} else if ("PAS_PERTE_CHARGE".equals(cmd)) {
			rbPerteChargeNon_.setSelected(true);
			rbPerteChargeOui_.setSelected(false);
		} else if ("ACTIVATION_CASIER".equals(cmd)) {
			rbActivationCasierOui_.setSelected(true);
			rbActivationCasierNon_.setSelected(false);
		} else if ("PAS_ACTIVATION_CASIER".equals(cmd)) {
			rbActivationCasierNon_.setSelected(true);
			rbActivationCasierOui_.setSelected(false);
		} else {
			super.actionPerformed(_evt);
		}
	}

	
	public boolean getValeursFrottement() {
		boolean changed = false;
		boolean frotParois = rbFrotParoisOui_.isSelected();
		if (frotParois != param_.frottementsParois()) {
			param_.frottementsParois(frotParois);
			changed = true;
		}
		EnumMetierTypeFrottement typFrot = EnumMetierTypeFrottement.STRICKLER;
		String item = (String) cmbTypeFrot_.getSelectedItem();
		if ("Strickler".equals(item)) {
			typFrot = EnumMetierTypeFrottement.STRICKLER;
		} else if ("Ch�zy".equals(item)) {
			typFrot = EnumMetierTypeFrottement.CHEZY;
		} else if ("Colebrook".equals(item)) {
			typFrot = EnumMetierTypeFrottement.COLEBROOK;
		} else if ("Bazin".equals(item)) {
			typFrot = EnumMetierTypeFrottement.BAZIN;
		}
		if (typFrot.value() != param_.typeFrottement().value()) {
			param_.typeFrottement(typFrot);
			changed = true;
		}
		
		EnumMetierTypeCoefficient typCoeff = null;
		if(cmbCoef_.getSelectedIndex() == 0)
			typCoeff = EnumMetierTypeCoefficient.STRICKLER;
		else
			typCoeff = EnumMetierTypeCoefficient.MANNING;
		if (typCoeff.value() != param_.typeCoefficient().value()) {
			param_.typeCoefficient(typCoeff);
			changed = true;
		}
		return changed;
	}
	@Override
	protected boolean getValeurs() {
		boolean changed = false;
		boolean litMaj = cbLitMajeur_.isSelected();
		if (litMaj != param_.debordProgressifLitMajeur()) {
			param_.debordProgressifLitMajeur(litMaj);
			changed = true;
		}
		boolean zoneStock = cbZoneStockage_.isSelected();
		if (zoneStock != param_.debordProgressifZoneStockage()) {
			param_.debordProgressifZoneStockage(zoneStock);
			changed = true;
		}
		
		boolean onde = rbOndeOui_.isSelected();
		if (onde != param_.ondeSubmersion()) {
			param_.ondeSubmersion(onde);
			changed = true;
		}
		if (param_.barragePrincipal() == null) {
			param_.creeBarragePrincipal();
		}
		double absc = ((Double) tfAbscisse_.getValue()).doubleValue();
		if (absc != param_.barragePrincipal().site().abscisse()) {
			param_.barragePrincipal().site().abscisse(absc);
			changed = true;
		}
		double zEau = ((Double) tfCoteEau_.getValue()).doubleValue();
		if (zEau != param_.barragePrincipal().cotePlanEau()) {
			param_.barragePrincipal().cotePlanEau(zEau);
			changed = true;
		}
		boolean rupInstan = rbProgressive_.isSelected();
		if (rupInstan != param_.barragePrincipal().ruptureProgressive()) {
			param_.barragePrincipal().ruptureProgressive(rupInstan);
			changed = true;
		}
		boolean perteCh = rbPerteChargeOui_.isSelected();
		if (perteCh != param_.perteChargeConfluents()) {
			param_.perteChargeConfluents(perteCh);
			changed = true;
		}
		
		
		changed = changed  || getValeursFrottement();
		
		if (param_.parametresCasier() == null) {
			param_.parametresCasier(new MetierParametresGenerauxCasier());
			changed = true;
		}
		if (param_.parametresCasier().activation() != rbActivationCasierOui_.isSelected()) {
			param_.parametresCasier().activation(rbActivationCasierOui_.isSelected());
			changed = true;
		}
		
		boolean termesNonHydrostatique = btTermesNonHydrostatique_.isSelected();
	    if (termesNonHydrostatique != param_.termesNonHydrostatiques()) {
	      param_.termesNonHydrostatiques(termesNonHydrostatique);
	      changed = true;
	    }
	    if (param_.apportDebit() != apportDebit_.isSelected()) {
	        changed = true;
	        param_.apportDebit(apportDebit_.isSelected());
	      }
		
		if(panelDataNumeriquesAvance.getValeurs()) {
			changed = true;
		}
		
		return changed;
	}

	@Override
	public void setObject(MetierHydraulique1d _param) {
		if (!(_param instanceof MetierParametresGeneraux)) {
			return;
		}
		if (_param == param_) {
			return;
		}
		param_ = (MetierParametresGeneraux) _param;
		setValeurs();

		
		//-- adrien - propage vers le panel des data num�riques --//
		panelDataNumeriquesAvance.setObject(param_);
		
	}

	@Override
	protected void setValeurs() {
		boolean casierEnable = hasCasiers && EnumMetierRegime.FLUVIAL_NON_PERMANENT.equals(param_.regime());
		rbActivationCasierOui_.setEnabled(casierEnable);
		rbActivationCasierNon_.setEnabled(casierEnable);
		rbActivationCasierOui_.setSelected(false);
		rbActivationCasierNon_.setSelected(true);
		if (param_.parametresCasier() != null && param_.parametresCasier().activation()) {
			rbActivationCasierOui_.setSelected(true);
			rbActivationCasierNon_.setSelected(false);
		}
		cbLitMajeur_.setSelected(param_.debordProgressifLitMajeur());
		cbZoneStockage_.setSelected(param_.debordProgressifZoneStockage());
		if (param_.frottementsParois()) {
			rbFrotParoisOui_.setSelected(true);
			rbFrotParoisNon_.setSelected(false);
		} else {
			rbFrotParoisOui_.setSelected(false);
			rbFrotParoisNon_.setSelected(true);
		}
		if (param_.perteChargeConfluents()) {
			rbPerteChargeOui_.setSelected(true);
			rbPerteChargeNon_.setSelected(false);
		} else {
			rbPerteChargeOui_.setSelected(false);
			rbPerteChargeNon_.setSelected(true);
		}
		
		if (param_.typeFrottement().value() == EnumMetierTypeFrottement._STRICKLER) {
			cmbTypeFrot_.setSelectedIndex(0);
		} else if (param_.typeFrottement().value() == EnumMetierTypeFrottement._CHEZY) {
			cmbTypeFrot_.setSelectedIndex(1);
		} else if (param_.typeFrottement().value() == EnumMetierTypeFrottement._COLEBROOK) {
			cmbTypeFrot_.setSelectedIndex(2);
		} else if (param_.typeFrottement().value() == EnumMetierTypeFrottement._BAZIN) {
			cmbTypeFrot_.setSelectedIndex(3);
		}
		
		if(param_.typeCoefficient().value() == EnumMetierTypeCoefficient._STRICKLER)
			cmbCoef_.setSelectedIndex(0);
		else
			if(param_.typeCoefficient().value() == EnumMetierTypeCoefficient._MANNING)				
				cmbCoef_.setSelectedIndex(1);
		
		if (param_.ondeSubmersion()) {
			rbOndeOui_.setSelected(true);
			rbOndeNon_.setSelected(false);
		} else {
			rbOndeOui_.setSelected(false);
			rbOndeNon_.setSelected(true);
		}
		if (param_.regime().value() != EnumMetierRegime._TRANSCRITIQUE) {
			rbOndeOui_.setEnabled(false);
			rbOndeNon_.setEnabled(false);
			 btTermesNonHydrostatique_.setEnabled(false);
		} else {
			rbOndeOui_.setEnabled(true);
			rbOndeNon_.setEnabled(true);
			 btTermesNonHydrostatique_.setEnabled(true);
		}
		setValeursBarragePrincipal();
		
		btTermesNonHydrostatique_.setSelected(param_.termesNonHydrostatiques());
		apportDebit_.setSelected(param_.apportDebit());
		  
		  
		if(panelDataNumeriquesAvance != null )
			panelDataNumeriquesAvance.setValeurs();
		
	}

	private void setValeursBarragePrincipal() {
		if (param_.barragePrincipal() == null) {
			param_.creeBarragePrincipal();
		}
		tfAbscisse_.setValue(
				new Double(param_.barragePrincipal().site().abscisse()));
		tfCoteEau_.setValue(new Double(param_.barragePrincipal().cotePlanEau()));
		rbProgressive_.setSelected(param_.barragePrincipal().ruptureProgressive());
		rbInstantanee_.setSelected(!param_.barragePrincipal().ruptureProgressive());
		if (param_.regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
			rbProgressive_.setEnabled(true);
			rbInstantanee_.setEnabled(true);
		} else {
			rbProgressive_.setEnabled(false);
			rbInstantanee_.setEnabled(false);
		}
		if (rbInstantanee_.isSelected() && rbInstantanee_.isEnabled()) {
			tfAbscisse_.setEnabled(true);
			tfCoteEau_.setEnabled(true);
		} else {
			tfAbscisse_.setEnabled(false);
			tfCoteEau_.setEnabled(false);
		}
	}

	public void setHasCasier(boolean hasCasiers) {
		this.hasCasiers = hasCasiers;
	}

	BuPanel panelFrottements =null;
	public BuPanel getPanelFrottement() {
		if(panelFrottements == null) {
			panelFrottements =new BuPanel(new BorderLayout());
			
			BuPanel panelInt = new BuPanel(new FlowLayout(FlowLayout.LEFT));
			BuLabelMultiLine lbmTyp = new BuLabelMultiLine(getS("Type de loi de frottement"));
			lbmTyp.setHorizontalAlignment(SwingConstants.RIGHT);
			panelInt.add(lbmTyp);
			panelInt.add(pnTypeFrot_);

			BuLabelMultiLine lbmCons = new BuLabelMultiLine(getS("Conservation du frottement") + "\n" + getS("sur les parois verticales ?"));
			lbmCons.setHorizontalAlignment(SwingConstants.RIGHT);
			lbmCons.setVerticalAlignment(SwingConstants.CENTER);
			panelInt.add(lbmCons);
			panelInt.add(pnFrotParois_);
			
			panelFrottements.add(panelInt,BorderLayout.CENTER);
			panelInt = new BuPanel(new FlowLayout(FlowLayout.LEFT));
			
			lbmTyp = new BuLabelMultiLine(getS("Type de Coefficient"));
			lbmTyp.setHorizontalAlignment(SwingConstants.RIGHT);
			panelInt.add(lbmTyp);
			
			
			panelInt.add(cmbCoef_);
			
			
			panelFrottements.add(panelInt,BorderLayout.SOUTH);
			
			
			TitledBorder bordertitle= new TitledBorder( getS("Param�tres du frottement"));
			panelFrottements.setBorder(bordertitle);
			
		}
		return panelFrottements;
	}



	public void displayFrottementGeneraux(Hydraulique1dIHM_ZonesFrottement ihm) {
		//-- adrien : add frottement panel to the table frottemetn frame --//
		
			ihm.getEdit_().panelCentral_.add(getPanelFrottement(),BorderLayout.NORTH);
			ihm.getEdit_().setParamPanelGeneraux(this);
	}
}
