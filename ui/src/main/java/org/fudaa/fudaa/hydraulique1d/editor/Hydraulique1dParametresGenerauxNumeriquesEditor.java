/*
 * @file         Hydraulique1dParametresGenerauxAvancesEditor.java
 * @creation     2000-12-08
 * @modification $Date: 2007-11-20 11:42:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCompositionLits;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.ebli.commun.EbliButtonYesNo;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.MascaretRadioButton;

/**
 * 
 * 
 * Editeur des param�tres g�n�raux avanc�s (DParametresGeneraux).<br>
 * concerne le panel des param�tres num�riques.
 * Lancer � partir de la classe Hydraulique1dParametresGenerauxEditor.<br>
 *
 * @version $Revision: 1.17 $ $Date: 2007-11-20 11:42:48 $ by $Author: hadouxad $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dParametresGenerauxNumeriquesEditor
        extends BuPanel
        implements ActionListener {

  private MascaretRadioButton btCompoLits_;
  private EbliButtonYesNo btInterpol_, btImplicitFrot_, attenuationConvection_;
  private EbliButtonYesNo btImplicitMascaret_, btOptimisMascaret_, btPerteChargeAutoElarg_;
  private BuTextField tfFroude_, tfSurelevation_;
  private MetierParametresGeneraux param_;
  public BuRadioButton rbAbscAbsolueOui_, rbAbscAbsolueNon_;
  public Hydraulique1dParametresGenerauxNumeriquesEditor() {
    this(null);
  }

  public Hydraulique1dParametresGenerauxNumeriquesEditor(BDialogContent parent) {
    super();
    param_ = null;
    LayoutManager loFroude = new BuHorizontalLayout(5, true, true);
    LayoutManager loSurelevation = new BuHorizontalLayout(5, true, true);
    LayoutManager loParamGenerauxAvances =  new BuGridLayout(1, 2, 5, true, true);//new BuVerticalLayout(10, true, true);
    Container pnMain_ = this;
    BuPanel pnFroude = new BuPanel();
    pnFroude.setLayout(loFroude);
    BuPanel pnSurelevation = new BuPanel();
    pnSurelevation.setLayout(loSurelevation);
    BuPanel pnParamGenerauxAvances = new BuPanel();
    pnParamGenerauxAvances.setLayout(loParamGenerauxAvances);
    pnParamGenerauxAvances.setBorder(
            new CompoundBorder(
                    new EtchedBorder(),
                    new EmptyBorder(new Insets(5, 5, 5, 5))));
    btCompoLits_ = new MascaretRadioButton(getS("Composition des lits"));
    btCompoLits_.setTextButtonTrue(getS("Mod�le Debord"));
    btCompoLits_.setTextButtonFalse(getS("Mod�le fond/berge"));
    btCompoLits_.setAcunEnabled(false);
   

    attenuationConvection_ = new EbliButtonYesNo(getS("Att�nuation de la convection"));
    attenuationConvection_.setSelected(false);

    btInterpol_ = new EbliButtonYesNo(new BuLabelMultiLine(getS("Interpolation lin�aire des") + " \n"
            + getS("coefficients de frottement ?")));
    btImplicitFrot_ = new EbliButtonYesNo(new BuLabelMultiLine(
            getS("Traitement implicite") + "\n" + getS("du frottement ?")));
    btImplicitMascaret_ = new EbliButtonYesNo(new BuLabelMultiLine(
            getS("Implicitation du noyau") + "\n" + getS("Transcritique ?")));
    btOptimisMascaret_ = new EbliButtonYesNo(new BuLabelMultiLine(
            getS("Optimisation du noyau") + "\n" + getS("Transcritique ?")));
    btPerteChargeAutoElarg_ = new EbliButtonYesNo(new BuLabelMultiLine(
            getS("Perte de charge automatique") + "\n" + getS("en cas d'�largissement ?")));

   

    tfFroude_ = BuTextField.createDoubleField();
    tfFroude_.setColumns(7);
    tfFroude_.setEditable(true);
    int n = 0;
    pnFroude.add(
            new BuLabel(getS("Froude limite pour les conditions limites")),
            n++);
    pnFroude.add(tfFroude_, n++);
    tfSurelevation_ = BuTextField.createDoubleField();
    tfSurelevation_.setColumns(7);
    tfSurelevation_.setEditable(true);
    n = 0;
    pnSurelevation.add(new BuLabel(getS("Sur�l�vation rep�rant le front (m)")), n++);
    pnSurelevation.add(tfSurelevation_, n++);
    n = 0;
    pnParamGenerauxAvances.add(btCompoLits_, n++);
    pnParamGenerauxAvances.add(btInterpol_, n++);

    
    //--profil abs absolue --//
	rbAbscAbsolueOui_ = new BuRadioButton(getS("Oui"));
	rbAbscAbsolueOui_.setActionCommand("ABSC_ABSOLUE");
	rbAbscAbsolueOui_.addActionListener(this);
	rbAbscAbsolueNon_ = new BuRadioButton(getS("Non"));
	rbAbscAbsolueNon_.setActionCommand("PAS_ABSC_ABSOLUE");
	rbAbscAbsolueNon_.addActionListener(this);
	
    BuPanel pnBoutons = new BuPanel();
   
	int p = 0;
	BuLabelMultiLine lbmAbscAbsolue	= new BuLabelMultiLine(getS("Profils en abscisse absolue"));
	lbmAbscAbsolue.setHorizontalAlignment(SwingConstants.RIGHT);
	//lbmAbscAbsolue.setVerticalAlignment(SwingConstants.CENTER);
	
    
	BuPanel panelAbs = new BuPanel();
	
	
	 pnBoutons.setLayout(new GridLayout(2, 1));
	    pnBoutons.setBorder(new LineChoiceBorder(false, false, false, true, false, false));
	    pnBoutons.add(rbAbscAbsolueOui_);
	    pnBoutons.add(rbAbscAbsolueNon_);

	    final BuPanel pnLabel = new BuPanel();
	    pnLabel.setLayout(new GridLayout());
	    pnLabel.add(lbmAbscAbsolue);

	    panelAbs.setLayout(new GridLayout(1, 2));
	    panelAbs.add(pnLabel);
	    panelAbs.add(pnBoutons);
	
	
	pnParamGenerauxAvances.add(panelAbs, n++);
    
	
	
    pnParamGenerauxAvances.add(attenuationConvection_, n++);

    pnParamGenerauxAvances.add(btImplicitFrot_, n++);
    pnParamGenerauxAvances.add(btImplicitMascaret_, n++);
    pnParamGenerauxAvances.add(btOptimisMascaret_, n++);
    pnParamGenerauxAvances.add(btPerteChargeAutoElarg_, n++);
    
    pnParamGenerauxAvances.add(pnFroude, n++);
    pnParamGenerauxAvances.add(pnSurelevation, n++);
    pnMain_.add(pnParamGenerauxAvances, BorderLayout.CENTER);
    //setNavPanel(EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    //pack();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd = _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("parametresGeneraux", null, param_);
      }
      
    } else if ("ONDE".equals(cmd)) {
      if (param_.regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
        tfSurelevation_.setEnabled(true);
      }
    }  else if ("ABSC_ABSOLUE".equals(cmd)) {
		rbAbscAbsolueOui_.setSelected(true);
		rbAbscAbsolueNon_.setSelected(false);
	} else if ("PAS_ABSC_ABSOLUE".equals(cmd)) {
		rbAbscAbsolueNon_.setSelected(true);
		rbAbscAbsolueOui_.setSelected(false);
	}
  }

 
  protected boolean getValeurs() {
    boolean changed = false;
    EnumMetierTypeCompositionLits typLit;

   
    if (param_.attenuationConvection() != attenuationConvection_.isSelected()) {
      param_.attenuationConvection(attenuationConvection_.isSelected());
      changed = true;
    }
    if (btCompoLits_.isAucunSelected()) {
      typLit = EnumMetierTypeCompositionLits.AUCUN;
    } else if (btCompoLits_.isSelected()) {
      typLit = EnumMetierTypeCompositionLits.MINEUR_MAJEUR;
    } else {
      typLit = EnumMetierTypeCompositionLits.FOND_BERGE;
    }
    if (typLit.value() != param_.compositionLits().value()) {
      param_.compositionLits(typLit);
      changed = true;
    }

    boolean interpol = btInterpol_.isSelected();
    if (interpol != param_.interpolationLineaireCoefFrottement()) {
      param_.interpolationLineaireCoefFrottement(interpol);
      changed = true;
    }
    boolean implicitFrot = btImplicitFrot_.isSelected();
    if (implicitFrot != param_.traitementImpliciteFrottements()) {
      param_.traitementImpliciteFrottements(implicitFrot);
      changed = true;
    }
    boolean implicitMascaret = btImplicitMascaret_.isSelected();
    if (implicitMascaret != param_.implicitationNoyauTrans()) {
      param_.implicitationNoyauTrans(implicitMascaret);
      changed = true;
    }
    boolean optimisMascaret = btOptimisMascaret_.isSelected();
    if (optimisMascaret != param_.optimisationNoyauTrans()) {
      param_.optimisationNoyauTrans(optimisMascaret);
      changed = true;
    }

    boolean perteChargeAutoElarg = btPerteChargeAutoElarg_.isSelected();
    if (perteChargeAutoElarg != param_.perteChargeAutoElargissement()) {
      param_.perteChargeAutoElargissement(perteChargeAutoElarg);
      changed = true;
    }
    
    double froude = ((Double) tfFroude_.getValue()).doubleValue();
    if (froude != param_.froudeLimConditionLimite()) {
      param_.froudeLimConditionLimite(froude);
      changed = true;
    }
    double surElev = ((Double) tfSurelevation_.getValue()).doubleValue();
    if (surElev != param_.elevationCoteArriveeFront()) {
      param_.elevationCoteArriveeFront(surElev);
      changed = true;
    }
    
    boolean abscAbsolue = rbAbscAbsolueOui_.isSelected();
	if (abscAbsolue != param_.profilsAbscAbsolu()) {
		param_.profilsAbscAbsolu(abscAbsolue);
		changed = true;
	}
    
    return changed;
  }

 
  public void setObject(MetierHydraulique1d _param) {
    if (!(_param instanceof MetierParametresGeneraux)) {
      return;
    }
    if (_param == param_) {
      return;
    }
    param_ = (MetierParametresGeneraux) _param;
    setValeurs();
  }

  
  protected void setValeurs() {

	if(param_ == null)
		return;
	  
   
    attenuationConvection_.setEnabled(EnumMetierRegime.FLUVIAL_NON_PERMANENT.equals(param_.regime()));
    attenuationConvection_.setSelected(param_.attenuationConvection());
    if (param_.compositionLits().value() == EnumMetierTypeCompositionLits._AUCUN) {
      btCompoLits_.setAucunSelected();
    } else {
      btCompoLits_.setSelected(
              param_.compositionLits().value() == EnumMetierTypeCompositionLits._MINEUR_MAJEUR);
    }
    btInterpol_.setSelected(param_.interpolationLineaireCoefFrottement());
    btImplicitFrot_.setSelected(param_.traitementImpliciteFrottements());
    btImplicitMascaret_.setSelected(param_.implicitationNoyauTrans());
    btOptimisMascaret_.setSelected(param_.optimisationNoyauTrans());
    btPerteChargeAutoElarg_.setSelected(param_.perteChargeAutoElargissement());
  
    tfFroude_.setValue(new Double(param_.froudeLimConditionLimite()));
    tfSurelevation_.setValue(new Double(param_.elevationCoteArriveeFront()));
    
    if (param_.profilsAbscAbsolu()) {
		rbAbscAbsolueOui_.setSelected(true);
		rbAbscAbsolueNon_.setSelected(false);
	} else {
		rbAbscAbsolueOui_.setSelected(false);
		rbAbscAbsolueNon_.setSelected(true);
	}
    
    
    if (param_.regime().value() != EnumMetierRegime._TRANSCRITIQUE) {
      btImplicitFrot_.setEnabled(false);
      btImplicitMascaret_.setEnabled(false);
      btOptimisMascaret_.setEnabled(false);
      btPerteChargeAutoElarg_.setEnabled(false);
      
      btCompoLits_.setEnabled(true);
      btCompoLits_.setAcunEnabled(true);
      tfFroude_.setEnabled(false);
      tfSurelevation_.setEnabled(false);
    } else { // regime transcritique
      btCompoLits_.setEnabled(false);
     
      if (param_.ondeSubmersion()) {
        tfSurelevation_.setEnabled(true);
      } else {
        tfSurelevation_.setEnabled(false);
      }
      if (param_.noyauV5P2()) {
        btOptimisMascaret_.setEnabled(false);
      } else {
        btOptimisMascaret_.setEnabled(true);
      }
    }

  }
  protected String getS(final String _s) {
	    return this.getS(_s,new Object[0]);
	  }
  
  protected String getS(final String _s, Object... _vals) {
	    return Hydraulique1dResource.getS(_s, _vals);
	  }
}
