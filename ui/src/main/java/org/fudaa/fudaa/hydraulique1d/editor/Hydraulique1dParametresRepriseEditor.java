/*
 * @file         Hydraulique1dParametresRepriseEditor.java
 * @creation     2000-12-14
 * @modification $Date: 2007-11-20 11:42:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;

import javax.swing.JTextArea;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresReprise;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsGeneraux;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur du fichier de reprise en lecture (DParametresReprise).<br>
 * Appeler si l'utilisateur clic sur le bouton "FICHIER REPRISE" de l'�diteur des conditions initiales.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PARAM_REPRISE().editer().<br>
 * @version      $Revision: 1.16 $ $Date: 2007-11-20 11:42:43 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dParametresRepriseEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuPanel pnParamReprise_, pnTempsFichier_, pnFichier_, pnTemps_;
  private BuBorderLayout loFichier_, loParamReprise_;
  private BuHorizontalLayout loTemps_;
  private BuVerticalLayout loTempsFichier_;
  private BuScrollPane spContenu_;
  private JTextArea taContenu_;
  private BuTextField tfFichier_, tfTemps_;
  private BuButton btRepriseResultat_;
  private MetierParametresReprise param_;
  private MetierLigneEauInitiale ligneEau_;
  private MetierResultatsGeneraux resultatsGeneraux_;

  /**
   * Constructeur par d�faut.
   * Utilise l'autre constructeur avec comme param�tre null.
   */
  public Hydraulique1dParametresRepriseEditor() {
    this(null);
  }
  /**
   * Constructeur habituel pr�cisant la fen�tre parente.
   * @param parent La fen�tre BDialogContent parente.
   */
  public Hydraulique1dParametresRepriseEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier de reprise en lecture"));
    param_= null;
    loParamReprise_= new BuBorderLayout();
    loFichier_= new BuBorderLayout();
    loTemps_= new BuHorizontalLayout(5, false, false);
    loTempsFichier_= new BuVerticalLayout(5, true, true);
    Container pnMain_= getContentPane();
    pnTemps_= new BuPanel();
    pnTemps_.setLayout(loTemps_);
    pnFichier_= new BuPanel();
    pnFichier_.setLayout(loFichier_);
    pnTempsFichier_= new BuPanel();
    pnTempsFichier_.setLayout(loTempsFichier_);
    pnParamReprise_= new BuPanel();
    pnParamReprise_.setLayout(loParamReprise_);
    pnParamReprise_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    taContenu_= new JTextArea(10, 40);
    taContenu_.setEditable(false);
    spContenu_= new BuScrollPane(taContenu_);
    spContenu_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    tfTemps_= BuTextField.createDoubleField();
    tfTemps_.setColumns(8);
    tfTemps_.setEditable(false);
    int n= 0;
    pnTemps_.add(new BuLabel(getS("temps final")+" : "), n++);
    pnTemps_.add(tfTemps_, n++);
    tfFichier_= BuTextField.createFileField();
    tfFichier_.setEditable(false);
    n= 0;
    pnFichier_.add(new BuLabel(getS("fichier")+" : "), BorderLayout.WEST);
    pnFichier_.add(tfFichier_, BorderLayout.CENTER);
    n= 0;
    pnTempsFichier_.add(pnTemps_, n++);
    pnTempsFichier_.add(pnFichier_, n++);
    pnParamReprise_.add(spContenu_, BorderLayout.CENTER);
    pnParamReprise_.add(pnTempsFichier_, BorderLayout.SOUTH);
    pnMain_.add(pnParamReprise_, BorderLayout.CENTER);
    setActionPanel(EbliPreferences.DIALOG.IMPORTER);
    btRepriseResultat_ = (BuButton)addAction(getS("REPRISE DERNIER RESULTAT"),"REPRISE_RESULTAT");
    btRepriseResultat_.setToolTipText(getS("R�cup�re le fichier de reprise du calcul pr�c�dent"));
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }

  /**
   * Implementation de ActionListener.
   * @param _evt L'�v�ment ayant comme commande VALIDER, IMPORTER et REPRISE_RESULTAT.
   */
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("parametresReprise", null, param_);
      }
      fermer();
    } else if ("IMPORTER".equals(cmd)) {
      importer();
    } else if ("REPRISE_RESULTAT".equals(cmd)) {
      repriseResultat();
    } else {
      super.actionPerformed(_evt);
    }

  }

  /**
   * Transfert les donn�es de l'�diteur vers l'objet m�tier.
   * Implementation de la m�thode abstraite de Hydraulique1dCustomizer.
   * @return VRAI s'il y a au moins une modification par rapport � l'objet m�tier.
   */
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    String nomFile= tfFichier_.getText();
    if (!nomFile.equals(param_.fichier())) {
      param_.fichier(nomFile);
      changed= true;
    }
    double tfinal= ((Double)tfTemps_.getValue()).doubleValue();
    if (tfinal != param_.tFinal()) {
      param_.tFinal(tfinal);
      changed= true;
    }
    byte[] contenu= taContenu_.getText().getBytes();
    if (!Arrays.equals(contenu, param_.contenu())) {
      param_.contenu(contenu);
      changed= true;
    }
    return changed;
  }


  /**
   * Transfert les objets m�tiers n�cessaire vers l'�diteur.
   * Implementation de la m�thode abstraite de Hydraulique1dCustomizer.
   * @param _n Les objets metiers n�cessaires � l'�diteur sont :
   * <br> - MetierLigneEauInitiale
   * <br> - DResultatsGeneraux
   * <br> - en dernier DParametresReprise
   */
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierParametresReprise) {
      MetierParametresReprise param= (MetierParametresReprise)_n;
      if (param == param_)
        return;
      param_= param;
      if (param_ != null)
        setValeurs();
    } else if (_n instanceof MetierLigneEauInitiale) {
      MetierLigneEauInitiale ligneEau= (MetierLigneEauInitiale)_n;
      if (ligneEau == ligneEau_)
        return;
      ligneEau_= ligneEau;
    } else if (_n instanceof MetierResultatsGeneraux) {
      resultatsGeneraux_= (MetierResultatsGeneraux)_n;
    }

  }

  /**
   * Transfert les donn�es de l'objet m�tier vers les composants graphique de l'�diteur.
   * Implementation de la m�thode abstraite de Hydraulique1dCustomizer.
   */
  @Override
  protected void setValeurs() {
    if (param_.fichier() != null) {
      tfFichier_.setValue(new File(param_.fichier()));
    }
    tfTemps_.setValue(new Double(param_.tFinal()));
    if (param_.contenu() != null) {
      taContenu_.setText(new String(param_.contenu()));
    }
    if (resultatsGeneraux_ == null)
      btRepriseResultat_.setEnabled(false);
    else if (resultatsGeneraux_.resultatReprise() == null)
      btRepriseResultat_.setEnabled(false);
    else if (resultatsGeneraux_.resultatReprise().contenu().length == 0)
      btRepriseResultat_.setEnabled(false);
    else
      btRepriseResultat_.setEnabled(true);
  }

  /**
   * Importe un fichier de reprise.
   */
  protected void importer() {
    File fichier= Hydraulique1dImport.chooseFile("rep", getS("Fichier de reprise"));
    if (fichier == null)
      return;
//      DParametresReprise paramLu=
//        ConvMasc_H1D.convertirResultatsRep(
//          DParametresMascaret.litParametresREP(fichier, fichier.length()));
      double[] conteneurTpsFinal = new double[1];
      byte[] contenu = Hydraulique1dImport.importFichierReprise(fichier, conteneurTpsFinal);
      if (contenu != null) {
        tfTemps_.setValue(new Double(conteneurTpsFinal[0]));
        taContenu_.setText(new String(contenu));
        tfFichier_.setValue(fichier);
      }
  }
  /**
   * R�cup�ration du fichier de reprise � partir du r�sultat courant.
   */
  protected void repriseResultat() {
    if (resultatsGeneraux_ == null) return;
    if (resultatsGeneraux_.resultatReprise() == null) return;
    MetierParametresReprise paramReprise = resultatsGeneraux_.resultatReprise();
    tfTemps_.setValue(new Double(paramReprise.tFinal()));
    tfFichier_.setText(getS("r�sultat_pr�c�dent"));
    taContenu_.setText(new String(paramReprise.contenu()));
  }

}
