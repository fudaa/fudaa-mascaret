/*
 * @file         Hydraulique1dParametresResultatsEditor.java
 * @creation     2000-12-04
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierOptionStockage;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresResultats;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresTailleMaxFichier;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.CGlobal;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des param�tres r�sultats (DParametresResultats).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Param�tres R�sultats".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PARAM_RESULTAT().editer().<br>
 * @version      $Revision: 1.19 $ $Date: 2007-11-20 11:42:49 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dParametresResultatsEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  public BuButton btAutresVariables_,btLaisseCrue_, btEditionSites_;
  private BuPanel pnParametres_, pnTitre_, pnTemps_, pnOptionsListing_, pnEnregResu_;
  private BuPanel pnOptionsStockage_, pnCertainsPoints_, pnStockage_, pnPostprocesseur_;
  private BuPanel pnLigCertainsPoints_, pnVarEnregistres_;
  private BuPanel pnTempsListing_,
    pnTailleMaxFichier_,
    pnPostprocesseurTailleMaxFichier_;
  private BuPanel pnBtAutresVariables_, pnVar_;
  private BuVerticalLayout loParametres_, loStockage_, loOptionsListing_;
  private BuVerticalLayout loOptionsStockage_, loPostprocesseur_;
  private BuHorizontalLayout loTitre_, loEnregResu_, loCertainsPoints_;
  private BuHorizontalLayout loVarEnregistres_,
    loTempsListing_,
    loPostprocesseurTailleMaxFichier_;
  private BuGridLayout loTemps_, loVar_, loTailleMaxFichier_;
  private BuBorderLayout loBtAutresVariables_;
  private BuTextField tfTitre_,
    tfPremierPasTemps_,
    tfPeriodeResultat_,
    tfPeriodeListing_;
  private BuTextField tfDistanceEntreBief_;
  private BuTextField tfMaxListingCode_;
//  private BuTextField tfMaxListingDamocles_;
  private BuTextField tfMaxListingCalage_;
  private BuTextField tfMaxResultatRubens_;
  private BuTextField tfMaxResultatOpthyca_;
  private BuTextField tfMaxResultatReprise_;
  private BuCheckBox cbGeometrie_,
    cbPlanimetrage_,
    cbReseau_,
    cbLoiHydraulique_,
    cbLigneEauInitiale_;
  private BuRadioButton rbOpthyca_, rbRubens_;
  private BuCheckBox cbCalcul_, cbHauteurEau_, cbVitesselitMin_;
  private BuCheckBox cbCoteFond_, cbCoteEau_, cbDebitMineur_, cbDebitMajeur_;
  private BuRadioButton rbSectionsCalcul_, rbCertainsPoints_;

  private MetierParametresResultats param_;
  private MetierEtude1d etude_;
  public Hydraulique1dParametresResultatsEditor() {
    this(null);
  }
  public Hydraulique1dParametresResultatsEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition des param�tres r�sultats"));
    param_= null;
    loParametres_= new BuVerticalLayout(5, false, false);
    loStockage_= new BuVerticalLayout(5, false, false);
    loOptionsListing_= new BuVerticalLayout(2, false, false);
    loOptionsStockage_= new BuVerticalLayout(5, false, false);
    loPostprocesseur_= new BuVerticalLayout(5, false, false);
    loTitre_= new BuHorizontalLayout(5, false, false);
    loEnregResu_= new BuHorizontalLayout(5, false, false);
    loCertainsPoints_= new BuHorizontalLayout(2, true, true);
    loVarEnregistres_= new BuHorizontalLayout(2, true, true);
    loTempsListing_= new BuHorizontalLayout(5, false, false);
    loPostprocesseurTailleMaxFichier_= new BuHorizontalLayout(5, true, true);
    loBtAutresVariables_= new BuBorderLayout(5, 5);
    loTemps_= new BuGridLayout(2, 3, 5, false, false);
    loVar_= new BuGridLayout(3, 3, 3, true, true);
    loTailleMaxFichier_= new BuGridLayout(4, 5, 5, true, false);
    Container pnMain_= getContentPane();
    pnParametres_= new BuPanel();
    pnParametres_.setLayout(loParametres_);
    pnParametres_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    pnTitre_= new BuPanel();
    pnTitre_.setLayout(loTitre_);
    pnTemps_= new BuPanel();
    pnTemps_.setLayout(loTemps_);
    pnTemps_.setBorder(new EmptyBorder(new Insets(10, 5, 10, 5)));
    pnOptionsListing_= new BuPanel();
    pnOptionsListing_.setLayout(loOptionsListing_);
    CompoundBorder bdOptionsListing=
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbListing=
      new TitledBorder(
        bdOptionsListing,
        getS("Ecriture et options pour le fichier listing"));
    pnOptionsListing_.setBorder(tbListing);
    pnEnregResu_= new BuPanel();
    pnEnregResu_.setLayout(loEnregResu_);
    pnOptionsStockage_= new BuPanel();
    pnOptionsStockage_.setLayout(loOptionsStockage_);
    pnOptionsStockage_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnCertainsPoints_= new BuPanel();
    pnCertainsPoints_.setLayout(loCertainsPoints_);
    pnStockage_= new BuPanel();
    pnStockage_.setLayout(loStockage_);
    CompoundBorder bdStockage=
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbStockage= new TitledBorder(bdStockage, getS("Stockage"));
    pnStockage_.setBorder(tbStockage);
    pnPostprocesseur_= new BuPanel();
    pnPostprocesseur_.setLayout(loPostprocesseur_);
    CompoundBorder bdPostprocesseur=
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbPostprocesseur=
      new TitledBorder(bdPostprocesseur, getS("Postprocesseur"));
    pnPostprocesseur_.setBorder(tbPostprocesseur);
    pnTailleMaxFichier_= new BuPanel();
    pnTailleMaxFichier_.setLayout(loTailleMaxFichier_);
    CompoundBorder bdTailleMaxFichier=
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbTailleMaxFichier=
      new TitledBorder(
        bdTailleMaxFichier,
        getS("Taille max des fichiers lus par Fudaa-Mascaret"));
    pnTailleMaxFichier_.setBorder(tbTailleMaxFichier);
    pnPostprocesseurTailleMaxFichier_= new BuPanel();
    pnPostprocesseurTailleMaxFichier_.setLayout(
      loPostprocesseurTailleMaxFichier_);
    pnLigCertainsPoints_= new BuPanel();
    pnLigCertainsPoints_.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    //    pnLigCertainsPoints_.setPreferredSize(dim);
    pnVarEnregistres_= new BuPanel();
    pnVarEnregistres_.setLayout(loVarEnregistres_);
    CompoundBorder bdVarEnregistres=
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbVar=
      new TitledBorder(bdVarEnregistres, getS("Variables enregistr�es"));
    pnVarEnregistres_.setBorder(tbVar);
    pnTempsListing_= new BuPanel();
    pnTempsListing_.setLayout(loTempsListing_);
    pnVar_= new BuPanel();
    pnVar_.setLayout(loVar_);
    pnBtAutresVariables_= new BuPanel();
    pnBtAutresVariables_.setLayout(loBtAutresVariables_);
    int textSize= 15;
    tfTitre_= new BuTextField();
    tfTitre_.setColumns(textSize);
    tfTitre_.setEditable(true);
    pnTitre_.add(new BuLabel(getS("Titre du calcul")), 0);
    pnTitre_.add(tfTitre_, 1);
    textSize= 5;
    tfPremierPasTemps_= BuTextField.createIntegerField();
    tfPremierPasTemps_.setColumns(textSize);
    tfPremierPasTemps_.setEditable(true);
    tfPeriodeResultat_= BuTextField.createIntegerField();
    tfPeriodeResultat_.setColumns(textSize);
    tfPeriodeResultat_.setEditable(true);
    tfPeriodeListing_= BuTextField.createIntegerField();
    tfPeriodeListing_.setColumns(textSize);
    tfPeriodeListing_.setEditable(true);
    tfDistanceEntreBief_= BuTextField.createDoubleField();
    tfDistanceEntreBief_.setColumns(textSize);
    tfDistanceEntreBief_.setEditable(true);
    int n= 0;
    pnTemps_.add(
      new BuLabelMultiLine(getS("N� du pas de temps origine pour les")+"\n "+getS("enregistrements")),
      n++);
    pnTemps_.add(tfPremierPasTemps_, n++);
    pnTemps_.add(
      new BuLabelMultiLine(getS("Fr�quence d'enregistrement des")+"\n"+getS("r�sultats (en nombre de pas de temps)")),
      n++);
    pnTemps_.add(tfPeriodeResultat_, n++);
    pnTemps_.add(
      new BuLabelMultiLine(getS("Fr�quence d'�criture dans le fichier")+"\n"+getS("listing (en nombre de pas de temps)")),
      n++);
    pnTemps_.add(tfPeriodeListing_, n++);
    pnTemps_.add(new BuLabelMultiLine(getS("Distance entre les biefs (m)")), n++);
    pnTemps_.add(tfDistanceEntreBief_, n++);
    cbGeometrie_= new BuCheckBox(getS("G�om�trie"));
    cbPlanimetrage_= new BuCheckBox(getS("Planim�trage"));
    cbReseau_= new BuCheckBox(getS("R�seau"));
    cbLoiHydraulique_= new BuCheckBox(getS("Lois hydrauliques utilis�es"));
    cbLigneEauInitiale_= new BuCheckBox(getS("Ligne d'eau initiale"));
    cbCalcul_=
      new BuCheckBox(getS("Ecriture du listing au fur et � mesure du calcul"));
    n= 0;
    pnOptionsListing_.add(cbGeometrie_, n++);
    pnOptionsListing_.add(cbPlanimetrage_, n++);
    pnOptionsListing_.add(cbReseau_, n++);
    pnOptionsListing_.add(cbLoiHydraulique_, n++);
    pnOptionsListing_.add(cbCalcul_, n++);
    pnTempsListing_.add(pnTemps_, 0);
    pnTempsListing_.add(pnOptionsListing_, 1);
    rbCertainsPoints_= new BuRadioButton(getS("En certains points d�finis ci-apr�s"));
    rbCertainsPoints_.setActionCommand("CERTAINS_POINTS");
    rbCertainsPoints_.addActionListener(this);
    btEditionSites_= new BuButton(getS("Edition Points"));
    btEditionSites_.setActionCommand("EDITION_SITE");
    btEditionSites_.addActionListener(this);
    n= 0;
    pnCertainsPoints_.add(rbCertainsPoints_, n++);
    pnCertainsPoints_.add(pnLigCertainsPoints_, n++);
    pnCertainsPoints_.add(btEditionSites_, n++);
    rbSectionsCalcul_=
      new BuRadioButton(getS("Au niveau de toutes les sections de calcul"));
    rbSectionsCalcul_.setActionCommand("SECTIONS_CALCUL");
    rbSectionsCalcul_.addActionListener(this);
    n= 0;
    pnOptionsStockage_.add(rbSectionsCalcul_, n++);
    pnOptionsStockage_.add(pnCertainsPoints_, n++);
    n= 0;
    pnEnregResu_.add(new BuLabel(getS("Enregistrement des r�sultats")), n++);
    pnEnregResu_.add(pnOptionsStockage_, n++);
    n= 0;
    pnStockage_.add(pnTempsListing_, n++);
    pnStockage_.add(pnEnregResu_, n++);
    rbRubens_= new BuRadioButton("Rubens");
    rbOpthyca_= new BuRadioButton("Opthyca");
    ButtonGroup grpRubensOpthyca = new ButtonGroup();
    grpRubensOpthyca.add(rbRubens_);
    grpRubensOpthyca.add(rbOpthyca_);

    n= 0;
    pnPostprocesseur_.add(rbRubens_, n++);
    pnPostprocesseur_.add(rbOpthyca_, n++);
    pnPostprocesseur_.setPreferredSize(new Dimension(120,82));

    textSize= 5;
    tfMaxListingCode_= BuTextField.createDoubleField();
    tfMaxListingCode_.setColumns(textSize);
    tfMaxListingCode_.setEditable(true);
//    tfMaxListingDamocles_= BuTextField.createDoubleField();
//    tfMaxListingDamocles_.setColumns(textSize);
//    tfMaxListingDamocles_.setEditable(true);
    tfMaxListingCalage_= BuTextField.createDoubleField();
    tfMaxListingCalage_.setColumns(textSize);
    tfMaxListingCalage_.setEditable(true);
    tfMaxResultatOpthyca_= BuTextField.createDoubleField();
    tfMaxResultatOpthyca_.setColumns(textSize);
    tfMaxResultatOpthyca_.setEditable(true);
    tfMaxResultatRubens_= BuTextField.createDoubleField();
    tfMaxResultatRubens_.setColumns(textSize);
    tfMaxResultatRubens_.setEditable(true);
    tfMaxResultatReprise_= BuTextField.createDoubleField();
    tfMaxResultatReprise_.setColumns(textSize);
    tfMaxResultatReprise_.setEditable(true);
    n= 0;
    pnTailleMaxFichier_.add(new BuLabel(getS("Listing Mascaret (Ko)"),BuLabel.RIGHT), n++);
    pnTailleMaxFichier_.add(tfMaxListingCode_, n++);
//    pnTailleMaxFichier_.add(new BuLabel(getS("Listing Damocles (Ko)"),BuLabel.RIGHT), n++);
//    pnTailleMaxFichier_.add(tfMaxListingDamocles_, n++);
    if (CGlobal.AVEC_CALAGE_AUTO) {
    	pnTailleMaxFichier_.add(new BuLabel(getS("Listing calage (Ko)"),BuLabel.RIGHT), n++);
    	pnTailleMaxFichier_.add(tfMaxListingCalage_, n++);
    }
    pnTailleMaxFichier_.add(new BuLabel(getS("R�sultat Opthyca (Ko)"),BuLabel.RIGHT), n++);
    pnTailleMaxFichier_.add(tfMaxResultatOpthyca_, n++);
    pnTailleMaxFichier_.add(new BuLabel(getS("R�sultat Rubens (Ko)"),BuLabel.RIGHT), n++);
    pnTailleMaxFichier_.add(tfMaxResultatRubens_, n++);
    pnTailleMaxFichier_.add(new BuLabel(getS("Reprise calcul (Ko)"),BuLabel.RIGHT), n++);
    pnTailleMaxFichier_.add(tfMaxResultatReprise_, n++);
    n= 0;
    pnPostprocesseurTailleMaxFichier_.add(pnPostprocesseur_, n++);
    pnPostprocesseurTailleMaxFichier_.add(pnTailleMaxFichier_, n++);
    cbHauteurEau_= new BuCheckBox(getS("Hauteur d'eau"));
    cbVitesselitMin_= new BuCheckBox(getS("Vitesse dans le lit mineur"));
    cbCoteFond_= new BuCheckBox(getS("Cote du fond"));
    cbCoteEau_= new BuCheckBox(getS("Cote de l'eau"));
    cbDebitMineur_= new BuCheckBox(getS("D�bit dans le lit mineur"));
    cbDebitMajeur_= new BuCheckBox(getS("D�bit dans le lit majeur"));
    btAutresVariables_= new BuButton(getS("AUTRES VARIABLES"));
    btAutresVariables_.setActionCommand("VARIABLES_RESULTAT");
    btAutresVariables_.addActionListener(this);
    
    btLaisseCrue_ = new BuButton(getS("LAISSES DE CRUES"));
	btLaisseCrue_.setActionCommand("LAISSES_DE_CRUES");        
    
    BuPanel panelBt = new BuPanel(new BorderLayout());
    panelBt.add(btAutresVariables_,BorderLayout.CENTER);
    panelBt.add(btLaisseCrue_,BorderLayout.SOUTH);
    pnBtAutresVariables_.add(panelBt, BuBorderLayout.EAST);
    n= 0;
    pnVar_.add(cbHauteurEau_, n++);
    pnVar_.add(cbVitesselitMin_, n++);
    pnVar_.add(cbCoteFond_, n++);
    pnVar_.add(cbCoteEau_, n++);
    pnVar_.add(cbDebitMineur_, n++);
    pnVar_.add(cbDebitMajeur_, n++);
    n= 0;
    pnVarEnregistres_.add(pnVar_, n++);
    pnVarEnregistres_.add(pnBtAutresVariables_, n++);
    n= 0;
    pnParametres_.add(pnTitre_, n++);
    pnParametres_.add(pnStockage_, n++);
    pnParametres_.add(pnPostprocesseurTailleMaxFichier_, n++);
    pnParametres_.add(pnVarEnregistres_, n++);
    BuScrollPane scrlpEditor= new BuScrollPane(pnParametres_);
    pnMain_.add(scrlpEditor, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      getValeurs();
      fermer();
    } else if ("SECTIONS_CALCUL".equals(cmd)) {
      rbSectionsCalcul_.setSelected(true);
      rbCertainsPoints_.setSelected(false);
      btEditionSites_.setEnabled(false);
    } else if ("CERTAINS_POINTS".equals(cmd)) {
      rbCertainsPoints_.setSelected(true);
      rbSectionsCalcul_.setSelected(false);
      btEditionSites_.setEnabled(true);
    } else {
      super.actionPerformed(_evt);
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (param_ == null)
      return changed;
    String titre= tfTitre_.getText();
    if (!titre.equals(etude_.description())) {
      etude_.description(titre);
      changed= true;
    }
    int premierPasTps= ((Integer)tfPremierPasTemps_.getValue()).intValue();
    if (premierPasTps != param_.paramStockage().premierPasTempsStocke()) {
      param_.paramStockage().premierPasTempsStocke(premierPasTps);
      changed= true;
    }
    int periodeResu= ((Integer)tfPeriodeResultat_.getValue()).intValue();
    if (periodeResu != param_.paramStockage().periodeResultat()) {
      param_.paramStockage().periodeResultat(periodeResu);
      changed= true;
    }
    int periodeList= ((Integer)tfPeriodeListing_.getValue()).intValue();
    if (periodeList != param_.paramStockage().periodeListing()) {
      param_.paramStockage().periodeListing(periodeList);
      changed= true;
    }
    double decale= ((Double)tfDistanceEntreBief_.getValue()).doubleValue();
    if (decale != param_.decalage()) {
      param_.decalage(decale);
      changed= true;
    }
    MetierParametresTailleMaxFichier paramTaille= param_.paramTailleMaxFichier();
    double listCode= ((Double)tfMaxListingCode_.getValue()).doubleValue();
    if (listCode != paramTaille.maxListingCode()) {
      paramTaille.maxListingCode(listCode);
      changed= true;
    }
//    double listDamoc= ((Double)tfMaxListingDamocles_.getValue()).doubleValue();
//    if (listDamoc != paramTaille.maxListingDamocles()) {
//      paramTaille.maxListingDamocles(listDamoc);
//      changed= true;
//    }
    double listCalage= ((Double)tfMaxListingCalage_.getValue()).doubleValue();
    if (listCalage != paramTaille.maxListingCalage()) {
      paramTaille.maxListingCalage(listCalage);
      changed= true;
    }
    double resuOpt= ((Double)tfMaxResultatOpthyca_.getValue()).doubleValue();
    if (resuOpt != paramTaille.maxResultatOpthyca()) {
      paramTaille.maxResultatOpthyca(resuOpt);
      changed= true;
    }
    double resuRub= ((Double)tfMaxResultatRubens_.getValue()).doubleValue();
    if (resuRub != paramTaille.maxResultatRubens()) {
      paramTaille.maxResultatRubens(resuRub);
      changed= true;
    }
    double resuRep= ((Double)tfMaxResultatReprise_.getValue()).doubleValue();
    if (resuRep != paramTaille.maxResultatReprise()) {
      paramTaille.maxResultatReprise(resuRep);
      changed= true;
    }
    boolean geo= cbGeometrie_.isSelected();
    if (geo != param_.optionsListing().geometrie()) {
      param_.optionsListing().geometrie(geo);
      changed= true;
    }
    boolean planim= cbPlanimetrage_.isSelected();
    if (planim != param_.optionsListing().planimetrage()) {
      param_.optionsListing().planimetrage(planim);
      changed= true;
    }
    boolean res= cbReseau_.isSelected();
    if (res != param_.optionsListing().reseau()) {
      param_.optionsListing().reseau(res);
      changed= true;
    }
    boolean lois= cbLoiHydraulique_.isSelected();
    if (lois != param_.optionsListing().loisHydrauliques()) {
      param_.optionsListing().loisHydrauliques(lois);
      changed= true;
    }
    boolean lig= cbLigneEauInitiale_.isSelected();
    if (lig != param_.optionsListing().ligneEauInitiale()) {
      param_.optionsListing().ligneEauInitiale(lig);
      changed= true;
    }
    boolean cal= cbCalcul_.isSelected();
    if (cal != param_.optionsListing().calcul()) {
      param_.optionsListing().calcul(cal);
      changed= true;
    }
    boolean rub= rbRubens_.isSelected();
    if (rub != param_.postRubens()) {
      param_.postRubens(rub);
      changed= true;
    }
    boolean opt= rbOpthyca_.isSelected();
    if (opt != param_.postOpthyca()) {
      param_.postOpthyca(opt);
      changed= true;
    }
    EnumMetierOptionStockage option;
    if (rbCertainsPoints_.isSelected())
      option= EnumMetierOptionStockage.CERTAINS_SITES;
    else
      option= EnumMetierOptionStockage.TOUTES_SECTIONS;
    if (option.value() != param_.paramStockage().option().value()) {
      param_.paramStockage().option(option);
      changed= true;
    }
    boolean hauteurEau= cbHauteurEau_.isSelected();
    if (hauteurEau != param_.existeVariable("Y")) {
      if (hauteurEau)
        param_.ajouteVariable("Y");
      else
        param_.supprimeVariable("Y");
      changed= true;
    }
    boolean vitMineur= cbVitesselitMin_.isSelected();
    if (vitMineur != param_.existeVariable("VMIN")) {
      if (vitMineur)
        param_.ajouteVariable("VMIN");
      else
        param_.supprimeVariable("VMIN");
      changed= true;
    }
    boolean zFond= cbCoteFond_.isSelected();
    if (zFond != param_.existeVariable("ZREF")) {
      if (zFond)
        param_.ajouteVariable("ZREF");
      else
        param_.supprimeVariable("ZREF");
      changed= true;
    }
    boolean zEau= cbCoteEau_.isSelected();
    if (zEau != param_.existeVariable("Z")) {
      if (zEau)
        param_.ajouteVariable("Z");
      else
        param_.supprimeVariable("Z");
      changed= true;
    }
    boolean qMin= cbDebitMineur_.isSelected();
    if (qMin != param_.existeVariable("QMIN")) {
      if (qMin)
        param_.ajouteVariable("QMIN");
      else
        param_.supprimeVariable("QMIN");
      changed= true;
    }
    boolean qMaj= cbDebitMajeur_.isSelected();
    if (qMaj != param_.existeVariable("QMAJ")) {
      if (qMaj)
        param_.ajouteVariable("QMAJ");
      else
        param_.supprimeVariable("QMAJ");
      changed= true;
    }
    return changed;
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierEtude1d))
      return;
    MetierEtude1d etude= (MetierEtude1d)_n;
    if (etude == etude_)
      return;
    etude_= etude;
    param_= etude.paramResultats();
    setValeurs();
  }
  @Override
  protected void setValeurs() {
    tfTitre_.setValue(etude_.description());
    tfPremierPasTemps_.setValue(
      new Integer(param_.paramStockage().premierPasTempsStocke()));
    tfPeriodeListing_.setValue(
      new Integer(param_.paramStockage().periodeListing()));
    tfPeriodeResultat_.setValue(
      new Integer(param_.paramStockage().periodeResultat()));
    tfDistanceEntreBief_.setValue(new Double(param_.decalage()));
    tfMaxListingCode_.setValue(
      new Double(param_.paramTailleMaxFichier().maxListingCode()));
//    tfMaxListingDamocles_.setValue(
//      new Double(param_.paramTailleMaxFichier().maxListingDamocles()));
    tfMaxListingCalage_.setValue(
      new Double(param_.paramTailleMaxFichier().maxListingCalage()));
    tfMaxResultatOpthyca_.setValue(
      new Double(param_.paramTailleMaxFichier().maxResultatOpthyca()));
    tfMaxResultatRubens_.setValue(
      new Double(param_.paramTailleMaxFichier().maxResultatRubens()));
    tfMaxResultatReprise_.setValue(
      new Double(param_.paramTailleMaxFichier().maxResultatReprise()));
    cbGeometrie_.setSelected(param_.optionsListing().geometrie());
    cbPlanimetrage_.setSelected(param_.optionsListing().planimetrage());
    cbReseau_.setSelected(param_.optionsListing().reseau());
    cbLoiHydraulique_.setSelected(param_.optionsListing().loisHydrauliques());
    cbLigneEauInitiale_.setSelected(param_.optionsListing().ligneEauInitiale());
    cbCalcul_.setSelected(param_.optionsListing().calcul());
    if (param_.paramStockage().option().value()
      == EnumMetierOptionStockage._CERTAINS_SITES) {
      rbCertainsPoints_.setSelected(true);
      rbSectionsCalcul_.setSelected(false);
      btEditionSites_.setEnabled(true);
    } else {
      rbCertainsPoints_.setSelected(false);
      rbSectionsCalcul_.setSelected(true);
      btEditionSites_.setEnabled(false);
    }
    rbRubens_.setSelected(param_.postRubens());
    rbOpthyca_.setSelected(param_.postOpthyca());
    if (param_.existeVariable("Y"))
      cbHauteurEau_.setSelected(true);
    else
      cbHauteurEau_.setSelected(false);
    if (param_.existeVariable("VMIN"))
      cbVitesselitMin_.setSelected(true);
    else
      cbVitesselitMin_.setSelected(false);
    if (param_.existeVariable("ZREF"))
      cbCoteFond_.setSelected(true);
    else
      cbCoteFond_.setSelected(false);
    if (param_.existeVariable("Z"))
      cbCoteEau_.setSelected(true);
    else
      cbCoteEau_.setSelected(false);
    if (param_.existeVariable("QMIN"))
      cbDebitMineur_.setSelected(true);
    else
      cbDebitMineur_.setSelected(false);
    if (param_.existeVariable("QMAJ"))
      cbDebitMajeur_.setSelected(true);
    else
      cbDebitMajeur_.setSelected(false);
  }
}
