package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresResultats;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuMultiLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur des param�tres r�sultats (DParametresResultats).<br>
 * Appeler si l'utilisateur clic sur le menu "Qualite d'eau/Param�tres R�sultats".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PARAM_RESULTAT_QUALITE_EAU().editer().<br>
 * @version      $Revision: 1.4 $ $Date: 2007-11-20 11:42:43 $ by $Author: bmarchan $
 * @author       Olivier Pasteur
 */
public class Hydraulique1dParametresResultatsQualiteDEauEditor extends
        Hydraulique1dCustomizerImprimable {

    private BuPanel pnParametres_, pnPostprocesseurOptionsListing_;
    private BuPanel pnPostprocesseur_, pnTailleMaxFichier_, pnOptionsListing_;
    private BuRadioButton rbOpthyca_, rbRubens_;
//    private ButtonGroup bgPostProcesseur_;
    private BuCheckBox cbConcInits_, cbLoisTracer_, cbConcentrations_, cbBilanTracer_;
    private BuTextField tfMaxListingTracer,tfMaxResultatOpthycaTracer_,tfMaxResultatRubensTracer_;
    private BuHorizontalLayout loPostprocesseurOptionsListing_;
    private BuVerticalLayout loPostprocesseur_, loOptionsListing_,
            loParametres_;
    private BuGridLayout loTailleMaxFichier_;
    private BuLabelMultiLine lbInfosResultatsQE_;
    private MetierEtude1d etude_;
    private MetierParametresResultats param_;

    public Hydraulique1dParametresResultatsQualiteDEauEditor() {
    this(null);
  }

  public Hydraulique1dParametresResultatsQualiteDEauEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition des param�tres r�sultats de la qualit� d'eau"));
    param_= null;

    loPostprocesseurOptionsListing_= new BuHorizontalLayout(5, false, false);
    loPostprocesseur_= new BuVerticalLayout(5, false, false);
    loOptionsListing_ = new BuVerticalLayout(5, false, false);
    loParametres_ = new BuVerticalLayout(5, false, false);
    loTailleMaxFichier_ = new BuGridLayout(4, 5, 5, true, false);

    Container pnMain_= getContentPane();
    pnParametres_ = new BuPanel();
    pnParametres_.setLayout(loParametres_);
    pnParametres_.setBorder(
            new CompoundBorder(
                    new EtchedBorder(),
                    new EmptyBorder(new Insets(5, 5, 5, 5))));

    pnOptionsListing_ = new BuPanel();
    pnOptionsListing_.setLayout(loOptionsListing_);
    CompoundBorder bdOptionsListing =
            new CompoundBorder(
                    new EtchedBorder(),
                    new EmptyBorder(new Insets(2, 2, 2, 2)));
    TitledBorder tbListing =
            new TitledBorder(
                    bdOptionsListing,
                    getS("Ecriture et options pour le fichier listing Tracer"));
    pnOptionsListing_.setBorder(tbListing);
    pnOptionsListing_.setPreferredSize(new Dimension(490,150));

    pnPostprocesseur_= new BuPanel();
        pnPostprocesseur_.setLayout(loPostprocesseur_);
        CompoundBorder bdPostprocesseur=
          new CompoundBorder(
            new EtchedBorder(),
            new EmptyBorder(new Insets(2, 2, 2, 2)));
        TitledBorder tbPostprocesseur=
          new TitledBorder(bdPostprocesseur, getS("Postprocesseur"));
        pnPostprocesseur_.setBorder(tbPostprocesseur);

        pnTailleMaxFichier_= new BuPanel();
        pnTailleMaxFichier_.setLayout(loTailleMaxFichier_);
        CompoundBorder bdTailleMaxFichier=
          new CompoundBorder(
            new EtchedBorder(),
            new EmptyBorder(new Insets(2, 2, 2, 2)));
        TitledBorder tbTailleMaxFichier=
          new TitledBorder(
            bdTailleMaxFichier,
            getS("Taille max des fichiers lus par Fudaa-Mascaret"));
        pnTailleMaxFichier_.setBorder(tbTailleMaxFichier);

        pnPostprocesseurOptionsListing_= new BuPanel();
        pnPostprocesseurOptionsListing_.setLayout(
      loPostprocesseurOptionsListing_);


    cbConcInits_= new BuCheckBox(getS("Concentrations initiales"));
    cbLoisTracer_= new BuCheckBox(getS("Lois Tracer"));
    cbConcentrations_= new BuCheckBox(getS("Concentrations"));
    cbBilanTracer_= new BuCheckBox(getS("Bilan Tracer"));
    int n= 0;
    pnOptionsListing_.add(cbConcInits_, n++);
    pnOptionsListing_.add(cbLoisTracer_, n++);
    pnOptionsListing_.add(cbConcentrations_, n++);
    pnOptionsListing_.add(cbBilanTracer_, n++);


    rbRubens_= new BuRadioButton("Rubens");
    rbOpthyca_= new BuRadioButton("Opthyca");
    ButtonGroup grpRubensOpthyca = new ButtonGroup();
    grpRubensOpthyca.add(rbRubens_);
    grpRubensOpthyca.add(rbOpthyca_);
    n= 0;
    pnPostprocesseur_.add(rbRubens_, n++);
    pnPostprocesseur_.add(rbOpthyca_, n++);
    pnPostprocesseur_.setPreferredSize(new Dimension(150,150));

    int textSize= 5;
    tfMaxListingTracer = BuTextField.createDoubleField();
    tfMaxListingTracer.setColumns(textSize);
    tfMaxListingTracer.setEditable(true);
    tfMaxResultatOpthycaTracer_ = BuTextField.createDoubleField();
    tfMaxResultatOpthycaTracer_.setColumns(textSize);
    tfMaxResultatOpthycaTracer_.setEditable(true);
    tfMaxResultatRubensTracer_ = BuTextField.createDoubleField();
    tfMaxResultatRubensTracer_.setColumns(textSize);
    tfMaxResultatRubensTracer_.setEditable(true);
    n= 0;
    pnTailleMaxFichier_.add(new BuLabel(getS("Listing de la qualit� d'eau (Ko)"),BuLabel.RIGHT), n++);
    pnTailleMaxFichier_.add(tfMaxListingTracer, n++);
    pnTailleMaxFichier_.add(new BuLabel(getS("R�sultat Opthyca de la qualit� d'eau(Ko)"),BuLabel.RIGHT), n++);
    pnTailleMaxFichier_.add(tfMaxResultatOpthycaTracer_, n++);
    pnTailleMaxFichier_.add(new BuLabel(getS("R�sultat Rubens de la qualit� d'eau (Ko)"),BuLabel.RIGHT), n++);
    pnTailleMaxFichier_.add(tfMaxResultatRubensTracer_, n++);

    //Label
    lbInfosResultatsQE_ = new BuMultiLabel(getS("La fr�quence d'enregistrement des r�sultats utilis�e est celle d�finie dans les param�tres r�sultats de l'hydraulique"));
    Font font = new Font("Arial", Font.ITALIC, 12);
    lbInfosResultatsQE_.setFont(font);
    Icon icon = BuResource.BU.getIcon("astuce_22.gif");
    lbInfosResultatsQE_.setIcon(icon);
    lbInfosResultatsQE_.setVisible(true);


    n= 0;
    pnPostprocesseurOptionsListing_.add(pnPostprocesseur_, n++);
    pnPostprocesseurOptionsListing_.add(pnOptionsListing_, n++);

    n= 0;
    pnParametres_.add(pnPostprocesseurOptionsListing_, n++);
    pnParametres_.add(pnTailleMaxFichier_, n++);
    pnParametres_.add(lbInfosResultatsQE_,n++);

    BuScrollPane scrlpEditor= new BuScrollPane(pnParametres_);
    pnMain_.add(scrlpEditor, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();

  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
      String cmd= _evt.getActionCommand();
      if ("VALIDER".equals(cmd)) {
        getValeurs();
        fermer();
      } else {
        super.actionPerformed(_evt);
      }
  }
    /**
     * Transfert les donn�es de l'�diteur vers l'objet m�tier.
     *
     * @return VRAI s'il y a au moins une modification par rapport � l'objet
     *   m�tier.
     * @todo Implement this
     *   org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer method
     */
  @Override
    protected boolean getValeurs() {
        boolean changed = false;
        if (param_ == null)
            return changed;
        boolean concInits = cbConcInits_.isSelected();
        if (concInits != param_.optionsListingTracer().concentInit()) {
            param_.optionsListingTracer().concentInit(concInits);
            changed = true;
        }
        boolean loisTracer = cbLoisTracer_.isSelected();
        if (loisTracer != param_.optionsListingTracer().loiTracer()) {
            param_.optionsListingTracer().loiTracer(loisTracer);
            changed = true;
        }
        boolean concentrations = cbConcentrations_.isSelected();
        if (concentrations != param_.optionsListingTracer().concentrations()) {
            param_.optionsListingTracer().concentrations(concentrations);
            changed = true;
        }
        boolean bilanTracer = cbBilanTracer_.isSelected();
        if (bilanTracer != param_.optionsListingTracer().bilanTracer()) {
            param_.optionsListingTracer().bilanTracer(bilanTracer);
            changed = true;
        }
        double listingTracer = ((Double) tfMaxListingTracer.getValue()).
                                       doubleValue();
        if (listingTracer != param_.paramTailleMaxFichier().maxListingTracer()) {
            param_.paramTailleMaxFichier().maxListingTracer(listingTracer);
            changed = true;
        }
        double resultatOpthyca = ((Double) tfMaxResultatOpthycaTracer_.getValue()).
                                 doubleValue();
        if (resultatOpthyca !=
            param_.paramTailleMaxFichier().maxResultatOpthycaTracer()) {
            param_.paramTailleMaxFichier().maxResultatOpthycaTracer(resultatOpthyca);
            changed = true;
        }
        double resultatRubens = ((Double) tfMaxResultatRubensTracer_.getValue()).
                                doubleValue();
        if (resultatRubens !=
            param_.paramTailleMaxFichier().maxResultatRubensTracer()) {
            param_.paramTailleMaxFichier().maxResultatRubensTracer(resultatRubens);
            changed = true;
        }

        boolean opt = rbOpthyca_.isSelected();
        if (opt != param_.postOpthycaQualiteDEau()) {
            param_.postOpthycaQualiteDEau(opt);
            changed = true;
        }

        return changed;
    }

    /**
     * setObject
     *
     * @param _n MetierHydraulique1d
     * @todo Implement this
     *   org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer method
     */
  @Override
    public void setObject(MetierHydraulique1d _n) {
        if (!(_n instanceof MetierEtude1d))
            return;
        MetierEtude1d etude = (MetierEtude1d) _n;
        if (etude == etude_)
            return;
        etude_ = etude;
        param_ = etude.paramResultats();
        setValeurs();
    }

    /**
     * Transfert les donn�es de l'objet m�tier vers les composants graphique
     * de l'�diteur.
     *
     * @todo Implement this
     *   org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer method
     */
  @Override
    protected void setValeurs() {
        System.out.println("setValeurs()");
        tfMaxListingTracer.setValue(
                new Double(param_.paramTailleMaxFichier().maxListingTracer()));
        tfMaxResultatOpthycaTracer_.setValue(
                new Double(param_.paramTailleMaxFichier().maxResultatOpthycaTracer()));
        tfMaxResultatRubensTracer_.setValue(
                new Double(param_.paramTailleMaxFichier().maxResultatRubensTracer()));

        rbOpthyca_.setSelected(param_.postOpthycaQualiteDEau());
        rbRubens_.setSelected(!param_.postOpthycaQualiteDEau());

        cbConcInits_.setSelected(param_.optionsListingTracer().concentInit());
        cbConcentrations_.setSelected(param_.optionsListingTracer().concentrations());
        cbLoisTracer_.setSelected(param_.optionsListingTracer().loiTracer());
        cbBilanTracer_.setSelected(param_.optionsListingTracer().bilanTracer());

    }
}
