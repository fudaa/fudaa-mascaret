package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierCalculSediment;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleSediment;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierParametresSediment;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierSediment;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.fu.FuLog;

/**
 * Editeur des param�tres de s�diment.
 * 
 * @version      $Id$
 * @author       Bertrand Marchand (marchand@deltacad.fr)
 */
public class Hydraulique1dParametresSedimentEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {

  private MetierParametresSediment param_;
  private MetierEtude1d etude_;

  private JCheckBox cbActif_;
  private BuLabel lbDmoy_;
  private BuTextField tfDmoy_;
  private BuLabel lbD30_;
  private BuTextField tfD30_;
  private BuLabel lbD50_;
  private BuTextField tfD50_;
  private BuLabel lbD84_;
  private BuTextField tfD84_;
  private BuLabel lbD90_;
  private BuTextField tfD90_;
  private BuLabel lbRugo_;
  private BuTextField tfRugo_;
  private BuLabel lbDensMat_;
  private BuTextField tfDensMat_;
  private BuLabel lbDensApparente_;
  private BuTextField tfDensApparente_;
  private BuLabel lbTauc_;
  private BuTextField tfTauc_;
  private BuLabel lbTEau_;
  private BuTextField tfTEau_;
  private BuLabel lbFormule_;
  private BuList lsFormule_;
  private DefaultListModel mdFormule_;
  private BuPanel pnPrincipal_;
  private BorderLayout lyPrincipal_;
  private JCheckBox cbResMoyennes_;
  private BuLabel lbLargeur_;
  private BuTextField tfLargeur_;
  private BuLabel lbResMoyennes_;

  public Hydraulique1dParametresSedimentEditor() {
    this(null);
  }

  public Hydraulique1dParametresSedimentEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.getS("Edition des param�tres de s�dimentologie"));

    cbActif_=new JCheckBox(getS("S�dimentologie active"));
    cbActif_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        changeCbActif();
      }
    });
    lbDmoy_=new BuLabel(getS("Diam�tre moyen"));
    tfDmoy_=BuTextField.createDoubleField();
    tfDmoy_.setColumns(7);
    lbD30_=new BuLabel(getS("Diam�tre fractile 30"));
    tfD30_=BuTextField.createDoubleField();
    lbD50_=new BuLabel(getS("Diam�tre fractile 50"));
    tfD50_=BuTextField.createDoubleField();
    lbD84_=new BuLabel(getS("Diam�tre fractile 84"));
    tfD84_=BuTextField.createDoubleField();
    lbD90_=new BuLabel(getS("Diam�tre fractile 90"));
    tfD90_=BuTextField.createDoubleField();
    lbRugo_=new BuLabel(getS("Strickler de peau"));
    tfRugo_=BuTextField.createDoubleField();
    lbDensMat_=new BuLabel(getS("Densit� de mat�riau"));
    tfDensMat_=BuTextField.createDoubleField();
    lbDensApparente_=new BuLabel(getS("Densit� apparente"));
    tfDensApparente_=BuTextField.createDoubleField();
    lbTauc_=new BuLabel(getS("Contrainte critique adimensionnelle"));
    tfTauc_=BuTextField.createDoubleField();
    lbTEau_=new BuLabel(getS("Temp�rature de l'eau"));
    tfTEau_=BuTextField.createDoubleField();

    BuPanel pnParams=new BuPanel();
    pnParams.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),getS("Param�tres")));
    pnParams.setLayout(new BuGridLayout(2,5,2));
    pnParams.add(lbDmoy_);
    pnParams.add(tfDmoy_);
    pnParams.add(lbD30_);
    pnParams.add(tfD30_);
    pnParams.add(lbD50_);
    pnParams.add(tfD50_);
    pnParams.add(lbD84_);
    pnParams.add(tfD84_);
    pnParams.add(lbD90_);
    pnParams.add(tfD90_);
    pnParams.add(lbRugo_);
    pnParams.add(tfRugo_);
    pnParams.add(lbDensMat_);
    pnParams.add(tfDensMat_);
    pnParams.add(lbDensApparente_);
    pnParams.add(tfDensApparente_);
    pnParams.add(lbTauc_);
    pnParams.add(tfTauc_);
    pnParams.add(lbTEau_);
    pnParams.add(tfTEau_);
    
    lbFormule_=new BuLabel(getS("R�sultats calcul�s"));
    lbFormule_.setVerticalAlignment(SwingConstants.TOP);
    mdFormule_=new DefaultListModel();
    for (MetierFormuleSediment item : MetierFormuleSediment.values()) {
      mdFormule_.addElement(item);
    }
    lsFormule_=new BuList(mdFormule_);
    JScrollPane spFormule=new JScrollPane(lsFormule_);
    spFormule.setPreferredSize(new Dimension(150,100));
    
    cbResMoyennes_=new JCheckBox();
    cbResMoyennes_.setHorizontalTextPosition(SwingConstants.LEFT);
    cbResMoyennes_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        lbLargeur_.setEnabled(cbResMoyennes_.isSelected());
        tfLargeur_.setEnabled(cbResMoyennes_.isSelected());
      }
    });
    lbLargeur_=new BuLabel(getS("Largeur de la fen�tre de moyennation")+" (m)");
    tfLargeur_=BuTextField.createDoubleField();
    lbResMoyennes_ = new BuLabel(getS("Inclure les r�sultats moyenn�s"));
    
    BuPanel pnResults=new BuPanel();
    pnResults.setLayout(new BuGridLayout(2, 5, 2));
    pnResults.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),getS("R�sultats")));
    pnResults.add(lbFormule_);
    pnResults.add(spFormule);
    pnResults.add(lbResMoyennes_);
    pnResults.add(cbResMoyennes_);
    pnResults.add(lbLargeur_);
    pnResults.add(tfLargeur_);

    pnPrincipal_=new BuPanel();
    lyPrincipal_=new BorderLayout(5,0);
    pnPrincipal_.setLayout(lyPrincipal_);
    pnPrincipal_.add(pnParams,BorderLayout.CENTER);
    pnPrincipal_.add(pnResults,BorderLayout.SOUTH);
    getContentPane().add(cbActif_,BorderLayout.NORTH);
    getContentPane().add(pnPrincipal_,BorderLayout.CENTER);
    
    BuLib.giveSameWidth(lbResMoyennes_,lbLargeur_,lbFormule_,lbTauc_);

    param_= null;
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      String mes=isValide();
      if (mes!=null) {
        JOptionPane.showMessageDialog(this,
         mes+".",
         getS("Erreur de mise en donn�es"),
          JOptionPane.ERROR_MESSAGE);
      }
      else {
        if (getValeurs()) firePropertyChange("parametres", null, param_);
        updateResults();
        fermer();
      }
    }
    else {
      super.actionPerformed(_evt);
    }
  }

  /**
   * Controle que le panneau est correctement rempli.
   * @return Un message indiquant l'origine du pb. null si pas de pb.
   */
  public String isValide() {
    if (!cbActif_.isSelected()) return null;
    
    if (tfDmoy_.getValue()==null || ((Double)tfDmoy_.getValue())<=0) 
      return getS("Diam�tre moyen")+" : "+getS("La valeur doit �tre strictement positive");
    if (tfD30_.getValue()==null || ((Double)tfD30_.getValue())<=0) 
      return getS("Diam�tre fractile 30")+" : "+getS("La valeur doit �tre strictement positive");
    if (tfD50_.getValue()==null || ((Double)tfD50_.getValue())<=0) 
      return getS("Diam�tre fractile 50")+" : "+getS("La valeur doit �tre strictement positive");
    if (tfD84_.getValue()==null || ((Double)tfD84_.getValue())<=0) 
      return getS("Diam�tre fractile 84")+" : "+getS("La valeur doit �tre strictement positive");
    if (tfD90_.getValue()==null || ((Double)tfD90_.getValue())<=0) 
      return getS("Diam�tre fractile 90")+" : "+getS("La valeur doit �tre strictement positive");
    if (tfRugo_.getValue()==null || ((Double)tfRugo_.getValue())<=0) 
      return getS("Rugosit� de grain")+" : "+getS("La valeur doit �tre strictement positive");
    if (tfDensMat_.getValue()==null || ((Double)tfDensMat_.getValue())<=0) 
      return getS("Densit� de mat�riau")+" : "+getS("La valeur doit �tre strictement positive");
    if (tfDensApparente_.getValue()==null || ((Double)tfDensApparente_.getValue())<=0) 
      return getS("Densit� apparente")+" : "+getS("La valeur doit �tre strictement positive");
    if (tfTauc_.getValue()==null) 
      return getS("Contrainte critique adimensionnelle")+" : "+getS("La valeur doit �tre donn�e");
    if (tfTEau_.getValue()==null) 
      return getS("Temp�rature de l'eau")+" : "+getS("La valeur doit �tre donn�e");
    if (lsFormule_.getSelectedIndex()==-1) 
      return getS("Aucun r�sultat n'est s�lectionn�");
    if (cbResMoyennes_.isSelected() && (tfLargeur_.getValue()==null || ((Double)tfLargeur_.getValue())<0)) 
      return getS("Largeur de la fen�tre de moyennation")+" : "+getS("La valeur doit �tre positive");

    return null;
  }

  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    
    boolean isActif=cbActif_.isSelected();
    if (isActif!=param_.isActif()) {
      param_.setActif(isActif);
      changed=true;
    }
    if (!isActif) return changed;

    double dmoy=((Double)tfDmoy_.getValue());
    if (dmoy!=param_.getDmoyen()) {
      param_.setDmoyen(dmoy);
      changed=true;
    }
    double d30=((Double)tfD30_.getValue());
    if (d30!=param_.getD30()) {
      param_.setD30(d30);
      changed=true;
    }
    double d50=((Double)tfD50_.getValue());
    if (d50!=param_.getD50()) {
      param_.setD50(d50);
      changed=true;
    }
    double d84=((Double)tfD84_.getValue());
    if (d84!=param_.getD84()) {
      param_.setD84(d84);
      changed=true;
    }
    double d90=((Double)tfD90_.getValue());
    if (d90!=param_.getD90()) {
      param_.setD90(d90);
      changed=true;
    }
    double rugo=((Double)tfRugo_.getValue());
    if (rugo!=param_.getRugosite()) {
      param_.setRugosite(rugo);
      changed=true;
    }
    double densMat=((Double)tfDensMat_.getValue());
    if (densMat!=param_.getDensiteMateriau()) {
      param_.setDensiteMateriau(densMat);
      changed=true;
    }
    double densApparente=((Double)tfDensApparente_.getValue());
    if (densApparente!=param_.getDensiteApparente()) {
      param_.setDensiteApparente(densApparente);
      changed=true;
    }
    double tauc=((Double)tfTauc_.getValue());
    if (tauc!=param_.getTauc()) {
      param_.setTauc(tauc);
      changed=true;
    }
    double tEau=((Double)tfTEau_.getValue());
    if (tEau!=param_.getTEau()) {
      param_.setTEau(tEau);
      changed=true;
    }
    int[] selIdx=lsFormule_.getSelectedIndices();
    MetierFormuleSediment[] formules=new MetierFormuleSediment[selIdx.length];
    for (int i=0; i<selIdx.length; i++) {
      formules[i]=(MetierFormuleSediment)mdFormule_.get(selIdx[i]);
    }
    if (!Arrays.equals(formules, param_.getFormules())) {
      param_.setFormules(formules);
      changed=true;
    }
    
    boolean isResMoyennes=cbResMoyennes_.isSelected();
    if (isResMoyennes!=param_.isResMoyCalcules()) {
      param_.setResMoyCalcules(isResMoyennes);
      changed=true;
    }
    if (!isResMoyennes) return changed;
    
    double lgFenetre=((Double)tfLargeur_.getValue());
    if (lgFenetre!=param_.getLargeurFenetre()) {
      param_.setLargeurFenetre(lgFenetre);
      changed=true;
    }

    return changed;
  }

  /**
   * Pas utilis�. Trop g�n�raliste.
   */
  @Override
  public void setObject(MetierHydraulique1d _param) {
  }
  
  public void setParametres(MetierParametresSediment _param) {
    if (_param==param_) return;
    param_=_param;
    setValeurs();
  }
  
  public void setEtude(MetierEtude1d _etude) {
    if (etude_==_etude) return;
    etude_= _etude;
  }

  private void updateResults() {
    if (etude_.resultatsGeneraux().hasResultatsTemporelSpatial()) {
      MetierCalculSediment calSed=new MetierCalculSediment(etude_.resultatsGeneraux().resultatsTemporelSpatial(), etude_.sediment().parametres());      
      // On supprime les resultats de s�dimentologie pr�c�dents.
      calSed.clearResultatsSediment();

      // On ajoute le nouveau r�sultat
      if (etude_.sediment().parametres().isActif()) {
        
        MetierDescriptionVariable[] vars=calSed.computeResultsForVariables();
        if (vars.length!=0) {
          StringBuilder sb=new StringBuilder();
          for (MetierDescriptionVariable var : vars) {
            sb.append("\n- ").append(var.description());
          }
          JOptionPane.showMessageDialog(this, getS("Les r�sultats hydrauliques suivants sont n�cessaires pour\nle calcul des r�sultats s�dimentologiques choisis:{0}\n\nCertains r�sultats s�dimentologiques ne seront pas calcul�s.", sb.toString()),
              getS("Attention"), JOptionPane.WARNING_MESSAGE);
        
        }
      }
    }
  }
  
  private void changeCbActif() {
    boolean b=cbActif_.isSelected();
    lbDmoy_.setEnabled(b);
    lbD30_.setEnabled(b);
    lbD50_.setEnabled(b);
    lbD84_.setEnabled(b);
    lbD90_.setEnabled(b);
    lbRugo_.setEnabled(b);
    lbDensMat_.setEnabled(b);
    lbDensApparente_.setEnabled(b);
    lbTauc_.setEnabled(b);
    lbTEau_.setEnabled(b);
    lbFormule_.setEnabled(b);
    tfDmoy_.setEnabled(b);
    tfD30_.setEnabled(b);
    tfD50_.setEnabled(b);
    tfD84_.setEnabled(b);
    tfD90_.setEnabled(b);
    tfRugo_.setEnabled(b);
    tfDensMat_.setEnabled(b);
    tfDensApparente_.setEnabled(b);
    tfTauc_.setEnabled(b);
    tfTEau_.setEnabled(b);
    lsFormule_.setEnabled(b);
    lbResMoyennes_.setEnabled(b);
    cbResMoyennes_.setEnabled(b);
    lbLargeur_.setEnabled(b & cbResMoyennes_.isSelected());
    tfLargeur_.setEnabled(b & cbResMoyennes_.isSelected());
  }
  
  @Override
  protected void setValeurs() {
    for (MetierDescriptionVariable var : MetierSediment.VARIABLES) {
      FuLog.debug("Description=" + var.description() + ", Name=" + var.nom());
    }

    cbActif_.setSelected(param_.isActif());
    tfDmoy_.setValue(param_.getDmoyen());
    tfD30_.setValue(param_.getD30());
    tfD50_.setValue(param_.getD50());
    tfD84_.setValue(param_.getD84());
    tfD90_.setValue(param_.getD90());
    tfRugo_.setValue(param_.getRugosite());
    tfDensMat_.setValue(param_.getDensiteMateriau());
    tfDensApparente_.setValue(param_.getDensiteApparente());
    tfTauc_.setValue(param_.getTauc());
    tfTEau_.setValue(param_.getTEau());
    
    if (param_.getFormules()!=null) {
      lsFormule_.clearSelection();
      for (MetierFormuleSediment form : param_.getFormules()) {
        int idx=mdFormule_.indexOf(form);
        if (idx!=-1)
          lsFormule_.addSelectionInterval(idx,idx);
      }
    }
    
    cbResMoyennes_.setSelected(param_.isResMoyCalcules());
    tfLargeur_.setValue(param_.getLargeurFenetre());
    
    changeCbActif();
  }
}
