/*
 * @file         Hydraulique1dParametresTemporelsEditor.java
 * @creation     2000-12-07
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierCritereArret;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresTemporels;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dDialogContraintes;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des param�tres temporels (DParametresTemporels).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Param�tres Temporels".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PARAM_TEMPOREL().editer().<br>
 * @version      $Revision: 1.22 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dParametresTemporelsEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  BuPanel pnParamTemporels_, pnTps_, pnNbCourant_, pnCritereArret_,pnTfNbCourant_;
  BuPanel pnCritereArretRb_, pnTpsMax_, pnNbPasTps_,pnCoteMaxCtr_,pnPtsControle_,pnCoteMaxRb_, pnTpsCritere_;
  BuGridLayout loTps_;
  BuHorizontalLayout
    loCritereArret_,
    loTpsMax_,
    loNbPasTps_,
    loTpsCritere_,
    loPtsControle_,
    loCoteMaxRb_;
  BuVerticalLayout loParamTemporels_, loCritereArretRb_,loCoteMaxCtr_, loNbCourant_;
  BuTextField tfTpsInitial_, tfPasTps_, tfTpsMax_, tfNbPasTps_,tfCoteMaxCtr_,tfAbsPtsCtr_,tfBiefPtsCtr_, tfNbCourant_;
  BuRadioButton rbTpsMax_, rbNbPasTps_,rbCoteMaxCtr_;
  BuCheckBox cbPasTpsVariable_;
  ButtonGroup bcCritereArretRb_;
  private MetierParametresGeneraux paramGeneraux_;
  private MetierParametresTemporels param_;
  private MetierReseau paramReseau_;
  public Hydraulique1dParametresTemporelsEditor() {
    this(null);
  }
  public Hydraulique1dParametresTemporelsEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition des param�tres temporels"));
    param_= null;
    loTps_= new BuGridLayout(2, 5, 5, false, false);
    loNbCourant_= new BuVerticalLayout(5, true, true);
    loCritereArret_= new BuHorizontalLayout(5, false, false);
    loTpsMax_= new BuHorizontalLayout(2, false, false);
    loNbPasTps_= new BuHorizontalLayout(2, false, false);
    loTpsCritere_= new BuHorizontalLayout(10, false, false);
    loCritereArretRb_= new BuVerticalLayout(10, false, false);
    loParamTemporels_= new BuVerticalLayout(5, true, false);
    loPtsControle_= new BuHorizontalLayout(2, false, false);
    loCoteMaxRb_= new BuHorizontalLayout(2, false, false);
    loCoteMaxCtr_= new BuVerticalLayout(1, false, false);
    Container pnMain_= getContentPane();
    pnNbPasTps_= new BuPanel();
    pnNbPasTps_.setLayout(loNbPasTps_);
    pnTpsMax_= new BuPanel();
    pnTpsMax_.setLayout(loTpsMax_);
    pnCoteMaxCtr_ = new BuPanel();
    pnCoteMaxCtr_.setLayout(loCoteMaxCtr_);
    pnPtsControle_ = new BuPanel();
    pnPtsControle_.setLayout(loPtsControle_);
    pnCoteMaxRb_= new BuPanel();
    pnCoteMaxRb_.setLayout(loCoteMaxRb_);
    pnCritereArretRb_= new BuPanel();
    pnCritereArretRb_.setLayout(loCritereArretRb_);
    pnCritereArretRb_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnCritereArret_= new BuPanel();
    pnCritereArret_.setLayout(loCritereArret_);
    pnNbCourant_= new BuPanel();
    pnNbCourant_.setLayout(loNbCourant_);
    pnNbCourant_.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),getS("Noyau transcritique")));
    pnTps_= new BuPanel();
    pnTps_.setLayout(loTps_);
    pnTpsCritere_= new BuPanel();
    pnTpsCritere_.setLayout(loTpsCritere_);
    pnParamTemporels_= new BuPanel();
    pnParamTemporels_.setLayout(loParamTemporels_);
    pnParamTemporels_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    bcCritereArretRb_ = new ButtonGroup();
    int n= 0;
    int textSize= 7;
    pnTps_.add(new BuLabel(getS("Temps initial")), n++);
    tfTpsInitial_= BuTextField.createDoubleField();
    tfTpsInitial_.setColumns(textSize);
    tfTpsInitial_.setEditable(true);
    pnTps_.add(tfTpsInitial_, n++);
    pnTps_.add(new BuLabel(getS("Pas de temps")), n++);
    tfPasTps_= BuTextField.createDoubleField();
    tfPasTps_.setColumns(textSize);
    tfPasTps_.setEditable(true);
    pnTps_.add(tfPasTps_, n++);

    //pnTpsMax_
    rbTpsMax_= new BuRadioButton(getS("Temps maximum"));
    bcCritereArretRb_.add(rbTpsMax_);
    rbTpsMax_.addActionListener(this);
    rbTpsMax_.setActionCommand("TEMPS_MAX");
    tfTpsMax_= BuTextField.createDoubleField();
    tfTpsMax_.setColumns(textSize);
    tfTpsMax_.setEditable(true);
    n= 0;
    pnTpsMax_.add(rbTpsMax_, n++);
    pnTpsMax_.add(tfTpsMax_, n++);

    //pnNbPasTps_
    rbNbPasTps_= new BuRadioButton(getS("Nombre de pas de temps max"));
    bcCritereArretRb_.add(rbNbPasTps_);
    rbNbPasTps_.addActionListener(this);
    rbNbPasTps_.setActionCommand("NB_PAS_TEMPS");
    tfNbPasTps_= BuTextField.createIntegerField();
    tfNbPasTps_.setColumns(textSize);
    tfNbPasTps_.setEditable(true);
    n= 0;
    pnNbPasTps_.add(rbNbPasTps_, n++);
    pnNbPasTps_.add(tfNbPasTps_, n++);

    //Pannel Point de controle
    n = 0;
    pnPtsControle_.add(new BuLabel("      "+getS("Bief")), n++);
    tfBiefPtsCtr_ = BuTextField.createIntegerField();
    tfBiefPtsCtr_.setColumns(textSize);
    tfBiefPtsCtr_.setEditable(true);
    pnPtsControle_.add(tfBiefPtsCtr_, n++);
    pnPtsControle_.add(new BuLabel("  "+getS("Abscisse")), n++);
    tfAbsPtsCtr_ = BuTextField.createDoubleField();
    tfAbsPtsCtr_.setColumns(textSize);
    tfAbsPtsCtr_.setEditable(true);
    pnPtsControle_.add(tfAbsPtsCtr_, n++);

    //Pannel Radio bouton cote max
    rbCoteMaxCtr_ = new BuRadioButton(getS("Cote maximale de contr�le"));
    bcCritereArretRb_.add(rbCoteMaxCtr_);
    rbCoteMaxCtr_.addActionListener(this);
    rbCoteMaxCtr_.setActionCommand("COTE_MAX");
    tfCoteMaxCtr_ = BuTextField.createDoubleField();
    tfCoteMaxCtr_.setColumns(textSize);
    tfCoteMaxCtr_.setEditable(true);
    n = 0;
    pnCoteMaxRb_.add(rbCoteMaxCtr_, n++);
    pnCoteMaxRb_.add(tfCoteMaxCtr_, n++);

    //Pannel pnCoteMaxCtr_ contenant pnCoteMaxRb_, pnPtsControle_ et un Label
    n = 0;
    pnCoteMaxCtr_.add(pnCoteMaxRb_, n++);
    pnCoteMaxCtr_.add(new BuLabel("      "+getS("Point de contr�le")+": "), n++);
    pnCoteMaxCtr_.add(pnPtsControle_, n++);

    //pnCritereArretRb_
    n= 0;
    pnCritereArretRb_.add(pnTpsMax_, n++);
    pnCritereArretRb_.add(pnNbPasTps_, n++);
    pnCritereArretRb_.add(pnCoteMaxCtr_,n++);
    Dimension dimPn= pnCritereArretRb_.getPreferredSize();
    BuLabel lbCritereArret= new BuLabel(getS("Crit�re d'arr�t du calcul ?"));
    Dimension dimLb= lbCritereArret.getPreferredSize();
    dimLb.height= dimPn.height;
    lbCritereArret.setPreferredSize(dimLb);
    n= 0;
    pnCritereArret_.add(lbCritereArret, n++);
    pnCritereArret_.add(pnCritereArretRb_, n++);
    n= 0;
    pnTpsCritere_.add(pnTps_, n++);
    pnTpsCritere_.add(pnCritereArret_, n++);
    cbPasTpsVariable_=
      new BuCheckBox(getS("Pas de temps variable suivant le nombre de courant"));
    cbPasTpsVariable_.addActionListener(this);
    cbPasTpsVariable_.setActionCommand("PAS_TPS_VARIABLE");
    //    Dimension dimCb = cbPasTpsVariable_.getPreferredSize();
    tfNbCourant_= BuTextField.createDoubleField();
    tfNbCourant_.setColumns(textSize);
    tfNbCourant_.setEditable(true);
    n= 0;
    pnNbCourant_.add(cbPasTpsVariable_, n++);
    pnTfNbCourant_ = new BuPanel(new BuHorizontalLayout(5));
    pnTfNbCourant_.add(new BuLabel(getS("Nombre de courant souhait�")));
    pnTfNbCourant_.add(tfNbCourant_);
    pnNbCourant_.add(pnTfNbCourant_, n++);
    //    Dimension dimPnNbCourant = pnNbCourant_.getPreferredSize();
    //    dimCb.height = dimPnNbCourant.height;
    //    cbPasTpsVariable_.setPreferredSize(dimCb);
    n= 0;
    pnParamTemporels_.add(pnTpsCritere_, n++);
    pnParamTemporels_.add(pnNbCourant_, n++);
    pnMain_.add(pnParamTemporels_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
        String message  = validationDonnees();
      if (message.isEmpty()) {
          if (getValeurs()) {
            firePropertyChange("parametresTemporels", null, param_);
          }
          fermer();
      } else {
          int resp = new Hydraulique1dDialogContraintes(
                  (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                  ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                  .getInformationsSoftware(),
                  getS("Contraintes non v�rifi�es")+": \n"
                  + message
                  + "\n\n", true)
                     .activate();
          if (resp == Hydraulique1dDialogContraintes.IGNORER){
              if (getValeurs()) {
                  firePropertyChange("parametresTemporels", null, param_);
              }
           fermer();

          }
      }

    } else if ("TEMPS_MAX".equals(cmd)) {
        tfNbPasTps_.setEnabled(false);
        tfTpsMax_.setEnabled(true);
        if (!paramGeneraux_.noyauV5P2()) {
            CtuluLibSwing.griserPanel(pnCoteMaxCtr_, false);
        	  rbCoteMaxCtr_.setEnabled(true);
          }
      tfTpsMax_.setEnabled(true);
      if (tfNbPasTps_.getValue() == null) {
        tfNbPasTps_.setValue(new Integer(10));
      }
      tfNbPasTps_.setEnabled(false);
    } else if ("NB_PAS_TEMPS".equals(cmd)) {
        tfNbPasTps_.setEnabled(true);
        tfTpsMax_.setEnabled(false);
        if (!paramGeneraux_.noyauV5P2()) {
          CtuluLibSwing.griserPanel(pnCoteMaxCtr_, false);
      	  rbCoteMaxCtr_.setEnabled(true);
        }
      if (tfTpsMax_.getValue() == null) {
        tfTpsMax_.setValue(new Double(10));
      }
      tfTpsMax_.setEnabled(false);
      tfNbPasTps_.setEnabled(true);
    } else if ("COTE_MAX".equals(cmd)) {
        tfNbPasTps_.setEnabled(false);
        tfTpsMax_.setEnabled(false);
        CtuluLibSwing.griserPanel(pnCoteMaxCtr_, true);
        tfNbPasTps_.setValue(new Integer(1000000));
    } else if ("PAS_TPS_VARIABLE".equals(cmd)) {
      if (cbPasTpsVariable_.isSelected())
       CtuluLibSwing.griserPanel(pnTfNbCourant_, true);
      else {
        if (tfNbCourant_.getValue() == null) {
          tfNbCourant_.setValue(new Double(0.9));
        }
       CtuluLibSwing.griserPanel(pnTfNbCourant_, false);
      }

    } else {
      super.actionPerformed(_evt);
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    String message ="";
    try {
      double tInit= ((Double)tfTpsInitial_.getValue()).doubleValue();
      if (tInit != param_.tempsInitial()) {
        param_.tempsInitial(tInit);
        changed= true;
      }
      double pasTps= ((Double)tfPasTps_.getValue()).doubleValue();
      if (pasTps != param_.pasTemps()) {
        param_.pasTemps(pasTps);
        changed= true;
      }
      double tpsMax= ((Double)tfTpsMax_.getValue()).doubleValue();
      if (tpsMax != param_.tempsFinal()) {
        param_.tempsFinal(tpsMax);
        changed= true;
      }
      int nbPas= ((Integer)tfNbPasTps_.getValue()).intValue();
      if (nbPas != param_.nbPasTemps()) {
        param_.nbPasTemps(nbPas);
        changed= true;
      }
      double nbCourant= ((Double)tfNbCourant_.getValue()).doubleValue();
      if (nbCourant != param_.nbCourant()) {
        param_.nbCourant(nbCourant);
        changed= true;
      }
      boolean pasTpsVar= cbPasTpsVariable_.isSelected();
      if (pasTpsVar != param_.pasTempsVariable()) {
        param_.pasTempsVariable(pasTpsVar);
        changed= true;
      }
     double coteMax= ((Double)  tfCoteMaxCtr_.getValue()).doubleValue();
      if (coteMax != param_.coteMax()) {
          param_.coteMax(coteMax);
          changed = true;
      }
      int biefControle = ((Integer) tfBiefPtsCtr_.getValue()).intValue();
      if (biefControle != param_.biefControle()) {
          param_.biefControle(biefControle);
          changed = true;
      }

      double absControle = ((Double) tfAbsPtsCtr_.getValue()).doubleValue();
      if (absControle != param_.abscisseControle()) {
          param_.abscisseControle(absControle);
          changed = true;
      }
      EnumMetierCritereArret critereArret;
      if (rbTpsMax_.isSelected())
        critereArret= EnumMetierCritereArret.TEMPS_MAX;
      else if (rbNbPasTps_.isSelected())
        critereArret= EnumMetierCritereArret.NB_PAS_TEMPS;
      else
          critereArret= EnumMetierCritereArret.COTE_MAX;
      if (critereArret.value() != param_.critereArret().value()) {
        param_.critereArret(critereArret);
        changed= true;
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }
    if (message!="") {
        message=getS("Attention !")+"\n"+message;
        CtuluLibDialog.showWarn(pnParamTemporels_, getS("Erreur"), message);
    }

    return changed;
  }

  protected String validationDonnees() {
    String message ="";
    boolean biefOK;
    try {
        if (rbCoteMaxCtr_.isSelected()){
            int biefControle = ((Integer) tfBiefPtsCtr_.getValue()).intValue();

            if ((biefControle < (paramReseau_.biefs().length + 1))) {
                biefOK = true;
            } else {
                message +=getS("Le num�ro du bief de contr�le indiqu� n'existe pas !");
                message +="\n";
                biefOK = false;
            }

            double absControle = ((Double) tfAbsPtsCtr_.getValue()).doubleValue();
            if (!(biefOK &&
                  paramReseau_.biefs()[biefControle -
                  1].contientAbscisse(absControle))) {
                message += getS("L'abscisse de contr�le indiqu�e n'existe pas !")+"\n";
            }
        }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
    }
    return message;
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    //toujours faux a distance utiliser _is_a
    if (_n instanceof MetierReseau)
        paramReseau_ = (MetierReseau)_n;
    else if (_n instanceof MetierParametresGeneraux)
      paramGeneraux_= (MetierParametresGeneraux)_n;
    else if (_n instanceof MetierParametresTemporels) {
      MetierParametresTemporels param= (MetierParametresTemporels)_n;
      param_= param;
      setValeurs();
    }
  }
  @Override
  protected void setValeurs() {
    tfNbCourant_.setValue(new Double(param_.nbCourant()));
    tfNbPasTps_.setValue(new Integer(param_.nbPasTemps()));
    tfPasTps_.setValue(new Double(param_.pasTemps()));
    tfTpsInitial_.setValue(new Double(param_.tempsInitial()));
    tfTpsMax_.setValue(new Double(param_.tempsFinal()));
    tfCoteMaxCtr_.setValue(new Double(param_.coteMax()));
    tfAbsPtsCtr_.setValue(new Double(param_.abscisseControle()));
    tfBiefPtsCtr_.setValue(new Integer(param_.biefControle()));

    if (paramGeneraux_.noyauV5P2()) CtuluLibSwing.griserPanel(pnCoteMaxCtr_,false);
    else CtuluLibSwing.griserPanel(pnCoteMaxCtr_,true);

    if (param_.pasTempsVariable()) {
      cbPasTpsVariable_.setSelected(true);
      tfNbCourant_.setEnabled(true);
    } else {
      cbPasTpsVariable_.setSelected(false);
      tfNbCourant_.setEnabled(false);
    }
    if (param_.critereArret().value() == EnumMetierCritereArret._NB_PAS_TEMPS) {
      rbNbPasTps_.setSelected(true);
      tfTpsMax_.setEnabled(false);
      tfNbPasTps_.setEnabled(true);
      if (!paramGeneraux_.noyauV5P2()) {
          CtuluLibSwing.griserPanel(pnCoteMaxCtr_, false);
    	  rbCoteMaxCtr_.setEnabled(true);
      }
    } else if (param_.critereArret().value() == EnumMetierCritereArret._TEMPS_MAX){
      rbTpsMax_.setSelected(true);
      tfNbPasTps_.setEnabled(false);
      tfTpsMax_.setEnabled(true);
      if (!paramGeneraux_.noyauV5P2()) {
	      CtuluLibSwing.griserPanel(pnCoteMaxCtr_, false);
	      rbCoteMaxCtr_.setEnabled(true);
      }
    } else { //cas cote_max
    	if (!paramGeneraux_.noyauV5P2()) {
	        rbCoteMaxCtr_.setSelected(true);
	        tfNbPasTps_.setEnabled(false);
	        tfTpsMax_.setEnabled(false);
	        CtuluLibSwing.griserPanel(pnCoteMaxCtr_, true);
    	}else{
    	      rbNbPasTps_.setSelected(true);
    	      tfTpsMax_.setEnabled(false);
    	      tfNbPasTps_.setEnabled(true);
    	      CtuluLibSwing.griserPanel(pnCoteMaxCtr_, false);
    	      rbCoteMaxCtr_.setEnabled(false);
    	}
    }
    if (paramGeneraux_.regime().value() != EnumMetierRegime._TRANSCRITIQUE) {
      CtuluLibSwing.griserPanel(pnNbCourant_, false);
    } else {
      CtuluLibSwing.griserPanel(pnNbCourant_, true);
      tfNbCourant_.setEnabled(cbPasTpsVariable_.isSelected());
    }

  }


}
