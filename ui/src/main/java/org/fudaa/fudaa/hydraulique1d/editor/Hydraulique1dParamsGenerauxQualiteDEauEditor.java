/*
 * @file         Hydraulique1dParamsGenerauxQualiteDEauEditor.java
 * @creation     2006-14-06
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JScrollPane;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierModeleQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierOptionConvec;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierOptionDiffus;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresConvecDiffu;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresGenerauxQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresQualiteDEau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauOptionsTraceurModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;



/**
 * Editeur des param�tres generaux de la qualite d'eau (MetierParametresGenerauxQualiteDEau).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Param�tres Temporels".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PARAM_TEMPOREL().editer().<br>
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:42:45 $ by $Author: bmarchan $
 * @author       Olivier Pasteur
 */
public class Hydraulique1dParamsGenerauxQualiteDEauEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  BuPanel pnParamsQualiteDEau_, pnFreqCouplage_, pnTabDiffConv_, pnMethConv, pnMethDiff_,pnCoeffDiff1_,pnCoeffDiff2_,pnBoutons;
  BuPanel pnParamsMethConv_, pnChoixMethConv_,pnChoixOrdreSchema_, pnChoixLimitPente_, pnChoixMethDiff_,pnParamW_,pnOrdreSchema_,pnLimitPente_,pnContainerMethConv_;
  BuHorizontalLayout loHorizontal_,loHorizontal1_;
  BuVerticalLayout loVertical_;
  GridLayout loGrid_;
  FlowLayout loFlowCenter_,loFlowLeft_;
  BuTextField tfFreqCouplage_, tfParamW_, tfCoeffDiff1_, tfCoeffDiff2_;
  BuRadioButton rbHYP1FAfnc_, rbHYP1FAfc_, rbVF_, rbOrdre1_, rbOrdre2_,rbOrdre3_,rbLimitPenteOui_, rbLimitPenteNon_;
  BuRadioButton rbKC1UC2_, rbElder_, rbFisher_, rbLiu_, rbIsawaAga_, rbMcQuiveyKeefer_, rbKashefipurFalconer_;
  BuRadioButton rbMagazineAl_, rbKoussisRodriguez_, rbSeoCheong_, rbDeng_;
  ButtonGroup bcMethConv_, bcOrdreSchema_, bcLimitPente_, bcMethDiff_;
  BuButton btParamsPhys_, btMeteo_;
  Hydraulique1dTableau tabOptionTraceur_;

  private MetierParametresGenerauxQualiteDEau paramGenerauxQualiteDEau_;
  private MetierParametresQualiteDEau qualiteDEau_;

  public Hydraulique1dParamsGenerauxQualiteDEauEditor() {
    this(null);
  }
  public Hydraulique1dParamsGenerauxQualiteDEauEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition des param�tres g�n�raux de la qualit� d'eau"));
    paramGenerauxQualiteDEau_= null;
    qualiteDEau_= null;
    loVertical_= new BuVerticalLayout(5, true, true);
    loHorizontal_= new BuHorizontalLayout(5, true, true);
    loHorizontal1_= new BuHorizontalLayout(5,false , false);
    loGrid_ = new GridLayout(6,2,1,1);
    loFlowCenter_=new FlowLayout(FlowLayout.CENTER,20,20);
    loFlowLeft_=new FlowLayout(FlowLayout.LEFT,20,20);
    Container pnMain_= getContentPane();


    pnParamsQualiteDEau_= new BuPanel();
    pnParamsQualiteDEau_.setLayout(loVertical_);
    pnFreqCouplage_= new BuPanel();
    pnFreqCouplage_.setLayout(loHorizontal_);
    pnTabDiffConv_ = new BuPanel();
    pnTabDiffConv_.setLayout(loHorizontal_);
    pnContainerMethConv_= new BuPanel();
    pnContainerMethConv_.setLayout(loHorizontal_);
    pnMethConv = new BuPanel();
    pnMethConv.setLayout(loFlowLeft_);
    pnChoixMethConv_ = new BuPanel();
    pnChoixMethConv_.setLayout(loVertical_);
    pnChoixMethConv_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnParamsMethConv_= new BuPanel();
    pnParamsMethConv_.setLayout(loVertical_);
    pnParamsMethConv_.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    pnParamW_ = new BuPanel();
    pnParamW_.setLayout(loHorizontal_);
    pnChoixOrdreSchema_ = new BuPanel();
    pnChoixOrdreSchema_.setLayout(loHorizontal_);
    pnOrdreSchema_ = new BuPanel();
    pnOrdreSchema_.setLayout(loVertical_);
    pnOrdreSchema_.setBorder(
            new LineChoiceBorder(false, false, false, true, false, false));
    pnChoixLimitPente_ = new BuPanel();
    pnChoixLimitPente_.setLayout(loHorizontal_);
    pnLimitPente_ = new BuPanel();
    pnLimitPente_.setLayout(loVertical_);
    pnLimitPente_.setBorder(
            new LineChoiceBorder(false, false, false, true, false, false));
    pnMethDiff_= new BuPanel();
    pnMethDiff_.setLayout(loHorizontal_);
    pnChoixMethDiff_= new BuPanel();
    pnChoixMethDiff_.setLayout(loGrid_);
    pnChoixMethDiff_.setBorder(new LineChoiceBorder(false, false, false, true, false, false));
    pnCoeffDiff1_= new BuPanel();
    pnCoeffDiff1_.setLayout(loHorizontal_);
    pnCoeffDiff2_= new BuPanel();
    pnCoeffDiff2_.setLayout(loHorizontal_);
    pnBoutons= new BuPanel();
    pnBoutons.setLayout(loFlowCenter_);

    bcMethConv_ = new ButtonGroup();
    bcOrdreSchema_ = new ButtonGroup();
    bcLimitPente_ = new ButtonGroup();
    bcMethDiff_ = new ButtonGroup();

    int n= 0;
    int textSize= 7;

    //pnFreqCouplage_
    pnFreqCouplage_.add(new BuLabel(getS("Fr�quence de couplage")));
    tfFreqCouplage_= BuTextField.createIntegerField();
    tfFreqCouplage_.setColumns(textSize);
    tfFreqCouplage_.setEditable(true);
    pnFreqCouplage_.add(tfFreqCouplage_);

    //pnTabDiffConv_
    Hydraulique1dTableauOptionsTraceurModel model = new
                Hydraulique1dTableauOptionsTraceurModel();
    tabOptionTraceur_ = new Hydraulique1dTableau(model);
    JScrollPane sp =new JScrollPane(tabOptionTraceur_);
    sp.setPreferredSize(new Dimension(400,150));
    pnTabDiffConv_.add(sp);


    //pnChoixMethConv_
    rbHYP1FAfnc_= new BuRadioButton("HYP1FA fnc");
    rbHYP1FAfnc_.setToolTipText(getS("Caract�ristiques en convection faible - HYP1FA forme non conservative"));
    rbHYP1FAfnc_.addActionListener(this);
    rbHYP1FAfnc_.setActionCommand("HYP1FAfnc");
    rbHYP1FAfc_= new BuRadioButton("HYP1FA fc");
    rbHYP1FAfc_.setToolTipText(getS("Caract�ristiques en convection faible - HYP1FA forme conservative"));
    rbHYP1FAfc_.addActionListener(this);
    rbHYP1FAfc_.setActionCommand("HYP1FAfc");
    rbVF_= new BuRadioButton(getS("VF"));
    rbHYP1FAfc_.setToolTipText(getS("Volumes finis"));
    rbVF_.addActionListener(this);
    rbVF_.setActionCommand("VF");
    bcMethConv_.add(rbHYP1FAfnc_);
    bcMethConv_.add(rbHYP1FAfc_);
    bcMethConv_.add(rbVF_);
    pnChoixMethConv_.add(rbHYP1FAfnc_);
    pnChoixMethConv_.add(rbHYP1FAfc_);
    pnChoixMethConv_.add(rbVF_);

    //pnParamW_
    pnParamW_.add(new BuLabel(getS("Param�tre W")));
    tfParamW_ = BuTextField.createDoubleField();
    tfParamW_.setColumns(textSize);
    tfParamW_.setEditable(true);
    pnParamW_.add(tfParamW_);

    //pnOrdreSchema_
    rbOrdre1_ = new BuRadioButton("1");
    rbOrdre1_.addActionListener(this);
    rbOrdre1_.setActionCommand("ORDRE_1");
    rbOrdre2_ = new BuRadioButton("2");
    rbOrdre2_.addActionListener(this);
    rbOrdre2_.setActionCommand("ORDRE_2");
    rbOrdre3_ = new BuRadioButton("3");
    rbOrdre3_.addActionListener(this);
    rbOrdre3_.setActionCommand("ORDRE_3");
    rbOrdre3_.setVisible(false);
    bcOrdreSchema_.add(rbOrdre1_);
    bcOrdreSchema_.add(rbOrdre2_);
    bcOrdreSchema_.add(rbOrdre3_);
    pnOrdreSchema_.add(rbOrdre1_);
    pnOrdreSchema_.add(rbOrdre2_);
    pnOrdreSchema_.add(rbOrdre3_);

    //pnChoixOrdreSchema_
    pnChoixOrdreSchema_.add(new BuLabel(getS("Ordre du schema")));
    pnChoixOrdreSchema_.add(pnOrdreSchema_);

    //pnLimitPente_
    rbLimitPenteOui_ = new BuRadioButton(getS("Oui"));
    rbLimitPenteOui_.addActionListener(this);
    rbLimitPenteOui_.setActionCommand("LIMITEUR_OUI");
    rbLimitPenteNon_ = new BuRadioButton(getS("Non"));
    rbLimitPenteNon_.addActionListener(this);
    rbLimitPenteNon_.setActionCommand("LIMITEUR_NON");
    bcLimitPente_.add(rbLimitPenteOui_);
    bcLimitPente_.add(rbLimitPenteNon_);
    pnLimitPente_.add(rbLimitPenteOui_);
    pnLimitPente_.add(rbLimitPenteNon_);

    //pnChoixLimitPente_
    pnChoixLimitPente_.add(new BuLabel(getS("Limiteur de pente")));
    pnChoixLimitPente_.add(pnLimitPente_);

    //pnParamsMethConv_
    pnParamsMethConv_.add(pnChoixOrdreSchema_);
    pnParamsMethConv_.add(pnChoixLimitPente_);
    pnParamsMethConv_.add(pnParamW_);


    //pnMethConv
    n= 0;
    pnMethConv.add(new BuLabel(getS("M�thode de convection")), n++);
    pnMethConv.add(pnChoixMethConv_, n++);

    //pnContainerMethConv_
    pnContainerMethConv_.add(pnMethConv);
    pnContainerMethConv_.add(pnParamsMethConv_);

    //pnMethDiff
    /**
    * les choix sont :
    *     1 : Coefficient de diffusion constant
    *     2 : Elder (1959)
    *     3 : Fisher (1975)
    *     4 : Liu (1977)
    *     5 : Iwasa et Aya (1991)
    *     6 : McQuivey et Keefer (1974)
    *     7 : Kashefipur et Falconer (2002 )
    *     8 : Magazine et al. (1988)
    *     9 : Koussis et Rodriguez-Mirasol (1998)
    *     10: Seo et Cheong (1998)
    *     11: Deng et al. (2001)
    * */
    rbKC1UC2_ = new BuRadioButton(getS("Coef. de diffusion constant")+" :");
    rbKC1UC2_.addActionListener(this);
    rbKC1UC2_.setActionCommand("KC1UC2");
    rbElder_ = new BuRadioButton("Elder (1959)");
    rbElder_.addActionListener(this);
    rbElder_.setActionCommand("Elder");
    rbFisher_ = new BuRadioButton("Fisher (1975)");
    rbFisher_.addActionListener(this);
    rbFisher_.setActionCommand("Fisher");
    rbLiu_ = new BuRadioButton("Liu (1977)");
    rbLiu_.addActionListener(this);
    rbLiu_.setActionCommand("Liu");
    rbIsawaAga_ = new BuRadioButton("Isawa & Aga (1991)");
    rbIsawaAga_.addActionListener(this);
    rbIsawaAga_.setActionCommand("IsawaAga");
    rbMcQuiveyKeefer_ = new BuRadioButton("McQuivey & Keefer (1974)");
    rbMcQuiveyKeefer_.addActionListener(this);
    rbMcQuiveyKeefer_.setActionCommand("McQuiveyKeefer");
    rbKashefipurFalconer_ = new BuRadioButton("Kashefipur & Falconer (2002)");
    rbKashefipurFalconer_.addActionListener(this);
    rbKashefipurFalconer_.setActionCommand("KashefipurFalconer");
    rbMagazineAl_ = new BuRadioButton("Magazine & al. (1988)");
    rbMagazineAl_.addActionListener(this);
    rbMagazineAl_.setActionCommand("MagazineAl");
    rbKoussisRodriguez_ = new BuRadioButton("Koussis & Rodriguez-Mirasol (1998)");
    rbKoussisRodriguez_.addActionListener(this);
    rbKoussisRodriguez_.setActionCommand("KoussisRodriguezMirasol");
    rbSeoCheong_ = new BuRadioButton("Seo & Cheong (1998)");
    rbSeoCheong_.addActionListener(this);
    rbSeoCheong_.setActionCommand("SeoCheong");
    rbDeng_ = new BuRadioButton("Deng & al. (2001)");
    rbDeng_.addActionListener(this);
    rbDeng_.setActionCommand("DengAl");

    bcMethDiff_.add(rbKC1UC2_);
    bcMethDiff_.add(rbElder_);
    bcMethDiff_.add(rbFisher_);
    bcMethDiff_.add(rbLiu_);
    bcMethDiff_.add(rbIsawaAga_);
    bcMethDiff_.add(rbMcQuiveyKeefer_);
    bcMethDiff_.add(rbKashefipurFalconer_);
    bcMethDiff_.add(rbMagazineAl_);
    bcMethDiff_.add(rbKoussisRodriguez_);
    bcMethDiff_.add(rbSeoCheong_);
    bcMethDiff_.add(rbDeng_);

    pnChoixMethDiff_.add(rbKC1UC2_);
    tfCoeffDiff2_ = BuTextField.createDoubleField();
    tfCoeffDiff2_.setEditable(true);
    pnChoixMethDiff_.add(tfCoeffDiff2_);
    pnChoixMethDiff_.add(rbElder_);
    pnChoixMethDiff_.add(rbFisher_);
    pnChoixMethDiff_.add(rbLiu_);
    pnChoixMethDiff_.add(rbIsawaAga_);
    pnChoixMethDiff_.add(rbMcQuiveyKeefer_);
    pnChoixMethDiff_.add(rbKashefipurFalconer_);
    pnChoixMethDiff_.add(rbMagazineAl_);
    pnChoixMethDiff_.add(rbKoussisRodriguez_);
    pnChoixMethDiff_.add(rbSeoCheong_);
    pnChoixMethDiff_.add(rbDeng_);

    //pnMethDiff_
    n = 0;
    pnMethDiff_.add(new BuLabel(getS("M�thode de diffusion")+"  "), n++);
    BuPanel pnContentPnChoixMethDiff = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    pnContentPnChoixMethDiff.add(pnChoixMethDiff_);
    pnMethDiff_.add(pnContentPnChoixMethDiff, n++);



    //pnCoeffDiff1_
    tfCoeffDiff1_= BuTextField.createDoubleField(); // ne sert plus � rien

    //pnBoutons
    btParamsPhys_ = new BuButton(getS("Param�tres Physiques"));
    btParamsPhys_.addActionListener(this);
    btParamsPhys_.setActionCommand("PARAMPHY");
    btMeteo_ = new BuButton("  "+getS("Param�tres M�t�o")+"  ");
    btMeteo_.addActionListener(this);
    btMeteo_.setActionCommand("METEO");
    pnBoutons.add(btParamsPhys_);
    pnBoutons.add(btMeteo_);



    n= 0;
    pnParamsQualiteDEau_.add(pnFreqCouplage_, n++);
    pnParamsQualiteDEau_.add(pnTabDiffConv_, n++);
    pnParamsQualiteDEau_.add(pnContainerMethConv_, n++);
    pnParamsQualiteDEau_.add(pnMethDiff_, n++);
    pnParamsQualiteDEau_.add(pnBoutons, n++);


    pnMain_.add(new JScrollPane(pnParamsQualiteDEau_), BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }



  @Override
  public void actionPerformed(ActionEvent _evt) {
      String cmd= _evt.getActionCommand();
      if ("VALIDER".equals(cmd)) {
          if (getValeurs()) {
              firePropertyChange("parametresGenerauxQualiteDEau", null,
                                 paramGenerauxQualiteDEau_);
          }
          fermer();
      }
      else if ("HYP1FAfnc".equals(cmd)) {
          CtuluLibSwing.griserPanel(pnChoixOrdreSchema_, false);
          CtuluLibSwing.griserPanel(pnParamW_, false);
          CtuluLibSwing.griserPanel(pnChoixLimitPente_, false);
      } else if ("HYP1FAfc".equals(cmd)) {
          CtuluLibSwing.griserPanel(pnChoixOrdreSchema_, false);
          CtuluLibSwing.griserPanel(pnParamW_, false);
          CtuluLibSwing.griserPanel(pnChoixLimitPente_, false);
      } else if ("VF".equals(cmd)) {
          CtuluLibSwing.griserPanel(pnChoixOrdreSchema_, true);
          if (rbOrdre2_.isSelected()) {
              CtuluLibSwing.griserPanel(pnParamW_, true);
              CtuluLibSwing.griserPanel(pnChoixLimitPente_, true);
          } else if (rbOrdre3_.isSelected()) {
              CtuluLibSwing.griserPanel(pnParamW_, false);
              CtuluLibSwing.griserPanel(pnChoixLimitPente_, true);
          }
      } else if ("ORDRE_1".equals(cmd)) {
          CtuluLibSwing.griserPanel(pnParamW_, false);
          CtuluLibSwing.griserPanel(pnChoixLimitPente_, false);
      } else if ("ORDRE_2".equals(cmd)) {
          CtuluLibSwing.griserPanel(pnParamW_, true);
          CtuluLibSwing.griserPanel(pnChoixLimitPente_, true);
      } else if ("ORDRE_3".equals(cmd)) {
          CtuluLibSwing.griserPanel(pnParamW_, false);
          CtuluLibSwing.griserPanel(pnChoixLimitPente_, true);
      } else if ("LIMITEUR_OUI".equals(cmd)) {
      } else if ("LIMITEUR_NON".equals(cmd)) {
      }
      else if ("KC1UC2".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(true);
      }
      else if ("Elder".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("Fisher".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("Liu".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("IsawaAga".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("McQuiveyKeefer".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("KashefipurFalconer".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("MagazineAl".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("KoussisRodriguezMirasol".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("SeoCheong".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("DengAl".equals(cmd)) {
        tfCoeffDiff2_.setEnabled(false);
      }
      else if ("PARAMPHY".equals(cmd)) {
          Hydraulique1dIHMRepository.getInstance().PARAMS_PHYSIQUES_QE().editer();
      } else if ("METEO".equals(cmd)) {
          Hydraulique1dIHMRepository.getInstance().METEO().editer();
      } else {
          super.actionPerformed(_evt);
      }
  }

  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    try {

        int freq= ((Integer)tfFreqCouplage_.getValue()).intValue();
        if (freq != qualiteDEau_.parametresModeleQualiteEau().frequenceCouplHydroTracer()) {
            qualiteDEau_.parametresModeleQualiteEau().frequenceCouplHydroTracer(freq);
            changed = true;
        }

        //Tableau
        Hydraulique1dTableauOptionsTraceurModel model = (
               Hydraulique1dTableauOptionsTraceurModel) tabOptionTraceur_.getModel();
       if (model.getValeurs()) {
           changed = true;
       }

        //Convection
        MetierParametresConvecDiffu parametresConvecDiffu =qualiteDEau_.parametresGenerauxQualiteDEau().parametresConvecDiffu();

        //optionConvec
        EnumMetierOptionConvec optionConvec;
        if (rbHYP1FAfnc_.isSelected())
            optionConvec = EnumMetierOptionConvec.HYP1FA_NON_CONS;
        else if (rbHYP1FAfc_.isSelected())
            optionConvec = EnumMetierOptionConvec.HYP1FA_CONS;
        else //Vol fini
            optionConvec = EnumMetierOptionConvec.VOLUMES_FINIS;
        if (optionConvec.value() !=
            parametresConvecDiffu.optionConvection().value()) {
            parametresConvecDiffu.optionConvection(optionConvec);
            changed = true;
        }

        //Ordre du schema
        int ordre;
        if (rbOrdre1_.isSelected())
            ordre = 1;
        else if (rbOrdre2_.isSelected())
            ordre = 2;
        else
            ordre = 3;
        if (ordre !=
            parametresConvecDiffu.ordreSchemaConvec()) {
            parametresConvecDiffu.ordreSchemaConvec(ordre);
            changed = true;
        }

        //Limiteur de pente
        boolean limitPente;
        if (rbLimitPenteOui_.isSelected())
            limitPente = true;
        else
            limitPente = false;
        if (limitPente !=
            parametresConvecDiffu.LimitPente()) {
            parametresConvecDiffu.LimitPente(limitPente);
            changed = true;
        }

        double w= ((Double)tfParamW_.getValue()).doubleValue();
        if (w != parametresConvecDiffu.paramW()) {
            parametresConvecDiffu.paramW(w);
            changed = true;
        }



        //Diffusion

        //optionDiffus
        EnumMetierOptionDiffus optionDiffus=null;
        if (rbKC1UC2_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.K_C1U_C2;
        }
        else if (rbElder_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.ELDER;
        }
        else if (rbFisher_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.FISHER;
        }
        else if (rbLiu_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.LIU;
        }
        else if (rbIsawaAga_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.ISAWA_AGA;
        }
        else if (rbMcQuiveyKeefer_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.MC_QUIVEY_KEEFER;
        }
        else if (rbKashefipurFalconer_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.KASHEFIPUR_FALCONER;
        }
        else if (rbMagazineAl_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.MAGAZINE_AL;
        }
        else if (rbKoussisRodriguez_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.KOUSSIS_RODRIGUEZ;
        }
        else if (rbSeoCheong_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.SEO_CHEONG;
        }
        else if (rbDeng_.isSelected()) {
          optionDiffus = EnumMetierOptionDiffus.DENG_AL;
        }
        if (optionDiffus.value() != parametresConvecDiffu.optionCalculDiffusion().value()) {
            parametresConvecDiffu.optionCalculDiffusion(optionDiffus);
            changed = true;
        }

        double coeff2= ((Double)tfCoeffDiff2_.getValue()).doubleValue();
        if (coeff2 != parametresConvecDiffu.coeffDiffusion2()) {
            parametresConvecDiffu.coeffDiffusion2(coeff2);
            changed = true;
        }



    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }

    return changed;
  }



  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierParametresQualiteDEau)
        qualiteDEau_ = (MetierParametresQualiteDEau)_n;
        paramGenerauxQualiteDEau_ = qualiteDEau_.parametresGenerauxQualiteDEau();
        setValeurs();
  }

  @Override
  protected void setValeurs() {
    //frequence
    tfFreqCouplage_.setValue(new Integer(qualiteDEau_.parametresModeleQualiteEau().frequenceCouplHydroTracer()));

    //Parametres Diffusion et Convection
    MetierParametresConvecDiffu paramConvDiff = paramGenerauxQualiteDEau_.parametresConvecDiffu();

    //tableau
    Hydraulique1dTableauOptionsTraceurModel model = (
          Hydraulique1dTableauOptionsTraceurModel) tabOptionTraceur_.getModel();

    /*int nbTraceurs = qualiteDEau_.parametresModeleQualiteEau().nbTraceur();
    if (qualiteDEau_.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers().length!=nbTraceurs){
        MetierOptionTraceur[] newTabOptions = new MetierOptionTraceur[nbTraceurs];
        for (int i = 0; i < newTabOptions.length; i++) {
            newTabOptions[i]= new MetierOptionTraceur();
        }
        if (qualiteDEau_.parametresModeleQualiteEau().modeleQualiteEau().value()==EnumMetierModeleQualiteDEau._MICROPOL){
            newTabOptions[1].convectionDuTraceur(false);
            newTabOptions[1].diffusionDuTraceur(false);
            newTabOptions[4].convectionDuTraceur(false);
            newTabOptions[4].diffusionDuTraceur(false);
        }
        qualiteDEau_.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers(newTabOptions);
    }*/

    model.setValeurs(qualiteDEau_);


    // Convection
    tfParamW_.setValue(new Double(paramConvDiff.paramW()));

    if (paramConvDiff.optionConvection().value() == EnumMetierOptionConvec._HYP1FA_CONS) {
        rbHYP1FAfc_.setSelected(true);
        CtuluLibSwing.griserPanel(pnChoixOrdreSchema_, false);
        CtuluLibSwing.griserPanel(pnParamW_, false);
        CtuluLibSwing.griserPanel(pnChoixLimitPente_, false);
    } else if (paramConvDiff.optionConvection().value() ==
               EnumMetierOptionConvec._HYP1FA_NON_CONS) {
        rbHYP1FAfnc_.setSelected(true);
        CtuluLibSwing.griserPanel(pnChoixOrdreSchema_, false);
        CtuluLibSwing.griserPanel(pnParamW_, false);
        CtuluLibSwing.griserPanel(pnChoixLimitPente_, false);
    } else { //vol fini
        rbVF_.setSelected(true);
        CtuluLibSwing.griserPanel(pnChoixOrdreSchema_, true);
        /*CtuluLib.griserPanel(pnParamW_,false);
         CtuluLib.griserPanel(pnLimitPente_,false); Ces 2 cas sont trait�s avec le radio bouton "ordre"*/
    }

    if (paramConvDiff.ordreSchemaConvec() == 1) {
        rbOrdre1_.setSelected(true);
        CtuluLibSwing.griserPanel(pnParamW_, false);
        CtuluLibSwing.griserPanel(pnChoixLimitPente_, false);
    } else if (paramConvDiff.ordreSchemaConvec() == 2) {
        rbOrdre2_.setSelected(true);
        if (paramConvDiff.optionConvection().value() == EnumMetierOptionConvec._VOLUMES_FINIS){
	        CtuluLibSwing.griserPanel(pnParamW_, true);
	        CtuluLibSwing.griserPanel(pnChoixLimitPente_, true);
        }
    } else if (paramConvDiff.ordreSchemaConvec() == 3) {
        rbOrdre3_.setSelected(true);
        if (paramConvDiff.optionConvection().value() == EnumMetierOptionConvec._VOLUMES_FINIS){
	        CtuluLibSwing.griserPanel(pnParamW_, false);
	        CtuluLibSwing.griserPanel(pnChoixLimitPente_, true);
        }
    } else { //par defaut ordre 1
        rbOrdre1_.setSelected(true);
        CtuluLibSwing.griserPanel(pnParamW_, false);
        CtuluLibSwing.griserPanel(pnChoixLimitPente_, false);
    }

    if (paramConvDiff.LimitPente()) {
        rbLimitPenteOui_.setSelected(true);
    } else {
        rbLimitPenteNon_.setSelected(true);
    }


    //Diffusion
//    tfCoeffDiff1_.setValue(new Double(paramConvDiff.coeffDiffusion1()));
    tfCoeffDiff2_.setValue(new Double(paramConvDiff.coeffDiffusion2()));
    tfCoeffDiff2_.setEnabled(false);
    EnumMetierOptionDiffus optionCalculDiffusion = paramConvDiff.optionCalculDiffusion();
    if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.K_C1U_C2) ) {
      rbKC1UC2_.setSelected(true);
      tfCoeffDiff2_.setEnabled(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.ELDER) ) {
      rbElder_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.FISHER) ) {
      rbFisher_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.LIU) ) {
      rbLiu_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.ISAWA_AGA) ) {
      rbIsawaAga_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.MC_QUIVEY_KEEFER) ) {
      rbMcQuiveyKeefer_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.KASHEFIPUR_FALCONER) ) {
      rbKashefipurFalconer_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.MAGAZINE_AL) ) {
      rbMagazineAl_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.KOUSSIS_RODRIGUEZ) ) {
      rbKoussisRodriguez_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.SEO_CHEONG) ) {
      rbSeoCheong_.setSelected(true);
    }
    else if (optionCalculDiffusion.equals(EnumMetierOptionDiffus.DENG_AL) ) {
      rbDeng_.setSelected(true);
    }

    int modele = qualiteDEau_.parametresModeleQualiteEau().modeleQualiteEau().value();
    if (modele==EnumMetierModeleQualiteDEau._TRANSPORT_PUR){
        btParamsPhys_.setEnabled(false);
        btMeteo_.setEnabled(false);
    }else if(modele==EnumMetierModeleQualiteDEau._BIOMASS || modele==EnumMetierModeleQualiteDEau._EUTRO || modele==EnumMetierModeleQualiteDEau._THERMIC){
        btParamsPhys_.setEnabled(true);
        btMeteo_.setEnabled(true);
    }else{
        btParamsPhys_.setEnabled(true);
        btMeteo_.setEnabled(false);
    }
  }


}
