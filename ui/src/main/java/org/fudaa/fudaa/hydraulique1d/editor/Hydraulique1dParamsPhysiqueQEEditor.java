/*
 * @file         Hydraulique1dParamsPhysiqueQEEditor.java
 * @creation     2006-07-05
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.EnumMetierModeleQualiteDEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParamPhysTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresQualiteDEau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneParamsPhysiqueQETableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauParamsPhysQEModel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des parametres physiques (Qualit� d'eau).<br>
 * Appel� si l'utilisateur clic sur le bouton "Parametres Physiques".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PARAMS_PHYSIQUES_QE().editer().<br>
 * @version      1.0
 * @author       Olivier Pasteur
 */

public class Hydraulique1dParamsPhysiqueQEEditor extends Hydraulique1dCustomizerImprimable
  implements ActionListener {

    private BuPanel pnParamsPhys_, pnBas_;
    private BuBorderLayout loParamsPhys_;
    private BuVerticalLayout loBas_;
    private BuScrollPane spParamsPhys_;
    private BuButton btAjouterSeuil_;
    private BuButton btSupprimerSeuil_;
    private Hydraulique1dTableauReel tabParamsPhys_;
    private MetierParamPhysTracer[] paramsPhys_;
    private MetierParametresQualiteDEau qualiteDEau_;
//    private MetierParametresModeleQualiteEau modeleQE_;

    private int nbSeuil_;



  public Hydraulique1dParamsPhysiqueQEEditor() {
    this(null);
  }
  public Hydraulique1dParamsPhysiqueQEEditor(BDialogContent parent) {
      super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Param�tres physiques"));
     paramsPhys_ = null;
//     modeleQE_ = null;
     loBas_ = new BuVerticalLayout(5, true, true);
     loParamsPhys_ = new BuBorderLayout(5, 5);
     Container pnMain_ = getContentPane();
     pnBas_ = new BuPanel();
     pnBas_.setLayout(loBas_);
     pnParamsPhys_ = new BuPanel();
     pnParamsPhys_.setLayout(loParamsPhys_);
     pnParamsPhys_.setBorder(
             new CompoundBorder(
                     new EtchedBorder(),
                     new EmptyBorder(new Insets(5, 5, 5, 5))));
     Hydraulique1dTableauParamsPhysQEModel model = new
             Hydraulique1dTableauParamsPhysQEModel();

     tabParamsPhys_ = new Hydraulique1dTableauReel(model);
     spParamsPhys_ = new BuScrollPane(tabParamsPhys_);

     spParamsPhys_.setBorder(
             new CompoundBorder(
                     new EtchedBorder(),
                     new EmptyBorder(new Insets(5, 5, 5, 5))));
     setActionPanel(
             EbliPreferences.DIALOG.IMPORTER
             | EbliPreferences.DIALOG.EXPORTER);
     btAjouterSeuil_=(BuButton)addAction(getS("Ajouter un seuil"),"AJOUTER_SEUIL");
     btAjouterSeuil_.setToolTipText(getS("Ajoute les param�tres physiques pour un seuil suppl�mentaire"));

     btSupprimerSeuil_=(BuButton)addAction(getS("Supprimer un seuil"),"SUPPRIMER_SEUIL");
     btSupprimerSeuil_.setToolTipText(getS("Supprime les param�tres physiques du dernier seuil"));



     pnParamsPhys_.add(spParamsPhys_, BorderLayout.CENTER);
     pnParamsPhys_.add(pnBas_, BorderLayout.SOUTH);
     pnMain_.add(pnParamsPhys_, BorderLayout.CENTER);
     setNavPanel(EbliPreferences.DIALOG.VALIDER |
                 EbliPreferences.DIALOG.ANNULER);
     pack();

  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
      String cmd = _evt.getActionCommand();
      if ("VALIDER".equals(cmd)) {
          if (getValeurs()) {
              firePropertyChange("Param�tres physiques", null, paramsPhys_);
          }
          fermer();
      } else if ("IMPORTER".equals(cmd)) {
          importer();
      } else if ("EXPORTER".equals(cmd)) {
          exporter();
      } else if ("AJOUTER_SEUIL".equals(cmd)) {
          ajouterSeuil();
      } else if ("SUPPRIMER_SEUIL".equals(cmd)) {
          supprimerSeuil();
      } else {
          super.actionPerformed(_evt);
        }
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierParametresQualiteDEau))  return;
    qualiteDEau_= (MetierParametresQualiteDEau)_n;
    paramsPhys_ = qualiteDEau_.parametresGenerauxQualiteDEau().paramsPhysTracer();
  }

  @Override
  protected boolean getValeurs() {
	  boolean changed = false;
	  //Tableau
	  Hydraulique1dTableauParamsPhysQEModel model = (
			  Hydraulique1dTableauParamsPhysQEModel) tabParamsPhys_.getModel();
	  if (model.getValeurs()) {
		  changed = true;
	  }

	  return changed;
  }



  @Override
  protected void setValeurs() {
      Hydraulique1dTableauParamsPhysQEModel model = (
              Hydraulique1dTableauParamsPhysQEModel) tabParamsPhys_.getModel();

      EnumMetierModeleQualiteDEau modeleQE = qualiteDEau_.parametresModeleQualiteEau().modeleQualiteEau();

      if (modeleQE.value()!=EnumMetierModeleQualiteDEau._EUTRO && modeleQE.value()!=EnumMetierModeleQualiteDEau._O2) {
    	  btAjouterSeuil_.setVisible(false);
    	  btSupprimerSeuil_.setVisible(false);
      } else {
    	  btAjouterSeuil_.setVisible(true);
    	  btSupprimerSeuil_.setVisible(true);
      }


      //necessaire
      paramsPhys_=qualiteDEau_.parametresGenerauxQualiteDEau().paramsPhysTracer();
      nbSeuil_=getNbSeuil();
      model.setValeurs(qualiteDEau_);


  }
  private boolean verificationParametres(MetierParamPhysTracer[] params){
      if (params.length==0/*paramsPhys_.length*/) {
          new BuDialogError(
                    (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                    ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                    .getInformationsSoftware(),
                    getS("ERREUR")+": "+getS("Aucun param�tre physique n'a pu �tre import�")+"*/")
                    .activate();
            return false;

      }else{
          //Autres tests sur les noms ???
          return true;
      }
  }

  private void importer() {
        File file = Hydraulique1dImport.chooseFile("phy");
        if (file == null) {
            return;
        }

        MetierParamPhysTracer[] params = Hydraulique1dImport.importParamsPhysiquesQE(file);

        if ((params == null)) {
            new BuDialogError(
                    (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                    ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                    .getInformationsSoftware(),
                    getS("ERREUR")+": "+getS("l'importation des param�tres physiques")+"\n" +
                    getS("a �chou�."))
                    .activate();
            return;
        }
        if ((params.length == 0)) {
            new BuDialogError(
                    (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                    ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                    .getInformationsSoftware(),
                    getS("ERREUR")+": "+getS("aucun param�tre n'est")+"\n" +
                    getS("disponible dans cet import!"))
                    .activate();
            return;
        }
        if (verificationParametres(params)) {
            Hydraulique1dTableauParamsPhysQEModel model = (
                    Hydraulique1dTableauParamsPhysQEModel) tabParamsPhys_.
                    getModel();
            model.setValeurs(params);
        }
    }

    private void exporter() {

        File fichier = Hydraulique1dExport.chooseFile("phy");

        //Conversion
        if (fichier == null) {
            return;
        }
        Hydraulique1dTableauParamsPhysQEModel model = (
              Hydraulique1dTableauParamsPhysQEModel) tabParamsPhys_.getModel();
        List listePts = model.getListePtsComplets();

        MetierParamPhysTracer[] params = new MetierParamPhysTracer[listePts.
                                         size()];
        Notifieur.getNotifieur().setEventMuet(true);
        for (int i = 0; i < params.length; i++) {
            params[i] = new MetierParamPhysTracer();
            ((Hydraulique1dLigneParamsPhysiqueQETableau) listePts.get(i)).
                    setDParamPhysTracer(params[i]);
        }
        Notifieur.getNotifieur().setEventMuet(false);

        Hydraulique1dExport.exportParamsPhysiquesQE(fichier, params);
    }

    private void supprimerSeuil() {
		if (nbSeuil_!=0){
			Hydraulique1dTableauParamsPhysQEModel model = (
			        Hydraulique1dTableauParamsPhysQEModel) tabParamsPhys_.getModel();
			model.supprimerNDernieresLignes(3);
			nbSeuil_--;

			//Modification du param�tre NOMBRE DE SEUIL
	    	setNbSeuil(nbSeuil_);
		}

    }
    private void ajouterSeuil() {
    	Hydraulique1dTableauParamsPhysQEModel model = (
                Hydraulique1dTableauParamsPhysQEModel) tabParamsPhys_.getModel();
    	nbSeuil_++;
    	model.ajouterLigne(getS("COEFFICIENT A DES FORMULES DE CALCUL DE R POUR LE SEUIL N�")+nbSeuil_,1.2);
    	model.ajouterLigne(getS("COEFFICIENT B DES FORMULES DE CALCUL DE R POUR LE SEUIL N�")+nbSeuil_,0.7);
    	model.ajouterLigne(getS("N� DU SEUIL N�")+nbSeuil_,nbSeuil_);

    	//Modification du param�tre NOMBRE DE SEUIL
    	setNbSeuil(nbSeuil_);


    }

    //Cette fonction r�cup�re
    private int getNbSeuil() {
    	if (paramsPhys_==null) return 1;
    	 for (int i=0;i<paramsPhys_.length;i++){
    		 if (paramsPhys_[i].nomParamPhys().equalsIgnoreCase("NOMBRE DE SEUILS N")||paramsPhys_[i].nomParamPhys().equalsIgnoreCase(getS("NOMBRE DE SEUILS N")))
    			 return ((int) paramsPhys_[i].valeurParamPhys());
    	 }
    	 System.out.println(getS("Erreur le param�tre NOMBRE DE SEUILS N n'a pas �t� trouv�"));
    	 return 1;

    }

    //Cette fonction r�cup�re
    private void setNbSeuil(int nb) {
    	Hydraulique1dTableauParamsPhysQEModel model = (
                Hydraulique1dTableauParamsPhysQEModel) tabParamsPhys_.getModel();
    	        for (int i=0;i<model.getRowCount();i++){
                        String paramPhy = (String)model.getValueAt(i,0);
   	 		if (paramPhy.equalsIgnoreCase("NOMBRE DE SEUILS N")||paramPhy.equalsIgnoreCase(getS("NOMBRE DE SEUILS N"))){
   	 			model.setValueAt((Object)new Double(nb),i,1);
   	 			return;
   	 		}
   	 	}
   	 	System.out.println(getS("Erreur le param�tre NOMBRE DE SEUILS N n'a pas �t� trouv�"));

    }

}
