/*
 * @file         Hydraulique1dPerteChargeEditor.java
 * @creation     2000-11-29
 * @modification $Date: 2007-11-20 11:42:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'une perte de charge locale (MetierPerteCharge).<br>
 * Appeler si l'utilisateur clic sur une singularit� de type "Hydraulique1dReseauPerteCharge".<br>
 * Lancer dans la classe Hydraulique1dReseauMouseAdapter sans utiliser l'ihm helper.<br>
 * @version      $Revision: 1.13 $ $Date: 2007-11-20 11:42:42 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dPerteChargeEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuTextField tfNumero_, tfAbscisse_, tfCoef_, tfNom_;
  private BuLabel lbAbscisse_;
  private BuPanel pnNumero_, pnPerteCharge_, pnAbscisse_, pnNom_;
  private BuPanel pnCoef_;
  private BuVerticalLayout loPerteCharge_;
  private BuHorizontalLayout loNumero_, loAbscisse_, loCoef_,loNom_;
  private MetierPerteCharge perteCharge_;
  private MetierBief bief_;
  public Hydraulique1dPerteChargeEditor() {
    this(null);
  }
  public Hydraulique1dPerteChargeEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition perte de charge singuli�re"));
    perteCharge_= null;
    loNumero_= new BuHorizontalLayout(5, false, false);
    loAbscisse_= new BuHorizontalLayout(5, false, false);
    loNom_= new BuHorizontalLayout(5, false, false);
    loCoef_= new BuHorizontalLayout(5, false, false);
    loPerteCharge_= new BuVerticalLayout(5, false, false);
    Container pnMain_= getContentPane();
    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loNumero_);
    pnAbscisse_= new BuPanel();
    pnAbscisse_.setLayout(loAbscisse_);
    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    pnCoef_= new BuPanel();
    pnCoef_.setLayout(loCoef_);
    pnPerteCharge_= new BuPanel();
    pnPerteCharge_.setLayout(loPerteCharge_);
    pnPerteCharge_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    int textSize= 5;
    BuLabel lbCoef_= new BuLabel(getS("Coefficient"));
    Dimension dimLabel= lbCoef_.getPreferredSize();
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(true);
    BuLabel lbNumero= new BuLabel(getS("N� perte"));
    lbNumero.setPreferredSize(dimLabel);
    pnNumero_.add(lbNumero, 0);
    pnNumero_.add(tfNumero_, 1);
    textSize= 10;
    tfAbscisse_= BuTextField.createDoubleField();
    tfAbscisse_.setColumns(textSize);
    tfAbscisse_.setEditable(true);
    BuLabel lbAbscisse= new BuLabel(getS("Abscisse"));
    lbAbscisse.setPreferredSize(dimLabel);
    pnAbscisse_.add(lbAbscisse, 0);
    pnAbscisse_.add(tfAbscisse_, 1);
    lbAbscisse_= new BuLabel("   ");
    pnAbscisse_.add(lbAbscisse_, 2);
    tfNom_= new BuTextField();
    //tfNom_.setColumns(textSize);
    tfNom_.setEditable(true);
    BuLabel lbNom = new BuLabel(getS("Nom"));
    lbNom.setPreferredSize(dimLabel);
    pnNom_.add(lbNom, 0);
    pnNom_.add(tfNom_, 1);
    tfCoef_= BuTextField.createDoubleField();
    tfCoef_.setColumns(textSize);
    tfCoef_.setEditable(true);
    pnCoef_.add(lbCoef_, 0);
    pnCoef_.add(tfCoef_, 1);
    pnPerteCharge_.add(pnNumero_, 0);
    pnPerteCharge_.add(pnAbscisse_, 1);
    pnPerteCharge_.add(pnNom_, 2);
    pnPerteCharge_.add(pnCoef_, 3);
    pnMain_.add(pnPerteCharge_, BorderLayout.CENTER);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("ANNULER".equals(cmd)) {
      fermer();
    } else if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("singularite", null, perteCharge_);
      }
      fermer();
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _perte) {
    if (!(_perte instanceof MetierPerteCharge))
      return;
    if (_perte == perteCharge_)
      return;
    perteCharge_= (MetierPerteCharge)_perte;
    setValeurs();
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (perteCharge_ == null)
      return changed;
    int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != perteCharge_.numero()) {
      perteCharge_.numero(nnum);
      changed= true;
    }
    double absc= ((Double)tfAbscisse_.getValue()).doubleValue();
    if (absc != perteCharge_.abscisse()) {
      perteCharge_.abscisse(absc);
      changed= true;
    }
    String nom= tfNom_.getText();
    if (!nom.equals(perteCharge_.nom())) {
      perteCharge_.nom(nom);
      changed = true;
    }
    double coef= ((Double)tfCoef_.getValue()).doubleValue();
    if (coef != perteCharge_.coefficient()) {
      perteCharge_.coefficient(coef);
      changed= true;
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(perteCharge_.numero()));
    tfAbscisse_.setValue(new Double(perteCharge_.abscisse()));
    tfNom_.setValue(perteCharge_.nom());
    tfCoef_.setValue(new Double(perteCharge_.coefficient()));
    String textAbsc= "";
    if (bief_ != null) {
      textAbsc= getS("du bief n�") + (bief_.indice()+1);
      if ((bief_.extrAmont().profilRattache() != null)
        && (bief_.extrAval().profilRattache() != null))
        textAbsc=
          textAbsc
            + getS(" entre ")
            + bief_.extrAmont().profilRattache().abscisse()
            + getS(" et ")
            + bief_.extrAval().profilRattache().abscisse();
      else
        textAbsc= textAbsc + " ("+getS("abscisses des extremit�s inconnues")+")";
    } else
      textAbsc= getS("bief inconnu");
    lbAbscisse_.setText(textAbsc);
  }
  public void setBiefParent(MetierBief _bief) {
    bief_= _bief;
  }
}
