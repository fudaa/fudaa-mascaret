/*
 * @file         Hydraulique1dPlanimetrageEditor.java
 * @creation     1999-09-15
 * @modification $Date: 2007-11-20 11:42:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierZonePlanimetrage;
import org.fudaa.ebli.dialog.BPanneauEditorAction;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneZoneTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauZoneTailleModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des zones de planim�trages (MetierZonePlanimetrage).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/Planim�trage".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().PLANIMETRAGE2().editer().<br>
 * Voir les classes Hydraulique1dTableauZoneEditor.
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.12 $ $Date: 2007-11-20 11:42:50 $ by $Author: bmarchan $
 */
public class Hydraulique1dPlanimetrageEditor
    extends Hydraulique1dTableauZoneEditor {
  private MetierReseau reseau_;
  private BuTextField tfNbPasPlanim_ = BuTextField.createIntegerField();
  private BuCheckBox cbxForcerPasPlanim_ = new BuCheckBox(getS("Forcer le nombre de pas de planimetrage"),false);
  private BuButton btEstimationPasPanim_ = new BuButton(getS("Estimation"));
  public Hydraulique1dPlanimetrageEditor() {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Planimetrage"),
          new Hydraulique1dTableauZoneTailleModel(Hydraulique1dTableauZoneTailleModel.
            MODE_PLANIMETRAGE));
    BuPanel pnSud = new BuPanel(new BuHorizontalLayout(10));
    tfNbPasPlanim_.setColumns(5);
    btEstimationPasPanim_.addActionListener(new ActionListener() {
      @Override
        public void actionPerformed(ActionEvent e) {
            estimationPasPlanim();
        }
    });

    cbxForcerPasPlanim_.addActionListener(new ActionListener() {
      @Override
        public void actionPerformed(ActionEvent e) {
            enableTxtFieldButton();
        }
    });

    pnSud.add(cbxForcerPasPlanim_);
    pnSud.add(tfNbPasPlanim_);
    pnSud.add(btEstimationPasPanim_);
    
    panelButtons.add(pnSud,BorderLayout.NORTH);
    
    panelCentral_.add(panelButtons, BuBorderLayout.SOUTH);
    pack();
  }
  
  BuPanel panelButtons;
  
  public void setActionPanel(final int _mode) {
	  if(panelButtons == null)
		  panelButtons = new BuPanel(new BorderLayout());
	  BPanneauEditorAction pnAction = new BPanneauEditorAction(_mode);
	  pnAction.addActionListener(this);
	  panelButtons.add(pnAction,BorderLayout.SOUTH);
	    
	  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    super.setObject(_n);
    if (_n instanceof MetierReseau) {
        reseau_ = (MetierReseau) _n;
    }

  }

  @Override
  public void setValeurs() {
    modele_.setValeurs();
    if (reseau_.nbPasPlanimetrageImpose()) {
        tfNbPasPlanim_.setValue(new Integer(reseau_.nbPasPlanimetrage()));
    }
    else {
        tfNbPasPlanim_.setValue(new Integer(reseau_.getNbPasPlanimetrage())); // getNbPasPlanimetrage() retourne l'estimation
    }
    cbxForcerPasPlanim_.setSelected(reseau_.nbPasPlanimetrageImpose());
    enableTxtFieldButton();
   }

  @Override
  public boolean getValeurs() {
    boolean modifie = false;
    boolean impose= cbxForcerPasPlanim_.isSelected();
    if (impose != reseau_.nbPasPlanimetrageImpose()) {
        reseau_.nbPasPlanimetrageImpose(impose);
        modifie = true;
    }
    int nbPas = ((Integer)tfNbPasPlanim_.getValue()).intValue();
    if (nbPas != reseau_.nbPasPlanimetrage()) {
        reseau_.nbPasPlanimetrage(nbPas);
        modifie = true;
    }
    boolean modifTab =  modele_.getValeurs();
    if (modifie || modifTab) return true;
    else return false;
  }

  private void estimationPasPlanim() {
	  List lstPoints = modele_.getListePtsComplets();

	  //ON doit construire metierZonePlanimetrages car celui de l'objet m�tier n'est pas � jour
	   MetierZonePlanimetrage[] metierZonePlanimetrages = new MetierZonePlanimetrage[lstPoints.size()];
	  int i = 0;
	  for (Iterator iter = lstPoints.iterator(); iter.hasNext();) {

		  Hydraulique1dLigneZoneTableau element = (Hydraulique1dLigneZoneTableau) iter.next();
		  metierZonePlanimetrages[i]=new MetierZonePlanimetrage();
		  metierZonePlanimetrages[i].biefRattache(reseau_.getBiefNumero(element.iBief()));
		  metierZonePlanimetrages[i].abscisseDebut(element.abscDebut());
		  metierZonePlanimetrages[i].abscisseFin(element.abscFin());
		  metierZonePlanimetrages[i].taillePas(element.getValue(2).doubleValue());


		  /*System.err.println(reseau_.getBiefNumero(element.iBief()));
		  System.err.println(element.abscDebut());
	      System.err.println(element.abscFin());
		  System.err.println(element.getValue(2).doubleValue());*/

		  i++;
	}
	  System.err.println();
	  int res = reseau_.getNbPasPlanimetrage(metierZonePlanimetrages);
      tfNbPasPlanim_.setValue(new Integer(res));
      System.err.println("plani"+res);
  }

  private void enableTxtFieldButton() {
      boolean impose= cbxForcerPasPlanim_.isSelected();
      tfNbPasPlanim_.setEnabled(impose);
      btEstimationPasPanim_.setEnabled(impose);
  }
}
