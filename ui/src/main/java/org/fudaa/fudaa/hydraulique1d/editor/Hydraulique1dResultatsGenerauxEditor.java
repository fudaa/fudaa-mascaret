/*
 * @file         Hydraulique1dResultatsGenerauxEditor.java
 * @creation     2001-09-25
 * @modification $Date: 2008-03-04 15:46:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsGeneraux;
import org.fudaa.ebli.commun.EbliListModelLargeString;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.CGlobal;

import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des r�sultats g�n�raux d'une �tude (DResultatsGeneraux).<br>
 * Appeler si l'utilisateur clic sur le menu "Mascaret/R�sultats G�n�raux".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().RESULTATS_GENERAUX().editer().<br>
 * @version      $Revision: 1.21 $ $Date: 2008-03-04 15:46:52 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public final class Hydraulique1dResultatsGenerauxEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  BuTabbedPane tbpResultatsGeneraux_;
  BuScrollPane scrlpAvertissments_,
  	scrlpEcran_,
    scrlpDamocle_,
    scrlpListing_,
    scrlpCasier_,
    scrlpLiaison_,
    scrlpTracer_;
  JList txtaDamocle_, txtaListing_, txtaCasier_, txtaLiaison_,txtaTracer_;
  JTextPane txtaEcran_,txtaAvertissements_;
  private MetierResultatsGeneraux param_;
  public Hydraulique1dResultatsGenerauxEditor() {
    this(null);
  }
  public Hydraulique1dResultatsGenerauxEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Resultats Generaux"));
    param_= null;
    tbpResultatsGeneraux_= new BuTabbedPane();
    tbpResultatsGeneraux_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));

//     Font fontNew= new Font("Monospaced", Font.PLAIN, 10);
    Font fontNew= new Font("DialogInput", Font.PLAIN, 12);
    txtaEcran_= new JTextPane();
    txtaEcran_.setEditable(false);
    txtaEcran_.setFont(fontNew);
    StyledDocument doc = txtaEcran_.getStyledDocument();
    Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
    Style normal = doc.addStyle("normal", def);
    StyleConstants.setFontFamily(def,"DialogInput");

    txtaAvertissements_= new JTextPane();
    txtaAvertissements_.setEditable(false);
    txtaAvertissements_.setFont(fontNew);

    Style s = doc.addStyle("erreur", normal);
    StyleConstants.setForeground(s,Color.red);

    scrlpEcran_= new BuScrollPane(txtaEcran_);

    scrlpAvertissments_= new BuScrollPane(txtaAvertissements_);

    txtaDamocle_= new JList();
//    txtaDamocle_.setEditable(false);
    txtaDamocle_.setFont(fontNew);
    scrlpDamocle_= new BuScrollPane(txtaDamocle_);

    txtaListing_= new JList();
//    txtaListing_.setEditable(false);
    txtaListing_.setFont(fontNew);
    scrlpListing_= new BuScrollPane(txtaListing_);

    txtaCasier_= new JList();
//    txtaCasier_.setEditable(false);
    txtaCasier_.setFont(fontNew);
    scrlpCasier_= new BuScrollPane(txtaCasier_);

    txtaLiaison_= new JList();
//    txtaLiaison_.setEditable(false);
    txtaLiaison_.setFont(fontNew);
    scrlpLiaison_= new BuScrollPane(txtaLiaison_);

    if (CGlobal.AVEC_QUALITE_DEAU) {
	    txtaTracer_= new JList();
	//  txtaTracer__.setEditable(false);
	    txtaTracer_.setFont(fontNew);
	  scrlpTracer_= new BuScrollPane(txtaTracer_);
    }

    tbpResultatsGeneraux_.setPreferredSize(new Dimension(600, 600));
    tbpResultatsGeneraux_.addTab(" "+getS("Messages noyau de calcul")+" ", scrlpEcran_);
    tbpResultatsGeneraux_.setSelectedIndex(0);
    tbpResultatsGeneraux_.addTab(getS("Avertissements"), scrlpAvertissments_);
//    tbpResultatsGeneraux_.addTab(" "+getS("Listing Damocl�s")+" ", scrlpDamocle_);
    tbpResultatsGeneraux_.addTab(getS("Listing Mascaret"), scrlpListing_);
    getContentPane().add(tbpResultatsGeneraux_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.FERMER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("FERMER".equals(cmd)) {
      fermer();
    }
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierResultatsGeneraux))
      return;
    MetierResultatsGeneraux param= (MetierResultatsGeneraux)_n;
    if (param == param_)
      return;
    param_= param;
    setValeurs();
  }
  @Override
  public void show() {
    super.activate();
  }

  public void setAvertissements(String avertissements) {

	    int indexAvertissements= tbpResultatsGeneraux_.indexOfTab(getS("Avertissements"));

	    if ((avertissements == null) || (avertissements == "")) {
	      if (indexAvertissements != -1) {
	    	  System.out.println(getS("Avertissements vide"));
		      tbpResultatsGeneraux_.removeTabAt(indexAvertissements);
		      	tbpResultatsGeneraux_.setSelectedIndex(0);
	      }
	    } else {
	      if (indexAvertissements == -1) tbpResultatsGeneraux_.insertTab(getS("Avertissements"),null, scrlpAvertissments_,"",0);
	      	tbpResultatsGeneraux_.setSelectedIndex(0);
	      	txtaAvertissements_.setText(avertissements);
	    }



  }
  public void setMessagesEcran(byte[] messageEcran) {
    StyledDocument doc = txtaEcran_.getStyledDocument();
    try {
      doc.remove(0, doc.getLength());
      if ( (messageEcran == null) || (messageEcran.length == 0)) {
        System.out.println("messageEcran vide");
      }
      else {
        System.out.println("messageEcran pas vide");
        doc.insertString(0, new String(messageEcran), doc.getStyle("normal"));
      }
    }
    catch (BadLocationException ex) {
      ex.printStackTrace();
    }
  }


  public void setMessagesEcranErreur(byte[] messageEcranErreur) {
    if ((messageEcranErreur != null) && (messageEcranErreur.length != 0)) {
      StyledDocument doc = txtaEcran_.getStyledDocument();
      try {
        doc.insertString(doc.getLength(), new String(messageEcranErreur), doc.getStyle("erreur"));
      }
      catch (BadLocationException ex) {
        ex.printStackTrace();
      }
    } else {
      System.out.println("messageEcranErreur vide");
    }
  }
//  public void setListingDamocle(byte[] listingDamoc) {
//    if ((listingDamoc == null) || (listingDamoc.length == 0)) {
//      tbpResultatsGeneraux_.getComponent(1).setEnabled(false);
//    }
//    EbliListModelLargeString modele = new EbliListModelLargeString(listingDamoc);
//    txtaDamocle_.setModel(modele);
//  }
  public void setListing(byte[] listing) {
    if ((listing == null) || (listing.length == 0)) {
      tbpResultatsGeneraux_.getComponent(1).setEnabled(false);
    }
    EbliListModelLargeString modele = new EbliListModelLargeString(listing);
    txtaListing_.setModel(modele);
  }
  public void setListingCasier(byte[] listingCasier) {
    int indexCasier= tbpResultatsGeneraux_.indexOfTab(getS("Listing Casier"));
    if ((listingCasier == null) || (listingCasier.length == 0)) {
      if (indexCasier != -1)
        tbpResultatsGeneraux_.removeTabAt(indexCasier);
    } else {
      if (indexCasier == -1)
        tbpResultatsGeneraux_.addTab(getS("Listing Casier"), scrlpCasier_);
    }
    EbliListModelLargeString modele = new EbliListModelLargeString(listingCasier);
    txtaCasier_.setModel(modele);
  }
  public void setListingLiaison(byte[] listingLiaison) {
    int indexLiaison= tbpResultatsGeneraux_.indexOfTab(getS("Listing Liaison"));
    if ((listingLiaison == null) || (listingLiaison.length == 0)) {
      if (indexLiaison != -1)
        tbpResultatsGeneraux_.removeTabAt(indexLiaison);
    } else {
      if (indexLiaison == -1)
        tbpResultatsGeneraux_.addTab(getS("Listing Liaison"), scrlpLiaison_);
    }
    EbliListModelLargeString modele = new EbliListModelLargeString(listingLiaison);
    txtaLiaison_.setModel(modele);
  }
  public void setListingTracer(byte[] listingTracer) {
	  if (CGlobal.AVEC_QUALITE_DEAU) {
	    int indexTracer= tbpResultatsGeneraux_.indexOfTab(getS("Listing Tracer"));
	    if ((listingTracer == null) || (listingTracer.length == 0)) {
	      if (indexTracer != -1)
	        tbpResultatsGeneraux_.removeTabAt(indexTracer);
	    } else {
	      if (indexTracer == -1)
	        tbpResultatsGeneraux_.addTab(getS("Listing Tracer"), scrlpTracer_);
	    }
	    EbliListModelLargeString modele = new EbliListModelLargeString(listingTracer);
	    txtaTracer_.setModel(modele);
	  }
  }

  @Override
  protected boolean getValeurs() {
    return false;
  }
  @Override
  protected void setValeurs() {
	setAvertissements(param_.avertissements());
    setMessagesEcran(param_.messagesEcran());
    setMessagesEcranErreur(param_.messagesEcranErreur());
//    setListingDamocle(param_.listingDamocles());
    setListing(param_.listing());
    setListingCasier(param_.listingCasier());
    if (CGlobal.AVEC_QUALITE_DEAU) {
    	setListingTracer(param_.listingTracer());
    }
    int indexLiaison= tbpResultatsGeneraux_.indexOfTab(getS("Listing Liaison"));
    if ((param_.listingLiaison() == null)
      || (param_.listingLiaison().length == 0)) {
      if (indexLiaison != -1)
        tbpResultatsGeneraux_.removeTabAt(indexLiaison);
    } else {
      if (indexLiaison == -1)
        tbpResultatsGeneraux_.addTab(getS("Listing Liaison"), scrlpLiaison_);
    }
    EbliListModelLargeString modele = new EbliListModelLargeString(param_.listingLiaison());
    txtaLiaison_.setModel(modele);

  }
}
