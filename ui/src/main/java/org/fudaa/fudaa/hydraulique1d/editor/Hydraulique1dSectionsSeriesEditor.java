/*
 * @creation     2004-07-07
 * @modification $Date: 2007-11-20 11:42:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.fr
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeries;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.ebli.commun.EbliButtonYesNo;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauZoneTailleModel;

import com.memoire.bu.BuBorderLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur de sections de calcul � partir d'une carte des tailles de maille.<br>
 * Appeler si l'utilisateur clic sur le bouton "EDITER SECTIONS" apr�s avoir s�lectionner
 * "Sections positionn�es � partir d'une carte des tailles de mailles".<br>
 * Lancer � partir de la classe Hydraulique1dMaillageEditor avec l'aide de l'iHM helper Hydraulique1dIHM_SectionsParSerie2.<br>
 * voir la classe Hydraulique1dTableauSectionsSeriesModel.
 * @author Jean-Marc Lacombe
 * @version $Id: Hydraulique1dSectionsSeriesEditor.java,v 1.7 2007-11-20 11:42:42 bmarchan Exp $
 */
public class Hydraulique1dSectionsSeriesEditor extends Hydraulique1dTableauZoneEditor {
  private EbliButtonYesNo btSectionsSurProfil_;
  private MetierDefinitionSectionsParSeries definitionSections_;
  public Hydraulique1dSectionsSeriesEditor() {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Sections de calcul � partir d'une carte des tailles de maille"),
          new Hydraulique1dTableauZoneTailleModel(Hydraulique1dTableauZoneTailleModel.MODE_MAILLAGE));
    btSectionsSurProfil_ = new EbliButtonYesNo(getS("Sections sur les profils"));
    panelCentral_.add(btSectionsSurProfil_,BuBorderLayout.SOUTH);
    pack();
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    super.setObject(_n);
    if (_n instanceof MetierDefinitionSectionsParSeries) {
      definitionSections_ = (MetierDefinitionSectionsParSeries)_n;
      Hydraulique1dTableauZoneTailleModel model = (Hydraulique1dTableauZoneTailleModel) modele_;
      model.setModelMetier(definitionSections_);
      super.setTitle(getS("Maillage d�fini section par section"));
    }

  }
  @Override
  public void setValeurs() {
    modele_.setValeurs();
    btSectionsSurProfil_.setSelected(definitionSections_.surProfils());
  }
  @Override
  public boolean getValeurs() {
    if (definitionSections_.surProfils() != btSectionsSurProfil_.isSelected()) {
      definitionSections_.surProfils(btSectionsSurProfil_.isSelected());
      modele_.getValeurs();
      return true;
    }
    return modele_.getValeurs();
  }
}
