/*
 * @file         Hydraulique1dSeuilAvecLoiEditor.java
 * @creation     2000-11-29
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimnigramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilDenoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLimniAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilNoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAval;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuMultiLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des seuils faisant r�f�rence � une loi hydraulique (MetierSeuil).<br>
 * Appeler si l'utilisateur clic sur une singularit� de type "Hydraulique1dReseauSeuil"
 * si la seuil n'est pas un seuil loi ni vanne pour un noyau fluvial.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().SEUIL_AVEC_LOI().editer().<br>
 * @version      $Revision: 1.27 $ $Date: 2007-11-20 11:42:45 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dSeuilAvecLoiEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuButton btDefinirLoi_= new BuButton(getS("DEFINIR UNE LOI"));
  //private BuButton btDefinirLoiHydro_= new BuButton("DEFINIR UNE LOI");
  private BuTextField tfNumero_, tfNom_, tfAbscisse_, tfZRupture_,tfZmoyCrete_,tfZCrete_,tfcoefQ_,tfGradient_;
  private BuLabel lbAbscisse_;
  private BuLabel lbNomLoi_;
  private BuLabel lbZRupture_;
  private BuLabel lbZCrete_;//seuil denoye
  private BuLabel lbZmoyCrete_;//tous les autres seuils
  private BuLabel lbcoefQ_;
  private BuLabel lbGradient_;
  private BuMultiLabel lbMessageDebit_,lbMessageGradient_,lbMessageCrete_;
  private Hydraulique1dListeLoiCombo cmbNomLoi_;
  private BuPanel pnNumero_, pnSeuil_, pnNomLoi_, pnAbscisse_;
  private BuPanel pnNom_, pnZRupture_,pnZmoyCrete_,pnZCrete_,pnZmoyCreteAvecMessage_,pncoefQ_;
  private BuPanel pnCaracteristiques_,pnRupture_,pnGradient_;
  private BuVerticalLayout loSeuil_, loNomLoi_,loVertical_;
  private BuHorizontalLayout loNumero_, loAbscisse_, loNom_, loZRupture_,loZmoyCrete_,locoefQ_,loHorizontal_;
  private MetierSeuil seuil_;
  private MetierDonneesHydrauliques donneesHydro_;
  private MetierBief bief_;
  private EnumMetierRegime regime_;
  public Hydraulique1dSeuilAvecLoiEditor() {
    this(null);
  }
  public Hydraulique1dSeuilAvecLoiEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("edition d'un seuil avec une loi"));
    cmbNomLoi_ = new Hydraulique1dListeLoiCombo(Hydraulique1dListeLoiCombo.HYDRAULIQUE);
    btDefinirLoi_.addActionListener(this);
    seuil_= null;
    regime_= null;
    loNumero_= new BuHorizontalLayout(5, false, false);
    loNom_= new BuHorizontalLayout(5, false, false);
    loAbscisse_= new BuHorizontalLayout(5, false, false);
    loZRupture_= new BuHorizontalLayout(5, false, false);
    loZmoyCrete_= new BuHorizontalLayout(5, false, false);
    locoefQ_= new BuHorizontalLayout(5, false, false);
    loSeuil_= new BuVerticalLayout(5, false, false);
    loNomLoi_= new BuVerticalLayout(5, false, false);
    loVertical_= new BuVerticalLayout(5, false, false);
    loHorizontal_= new BuHorizontalLayout(2, false, false);
    Container pnMain_= getContentPane();
    pnCaracteristiques_= new BuPanel();
    pnCaracteristiques_.setLayout(loVertical_);
    pnCaracteristiques_.setBorder(BorderFactory.createTitledBorder(getS("Caracteristiques du seuil")));
    pnRupture_= new BuPanel();
    pnRupture_.setLayout(loVertical_);
    pnRupture_.setBorder(BorderFactory.createTitledBorder(getS("Rupture")));


    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loNumero_);
    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    pnAbscisse_= new BuPanel();
    pnAbscisse_.setLayout(loAbscisse_);
    pnZRupture_= new BuPanel();
    pnZRupture_.setLayout(loZRupture_);
    pnZmoyCrete_= new BuPanel();
    pnZmoyCrete_.setLayout(loZmoyCrete_);
    pnZmoyCreteAvecMessage_= new BuPanel();
    pnZmoyCreteAvecMessage_.setLayout(loVertical_);
    pnZCrete_= new BuPanel();
    pnZCrete_.setLayout(loZmoyCrete_);
    pncoefQ_= new BuPanel();
    pncoefQ_.setLayout(locoefQ_);
    pnNomLoi_= new BuPanel();
    pnNomLoi_.setLayout(loNomLoi_);
    pnGradient_= new BuPanel();
    pnGradient_.setLayout(loHorizontal_);

    pnSeuil_= new BuPanel();
    pnSeuil_.setLayout(loSeuil_);
    pnSeuil_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    int textSize= 5;

    lbZmoyCrete_ = new BuLabel(getS("Cote moy. cr�te"));
    Dimension dimLabel= lbZmoyCrete_.getPreferredSize();
    dimLabel.width = dimLabel.width + 30;
    System.out.println("dimLabel="+dimLabel);
    lbZmoyCrete_.setPreferredSize(dimLabel);



    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(true);
    BuLabel lbNumero= new BuLabel(getS("N� seuil"));
    lbNumero.setPreferredSize(dimLabel);
    pnNumero_.add(lbNumero, 0);
    pnNumero_.add(tfNumero_, 1);


    tfNom_= new BuTextField();
    //tfNom_.setColumns(15);
    tfNom_.setEditable(true);
    BuLabel lbNom= new BuLabel(getS("Nom du seuil"));
    lbNom.setPreferredSize(dimLabel);
    pnNom_.add(lbNom, 0);
    pnNom_.add(tfNom_, 1);

    textSize= 10;
    tfAbscisse_= BuTextField.createDoubleField();
    tfAbscisse_.setColumns(textSize);
    tfAbscisse_.setEditable(true);
    BuLabel lbAbscisse= new BuLabel(getS("Abscisse"));
    lbAbscisse.setPreferredSize(dimLabel);
    pnAbscisse_.add(lbAbscisse, 0);
    pnAbscisse_.add(tfAbscisse_, 1);
    lbAbscisse_= new BuLabel("   ");
    pnAbscisse_.add(lbAbscisse_, 2);

    tfZmoyCrete_= BuTextField.createDoubleField();
    tfZmoyCrete_.setColumns(textSize);
    tfZmoyCrete_.setEditable(true);
    //lbZmoyCrete_ = new BuLabel("Cote moy. cr�te");
    lbZmoyCrete_.setPreferredSize(dimLabel);
    pnZmoyCrete_.add(lbZmoyCrete_, 0);
    pnZmoyCrete_.add(tfZmoyCrete_, 1);
    pnZmoyCrete_.setVisible(false);
    pnCaracteristiques_.add(pnZmoyCrete_);


    tfcoefQ_= BuTextField.createDoubleField();
    tfcoefQ_.setColumns(textSize);
    tfcoefQ_.setEditable(true);
    lbcoefQ_=new BuLabel(getS("Coef. de d�bit"));
    lbcoefQ_.setPreferredSize(dimLabel);
    pncoefQ_.add(lbcoefQ_, 0);
    pncoefQ_.add(tfcoefQ_, 1);
    pncoefQ_.setVisible(false);
    pnCaracteristiques_.add(pncoefQ_);


    lbNomLoi_= new BuLabel(getS("Nom de la loi"));
    pnNomLoi_.add(lbNomLoi_, 0);
    pnNomLoi_.add(cmbNomLoi_, 1);
    pnNomLoi_.add(btDefinirLoi_, 2);
    pnCaracteristiques_.add(pnNomLoi_);

    lbMessageDebit_= new BuMultiLabel(getS("Il est possible de mod�liser un �ventuel d�bit turbin� en l'int�grant dans la courbe de tarage"));
    Font font2 = new Font("Arial",Font.ITALIC,12);
    lbMessageDebit_.setFont(font2);
    Icon icon2=	BuResource.BU.getIcon("astuce_22.gif");
    lbMessageDebit_.setIcon(icon2);
    lbMessageDebit_.setVisible(false);
    pnCaracteristiques_.add(lbMessageDebit_);


    lbGradient_= new BuLabel(getS("Gradient d'abaissement de la cr�te (m/s) "));
    Dimension dimLabel2 = lbGradient_.getPreferredSize();


    tfZRupture_= BuTextField.createDoubleField();
    tfZRupture_.setColumns(textSize);
    tfZRupture_.setEditable(true);
    lbZRupture_= new BuLabel(getS("Cote de Rupture"));
    lbZRupture_.setPreferredSize(dimLabel2);
    pnZRupture_.add(lbZRupture_, 0);
    pnZRupture_.add(tfZRupture_, 1);
    pnRupture_.add(pnZRupture_);

    tfZCrete_= BuTextField.createDoubleField();
    tfZCrete_.setColumns(textSize);
    tfZCrete_.setEditable(true);
    lbZCrete_ = new BuLabel(getS("Cote de cr�te"));
    lbZCrete_.setPreferredSize(dimLabel2);
    pnZCrete_.add(lbZCrete_, 0);
    pnZCrete_.add(tfZCrete_, 1);
    pnZmoyCreteAvecMessage_.add(pnZCrete_);

    lbMessageCrete_= new BuMultiLabel(getS("Attention, la cote de cr�te n'est pas utilis�e en r�gime transcritique !"));
    Font font = new Font("Arial",Font.ITALIC,12);
    lbMessageCrete_.setFont(font);
    Icon icon=	BuResource.BU.getIcon("astuce_22.gif");
    lbMessageCrete_.setIcon(icon);
    lbMessageCrete_.setVisible(false);
    pnZmoyCreteAvecMessage_.add(lbMessageCrete_);
    pnZmoyCreteAvecMessage_.setVisible(false);
    pnRupture_.add(pnZmoyCreteAvecMessage_);


    tfGradient_= BuTextField.createDoubleField();
    tfGradient_.setColumns(textSize);
    tfGradient_.setEditable(true);
    //lbGradient_= new BuLabel("Gradient d'abaissement de la cr�te");
    lbGradient_.setPreferredSize(dimLabel2);
    pnGradient_.setToolTipText(getS("La valeur par d�faut tr�s �lev�e du gradient permet de simuler une rupture instantan�e"));
    pnGradient_.add(lbGradient_, 0);
    pnGradient_.add(tfGradient_, 1);
    pnGradient_.setVisible(false);
    pnRupture_.add(pnGradient_);

   lbMessageGradient_= new BuMultiLabel(getS("La valeur par d�faut tr�s �lev�e du gradient permet de simuler une rupture instantan�e"));
   lbMessageGradient_.setFont(font);
   //Icon icon=	BuResource.BU.getIcon("astuce_22.gif");
   lbMessageGradient_.setIcon(icon);
   lbMessageGradient_.setVisible(false);
   pnRupture_.add(lbMessageGradient_);




    int n= 0;
    pnSeuil_.add(pnNumero_, n++);
    pnSeuil_.add(pnNom_, n++);
    pnSeuil_.add(pnAbscisse_, n++);
    pnSeuil_.add(pnCaracteristiques_, n++);
    pnSeuil_.add(pnRupture_, n++);

    pnMain_.add(pnSeuil_, BorderLayout.CENTER);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("ANNULER".equals(cmd)) {
      fermer();
    }
    else if (cmd.startsWith("DEFINIR_LOI")) {
        Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().setQualiteDEau(false);
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer();
    }

    else if ("VALIDER".equals(cmd)) {
      int reponse = JOptionPane.YES_OPTION;
      if (cmbNomLoi_.getValeurs() == null) {
        showBuError(getS("Aucune loi s�lectionn�e"), true);
        System.err.println(getS("Aucune loi selectionnee"));
        return;
      } else  if (cmbNomLoi_.getValeurs() instanceof MetierLoiTarage){
          MetierLoiTarage l = (MetierLoiTarage) cmbNomLoi_.getValeurs();

          if (l.amont() /*<=> Z=f(Q)*/) {
              if ((seuil_ instanceof MetierSeuilTarageAmont) ||
                  (seuil_ instanceof MetierSeuilTarageAval)) {
                  reponse = JOptionPane.showConfirmDialog(pnSeuil_, getS("La loi choisie est de type 'Courbe de tarage Z")+"="+getS("f(Q)', elle ne correspond pas � ce type de seuil !")+
                                                              "\n "+getS("Souhaitez-vous confirmer cette loi ?"),
                          getS("Attention !"),
                          JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE);
              }
          }else if (seuil_ instanceof MetierSeuilDenoye) {
                  reponse = JOptionPane.showConfirmDialog(pnSeuil_, getS("La loi choisie est de type 'Courbe de tarage Q")+"="+getS("f(Z)', elle ne correspond pas � ce type de seuil !")+
                                                              "\n "+getS("Souhaitez-vous confirmer cette loi ?"),
                          getS("Attention !"),
                          JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE);
              }
      }

      if (reponse == JOptionPane.YES_OPTION) {
          if (getValeurs()) {
              firePropertyChange("singularite", null, seuil_);
          }
          fermer();
      }
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _o) {
    if (_o instanceof MetierSeuil) {
      seuil_= (MetierSeuil)_o;
      setTypeListeLois();
      setValeurs();
      if (regime_.value()==EnumMetierRegime._TRANSCRITIQUE){
          lbMessageCrete_.setVisible(true);
        } else {
          lbMessageCrete_.setVisible(false);
        }
      //En cas de seuil de type 6 (MetierSeuilTarageAmont)
      if (seuil_ instanceof MetierSeuilTarageAmont) {
        pnZmoyCrete_.setVisible(false);
        pncoefQ_.setVisible(false);
        pnZmoyCreteAvecMessage_.setVisible(true);
        pnGradient_.setVisible(true);

        if (regime_.value()==EnumMetierRegime._TRANSCRITIQUE){
          CtuluLibSwing.griserPanel(pnGradient_,true);

          //CtuluLib.griserPanel(pnZRupture_,true);
          lbMessageGradient_.setVisible(true);
          lbMessageDebit_.setVisible(true);
        } else {
          CtuluLibSwing.griserPanel(pnGradient_,false);

          lbMessageGradient_.setVisible(false);
          lbMessageDebit_.setVisible(false);
        }
      } else if (seuil_ instanceof MetierSeuilGeometrique) {
        pnZmoyCrete_.setVisible(true);
        pncoefQ_.setVisible(true);
        pnZmoyCreteAvecMessage_.setVisible(false);
        pnGradient_.setVisible(false);
        lbMessageGradient_.setVisible(false);
        lbMessageDebit_.setVisible(false);

        } else { //Dans tous les autres cas
          pnZmoyCrete_.setVisible(false);
          pncoefQ_.setVisible(false);
          pnZmoyCreteAvecMessage_.setVisible(true);
          pnGradient_.setVisible(false);
          lbMessageGradient_.setVisible(false);
          lbMessageDebit_.setVisible(false);

        }
      }
    else if (_o instanceof MetierParametresGeneraux)
      regime_= (EnumMetierRegime)((MetierParametresGeneraux)_o).regime();
    else if (_o instanceof MetierBief)
      bief_= (MetierBief)_o;
    else if (_o instanceof MetierDonneesHydrauliques) {
      donneesHydro_ = (MetierDonneesHydrauliques) _o;
      cmbNomLoi_.setDonneesHydro(donneesHydro_);

    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (seuil_ == null)
      return changed;
    int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != seuil_.numero()) {
      seuil_.numero(nnum);
      changed= true;
    }
    String nom= tfNom_.getText();
    if (!nom.equals(seuil_.nom())) {
      seuil_.nom(nom);
      changed= true;
    }
    double absc= ((Double)tfAbscisse_.getValue()).doubleValue();
    if (absc != seuil_.abscisse()) {
      seuil_.abscisse(absc);
      changed= true;
    }
    double zRupt = ( (Double) tfZRupture_.getValue()).doubleValue();
    if (zRupt != seuil_.coteRupture()) {
      seuil_.coteRupture(zRupt);
      changed = true;
    }
    MetierLoiHydraulique l= cmbNomLoi_.getValeurs();

    if (seuil_ instanceof MetierSeuilGeometrique) {
      MetierSeuilGeometrique s= (MetierSeuilGeometrique)seuil_;
      double c= ((Double)tfZmoyCrete_.getValue()).doubleValue();
      if (c != s.coteMoyenneCrete()) {
          s.coteMoyenneCrete(c);
          changed = true;
      }
      double coefQ= ((Double)tfcoefQ_ .getValue()).doubleValue();
      if (coefQ != s.coefQ()) {
          s.coefQ(coefQ);
          changed = true;
      }
      MetierLoiGeometrique loi= (MetierLoiGeometrique)l;
      if (loi != s.loi()) {
        s.loi(loi);
        changed= true;
      }
    } else if (seuil_ instanceof MetierSeuilLimniAmont) {
      MetierSeuilLimniAmont s= (MetierSeuilLimniAmont)seuil_;
      double Zcrete= ((Double)tfZCrete_.getValue()).doubleValue();
      if (Zcrete != s.coteCrete()) {
        s.coteCrete(Zcrete);
        changed= true;
      }
      MetierLoiLimnigramme loi= (MetierLoiLimnigramme)l;
      if (loi != s.loi()) {
        s.loi(loi);
        changed= true;
      }
    } else if (seuil_ instanceof MetierSeuilDenoye) {
      MetierSeuilDenoye seuilDenoye= (MetierSeuilDenoye)seuil_;
      double Zcrete= ((Double)tfZCrete_.getValue()).doubleValue();
      if (Zcrete != seuilDenoye.coteCrete()) {
    	  seuilDenoye.coteCrete(Zcrete);
        changed= true;
      }
      MetierLoiTarage loi= (MetierLoiTarage)l;
      if (loi != seuilDenoye.loi()) {
        seuilDenoye.loi(loi);
        changed= true;
      }

    } else if (seuil_ instanceof MetierSeuilTarageAmont) {
      MetierSeuilTarageAmont s= (MetierSeuilTarageAmont)seuil_;
      double Zcrete= ((Double)tfZCrete_.getValue()).doubleValue();
     if (Zcrete != s.coteCrete()) {
       s.coteCrete(Zcrete);
       changed= true;
     }
     double grad= ((Double)tfGradient_.getValue()).doubleValue();
          if (grad != s.gradient()) {
              s.gradient(grad);
              changed = true;
      }
      MetierLoiTarage loi= (MetierLoiTarage)l;
      if (loi != s.loi()) {
        s.loi(loi);
        changed= true;
      }
    } else if (seuil_ instanceof MetierSeuilTarageAval) {
      MetierSeuilTarageAval s= (MetierSeuilTarageAval)seuil_;
      double Zcrete= ((Double)tfZCrete_.getValue()).doubleValue();
      if (Zcrete != s.coteCrete()) {
        s.coteCrete(Zcrete);
        changed= true;
      }
      MetierLoiTarage loi= (MetierLoiTarage)l;
      if (loi != s.loi()) {
        s.loi(loi);
        changed= true;
      }
    } else if (seuil_ instanceof MetierSeuilNoye) {
      MetierSeuilNoye s= (MetierSeuilNoye)seuil_;
      double Zcrete= ((Double)tfZCrete_.getValue()).doubleValue();
      if (Zcrete != s.coteCrete()) {
        s.coteCrete(Zcrete);
        changed= true;
      }
      MetierLoiSeuil loi= (MetierLoiSeuil)l;
      if (loi != s.loi()) {
        s.loi(loi);
        changed= true;
      }
    }
    return changed;
  }

  @Override
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(seuil_.numero()));
    tfNom_.setValue(seuil_.nom());
    tfAbscisse_.setValue(new Double(seuil_.abscisse()));
    tfZRupture_.setValue(new Double(seuil_.coteRupture()));
    cmbNomLoi_.initListeLoi();
    //En cas de seuil de type 6 (MetierSeuilTarageAmont)
    if (seuil_ instanceof MetierSeuilTarageAmont) {
      MetierSeuilTarageAmont s = (MetierSeuilTarageAmont) seuil_;
      pnZmoyCrete_.setVisible(false);
      pncoefQ_.setVisible(false);
      pnZmoyCreteAvecMessage_.setVisible(true);
      pnGradient_.setVisible(true);
      tfZCrete_.setValue(new Double(s.coteCrete()));
      tfGradient_.setValue(new Double(s.gradient()));
      if (regime_.value() == EnumMetierRegime._TRANSCRITIQUE) {
        CtuluLibSwing.griserPanel(pnGradient_, true);
        lbMessageGradient_.setVisible(true);
        lbMessageDebit_.setVisible(true);
        lbMessageCrete_.setVisible(true);
      }
      else {
        CtuluLibSwing.griserPanel(pnGradient_, false);
        lbMessageGradient_.setVisible(false);
        lbMessageDebit_.setVisible(false);
        lbMessageCrete_.setVisible(false);
      }
    }
    else if (seuil_ instanceof MetierSeuilGeometrique) {
      pnZmoyCrete_.setVisible(true);
      pncoefQ_.setVisible(true);
      pnZmoyCreteAvecMessage_.setVisible(false);
      pnGradient_.setVisible(false);
      lbMessageGradient_.setVisible(false);
      lbMessageDebit_.setVisible(false);
      tfZmoyCrete_.setValue(new Double( ( (MetierSeuilGeometrique) seuil_).
                                       coteMoyenneCrete()));
      tfcoefQ_.setValue(new Double( ( (MetierSeuilGeometrique) seuil_).coefQ()));
      tfZRupture_.setValue(new Double(seuil_.coteRupture()));


    }else { //Dans tous les autres cas
      pnZmoyCrete_.setVisible(false);
      pncoefQ_.setVisible(false);
      pnZmoyCreteAvecMessage_.setVisible(true);
      pnGradient_.setVisible(false);
      lbMessageGradient_.setVisible(false);
      lbMessageDebit_.setVisible(false);
      if (seuil_ instanceof MetierSeuilLimniAmont) {
          MetierSeuilLimniAmont s= (MetierSeuilLimniAmont)seuil_;
          tfZCrete_.setValue(new Double(s.coteCrete()));
        } else if (seuil_ instanceof MetierSeuilDenoye) {
          MetierSeuilDenoye s= (MetierSeuilDenoye)seuil_;
          tfZCrete_.setValue(new Double(s.coteCrete()));

        } else if (seuil_ instanceof MetierSeuilTarageAval) {
          MetierSeuilTarageAval s= (MetierSeuilTarageAval)seuil_;
          tfZCrete_.setValue(new Double(s.coteCrete()));
        } else if (seuil_ instanceof MetierSeuilNoye) {
        	MetierSeuilNoye s= (MetierSeuilNoye)seuil_;
            tfZCrete_.setValue(new Double(s.coteCrete()));
        }


    }
    if (seuil_.getLoi() != null)
      cmbNomLoi_.setValeurs(seuil_.getLoi());
    String textAbsc= "";
    if (bief_ != null) {
      textAbsc= getS("du bief n�") + (bief_.indice()+1);
      if ((bief_.extrAmont().profilRattache() != null)
        && (bief_.extrAval().profilRattache() != null))
        textAbsc=
          textAbsc
            + getS(" entre ")
            + bief_.extrAmont().profilRattache().abscisse()
            + getS(" et ")
            + bief_.extrAval().profilRattache().abscisse();
      else
        textAbsc= textAbsc + " ("+getS("abscisses des extremit�s inconnues")+")";
    } else
      textAbsc= getS("bief inconnu");
    lbAbscisse_.setText(textAbsc);
    setTitre();
  }
  // ObjetEventListener
  @Override
  public void objetCree(H1dObjetEvent e) {
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
    MetierHydraulique1d src= e.getSource();
    String champ= e.getChamp();
    if (src == null)
      return;
    if ((src instanceof MetierDonneesHydrauliques)&&("lois".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
    if ((src instanceof MetierLoiHydraulique)&&("nom".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
  }
  private void setTitre() {
    if (seuil_ instanceof MetierSeuilGeometrique) {
      super.setTitle(getS("Edition d'un seuil")+" : "+getS("'Profil de cr�te'"));
      lbNomLoi_.setText(getS("nom de la loi de type 'Profil de cr�te'"));
      btDefinirLoi_.setActionCommand("DEFINIR_LOI_GEOMETRIQUE");
    }
    if (seuil_ instanceof MetierSeuilLimniAmont) {
      super.setTitle(
        getS("Edition d'un seuil limnigramme amont")+" : "+getS("'Abaques (Zam, t)'"));
      lbNomLoi_.setText(getS("nom de la loi de type 'Limnigramme Z(t)'"));
      btDefinirLoi_.setActionCommand("DEFINIR_LOI_LIMNIGRAMME");
    }
    if (seuil_ instanceof MetierSeuilDenoye) {
      super.setTitle(getS("Edition d'un seuil")+" : "+getS("'Abaques Zam")+"="+getS("f(Q)'"));
      lbNomLoi_.setText(getS("nom de la loi de type 'Courbe de tarage Zam")+"="+getS("f(Q)'"));
      btDefinirLoi_.setActionCommand("DEFINIR_LOI_TARAGE");
      //btDefinirLoiHydro_.setActionCommand("DEFINIR_LOI_HYDRO");

    }
    if (seuil_ instanceof MetierSeuilTarageAmont) {
      super.setTitle(getS("Edition d'un seuil")+" : "+getS("'Abaques Q")+"="+getS("f(Zam)'"));
      lbNomLoi_.setText(getS("nom de la loi de type 'Courbe de tarage Q")+"="+getS("f(Zam)'"));
      btDefinirLoi_.setActionCommand("DEFINIR_LOI_TARAGE");
    }
    if (seuil_ instanceof MetierSeuilTarageAval) {
      super.setTitle(getS("Edition d'un seuil")+" : "+getS("'Abaques (Q, Zav)'"));
      lbNomLoi_.setText(getS("nom de la loi de type 'Courbe de tarage Q")+"="+getS("f(Zav)'"));
      btDefinirLoi_.setActionCommand("DEFINIR_LOI_TARAGE");
    }
    if (seuil_ instanceof MetierSeuilNoye) {
      super.setTitle(getS("Edition d'un seuil noy�")+" : "+getS("'Abaques (Zam, Zav, Q)'"));
      lbNomLoi_.setText(getS("nom de la loi de type 'Seuil Zam")+"="+getS("f(Zav,Q)'"));
      btDefinirLoi_.setActionCommand("DEFINIR_LOI_SEUIL");
    }
  }
  private void setTypeListeLois() {
    if (seuil_ instanceof MetierSeuilGeometrique) {
      cmbNomLoi_.setTypeLois(Hydraulique1dListeLoiCombo.GEOMETRIQUE);
    }
    if (seuil_ instanceof MetierSeuilLimniAmont) {
      cmbNomLoi_.setTypeLois(Hydraulique1dListeLoiCombo.LIMNIGRAMME);
    }
    if (seuil_ instanceof MetierSeuilDenoye) {
      cmbNomLoi_.setTypeLois(Hydraulique1dListeLoiCombo.TARAGE);


    }
    if (seuil_ instanceof MetierSeuilTarageAmont) {
      cmbNomLoi_.setTypeLois(Hydraulique1dListeLoiCombo.TARAGE);
    }
    if (seuil_ instanceof MetierSeuilTarageAval) {
      cmbNomLoi_.setTypeLois(Hydraulique1dListeLoiCombo.TARAGE);
    }
    if (seuil_ instanceof MetierSeuilNoye) {
      cmbNomLoi_.setTypeLois(Hydraulique1dListeLoiCombo.SEUIL);
    }
  }



}
