/*
 * @file         Hydraulique1dSeuilChooser.java
 * @creation     2000-12-03
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Boite de dialogue permettant de s�lectionner le type de seuil � cr�er.<br>
 * Utiliser par le classe Hydraulique1dReseauFrame.<br>
 * Il s'adapte au noyau de calcul.
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dSeuilChooser
  extends BDialog
  implements ActionListener {
  BuGridLayout loBoutons_;
  Vector radioBoutons_= new Vector(8);
  MetierReseau reseau_;
  MetierSeuil seuilchoisi_;
  boolean transcritique_;
  public Hydraulique1dSeuilChooser(MetierReseau reseau, boolean transcritique) {
    super((BuCommonInterface)Hydraulique1dBaseApplication.FRAME);
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Choix du seuil"));
    setLocationRelativeTo(Hydraulique1dBaseApplication.FRAME);
    reseau_= reseau;
    transcritique_= transcritique;
    init();
  }
  public Hydraulique1dSeuilChooser(
    IDialogInterface parent,
    MetierReseau reseau,
    boolean transcritique) {
    super((BuCommonInterface)Hydraulique1dBaseApplication.FRAME, parent);
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Choix du type de seuil"));
    setLocationRelativeTo(parent.getComponent());
    reseau_= reseau;
    transcritique_= transcritique;
    init();
  }
  private void init() {
    setModal(true);
    seuilchoisi_= null;
    loBoutons_= new BuGridLayout(1, 5, 5, true, true);
    loBoutons_.setCfilled(false);
    Container content= getContentPane();
    content.setLayout(loBoutons_);
    BuPanel pnRadioBoutons= new BuPanel();
    pnRadioBoutons.setLayout(new BuVerticalLayout(5, false, false));
    pnRadioBoutons.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    int j= 0;
    BuRadioButton rb;
    if (!transcritique_) {
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Abaques (Zam, Zav, Q)"));
      rb.setActionCommand("Abaques (Zam, Zav, Q)");
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Abaques Zam")+"="+
                            Hydraulique1dResource.HYDRAULIQUE1D.getString("f(Q)"));
      rb.setActionCommand("Abaques (Q, Zam)");
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Abaques (Zam, t)"));
      rb.setActionCommand("Abaques (Zam, t)");
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Abaques Q")+"="+
                            Hydraulique1dResource.HYDRAULIQUE1D.getString("f(Zam)"));
      rb.setActionCommand("Abaques (Zam, Q)");
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Abaques (Q, Zav)"));
      rb.setActionCommand("Abaques (Q, Zav)");
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi de seuil (Cote cr�te, coeff. d�bit)"));
      rb.setActionCommand("Loi de seuil");
      rb.setSelected(true); // s�lectionn� par d�faut
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Profil de cr�te"));
      rb.setActionCommand("Profil de cr�te");
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Vanne"));
      rb.setActionCommand("Vanne");
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
    } else {
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Abaques Q")+"="+
                            Hydraulique1dResource.HYDRAULIQUE1D.getString("f(Zam)"));
      rb.setActionCommand("Abaques (Zam, Q)");
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
      rb= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Loi de seuil transcritique (cr�te, coeff. d�bit)"));
      rb.setActionCommand("Loi de seuil transcritique");
      rb.setSelected(true); // s�lectionn� par d�faut
      rb.addActionListener(this);
      radioBoutons_.add(rb);
      pnRadioBoutons.add(rb, j++);
    }
    BuLabel lb= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Choisissez un seuil ?"));
    BuPanel pn= new BuPanel();
    FlowLayout lo= new FlowLayout(FlowLayout.CENTER);
    pn.setLayout(lo);
    BuButton bt1= new BuButton();
    bt1.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    bt1.setIcon(BuResource.BU.getIcon("valider"));
    bt1.setName("btBIEFVALIDER");
    bt1.setActionCommand("VALIDER");
    bt1.addActionListener(this);
    BuButton bt2= new BuButton();
    bt2.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    bt2.setIcon(BuResource.BU.getIcon("annuler"));
    bt2.setName("btBIEFANNULER");
    bt2.setActionCommand("ANNULER");
    bt2.addActionListener(this);
    pn.add(bt2);
    pn.add(bt1);
    j= 0;
    content.add(lb, j++);
    content.add(pnRadioBoutons, j++);
    content.add(pn, j++);
    pack();
  }
  public MetierSeuil activate() {
    System.err.println("Hydraulique1dSeuilChooser: activate debut");
    show();
    System.err.println("Hydraulique1dSeuilChooser: activate fin");
    return seuilchoisi_;
  }
  @Override
  public void actionPerformed(ActionEvent e) {
    String cmd= e.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      Iterator ite= radioBoutons_.iterator();
      while (ite.hasNext()) {
        BuRadioButton bt= (BuRadioButton)ite.next();
        if (bt.isSelected()) {
          seuilchoisi_= creationSeuil(bt.getActionCommand());
        }
      }
      dispose();
    } else if ("ANNULER".equals(cmd)) {
      seuilchoisi_= null;
      dispose();
    } else {
      Iterator ite= radioBoutons_.iterator();
      while (ite.hasNext()) {
        BuRadioButton bt= (BuRadioButton)ite.next();
        if (e.getSource() == bt) {
          bt.setSelected(true);
        } else {
          bt.setSelected(false);
        }
      }
    }
  }
  private MetierSeuil creationSeuil(String command) {
    if ("Abaques (Zam, Zav, Q)".equals(command))
      return reseau_.creeSeuilNoye();
    if ("Abaques (Q, Zam)".equals(command))
      return reseau_.creeSeuilDenoye();
    if ("Abaques (Zam, t)".equals(command))
      return reseau_.creeSeuilLimniAmont();
    if ("Abaques (Zam, Q)".equals(command))
      return reseau_.creeSeuilTarageAmont();
    if ("Abaques (Q, Zav)".equals(command))
      return reseau_.creeSeuilTarageAval();
    if ("Loi de seuil".equals(command))
      return reseau_.creeSeuilLoi();
    if ("Loi de seuil transcritique".equals(command))
      return reseau_.creeSeuilLoi();
    if ("Profil de cr�te".equals(command))
      return reseau_.creeSeuilGeometrique();
    if ("Vanne".equals(command))
      return reseau_.creeSeuilVanne();
    return null;
  }
}
