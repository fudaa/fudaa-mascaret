/*
 * @file         Hydraulique1dSeuilLoiEditor.java
 * @creation     2000-11-29
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierEpaisseurSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTranscritique;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuMultiLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des seuils lois (MetierSeuilLoi).<br>
 * Appeler si l'utilisateur clic sur une singularit� de type "Hydraulique1dReseauSeuil"
 * si la seuil est un seuil loi pour un noyau fluvial.<br>
 * Lancer par la classe Hydraulique1dReseauMouseAdapter sans utiliser d'IHM HELPER.<br>
 * @version      $Revision: 1.18 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dSeuilLoiEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuTextField tfNumero_, tfAbscisse_, tfNom_, tfZRupture_,tfDebit_,tfGradient_;
  private BuTextField tfZCrete_, tfCoefQ_;
  private BuComboBox cbEpaisseur_;
   private BuMultiLabel lbMessageGradient_;
  private BuLabel lbAbscisse_,lbGradient_,lbDebit_,lbZRupture_;
  private BuPanel pnSeuil_, pnNumero_, pnAbscisse_, pnNom_, pnZRupture_,pnGradient_,pnDebit_;
  private BuPanel pnZCrete_, pnCoefQ_, pnEpaisseur_;
  private BuPanel pnCaracteristiques_,pnRupture_,pnPanelDebit_;
  private BuVerticalLayout loSeuil_,loVertical_;
  private BuHorizontalLayout loHorizontal_;
  private MetierSeuilLoi seuil_;
  private MetierBief bief_;
  private EnumMetierRegime regime_;
  private final static String[] TYPE_EPAISSEUR= { Hydraulique1dResource.HYDRAULIQUE1D.getString("�pais"),
                                                  Hydraulique1dResource.HYDRAULIQUE1D.getString("mince") };
  public Hydraulique1dSeuilLoiEditor() {
    this(null);
  }
  public Hydraulique1dSeuilLoiEditor(BDialogContent parent) {
    super(
      parent,
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition d'un seuil (loi de seuil")+" : "+Hydraulique1dResource.HYDRAULIQUE1D.getString("cote cr�te, coef. d�bit)"));
    seuil_= null;
    regime_=null;
    loHorizontal_= new BuHorizontalLayout(5, false, false);
    loSeuil_= new BuVerticalLayout(5, false, false);
    loVertical_= new BuVerticalLayout(5, false, false);
    Container pnMain_= getContentPane();

    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loHorizontal_);
    pnAbscisse_= new BuPanel();
    pnAbscisse_.setLayout(loHorizontal_);
    pnNom_= new BuPanel();
    pnNom_.setLayout(loHorizontal_);
    pnZRupture_= new BuPanel();
    pnZRupture_.setLayout(loHorizontal_);
    pnZCrete_= new BuPanel();
    pnZCrete_.setLayout(loHorizontal_);
    pnCoefQ_= new BuPanel();
    pnCoefQ_.setLayout(loHorizontal_);
    pnEpaisseur_= new BuPanel();
    pnEpaisseur_.setLayout(loHorizontal_);
    pnGradient_= new BuPanel();
    pnGradient_.setLayout(loHorizontal_);
    pnDebit_= new BuPanel();
    pnDebit_.setLayout(loHorizontal_);



    pnCaracteristiques_= new BuPanel();
    pnCaracteristiques_.setLayout(loVertical_);
    pnCaracteristiques_.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
        getS("Caracteristiques du seuil")));
    pnRupture_ = new BuPanel();
    pnRupture_.setLayout(loVertical_);
    pnRupture_.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),getS("Rupture")));
    pnPanelDebit_= new BuPanel();
    pnPanelDebit_.setLayout(loVertical_);
    pnPanelDebit_.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
        ""));


    pnSeuil_= new BuPanel();
    pnSeuil_.setLayout(loSeuil_);
    pnSeuil_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));

    int textSize= 5;
    BuLabel lbNumero= new BuLabel(getS("Num�ro du seuil"));
    Dimension dimLabel= lbNumero.getPreferredSize();
    dimLabel.width = dimLabel.width + 10;
    lbNumero.setPreferredSize(dimLabel);

    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(true);
    //BuLabel lbNumero= new BuLabel("Num�ro du seuil");
    pnNumero_.add(lbNumero, 0);
    pnNumero_.add(tfNumero_, 1);

    textSize= 10;
    tfAbscisse_= BuTextField.createDoubleField();
    tfAbscisse_.setColumns(textSize);
    tfAbscisse_.setEditable(true);
    BuLabel lbAbscisse= new BuLabel(getS("Abscisse"));
    lbAbscisse.setPreferredSize(dimLabel);
    pnAbscisse_.add(lbAbscisse, 0);
    pnAbscisse_.add(tfAbscisse_, 1);
    lbAbscisse_= new BuLabel("   ");
    pnAbscisse_.add(lbAbscisse_, 2);

    tfNom_= new BuTextField();
    //tfNom_.setColumns(textSize);
    tfNom_.setEditable(true);
    BuLabel lbNom= new BuLabel(getS("Nom"));
    lbNom.setPreferredSize(dimLabel);
    pnNom_.add(lbNom, 0);
    pnNom_.add(tfNom_, 1);

    BuLabel lbQ= new BuLabel(getS("Coeff. de d�bit"));
    Dimension dimLabel3 =lbQ.getPreferredSize();
    tfZCrete_= BuTextField.createDoubleField();
    tfZCrete_.setColumns(textSize);
    tfZCrete_.setEditable(true);
    BuLabel lbZCrete= new BuLabel(getS("Cote de cr�te"));
    lbZCrete.setPreferredSize(dimLabel3);
    pnZCrete_.add(lbZCrete, 0);
    pnZCrete_.add(tfZCrete_, 1);
    pnCaracteristiques_.add(pnZCrete_);

    tfCoefQ_= BuTextField.createDoubleField();
    tfCoefQ_.setColumns(textSize);
    tfCoefQ_.setEditable(true);
//    BuLabel lbQ= new BuLabel(getS("Coeff. de d�bit"));
    lbQ.setPreferredSize(dimLabel3);
    pnCoefQ_.add(lbQ, 0);
    pnCoefQ_.add(tfCoefQ_, 1);
    pnCaracteristiques_.add(pnCoefQ_);

    cbEpaisseur_= new BuComboBox(TYPE_EPAISSEUR);
    cbEpaisseur_.setEditable(false);
    BuLabel lbEpaisseur= new BuLabel(getS("Epaisseur"));
    lbEpaisseur.setPreferredSize(dimLabel3);
    pnEpaisseur_.add(lbEpaisseur, 0);
    pnEpaisseur_.add(cbEpaisseur_, 1);
    pnCaracteristiques_.add(pnEpaisseur_);


    lbGradient_= new BuLabel(getS("Gradient d'abaissement de la cr�te (m/s)"));
    Dimension dimLabel2= lbGradient_.getPreferredSize();

    tfZRupture_= BuTextField.createDoubleField();
    tfZRupture_.setColumns(textSize);
    tfZRupture_.setEditable(true);
    lbZRupture_ = new BuLabel(getS("Cote de rupture"));
    lbZRupture_.setPreferredSize(dimLabel2);
    pnZRupture_.add(lbZRupture_, 0);
    pnZRupture_.add(tfZRupture_, 1);
    pnRupture_.add(pnZRupture_);

    tfGradient_= BuTextField.createDoubleField();
    tfGradient_.setColumns(textSize);
    tfGradient_.setEditable(true);
    //lbGradient_= new BuLabel("Gradient d'abaissement de la cr�te");
    lbGradient_.setPreferredSize(dimLabel2);
    pnGradient_.add(lbGradient_, 0);
    pnGradient_.add(tfGradient_, 1);
    pnRupture_.add(pnGradient_);

    lbMessageGradient_ = new BuMultiLabel(getS("La valeur par d�faut tr�s �lev�e du gradient permet de simuler une rupture instantan�e"));
    Font font = new Font("Arial", Font.ITALIC, 12);
    lbMessageGradient_.setFont(font);
    Icon icon = BuResource.BU.getIcon("astuce_22.gif");
    lbMessageGradient_.setIcon(icon);
    lbMessageGradient_.setVisible(true);
    pnRupture_.add(lbMessageGradient_);

    tfDebit_= BuTextField.createDoubleField();
    tfDebit_.setColumns(textSize);
    tfDebit_.setEditable(true);
    lbDebit_ = new BuLabel(getS("D�bit transitant � travers le seuil"));
    lbDebit_.setPreferredSize(dimLabel2);
    pnDebit_.add(lbDebit_, 0);
    pnDebit_.add(tfDebit_,1);
    pnPanelDebit_.add(pnDebit_);

    pnSeuil_.add(pnNumero_, 0);
    pnSeuil_.add(pnAbscisse_, 1);
    pnSeuil_.add(pnNom_, 2);
    pnSeuil_.add(pnCaracteristiques_, 3);
    pnSeuil_.add(pnRupture_, 4);
    pnSeuil_.add(pnPanelDebit_, 5);

    pnMain_.add(pnSeuil_, BorderLayout.CENTER);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("ANNULER".equals(cmd)) {
      fermer();
    } else if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("singularite", null, seuil_);
      }
      fermer();
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _o) {
    if (_o instanceof MetierBief)
      bief_= (MetierBief)_o;
    else if (_o instanceof MetierParametresGeneraux){
      regime_ = (EnumMetierRegime) ( (MetierParametresGeneraux) _o).regime();
      if (regime_.value() == EnumMetierRegime._TRANSCRITIQUE) {;
        CtuluLibSwing.griserPanel(pnGradient_,true);
        lbMessageGradient_.setVisible(true);
        CtuluLibSwing.griserPanel(pnDebit_,true);
      }
      else {
        CtuluLibSwing.griserPanel(pnGradient_,false);
        lbMessageGradient_.setVisible(false);
        CtuluLibSwing.griserPanel(pnDebit_,true);

      }
    }
    else if (_o instanceof MetierSeuilLoi) {
      if (seuil_ == _o) return;
      seuil_= (MetierSeuilLoi)_o;
      setValeurs();

    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (seuil_ == null)
      return changed;
    int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != seuil_.numero()) {
      seuil_.numero(nnum);
      changed= true;
    }
    String nom= tfNom_.getText();
    if (!nom.equals(seuil_.nom())) {
      seuil_.nom(nom);
      changed= true;
    }
    double absc= ((Double)tfAbscisse_.getValue()).doubleValue();
    if (absc != seuil_.abscisse()) {
      seuil_.abscisse(absc);
      changed= true;
    }
    double zRup= ((Double)tfZRupture_.getValue()).doubleValue();
    if (zRup != seuil_.coteRupture()) {
      seuil_.coteRupture(zRup);
      changed= true;
    }
    double zCrete= ((Double)tfZCrete_.getValue()).doubleValue();
    if (zCrete != seuil_.coteCrete()) {
      seuil_.coteCrete(zCrete);
      changed= true;
    }
    double coefQ= ((Double)tfCoefQ_.getValue()).doubleValue();
    if (coefQ != seuil_.coefQ()) {
      seuil_.coefQ(coefQ);
      changed= true;
    }
    int index= cbEpaisseur_.getSelectedIndex();
    if (index != seuil_.epaisseur().value()) {
      seuil_.epaisseur(EnumMetierEpaisseurSeuil.from_int(index));
      changed= true;
    }
    double grad= ((Double)tfGradient_.getValue()).doubleValue();
    if (grad != seuil_.gradient()) {
      seuil_.gradient(grad);
      changed = true;
    }
    double debit = ( (Double) tfDebit_.getValue()).doubleValue();
    if (debit != seuil_.debit()) {
      seuil_.debit(debit);
      changed = true;
    }

    return changed;
  }
  @Override
  protected void setValeurs() {

    if (regime_.value() == EnumMetierRegime._TRANSCRITIQUE) {
      CtuluLibSwing.griserPanel(pnGradient_,true);
      lbMessageGradient_.setVisible(true);
      CtuluLibSwing.griserPanel(pnDebit_,true);
    }
    else {
      CtuluLibSwing.griserPanel(pnGradient_,false);
      lbMessageGradient_.setVisible(false);
      CtuluLibSwing.griserPanel(pnDebit_,true);
    }

    tfNumero_.setValue(new Integer(seuil_.numero()));
    tfAbscisse_.setValue(new Double(seuil_.abscisse()));
    tfNom_.setValue(seuil_.nom());
    tfZRupture_.setValue(new Double(seuil_.coteRupture()));
    tfZCrete_.setValue(new Double(seuil_.coteCrete()));
    tfCoefQ_.setValue(new Double(seuil_.coefQ()));
    cbEpaisseur_.setSelectedIndex(seuil_.epaisseur().value());
    tfGradient_.setValue(new Double(seuil_.gradient()));
    //En cas de r�cup�ration d'un ancien mod�le contenant des MetierSeuilTranscritique avec une loi Hydrogramme � la place du d�bit (Fudaa-Masc 1.2)
    if (seuil_ instanceof MetierSeuilTranscritique){
      MetierSeuilTranscritique seuilTrans_ = (MetierSeuilTranscritique) seuil_;
      if (seuilTrans_.loi() != null) {
        double debLoi = seuilTrans_.loi().q()[0];
        seuilTrans_.debit(debLoi);
        seuilTrans_.loi(null);
        //seuil_.debit(debLoi);
      }
    }
    tfDebit_.setValue(new Double(seuil_.debit()));

    String textAbsc= "";
    if (bief_ != null) {
      textAbsc= getS("du bief n�") + (bief_.indice()+1);
      if ((bief_.extrAmont().profilRattache() != null)
        && (bief_.extrAval().profilRattache() != null))
        textAbsc=
          textAbsc
            + getS(" entre ")
            + bief_.extrAmont().profilRattache().abscisse()
            + getS(" et ")
            + bief_.extrAval().profilRattache().abscisse();
      else
        textAbsc= textAbsc + " ("+getS("abscisses des extremit�s inconnues")+")";
    } else
      textAbsc= getS("bief inconnu");
    lbAbscisse_.setText(textAbsc);
  }
}
