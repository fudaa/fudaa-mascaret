/*
 * @file         Hydraulique1dSeuilVanneEditor.java
 * @creation     2000-11-29
 * @modification $Date: 2007-11-20 11:42:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiOuvertureVanne;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilVanne;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des seuils vannes (MetierSeuilVanne).<br>
 * Appeler si l'utilisateur clic sur une singularit� de type "Hydraulique1dReseauSeuil"
 * si la seuil est un seuil vanne pour un noyau fluvial.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().SEUIL_VANNE().editer().<br>
 * @version      $Revision: 1.17 $ $Date: 2007-11-20 11:42:43 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dSeuilVanneEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuButton btDefinirLoi_= new BuButton(getS("DEFINIR UNE LOI"));
  private BuTextField tfNumero_, tfNom_, tfAbscisse_, tfZRupture_, tfLargeur_;
  private BuLabel lbAbscisse_;
  private Hydraulique1dListeLoiCombo cmbNomLoi_;
  private BuPanel pnNumero_, pnSeuil_, pnNomLoi_, pnAbscisse_;
  private BuPanel pnLargeur_, pnNom_, pnZRupture_;
  private BuPanel pnCaracteristiques_,pnRupture_;
  private BuVerticalLayout loSeuil_, loNomLoi_,loVertical_;
  private BuHorizontalLayout loNumero_, loAbscisse_, loLargeur_, loNom_, loZRupture_;
  private MetierSeuilVanne seuil_;
  private MetierDonneesHydrauliques donneesHydro_;
  private MetierBief bief_;
  public Hydraulique1dSeuilVanneEditor() {
    this(null);
  }
  public Hydraulique1dSeuilVanneEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition seuil Vanne"));
    cmbNomLoi_ = new Hydraulique1dListeLoiCombo(Hydraulique1dListeLoiCombo.OUVERTURE_VANNE);
    seuil_= null;
    loNumero_= new BuHorizontalLayout(5, false, false);
    loNom_= new BuHorizontalLayout(5, false, false);
    loAbscisse_= new BuHorizontalLayout(5, false, false);
    loZRupture_= new BuHorizontalLayout(5, false, false);
    loLargeur_= new BuHorizontalLayout(5, false, false);
    loSeuil_= new BuVerticalLayout(5, false, false);
    loNomLoi_= new BuVerticalLayout(5, false, false);
    Container pnMain_= getContentPane();
    pnNumero_= new BuPanel();
    pnNumero_.setLayout(loNumero_);
    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    pnAbscisse_= new BuPanel();
    pnAbscisse_.setLayout(loAbscisse_);
    pnZRupture_= new BuPanel();
    pnZRupture_.setLayout(loZRupture_);
    pnLargeur_= new BuPanel();
    pnLargeur_.setLayout(loLargeur_);
    pnNomLoi_= new BuPanel();
    pnNomLoi_.setLayout(loNomLoi_);
    pnSeuil_= new BuPanel();
    pnSeuil_.setLayout(loSeuil_);
    pnSeuil_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));

    loVertical_= new BuVerticalLayout(5, false, false);
    pnCaracteristiques_= new BuPanel();
    pnCaracteristiques_.setLayout(loVertical_);
    pnCaracteristiques_.setBorder(BorderFactory.createTitledBorder(getS("Caracteristiques du seuil")));
    pnRupture_= new BuPanel();
    pnRupture_.setLayout(loVertical_);
    pnRupture_.setBorder(BorderFactory.createTitledBorder(getS("Rupture")));

    int textSize= 5;
    BuLabel lbZRupture= new BuLabel(getS("Cote de Rupture"));
    Dimension dimLabel= lbZRupture.getPreferredSize();
    dimLabel.width = dimLabel.width + 15;
    lbZRupture.setPreferredSize(dimLabel);
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(true);
    BuLabel lbNumero= new BuLabel(getS("N� seuil"));
    lbNumero.setPreferredSize(dimLabel);
    pnNumero_.add(lbNumero, 0);
    pnNumero_.add(tfNumero_, 1);
    tfNom_= new BuTextField();
    //tfNom_.setColumns(15);
    tfNom_.setEditable(true);
    BuLabel lbNom= new BuLabel(getS("Nom du seuil"));
    lbNom.setPreferredSize(dimLabel);
    pnNom_.add(lbNom, 0);
    pnNom_.add(tfNom_, 1);
    textSize= 10;
    tfAbscisse_= BuTextField.createDoubleField();
    tfAbscisse_.setColumns(textSize);
    tfAbscisse_.setEditable(true);
    BuLabel lbAbscisse= new BuLabel(getS("Abscisse"));
    lbAbscisse.setPreferredSize(dimLabel);
    pnAbscisse_.add(lbAbscisse, 0);
    pnAbscisse_.add(tfAbscisse_, 1);
    lbAbscisse_= new BuLabel("   ");
    pnAbscisse_.add(lbAbscisse_, 2);
    tfZRupture_= BuTextField.createDoubleField();
    tfZRupture_.setColumns(textSize);
    tfZRupture_.setEditable(true);
    //    BuLabel lbZRupture = new BuLabel("Cote de Rupture");
    //    lbZRupture.setPreferredSize(dimLabel);
    pnZRupture_.add(lbZRupture, 0);
    pnZRupture_.add(tfZRupture_, 1);
    tfLargeur_= BuTextField.createDoubleField();
    tfLargeur_.setColumns(textSize);
    tfLargeur_.setEditable(true);
    BuLabel lbLargeur= new BuLabel(getS("Largeur"));
    lbLargeur.setPreferredSize(dimLabel);
    pnLargeur_.add(lbLargeur, 0);
    pnLargeur_.add(tfLargeur_, 1);
    pnNomLoi_.add(new BuLabel(getS("nom de la loi de type 'Ouverture vanne'")), 0);
    pnNomLoi_.add(cmbNomLoi_, 1);
    btDefinirLoi_.setActionCommand("DEFINIR_LOI_OUVERTURE_VANNE");
    btDefinirLoi_.addActionListener(this);
    pnNomLoi_.add(btDefinirLoi_, 2);
    int n= 0;

    pnCaracteristiques_.add(pnLargeur_);
    pnCaracteristiques_.add(pnNomLoi_);
    pnRupture_.add(pnZRupture_);

    pnSeuil_.add(pnNumero_, n++);
    pnSeuil_.add(pnNom_, n++);
    pnSeuil_.add(pnAbscisse_, n++);
    pnSeuil_.add(pnCaracteristiques_, n++);
    pnSeuil_.add(pnRupture_, n++);
    pnMain_.add(pnSeuil_, BorderLayout.CENTER);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("ANNULER".equals(cmd)) {
      fermer();
    }
    else if (cmd.startsWith("DEFINIR_LOI")) {
        Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().setQualiteDEau(false);
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer();
    }
    else if ("VALIDER".equals(cmd)) {
      if (cmbNomLoi_.getValeurs() == null) {
        showBuError(getS("Aucune loi s�lectionn�e"), true);
        System.err.println(getS("Aucune loi selectionnee"));
        return;
      }
      if (getValeurs()) {
        firePropertyChange("singularite", null, seuil_);
      }
      fermer();
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _o) {
    if (_o instanceof MetierSeuilVanne) {
      seuil_= (MetierSeuilVanne)_o;
      setValeurs();
    } else if (_o instanceof MetierBief) {
      bief_ = (MetierBief) _o;
    }
    else if (_o instanceof MetierDonneesHydrauliques) {
      donneesHydro_ = (MetierDonneesHydrauliques) _o;
      cmbNomLoi_.setDonneesHydro(donneesHydro_);
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (seuil_ == null)
      return changed;
    int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != seuil_.numero()) {
      seuil_.numero(nnum);
      changed= true;
    }
    String nom= tfNom_.getText();
    if (!nom.equals(seuil_.nom())) {
      seuil_.nom(nom);
      changed= true;
    }
    double absc= ((Double)tfAbscisse_.getValue()).doubleValue();
    if (absc != seuil_.abscisse()) {
      seuil_.abscisse(absc);
      changed= true;
    }
    double z= ((Double)tfZRupture_.getValue()).doubleValue();
    if (z != seuil_.coteRupture()) {
      seuil_.coteRupture(z);
      changed= true;
    }
    double larg= ((Double)tfLargeur_.getValue()).doubleValue();
    if (larg != seuil_.largeur()) {
      seuil_.largeur(larg);
      changed= true;
    }
    MetierLoiOuvertureVanne loi= (MetierLoiOuvertureVanne)cmbNomLoi_.getValeurs();
    if (loi != seuil_.loi()) {
      seuil_.loi(loi);
      changed= true;
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(seuil_.numero()));
    tfNom_.setValue(seuil_.nom());
    tfAbscisse_.setValue(new Double(seuil_.abscisse()));
    tfZRupture_.setValue(new Double(seuil_.coteRupture()));
    tfLargeur_.setValue(new Double(seuil_.largeur()));
    cmbNomLoi_.initListeLoi();
    if (seuil_.getLoi() != null)
      cmbNomLoi_.setValeurs(seuil_.getLoi());
    String textAbsc= "";
    if (bief_ != null) {
      textAbsc= getS("du bief n�") + (bief_.indice()+1);
      if ((bief_.extrAmont().profilRattache() != null)
        && (bief_.extrAval().profilRattache() != null))
        textAbsc=
          textAbsc
            + getS(" entre ")
            + bief_.extrAmont().profilRattache().abscisse()
            + getS(" et ")
            + bief_.extrAval().profilRattache().abscisse();
      else
        textAbsc= textAbsc + " ("+getS("abscisses des extremit�s inconnues")+")";
    } else
      textAbsc= getS("bief inconnu");
    lbAbscisse_.setText(textAbsc);
  }
  // ObjetEventListener
  @Override
  public void objetCree(H1dObjetEvent e) {
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
	  MetierHydraulique1d src= e.getSource();
    String champ= e.getChamp();
    if (src == null)
      return;
    if ((src instanceof MetierDonneesHydrauliques)&&("lois".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
    if ((src instanceof MetierLoiOuvertureVanne)&&("nom".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
  }
}
