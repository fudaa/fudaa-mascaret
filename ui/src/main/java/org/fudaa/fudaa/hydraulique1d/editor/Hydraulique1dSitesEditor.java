/*
 * @file         Hydraulique1dSitesEditor.java
 * @creation     2004-07-06
 * @modification $Date: 2007-11-20 11:42:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.util.Iterator;
import java.util.List;

import javax.swing.JScrollPane;

import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSections;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresStockage;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dDialogContraintes;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauSiteModel;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuCutCopyPasteInterface;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeurs des tableaux de sites (ISITE) des points r�sultats (Editeur Param�tres r�sultats),
 * des sections de maillages.<br>
 * Classe m�re de l'�diteur des laisses de crues.<br>
 * Utilis� par les classes Hydraulique1dIHM_SitesStockage, Hydraulique1dIHM_SectionsParSections
 * et Hydraulique1dLaissesEditor.<br>
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:42:45 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dSitesEditor
    extends Hydraulique1dCustomizerImprimable
    implements BuCutCopyPasteInterface{
  protected Hydraulique1dTableauSiteModel modele_;
  protected Hydraulique1dTableauReel tableau_;

  public Hydraulique1dSitesEditor(String titre, Hydraulique1dTableauSiteModel modele) {
    this(titre,modele,EbliPreferences.DIALOG.CREER|EbliPreferences.DIALOG.SUPPRIMER);
  }
  public Hydraulique1dSitesEditor(String titre, Hydraulique1dTableauSiteModel modele, int modeActionPanel) {
    super(titre);
    modele_ = modele;
    tableau_ = new Hydraulique1dTableauReel(modele_);
    JScrollPane sp= new JScrollPane(tableau_);
    getContentPane().add(sp);
    setActionPanel(modeActionPanel);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  public Hydraulique1dSitesEditor() {
    this(Hydraulique1dResource.HYDRAULIQUE1D.getString("Sites"), new Hydraulique1dTableauSiteModel());
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equalsIgnoreCase("CREER")) {
      tableau_.ajouterLigne();
    }
    else if (cmd.equalsIgnoreCase("SUPPRIMER")) {
      tableau_.supprimeLignesSelectionnees();
    }
    else if (cmd.equalsIgnoreCase("VALIDER")) {
      List lignesIncorrectes = modele_.getListeLignesInCorrectes();
      if (lignesIncorrectes.isEmpty()) {
        getValeurs();
        fermer();
      } else {
        Iterator iter = lignesIncorrectes.iterator();
        String message="";
        while (iter.hasNext()) {
          message += iter.next();
        }
        int resp= new Hydraulique1dDialogContraintes(
          (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
          ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
          .getInformationsSoftware(),getS("Lignes incorrectes :")+"\n"+
          message,true).activate();

        if (resp==Hydraulique1dDialogContraintes.IGNORER) {
          getValeurs();
          fermer();
        }
      }
    }
    super.actionPerformed(_evt);
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierReseau) {
      modele_.setReseau((MetierReseau)_n);
    }
    else if (_n instanceof MetierDefinitionSectionsParSections) {
      modele_.setModelMetier((MetierDefinitionSectionsParSections)_n);
      super.setTitle(getS("Maillage d�fini section par section"));
    }
    else if (_n instanceof MetierParametresStockage) {
      modele_.setModelMetier((MetierParametresStockage)_n);
      super.setTitle(getS("Sites de stockage"));
    }

  }

  @Override
  public void setValeurs() {
    modele_.setValeurs();
  }
  @Override
  public boolean getValeurs() {
    return modele_.getValeurs();
  }

  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   * @param _g Graphics
   * @param _format PageFormat
   * @param _numPage int
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe, sinon
   *   <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _numPage) {
    return EbliPrinter.printComponent(_g, _format, tableau_, true, _numPage);
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void cut() {
    tableau_.cut();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void copy() {
    tableau_.copy();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void paste() {
    tableau_.paste();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void duplicate() {
    tableau_.duplicate();
  }

}
