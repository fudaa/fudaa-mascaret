/*
 * @file         Hydraulique1dSourceEditor.java
 * @creation     2006-03-18
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTracer;
import org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierTypeSource;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSource;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.LineChoiceBorder;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'une source de traceur (MetierSource).<br>
 * Appeler si l'utilisateur clic sur une singularit� de type "Hydraulique1dReseauSource".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().SOURCE().editer().<br>
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:42:49 $ by $Author: bmarchan $
 * @author       Olivier Pasteur
 */
public class Hydraulique1dSourceEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuButton btDefinirLoi_= new BuButton(getS("DEFINIR UNE LOI"));
  private BuTextField tfNom_, tfAbscisse_, tfLongueur_;
  private BuLabel lbAbscisse_;
  private Hydraulique1dListeLoiCombo cmbNomLoi_;
  private BuPanel pnNom_, pnSource_, pnNomLoi_, pnAbscisse_;
  private BuPanel pnTypeSource_;
  private BuRadioButton rbFlux_, rbVolumique_,rbSurfacique_;
  private BuVerticalLayout loSource_, loNomLoi_;
  private BuHorizontalLayout loNom_, loAbscisse_, loTypeSource_;
  private MetierSource source_;
  private MetierDonneesHydrauliques donneesHydro_;
  private MetierBief bief_;
  public Hydraulique1dSourceEditor() {
    this(null);
  }
  public Hydraulique1dSourceEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition d'une source de traceur"));
    cmbNomLoi_ = new Hydraulique1dListeLoiCombo(Hydraulique1dListeLoiCombo.TRACER);
    source_= null;
    loNom_= new BuHorizontalLayout(5, false, false);
    loAbscisse_= new BuHorizontalLayout(5, false, false);
    loTypeSource_= new BuHorizontalLayout(5, false, false);
    loSource_= new BuVerticalLayout(5, false, false);
    loNomLoi_= new BuVerticalLayout(5, false, false);
    Container pnMain_= getContentPane();
    pnNom_= new BuPanel();
    pnNom_.setLayout(loNom_);
    pnAbscisse_= new BuPanel();
    pnAbscisse_.setLayout(loAbscisse_);
    pnTypeSource_= new BuPanel();
    pnTypeSource_.setLayout(loTypeSource_);
    pnNomLoi_= new BuPanel();
    pnNomLoi_.setLayout(loNomLoi_);
    pnSource_= new BuPanel();
    pnSource_.setLayout(loSource_);
    pnSource_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    int textSize= 5;
    BuLabel lbTypeSource= new BuLabel(getS("Type de source"));
    Dimension dimLabel= lbTypeSource.getPreferredSize();
    tfNom_= new BuTextField();
    //tfNom_.setColumns(textSize);
    tfNom_.setEditable(true);
    BuLabel lbNom= new BuLabel(getS("Nom"));
    lbNom.setPreferredSize(dimLabel);
    pnNom_.add(lbNom, 0);
    pnNom_.add(tfNom_, 1);
    textSize= 10;
    tfAbscisse_= BuTextField.createDoubleField();
    tfAbscisse_.setColumns(textSize);
    tfAbscisse_.setEditable(true);
    BuLabel lbAbscisse= new BuLabel(getS("Abscisse"));
    lbAbscisse.setPreferredSize(dimLabel);
    pnAbscisse_.add(lbAbscisse, 0);
    pnAbscisse_.add(tfAbscisse_, 1);
    lbAbscisse_= new BuLabel("   ");
    pnAbscisse_.add(lbAbscisse_, 2);
    tfLongueur_= BuTextField.createDoubleField();
    tfLongueur_.setColumns(textSize);
    pnTypeSource_.add(lbTypeSource, 0);
    pnTypeSource_.add(constructPanelSource(), 1);
    pnNomLoi_.add(new BuLabel(getS("nom de la loi de type 'Tracer C( t )'")), 0);
    pnNomLoi_.add(cmbNomLoi_, 1);
    btDefinirLoi_.setActionCommand("DEFINIR_LOI_TRACER");
    btDefinirLoi_.addActionListener(this);
    pnNomLoi_.add(btDefinirLoi_, 2);
    pnSource_.add(pnNom_, 0);
    pnSource_.add(pnAbscisse_, 1);
    pnSource_.add(pnTypeSource_, 2);
    pnSource_.add(pnNomLoi_, 3);
    pnMain_.add(pnSource_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("FLUX".equals(cmd)) {
        tfLongueur_.setEnabled(false);
        tfLongueur_.setValue(new Double(0));
    } else if ("VOLUMIQUE".equals(cmd)) {
        tfLongueur_.setEnabled(true);
    } else if ("SURFACIQUE".equals(cmd)) {
        tfLongueur_.setEnabled(true);
    } else if ("ANNULER".equals(cmd)) {
        fermer();
    }
    else if (cmd.startsWith("DEFINIR_LOI_TRACER")) {
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().setQualiteDEau(true);
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer();
    }
    else if ("VALIDER".equals(cmd)) {
      if (!rbFlux_.isSelected()) {
        Double longueur = (Double) tfLongueur_.getValue();
        if (longueur == null) {
          showBuError(getS("Longueur vide pas autoris�e pour ce type de source"), true);
          return;
        }
        else if (longueur.doubleValue() < 0) {
          showBuError("Longueur n�gative pas autoris�e pour ce type de source", true);
          return;
        }
      }
      if (cmbNomLoi_.getValeurs() == null) {
        showBuError(getS("Aucune loi s�lectionn�e"), true);
        System.err.println("Aucune loi selectionnee");
        return;
      }
      if (getValeurs()) {
        firePropertyChange("singularite", null, source_);
      }
      fermer();
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _app) {
    if (!(_app instanceof MetierSource))
      return;
    if (_app == source_)
      return;
    source_= (MetierSource)_app;
    setValeurs();
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (source_ == null)
      return changed;

    //type
    EnumMetierTypeSource typeSource;
    if (rbVolumique_.isSelected())
    	typeSource = EnumMetierTypeSource.VOLUME;
    else if (rbSurfacique_.isSelected())
    	typeSource = EnumMetierTypeSource.SURFACIQUE;
    else //flux
    	typeSource = EnumMetierTypeSource.FLUX_UNITE_TEMPS;
    if (typeSource.value() != source_.type().value()) {
        source_.type(typeSource);
        changed = true;
    }


    double longu= 0;
    if (!rbFlux_.isSelected()) {
      longu = ( (Double) tfLongueur_.getValue()).doubleValue();
    }
    if (longu != source_.longueur()) {
      source_.longueur(longu);
      changed= true;
    }
    String nnom= tfNom_.getText();
    if (nnom != source_.nom()) {
      source_.nom(nnom);
      changed= true;
    }
    double absc= ((Double)tfAbscisse_.getValue()).doubleValue();
    if (absc != source_.abscisse()) {
      source_.abscisse(absc);
      changed= true;
    }
    MetierLoiTracer loi= (MetierLoiTracer) cmbNomLoi_.getValeurs();
    if (loi != source_.loi()) {
      source_.loi(loi);
      changed= true;
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    tfNom_.setText(source_.nom());
    tfAbscisse_.setValue(new Double(source_.abscisse()));


    tfLongueur_.setValue(new Double(source_.longueur()));


      if (source_.type().value() ==EnumMetierTypeSource._SURFACIQUE){
          rbSurfacique_.setSelected(true);
          tfLongueur_.setEnabled(true);
      }  else  if (source_.type().value()==EnumMetierTypeSource._VOLUME){
          rbVolumique_.setSelected(true);
          tfLongueur_.setEnabled(true);
      } else {
      rbFlux_.setSelected(true);
      tfLongueur_.setEnabled(false);
      tfLongueur_.setValue(new Double(0));
      }

    //loi
    cmbNomLoi_.initListeLoi();
    if (source_.loi() != null)
      cmbNomLoi_.setValeurs(source_.loi());

    //label bief
    String textAbsc= "";
    if (bief_ != null) {
      textAbsc= getS("du bief n�") + (bief_.indice()+1);
      if ((bief_.extrAmont().profilRattache() != null)
        && (bief_.extrAval().profilRattache() != null))
        textAbsc=
          textAbsc
            + getS(" entre ")
            + bief_.extrAmont().profilRattache().abscisse()
            + getS(" et ")
            + bief_.extrAval().profilRattache().abscisse();
      else
        textAbsc= textAbsc + " ("+getS("abscisses des extremit�s inconnues")+")";
    } else
      textAbsc= getS("bief inconnu");
    lbAbscisse_.setText(textAbsc);
  }
  // ObjetEventListener
  @Override
  public void objetCree(H1dObjetEvent e) {
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
/*    System.out.println("objetModifie e="+e.enChaine());
    System.out.println("e.getSource().getClass()="+e.getSource().getClass());
    System.out.println("e.getMessage()="+e.getMessage());
    System.out.println("e.getChamp()="+e.getChamp());*/
    MetierHydraulique1d src= e.getSource();
    String champ= e.getChamp();
    if (src == null)
      return;
    if ((src instanceof MetierDonneesHydrauliques)&&("lois".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
    if ((src instanceof MetierLoiTracer)&&("nom".equals(champ))) {
      cmbNomLoi_.initListeLoi();
    }
  }

  public void setDonneesHydrauliques(MetierDonneesHydrauliques _donneesHydro) {
    donneesHydro_= _donneesHydro;
    cmbNomLoi_.setDonneesHydro(donneesHydro_);
  }
  public void setBiefParent(MetierBief _bief) {
    bief_= _bief;
  }
  private BuPanel constructPanelSource() {
    BuPanel panel= new BuPanel();
    panel.setLayout(new GridBagLayout());
    panel.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    rbVolumique_= new BuRadioButton(getS("Volumique"));
    rbVolumique_.addActionListener(this);
    rbVolumique_.setActionCommand("VOLUMIQUE");
    rbSurfacique_= new BuRadioButton(getS("Surfacique"));
    rbSurfacique_.addActionListener(this);
    rbSurfacique_.setActionCommand("SURFACIQUE");
    rbFlux_= new BuRadioButton(getS("Flux par unit� de temps"));
    rbFlux_.addActionListener(this);
    rbFlux_.setActionCommand("FLUX");

    ButtonGroup groupeRadioBouton= new ButtonGroup();
    groupeRadioBouton.add(rbVolumique_);
    groupeRadioBouton.add(rbSurfacique_);
    groupeRadioBouton.add(rbFlux_);

    rbFlux_.setSelected(true);
    Insets inset3_5= new Insets(3, 5, 0, 0);
    Insets inset3_0= new Insets(3, 0, 0, 0);
    // radio bouton Volumique
    panel.add(
      rbVolumique_,
      new GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // ligne horizontale
    BuPanel pnLigneHorizontal= new BuPanel();
    pnLigneHorizontal.setPreferredSize(new Dimension(30, 5));
    pnLigneHorizontal.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    panel.add(
      pnLigneHorizontal,
      new GridBagConstraints(
        1,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_0,
        10,
        0));
    // label  "Nom de la loi de type "hydrogramme Q(t)"
    BuLabel lb= new BuLabel(getS("Longueur"));
    panel.add(
      lb,
      new GridBagConstraints(
        2,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
      panel.add(
      tfLongueur_,
      new GridBagConstraints(
        3,
        0,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    // radio bouton ponctuel
    panel.add(
      rbFlux_,
      new GridBagConstraints(
        0,
        2,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // radio bouton Volumique
    panel.add(
      rbSurfacique_,
      new GridBagConstraints(
        0,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));

    return panel;
  }


}
