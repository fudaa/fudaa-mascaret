package org.fudaa.fudaa.hydraulique1d.editor;
/*
 * @file         Hydraulique1dTableauZoneEditor.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.util.Iterator;
import java.util.List;

import javax.swing.JScrollPane;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dDialogContraintes;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauZoneFrottementModel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauZoneModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuPanel;

/**
 * Classe m�re des editeurs des tableaux de zones (IZONE).<br>
 * A savoir :
 * <ul>
 *   <li> des zones s�ches,
 *   <li> zones de frottement,
 *   <li> des zones de planim�trage,
 *   <li> des zones de carte des mailles.
 * </ul>
 * @version      $Revision: 1.7 $ $Date: 2007-11-20 11:42:49 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 **/
public class Hydraulique1dTableauZoneEditor
    extends Hydraulique1dCustomizerImprimable
    implements BuCutCopyPasteInterface {
  protected Hydraulique1dTableauZoneModel modele_;
  protected Hydraulique1dTableauReel tableau_;
  protected BuPanel panelCentral_;
  public Hydraulique1dTableauZoneEditor(String titre) {
    this(titre, new Hydraulique1dTableauZoneFrottementModel());
  }
  public Hydraulique1dTableauZoneEditor(String titre, Hydraulique1dTableauZoneModel modeleTableau) {
    super(titre);
    modele_ = modeleTableau;
    tableau_ = new Hydraulique1dTableauReel(modele_);
    JScrollPane sp= new JScrollPane(tableau_);

    panelCentral_ = new BuPanel();
    panelCentral_.setLayout(new BuBorderLayout(5, 5));
    panelCentral_.add(sp,BuBorderLayout.CENTER);

    getContentPane().add(panelCentral_);

    setActionPanel(EbliPreferences.DIALOG.CREER|EbliPreferences.DIALOG.SUPPRIMER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }

  public Hydraulique1dTableauZoneModel getTableauZoneModel() {
    return modele_;
  }
  public void setTableauZoneModel(Hydraulique1dTableauZoneModel modele) {
    modele_ = modele;
    tableau_.setModel(modele_);
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equalsIgnoreCase("CREER")) {
      tableau_.ajouterLigne();
    }
    else if (cmd.equalsIgnoreCase("SUPPRIMER")) {
      tableau_.supprimeLignesSelectionnees();
    }
    else if (cmd.equalsIgnoreCase("VALIDER")) {
    	validatePlanimetrage();
    }
    super.actionPerformed(_evt);
  }
  public boolean validatePlanimetrage() {
	  return validatePlanimetrage(true);
  }
  
  public boolean validatePlanimetrage(boolean doClose) {
	  {
	      List lignesIncorrectes = modele_.getListeLignesInCorrectes();
	      if (lignesIncorrectes.isEmpty()) {
	        String message = modele_.getMessageAvertissement();
	        if (! "".equals(message)) {
	            int resp= new Hydraulique1dDialogContraintes("Information",message,true).activate();
	            if (resp==Hydraulique1dDialogContraintes.IGNORER) {
	                getValeurs();
	                if(doClose)
	                	fermer();
	            } else 
	            	return false;
	        }
	        else {
	            getValeurs();
	            if(doClose)
	            	fermer();
	        }
	      }
	      else {
	        Iterator iter = lignesIncorrectes.iterator();
	        String message="";
	        while (iter.hasNext()) {
	          message += iter.next();
	        }
	        int resp= new Hydraulique1dDialogContraintes(getS("Lignes incorrectes :")+"\n"+
	          message,true).activate();

	        if (resp==Hydraulique1dDialogContraintes.IGNORER) {
	          getValeurs();
	          if(doClose)
	        	  fermer();
	        }else
	        	return false;
	      }
	    }
	  return true;
  }
  
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierReseau) {
      modele_.setReseau((MetierReseau)_n);
      modele_.setValeurs();
    }
  }
  @Override
  public void setValeurs() {
    modele_.setValeurs();
  }
  @Override
  public boolean getValeurs() {
    return modele_.getValeurs();
  }

  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   * @param _g Graphics
   * @param _format PageFormat
   * @param _numPage int
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe, sinon
   *   <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _numPage) {
    return EbliPrinter.printComponent(_g, _format, tableau_, true, _numPage);
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void cut() {
    tableau_.cut();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void copy() {
    tableau_.copy();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void paste() {
    tableau_.paste();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void duplicate() {
    tableau_.duplicate();
  }
}
