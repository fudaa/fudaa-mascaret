/*
 * @file         Hydraulique1dTableauxEditor.java
 * @creation     2004-09-15
 * @modification $Date: 2007-11-20 11:42:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import jxl.Workbook;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierLimiteCalcule;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimniHydrogramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiLimnigramme;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiOuvertureVanne;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiRegulation;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiTarage;
import org.fudaa.dodico.hydraulique1d.metier.singularite.EnumMetierTypeSource;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoirComportementZCoefQ;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilDenoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLimniAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilNoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAval;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTranscritique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilVanne;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSource;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Apport2;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Bief;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Casier;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_CasierLiaison;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Deversoir;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_ExtremiteLibre;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_LoiHydraulique2;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_SeuilAvecLoi;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_SeuilLoi;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_SeuilVanne;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Source;
import org.fudaa.fudaa.hydraulique1d.tableau.ActionDoubleClickEvent;
import org.fudaa.fudaa.hydraulique1d.tableau.ActionDoubleClickListener;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dAbstractTableauModel;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauCellRenderer;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogInput;
import com.memoire.bu.BuDialogWarning;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.fu.FuLib;

/**
 * Editeurs des tableaux de synth�se (IEtude).<br>
 * Appeler si l'utilisateur clic sur le menu "Synth�se/Tableaux".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().TABLEAUX().editer().<br>
 * @version      $Revision: 1.22 $ $Date: 2007-11-20 11:42:47 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public final class Hydraulique1dTableauxEditor
  extends Hydraulique1dCustomizerImprimable
  implements FocusListener {
  private MetierEtude1d ietude_;
  private Hydraulique1dTableau tableBiefs_, tableConfluents_, tableConditionsLimites_,
               tableSeuils_, tableApports_, tableSources_, tableDeversoirs_, tablePerteCharges_,
               tableLois_,tableLoisTracer_ ,tableCasiers_, tableLiaisonsCasCas_, tableLiaisonsRivCas_;
  private JScrollPane spBiefs_, spConfluents_, spConditionsLimites_,
                      spSeuils_, spApports_, spSources_, spDeversoirs_, spPerteCharges_, spLois_, spLoisTracer_,
                      spCasiers_, spLiaisonsCasCas_, spLiaisonsRivCas_;
  private List listeTables_, listeTablesModel_, listeJComponent_;
  private TableBiefModel tableBiefsModel_;
  private TableConfluentsModel tableConfluentsModel_;
  private TableConditionsLimitesModel tableConditionsLimitesModel_;
  private TableSeuilsModel tableSeuilsModel_;
  private TableApportsModel tableApportsModel_;
  private TableSourcesModel tableSourcesModel_;
  private TableDeversoirsModel tableDeversoirsModel_;
  private TablePerteChargesModel tablePerteChargesModel_;
  private TableLoisModel tableLoisModel_;
  private TableLoisTracerModel tableLoisTracerModel_;
  private TableCasiersModel tableCasiersModel_;
  private TableLiaisonsCasCasModel tableLiaisonsCasCasModel_;
  private TableLiaisonsRivCasModel tableLiaisonsRivCasModel_;
  private BuPanel panelCentral_,panelCasiers_;
  private BuScrollPane scrlPanel_;
  private BuButton btModifierCasiers;

  public Hydraulique1dTableauxEditor() {
    this(null);
  }

  public Hydraulique1dTableauxEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Tableaux de Synth�se"));
    listeTables_      = new ArrayList(11);
    listeTablesModel_ = new ArrayList(11);
    listeJComponent_  = new ArrayList(11);

    panelCentral_ = new BuPanel();
    panelCentral_.setLayout(new GridBagLayout());


    tableBiefsModel_ = new TableBiefModel();
    tableBiefs_ = new Hydraulique1dTableau(tableBiefsModel_);
    spBiefs_ = new JScrollPane(tableBiefs_);
    initTableaux(tableBiefs_, tableBiefsModel_,spBiefs_, getS("BIEFS"));

    tableConfluentsModel_ = new TableConfluentsModel();
    tableConfluents_ = new TableConfluent(tableConfluentsModel_);
    spConfluents_ = new JScrollPane(tableConfluents_);
    initTableaux(tableConfluents_, tableConfluentsModel_,spConfluents_, getS("CONFLUENTS"));

    tableConditionsLimitesModel_ = new TableConditionsLimitesModel();
    tableConditionsLimites_ = new Hydraulique1dTableau(tableConditionsLimitesModel_);
    spConditionsLimites_ = new JScrollPane(tableConditionsLimites_);
    initTableaux(tableConditionsLimites_, tableConditionsLimitesModel_,spConditionsLimites_,
                                      getS("CONDITIONS LIMITES - Extr�mit�s libres"));

    tableSeuilsModel_ = new TableSeuilsModel();
    tableSeuils_ = new Hydraulique1dTableau(tableSeuilsModel_);
    spSeuils_ = new JScrollPane(tableSeuils_);
    initTableaux(tableSeuils_, tableSeuilsModel_,spSeuils_, getS("SEUILS"));

    tableApportsModel_ = new TableApportsModel();
    tableApports_ = new Hydraulique1dTableau(tableApportsModel_);
    spApports_ = new JScrollPane(tableApports_);
    initTableaux(tableApports_, tableApportsModel_, spApports_, getS("APPORTS"));

    tableSourcesModel_ = new TableSourcesModel();
    tableSources_ = new Hydraulique1dTableau(tableSourcesModel_);
    spSources_ = new JScrollPane(tableSources_);
    initTableaux(tableSources_, tableSourcesModel_, spSources_, getS("SOURCES"));


    tableDeversoirsModel_ = new TableDeversoirsModel();
    tableDeversoirs_ = new Hydraulique1dTableau(tableDeversoirsModel_);
    spDeversoirs_ = new JScrollPane(tableDeversoirs_);
    initTableaux(tableDeversoirs_, tableDeversoirsModel_, spDeversoirs_, getS("DEVERSOIRS"));

    tablePerteChargesModel_ = new TablePerteChargesModel();
    tablePerteCharges_ = new Hydraulique1dTableau(tablePerteChargesModel_);
    spPerteCharges_ = new JScrollPane(tablePerteCharges_);
    initTableaux(tablePerteCharges_, tablePerteChargesModel_, spPerteCharges_,
                                                           getS("PERTES DE CHARGES"));

    tableLoisModel_ = new TableLoisModel();
    tableLois_ = new Hydraulique1dTableau(tableLoisModel_);
    spLois_ = new JScrollPane(tableLois_);
    initTableaux(tableLois_, tableLoisModel_, spLois_, getS("LOIS HYDRAULIQUES"));

    tableLoisTracerModel_ = new TableLoisTracerModel();
    tableLoisTracer_ = new Hydraulique1dTableau(tableLoisTracerModel_);
    spLoisTracer_ = new JScrollPane(tableLoisTracer_);
    initTableaux(tableLoisTracer_, tableLoisTracerModel_, spLoisTracer_, getS("LOIS TRACER"));



    tableCasiersModel_ = new TableCasiersModel();
    tableCasiers_ = new Hydraulique1dTableau(tableCasiersModel_);
    spCasiers_ = new JScrollPane(tableCasiers_);


    //Creation du bouton de modification des cotes des casiers
    BuPanel pnBtCasier = new BuPanel();
    pnBtCasier.setLayout(new FlowLayout());
    btModifierCasiers = new BuButton();
    btModifierCasiers.setText(getS("Modifier la cote initiale des casiers s�lectionn�s "));
    btModifierCasiers.setName("btModifierCasiers");
    btModifierCasiers.setActionCommand("MODIFIER_CASIER");
    btModifierCasiers.addActionListener(this);
    pnBtCasier.add(btModifierCasiers);


    panelCasiers_ = new BuPanel();
    panelCasiers_.setLayout(new BuBorderLayout());
    panelCasiers_.add(spCasiers_, BuBorderLayout.CENTER);
    panelCasiers_.add(pnBtCasier, BuBorderLayout.SOUTH);


    initTableaux(tableCasiers_, tableCasiersModel_, panelCasiers_, getS("CASIERS"));



    tableLiaisonsCasCasModel_ = new TableLiaisonsCasCasModel();
    tableLiaisonsCasCas_ = new Hydraulique1dTableau(tableLiaisonsCasCasModel_);
    spLiaisonsCasCas_ = new JScrollPane(tableLiaisonsCasCas_);
    initTableaux(tableLiaisonsCasCas_, tableLiaisonsCasCasModel_, spLiaisonsCasCas_,
                                                     getS("LIAISONS Casier-Casier"));

    tableLiaisonsRivCasModel_ = new TableLiaisonsRivCasModel();
    tableLiaisonsRivCas_ = new Hydraulique1dTableau(tableLiaisonsRivCasModel_);
    spLiaisonsRivCas_ = new JScrollPane(tableLiaisonsRivCas_);
    initTableaux(tableLiaisonsRivCas_, tableLiaisonsRivCasModel_, spLiaisonsRivCas_,
                                                      getS("LIAISONS Rivi�re-Casier"));

    scrlPanel_ = new BuScrollPane(panelCentral_);
    getContentPane().add(scrlPanel_);

    setActionPanel(EbliPreferences.DIALOG.EXPORTER);
    if (FuLib.isWindows()) {
      addAction("Excel", "EXCEL");
    }

    setNavPanel(EbliPreferences.DIALOG.FERMER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    System.out.println("cmd="+cmd);
    if ("FERMER".equals(cmd)) {
      fermer();
    }
    else if ("EXCEL".equals(cmd)) {
      lanceExcel();
    }
    else if ("EXPORTER".equals(cmd)) {
      exporter();
    }
    else if ("MODIFIER_CASIER".equals(cmd)) {
      modifierCasiers();
    }


  }
  /**
   * Impl�mentation de FocusListener.
   * Invoked when a component gains the keyboard focus.
   * @param e L'�v�nement.
   */
  @Override
  public void focusGained(FocusEvent e) {
    Iterator ite = listeTables_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dTableau table = (Hydraulique1dTableau)ite.next();
      if (!table.hasFocus()) {
        table.getSelectionModel().clearSelection();
      }
    }
  }

  /**
   * Impl�mentation de FocusListener.
   * Invoked when a component loses the keyboard focus.
   * @param e L'�v�nement.
   */
  @Override
  public void focusLost(FocusEvent e) {

  }

  @Override
  public void objetCree(H1dObjetEvent e) {
    if (e.getSource() instanceof MetierEtude1d) {
      ietude_ = (MetierEtude1d) e.getSource();
      setValeurs();
    }
    if (e.getSource() instanceof MetierNoeud) {
      setValeurs();
    }
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
    if (ietude_ == e.getSource()) {
      fermer();
    }
    if (e.getSource() instanceof MetierNoeud) {
      setValeurs();
    }
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
    if (e.getSource() instanceof MetierEtude1d) {
      if (ietude_ != e.getSource()) {
        ietude_ = (MetierEtude1d) e.getSource();
      }
    }
    if (e.getSource() instanceof MetierReseau) {
      setValeurs();
    }
    if (e.getSource() instanceof MetierBief) {
      setValeurs();
    }

    if (e.getSource() instanceof MetierNoeud) {
      setValeurs();
    }

    if (e.getSource() instanceof MetierSingularite) {
      setValeurs();
    }

    if (e.getSource() instanceof MetierDonneesHydrauliques) {
      setValeurs();
    }
    if (e.getSource() instanceof MetierLoiHydraulique) {
      setValeurs();
    }

}
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierEtude1d))
      return;
    MetierEtude1d param= (MetierEtude1d)_n;
    if (param == ietude_)
      return;
    ietude_= param;
    setValeurs();
  }
  public String lanceExcel() {
    String sortie ="";
    try {
      if (FuLib.isWindows()) {
        File fichierTemp = new File(System.getProperty("java.io.tmpdir") +
                                    "\\FudaaTableauxSynthesesTMP.xls");
        if (fichierTemp.exists()) {
          fichierTemp.delete();
        }
        fichierTemp.deleteOnExit();
        exportExcel(fichierTemp);
        String[] cmds = new String[4];
        cmds[0]="cmd";
        cmds[1]="/c";
        cmds[2]="start";
        cmds[3]=fichierTemp.getAbsolutePath();
        sortie = FuLib.runProgram(cmds);
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
      FudaaCommonImplementation fci = (FudaaCommonImplementation)
          ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
          .getImplementation();
      fci.error(getS("Ex�cution d'Excel"), getS("Un probl�me est survenu au lancement d'Excel"), false);
    }
    return sortie;
  }

  /**
     * Permet de modifier les casiers selectionn�s.
   */
  public void modifierCasiers() {
     System.out.println("in modifierCasier");
      try {
         int[] selection = tableCasiers_.getSelectedRows();
         if (selection.length==0) {
             BuDialogWarning w = new BuDialogWarning(null, null,
          getS("Aucun casier s�lectionn�"));
          w.activate();
          return;
         }
         BuDialogInput d = new BuDialogInput(null, null, getS("Modifier cote initiale"),
        getS("Cote initiale:"), "");
         if(d.activate()==JOptionPane.OK_OPTION){
             double cote = Double.parseDouble(d.getValue());
             int numeroCasier;
             for (int i = 0; i < selection.length; i++) {
                 numeroCasier = selection[i];
                 ietude_.reseau().casiers()[numeroCasier].coteInitiale(cote);
             }
         }

      }
      catch (Exception ex) {
          BuDialogError e = new BuDialogError(null, null,
          getS("Erreur de saisie")+" : "+getS("veuillez entrer un nombre"));
          e.activate();

      }
  }

  public void exporter() {
    String[] ext = {"xls"};
    String[] desc = {getS("Tableaux de synth�se au format Excel")};

    File fToSave = Hydraulique1dExport.chooseFile(ext, desc);
    if (fToSave == null) return;

    FudaaCommonImplementation fci = (FudaaCommonImplementation)
          ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
          .getImplementation();

    if (fToSave.exists()) {
      boolean b = fci.question(getS("Confirmation"), getS("Le fichier {0} existe d�j�. Voulez-vous l'�craser?", fToSave.getName()));
      if (!b) return;
    }

    try {
      exportExcel(fToSave);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      fci.error(getS("Erreur lors de l'exportation Excel :")+ex.getLocalizedMessage());
    }
  }
  /**
   * Export vers Excel.
   * @param fToSave File
   * @throws WriteException
   * @throws IOException
   */
  private void exportExcel(File fToSave) throws WriteException, IOException {
    WritableWorkbook classeur = Workbook.createWorkbook(fToSave);


    WritableSheet feuille =null;

    feuille = classeur.createSheet(ietude_.description(),0);
    WritableFont arial12ptBold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
    WritableCellFormat arial12BoldFormatNoBorder = new WritableCellFormat(arial12ptBold);

    int ligne=0;
    Iterator ite = listeTables_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dTableau table = (Hydraulique1dTableau)ite.next();
      if (table.getRowCount() >0) {
        feuille.addCell(new jxl.write.Label(0, ligne, table.getName(),
                                            arial12BoldFormatNoBorder));
        ligne++;
        ligne = table.ecritSurFeuilleExcel(feuille, 0, ligne);
        ligne = ligne+2;
      }
    }

    classeur.write();
    classeur.close();
  }

/*  public void show() {
    super.activate();
  }
*/
  @Override
  protected boolean getValeurs() {
    return false;
  }
  @Override
  protected void setValeurs() {
    try {
      tableConfluentsModel_.setParamGeneraux(ietude_.paramGeneraux());
      tableLoisModel_.setDDonneesHydrauliques(ietude_.donneesHydro());
      tableLoisTracerModel_.setDDonneesHydrauliques(ietude_.donneesHydro());

      getContentPane().removeAll();
      panelCentral_.removeAll();

      Iterator iteModel = listeTablesModel_.iterator();
      Iterator iteComp = listeJComponent_.iterator();


      int i = 0;
      while (iteModel.hasNext() && iteComp.hasNext()) {
        TableAbstractModel model = (TableAbstractModel) iteModel.next();
        JComponent comp = (JComponent) iteComp.next();
        model.setDReseau(ietude_.reseau());
        initVisuComponent(model, comp);
        if (comp.isVisible()) {
          GridBagConstraints cst = new GridBagConstraints(0, i, 1, 1, 1.0, 1.0,
              GridBagConstraints.CENTER,
              GridBagConstraints.BOTH,
              new Insets(5, 3, 0, 3), 10,
              10);
            panelCentral_.add(comp, cst);
          i++;
        }
      }
      scrlPanel_ = new BuScrollPane(panelCentral_);
      getContentPane().add(scrlPanel_);
      pack();
    }
    catch (NullPointerException ex) {
      // si probl�me ne fait rien....
    }
  }



 private void initTableaux(Hydraulique1dTableau tab, TableAbstractModel model, JComponent comp, String nom) {
   tab.addActionDoubleClickListener(model);
   tab.addFocusListener(this);

   listeTables_.add(tab);
   listeTablesModel_.add(model);
   listeJComponent_.add(comp);

   ((Hydraulique1dTableauCellRenderer)tab.getDefaultRenderer(null)).setItalicUneditable(false);
   comp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),nom));
   tab.setName(nom);
   if (comp instanceof BuPanel) comp.setPreferredSize(new Dimension(700, 150));
   else comp.setPreferredSize(new Dimension(700, 100));

   int i = listeTables_.size()-1;
   GridBagConstraints cst = new GridBagConstraints(0,i,1, 1, 1.0, 1.0,
                                                   GridBagConstraints.CENTER,
                                                   GridBagConstraints.BOTH,
                                                   new Insets(5, 3, 0, 3), 10,
                                                   10);
      panelCentral_.add(comp, cst);

 }



/*  private void initVisuScrollPane(Hydraulique1dAbstractTableauModel model, JScrollPane sp) {
    if (model.getRowCount() ==0) {
      sp.setVisible(false);
      pack();
    }
    else {
      sp.setVisible(true);
      pack();
    }
  }*/


 private void initVisuComponent(Hydraulique1dAbstractTableauModel model, JComponent comp) {
    if (model.getRowCount() ==0) {
      comp.setVisible(false);
      pack();
    }
    else {
      comp.setVisible(true);
      pack();
    }
  }


}

abstract class TableAbstractModel extends Hydraulique1dAbstractTableauModel
                                  implements ActionDoubleClickListener {
  protected MetierReseau reseau_;
  public TableAbstractModel(String[] nomsColonnes) {
    super(nomsColonnes);
  }

  public void setDReseau(MetierReseau reseau) {
    reseau_ = reseau;
    fireTableDataChanged();
  }
  @Override
  public void actionDoubleClick(ActionDoubleClickEvent event) {
    edite(event.getIndexeLigne());
  }
  abstract protected void edite(int indiceLigne);
  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0:
        return Integer.class;
      case 1:
        return String.class;
      case 2:
        return Integer.class;
    }
    return Double.class;
  }
}

class TableBiefModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("Num�ro"), getS("Nom"), getS("Abscisse d�but"),
                                          getS("Abscisse fin"),getS("Nombre de profils")};
  public TableBiefModel() {
    super(COL_NAMES);
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.biefs() == null) return 0;
    return reseau_.biefs().length;
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return Double.class;
      case 3: return Double.class;
      case 4: return Integer.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierBief b = reseau_.biefs()[row];
    String[] infos = b.getInfos();
    if (b == null) return null;
    switch(col) {
      case 0: return new Integer(b.indice()+1);
      case 1: return infos[0];
      case 2:
        if (b.extrAmont()==null) return null;
        if (b.extrAmont().profilRattache()==null) return null;
        return new Double(b.extrAmont().profilRattache().abscisse());
     case 3:
        if (b.extrAval()==null) return null;
        if (b.extrAval().profilRattache()==null) return null;
        return new Double(b.extrAval().profilRattache().abscisse());
     case 4:
        if (b.profils() == null) return null;
        return new Integer(b.profils().length);
    }
    return null;
  }
  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      Hydraulique1dIHMRepository ihmP = Hydraulique1dIHMRepository.getInstance();
      Hydraulique1dIHM_Bief ihm = ihmP.BIEF();
      ihm.setBief(reseau_.biefs()[row]);
      ihm.editer();
    }
  }
}
class TableConfluent extends Hydraulique1dTableau {
  public TableConfluent(TableConfluentsModel model) {
    super(model);
  }
  /**
 * Retourne la valeur la plus large � afficher pour une colonne.
 * @param col la valeur la plus large � afficher pour une colonne.
 * @return Object
 */
  @Override
  public Object getLongValues(int col) {
    if (col > 2)return "chaine   tr�s   longue";
    return super.getLongValues(col);
  }

}
class TableConfluentsModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("Num�ro"), getS("Nom"), getS("Extr�mit�s jointe"),
                                          getS("Abscisses"),getS("Ordonn�es"), getS("Angles")};
  private MetierParametresGeneraux paramGeneraux_;
  public TableConfluentsModel() {
    super(COL_NAMES);
  }
  public void setParamGeneraux(MetierParametresGeneraux paramGeneraux) {
    paramGeneraux_ = paramGeneraux;
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.noeudsConnectesBiefs() == null) return 0;
    return reseau_.noeudsConnectesBiefs().length;
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return String.class;
      case 3: return String.class;
      case 4: return String.class;
      case 5: return String.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierNoeud n = reseau_.noeudsConnectesBiefs()[row];
    String[] infos = n.getInfos();

    if (n == null) return null;
    switch(col) {
      case 0: return new Integer(n.numero());
      case 1: return infos[0];
      case 2: return extremitesJointe(n);
      case 3: return abscisses(n);
      case 4: return ordonnes(n);
      case 5: return angles(n);
    }
    return null;
  }
  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierNoeud n = reseau_.noeudsConnectesBiefs()[row];
      Hydraulique1dCustomizerImprimable editeur = null;
      if (paramGeneraux_.regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
        editeur= new Hydraulique1dNoeudTransEditor();
      }
      else {
        editeur= new Hydraulique1dNoeudFluvEditor();
      }
      editeur.setObject(n);
      editeur.setModal(true);
      editeur.pack();
      editeur.show();
    }
  }
  private String extremitesJointe(MetierNoeud n) {
    if (n.extremites() == null) return "";
    String res="";
    for (int i = 0; i < n.extremites().length; i++) {
      if (i==0)
        res +=n.extremites()[i].numero();
      else
        res +=", " + n.extremites()[i].numero();
    }
    return res;
  }
  private String abscisses(MetierNoeud n) {
    if (n.extremites() == null) return "";
    String res="";
    for (int i = 0; i < n.extremites().length; i++) {
      if (i==0) {
        if (n.extremites()[i].pointMilieu()==null) return "";
        res += n.extremites()[i].pointMilieu().x;
      }
      else
        res +=", " + n.extremites()[i].pointMilieu().x;
    }
    return res;
  }
  private String ordonnes(MetierNoeud n) {
    if (n.extremites() == null) return "";
    String res="";
    for (int i = 0; i < n.extremites().length; i++) {
      if (i==0) {
        if (n.extremites()[i].pointMilieu() == null) return "";
        res += n.extremites()[i].pointMilieu().y;
      }
      else
        res +=", " + n.extremites()[i].pointMilieu().y;
    }
    return res;
  }
  private String angles(MetierNoeud n) {
    if (n.extremites() == null) return "";
    String res="";
    for (int i = 0; i < n.extremites().length; i++) {
      if (i==0) {
        if (n.extremites()[i].pointMilieu() == null)return "";
        res += n.extremites()[i].angle();
      }
      else
        res +=", " + n.extremites()[i].angle();
    }
    return res;
  }
}
class TableConditionsLimitesModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("Num�ro"), getS("Nom condition"), getS("Num�ro extr�mit�"),
                                          getS("Type de condition"),getS("Num�ro de la loi")};
  public TableConditionsLimitesModel() {
    super(COL_NAMES);
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.extremitesLibres() == null) return 0;
    return reseau_.extremitesLibres().length;
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return Integer.class;
      case 3: return String.class;
      case 4: return Integer.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierExtremite e = reseau_.extremitesLibres()[row];
    try {
      switch (col) {
        case 0:
          return new Integer(row + 1);
        case 1:
          return e.conditionLimite().nom();
        case 2:
          return new Integer(e.numero());
        case 3:
          return typeCondition(e);
        case 4:
          return numLoi(e);
      }
    }
    catch (NullPointerException ex) {
      return null;
    }
    return null;
  }
  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      Hydraulique1dIHM_ExtremiteLibre ihmExtremite= Hydraulique1dIHMRepository.
                                                getInstance().EXTREMITE_LIBRE();
      ihmExtremite.setExtremite(reseau_.extremitesLibres()[row]);
      ihmExtremite.editer();
    }
  }
  private String typeCondition(MetierExtremite e) {
    if (e.conditionLimite() == null) return "";
    if (e.conditionLimite().loi() == null) {
      int valType = e.conditionLimite().typeLimiteCalcule().value();
      if (valType == EnumMetierLimiteCalcule._HAUTEUR_NORMALE)return getS("Hauteur normale")+" (7)";
      if (valType == EnumMetierLimiteCalcule._EVACUATION_LIBRE)return getS("Evacuation libre")+" (6)";
      return "";
    }
    MetierLoiHydraulique loi = e.conditionLimite().loi();
    if (loi instanceof MetierLoiHydrogramme)
      return getS("D�bit impos�")+" (1)"; // debit impose
    if (loi instanceof MetierLoiLimniHydrogramme)
      return getS("D�bit et cote impos�s")+" (1)"; // debit impose
    if (loi instanceof MetierLoiLimnigramme)
      return getS("Cote impos�e")+" (2)"; // cote impose
    if (loi instanceof MetierLoiTarage) {
      MetierLoiTarage l= (MetierLoiTarage)loi;
      if (l.amont())
        return getS("Z(Q)")+" (3)"; // Z(Q)
      return getS("Q(Z)")+" (4)"; // Q(Z)
    }
    if (loi instanceof MetierLoiRegulation)
      return getS("Zav(Qam)")+" (5)";
    return "";
  }

  private Integer numLoi(MetierExtremite e) {
    if (e.conditionLimite() == null) return null;
    if (e.conditionLimite().loi() == null) {
      return null;
    }
    return new Integer(e.conditionLimite().loi().indice()+1);
  }
}
class TableSeuilsModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("No"), getS("Nom"), getS("Bief No"), getS("Abscisse"),
                                          " "+getS("Type")+" ",getS("Cote cr�te"),getS("Cote rupture"),
                                          getS("Coef. d�bit"), getS("Grad. descente"),getS("Debit")};
  public TableSeuilsModel() {
    super(COL_NAMES);
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.seuils() == null) return 0;
    return reseau_.seuils().length;
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return Integer.class;
      case 3: return Double.class;
      case 4: return String.class;
      case 5: return Double.class;
      case 6: return Double.class;
      case 7: return Double.class;
      case 8: return Double.class;
      case 9: return Double.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierSeuil e = reseau_.seuils()[row];
    if (e == null) return null;
    switch(col) {
      case 0: return new Integer(e.numero());
      case 1: return e.nom();
      case 2: return new Integer(reseau_.getBiefContenantSingularite(e).indice()+1);
      case 3: return new Double(e.abscisse());
      case 4: return type(e);
      case 5: return coteCrete(e);
      case 6: return new Double(e.coteRupture());
      case 7: return coefDebit(e);
      case 8: return gradient(e);
      case 9: return debit(e);
    }
    return null;
  }
  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierSeuil s = reseau_.seuils()[row];
      if (s instanceof MetierSeuilTranscritique) { //SeuilTranscritique==SeuilLoi
        Hydraulique1dIHM_SeuilLoi ihmSeuilTrans =
            Hydraulique1dIHMRepository.getInstance().SEUIL_LOI();
        ihmSeuilTrans.setBief(reseau_.getBiefContenantSingularite(s));
        ihmSeuilTrans.setSeuil((MetierSeuilLoi)s);
        ihmSeuilTrans.editer();
      }
      else if (s instanceof MetierSeuilLoi) {
        Hydraulique1dIHM_SeuilLoi ihmSeuilLoi =
            Hydraulique1dIHMRepository.getInstance().SEUIL_LOI();
        ihmSeuilLoi.setBief(reseau_.getBiefContenantSingularite(s));
        ihmSeuilLoi.setSeuil((MetierSeuilLoi)s);
        ihmSeuilLoi.editer();
      }
      else if (s instanceof MetierSeuilVanne) {
        Hydraulique1dIHM_SeuilVanne ihmSeuilVanne =
            Hydraulique1dIHMRepository.getInstance().SEUIL_VANNE();
        ihmSeuilVanne.setBief(reseau_.getBiefContenantSingularite(s));
        ihmSeuilVanne.setSeuil(s);
        ihmSeuilVanne.editer();
      }
      else {
        Hydraulique1dIHM_SeuilAvecLoi ihmSeuilAvecLoi=
            Hydraulique1dIHMRepository.getInstance().SEUIL_AVEC_LOI();
        ihmSeuilAvecLoi.setBief(reseau_.getBiefContenantSingularite(s));
        ihmSeuilAvecLoi.setSeuil(s);
        ihmSeuilAvecLoi.editer();
      }
    }
  }
  private String type(MetierSeuil s) {
    try {
      if (s instanceof MetierSeuilVanne) {
        return getS("Vanne")+" (8)";
      }
      else if (s instanceof MetierSeuilNoye) {
        return getS("Zam")+"="+getS("f(Zav,Q)")+" (1)";
      }
      else if (s instanceof MetierSeuilTarageAval) {
        return getS("Q")+"="+getS("f(Zav)")+" (7)";
      }
      else if (s instanceof MetierSeuilTarageAmont) {
        return getS("Q")+"="+getS("f(Zam)")+" (6)";
      }
      else if (s instanceof MetierSeuilDenoye) {
        MetierSeuilDenoye d = (MetierSeuilDenoye) s;
        if (d.loi().amont()) {
          return getS("Z")+"="+getS("f(Q)")+" (2)";
        }
        return getS("Q")+"="+getS("f(Z)")+" (6)";
      }
      else if (s instanceof MetierSeuilLimniAmont) {
        return getS("Zam")+"="+getS("f(t)")+" (5)";
      }
      else if (s instanceof MetierSeuilGeometrique) {
        return getS("Profil de crete")+" (3)";
      }
      else if (s instanceof MetierSeuilTranscritique) {
        return getS("Zam")+"="+getS("f(Z cr�te , Coef. Q)")+" (4)";
      }
      else if (s instanceof MetierSeuilLoi) {
        return getS("Zam")+"="+getS("f(Z cr�te , Coef. Q)")+" (4)";
      }
      else {
        return null;
      }
    }
    catch (NullPointerException ex) {
      return null;
    }
  }

  private Double coteCrete(MetierSeuil s) {
    if (s instanceof MetierSeuilDenoye) {
      return new Double(((MetierSeuilDenoye)s).coteCrete());
    }
    else if (s instanceof MetierSeuilTranscritique) {
      return new Double(((MetierSeuilTranscritique)s).coteCrete());
    }
    else if (s instanceof MetierSeuilLoi) {
      return new Double(((MetierSeuilLoi)s).coteCrete());
    } else {
      return null;
    }
  }

  private Double coefDebit(MetierSeuil s) {
    if (s instanceof MetierSeuilTranscritique) {
      MetierSeuilTranscritique st = (MetierSeuilTranscritique)s;
        return new Double( st.coefQ());
    }
    else if (s instanceof MetierSeuilLoi) {
      return new Double(((MetierSeuilLoi)s).coefQ());
    } else {
      return null;
    }
  }
  private Double gradient(MetierSeuil s) {
    if (s instanceof MetierSeuilTranscritique) {
      MetierSeuilTranscritique st = (MetierSeuilTranscritique)s;
        return new Double( st.gradient());
    }
    else if (s instanceof MetierSeuilLoi) {
      MetierSeuilLoi st = (MetierSeuilLoi)s;
      return new Double(st.gradient());
    }
    else if (s instanceof MetierSeuilDenoye) {
      MetierSeuilDenoye st = (MetierSeuilDenoye)s;
      return new Double( st.gradient());
   } else {
     return null;
   }
  }

  private Double debit(MetierSeuil s) {
    if (s instanceof MetierSeuilTranscritique) {
      MetierSeuilTranscritique st = (MetierSeuilTranscritique)s;
        return new Double(st.debit());
    }
    else if (s instanceof MetierSeuilLoi) {
       MetierSeuilLoi st = (MetierSeuilLoi)s;
      return new Double(st.debit());
    }
     else {
     return null;
   }
}


}

class TableApportsModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("Num�ro"), getS("Nom"), getS("Bief No"),
                                          getS("Abscisse"),getS("Longueur"), getS("Num�ro de la loi")};
  public TableApportsModel() {
    super(COL_NAMES);
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.apports() == null) return 0;
    return reseau_.apports().length;
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return Integer.class;
      case 3: return Double.class;
      case 4: return Double.class;
      case 5: return Integer.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierApport a = reseau_.apports()[row];
    if (a == null) return null;
    switch(col) {
      case 0: return new Integer(a.numero());
      case 1: return a.nom();
      case 2: return new Integer(reseau_.getBiefContenantSingularite(a).indice()+1);
      case 3: return new Double(a.abscisse());
      case 4: return new Double(a.longueur());
      case 5: if (a.loi() == null) return null;
              return new Integer((a.loi().indice())+1);
    }
    return null;
  }
  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierApport a = reseau_.apports()[row];
      Hydraulique1dIHM_Apport2 ihmApport =
          Hydraulique1dIHMRepository.getInstance().APPORT2();
      ihmApport.setBief(reseau_.getBiefContenantSingularite(a));
      ihmApport.setApport(a);
      ihmApport.editer();
    }
  }
}
class TableSourcesModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("Num�ro"), getS("Nom"), getS("Bief No"),
                                          getS("Abscisse"),getS("Type"),getS("Longueur"), getS("Num�ro de la loi")};
  public TableSourcesModel() {
    super(COL_NAMES);
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.sources() == null) return 0;
    return reseau_.sources().length;
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return Integer.class;
      case 3: return Double.class;
      case 4: return String.class;
      case 5: return Double.class;
      case 6: return Integer.class;
    }
    return Object.class;
  }

  protected  String type(EnumMetierTypeSource t) {
    if (t.value()==EnumMetierTypeSource._FLUX_UNITE_TEMPS) return "FLUX_UNITE_TEMPS";
    else if (t.value()==EnumMetierTypeSource._SURFACIQUE) return "SURFACIQUE";
    else if (t.value()==EnumMetierTypeSource._VOLUME) return "VOLUMIQUE";
    else return getS("Type inconnu");
}

  @Override
  public Object getValueAt(int row, int col)
  {
    MetierSource s = reseau_.sources()[row];
    if (s == null) return null;
    switch(col) {
      case 0: return new Integer(s.numero());
      case 1: return s.nom();
      case 2: return new Integer(reseau_.getBiefContenantSingularite(s).indice()+1);
      case 3: return new Double(s.abscisse());
      case 4: return type(s.type());
      case 5: return new Double(s.longueur());
      case 6: if (s.loi() == null) return null;
              return new Integer((s.loi().indice())+1);
    }
    return null;
  }

  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierSource s = reseau_.sources()[row];
      Hydraulique1dIHM_Source ihmSources =
          Hydraulique1dIHMRepository.getInstance().SOURCE();
      ihmSources.setBief(reseau_.getBiefContenantSingularite(s));
      ihmSources.setSource(s);
      ihmSources.editer();
    }
  }
}
class TableDeversoirsModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("No"), getS("Nom"), getS("Bief No"),
                                          getS("Abscisse"),getS("Longueur"), getS("Type"),getS("No loi")
                                          ,getS("Coef. Q") ,getS("Cote cr�te")};
  public TableDeversoirsModel() {
    super(COL_NAMES);
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.deversoirs() == null) return 0;
    return reseau_.deversoirs().length;
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return Integer.class;
      case 3: return Double.class;
      case 4: return Double.class;
      case 5: return String.class;
      case 6: return Integer.class;
      case 7: return Double.class;
      case 8: return Double.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierDeversoir a = reseau_.deversoirs()[row];
    if (a == null) return null;
    switch(col) {
      case 0: return new Integer(a.numero());
      case 1: return a.nom();
      case 2: return new Integer(reseau_.getBiefContenantSingularite(a).indice()+1);
      case 3: return new Double(a.abscisse());
      case 4: return new Double(a.longueur());
      case 5: return type(a);
      case 6: if (a instanceof MetierDeversoirComportementLoi) {
                MetierDeversoirComportementLoi dl = (MetierDeversoirComportementLoi)a;
                if (dl.loi() == null) return null;
                return new Integer((dl.loi().indice())+1);
              }
              return null;
      case 7: if (a instanceof MetierDeversoirComportementZCoefQ) {
                MetierDeversoirComportementZCoefQ dZQ = (MetierDeversoirComportementZCoefQ)a;
                return new Double(dZQ.coefQ());
              }
              return null;
      case 8: if (a instanceof MetierDeversoirComportementZCoefQ) {
          MetierDeversoirComportementZCoefQ dZQ = (MetierDeversoirComportementZCoefQ)a;
          return new Double(dZQ.coteCrete());
        }
        return null;
    }
    return null;
  }

  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierDeversoir a = reseau_.deversoirs()[row];
      Hydraulique1dIHM_Deversoir ihmDeversoir=
            Hydraulique1dIHMRepository.getInstance().DEVERSOIR();
      ihmDeversoir.setBiefParent(reseau_.getBiefContenantSingularite(a));
      ihmDeversoir.setDeversoir(a);
      ihmDeversoir.editer();
    }
  }
  private String type(MetierDeversoir d) {
    if (d instanceof MetierDeversoirComportementZCoefQ) {
      return getS("Z Cr�te, Coef. Q")+" (1)";
    }
    else if (d instanceof MetierDeversoirComportementLoi) {
      return getS("Loi de tarage")+" (2)";
    }
    return null;
  }
}

class TablePerteChargesModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("Num�ro"), getS("Nom"), getS("Bief No"),
                                          getS("Abscisse"),getS("Coefficient")};
  public TablePerteChargesModel() {
    super(COL_NAMES);
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.pertesCharges() == null) return 0;
    return reseau_.pertesCharges().length;
  }

  @Override
  public Object getValueAt(int row, int col)
  {
    MetierPerteCharge a = reseau_.pertesCharges()[row];
    if (a == null) return null;
    switch(col) {
      case 0: return new Integer(a.numero());
      case 1: return a.nom();
      case 2: return new Integer(reseau_.getBiefContenantSingularite(a).indice()+1);
      case 3: return new Double(a.abscisse());
      case 4: return new Double(a.coefficient());
    }
    return null;
  }
  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierPerteCharge a = reseau_.pertesCharges()[row];
      Hydraulique1dPerteChargeEditor editeur=new Hydraulique1dPerteChargeEditor();
      editeur.setBiefParent(reseau_.getBiefContenantSingularite(a));
      editeur.setObject(a);
      editeur.pack();
      editeur.setModal(true);
      editeur.show();
    }
  }
}

class TableLoisModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("Num�ro"), getS("Nom"), getS("Type")};
  private MetierDonneesHydrauliques donnesHydro_;
  public TableLoisModel() {
    super(COL_NAMES);
  }
  public void setDDonneesHydrauliques(MetierDonneesHydrauliques donnesHydro) {
    donnesHydro_ = donnesHydro;
    fireTableDataChanged();
  }
  @Override
  public int getRowCount() {
    if (donnesHydro_ == null) return 0;
    if (donnesHydro_.getToutesLoisSaufTracerEtGeometrique() == null) return 0;
    return donnesHydro_.getToutesLoisSaufTracerEtGeometrique().length;
  }

  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierLoiHydraulique l = donnesHydro_.getToutesLoisSaufTracerEtGeometrique()[row];
      Hydraulique1dIHM_LoiHydraulique2 ihm = Hydraulique1dIHMRepository.
          getInstance().LOI_HYDRAULIQUE2();
      ihm.editer(l);
    }
  }


  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return String.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierLoiHydraulique l = donnesHydro_.getToutesLoisSaufTracerEtGeometrique()[row];
    if (l == null) return null;
    switch(col) {
      case 0: return  new Integer(l.indice()+1);// Integer(row+1);
      case 1: return l.nom();
      case 2: return type(l);
    }
    return null;
  }

  private String type(MetierLoiHydraulique l) {
    if (l instanceof MetierLoiHydrogramme) {
      return getS("Hydrogramme Q")+"="+getS("f(t)")+" (1)";
    }
    else if (l instanceof MetierLoiLimnigramme) {
      return getS("Limnigramme Z")+"="+getS("f(t)")+" (2)";
    }
    else if (l instanceof MetierLoiLimniHydrogramme) {
      return getS("Limni-hydrogramme (Q,Z)")+"="+getS("f(t)")+" (3)";
    }
    else if (l instanceof MetierLoiTarage) {
      MetierLoiTarage ll= (MetierLoiTarage)l;
      if (ll.amont()) {
        // amont <=> Z=f(Q)
        return getS("Courbe de tarage Z")+"="+getS("f(Q)")+" (4)";
      }
      // aval <=> Q=f(Z)
      return getS("Courbe de tarage Q")+"="+getS("f(Z)")+" (5)";
    }
    else if (l instanceof MetierLoiSeuil) {
      return getS("Loi seuil Zam")+"="+getS("f(Zaval,Q)")+" (6)";
    }
    else if (l instanceof MetierLoiRegulation) {
      return getS("Courbe de tarage Z")+"="+getS("f(Q)")+" (4)";
    }
    else if (l instanceof MetierLoiOuvertureVanne) {
      return getS("Ouverture vanne (Zinf,Zsup)")+"="+getS("f(t)")+" (7)";
    }
    return null;
  }
}

class TableLoisTracerModel extends TableAbstractModel {
     private final static String[] COL_NAMES = {getS("Num�ro"), getS("Nom")};
     private MetierDonneesHydrauliques donnesHydro_;
     public TableLoisTracerModel() {
         super(COL_NAMES);
     }

     public void setDDonneesHydrauliques(MetierDonneesHydrauliques donnesHydro) {
         donnesHydro_ = donnesHydro;
         fireTableDataChanged();
     }

  @Override
     public int getRowCount() {
         if (donnesHydro_ == null)
             return 0;
         if (donnesHydro_.getLoisTracer() == null)
             return 0;
         return donnesHydro_.getLoisTracer().length;
     }

  @Override
     protected void edite(int row) {
         if (row < getRowCount()) {
             MetierLoiHydraulique l = donnesHydro_.getLoisTracer()[row];
             Hydraulique1dIHM_LoiHydraulique2 ihm = Hydraulique1dIHMRepository.
                     getInstance().LOI_HYDRAULIQUE2();
             ihm.editer(l);
         }
     }
     /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierLoiHydraulique l = donnesHydro_.getLoisTracer()[row];
    if (l == null) return null;

    switch(col) {
      case 0: return new Integer(l.indice()+1) /*new Integer(row+1)*/;
      case 1: return l.nom();
    }
    return null;
  }


}
class TableCasiersModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("Num�ro"), getS("Nom"), getS("Cote initiale"), getS("  Planim�trage  ")};
  public TableCasiersModel() {
    super(COL_NAMES);
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.casiers() == null) return 0;
    return reseau_.casiers().length;
  }


  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return Double.class;
      case 3: return String.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierCasier c = reseau_.casiers()[row];
    if (c == null) return null;
    switch(col) {
      case 0: return new Integer(c.numero());
      case 1: return c.nom();
      case 2: return new Double(c.coteInitiale());
      case 3: if (c.isPlanimetrage()) return getS("Manuel")+" (1)";
              return getS("Automatique")+" (2)";
    }
    return null;
  }
  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierCasier c = reseau_.casiers()[row];
      Hydraulique1dIHM_Casier ihm = Hydraulique1dIHMRepository.
          getInstance().CASIER();
      ihm.setCasier(c);
      ihm.editer();
    }
  }
}
class TableLiaisonsCasCasModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("No"), getS("Casier amont"), getS("Casier aval"),
                                getS("Type"), getS("Cote"), getS("Coef. Q"), getS("Coef. d'act."), getS("Largueur"),getS("Longueur")};
  private java.util.List listeLiaisonCasCas_;
  public TableLiaisonsCasCasModel() {
    super(COL_NAMES);
  }
  @Override
  public void setDReseau(MetierReseau reseau) {
    reseau_ = reseau;
    initListeLiaisonsCasCas();
    fireTableDataChanged();
  }

  private void initListeLiaisonsCasCas() {
    listeLiaisonCasCas_ = new ArrayList();
    try {
      MetierLiaison[] liaisons = reseau_.liaisons();
      for (int i = 0; i < liaisons.length; i++) {
        if (liaisons[i].isCasierCasier()) {
          listeLiaisonCasCas_.add(liaisons[i]);
        }
      }
    }
    catch (NullPointerException ex) {
      // rien a faire la liste est vide si liaisons est null
    }
  }
  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.liaisons() == null) return 0;
    initListeLiaisonsCasCas();
    return listeLiaisonCasCas_.size();
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return String.class;
      case 3: return String.class;
      case 4: return Double.class;
      case 5: return Double.class;
      case 6: return Double.class;
      case 7: return Double.class;
      case 8: return Double.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierLiaison l = (MetierLiaison)listeLiaisonCasCas_.get(row);
    try {
      switch (col) {
        case 0: return new Integer(l.numero());
        case 1: return l.getCasierAmontRattache().nom();
        case 2: return l.getCasierAvalRattache().nom();
        case 3: return type(l);
        case 4: return new Double(l.getCote());
        case 5: return new Double(l.getCoefQ());
        case 6: return new Double(l.getCoefActivation());
        case 7: return new Double(l.getLargeur());
        case 8: return new Double(l.getLongueur());
      }
    }
    catch (Exception ex1) {
      return null;
    }
    return null;
  }

  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierLiaison l = (MetierLiaison)listeLiaisonCasCas_.get(row);
      Hydraulique1dIHM_CasierLiaison ihm = Hydraulique1dIHMRepository.
          getInstance().CASIER_LIAISON();
      ihm.setLiaison(l);
      ihm.editer();
    }
  }
  private String type(MetierLiaison l) {
    if (l.isSeuil()) {
      return getS("Seuil")+" (1)";
    }
    else if (l.isChenal()) {
      return getS("Chenal")+" (2)";
    }
    else if (l.isSiphon()) {
      return getS("Siphon")+" (3)";
    }
    else if (l.isOrifice()) {
      return getS("Orifice")+" (4)";
    }
    return null;
  }
}
class TableLiaisonsRivCasModel extends TableAbstractModel {
  private final static String[] COL_NAMES={getS("No"), getS("Casier associ�"), getS("Abscisse (n�bief)"),
                                getS("Type"), getS("Cote"), getS("Coef. Q"), getS("Coef. d'act."), getS("Largeur"),getS("Longueur")};
  //private DReseau reseau_;
  private java.util.List listeLiaisonRivCas_;
  public TableLiaisonsRivCasModel() {
    super(COL_NAMES);
  }
  @Override
  public void setDReseau(MetierReseau reseau) {
    reseau_ = reseau;
    initListeLiaisonsRivCas();
    fireTableDataChanged();
  }

  @Override
  public int getRowCount() {
    if (reseau_ == null) return 0;
    if (reseau_.liaisons() == null) return 0;
    initListeLiaisonsRivCas();
    return listeLiaisonRivCas_.size();
  }

  /**
   *  Returns <code>Object.class</code> regardless of <code>columnIndex</code>.
   *
   *  @param col  the column being queried
   *  @return the Object.class
   */
  @Override
  public Class getColumnClass(int col) {
    switch (col) {
      case 0: return Integer.class;
      case 1: return String.class;
      case 2: return String.class;
      case 3: return String.class;
      case 4: return Double.class;
      case 5: return Double.class;
      case 6: return Double.class;
      case 7: return Double.class;
      case 8: return Double.class;
    }
    return Object.class;
  }
  @Override
  public Object getValueAt(int row, int col)
  {
    MetierLiaison l = (MetierLiaison)listeLiaisonRivCas_.get(row);
    try {
      switch (col) {
        case 0:
          return new Integer(l.numero());
        case 1:
          return l.getCasierRattache().nom();
        case 2:
          int nbBief = l.getBiefRattache().indice() + 1;
          String Abscisse = l.getAbscisse()+" ("+nbBief+")";
          return  Abscisse;
        case 3:
          return type(l);
        case 4:
          return new Double(l.getCote());
        case 5:
          return new Double(l.getCoefQ());
        case 6:
          return new Double(l.getCoefActivation());
        case 7:
          return new Double(l.getLargeur());
        case 8:
          return new Double(l.getLongueur());
      }
    }
    catch (Exception ex) {
      return null;
    }
    return null;
  }

  @Override
  protected void edite(int row) {
    if (row < getRowCount()) {
      MetierLiaison l = (MetierLiaison)listeLiaisonRivCas_.get(row);
      Hydraulique1dIHM_CasierLiaison ihm = Hydraulique1dIHMRepository.
          getInstance().CASIER_LIAISON();
      ihm.setLiaison(l);
      ihm.editer();
    }
  }
  private void initListeLiaisonsRivCas() {
    listeLiaisonRivCas_ = new ArrayList();
    try {
      MetierLiaison[] liaisons = reseau_.liaisons();
      for (int i = 0; i < liaisons.length; i++) {
        if (!liaisons[i].isCasierCasier()) {
          listeLiaisonRivCas_.add(liaisons[i]);
        }
      }
    }
    catch (NullPointerException ex) {
      // rien a faire la liste est vide si liaisons est null
    }
  }
  private String type(MetierLiaison l) {
    if (l.isSeuil()) {
      return getS("Seuil")+" (1)";
    }
    else if (l.isChenal()) {
      return getS("Chenal")+" (2)";
    }
    else if (l.isSiphon()) {
      return getS("Siphon")+" (3)";
    }
    else if (l.isOrifice()) {
      return getS("Orifice")+" (4)";
    }
    return null;
  }
}
