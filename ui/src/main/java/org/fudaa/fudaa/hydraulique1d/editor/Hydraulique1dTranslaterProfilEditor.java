/*
 * @file         Hydraulique1dTranslaterProfilEditor.java
 * @creation     1999-12-28
 * @modification $Date: 2007-02-21 16:33:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur pour le choix de la m�thode de lissage.
 * @author Bertrand Marchand
 * @version 1.0
 */
public class Hydraulique1dTranslaterProfilEditor extends JDialog {
  private JPanel pnPrinc_=new JPanel();
  private Border bdpnPrinc_;
  private JPanel pnBoutons_=new JPanel();
  private BorderLayout lyThis_=new BorderLayout();
  private BuButton btOk_=new BuButton();
  private BuButton btAnnuler_=new BuButton();
  private BuGridLayout lyPrinc_=new BuGridLayout();
  private JLabel lbTx_=new JLabel();
  private JLabel lbTxUnite_=new JLabel();
  private JLabel lbTy_=new JLabel();
  private BuTextField tfTy_=BuTextField.createDoubleField();
  private JLabel lbTyUnite_=new JLabel();
  public int reponse=0;
  private BuTextField tfTx_=BuTextField.createDoubleField();
  public Hydraulique1dTranslaterProfilEditor(Frame parent) {
    super(parent);
    setModal(true);
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    customize();
    pack();
    setLocationRelativeTo(parent);
  }

  private void jbInit() throws Exception {
    bdpnPrinc_=BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
                                               BorderFactory.createEmptyBorder(5, 5, 5, 5));
    setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Translater les points"));
    this.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        this_windowClosed(e);
      }
    }); this.getContentPane().setLayout(lyThis_);
    pnPrinc_.setBorder(bdpnPrinc_);
    pnPrinc_.setLayout(lyPrinc_);
    btOk_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    btOk_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btOk_actionPerformed(e);
      }
    });
    btAnnuler_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Annuler"));
    btAnnuler_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btAnnuler_actionPerformed(e);
      }
    });
    lbTx_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Translation suivant l'abscisse X")+":");
    lbTy_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Translation suivant la cote Z")+":");
    lbTyUnite_.setText("m");
    tfTx_.setPreferredSize(new Dimension(70, 20));
    lbTxUnite_.setText("m");
    tfTy_.setPreferredSize(new Dimension(70, 20));
    lyPrinc_.setHgap(5);
    lyPrinc_.setVgap(2);
    pnPrinc_.add(lbTx_);
    pnPrinc_.add(tfTx_);
    pnPrinc_.add(lbTxUnite_);
    pnPrinc_.add(lbTy_);
    pnPrinc_.add(tfTy_);
    pnPrinc_.add(lbTyUnite_);
    pnBoutons_.add(btOk_);
    pnBoutons_.add(btAnnuler_);
    this.getContentPane().add(pnBoutons_, java.awt.BorderLayout.SOUTH);
    this.getContentPane().add(pnPrinc_, java.awt.BorderLayout.CENTER);
  }

  private void customize() {
    btOk_.setIcon(BuResource.BU.getIcon("valider"));
    btAnnuler_.setIcon(BuResource.BU.getIcon("annuler"));
  }

  public void btOk_actionPerformed(ActionEvent e) {
    reponse=1;
    setVisible(false);
  }

  public void btAnnuler_actionPerformed(ActionEvent e) {
    reponse=0;
    setVisible(false);
  }

  public void this_windowClosed(WindowEvent e) {
    reponse=0;
    setVisible(false);
  }

  /**
   * Definit les translations suivant x et y.
   * @param bornes Les bornes, dans l'ordre tx,ty.
   */
  public void setTranslation(double[] _txy) {
    tfTx_.setText(new DecimalFormat("0.00").format(_txy[0]));
    tfTy_.setText(new DecimalFormat("0.00").format(_txy[1]));
  }

  /**
   * Retourne les translation tx,ty.
   * @return null si pb.
   */
  public double[] getTranslation() {
    double tx=0;
    double ty=0;
    try { tx=Double.parseDouble(tfTx_.getText()); }
    catch (NumberFormatException _exc) {}
    try { ty=Double.parseDouble(tfTy_.getText()); }
    catch (NumberFormatException _exc) {}

    return new double[]{tx,ty};
  }

  /**
   * Pour tester l'ergonomie
   */
  public static void main(String[] _args) {
    Hydraulique1dTranslaterProfilEditor ed=new Hydraulique1dTranslaterProfilEditor(null);
    ed.setVisible(true);
    System.exit(0);
  }
}
