/*
 * @file         Hydraulique1dVariablesResultatsEditor.java
 * @creation     2000-12-06
 * @modification $Date: 2007-11-20 11:42:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresResultats;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des variables r�sultats suppl�mentaires (DParametresResultats).<br>
 * Appeler si l'utilisateur clic sur le bouton "AUTRES VARIABLES" de l'�diteur des param�tres r�sultats.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().VARIABLE_RESULTAT().editer().<br>
 * @version      $Revision: 1.11 $ $Date: 2007-11-20 11:42:44 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dVariablesResultatsEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  BuPanel pnVariables_;
  BuGridLayout loVariables_;
  BuCheckBox cbLargMiroirMin_,
    cbLargMiroirMaj_,
    cbLargMiroirStock_,
    cbLargMiroirMax_;
  BuCheckBox cbSectionMouilMin_,
    cbSectionMouilMaj_,
    cbSectionMouilStock_,
    cbPerimetreMouilMin_;
  BuCheckBox cbPerimetreMouilMaj_,
    cbRayonMin_,
    cbRayonMaj_,
    cbVolCumulActif_,
    cbVolCumulStock_;
  BuCheckBox cbHEauMoy_,
    cbZRiveGauche_,
    cbZRiveDroite_,
    cbZMinSimulation_,
    cbZMaxSimulation_;
  BuCheckBox cbContrainteFond_, cbChargeHydrau_, cbEnergieMax_;
  BuCheckBox cbTpsZMax_, cbTpsZMin_, cbTpsArriveOnde_, cbTpsQMax_;
  BuCheckBox cbQMajGauche_,
    cbQMajDroit_,
    cbQMax_,
    cbVitMaj_,
    cbVitMinMin_,
    cbVitMinMax_;
  BuCheckBox cbVitZMax_;
  BuCheckBox cbFroude_, cbCoefBeta_, cbCoefFrotMin_, cbCoefFrotMaj_;
  BuButton btVariableHydraulique_, btCoteHauteurEau_, btContrainteNRJ_;
  BuButton btTemps_, btDebitVitesse_, btNbAdimCoeff_;
  private MetierParametresResultats param_, paramOld_;
  public Hydraulique1dVariablesResultatsEditor() {
    this(null);
  }
  public Hydraulique1dVariablesResultatsEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition des autres variables r�sultats"));
    param_= null;
    loVariables_= new BuGridLayout(3, 2, 2, true, true);
    Container pnMain_= getContentPane();
    cbLargMiroirMin_= new BuCheckBox(getS("Largeur au miroir lit mineur"));
    cbLargMiroirMaj_= new BuCheckBox(getS("Largeur au miroir lit majeur"));
    cbLargMiroirStock_= new BuCheckBox(getS("Largeur au miroir zone de stockage"));
    cbLargMiroirMax_= new BuCheckBox(getS("Largeur au miroir maximale"));
    cbSectionMouilMin_= new BuCheckBox(getS("Section mouill�e lit mineur"));
    cbSectionMouilMaj_= new BuCheckBox(getS("Section mouill�e lit majeur"));
    cbSectionMouilStock_= new BuCheckBox(getS("Section mouill�e zone de stockage"));
    cbPerimetreMouilMin_= new BuCheckBox(getS("P�rim�tre mouill�e lit mineur"));
    cbPerimetreMouilMaj_= new BuCheckBox(getS("P�rim�tre mouill�e lit majeur"));
    cbRayonMin_= new BuCheckBox(getS("Rayon hydraulique lit mineur"));
    cbRayonMaj_= new BuCheckBox(getS("Rayon hydraulique lit majeur"));
    cbVolCumulActif_= new BuCheckBox(getS("Volume cumul� du lit actif"));
    cbVolCumulStock_= new BuCheckBox(getS("Volume cumul� des zones de stockage"));
    cbHEauMoy_= new BuCheckBox(getS("Hauteur d'eau moyenne"));
    cbZRiveGauche_= new BuCheckBox(getS("Cote rive gauche"));
    cbZRiveDroite_= new BuCheckBox(getS("Cote rive droite"));
    cbZMinSimulation_=
      new BuCheckBox(getS("Cote minimale au cours de la simulation"));
    cbZMaxSimulation_=
      new BuCheckBox(getS("Cote maximale au cours de la simulation"));
    cbContrainteFond_= new BuCheckBox(getS("Contrainte du fond"));
    cbChargeHydrau_= new BuCheckBox(getS("Charge hydraulique"));
    cbEnergieMax_= new BuCheckBox(getS("Energie maximale"));
    cbTpsZMax_= new BuCheckBox(getS("Temps d'obtention de la cote maximale"));
    cbTpsZMin_= new BuCheckBox(getS("Temps d'obtention de la cote minimale"));
    cbTpsArriveOnde_= new BuCheckBox(getS("Temps d'arriv�e de l'onde"));
    cbTpsQMax_= new BuCheckBox(getS("Temps d'obtention du d�bit maximale"));
    cbQMajGauche_= new BuCheckBox(getS("D�bit lit majeur gauche"));
    cbQMajDroit_= new BuCheckBox(getS("D�bit lit majeur droit"));
    cbQMax_= new BuCheckBox(getS("D�bit maximal"));
    cbVitMaj_= new BuCheckBox(getS("Vitesse dans le lit majeur"));
    cbVitMinMin_= new BuCheckBox(getS("Vitesse lit mineur minimale"));
    cbVitMinMax_= new BuCheckBox(getS("Vitesse lit mineur maximale"));
    cbVitZMax_= new BuCheckBox(getS("Vitesse � la cote maximale"));
    cbFroude_= new BuCheckBox(getS("Nombre de Froude"));
    cbCoefBeta_= new BuCheckBox(getS("Coeff. \u03b2 de la formule de debord"));
    cbCoefFrotMin_= new BuCheckBox(getS("Coeff. de frottement mineur"));
    cbCoefFrotMaj_= new BuCheckBox(getS("Coeff. de frottement majeur"));
    pnVariables_= new BuPanel();
    pnVariables_.setLayout(loVariables_);
    pnVariables_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    btVariableHydraulique_= new BuButton(getS("Variables hydrauliques"));
    btVariableHydraulique_.addActionListener(this);
    btVariableHydraulique_.setActionCommand("VARIABLE_HYDRAULIQUE");
    btCoteHauteurEau_= new BuButton(getS("Cotes et hauteurs d'eau"));
    btCoteHauteurEau_.addActionListener(this);
    btCoteHauteurEau_.setActionCommand("COTE_HAUTEUR_EAU");
    btContrainteNRJ_= new BuButton(getS("Contraintes et �nergie"));
    btContrainteNRJ_.addActionListener(this);
    btContrainteNRJ_.setActionCommand("CONTRAINTE_NRJ");
    btTemps_= new BuButton(getS("Temps"));
    btTemps_.addActionListener(this);
    btTemps_.setActionCommand("TEMPS");
    btDebitVitesse_= new BuButton(getS("D�bits et Vitesses"));
    btDebitVitesse_.addActionListener(this);
    btDebitVitesse_.setActionCommand("DEBIT_VITESSE");
    btNbAdimCoeff_= new BuButton(getS("Nombres adimentionnels et coefficients"));
    btNbAdimCoeff_.addActionListener(this);
    btNbAdimCoeff_.setActionCommand("NB_ADIM_COEFF");
    int n= 0;
    pnVariables_.add(btVariableHydraulique_, n++);
    pnVariables_.add(btCoteHauteurEau_, n++);
    pnVariables_.add(btDebitVitesse_, n++);
    pnVariables_.add(cbLargMiroirMin_, n++);
    pnVariables_.add(cbHEauMoy_, n++);
    pnVariables_.add(cbQMajGauche_, n++);
    pnVariables_.add(cbLargMiroirMaj_, n++);
    pnVariables_.add(cbZRiveGauche_, n++);
    pnVariables_.add(cbQMajDroit_, n++);
    pnVariables_.add(cbLargMiroirStock_, n++);
    pnVariables_.add(cbZRiveDroite_, n++);
    pnVariables_.add(cbQMax_, n++);
    pnVariables_.add(cbLargMiroirMax_, n++);
    pnVariables_.add(cbZMinSimulation_, n++);
    pnVariables_.add(cbVitMaj_, n++);
    pnVariables_.add(cbSectionMouilMin_, n++);
    pnVariables_.add(cbZMaxSimulation_, n++);
    pnVariables_.add(cbVitMinMin_, n++);
    pnVariables_.add(cbSectionMouilMaj_, n++);
    pnVariables_.add(btContrainteNRJ_, n++);
    pnVariables_.add(cbVitMinMax_, n++);
    pnVariables_.add(cbSectionMouilStock_, n++);
    pnVariables_.add(cbContrainteFond_, n++);
    pnVariables_.add(cbVitZMax_, n++);
    pnVariables_.add(cbPerimetreMouilMin_, n++);
    pnVariables_.add(cbChargeHydrau_, n++);
    pnVariables_.add(new BuLabel(""), n++);
    pnVariables_.add(cbPerimetreMouilMaj_, n++);
    pnVariables_.add(cbEnergieMax_, n++);
    pnVariables_.add(btNbAdimCoeff_, n++);
    pnVariables_.add(cbRayonMin_, n++);
    pnVariables_.add(btTemps_, n++);
    pnVariables_.add(cbFroude_, n++);
    pnVariables_.add(cbRayonMaj_, n++);
    pnVariables_.add(cbTpsZMax_, n++);
    pnVariables_.add(cbCoefBeta_, n++);
    pnVariables_.add(cbVolCumulActif_, n++);
    pnVariables_.add(cbTpsZMin_, n++);
    pnVariables_.add(cbCoefFrotMin_, n++);
    pnVariables_.add(cbVolCumulStock_, n++);
    pnVariables_.add(cbTpsArriveOnde_, n++);
    pnVariables_.add(cbCoefFrotMaj_, n++);
    pnVariables_.add(new BuLabel(""), n++);
    pnVariables_.add(cbTpsQMax_, n++);
    pnVariables_.add(new BuLabel(""), n++);
    pnMain_.add(new BuScrollPane(pnVariables_), BorderLayout.CENTER);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("ANNULER".equals(cmd)) {
      fermer();
    } else if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("parametresResultats", paramOld_, param_);
      }
      fermer();
    } else if ("VARIABLE_HYDRAULIQUE".equals(cmd)) {
      boolean sel= cbLargMiroirMin_.isSelected();
      cbLargMiroirMin_.setSelected(!sel);
      cbLargMiroirMaj_.setSelected(!sel);
      cbLargMiroirStock_.setSelected(!sel);
      cbLargMiroirMax_.setSelected(!sel);
      cbSectionMouilMin_.setSelected(!sel);
      cbSectionMouilMaj_.setSelected(!sel);
      cbSectionMouilStock_.setSelected(!sel);
      cbPerimetreMouilMin_.setSelected(!sel);
      cbPerimetreMouilMaj_.setSelected(!sel);
      cbRayonMin_.setSelected(!sel);
      cbRayonMaj_.setSelected(!sel);
      cbVolCumulActif_.setSelected(!sel);
      cbVolCumulStock_.setSelected(!sel);
    } else if ("COTE_HAUTEUR_EAU".equals(cmd)) {
      boolean sel= cbHEauMoy_.isSelected();
      cbHEauMoy_.setSelected(!sel);
      cbZRiveGauche_.setSelected(!sel);
      cbZRiveDroite_.setSelected(!sel);
      cbZMinSimulation_.setSelected(!sel);
      cbZMaxSimulation_.setSelected(!sel);
    } else if ("CONTRAINTE_NRJ".equals(cmd)) {
      boolean sel= cbContrainteFond_.isSelected();
      cbContrainteFond_.setSelected(!sel);
      cbChargeHydrau_.setSelected(!sel);
      cbEnergieMax_.setSelected(!sel);
    } else if ("TEMPS".equals(cmd)) {
      boolean sel= cbTpsZMax_.isSelected();
      cbTpsZMax_.setSelected(!sel);
      cbTpsZMin_.setSelected(!sel);
      cbTpsArriveOnde_.setSelected(!sel);
      cbTpsQMax_.setSelected(!sel);
    } else if ("DEBIT_VITESSE".equals(cmd)) {
      boolean sel= cbQMajGauche_.isSelected();
      cbQMajGauche_.setSelected(!sel);
      cbQMajDroit_.setSelected(!sel);
      cbQMax_.setSelected(!sel);
      cbVitMaj_.setSelected(!sel);
      cbVitMinMin_.setSelected(!sel);
      cbVitMinMax_.setSelected(!sel);
      cbVitZMax_.setSelected(!sel);
    } else if ("NB_ADIM_COEFF".equals(cmd)) {
      boolean sel= cbFroude_.isSelected();
      cbFroude_.setSelected(!sel);
      cbCoefBeta_.setSelected(!sel);
      cbCoefFrotMin_.setSelected(!sel);
      cbCoefFrotMaj_.setSelected(!sel);
    }
  }
  // Hydraulique1dCustomizer
  @Override
  public void setObject(MetierHydraulique1d _param) {
    if (!(_param instanceof MetierParametresResultats))
      return;
    if (_param == param_)
      return;
    param_= (MetierParametresResultats)_param;
    setValeurs();
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    if (param_ == null)
      return changed;
    boolean ch= cbChargeHydrau_.isSelected();
    if (ch != param_.existeVariable("CHAR")) {
      if (ch)
        param_.ajouteVariable("CHAR");
      else
        param_.supprimeVariable("CHAR");
      changed= true;
    }
    boolean beta= cbCoefBeta_.isSelected();
    if (beta != param_.existeVariable("BETA")) {
      if (beta)
        param_.ajouteVariable("BETA");
      else
        param_.supprimeVariable("BETA");
      changed= true;
    }
    boolean frMaj= cbCoefFrotMaj_.isSelected();
    if (frMaj != param_.existeVariable("KMAJ")) {
      if (frMaj)
        param_.ajouteVariable("KMAJ");
      else
        param_.supprimeVariable("KMAJ");
      changed=true;
    }
    boolean frMin=cbCoefFrotMin_.isSelected();
    if (frMin!=param_.existeVariable("KMIN")) {
      if (frMin)
        param_.ajouteVariable("KMIN");
      else
        param_.supprimeVariable("KMIN");
      changed= true;
    }
    boolean cFond= cbContrainteFond_.isSelected();
    if (cFond != param_.existeVariable("TAUF")) {
      if (cFond)
        param_.ajouteVariable("TAUF");
      else
        param_.supprimeVariable("TAUF");
      changed= true;
    }
    boolean cEner= cbEnergieMax_.isSelected();
    if (cEner != param_.existeVariable("EMAX")) {
      if (cEner)
        param_.ajouteVariable("EMAX");
      else
        param_.supprimeVariable("EMAX");
      changed= true;
    }
    boolean fraude= cbFroude_.isSelected();
    if (fraude != param_.existeVariable("FR")) {
      if (fraude)
        param_.ajouteVariable("FR");
      else
        param_.supprimeVariable("FR");
      changed= true;
    }
    boolean hEauMoy= cbHEauMoy_.isSelected();
    if (hEauMoy != param_.existeVariable("HMOY")) {
      if (hEauMoy)
        param_.ajouteVariable("HMOY");
      else
        param_.supprimeVariable("HMOY");
      changed= true;
    }
    boolean larMaj= cbLargMiroirMaj_.isSelected();
    if (larMaj != param_.existeVariable("B2")) {
      if (larMaj)
        param_.ajouteVariable("B2");
      else
        param_.supprimeVariable("B2");
      changed= true;
    }
    boolean larMax= cbLargMiroirMax_.isSelected();
    if (larMax != param_.existeVariable("BMAX")) {
      if (larMax)
        param_.ajouteVariable("BMAX");
      else
        param_.supprimeVariable("BMAX");
      changed= true;
    }
    boolean larMin= cbLargMiroirMin_.isSelected();
    if (larMin != param_.existeVariable("B1")) {
      if (larMin)
        param_.ajouteVariable("B1");
      else
        param_.supprimeVariable("B1");
      changed= true;
    }
    boolean larStoc= cbLargMiroirStock_.isSelected();
    if (larStoc != param_.existeVariable("BS")) {
      if (larStoc)
        param_.ajouteVariable("BS");
      else
        param_.supprimeVariable("BS");
      changed=true;
    }
    boolean perMaj=cbPerimetreMouilMaj_.isSelected();
    if (perMaj!=param_.existeVariable("P2")) {
      if (perMaj)
        param_.ajouteVariable("P2");
      else
        param_.supprimeVariable("P2");
      changed=true;
    }
    boolean perMin=cbPerimetreMouilMin_.isSelected();
    if (perMin!=param_.existeVariable("P1")) {
      if (perMin)
        param_.ajouteVariable("P1");
      else
        param_.supprimeVariable("P1");
      changed=true;
    }
    boolean qMajDr=cbQMajDroit_.isSelected();
    if (qMajDr!=param_.existeVariable("Q2D")) {
      if (qMajDr)
        param_.ajouteVariable("Q2D");
      else
        param_.supprimeVariable("Q2D");
      changed=true;
    }
    boolean qMajG=cbQMajGauche_.isSelected();
    if (qMajG!=param_.existeVariable("Q2G")) {
      if (qMajG)
        param_.ajouteVariable("Q2G");
      else
        param_.supprimeVariable("Q2G");
      changed=true;
    }
    boolean qMax=cbQMax_.isSelected();
    if (qMax!=param_.existeVariable("QMAX")) {
      if (qMax)
        param_.ajouteVariable("QMAX");
      else
        param_.supprimeVariable("QMAX");
      changed=true;
    }
    boolean rMaj=cbRayonMaj_.isSelected();
    if (rMaj!=param_.existeVariable("RH2")) {
      if (rMaj)
        param_.ajouteVariable("RH2");
      else
        param_.supprimeVariable("RH2");
      changed=true;
    }
    boolean rMin=cbRayonMin_.isSelected();
    if (rMin!=param_.existeVariable("RH1")) {
      if (rMin)
        param_.ajouteVariable("RH1");
      else
        param_.supprimeVariable("RH1");
      changed=true;
    }
    boolean sMaj=cbSectionMouilMaj_.isSelected();
    if (sMaj!=param_.existeVariable("S2")) {
      if (sMaj)
        param_.ajouteVariable("S2");
      else
        param_.supprimeVariable("S2");
      changed=true;
    }
    boolean sMin=cbSectionMouilMin_.isSelected();
    if (sMin!=param_.existeVariable("S1")) {
      if (sMin)
        param_.ajouteVariable("S1");
      else
        param_.supprimeVariable("S1");
      changed=true;
    }
    boolean sStoc=cbSectionMouilStock_.isSelected();
    if (sStoc!=param_.existeVariable("SS")) {
      if (sStoc)
        param_.ajouteVariable("SS");
      else
        param_.supprimeVariable("SS");
      changed=true;
    }
    boolean tOnde=cbTpsArriveOnde_.isSelected();
    if (tOnde!=param_.existeVariable("TOND")) {
      if (tOnde)
        param_.ajouteVariable("TOND");
      else
        param_.supprimeVariable("TOND");
      changed=true;
    }
    boolean tQMax=cbTpsQMax_.isSelected();
    if (tQMax!=param_.existeVariable("TQMA")) {
      if (tQMax)
        param_.ajouteVariable("TQMA");
      else
        param_.supprimeVariable("TQMA");
      changed=true;
    }
    boolean tZMax=cbTpsZMax_.isSelected();
    if (tZMax!=param_.existeVariable("TZMA")) {
      if (tZMax)
        param_.ajouteVariable("TZMA");
      else
        param_.supprimeVariable("TZMA");
      changed=true;
    }
    boolean tZMin=cbTpsZMin_.isSelected();
    if (tZMin!=param_.existeVariable("TZMI")) {
      if (tZMin)
        param_.ajouteVariable("TZMI");
      else
        param_.supprimeVariable("TZMI");
      changed=true;
    }
    boolean vMaj=cbVitMaj_.isSelected();
    if (vMaj!=param_.existeVariable("VMAJ")) {
      if (vMaj)
        param_.ajouteVariable("VMAJ");
      else
        param_.supprimeVariable("VMAJ");
      changed=true;
    }
    boolean vMinMax=cbVitMinMax_.isSelected();
    if (vMinMax!=param_.existeVariable("VSUP")) {
      if (vMinMax)
        param_.ajouteVariable("VSUP");
      else
        param_.supprimeVariable("VSUP");
      changed=true;
    }
    boolean vMinMin=cbVitMinMin_.isSelected();
    if (vMinMin!=param_.existeVariable("VINF")) {
      if (vMinMin)
        param_.ajouteVariable("VINF");
      else
        param_.supprimeVariable("VINF");
      changed=true;
    }
    boolean vZMax=cbVitZMax_.isSelected();
    if (vZMax!=param_.existeVariable("VZMX")) {
      if (vZMax)
        param_.ajouteVariable("VZMX");
      else
        param_.supprimeVariable("VZMX");
      changed=true;
    }
    boolean volAc=cbVolCumulActif_.isSelected();
    if (volAc!=param_.existeVariable("VOL")) {
      if (volAc)
        param_.ajouteVariable("VOL");
      else
        param_.supprimeVariable("VOL");
      changed=true;
    }
    boolean volStoc=cbVolCumulStock_.isSelected();
    if (volStoc!=param_.existeVariable("VOLS")) {
      if (volStoc)
        param_.ajouteVariable("VOLS");
      else
        param_.supprimeVariable("VOLS");
      changed=true;
    }
    boolean zMaxSim=cbZMaxSimulation_.isSelected();
    if (zMaxSim!=param_.existeVariable("ZMAX")) {
      if (zMaxSim)
        param_.ajouteVariable("ZMAX");
      else
        param_.supprimeVariable("ZMAX");
      changed=true;
    }
    boolean zMinSim=cbZMinSimulation_.isSelected();
    if (zMinSim!=param_.existeVariable("ZMIN")) {
      if (zMinSim)
        param_.ajouteVariable("ZMIN");
      else
        param_.supprimeVariable("ZMIN");
      changed=true;
    }
    boolean zRivDr=cbZRiveDroite_.isSelected();
    if (zRivDr!=param_.existeVariable("RDC")) {
      if (zRivDr)
        param_.ajouteVariable("RDC");
      else
        param_.supprimeVariable("RDC");
      changed=true;
    }
    boolean zRivG=cbZRiveGauche_.isSelected();
    if (zRivG!=param_.existeVariable("RGC")) {
      if (zRivG)
        param_.ajouteVariable("RGC");
      else
        param_.supprimeVariable("RGC");
      changed= true;
    }
    return changed;
  }
  @Override
  protected void setValeurs() {
    if (param_.existeVariable("CHAR"))
      cbChargeHydrau_.setSelected(true);
    else
      cbChargeHydrau_.setSelected(false);
    if (param_.existeVariable("BETA"))
      cbCoefBeta_.setSelected(true);
    else
      cbCoefBeta_.setSelected(false);
    if (param_.existeVariable("KMAJ"))
      cbCoefFrotMaj_.setSelected(true);
    else
      cbCoefFrotMaj_.setSelected(false);
    if (param_.existeVariable("KMIN"))
      cbCoefFrotMin_.setSelected(true);
    else
      cbCoefFrotMin_.setSelected(false);
    if (param_.existeVariable("TAUF"))
      cbContrainteFond_.setSelected(true);
    else
      cbContrainteFond_.setSelected(false);
    if (param_.existeVariable("EMAX"))
      cbEnergieMax_.setSelected(true);
    else
      cbEnergieMax_.setSelected(false);
    if (param_.existeVariable("FR"))
      cbFroude_.setSelected(true);
    else
      cbFroude_.setSelected(false);
    if (param_.existeVariable("HMOY"))
      cbHEauMoy_.setSelected(true);
    else
      cbHEauMoy_.setSelected(false);
    if (param_.existeVariable("B2"))
      cbLargMiroirMaj_.setSelected(true);
    else
      cbLargMiroirMaj_.setSelected(false);
    if (param_.existeVariable("BMAX"))
      cbLargMiroirMax_.setSelected(true);
    else
      cbLargMiroirMax_.setSelected(false);
    if (param_.existeVariable("B1"))
      cbLargMiroirMin_.setSelected(true);
    else
      cbLargMiroirMin_.setSelected(false);
    if (param_.existeVariable("BS"))
      cbLargMiroirStock_.setSelected(true);
    else
      cbLargMiroirStock_.setSelected(false);
    if (param_.existeVariable("P2"))
      cbPerimetreMouilMaj_.setSelected(true);
    else
      cbPerimetreMouilMaj_.setSelected(false);
    if (param_.existeVariable("P1"))
      cbPerimetreMouilMin_.setSelected(true);
    else
      cbPerimetreMouilMin_.setSelected(false);
    if (param_.existeVariable("Q2D"))
      cbQMajDroit_.setSelected(true);
    else
      cbQMajDroit_.setSelected(false);
    if (param_.existeVariable("Q2G"))
      cbQMajGauche_.setSelected(true);
    else
      cbQMajGauche_.setSelected(false);
    if (param_.existeVariable("QMAX"))
      cbQMax_.setSelected(true);
    else
      cbQMax_.setSelected(false);
    if (param_.existeVariable("RH2"))
      cbRayonMaj_.setSelected(true);
    else
      cbRayonMaj_.setSelected(false);
    if (param_.existeVariable("RH1"))
      cbRayonMin_.setSelected(true);
    else
      cbRayonMin_.setSelected(false);
    if (param_.existeVariable("S2"))
      cbSectionMouilMaj_.setSelected(true);
    else
      cbSectionMouilMaj_.setSelected(false);
    if (param_.existeVariable("S1"))
      cbSectionMouilMin_.setSelected(true);
    else
      cbSectionMouilMin_.setSelected(false);
    if (param_.existeVariable("SS"))
      cbSectionMouilStock_.setSelected(true);
    else
      cbSectionMouilStock_.setSelected(false);
    if (param_.existeVariable("TOND"))
      cbTpsArriveOnde_.setSelected(true);
    else
      cbTpsArriveOnde_.setSelected(false);
    if (param_.existeVariable("TQMA"))
      cbTpsQMax_.setSelected(true);
    else
      cbTpsQMax_.setSelected(false);
    if (param_.existeVariable("TZMA"))
      cbTpsZMax_.setSelected(true);
    else
      cbTpsZMax_.setSelected(false);
    if (param_.existeVariable("TZMI"))
      cbTpsZMin_.setSelected(true);
    else
      cbTpsZMin_.setSelected(false);
    if (param_.existeVariable("VMAJ"))
      cbVitMaj_.setSelected(true);
    else
      cbVitMaj_.setSelected(false);
    if (param_.existeVariable("VSUP"))
      cbVitMinMax_.setSelected(true);
    else
      cbVitMinMax_.setSelected(false);
    if (param_.existeVariable("VINF"))
      cbVitMinMin_.setSelected(true);
    else
      cbVitMinMin_.setSelected(false);
    if (param_.existeVariable("VZMX"))
      cbVitZMax_.setSelected(true);
    else
      cbVitZMax_.setSelected(false);
    if (param_.existeVariable("VOL"))
      cbVolCumulActif_.setSelected(true);
    else
      cbVolCumulActif_.setSelected(false);
    if (param_.existeVariable("VOLS"))
      cbVolCumulStock_.setSelected(true);
    else
      cbVolCumulStock_.setSelected(false);
    if (param_.existeVariable("ZMAX"))
      cbZMaxSimulation_.setSelected(true);
    else
      cbZMaxSimulation_.setSelected(false);
    if (param_.existeVariable("ZMIN"))
      cbZMinSimulation_.setSelected(true);
    else
      cbZMinSimulation_.setSelected(false);
    if (param_.existeVariable("RDC"))
      cbZRiveDroite_.setSelected(true);
    else
      cbZRiveDroite_.setSelected(false);
    if (param_.existeVariable("RGC"))
      cbZRiveGauche_.setSelected(true);
    else
      cbZRiveGauche_.setSelected(false);
  }
}
