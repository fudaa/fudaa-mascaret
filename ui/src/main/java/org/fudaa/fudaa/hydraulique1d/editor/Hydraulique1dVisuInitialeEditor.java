/*
 * @file         Hydraulique1dVisuInitialeEditor.java
 * @creation     2001-11-15
 * @modification $Date: 2007-11-20 11:42:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.util.Map;
import java.util.Vector;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierConditionsInitiales;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauPoint;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierZone;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Valeur;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheConditionsInitiales;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur de visualisation graphiques des conditions initiales (MetierConditionsInitiales).<br>
 * Appeler si l'utilisateur clic sur le bouton "VISUALISER" de l'�diteur des conditions initiales.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().VISU_INITIALE().editer().<br>
 * @version      $Revision: 1.16 $ $Date: 2007-11-20 11:42:42 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dVisuInitialeEditor
  extends Hydraulique1dCustomizerImprimable//       implements ActionListener, EbliPageable

implements ActionListener {
  BuPanel pnGraphe_= new BuPanel();
  BuBorderLayout loGraphe_= new BuBorderLayout();
  BuPanel pnWest_= new BuPanel();
  BuVerticalLayout loWest_= new BuVerticalLayout();
  BuPanel pnCenter_= new BuPanel();
  BuBorderLayout loCenter_= new BuBorderLayout();
  BuPanel pnGrapheEditeurPersonnaliseur_= new BuPanel();
  Hydraulique1dGrapheConditionsInitiales bgraphe_=
    new Hydraulique1dGrapheConditionsInitiales();
  BuComboBox cmbListeBief_= new BuComboBox();
  BGrapheEditeurAxes bgEditeurAxes_;
  BGraphePersonnaliseur bgPersonaliseur_;
  //private EbliPageableDelegate delegueImpression_;
  private MetierConditionsInitiales condInitiales_;
  private MetierReseau reseau_;
  public Hydraulique1dVisuInitialeEditor() {
    this(null);
  }
  public Hydraulique1dVisuInitialeEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Visualisation Conditions Initiales"));
    //delegueImpression_= new EbliPageableDelegate(this);
    bgPersonaliseur_= bgraphe_.getPersonnaliseurGraphe();
    bgEditeurAxes_= bgraphe_.getEditeurAxes();
    loGraphe_= new BuBorderLayout();
    pnWest_.setLayout(loWest_);
    pnCenter_.setLayout(loCenter_);
    pnGraphe_.setLayout(loGraphe_);
    int n= 0;
    pnWest_.add(cmbListeBief_, n++);
    pnWest_.add(bgEditeurAxes_, n++);
    pnWest_.add(bgPersonaliseur_, n++);
    pnCenter_.add(bgraphe_, BorderLayout.CENTER);
    pnGraphe_.add(pnWest_, BorderLayout.WEST);
    pnGraphe_.add(pnCenter_, BorderLayout.CENTER);
    pnGraphe_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    getContentPane().add(pnGraphe_, BuBorderLayout.CENTER);
    cmbListeBief_.setActionCommand("BIEF");
    cmbListeBief_.addActionListener(this);
    setNavPanel(EbliPreferences.DIALOG.FERMER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("FERMER".equals(cmd))
      fermer();
    else if ("BIEF".equals(cmd))
      visualiser();
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    return changed;
  }
  public String[] getEnabledActions() {
    String[] r= new String[] { "IMPRIMER", "PREVISUALISER", "MISEENPAGE" };
    return r;
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierConditionsInitiales) {
      MetierConditionsInitiales condInitiales= (MetierConditionsInitiales)_n;
      if (condInitiales == condInitiales_)
        return;
      condInitiales_= condInitiales;
    }
    if (_n instanceof MetierReseau) {
      MetierReseau reseau= (MetierReseau)_n;
      if (reseau == reseau_)
        return;
      reseau_= reseau;
    }
    if ((condInitiales_ != null) && (reseau_ != null))
      setValeurs();
  }
  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   *
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe,
   *         sinon <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _page) {
    return bgraphe_.print(_g, _format, _page);
  }
  /**
   * @return l'image produite dans la taille courante du composant.
   */
  @Override
  public BufferedImage produceImage(Map _params) {
    return bgraphe_.produceImage(null);
  }


  @Override
  protected void setValeurs() {
    cmbListeBief_.removeAllItems();
    String bief = getS("Bief")+" ";
    for (int i= 0; i < reseau_.biefs().length; i++) {
      cmbListeBief_.addItem(bief + (i + 1));
    }
    cmbListeBief_.setSelectedIndex(0);
  }
  protected void visualiser() {
    int indexBief= cmbListeBief_.getSelectedIndex();
    if (indexBief < 0)
      return;
    if (indexBief >= reseau_.biefs().length)
      return;
    Vector cbs= new Vector();
    CourbeDefault c1= getCourbeFond(reseau_.biefs()[indexBief]);
    if (c1 != null)
      cbs.add(c1);
    CourbeDefault c2= getCourbeLigneInitiale(indexBief + 1);
    if (c2 != null)
      cbs.add(c2);
    CourbeDefault[] c3= getCourbesZonesSeches(reseau_.biefs()[indexBief]);
    if (c3 != null) {
      for (int i= 0; i < c3.length; i++) {
        if (c3[i] != null)
          cbs.add(c3[i]);
      }
    }
    CourbeDefault[] crbAVisu= new CourbeDefault[cbs.size()];
    for (int i= 0; i < crbAVisu.length; i++) {
      crbAVisu[i]= (CourbeDefault)cbs.get(i);
    }
    if (crbAVisu.length > 0)
      bgraphe_.affiche(crbAVisu);
  }
  protected CourbeDefault getCourbeFond(MetierBief bief) {
    int nbProfil= bief.profils().length;
    if (nbProfil == 0)
      return null;
    CourbeDefault courbe= new CourbeDefault();
    courbe.titre_= getS("Fond");
    Vector vals= new Vector(nbProfil);
    for (int i= 0; i < nbProfil; i++) {
      Valeur v= new Valeur();
      MetierProfil p= bief.profils()[i];
      v.s_= p.abscisse();
      v.v_= p.getFond();
      vals.add(v);
    }
    courbe.valeurs_= vals;
    courbe.visible_= true;
    courbe.marqueurs_= false;
    courbe.aspect_.contour_= Color.black;
    return courbe;
  }
  protected CourbeDefault getCourbeLigneInitiale(int numBief) {
    MetierLigneEauInitiale ligne= condInitiales_.ligneEauInitiale();
    if (ligne == null)
      return null;
    MetierLigneEauPoint[] pts= ligne.points();
    if ((pts == null) || (pts.length == 0))
      return null;
    Vector vals= new Vector();
    for (int i= 0; i < pts.length; i++) {
      if (numBief == pts[i].numeroBief()) {
        Valeur v= new Valeur();
        v.s_= pts[i].abscisse();
        v.v_= pts[i].cote();
        vals.add(v);
      }
    }
    if (vals.isEmpty())
      return null;
    CourbeDefault courbe= new CourbeDefault();
    courbe.titre_= getS("Cote ligne d'eau initiale");
    courbe.valeurs_= vals;
    courbe.visible_= true;
    courbe.marqueurs_= false;
    courbe.aspect_.contour_= Color.blue;
    return courbe;
  }
  protected CourbeDefault[] getCourbesZonesSeches(MetierBief bief) {
    MetierZone[] zones= condInitiales_.zonesSeches();
    if ((zones == null) || (zones.length == 0))
      return null;
    try {
      double cote= bief.extrAval().profilRattache().getFond();
      Vector cbs= new Vector();
      for (int i= 0; i < zones.length; i++) {
        if (bief == zones[i].biefRattache()) {
          CourbeDefault courbe= new CourbeDefault();
          courbe.titre_= getS("zones s�ches");
          courbe.visible_= true;
          courbe.marqueurs_= false;
          courbe.aspect_.contour_= Color.green;
          Valeur v1= new Valeur();
          v1.s_= zones[i].abscisseDebut();
          v1.v_= cote;
          Valeur v2= new Valeur();
          v2.s_= zones[i].abscisseFin();
          v2.v_= cote;
          courbe.valeurs_= new Vector(2);
          courbe.valeurs_.add(v1);
          courbe.valeurs_.add(v2);
          cbs.add(courbe);
        }
      }
      if (cbs.isEmpty())
        return null;
      CourbeDefault[] res= new CourbeDefault[cbs.size()];
      for (int i= 0; i < res.length; i++) {
        res[i]= (CourbeDefault)cbs.get(i);
      }
      return res;
    } catch (NullPointerException ex) {
      return null;
    }
  }
}
