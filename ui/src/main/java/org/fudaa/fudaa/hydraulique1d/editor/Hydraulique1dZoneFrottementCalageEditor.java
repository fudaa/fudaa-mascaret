/*
 * @file         Hydraulique1dZoneFrottementCalageEditor.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:42:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeCoefficient;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierParametresCalageAuto;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneZoneFrottementTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauZoneModel;
import org.fudaa.fudaa.hydraulique1d.tableau.ZoneFrottementCalageModelInterface;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;

import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur des zones de frottements � caler ou cal�es.<br>
 * Appel� si l'utilisateur clic sur le bouton "ZONES DE FROTTEMENTS INITIALES"
 * ou ZONES DE FROTTEMENT du menu Calage <br>
 * Lanc� par l'instruction Hydraulique1dIHMRepository.getInstance().ZONES_FROTTEMENT_CALAGE().editer().<br>
 * Voir les classes Hydraulique1dTableauZoneEditor.
 * @see org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dTableauZoneEditor
 * @author Bertrand Marchand
 * @version $Revision: 1.4 $ $Date: 2007-11-20 11:42:49 $ by $Author: bmarchan $
 */
public class Hydraulique1dZoneFrottementCalageEditor
    extends Hydraulique1dTableauZoneEditor {

  public static final int MODE_COEFS_A_CALER=0;
  public static final int MODE_COEFS_CALES=1;
  private int mode_=-1;

  public Hydraulique1dZoneFrottementCalageEditor(int mode, final Hydraulique1dTableauZoneModel model) {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zones de frottement"), model );
    
    String[] itemsCoeff = {Hydraulique1dResource.HYDRAULIQUE1D.getString("Coefficients de Strickler K"),
    		Hydraulique1dResource.HYDRAULIQUE1D.getString("Coefficients de Manning n")};
	cmbCoef_ = new JComboBox(itemsCoeff);
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equalsIgnoreCase("IMPORTER")) {
      File fichier = Hydraulique1dImport.chooseFile("pro",getS("fichier g�om�trie Lido 2.0"));
      if (fichier != null) {
        Hydraulique1dLigneZoneFrottementTableau[] lignes = Hydraulique1dImport.importFrottementPRO_LIDO(fichier);
        if ((lignes == null)) {
         new BuDialogError(
           (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
           ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
             .getInformationsSoftware(),
           getS("ERREUR")+": "+getS("l'importation des coefficients de frottement")+"\n" + getS("a �chou�."))
           .activate();
         return;
       }
       if ((lignes.length == 0)) {
         new BuDialogError(
           (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
           ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
             .getInformationsSoftware(),
           getS("ERREUR")+": "+getS("aucun point n'est")+"\n" + getS("disponible dans cette import!"))
           .activate();
         return;
       }
       modele_.estimationNumeroBief(lignes);
       modele_.setTabLignes(lignes);
      }
    }
    super.actionPerformed(_evt);
  }

  JComboBox cmbCoef_ = null;
  BuPanel panelFrottements =null;
	
	public BuPanel getPanelFrottement() {
		if(panelFrottements == null) {
			panelFrottements =new BuPanel(new BorderLayout());
			BuPanel panelInt = new BuPanel(new FlowLayout(FlowLayout.LEFT));
			
			BuLabelMultiLine lbmTyp = new BuLabelMultiLine(getS("Type de Coefficient"));
			lbmTyp.setHorizontalAlignment(SwingConstants.RIGHT);
			panelInt.add(lbmTyp);
			
			
			panelInt.add(cmbCoef_);
			
			
			panelFrottements.add(panelInt,BorderLayout.SOUTH);
			
			
			TitledBorder bordertitle= new TitledBorder( getS("Param�tres du frottement"));
			panelFrottements.setBorder(bordertitle);
			
		}
		return panelFrottements;
	}
  
  /**
   * D�finit si l'editeur repr�sente les zones � caler, ou les zones cal�es.
   */
  public void setMode(int _mode) {
    if (mode_==_mode) return;

    mode_=_mode;
    String titre=null;
    if (mode_==MODE_COEFS_A_CALER) {
      titre=getS("Zones de frottement");
      setActionPanel(EbliPreferences.DIALOG.CREER|EbliPreferences.DIALOG.SUPPRIMER| EbliPreferences.DIALOG.IMPORTER);
      setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
      getContentPane().add(getPanelFrottement(),BorderLayout.NORTH);
      
    }
    else {
      titre=getS("Zones de frottement cal�es");
      setActionPanel(EbliPreferences.DIALOG.AUCUN);
      setNavPanel(EbliPreferences.DIALOG.FERMER);
    }
    setTitle(titre);

    ZoneFrottementCalageModelInterface modele;
    modele=(ZoneFrottementCalageModelInterface)modele_;
    
  }

  
  MetierCalageAuto paramCalage_ = null;
  // Surchage pour r�cup�ration zones frottement du calage.
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierEtude1d) {
      MetierEtude1d etude=(MetierEtude1d)_n;
      ZoneFrottementCalageModelInterface modele;
      modele=(ZoneFrottementCalageModelInterface)modele_;
      paramCalage_ = etude.calageAuto();
      modele.setCalageAuto(paramCalage_);

      super.setObject(etude.reseau());
    }
  }

@Override
public void setValeurs() {
	super.setValeurs();
	if(cmbCoef_ != null && paramCalage_!= null) {
		MetierParametresCalageAuto params = paramCalage_.parametres();
	if(params.typeCoefficient().value() == EnumMetierTypeCoefficient._STRICKLER)
		cmbCoef_.setSelectedIndex(0);
	else
		if(params.typeCoefficient().value() == EnumMetierTypeCoefficient._MANNING)				
			cmbCoef_.setSelectedIndex(1);
	}
}

@Override
public boolean getValeurs() {
	boolean changed =  super.getValeurs();
	if(cmbCoef_ != null && paramCalage_!= null) {
		MetierParametresCalageAuto params = paramCalage_.parametres();
		EnumMetierTypeCoefficient typCoeff = null;
		if(cmbCoef_.getSelectedIndex() == 0)
			typCoeff = EnumMetierTypeCoefficient.STRICKLER;
		else
			typCoeff = EnumMetierTypeCoefficient.MANNING;
		if (typCoeff.value() != params.typeCoefficient().value()) {
			params.typeCoefficient(typCoeff);
			changed = true;
		}
	}
	return changed;
}
}
