/*
 * @file         Hydraulique1dZoneFrottementEditor.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:42:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;
import java.awt.event.ActionEvent;
import java.io.File;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeFrottement;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneZoneFrottementTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauZoneFrottementModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur des zones de frottements.<br>
 * Appeler si l'utilisateur clic sur le bouton "ZONES DE FROTTEMENTS" de l'�diteur des param�tres g�n�raux.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().ZONES_FROTTEMENT().editer().<br>
 * Voir les classes Hydraulique1dTableauZoneEditor.
 * @see org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dTableauZoneEditor
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.9 $ $Date: 2007-11-20 11:42:43 $ by $Author: bmarchan $
 */
public class Hydraulique1dZoneFrottementEditor
    extends Hydraulique1dTableauZoneEditor {

  BuButton btImporterCalage;

  public Hydraulique1dZoneFrottementEditor() {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zones de frottement"), new Hydraulique1dTableauZoneFrottementModel());
    setActionPanel(EbliPreferences.DIALOG.CREER|EbliPreferences.DIALOG.SUPPRIMER| EbliPreferences.DIALOG.IMPORTER);
    if (CGlobal.AVEC_CALAGE_AUTO){
    btImporterCalage=(BuButton)addAction(getS("Importer r�sultats calage"),"IMPORTER_CALAGE");
    btImporterCalage.setToolTipText(getS("Extrait les coefficients de frottement calcul�s lors du calage"));
    btImporterCalage.addActionListener(this);
    }
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equalsIgnoreCase("IMPORTER")) {
      File fichier = Hydraulique1dImport.chooseFile("pro",getS("fichier g�om�trie Lido 2.0"));
      if (fichier != null) {
        Hydraulique1dLigneZoneFrottementTableau[] lignes = Hydraulique1dImport.importFrottementPRO_LIDO(fichier);
        if ((lignes == null)) {
         new BuDialogError(
           (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
           ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
             .getInformationsSoftware(),
           getS("ERREUR")+": "+getS("l'importation des coefficients de frottement")+"\n" + getS("a �chou�."))
           .activate();
         return;
       }
       if ((lignes.length == 0)) {
         new BuDialogError(
           (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
           ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
             .getInformationsSoftware(),
           getS("ERREUR")+": "+getS("aucun point n'est")+"\n" + getS("disponible dans cette import!"))
           .activate();
         return;
       }
       modele_.estimationNumeroBief(lignes);
       modele_.setTabLignes(lignes);
      }
    }
    else if (cmd.equalsIgnoreCase("IMPORTER_CALAGE")) {
       ((Hydraulique1dTableauZoneFrottementModel)modele_).transfererZonesCalage();
    }
    super.actionPerformed(_evt);
  }


  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierCalageAuto) {
      MetierCalageAuto cal=(MetierCalageAuto)_n;
      ((Hydraulique1dTableauZoneFrottementModel)modele_).setCalageAuto(cal);
    }
    else super.setObject(_n);
  }

  /**
   * Override : Pour mettre a jour l'UI.
   */
  @Override
  public void setValeurs() {
	  if (CGlobal.AVEC_CALAGE_AUTO) {
		  btImporterCalage.setEnabled(
      ((Hydraulique1dTableauZoneFrottementModel)modele_).isImportationZonesPossible());
	  }
    super.setValeurs();
  }
@Override
public boolean getValeurs() {
	
	boolean value = super.getValeurs();
	if(uiParamsGeneraux != null) {
		value = value || uiParamsGeneraux.getValeursFrottement();
	}
	
	this.fermer();
	
	
	return value;
	 
}

Hydraulique1dParametresGenerauxEditor uiParamsGeneraux = null;

public void setParamPanelGeneraux(
		Hydraulique1dParametresGenerauxEditor hydraulique1dParametresGenerauxEditor) {
	uiParamsGeneraux = hydraulique1dParametresGenerauxEditor;
	
}
}
