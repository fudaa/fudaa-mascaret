/*
 * @file         Hydraulique1dZoneSecheEditor.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:42:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.event.ActionEvent;

import org.fudaa.dodico.hydraulique1d.metier.MetierConditionsInitiales;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresGeneraux;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauZoneSecheModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Editeur du tableau des zones s�ches des conditions initiales (MetierConditionsInitiales).<br>
 * Appeler si l'utilisateur clic sur le bouton "ZONES SECHES" de l'�diteur des conditions initiales.<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().ZONES_SECHES().editer().<br>
 * @see org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dTableauZoneEditor
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.6 $ $Date: 2007-11-20 11:42:43 $ by $Author: bmarchan $
 */
public class Hydraulique1dZoneSecheEditor extends Hydraulique1dTableauZoneEditor {
  private BuTextField tfHauteurEauMin_ = BuTextField.createDoubleField();
  private MetierConditionsInitiales conditionsInitiales_;
  private MetierParametresGeneraux parametresGeneraux_;
  private MetierEtude1d etude_;
  public Hydraulique1dZoneSecheEditor() {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zones s�ches"),new Hydraulique1dTableauZoneSecheModel());
    tfHauteurEauMin_.setColumns(10);
    BuPanel pnLabelTextField = new BuPanel();
    pnLabelTextField.add(new BuLabel(getS("Hauteur d'eau minimale")));
    pnLabelTextField.add(tfHauteurEauMin_);
    addAction("ESTIMATION", "ESTIMATION");
    panelCentral_.add(pnLabelTextField,BuBorderLayout.SOUTH);
    pack();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd = _evt.getActionCommand();
    if (cmd.equalsIgnoreCase("ESTIMATION")) {
      ((Hydraulique1dTableauZoneSecheModel)modele_).estimerZonesSeches();
    }
    super.actionPerformed(_evt);
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierEtude1d) {
      etude_ = (MetierEtude1d) _n;
      parametresGeneraux_ = etude_.paramGeneraux();
      conditionsInitiales_ = etude_.donneesHydro().conditionsInitiales();
      Hydraulique1dTableauZoneSecheModel modele = (Hydraulique1dTableauZoneSecheModel)modele_;
      modele.setConditionsInitiales(conditionsInitiales_);
      super.setObject(etude_.reseau());
    }

  }
  @Override
  public void setValeurs() {
    modele_.setValeurs();
    tfHauteurEauMin_.setValue(new Double(parametresGeneraux_.hauteurEauMinimal()));
  }

  @Override
  public boolean getValeurs() {
    double h = ((Double)tfHauteurEauMin_.getValue()).doubleValue();
    if (parametresGeneraux_.hauteurEauMinimal() != h) {
      parametresGeneraux_.hauteurEauMinimal(h);
      modele_.getValeurs();
      return true;
    }
    return modele_.getValeurs();
  }

}
