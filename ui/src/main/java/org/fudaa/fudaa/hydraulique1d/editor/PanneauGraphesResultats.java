package org.fudaa.fudaa.hydraulique1d.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSplitPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierUnite;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatialBief;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheResuBief;
import org.fudaa.fudaa.hydraulique1d.graphe.MascaretGridGraphe;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau d'affichage des r�sultats suivant le profil en long.
 *
 * @author Unknown
 * @version $Id$
 */
class PanneauGraphesResultats
        extends BuPanel implements ActionListener {

  private final static int MODE_BIEF = 0;
  private final static int MODE_CASIER = 1;
  private final static int MODE_LIAISON = 2;
  private final static String BIEF = Hydraulique1dResource.HYDRAULIQUE1D.getString("Bief");
  private final static String CASIER = Hydraulique1dResource.HYDRAULIQUE1D.getString("Casier");
  private final static String casier = Hydraulique1dResource.HYDRAULIQUE1D.getString("casier");
  private final static String LIAISON = Hydraulique1dResource.HYDRAULIQUE1D.getString("Liaison");
  private final static String liaison = Hydraulique1dResource.HYDRAULIQUE1D.getString("liaison");

  private int mode_ = MODE_BIEF;
  private BuPanel pnWest_ = new BuPanel();
  private BuVerticalLayout loWest_ = new BuVerticalLayout();
  private BuPanel pnCenter_ = new BuPanel();
  private BuBorderLayout loCenter_ = new BuBorderLayout();
  Hydraulique1dGrapheResuBief bgraphe_ = new Hydraulique1dGrapheResuBief();
  BuList lstVar_ = new BuList();
  BuList lstPasTpsSection_ = new BuList();
  private BuScrollPane scrlpListeVar_ = new BuScrollPane(lstVar_);
  private BuScrollPane scrlpListPasTpsSection_ = new BuScrollPane(lstPasTpsSection_);
  private TitledBorder ttlListeVar_ = new TitledBorder(Hydraulique1dResource.HYDRAULIQUE1D.getString("Liste Variable"));
  private TitledBorder ttlPasTempsSection_ = new TitledBorder("");
  private TitledBorder ttlListeBiefCasierLiaison__ = new TitledBorder(Hydraulique1dResource.HYDRAULIQUE1D.getString("Liste Biefs Casiers Liaisons"));
  BuCheckBox cbLaisse_ = new BuCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Laisse de crue"));
  BuList lstBiefCasierLiaison_ = new BuList();
  private BuScrollPane scrlpListeBiefCasierLiaison_ = new BuScrollPane(lstBiefCasierLiaison_);
  private BGrapheEditeurAxes bgEditeurAxes_;

  BuRadioButton rbSpatial_ = new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Profil Spatial"));
  BuRadioButton rbTemporel_ = new BuRadioButton();
  JRadioButton rbUniteS_;
  JRadioButton rbUniteM_;
  JRadioButton rbUniteH_;
  JRadioButton rbUniteJ_;
  JPanel pnUnite_;

  private MetierResultatsTemporelSpatial resultTempoSpatial_;
  private MetierResultatsTemporelSpatial resultatsCasier_;
  private MetierResultatsTemporelSpatial resultatsLiaison_;
  private MetierResultatsTemporelSpatial resultatsCourant_ = null;

  private Hydraulique1dGraphesResultatsEditor parent_;

  private JCheckBox checkboxGrid = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Afficher la Grille"));
  private JCheckBox checkboxMajAuto = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mise � jour automatique"));


  private enum UniteTps {

    s, mn, h, j
  }

  public PanneauGraphesResultats(Hydraulique1dGraphesResultatsEditor parent) {
    this.setLayout(new BuBorderLayout());

    parent_ = parent;
    resultTempoSpatial_ = null;

    bgEditeurAxes_ = bgraphe_.getEditeurAxes();
    bgraphe_.getEditeurAxes().getBtReset_().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Restaurer"));
    bgraphe_.getEditeurAxes().getBtZoom_().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zoom"));
    bgraphe_.getEditeurAxes().getLbAbs().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Abscisses"));
    bgraphe_.getEditeurAxes().getLbOrd().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Ordonn�es"));
    
    pnWest_.setLayout(loWest_);
    pnCenter_.setLayout(loCenter_);

    Dimension d = bgraphe_.getPersonnaliseurGraphe().getPreferredSize();
    Dimension dList = new Dimension(d.width, d.height / 2);

    scrlpListPasTpsSection_.setPreferredSize(dList);
    scrlpListPasTpsSection_.setBorder(ttlPasTempsSection_);

    scrlpListeVar_.setPreferredSize(dList);
    scrlpListeVar_.setBorder(ttlListeVar_);

    Dimension dListiefCasierLiaison = new Dimension(d.width, d.height / 3);
    scrlpListeBiefCasierLiaison_.setPreferredSize(dListiefCasierLiaison);
    scrlpListeBiefCasierLiaison_.setBorder(ttlListeBiefCasierLiaison__);

    lstBiefCasierLiaison_.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

    rbUniteS_ = new JRadioButton(Hydraulique1dResource.getS("sec."));
    rbUniteM_ = new JRadioButton(Hydraulique1dResource.getS("min."));
    rbUniteH_ = new JRadioButton(Hydraulique1dResource.getS("heure"));
    rbUniteJ_ = new JRadioButton(Hydraulique1dResource.getS("jour"));

    ButtonGroup bgUnite = new ButtonGroup();
    bgUnite.add(rbUniteS_);
    bgUnite.add(rbUniteM_);
    bgUnite.add(rbUniteH_);
    bgUnite.add(rbUniteJ_);
    rbUniteS_.setSelected(true);
    pnUnite_ = new JPanel();
    pnUnite_.setLayout(new FlowLayout(FlowLayout.CENTER, 3, 0));
    pnUnite_.add(rbUniteS_);
    pnUnite_.add(rbUniteM_);
    pnUnite_.add(rbUniteH_);
    pnUnite_.add(rbUniteJ_);

    pnWest_.add(scrlpListeBiefCasierLiaison_);
    pnWest_.add(rbSpatial_);
    pnWest_.add(rbTemporel_);
    pnWest_.add(pnUnite_);
    pnWest_.add(cbLaisse_);
    pnWest_.add(scrlpListeVar_);
    pnWest_.add(scrlpListPasTpsSection_);
    pnWest_.add(bgEditeurAxes_);
    pnWest_.add(checkboxGrid);
    pnWest_.add(checkboxMajAuto);
    
    pnCenter_.add(bgraphe_, BorderLayout.CENTER);

    JSplitPane spt = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnWest_, pnCenter_);
    spt.setOneTouchExpandable(true);

//		ScrollPane scrollPane = new ScrollPane();
//		scrollPane.add(spt);
//		BuScrollPane scrollPane = new BuScrollPane(spt);
//		scrollPane.setViewportView(spt);
    this.add(spt, BuBorderLayout.CENTER);

    lstBiefCasierLiaison_.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        listeBiefCasierLiaison();
      }
    });

    lstPasTpsSection_.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        listePasTpsSection();
      }
    });
    checkboxGrid.setSelected(true);
    checkboxGrid.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			boolean displayGrid = checkboxGrid.isSelected();
			if(bgraphe_.getGraphe() != null && bgraphe_.getGraphe() instanceof MascaretGridGraphe) {
				((MascaretGridGraphe)bgraphe_.getGraphe()).setDisplayGrid(displayGrid);
				bgraphe_.fullRepaint();
			}
			
		}
	});
    checkboxMajAuto.setSelected(true);
    checkboxMajAuto.addActionListener(this);

    rbSpatial_.setActionCommand("SPATIAL");
    rbSpatial_.addActionListener(this);
    rbTemporel_.setActionCommand("TEMPOREL");
    rbTemporel_.addActionListener(this);
    ButtonGroup bgProf = new ButtonGroup();
    bgProf.add(rbSpatial_);
    bgProf.add(rbTemporel_);

    rbSpatial_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() != ItemEvent.SELECTED) {
          return;
        }

        rbUniteS_.setEnabled(false);
        rbUniteM_.setEnabled(false);
        rbUniteH_.setEnabled(false);
        rbUniteJ_.setEnabled(false);
      }
    });
    rbTemporel_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() != ItemEvent.SELECTED) {
          return;
        }

        rbUniteS_.setEnabled(true);
        rbUniteM_.setEnabled(true);
        rbUniteH_.setEnabled(true);
        rbUniteJ_.setEnabled(true);
      }
    });
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd = _evt.getActionCommand();
    if ("SPATIAL".equals(cmd)) {
      spatial();
    } else if ("TEMPOREL".equals(cmd)) {
      temporel();
    }
  }

  public boolean isSelectedModeBief() {
    if (mode_ == MODE_BIEF) {
      return true;
    } else {
      return false;
    }
  }

  public void setResultatsCasier(MetierResultatsTemporelSpatial resultatsCasier) {
    if (resultatsCasier == resultatsCasier_) {
      return;
    }
    resultatsCasier_ = resultatsCasier;
  }

  public void setResultatsLiaison(MetierResultatsTemporelSpatial resultatsLiaison) {
    if (resultatsLiaison == resultatsLiaison_) {
      return;
    }
    resultatsLiaison_ = resultatsLiaison;
  }

  public void setResultats(MetierResultatsTemporelSpatial resultats) {
    if (resultats == resultTempoSpatial_) {
      return;
    }
    resultTempoSpatial_ = resultats;
    setValeurs();
  }

  /**
   * Modifie l'interface en fonction du fait qu'elle travaille sur des r�sultats temporels ou it�ratifs
   */
  public void changeUI() {
    //System.out.println("change UI");
    if (resultatsCourant_.isTemporel()) {
      ttlPasTempsSection_.setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Pas de Temps"));
      rbTemporel_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Profil Temporel"));
      rbTemporel_.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Temps en abscisse"));
    } else {
      ttlPasTempsSection_.setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("N� d'it�ration"));
      rbTemporel_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Profil It�ratif"));
      rbTemporel_.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("N� d'it�ration en abscisse"));
    }

    if (rbTemporel_.isSelected()) {
      ttlPasTempsSection_.setTitle(Hydraulique1dResource.HYDRAULIQUE1D.getString("Sections de calcul"));
    }
    scrlpListPasTpsSection_.repaint();
  }

  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface printable). Le format <code>_format</code> sera celui donne par la
   * methode <code>Pageable.getPageFormat(int)</code>.
   *
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe, sinon <code>Printable.NO_SUCH_PAGE</code>.
   */
  /*public int print(Graphics _g, PageFormat _format, int _page) {
   return bgraphe_.print(_g, _format, _page);
   }*/
  protected void setValeurs() {
    Vector<String> items = new Vector<String>();
    System.out.println("PanneauGraphesresultats setValeurs() LIAISON=" + LIAISON);
    System.out.println(
            "PanneauGraphesresultats setValeurs() Hydraulique1dResource.HYDRAULIQUE1D.getString(Liaison)=" + Hydraulique1dResource.HYDRAULIQUE1D.
            getString("Liaison"));
    if (resultTempoSpatial_ != null) {
      for (int i = 0; i < resultTempoSpatial_.resultatsBiefs().length; i++) {
        items.add(BIEF + " " + (i + 1));
      }
    }
    if (resultatsCasier_ != null) {
      for (int i = 0; i < resultatsCasier_.resultatsBiefs().length; i++) {
        items.add(CASIER + " " + (i + 1));
      }
    }
    if (resultatsLiaison_ != null) {
      for (int i = 0; i < resultatsLiaison_.resultatsBiefs().length; i++) {
        items.add(LIAISON + " " + (i + 1));
      }
    }
    lstBiefCasierLiaison_.setListData(items);
    
    if (resultTempoSpatial_ != null) {
      if (resultTempoSpatial_.resultatsBiefs().length > 0) {

        resultatsCourant_ = resultTempoSpatial_;
      }
      rbSpatial_.setSelected(true);
    } else if (resultatsCasier_ != null) {
      resultatsCourant_ = resultatsCasier_;
      mode_ = MODE_CASIER;
      rbTemporel_.setSelected(true);

    } else if (resultatsLiaison_ != null) {
      resultatsCourant_ = resultatsLiaison_;
      mode_ = MODE_LIAISON;
      rbTemporel_.setSelected(true);
    }
//		btProfil_.setVisible((reseau_ != null));
    setListeVariables();
    setListePasTps();
    enableLaisse();

    lstBiefCasierLiaison_.setSelectedIndex(0);
    changeUI();
  }

  private int[] getIndexElementsSelectionnes() {
    String item = (String) lstBiefCasierLiaison_.getSelectedValue();
    if (item == null) {
      return new int[0];
    }
    String debutChaine = "";
    if (item.startsWith(BIEF)) {
      debutChaine = BIEF;
    } else if (item.startsWith(CASIER)) {
      debutChaine = CASIER;
    } else if (item.startsWith(LIAISON)) {
      debutChaine = LIAISON;
    }
    Object[] values = lstBiefCasierLiaison_.getSelectedValues();

    ArrayList<Integer> listeIndexes = new ArrayList<Integer>(values.length);
    for (int i = 0; i < values.length; i++) {
      String valueChaine = values[i].toString();
      boolean commenceParDebutChaine = valueChaine.startsWith(debutChaine);
      if (commenceParDebutChaine) {
        String numFinChaine = valueChaine.substring(debutChaine.length()).trim();
        listeIndexes.add(new Integer(Integer.parseInt(numFinChaine) - 1));
      }
      //StringTokenizer st= new StringTokenizer(values[i].toString());
      //if (debutChaine.equals(st.nextToken())) {
      //	listeIndexes.add(new Integer(Integer.parseInt(st.nextToken()) - 1));
      //}
    }
    int[] res = new int[listeIndexes.size()];
    for (int i = 0; i < res.length; i++) {
      res[i] = listeIndexes.get(i).intValue();
    }
    return res;
  }

  protected void setListeSectionsCalcul() {
    if (rbTemporel_.isSelected()) {
      int[] indexElements = getIndexElementsSelectionnes();
      Vector<String> vSections = new Vector<String>();
      for (int i = 0; i < indexElements.length; i++) {
        MetierResultatsTemporelSpatialBief res
                = resultatsCourant_.resultatsBiefs()[indexElements[i]];
        if (res != null) {
          int nbSection = res.abscissesSections().length;
          for (int j = 0; j < nbSection; j++) {
            vSections.add(String.valueOf(res.abscissesSections()[j]));
          }
        }

      }
      lstPasTpsSection_.setListData(vSections);
    }
    if (mode_ == MODE_BIEF) {
      lstPasTpsSection_.setVisible(true);
    } else {
      lstPasTpsSection_.setSelectedIndex(0);
      lstPasTpsSection_.setVisible(false);
    }
  }

  protected void setListePasTps() {
    if (rbSpatial_.isSelected()) {
      double[] pasTps = resultatsCourant_.valPas();
      if (pasTps != null) {
        String[] pasTemps = new String[pasTps.length];
        for (int i = 0; i < pasTps.length; i++) {
          pasTemps[i] = "" + pasTps[i];
        }
        lstPasTpsSection_.setListData(pasTemps);
      }
    }
    if (mode_ == MODE_BIEF) {
      lstPasTpsSection_.setVisible(true);
    } else {
      lstPasTpsSection_.setSelectedIndex(0);
      lstPasTpsSection_.setVisible(false);
    }
  }

  protected void setListeVariables() {
    if (resultatsCourant_.descriptionVariables() != null) {
      String[] descriptions
              = new String[resultatsCourant_.descriptionVariables().length];
      for (int i = 0; i < descriptions.length; i++) {
    	  String desc = resultatsCourant_.descriptionVariables()[i].description();
    	  if(desc != null && desc.contains("(") && desc.contains(")")) {
    		  String traduction = Hydraulique1dResource.HYDRAULIQUE1D.getString(desc.substring(0,desc.indexOf("(")));
    		  desc = traduction + desc.substring(desc.indexOf("("));
    	  }
        descriptions[i] = Hydraulique1dResource.HYDRAULIQUE1D.getString(desc);
      }
      lstVar_.setListData(descriptions);
    }
  }

  public BuList getLstVar() {
    return lstVar_;
  }

  protected void listeBiefCasierLiaison() {

    int[] indices = lstBiefCasierLiaison_.getSelectedIndices();
    ArrayList<Integer> newIndices = new ArrayList<Integer>(indices.length);

    int modeInit = mode_;

    if (indices == null || indices.length == 0) {
      return;
    }

    ListModel model = lstBiefCasierLiaison_.getModel();
    String item1 = model.getElementAt(indices[0]).toString();

    if (item1.startsWith(BIEF)) {
      mode_ = MODE_BIEF;
      resultatsCourant_ = resultTempoSpatial_;
      for (int i = 0; i < indices.length; i++) {
        if (model.getElementAt(indices[i]).toString().startsWith(BIEF)) {
          newIndices.add(new Integer(indices[i]));
        }
      }

    } else if (item1.startsWith(CASIER)) {
      mode_ = MODE_CASIER;
      resultatsCourant_ = resultatsCasier_;
      for (int i = 0; i < indices.length; i++) {
        if (model.getElementAt(indices[i]).toString().startsWith(CASIER)) {
          newIndices.add(new Integer(indices[i]));
        }

      }
    } else if (item1.startsWith(LIAISON)) {
      mode_ = MODE_LIAISON;
      resultatsCourant_ = resultatsLiaison_;
      for (int i = 0; i < indices.length; i++) {
        if (model.getElementAt(indices[i]).toString().startsWith(LIAISON)) {
          newIndices.add(new Integer(indices[i]));
        }

      }
    }

    if (mode_ != MODE_BIEF) {

      rbSpatial_.setEnabled(false);
      rbTemporel_.setSelected(true);
      scrlpListPasTpsSection_.setVisible(false);
      //pnWest_.paintAll(pnWest_.getGraphics());
    } else {
      rbSpatial_.setEnabled(true);
      if (rbSpatial_.isSelected()) {
        spatial();
      } else {
        temporel();
      }
      scrlpListPasTpsSection_.setVisible(true);
      //pnWest_.paintAll(pnWest_.getGraphics());
    }

    if (modeInit != mode_) {
      pnWest_.paintAll(pnWest_.getGraphics());
    }

    if (newIndices.size() != indices.length) {
      lstBiefCasierLiaison_.clearSelection();
      //Transformation de la liste d'Integer en tableau de int
      int[] tabNewIndices = new int[newIndices.size()];
      for (int i = 0; i < tabNewIndices.length; i++) {
        tabNewIndices[i] = ((Integer) newIndices.get(i)).intValue();
      }
      lstBiefCasierLiaison_.setSelectedIndices(tabNewIndices);
    }

    setListeSectionsCalcul();
    setListePasTps();
    setListeVariables();
  }

  protected void listePasTpsSection() {
    if (lstPasTpsSection_.isVisible()) {
      int indexe = lstPasTpsSection_.getSelectedIndex();
      int indexeMax = lstPasTpsSection_.getModel().getSize() - 1;

      parent_.btAvancer_.setEnabled(indexe != indexeMax);
      parent_.btAvancerVite_.setEnabled(indexe != indexeMax);
      parent_.btReculer_.setEnabled(indexe != 0);
      parent_.btReculerVite_.setEnabled(indexe != 0);
    } else {
      parent_.btAvancer_.setEnabled(false);
      parent_.btAvancerVite_.setEnabled(false);
      parent_.btReculer_.setEnabled(false);
      parent_.btReculerVite_.setEnabled(false);
    }
  }

  protected void spatial() {
    lstBiefCasierLiaison_.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    rbSpatial_.setSelected(true);

//		ttlPasTempsSection_.setTitle("Pas de Temps");
    setListePasTps();
//		scrlpListPasTpsSection_.repaint();
    changeUI();
    enableLaisse();
  }

  protected void temporel() {
    int indexe = lstBiefCasierLiaison_.getSelectedIndex();
    lstBiefCasierLiaison_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    if (indexe != -1) {
      lstBiefCasierLiaison_.setSelectedIndex(indexe);
    }
    rbTemporel_.setSelected(true);

//		ttlPasTempsSection_.setTitle("Sections de calcul");
    setListeSectionsCalcul();
//		scrlpListPasTpsSection_.repaint();
    changeUI();
    enableLaisse();
  }

  protected void enableLaisse() {
    if(parent_.donneeCalageAuto != null ) {
    	cbLaisse_.setEnabled(true);
    }else
	 if ( (parent_.donnneHyd_ == null) || (parent_.donnneHyd_.laisses() == null) || (parent_.donnneHyd_.laisses().length == 0)) {
      cbLaisse_.setEnabled(false);
      cbLaisse_.setSelected(false);
    } else {
      cbLaisse_.setEnabled(true);
    }
  }

  protected boolean existeVariableUniteMetre(int[] indiceVar) {
    for (int i = 0; i < indiceVar.length; i++) {
      EnumMetierUnite unite
              = resultatsCourant_.descriptionVariables()[indiceVar[i]].unite();
      if (unite.value() == EnumMetierUnite._M) {
        return true;
      }
    }
    return false;
  }

  protected void visualiser(boolean calculBorne) {
	  visualiser(calculBorne, false);
  }
  
  protected void visualiser(boolean calculBorne,boolean silentMode) {
    int[] index = getIndexElementsSelectionnes();
    //System.err.println("Mode "+mode_);

    MetierResultatsTemporelSpatialBief[] res = new MetierResultatsTemporelSpatialBief[index.length];
    
    if(resultatsCourant_ !=null && resultatsCourant_.resultatsBiefs() != null)
	    for (int i = 0; i < index.length; i++) {
	      res[i] = resultatsCourant_.resultatsBiefs()[index[i]];
	    }
    int[] indicesVar = lstVar_.getSelectedIndices();
    int[] indicesPasTpsSection = lstPasTpsSection_.getSelectedIndices();

    /*System.err.print("index :");
     for (int i = 0; i < index.length; i++) {
     System.err.print(index[i]+" ");
     }

     System.err.println("\nres :");
     for (int i = 0; i < res.length; i++) {
     System.err.print(res[i].toString()+" ");
     }

     System.err.print("\nindicesVar :");
     for (int i = 0; i < indicesVar.length; i++) {
     System.err.print(indicesVar[i]+" ");
     }	System.err.print(" finindicesVar");

     System.err.println("\nindicesPasTpsSection :");
     for (int i = 0; i < indicesPasTpsSection.length; i++) {
     System.err.print(indicesPasTpsSection[i]+" ");
     }*/
//		Avertissements
    String message = "";
    if (index.length == 0) {
      message += Hydraulique1dResource.HYDRAULIQUE1D.getString("Veuillez selectionner au moins un bief, un casier ou une liaison !") + "\n";
    }
    if (indicesVar.length == 0) {
      message += Hydraulique1dResource.HYDRAULIQUE1D.getString("Veuillez selectionner au moins une variable !") + "\n";
    }
    if (mode_ == MODE_BIEF && indicesPasTpsSection.length == 0) {
      if (rbSpatial_.isSelected()) {
        message += Hydraulique1dResource.HYDRAULIQUE1D.getString("Veuillez selectionner au moins un pas de temps !") + "\n";
      } else {
        message += Hydraulique1dResource.HYDRAULIQUE1D.getString("Veuillez selectionner au moins une section de calcul !") + "\n";
      }
    }
    if (!message.isEmpty()) {
    	if(!silentMode)
    		JOptionPane.showMessageDialog(this, message);
      return;
    }

    String titre = "";
    String titreX = "";
    String absUnite = "";

    UniteTps unit;
    String sunit;
    if (rbUniteS_.isSelected()) {
      unit = UniteTps.s;
      sunit = Hydraulique1dResource.getS("s");
    } else if (rbUniteM_.isSelected()) {
      unit = UniteTps.mn;
      sunit = Hydraulique1dResource.getS("mn");
    } else if (rbUniteH_.isSelected()) {
      unit = UniteTps.h;
      sunit = Hydraulique1dResource.getS("h");
    } else {
      unit = UniteTps.j;
      sunit = Hydraulique1dResource.getS("j");
    }

    double[][] tab = null;

    if (mode_ == MODE_BIEF) {
      if (rbSpatial_.isSelected()) {
        tab = getTableauResultatsSpatial(res, indicesVar, indicesPasTpsSection);
        titre = Hydraulique1dResource.HYDRAULIQUE1D.getString("PROFIL SPATIAL");
        titreX = "X";
        absUnite = "m";
      } else if (resultatsCourant_.isTemporel()) {
        tab = getTableauResultatsTemporels(res, indicesVar,
                indicesPasTpsSection,
                resultatsCourant_.valPas(), unit);
        titre = Hydraulique1dResource.HYDRAULIQUE1D.getString("PROFIL TEMPOREL");
        titreX = Hydraulique1dResource.HYDRAULIQUE1D.getString("tps");
        absUnite = sunit;
      } else {//Cas du calage ???
        tab = getTableauResultatsTemporels(res, indicesVar,
                indicesPasTpsSection,
                resultatsCourant_.valPas(), unit);
        titre = Hydraulique1dResource.HYDRAULIQUE1D.getString("PROFIL ITERATIF");
        titreX = Hydraulique1dResource.HYDRAULIQUE1D.getString("it�ration");
        absUnite = sunit;
      }
    } else {
      tab = getTableauResultatsCasiersLiaisons(res, indicesVar,
              resultatsCourant_.valPas(), unit);

      titre = Hydraulique1dResource.HYDRAULIQUE1D.getString("PROFIL TEMPOREL");
      titreX = Hydraulique1dResource.HYDRAULIQUE1D.getString("tps");
      absUnite = sunit;
    }

    bgraphe_.setLabels(titre, titreX, "", absUnite, "");
    Object[] selectedValues = lstVar_.getSelectedValues();
    if (tab != null && (selectedValues != null) && (selectedValues.length > 0)) {
      bgraphe_.affiche(tab, getTitresCourbes(), parent_.getListLaisse(indicesVar), calculBorne);
    }
  }

  public Dimension getDefaultImageDimension() {
    return bgraphe_.getDefaultImageDimension();
  }

  protected String[] getTitresCourbes() {
    Object[] nomsVar = lstVar_.getSelectedValues();
    Object[] nomsPasTpsSection = lstPasTpsSection_.getSelectedValues();
    int[] index = getIndexElementsSelectionnes();
    String[] res;
    if (mode_ == MODE_BIEF) {
      res = new String[nomsVar.length * nomsPasTpsSection.length];
      int indiceRes = 0;
      for (int i = 0; i < nomsVar.length; i++) {
        for (int j = 0; j < nomsPasTpsSection.length; j++) {
          res[indiceRes++]
                  = nomsVar[i].toString() + " " + nomsPasTpsSection[j].toString();
        }
      }
    } else {
      res = new String[nomsVar.length * index.length];
      int indiceRes = 0;
      String type = "";
      for (int i = 0; i < nomsVar.length; i++) {
        for (int j = 0; j < index.length; j++) {
          if ((mode_ == MODE_CASIER) && (!nomsVar[i].toString().endsWith(casier))) {
            type = " " + casier;
          }
          if ((mode_ == MODE_LIAISON) && (!nomsVar[i].toString().endsWith(liaison))) {
            type = " " + liaison;
          }
          res[indiceRes++]
                  = nomsVar[i].toString() + type + " " + (index[j] + 1);
        }
      }
    }
    return res;
  }

  /**
   * @param resBiefs DResultatsTemporelSpatialBief[]
   * @param indicesVar int[]
   * @param indicesPasTps int[]
   * @return double[][]
   */
  private final double[][] getTableauResultatsSpatial(
          MetierResultatsTemporelSpatialBief[] resBiefs,
          int[] indicesVar,
          int[] indicesPasTps) {
	 
	 //TO BE REMOVED	
	if(indicesVar == null || resBiefs == null || indicesPasTps == null) {
		System.err.println("Error un element est null dans getTableauResultatsSpatial() values:"+ resBiefs+" indiceVar"+ indicesVar+" indicesPdt"+indicesPasTps);
		return null;
	}
	
	int nbCourbe = indicesVar.length * indicesPasTps.length;
    int nbBief = resBiefs.length;
    int nbSectionTotal = 0;
    ArrayList<double[][]> listeRes = new ArrayList<double[][]>(nbBief);
    for (int i = 0; i < nbBief; i++) {
      MetierResultatsTemporelSpatialBief resBief = resBiefs[i];
      double[][] resB = new double[nbCourbe + 1][];
//			l'abscisse : les sections
      resB[0] = resBief.abscissesSections();
      nbSectionTotal += resB[0].length;
//			les ordonn�es
      double[][][] vals = resBief.valeursVariables();
      int indiceCourbe = 1;
      for (int j = 0; j < indicesVar.length; j++) {
        for (int k = 0; k < indicesPasTps.length; k++) {
          resB[indiceCourbe] = vals[indicesVar[j]][indicesPasTps[k]];
          indiceCourbe++;
        }
      }
      listeRes.add(resB);
    }
    double[][] res = new double[nbCourbe + 1][nbSectionTotal];
    for (int i = 0; i < res.length; i++) {
      int indiceDest = 0;
      for (int j = 0; j < nbBief; j++) {
        double[][] resB = listeRes.get(j);
        double[] resBCbi = resB[i];
        int nbSectionsBief = resBCbi.length;
        System.arraycopy(resBCbi, 0, res[i], indiceDest, nbSectionsBief);
        indiceDest += nbSectionsBief;
      }
    }
    return res;
  }

  /**
   * @param resBiefs DResultatsTemporelSpatialBief[]
   * @param indicesVar int[]
   * @param indicesSection int[]
   * @param _unit L'unit� de temps.
   * @return double[][]
   */
  private final double[][] getTableauResultatsTemporels(
          MetierResultatsTemporelSpatialBief[] resBiefs,
          int[] indicesVar,
          int[] indicesSection,
          double[] pasTemps, UniteTps _unit) {

    int nbPasTemps = pasTemps.length;

    int coef;
    switch (_unit) {
      default:
      case s:
        coef = 1;
        break;
      case mn:
        coef = 60;
        break;
      case h:
        coef = 60 * 60;
        break;
      case j:
        coef = 60 * 60 * 24;
        break;
    }

    int nbCourbe = indicesVar.length * indicesSection.length;
    double[][] res = new double[nbCourbe + 1][nbPasTemps];
//		l'abscisse : les pas de temps
    res[0] = Arrays.copyOf(pasTemps, nbPasTemps);
    for (int i = 0; i < nbPasTemps; i++) {
      res[0][i] /= coef;
    }

//		les ordonn�es
    double[][][] vals = resBiefs[0].valeursVariables();
    for (int i = 0; i < nbPasTemps; i++) {
      int indiceCourbe = 1;
      for (int j = 0; j < indicesVar.length; j++) {
        for (int k = 0; k < indicesSection.length; k++) {
          res[indiceCourbe][i] = vals[indicesVar[j]][i][indicesSection[k]];
          indiceCourbe++;
        }
      }
    }
    return res;
  }

  /**
   * @param indicesVar.lengthresCasiersLiasons DResultatsTemporelSpatialBief[]
   * @param indicesVar int[]
   * @param indicesSection int[]
   * @param _unit L'unite de temps
   * @return double[][]
   */
  private final double[][] getTableauResultatsCasiersLiaisons(
          MetierResultatsTemporelSpatialBief[] resCasiersLiasons,
          int[] indicesVar,
          double[] pasTemps, UniteTps _unit) {

    int nbPasTemps = pasTemps.length;

    int coef;
    switch (_unit) {
      default:
      case s:
        coef = 1;
        break;
      case mn:
        coef = 60;
        break;
      case h:
        coef = 60 * 60;
        break;
      case j:
        coef = 60 * 60 * 24;
        break;
    }

//		!!!!!!!!!!!!!!!!!!!!!!!!
//		pour afficher plusieurs casiers il faudrait plusieurs courbe or normaleemnt en mode temporel le nombre de courbe devrait le nombre de variable
//		idem la courbe a pour titre casier 0.0 car elle affiche le numero de section qui est pas defaut 0
//		!!!!!!!!!!!!!!!!!!!!!!!!!!
    int nbCourbe = indicesVar.length * resCasiersLiasons.length;
    //System.err.println("NB VAR "+indicesVar.length);
    //System.err.println("NBCASIER "+resCasiersLiasons.length);
    //System.err.println("NB COURBE "+nbCourbe);

    double[][] res = new double[nbCourbe + 1][nbPasTemps];
//		l'abscisse : les pas de temps
    res[0] = Arrays.copyOf(pasTemps, nbPasTemps);
    for (int i = 0; i < nbPasTemps; i++) {
      res[0][i] /= coef;
    }

//		les ordonn�es
    double[][][] vals = resCasiersLiasons[0].valeursVariables();

    for (int i = 0; i < nbPasTemps; i++) {
      int indiceCourbe = 1;
      for (int n = 0; n < resCasiersLiasons.length; n++) {
        for (int j = 0; j < indicesVar.length; j++) {
          vals = resCasiersLiasons[n].valeursVariables();
          
          if(indicesVar[j]<vals.length)
        	  res[indiceCourbe][i] = vals[indicesVar[j]][i][0];
          
          indiceCourbe++;
        }
      }
    }


    /*System.err.println("\nres :");
     for (int u = 0; u < res.length; u++) {
     for (int v = 0; v < res[u].length; v++) {
     System.err.print(res[u][v]+" ");
     }
     System.err.println("");

     }*/
    return res;
  }

public BuList getLstVar_() {
	return lstVar_;
}

public void setLstVar_(BuList lstVar_) {
	this.lstVar_ = lstVar_;
}

public BuList getLstPasTpsSection_() {
	return lstPasTpsSection_;
}

public void setLstPasTpsSection_(BuList lstPasTpsSection_) {
	this.lstPasTpsSection_ = lstPasTpsSection_;
}

public BuList getLstBiefCasierLiaison_() {
	return lstBiefCasierLiaison_;
}

public void setLstBiefCasierLiaison_(BuList lstBiefCasierLiaison_) {
	this.lstBiefCasierLiaison_ = lstBiefCasierLiaison_;
}

public JCheckBox getCheckboxMajAuto() {
	return checkboxMajAuto;
}

public void setCheckboxMajAuto(JCheckBox checkboxMajAuto) {
	this.checkboxMajAuto = checkboxMajAuto;
}
}
