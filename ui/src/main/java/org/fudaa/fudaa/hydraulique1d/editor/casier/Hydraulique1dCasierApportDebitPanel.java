/*
 * @file         Hydraulique1dCasierApportDebitPanel.java
 * @creation     2003-06-18
 * @modification $Date: 2007-11-20 11:43:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.border.Border;


import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiHydrogramme;
import org.fudaa.ebli.commun.LineChoiceBorder;

import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dListeLoiCombo;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Panneau de s�lection d'un d�bit d'apport de l'�diteur de casier.
 * @version      $Revision: 1.9 $ $Date: 2007-11-20 11:43:29 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierApportDebitPanel
  extends BuPanel
  implements ActionListener {
  private BuRadioButton rbOui_, rbNon_;
  private BuButton btDefinirLoi_;
  private Hydraulique1dListeLoiCombo cmbNomLoi_;
  private MetierCasier model_;
  private MetierDonneesHydrauliques donneesHydro_;
  public Hydraulique1dCasierApportDebitPanel() {
    this(null, null);
  }
  public Hydraulique1dCasierApportDebitPanel(
    MetierDonneesHydrauliques donneesHydro,
    MetierCasier model) {
    super();
    setLayout(new BuBorderLayout(2, 2));
    Border outSideBorder=
      BorderFactory.createTitledBorder(
        BorderFactory.createEtchedBorder(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("Apport de d�bit"));
    Border inSideBorder= BorderFactory.createEmptyBorder(10, 10, 10, 10);
    setBorder(BorderFactory.createCompoundBorder(outSideBorder, inSideBorder));
    BuPanel pnOuest= constructPanelOuest();
    BuPanel pnCentre= constructPanelCentre();
    add(pnOuest, BuBorderLayout.WEST);
    add(pnCentre, BuBorderLayout.CENTER);
    donneesHydro_= donneesHydro;
    setModel(model);
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equals("OUI")) {
      setEnabledCmbBt(true);
    } else if (cmd.equals("NON")) {
      setEnabledCmbBt(false);
    }
  }
  public void setModel(MetierCasier model) {
    model_= model;
    setValeurs();
  }
  public void setDonneesHydrauliques(MetierDonneesHydrauliques donneesHydro) {
    donneesHydro_= donneesHydro;
    cmbNomLoi_.setDonneesHydro(donneesHydro_);
    cmbNomLoi_.initListeLoi();
  }
  public void addActionDefinirLoi(ActionListener listener) {
    btDefinirLoi_.addActionListener(listener);
  }
  boolean getValeurs() {
    boolean changed= false;
    try {
      if (rbNon_.isSelected()) {
        if (model_.loiRattachee() != null) {
          model_.loiRattachee(null);
          changed= true;
        }
      } else {
        MetierLoiHydrogramme loi= (MetierLoiHydrogramme)cmbNomLoi_.getValeurs();
        if (loi != null) {
          if (loi != model_.loiRattachee()) {
            model_.loiRattachee(loi);
            changed= true;
          }
        } else {
          showBuError(Hydraulique1dResource.HYDRAULIQUE1D.getString("Aucune loi s�lectionn�e"), true);
          return false;
        }
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }
    return changed;
  }
  void setValeurs() {
    cmbNomLoi_.initListeLoi();
    if (model_ == null)
      return;
    if (model_.loiRattachee() == null) {
      rbNon_.setSelected(true);
      setEnabledCmbBt(false);
    } else {
      rbOui_.setSelected(true);
      setEnabledCmbBt(true);
      cmbNomLoi_.setValeurs(model_.loiRattachee());
    }
  }
  public void showBuError(String _message, boolean _modale) {
    if (_message == null)
      return;
    BuCommonInterface appli = (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
    BuDialogError message=new BuDialogError(appli, appli.getInformationsSoftware(),
                                            _message);
    message.setModal(_modale);
    Point p= getLocation();
    if (p != null)
      message.setLocation(p);
    message.activate();
  }
  void initListeLoi() {
    cmbNomLoi_.initListeLoi();
  }
  private BuPanel constructPanelOuest() {
    BuPanel panel= new BuPanel();
    panel.setLayout(new BuBorderLayout());
    panel.add(
      new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Apport de d�bit ext�rieur")+" ?"),
      BuBorderLayout.CENTER);
    return panel;
  }
  private BuPanel constructPanelCentre() {
    BuPanel panel= new BuPanel();
    panel.setLayout(new GridBagLayout());
    panel.setBorder(
      new LineChoiceBorder(false, false, false, true, false, false));
    rbOui_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Oui"));
    rbOui_.addActionListener(this);
    rbOui_.setActionCommand("OUI");
    rbNon_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Non"));
    rbNon_.addActionListener(this);
    rbNon_.setActionCommand("NON");
    ButtonGroup groupeRadioBouton= new ButtonGroup();
    groupeRadioBouton.add(rbOui_);
    groupeRadioBouton.add(rbNon_);
    rbNon_.setSelected(true);
    Insets inset3_5= new Insets(3, 5, 0, 0);
    Insets inset3_0= new Insets(3, 0, 0, 0);
    // radio bouton Oui
    panel.add(
      rbOui_,
      new GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // ligne horizontale
    BuPanel pnLigneHorizontal= new BuPanel();
    pnLigneHorizontal.setPreferredSize(new Dimension(30, 5));
    pnLigneHorizontal.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    panel.add(
      pnLigneHorizontal,
      new GridBagConstraints(
        1,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_0,
        10,
        0));
    // label  "Nom de la loi de type "hydrogramme Q(t)"
    String nomLoiType= Hydraulique1dResource.HYDRAULIQUE1D.getString("Nom de la loi de type");
    String hydrogrammeQt= Hydraulique1dResource.HYDRAULIQUE1D.getString("hydrogramme Q(t)");
    BuLabel lb= new BuLabel(nomLoiType+" '"+hydrogrammeQt+"'");
    panel.add(
      lb,
      new GridBagConstraints(
        2,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // ComboBox un peu �laborer
    cmbNomLoi_= new Hydraulique1dListeLoiCombo(Hydraulique1dListeLoiCombo.HYDROGRAMME);
    panel.add(
      cmbNomLoi_,
      new GridBagConstraints(
        3,
        0,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    // radio bouton Non
    panel.add(
      rbNon_,
      new GridBagConstraints(
        0,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // bouton Definir loi
    btDefinirLoi_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("DEFINIR LOI"));
    btDefinirLoi_.setActionCommand("DEFINIR_LOI");
    panel.add(
      btDefinirLoi_,
      new GridBagConstraints(
        3,
        1,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    setEnabledCmbBt(false);
    return panel;
  }
  private void setEnabledCmbBt(boolean enabled) {
    cmbNomLoi_.setEnabled(enabled);
    btDefinirLoi_.setEnabled(enabled);
  }
  public static void main(String[] arg) {
    Hydraulique1dCasierApportDebitPanel panel=
      new Hydraulique1dCasierApportDebitPanel();
    JFrame fenetre= new JFrame("test Hydraulique1dCasierApportDebitPanel");
    fenetre.getContentPane().setLayout(new BuBorderLayout());
    panel.addActionDefinirLoi(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        System.out.println("DEFINIR LOI");
      }
    });
    fenetre.getContentPane().add(panel, BuBorderLayout.CENTER);
    fenetre.pack();
    fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fenetre.setVisible(true);
  }
}
