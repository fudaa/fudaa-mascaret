/*
 * @file         Hydraulique1dCasierCaracLiaisonPanel.java
 * @creation     2003-06-10
 * @modification $Date: 2007-11-20 11:43:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.border.Border;


import org.fudaa.dodico.hydraulique1d.metier.EnumMetierSensDebitLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.ebli.commun.LineChoiceBorder;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Panneau de s�lection des diff�rents types de liaison.
 * @version      $Revision: 1.11 $ $Date: 2007-11-20 11:43:27 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierCaracLiaisonPanel
  extends BuPanel
  implements ActionListener {
  private final static String[] TYPES_SENS_DEBIT_CASIER_CASIER=
  { Hydraulique1dResource.HYDRAULIQUE1D.getString("Les 2 sens"), Hydraulique1dResource.HYDRAULIQUE1D.getString("Casier amont vers aval"),
    Hydraulique1dResource.HYDRAULIQUE1D.getString("Casier aval vers amont") };
  private final static String[] TYPES_SENS_DEBIT_BIEF_CASIER=
    { Hydraulique1dResource.HYDRAULIQUE1D.getString("Les 2 sens"), Hydraulique1dResource.HYDRAULIQUE1D.getString("Casier vers bief"),
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Bief vers casier") };
  private BuTextField tfSeuilCote_, tfSeuilLargeur_, tfSeuilCoefQ_,
      tfSeuilCoefActivation_;
  private BuTextField tfChenalCote_, tfChenalLargeur_, tfChenalLongueur_,
      tfChenalRugosite_;
  private BuTextField tfSiphonCote_, tfSiphonLongueur_,
    tfSiphonSection_,
    tfSiphonCoefPerteCharge_;
  private BuTextField tfOrificeCote_, tfOrificeSection_, tfOrificeCoefQSeuil_;
  private BuTextField tfOrificeLargeur_, tfOrificeCoefQOrifice_;
  private BuComboBox cmbSensDebit_;
  private BuPanel pnTFSeuil_, pnTFChenal_, pnTFSiphon_, pnTFOrifice_;
  private BuRadioButton rbSeuil_, rbChenal_, rbSiphon_, rbOrifice_;
  private MetierLiaison model_;
  public Hydraulique1dCasierCaracLiaisonPanel() {
    this(null);
  }
  public Hydraulique1dCasierCaracLiaisonPanel(MetierLiaison model) {
    super();
    setLayout(new BuGridLayout(2, 0, 10));
    Border outSideBorder=
      BorderFactory.createTitledBorder(
        BorderFactory.createEtchedBorder(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("Type"));
    Border inSideBorder= BorderFactory.createEmptyBorder(10, 10, 10, 10);
    setBorder(BorderFactory.createCompoundBorder(outSideBorder, inSideBorder));
    construc4Panels();
    rbSeuil_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Seuil"));
    rbSeuil_.addActionListener(this);
    rbSeuil_.setActionCommand("SEUIL");
    rbChenal_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Chenal"));
    rbChenal_.addActionListener(this);
    rbChenal_.setActionCommand("CHENAL");
    rbSiphon_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Siphon"));
    rbSiphon_.addActionListener(this);
    rbSiphon_.setActionCommand("SIPHON");
    rbOrifice_= new BuRadioButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Orifice"));
    rbOrifice_.addActionListener(this);
    rbOrifice_.setActionCommand("ORIFICE");
    ButtonGroup groupeRadioBouton= new ButtonGroup();
    groupeRadioBouton.add(rbSeuil_);
    groupeRadioBouton.add(rbChenal_);
    groupeRadioBouton.add(rbSiphon_);
    groupeRadioBouton.add(rbOrifice_);
    BuPanel pnRbSeuilLigne= constructPanelRbLigne(rbSeuil_);
    BuPanel pnRbChenalLigne= constructPanelRbLigne(rbChenal_);
    BuPanel pnRbSiphonLigne= constructPanelRbLigne(rbSiphon_);
    BuPanel pnRbOrificeLigne= constructPanelRbLigne(rbOrifice_);
    add(pnRbSeuilLigne);
    add(pnTFSeuil_);
    add(pnRbChenalLigne);
    add(pnTFChenal_);
    add(pnRbSiphonLigne);
    add(pnTFSiphon_);
    add(pnRbOrificeLigne);
    add(pnTFOrifice_);
    setModel(model);
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equals("SEUIL")) {
      setEnabledSeuil(true);
      setEnabledChenal(false);
      setEnabledSiphon(false);
      setEnabledOrifice(false);
    } else if (cmd.equals("CHENAL")) {
      setEnabledSeuil(false);
      setEnabledChenal(true);
      setEnabledSiphon(false);
      setEnabledOrifice(false);
    } else if (cmd.equals("SIPHON")) {
      setEnabledSeuil(false);
      setEnabledChenal(false);
      setEnabledSiphon(true);
      setEnabledOrifice(false);
    } else if (cmd.equals("ORIFICE")) {
      setEnabledSeuil(false);
      setEnabledChenal(false);
      setEnabledSiphon(false);
      setEnabledOrifice(true);
    }
  }
  boolean getValeurs() {
    boolean changed= false;
    try {
      if (rbSeuil_.isSelected()) {
        if (!model_.isSeuil()) {
          model_.toSeuil();
          changed= true;
        }
        double cote=
          ((Double)tfSeuilCote_.getValue()).doubleValue();
        if (cote != model_.getCote()) {
          model_.setCote(cote);
          changed= true;
        }
        double coefActivation=
          ((Double)tfSeuilCoefActivation_.getValue()).doubleValue();
        if (coefActivation != model_.getCoefActivation()) {
          model_.setCoefActivation(coefActivation);
          changed= true;
        }
        double coefQ= ((Double)tfSeuilCoefQ_.getValue()).doubleValue();
        if (coefQ != model_.getCoefQ()) {
          model_.setCoefQ(coefQ);
          changed= true;
        }
        double largeur= ((Double)tfSeuilLargeur_.getValue()).doubleValue();
        if (largeur != model_.getLargeur()) {
          model_.setLargeur(largeur);
          changed= true;
        }
      } else if (rbChenal_.isSelected()) {
        if (!model_.isChenal()) {
          model_.toChenal();
          changed= true;
        }
        double cote=((Double)tfChenalCote_.getValue()).doubleValue();
        if (cote != model_.getCote()) {
          model_.setCote(cote);
          changed= true;
        }
        double largeur= ((Double)tfChenalLargeur_.getValue()).doubleValue();
        if (largeur != model_.getLargeur()) {
          model_.setLargeur(largeur);
          changed= true;
        }
        double longueur= ((Double)tfChenalLongueur_.getValue()).doubleValue();
        if (longueur != model_.getLongueur()) {
          model_.setLongueur(longueur);
          changed= true;
        }
        double rugosite= ((Double)tfChenalRugosite_.getValue()).doubleValue();
        if (rugosite != model_.getRugosite()) {
          model_.setRugosite(rugosite);
          changed= true;
        }
      } else if (rbSiphon_.isSelected()) {
        if (!model_.isSiphon()) {
          model_.toSiphon();
          changed= true;
        }
        double cote=((Double)tfSiphonCote_.getValue()).doubleValue();
        if (cote != model_.getCote()) {
           model_.setCote(cote);
           changed= true;
         }
        double perteCharge=
          ((Double)tfSiphonCoefPerteCharge_.getValue()).doubleValue();
        if (perteCharge != model_.getCoefPerteCharge()) {
          model_.setCoefPerteCharge(perteCharge);
          changed= true;
        }
        double longueur= ((Double)tfSiphonLongueur_.getValue()).doubleValue();
        if (longueur != model_.getLongueur()) {
          model_.setLongueur(longueur);
          changed= true;
        }
        double section= ((Double)tfSiphonSection_.getValue()).doubleValue();
        if (section != model_.getSection()) {
          model_.setSection(section);
          changed= true;
        }
      } else if (rbOrifice_.isSelected()) {
        if (!model_.isOrifice()) {
          model_.toOrifice();
          changed= true;
        }
        double cote=((Double)tfOrificeCote_.getValue()).doubleValue();
        if (cote != model_.getCote()) {
           model_.setCote(cote);
           changed= true;
        }
        double largeur= ((Double)tfOrificeLargeur_.getValue()).doubleValue();
        if (largeur != model_.getLargeur()) {
          model_.setLargeur(largeur);
          changed= true;
        }
        double section= ((Double)tfOrificeSection_.getValue()).doubleValue();
        if (section != model_.getSection()) {
          model_.setSection(section);
          changed= true;
        }
        double coefQ= ((Double)tfOrificeCoefQSeuil_.getValue()).doubleValue();
        if (coefQ != model_.getCoefQ()) {
          model_.setCoefQ(coefQ);
          changed= true;
        }
        double coefQOrifice= ((Double)tfOrificeCoefQOrifice_.getValue()).doubleValue();
        if (coefQOrifice != model_.getCoefQOrifice()) {
          model_.setCoefQOrifice(coefQOrifice);
          changed= true;
        }
        int sensDebit= cmbSensDebit_.getSelectedIndex();
        if (sensDebit != model_.getSensDebit().value()) {
          EnumMetierSensDebitLiaison newValeur = EnumMetierSensDebitLiaison.from_int(sensDebit);
          model_.setSensDebit(newValeur);
          changed= true;
        }
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }
    return changed;
  }
  public void setModel(MetierLiaison model) {
    model_= model;
    setValeurs();
  }
  void setValeurs() {


    initSeuil();
    initChenal();
    initSiphon();
    initOrifice();
    if (model_ == null)
      return;
    String[] items;
    if (model_.isCasierCasier()){
      items = TYPES_SENS_DEBIT_CASIER_CASIER;
    }
    else {
      items = TYPES_SENS_DEBIT_BIEF_CASIER;
    }
    cmbSensDebit_.removeAllItems();
    for (int i = 0; i < items.length; i++) {
      cmbSensDebit_.addItem(items[i]);
    }
    rbChenal_.setSelected(model_.isChenal());
    rbSeuil_.setSelected(model_.isSeuil());
    rbSiphon_.setSelected(model_.isSiphon());
    rbOrifice_.setSelected(model_.isOrifice());
    if (model_.isChenal()) {
      setEnabledChenal(true);
      setEnabledSeuil(false);
      setEnabledSiphon(false);
      setEnabledOrifice(false);
      tfChenalCote_.setValue(new Double(model_.getCote()));
      tfChenalLargeur_.setValue(new Double(model_.getLargeur()));
      tfChenalLongueur_.setValue(new Double(model_.getLongueur()));
      tfChenalRugosite_.setValue(new Double(model_.getRugosite()));
    } else if (model_.isSeuil()) {
      setEnabledChenal(false);
      setEnabledSeuil(true);
      setEnabledSiphon(false);
      setEnabledOrifice(false);
      tfSeuilCote_.setValue(new Double(model_.getCote()));
      tfSeuilCoefActivation_.setValue(new Double(model_.getCoefActivation()));
      tfSeuilCoefQ_.setValue(new Double(model_.getCoefQ()));
      tfSeuilLargeur_.setValue(new Double(model_.getLargeur()));
    } else if (model_.isSiphon()) {
      setEnabledChenal(false);
      setEnabledSeuil(false);
      setEnabledSiphon(true);
      setEnabledOrifice(false);
      tfSiphonCote_.setValue(new Double(model_.getCote()));
      tfSiphonCoefPerteCharge_.setValue(
        new Double(model_.getCoefPerteCharge()));
      tfSiphonLongueur_.setValue(new Double(model_.getLongueur()));
      tfSiphonSection_.setValue(new Double(model_.getSection()));
    } else if (model_.isOrifice()) {
      setEnabledChenal(false);
      setEnabledSeuil(false);
      setEnabledSiphon(false);
      setEnabledOrifice(true);
      tfOrificeCote_.setValue(new Double(model_.getCote()));
      tfOrificeLargeur_.setValue(new Double(model_.getLargeur()));
      tfOrificeSection_.setValue(new Double(model_.getSection()));
      tfOrificeCoefQSeuil_.setValue(new Double(model_.getCoefQ()));
      tfOrificeCoefQOrifice_.setValue(new Double(model_.getCoefQOrifice()));
      cmbSensDebit_.setSelectedIndex(model_.getSensDebit().value());
    }
  }
  private void construc4Panels() {
    BuLabel lePlusGrandLabel= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("cote moy. de la cr�te")+" (m)");
    Dimension dimPlusGrandLabel= lePlusGrandLabel.getPreferredSize();
    GridBagLayout gridBagLayout= new GridBagLayout();
    GridBagConstraints[] tabConstraintsLabel= new GridBagConstraints[4];
    GridBagConstraints[] tabConstraintsTextField= new GridBagConstraints[4];
    construcTabConstrainte(2,4,tabConstraintsLabel, tabConstraintsTextField);

    String[] tabTextLabel= new String[4];
    BuTextField[] tabTextField= new BuTextField[4];
    // pour la construction du Panel contenant les champs de saisie du seuil
    tabTextLabel[0]= Hydraulique1dResource.HYDRAULIQUE1D.getString("cote moy. de la cr�te")+" (m)";
    tfSeuilCote_= BuTextField.createDoubleField();
    tabTextField[0]= tfSeuilCote_;
    tabTextLabel[1]= Hydraulique1dResource.HYDRAULIQUE1D.getString("largeur")+" (m)";
    tfSeuilLargeur_= BuTextField.createDoubleField();
    tabTextField[1]= tfSeuilLargeur_;
    tabTextLabel[2]= Hydraulique1dResource.HYDRAULIQUE1D.getString("coefficient de d�bit");
    tfSeuilCoefQ_= BuTextField.createDoubleField();
    tabTextField[2]= tfSeuilCoefQ_;
    tabTextLabel[3]= Hydraulique1dResource.HYDRAULIQUE1D.getString("coefficient d'activation");
    tfSeuilCoefActivation_= BuTextField.createDoubleField();
    tabTextField[3]= tfSeuilCoefActivation_;
    pnTFSeuil_=
      construcPn4champSaisies(
        gridBagLayout,
        tabConstraintsLabel,
        tabConstraintsTextField,
        tabTextLabel,
        tabTextField,
        dimPlusGrandLabel);
    // pour la construction du Panel contenant les champs de saisie du chenal
    tabTextLabel[0]= Hydraulique1dResource.HYDRAULIQUE1D.getString("cote moy. du fond")+" (m)";
    tfChenalCote_= BuTextField.createDoubleField();
    tabTextField[0]= tfChenalCote_;
    tabTextLabel[1]= Hydraulique1dResource.HYDRAULIQUE1D.getString("largeur")+" (m)";
    tfChenalLargeur_= BuTextField.createDoubleField();
    tabTextField[1]= tfChenalLargeur_;
    tabTextLabel[2]= Hydraulique1dResource.HYDRAULIQUE1D.getString("longueur")+" (m)";
    tfChenalLongueur_= BuTextField.createDoubleField();
    tabTextField[2]= tfChenalLongueur_;
    tabTextLabel[3]= Hydraulique1dResource.HYDRAULIQUE1D.getString("rugosit�")+" (Strickler)";
    tfChenalRugosite_= BuTextField.createDoubleField();
    tabTextField[3]= tfChenalRugosite_;
    pnTFChenal_=
      construcPn4champSaisies(
        gridBagLayout,
        tabConstraintsLabel,
        tabConstraintsTextField,
        tabTextLabel,
        tabTextField,
        dimPlusGrandLabel);
    // pour la construction du Panel contenant les champs de saisie du Siphon
    tabTextLabel[0]= Hydraulique1dResource.HYDRAULIQUE1D.getString("cote moy. du fond")+" (m)";
    tfSiphonCote_= BuTextField.createDoubleField();
    tabTextField[0]= tfSiphonCote_;
    tabTextLabel[1]= Hydraulique1dResource.HYDRAULIQUE1D.getString("longueur")+" (m)";
    tfSiphonLongueur_= BuTextField.createDoubleField();
    tabTextField[1]= tfSiphonLongueur_;
    tabTextLabel[2]= Hydraulique1dResource.HYDRAULIQUE1D.getString("section")+" (m2)";
    tfSiphonSection_= BuTextField.createDoubleField();
    tabTextField[2]= tfSiphonSection_;
    tabTextLabel[3]= Hydraulique1dResource.HYDRAULIQUE1D.getString("coef. perte de charge");
    tfSiphonCoefPerteCharge_= BuTextField.createDoubleField();
    tabTextField[3]= tfSiphonCoefPerteCharge_;
    pnTFSiphon_=
      construcPn4champSaisies(
        gridBagLayout,
        tabConstraintsLabel,
        tabConstraintsTextField,
        tabTextLabel,
        tabTextField,
        dimPlusGrandLabel);
    // pour la construction du Panel contenant les champs de saisie de l'Orifice
    tabConstraintsLabel= new GridBagConstraints[6];
    tabConstraintsTextField= new GridBagConstraints[6];
    construcTabConstrainte(3,4,tabConstraintsLabel, tabConstraintsTextField);
    tabTextLabel= new String[6];
    tabTextField= new BuTextField[5];
    tabTextLabel[0]= Hydraulique1dResource.HYDRAULIQUE1D.getString("cote moy. du radier")+" (m)";
    tfOrificeCote_= BuTextField.createDoubleField();
    tabTextField[0]= tfOrificeCote_;

    tabTextLabel[1]= Hydraulique1dResource.HYDRAULIQUE1D.getString("largeur")+" (m)";
    tfOrificeLargeur_= BuTextField.createDoubleField();
    tabTextField[1]= tfOrificeLargeur_;
    tabTextLabel[2]= Hydraulique1dResource.HYDRAULIQUE1D.getString("section")+" (m2)";
    tfOrificeSection_= BuTextField.createDoubleField();
    tabTextField[2]= tfOrificeSection_;
    tabTextLabel[3]= Hydraulique1dResource.HYDRAULIQUE1D.getString("coef. de d�bit seuil");
    tfOrificeCoefQSeuil_= BuTextField.createDoubleField();
    tabTextField[3]= tfOrificeCoefQSeuil_;
    tabTextLabel[4]= Hydraulique1dResource.HYDRAULIQUE1D.getString("coef. de d�bit orifice");
    tfOrificeCoefQOrifice_= BuTextField.createDoubleField();
    tabTextField[4]= tfOrificeCoefQOrifice_;
    tabTextLabel[5]= Hydraulique1dResource.HYDRAULIQUE1D.getString("sens d�bit");
    cmbSensDebit_= new BuComboBox(TYPES_SENS_DEBIT_BIEF_CASIER);
    pnTFOrifice_=
      construcPnOrifice(
        gridBagLayout,
        tabConstraintsLabel,
        tabConstraintsTextField,
        tabTextLabel,
        tabTextField,
        dimPlusGrandLabel);
  }

  private void construcTabConstrainte(int nbLigne, int nbColonne,
                                      GridBagConstraints[] tabConstraintsLabel,
                                      GridBagConstraints[]
                                      tabConstraintsTextField) {
    Insets inset3_5= new Insets(3, 5, 0, 0);
    int indiceTab=0;
    for (int i= 0; i < nbLigne; i++) {
      for (int j = 0; j < nbColonne; j+=2) {
        tabConstraintsLabel[indiceTab++] =
            new GridBagConstraints(
            j,
            i,
            1,
            1,
            0.0,
            0.0,
            GridBagConstraints.WEST,
            GridBagConstraints.NONE,
            inset3_5,
            10,
            0);
      }
    }
    indiceTab=0;
    for (int i= 0; i < nbLigne; i++) {
      for (int j = 1; j < nbColonne; j+=2) {
        tabConstraintsTextField[indiceTab++] =
        new GridBagConstraints(
        j,
        i,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0);
     }
    }
  }
  private BuPanel construcPn4champSaisies(
      GridBagLayout gridBagLayout,
      GridBagConstraints[] tabConstraintsLabel,
      GridBagConstraints[] tabConstraintsTextField,
      String[] tabTextLabel,
      BuTextField[] tabTextField,
      Dimension dimPlusGrandLabel) {
    BuPanel panel= new BuPanel();
    panel.setLayout(gridBagLayout);
    Border outSideBorder= BorderFactory.createEtchedBorder();
    Border inSideBorder= BorderFactory.createEmptyBorder(5, 5, 5, 5);
    panel.setBorder(
      BorderFactory.createCompoundBorder(outSideBorder, inSideBorder));
    for (int i= 0; i < tabConstraintsLabel.length; i++) {
      BuLabel lb= new BuLabel(tabTextLabel[i]);
      lb.setPreferredSize(dimPlusGrandLabel);
      panel.add(lb, tabConstraintsLabel[i]);
      BuTextField tf= tabTextField[i];
      tf.setColumns(12);
      panel.add(tf, tabConstraintsTextField[i]);
    }
    return panel;
  }
  private BuPanel construcPnOrifice(
      GridBagLayout gridBagLayout,
      GridBagConstraints[] tabConstraintsLabel,
      GridBagConstraints[] tabConstraintsTextField,
      String[] tabTextLabel,
      BuTextField[] tabTextField,
      Dimension dimPlusGrandLabel) {
    BuPanel panel= new BuPanel();
    panel.setLayout(gridBagLayout);
    Border outSideBorder= BorderFactory.createEtchedBorder();
    Border inSideBorder= BorderFactory.createEmptyBorder(5, 5, 5, 5);
    panel.setBorder(
      BorderFactory.createCompoundBorder(outSideBorder, inSideBorder));
    for (int i= 0; i < tabConstraintsLabel.length; i++) {
      BuLabel lb= new BuLabel(tabTextLabel[i]);
      lb.setPreferredSize(dimPlusGrandLabel);
      panel.add(lb, tabConstraintsLabel[i]);
      if (i != 5) {
        BuTextField tf= tabTextField[i];
        tf.setColumns(12);
        panel.add(tf, tabConstraintsTextField[i]);
      } else {
        panel.add(cmbSensDebit_, tabConstraintsTextField[i]);
      }
    }
    return panel;
  }
  private BuPanel constructPanelRbLigne(BuRadioButton rb) {
    BuPanel res= new BuPanel();
    res.setLayout(new BuBorderLayout());
    BuPanel pnRbPnLigne= new BuPanel();
    pnRbPnLigne.setLayout(new BuBorderLayout());
    BuPanel pnLigne= new BuPanel();
    pnLigne.setPreferredSize(new Dimension(30, 3));
    pnLigne.setBorder(
      new LineChoiceBorder(false, false, false, false, false, true));
    pnRbPnLigne.add(rb, BuBorderLayout.WEST);
    pnRbPnLigne.add(pnLigne, BuBorderLayout.CENTER);
    res.add(pnRbPnLigne, BuBorderLayout.NORTH);
    return res;
  }
  private void setEnabledChenal(boolean enabled) {
    tfChenalCote_.setEnabled(enabled);
    tfChenalLargeur_.setEnabled(enabled);
    tfChenalLongueur_.setEnabled(enabled);
    tfChenalRugosite_.setEnabled(enabled);
    pnTFChenal_.setEnabled(enabled);
  }
  private void setEnabledSeuil(boolean enabled) {
    tfSeuilCote_.setEnabled(enabled);
    tfSeuilLargeur_.setEnabled(enabled);
    tfSeuilCoefQ_.setEnabled(enabled);
    tfSeuilCoefActivation_.setEnabled(enabled);
    pnTFSeuil_.setEnabled(enabled);
  }
  private void setEnabledSiphon(boolean enabled) {
    tfSiphonCote_.setEnabled(enabled);
    tfSiphonLongueur_.setEnabled(enabled);
    tfSiphonSection_.setEnabled(enabled);
    tfSiphonCoefPerteCharge_.setEnabled(enabled);
    pnTFSiphon_.setEnabled(enabled);
  }
  private void setEnabledOrifice(boolean enabled) {
    tfOrificeCote_.setEnabled(enabled);
    tfOrificeLargeur_.setEnabled(enabled);
    tfOrificeSection_.setEnabled(enabled);
    tfOrificeCoefQSeuil_.setEnabled(enabled);
    tfOrificeCoefQOrifice_.setEnabled(enabled);
    cmbSensDebit_.setEnabled(enabled);
    pnTFOrifice_.setEnabled(enabled);
  }
  private void initChenal() {
    tfChenalCote_.setValue(new Double(1));
    tfChenalLargeur_.setValue(new Double(5));
    tfChenalLongueur_.setValue(new Double(10));
    tfChenalRugosite_.setValue(new Double(30));
  }
  private void initSeuil() {
    tfSeuilCote_.setValue(new Double(1));
    tfSeuilLargeur_.setValue(new Double(5));
    tfSeuilCoefQ_.setValue(new Double(0.4));
    tfSeuilCoefActivation_.setValue(new Double(0.5));
  }
  private void initSiphon() {
    tfSiphonCote_.setValue(new Double(1));
    tfSiphonLongueur_.setValue(new Double(10));
    tfSiphonCoefPerteCharge_.setValue(new Double(0.5));
    tfSiphonSection_.setValue(new Double(10));
  }
  private void initOrifice() {
    tfOrificeCote_.setValue(new Double(1));
    tfOrificeLargeur_.setValue(new Double(5));
    tfOrificeSection_.setValue(new Double(10));
    tfOrificeCoefQSeuil_.setValue(new Double(0.4));
    tfOrificeCoefQOrifice_.setValue(new Double(0.5));
    cmbSensDebit_.setSelectedIndex(EnumMetierSensDebitLiaison._DEUX_SENS);
  }
  public static void main(String[] arg) {
    BuPanel panel= new Hydraulique1dCasierCaracLiaisonPanel();
    JFrame fenetre= new JFrame("test Hydraulique1dCasierCaracLiaisonPanel");
    fenetre.getContentPane().add(panel);
    fenetre.pack();
    fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fenetre.setVisible(true);
  }
}
