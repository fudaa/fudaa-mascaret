/*
 * @file         Hydraulique1dCasierEditor.java
 * @creation     2003-06-20
 * @modification $Date: 2007-11-20 11:43:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierNuagePointsCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierPlanimetrageCasier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizerImprimable;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_CasierPlanim;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_CasierSemiPoints;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'un casier (MetierCasier).<br>
 * Appeler si l'utilisateur clic sur un casier du r�seau de type "Hydraulique1dReseauCasier".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().CASIER().editer().<br>
 * Utilise 2 panneaux Hydraulique1dCasierApportDebitPanel et Hydraulique1dCasierGeometriePanel.
 * @version      $Revision: 1.14 $ $Date: 2007-11-20 11:43:28 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
  private BuTextField tfNom_, tfCoteInitiale_;
  private BuPanel pnCasierEditor_;
  private BuPanel pnNord_;
  private Hydraulique1dCasierApportDebitPanel pnApportDebit_;
  private Hydraulique1dCasierGeometriePanel pnGeometrie_;
  private MetierCasier param_;
  private MetierDonneesHydrauliques donneesHydro_;
  public Hydraulique1dCasierEditor() {
    this(null);
  }
  public Hydraulique1dCasierEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Casier"));
    param_= null;
    pnCasierEditor_= new BuPanel();
    pnCasierEditor_.setLayout(new BuVerticalLayout(10));
    Border borderExt= BorderFactory.createEtchedBorder();
    Border borderInt= BorderFactory.createEmptyBorder(10, 10, 10, 10);
    pnCasierEditor_.setBorder(
      BorderFactory.createCompoundBorder(borderExt, borderInt));
    Insets inset3_5= new Insets(3, 5, 0, 0);
    // *********** construction du panel Nord   ********************************
    pnNord_= new BuPanel();
    pnNord_.setLayout(new GridBagLayout());
    // label et champ de saisie "nom"
    BuLabel lbNom= new BuLabel(getS("nom"));
    pnNord_.add(
      lbNom,
      new GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    tfNom_= new BuTextField();
    pnNord_.add(
      tfNom_,
      new GridBagConstraints(
        1,
        0,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    // label et champ de saisie "cote initiale (m)"
    BuLabel lbCoteInitiale= new BuLabel(getS("cote initiale")+" (m)");
    pnNord_.add(
      lbCoteInitiale,
      new GridBagConstraints(
        0,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    tfCoteInitiale_= BuTextField.createDoubleField();
    pnNord_.add(
      tfCoteInitiale_,
      new GridBagConstraints(
        1,
        1,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    tfNom_.setColumns(10);
    tfCoteInitiale_.setColumns(10);
    // *********** FIN construction du panel Nord   ****************************
    // *********** Panel "Apport de d�bit" au centre **************************************
    pnApportDebit_= new Hydraulique1dCasierApportDebitPanel();
    pnApportDebit_.addActionDefinirLoi(this);

    // *********** FIN Panel "Type" au centre **********************************
    // *********** Panel "G�om�trie" au sud **************************************
    pnGeometrie_= new Hydraulique1dCasierGeometriePanel();
    pnGeometrie_.addActionEditerGeometrie(this);
    // *********** FIN Panel "Type" au centre **********************************
    BuPanel pnEnveloppeNord= new BuPanel();
    pnEnveloppeNord.setLayout(new GridLayout(1, 2));
    BuPanel pnVide= new BuPanel();
    pnEnveloppeNord.add(pnNord_);
    pnEnveloppeNord.add(pnVide);
    pnCasierEditor_.add(pnEnveloppeNord, 0);
    pnCasierEditor_.add(pnApportDebit_, 1);
    BuPanel pnEnveloppeGeo= new BuPanel();
    pnEnveloppeGeo.setLayout(new BuBorderLayout());
    pnEnveloppeGeo.add(pnGeometrie_, BuBorderLayout.WEST);
    pnCasierEditor_.add(pnEnveloppeGeo, 2);
    Container pnMain_= getContentPane();
    pnMain_.add(new BuScrollPane(pnCasierEditor_), BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("casier", null, param_);
      }
      fermer();
    }
    else if (cmd.equals("EDITER_GEO_CASIER_"+getS("Planim�trie"))) {
      if (param_.geometrie() instanceof MetierNuagePointsCasier) {
        param_.toPlanimetrage();
      }
      Hydraulique1dIHM_CasierPlanim ihmCasierPlanim=
        Hydraulique1dIHMRepository.getInstance().CASIER_PLANIM();
      ihmCasierPlanim.setCasier(param_);
      ihmCasierPlanim.editer();
    }
    else if (cmd.equals("EDITER_GEO_CASIER_"+getS("Semis de points"))) {
      if (param_.geometrie() instanceof MetierPlanimetrageCasier) {
        param_.toNuagePoints();
      }
      Hydraulique1dIHM_CasierSemiPoints ihmCasierSemiPoints=
        Hydraulique1dIHMRepository.getInstance().CASIER_SEMI_POINTS();
      ihmCasierSemiPoints.setCasier(param_);
      ihmCasierSemiPoints.editer();
    }
    else if (cmd.startsWith("DEFINIR_LOI")) {
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().setQualiteDEau(false);
      Hydraulique1dIHMRepository.getInstance().LIBRARY_LOI().editer();
    }
    else {
      super.actionPerformed(_evt);
    }
  }
  // ObjetEventListener
  @Override
  public void objetCree(H1dObjetEvent e) {
    MetierHydraulique1d src= e.getSource();
    if (src == null)
      return;
    if (src instanceof MetierLoiHydraulique) {
      pnApportDebit_.initListeLoi();
    }
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
    MetierHydraulique1d src= e.getSource();
    if (src == null)
      return;
    if (src instanceof MetierLoiHydraulique) {
      pnApportDebit_.initListeLoi();
    }
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
    MetierHydraulique1d src= e.getSource();
    if (src == null)
      return;
    if (src instanceof MetierLoiHydraulique) {
      pnApportDebit_.initListeLoi();
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    try {
      double cote= ((Double)tfCoteInitiale_.getValue()).doubleValue();
      if (cote != param_.coteInitiale()) {
        param_.coteInitiale(cote);
        changed= true;
      }
      String nom= tfNom_.getText().trim();
      if (!nom.equals(param_.nom())) {
        param_.nom(nom);
        changed= true;
      }
      boolean changeApport= pnApportDebit_.getValeurs();
      if (changeApport) {
        changed= true;
      }
      boolean changeGeo= pnGeometrie_.getValeurs();
      if (changeGeo) {
        changed= true;
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }
    return changed;
  }
  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierCasier) {
      MetierCasier param= (MetierCasier)_n;
      param_= param;
      pnApportDebit_.setModel(param_);
      pnGeometrie_.setModel(param_);
      setValeurs();
    } else if (_n instanceof MetierDonneesHydrauliques) {
      MetierDonneesHydrauliques donnes= (MetierDonneesHydrauliques)_n;
      donneesHydro_= donnes;
      pnApportDebit_.setDonneesHydrauliques(donneesHydro_);
    }
  }
/*  public void addActionEditerGeometrie(ActionListener listener) {
    pnGeometrie_.addActionEditerGeometrie(listener);
  }
  public void addActionDefinirLoi(ActionListener listener) {
    pnApportDebit_.addActionDefinirLoi(listener);
  }*/
  @Override
  protected void setValeurs() {
    setTitle(getS("Casier n�")+param_.numero());
    tfNom_.setText(param_.nom());
    tfCoteInitiale_.setValue(new Double(param_.coteInitiale()));
    pnApportDebit_.setValeurs();
    pnGeometrie_.setValeurs();
  }
}
