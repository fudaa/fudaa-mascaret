/*
 * @file         Hydraulique1dCasierGeometriePanel.java
 * @creation     2003-06-18
 * @modification $Date: 2007-11-20 11:43:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.border.Border;

import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierGeometrieCasier;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Panneau de sélection de type de géométrie casier de l'éditeur de casier.
 * @version      $Revision: 1.7 $ $Date: 2007-11-20 11:43:29 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierGeometriePanel
  extends BuPanel
  implements ActionListener {
  private final static String[] TYPES_GEO= { Hydraulique1dResource.HYDRAULIQUE1D.getString("Planimétrie"),
                                             Hydraulique1dResource.HYDRAULIQUE1D.getString("Semis de points") };
  private BuButton btEditerGeo_, btImport_;
  private BuComboBox cmbGeo_;
  private MetierCasier model_;
  private MetierGeometrieCasier geoCasierImport_;
  public Hydraulique1dCasierGeometriePanel() {
    this(null);
  }
  public Hydraulique1dCasierGeometriePanel(MetierCasier model) {
    super();
    setLayout(new BuBorderLayout(5, 5));
    Border outSideBorder=
      BorderFactory.createTitledBorder(
        BorderFactory.createEtchedBorder(),
        Hydraulique1dResource.HYDRAULIQUE1D.getString("Géométrie"));
    Border inSideBorder= BorderFactory.createEmptyBorder(10, 20, 10, 20);
    setBorder(BorderFactory.createCompoundBorder(outSideBorder, inSideBorder));
    constructPanel();
    setModel(model);
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equals("IMPORTER")) {
      importer();
    } else if (cmd.equals("CHANGE_TYPE_GEO")) {
      btEditerGeo_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Editer")+" "+ cmbGeo_.getSelectedItem().toString());
      btEditerGeo_.setActionCommand(
        "EDITER_GEO_CASIER_" + cmbGeo_.getSelectedItem().toString());
    }
  }
  public void setModel(MetierCasier model) {
    model_= model;
    setValeurs();
  }
  public void addActionEditerGeometrie(ActionListener listener) {
    btEditerGeo_.addActionListener(listener);
  }
  boolean getValeurs() {
    boolean changed= false;
    try {
      if (geoCasierImport_ == null)
        return false;
      if (geoCasierImport_.isNuagePoints()) {
        if (!model_.isNuagePoints())
          model_.toNuagePoints();
      } else {
        if (!model_.isPlanimetrage())
          model_.toPlanimetrage();
      }
      if (!model_.geometrie().egale(geoCasierImport_)) {
        model_.geometrie(geoCasierImport_);
        changed= true;
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialisée");
      ex.printStackTrace();
      changed= false;
    }
    geoCasierImport_ = null;
    return changed;
  }
  void setValeurs() {
    if (model_ == null)
      return;
    if (model_.isPlanimetrage()) {
      cmbGeo_.setSelectedItem(TYPES_GEO[0]);
    } else {
      cmbGeo_.setSelectedItem(TYPES_GEO[1]);
    }
  }
  private void constructPanel() {
    setLayout(new GridBagLayout());
    btImport_= new BuButton();
    btImport_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Importer"));
    btImport_.setIcon(BuResource.BU.getIcon("importer"));
    btImport_.setName("btImporter");
    btImport_.setActionCommand("IMPORTER");
    btImport_.addActionListener(this);
    btEditerGeo_= new BuButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Editer ") + TYPES_GEO[0]);
    btEditerGeo_.setActionCommand("EDITER_GEO_CASIER");
    cmbGeo_= new BuComboBox(TYPES_GEO);
    cmbGeo_.setActionCommand("CHANGE_TYPE_GEO");
    cmbGeo_.setSelectedItem(TYPES_GEO[0]);
    cmbGeo_.addActionListener(this);
    Insets inset3_5= new Insets(3, 5, 0, 0);
    // label "Représentation de la géométrie"
    BuLabel lb= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Représentation de la géométrie"));
    add(
      lb,
      new GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // ComboBox
    add(
      cmbGeo_,
      new GridBagConstraints(
        1,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.BOTH,
        inset3_5,
        10,
        0));
    // bouton import
    add(
      btImport_,
      new GridBagConstraints(
        2,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // bouton Editer géométrie
    add(
      btEditerGeo_,
      new GridBagConstraints(
        1,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
  }
  protected void importer() {

	String[] extensionGeometrie = {"casier","geo"};
	String[] descriptionGeometrie = {Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier geométrie du casier"),
                                         Hydraulique1dResource.HYDRAULIQUE1D.getString("Fichier geométrie du casier")};
	File fichier=Hydraulique1dImport.chooseFile(extensionGeometrie,descriptionGeometrie);

    if (fichier != null) {
      try {
        MetierGeometrieCasier[] tabGeo=
          Hydraulique1dImport.geometrieCasier(fichier, true);
        if (tabGeo != null) {
          geoCasierImport_= tabGeo[0];
          if (geoCasierImport_.isNuagePoints()) {
            cmbGeo_.setSelectedItem(TYPES_GEO[1]);
          } else {
            cmbGeo_.setSelectedItem(TYPES_GEO[0]);
          }
          model_.geometrie(geoCasierImport_);
        }
      } catch (Exception exception) {
        System.err.println("$$$ " + exception);
        exception.printStackTrace();
      }
    }
  }
  public static void main(String[] arg) {
    Hydraulique1dCasierGeometriePanel panel=
      new Hydraulique1dCasierGeometriePanel();
    JFrame fenetre= new JFrame("test Hydraulique1dCasierGeometriePanel");
    fenetre.getContentPane().setLayout(new BuBorderLayout());
    panel.addActionEditerGeometrie(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        System.out.println("EDITER Géométrie");
      }
    });
    fenetre.getContentPane().add(panel, BuBorderLayout.CENTER);
    fenetre.pack();
    fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fenetre.setVisible(true);
  }
}
