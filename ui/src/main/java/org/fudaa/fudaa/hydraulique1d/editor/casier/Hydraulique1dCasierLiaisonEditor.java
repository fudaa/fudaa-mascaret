/*
 * @file         Hydraulique1dCasierLiaisonEditor.java
 * @creation     2003-06-10
 * @modification $Date: 2007-11-20 11:43:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizerImprimable;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur d'une liaison (MetierLiaison) avec des casiers (ou casier et bief).<br>
 * Appeler si l'utilisateur clic sur une liaison du r�seau de type "Hydraulique1dReseauLiaison".<br>
 * Lancer par l'instruction Hydraulique1dIHMRepository.getInstance().CASIER_LIAISON().editer().<br>
 * Utilise un panneau Hydraulique1dCasierCaracLiaisonPanel.
 * @version      $Revision: 1.13 $ $Date: 2007-11-20 11:43:29 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierLiaisonEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener {
// private BuTextField tfAbscisse_, tfCote_;
  private BuTextField tfAbscisse_;
  private BuLabel lbInfoBief_, lbAbsc_;
  private BuPanel pnCasierLiaisonEditor_;
  private BuPanel pnNord_;
  private Hydraulique1dCasierCaracLiaisonPanel pnCaracLiaison_;
  private MetierLiaison param_;
  public Hydraulique1dCasierLiaisonEditor() {
    this(null);
  }
  public Hydraulique1dCasierLiaisonEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Liaison"));
    param_= null;
    Insets inset3_5= new Insets(3, 5, 0, 0);
    pnCasierLiaisonEditor_= new BuPanel();
    pnCasierLiaisonEditor_.setLayout(new BuBorderLayout());
    pnCasierLiaisonEditor_.setBorder(BorderFactory.createEtchedBorder());
    // *********** construction du panel Nord   ********************************
    pnNord_= new BuPanel();
    pnNord_.setLayout(new GridBagLayout());
    // label et champ de saisie "abscisse (m) et label info Bief"
    lbAbsc_= new BuLabel(getS("abscisse")+" (m)");
    pnNord_.add(
      lbAbsc_,
      new GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    tfAbscisse_= BuTextField.createDoubleField();
    tfAbscisse_.setColumns(10);
    pnNord_.add(
      tfAbscisse_,
      new GridBagConstraints(
        1,
        0,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    lbInfoBief_= new BuLabel(getS("bief connect� sans information"));
    Dimension dimLabel= lbInfoBief_.getPreferredSize();
    pnNord_.add(
      lbInfoBief_,
      new GridBagConstraints(
        2,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
/*    // label et champ de saisie "cote (m)"
    BuLabel lbCote= new BuLabel("cote (m)");
    pnNord_.add(
      lbCote,
      new GridBagConstraints(
        0,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    tfCote_= BuTextField.createDoubleField();
    tfCote_.setColumns(10);
    pnNord_.add(
      tfCote_,
      new GridBagConstraints(
        1,
        1,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));*/
    BuPanel pnVide= new BuPanel();
    pnVide.setPreferredSize(dimLabel);
    pnNord_.add(
      pnVide,
      new GridBagConstraints(
        2,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    // *********** FIN construction du panel Nord   ****************************
    // *********** Panel "Type" au centre **************************************
    pnCaracLiaison_= new Hydraulique1dCasierCaracLiaisonPanel();
    // *********** FIN Panel "Type" au centre **********************************
    pnCasierLiaisonEditor_.add(pnNord_, BuBorderLayout.NORTH);
    pnCasierLiaisonEditor_.add(pnCaracLiaison_, BuBorderLayout.CENTER);
    Container pnMain_= getContentPane();
    BuScrollPane scrlpCasierLiaisonEditor=
      new BuScrollPane(pnCasierLiaisonEditor_);
    pnMain_.add(scrlpCasierLiaisonEditor, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("liaison", null, param_);
      }
      fermer();
    } else {
      super.actionPerformed(_evt);
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    try {
/*      double cote= ((Double)tfCote_.getValue()).doubleValue();
      if (cote != param_.getCote()) {
        param_.setCote(cote);
        changed= true;
      }*/
      if (param_.isRiviereCasier()) {
        double abscisse= ((Double)tfAbscisse_.getValue()).doubleValue();
        if (abscisse != param_.getAbscisse()) {
          param_.setAbscisse(abscisse);
          changed= true;
        }
      }
      boolean changeCarac= pnCaracLiaison_.getValeurs();
      if (changeCarac) {
        changed= true;
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }
    return changed;
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (!(_n instanceof MetierLiaison))
      return;
    MetierLiaison param= (MetierLiaison)_n;
    param_= param;
    pnCaracLiaison_.setModel(param_);
    setValeurs();
  }
  @Override
  protected void setValeurs() {
    setTitle(getS("Liaison n�")+param_.numero());
    pnCaracLiaison_.setValeurs();
    if (param_.isRiviereCasier()) {
      lbAbsc_.setVisible(true);
      lbInfoBief_.setVisible(true);
      tfAbscisse_.setVisible(true);
      tfAbscisse_.setValue(new Double(param_.getAbscisse()));
      String textLbInfoBief= "";
      MetierBief bief= param_.getBiefRattache();
      if (bief != null) {
        textLbInfoBief= getS("du bief n�") + (bief.indice()+1);
        if ((bief.extrAmont().profilRattache() != null)
          && (bief.extrAval().profilRattache() != null))
          textLbInfoBief=
            textLbInfoBief
              + getS(" entre ")
              + bief.extrAmont().profilRattache().abscisse()
              + getS(" et ")
              + bief.extrAval().profilRattache().abscisse();
        else
          textLbInfoBief=
            textLbInfoBief + " ("+getS("abscisses des extremit�s inconnues")+")";
      } else
        textLbInfoBief= getS("bief inconnu");
      lbInfoBief_.setText(textLbInfoBief);
    } else if (param_.isCasierCasier()) {
      lbAbsc_.setVisible(false);
      lbInfoBief_.setVisible(false);
      tfAbscisse_.setVisible(false);
    }
    pnCaracLiaison_.setValeurs();
  }
}
