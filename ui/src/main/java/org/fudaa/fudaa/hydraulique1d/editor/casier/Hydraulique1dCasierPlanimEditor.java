/*
 * @file         Hydraulique1dCasierPlanimEditor.java
 * @creation     2003-06-20
 * @modification $Date: 2007-11-20 11:43:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;

import javax.swing.BorderFactory;
import javax.swing.JSplitPane;
import javax.swing.border.Border;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierPlanimetrageCasier;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizerImprimable;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dTableauCasierPlanimSurGraphe;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dTableauCasierPlanimVolGraphe;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuPanel;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur du tableau du planim�trage de la g�om�trie d'un casier (MetierPlanimetrageCasier).<br>
 * Appeler si l'utilisateur clic sur le bouton "Editer planim�trie" apr�s avoir s�lectionner
 * "Planim�trie" dans l'�diteur du casier.<br>
 * Lancer � partir de la classe Hydraulique1dCasierEditor avec l'aide de l'instruction
 * Hydraulique1dIHMRepository.getInstance().CASIER_PLANIM().editer().<br>
 * Utilise un panneau Hydraulique1dCasierTableauPlanimPanel contenant le tableau et
 * 2 graphiques Hydraulique1dTableauCasierPlanimSurGraphe et Hydraulique1dTableauCasierPlanimVolGraphe.<br>
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:43:27 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierPlanimEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener, BuCutCopyPasteInterface {
  private BuPanel pnCasierEditor_;
  private Hydraulique1dCasierTableauPlanimPanel pnTableau_;
  private Hydraulique1dTableauCasierPlanimSurGraphe grapheSurface_;
  private Hydraulique1dTableauCasierPlanimVolGraphe grapheVolume_;
  private BuButton btEditAxesSurf_,
    btPersoGrapheSurf_,
    btEditAxesVol_,
    btPersoGrapheVol_;
  private MetierPlanimetrageCasier param_;
  public Hydraulique1dCasierPlanimEditor() {
    this(null);
  }
  public Hydraulique1dCasierPlanimEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Planim�trage Casier"));
    param_= null;
    pnCasierEditor_= new BuPanel();
    pnCasierEditor_.setLayout(new BuBorderLayout(10, 10));
    Border borderExt= BorderFactory.createEtchedBorder();
    Border borderInt= BorderFactory.createEmptyBorder(10, 10, 10, 10);
    pnCasierEditor_.setBorder(
      BorderFactory.createCompoundBorder(borderExt, borderInt));
    //Insets inset3_5= new Insets(3, 5, 0, 0);
    pnTableau_= new Hydraulique1dCasierTableauPlanimPanel();
    grapheSurface_= new Hydraulique1dTableauCasierPlanimSurGraphe();
    btEditAxesSurf_= new BuButton(getS("Editer Axes"));
    btEditAxesSurf_.setActionCommand("EDITER_AXES_SURFACE");
    btEditAxesSurf_.addActionListener(this);
    btPersoGrapheSurf_= new BuButton(getS("Personaliser Graphe"));
    btPersoGrapheSurf_.setActionCommand("PERSONNALISER_GRAPHE_SURFACE");
    btPersoGrapheSurf_.addActionListener(this);
    btEditAxesVol_= new BuButton(getS("Editer Axes"));
    btEditAxesVol_.setActionCommand("EDITER_AXES_VOLUME");
    btEditAxesVol_.addActionListener(this);
    btPersoGrapheVol_= new BuButton(getS("Personaliser Graphe"));
    btPersoGrapheVol_.setActionCommand("PERSONNALISER_GRAPHE_VOLUME");
    btPersoGrapheVol_.addActionListener(this);
    grapheVolume_= new Hydraulique1dTableauCasierPlanimVolGraphe();
    BuBorderLayout bdLayout= new BuBorderLayout(10, 10);
    BuPanel pnGrapheSurface= new BuPanel();
    pnGrapheSurface.setLayout(bdLayout);
    pnGrapheSurface.add(grapheSurface_, BuBorderLayout.CENTER);
    BuPanel btsSurface= new BuPanel();
    btsSurface.add(btEditAxesSurf_);
    btsSurface.add(btPersoGrapheSurf_);
    pnGrapheSurface.add(btsSurface, BuBorderLayout.SOUTH);
    BuPanel pnGrapheVolume= new BuPanel();
    pnGrapheVolume.setLayout(bdLayout);
    pnGrapheVolume.add(grapheVolume_, BuBorderLayout.CENTER);
    BuPanel btsVolume= new BuPanel();
    btsVolume.add(btEditAxesVol_);
    btsVolume.add(btPersoGrapheVol_);
    pnGrapheVolume.add(btsVolume, BuBorderLayout.SOUTH);
    Dimension dimGraph= new Dimension(250, 200);
    pnGrapheSurface.setPreferredSize(dimGraph);
    pnGrapheVolume.setPreferredSize(dimGraph);
    JSplitPane sptHorizontal=
      new JSplitPane(
        JSplitPane.VERTICAL_SPLIT,
        pnGrapheSurface,
        pnGrapheVolume);
    sptHorizontal.setOneTouchExpandable(true);
    pnTableau_.setPreferredSize(new Dimension(350, 450));
    JSplitPane sptVert=
      new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnTableau_, sptHorizontal);
    sptVert.setOneTouchExpandable(true);
    pnCasierEditor_.add(sptVert, BuBorderLayout.CENTER);
    Container pnMain_= getContentPane();
    pnMain_.add(pnCasierEditor_, BorderLayout.CENTER);
    pnTableau_.addTableModelListener(grapheSurface_);
    pnTableau_.addTableModelListener(grapheVolume_);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("casier", null, param_);
      }
      fermer();
    } else if ("EDITER_AXES_SURFACE".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          getS("EDITER LES AXES SURFACE"),
          grapheSurface_.getEditeurAxes());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else if ("PERSONNALISER_GRAPHE_SURFACE".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          getS("PERSONNALISER LE GRAPHE SURFACE"),
          grapheSurface_.getPersonnaliseurGraphe());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else if ("EDITER_AXES_VOLUME".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          getS("EDITER LES AXES VOLUME"),
          grapheVolume_.getEditeurAxes());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else if ("PERSONNALISER_GRAPHE_VOLUME".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          getS("PERSONNALISER LE GRAPHE VOLUME"),
          grapheVolume_.getPersonnaliseurGraphe());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else {
      super.actionPerformed(_evt);
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    try {
      boolean changeTab= pnTableau_.getValeurs();
      if (changeTab) {
        changed= true;
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }
    return changed;
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierPlanimetrageCasier) {
      MetierPlanimetrageCasier param= (MetierPlanimetrageCasier)_n;
      param_= param;
      pnTableau_.setModel(param_);
      setValeurs();
    }
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void cut() {
    pnTableau_.cut();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void copy() {
    pnTableau_.copy();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void paste() {
    pnTableau_.paste();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void duplicate() {
    pnTableau_.duplicate();
  }
  @Override
  public int print(Graphics _g, PageFormat _format, int _page) {
    if (_page == 0)
      return grapheSurface_.print(_g, _format, _page);
    return grapheVolume_.print(_g, _format, _page - 1);
  }
  @Override
  public int getNumberOfPages() {
    return 2;
  }
  public String[] getEnabledActions() {
    String[] r= new String[] { "IMPRIMER", "PREVISUALISER", "MISEENPAGE" };
    return r;
  }
  @Override
  protected void setValeurs() {
    pnTableau_.setValeurs();
  }
}
