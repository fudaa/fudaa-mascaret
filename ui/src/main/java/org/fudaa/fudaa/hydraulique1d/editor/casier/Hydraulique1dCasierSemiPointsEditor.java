/*
 * @file         Hydraulique1dCasierSemiPointsEditor.java
 * @creation     2003-06-30
 * @modification $Date: 2007-11-20 11:43:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JSplitPane;
import javax.swing.border.Border;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierNuagePointsCasier;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dDialogContraintes;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizerImprimable;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dTableauCasierSemiPointsGraphe;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuMultiLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Editeur des tableaux du semi de point de la g�om�trie d'un casier (MetierNuagePointsCasier).<br>
 * Appeler si l'utilisateur clic sur le bouton "Editer semis de points" apr�s avoir s�lectionner
 * "Semis de points" dans l'�diteur du casier.<br>
 * Lancer � partir de la classe Hydraulique1dCasierEditor avec l'aide de l'instruction
 * Hydraulique1dIHMRepository.getInstance().CASIER_SEMI_POINTS().editer().<br>
 * Utilise 2 panneaux Hydraulique1dCasierTableauXYZPanel contenant les tableaux des points 3D et
 * un graphique Hydraulique1dTableauCasierSemiPointsGraphe.<br>
 * @version      $Revision: 1.14 $ $Date: 2007-11-20 11:43:27 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierSemiPointsEditor
  extends Hydraulique1dCustomizerImprimable
  implements ActionListener, BuCutCopyPasteInterface {
  private final static String[] OPTIONS_CALCUL=
    { Hydraulique1dResource.HYDRAULIQUE1D.getString("Surface const"),
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Surface f(Z)") };
  private BuPanel pnCasierEditor_;
  private Hydraulique1dCasierTableauXYZPanel pnTableauPtsFrontieres_;
  private Hydraulique1dCasierTableauXYZPanel pnTableauPtsInterieurs_;
  private Hydraulique1dTableauCasierSemiPointsGraphe graphe_;
  private BuTextField tfPasPlanim_;
  private BuComboBox cmbOptionCalcul_;
  private BuButton btEditAxes_, btPersoGraphe_;
  private BuButton btSupprFond_, btImportFond_;
  private BuMultiLabel lbparamsAvances_;
  private MetierNuagePointsCasier param_;
  public Hydraulique1dCasierSemiPointsEditor() {
    this(null);
  }
  public Hydraulique1dCasierSemiPointsEditor(BDialogContent parent) {
    super(parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Semi points Casier"));
    param_= null;
    pnCasierEditor_= new BuPanel();
    pnCasierEditor_.setLayout(new BuBorderLayout(10, 10));
    Border borderExt= BorderFactory.createEtchedBorder();
    Border borderInt= BorderFactory.createEmptyBorder(10, 10, 10, 10);
    pnCasierEditor_.setBorder(
      BorderFactory.createCompoundBorder(borderExt, borderInt));
    BuPanel pnCalculPlanimetrage= createPanelCalculPlanimetrage();
    pnTableauPtsFrontieres_=
      new Hydraulique1dCasierTableauXYZPanel(
        Hydraulique1dCasierTableauXYZPanel.POINTS_FRONTIERES);
    pnTableauPtsInterieurs_=
      new Hydraulique1dCasierTableauXYZPanel(
        Hydraulique1dCasierTableauXYZPanel.POINTS_INTERIEURS);
    BuPanel pnWest= new BuPanel();
    pnWest.setLayout(new BuBorderLayout(10, 10));
    pnWest.add(pnCalculPlanimetrage, BuBorderLayout.NORTH);
    BuPanel pnTableaux= new BuPanel();
    pnTableaux.setLayout(new GridLayout(2, 1));
    pnTableaux.add(pnTableauPtsFrontieres_);
    pnTableaux.add(pnTableauPtsInterieurs_);
    pnWest.add(pnTableaux, BuBorderLayout.CENTER);
    graphe_= new Hydraulique1dTableauCasierSemiPointsGraphe();
    btEditAxes_= new BuButton(getS("Editer Axes"));
    btEditAxes_.setActionCommand("EDITER_AXES");
    btEditAxes_.addActionListener(this);
    btPersoGraphe_= new BuButton(getS("Personaliser Graphe"));
    btPersoGraphe_.setActionCommand("PERSONNALISER_GRAPHE");
    btPersoGraphe_.addActionListener(this);
    btSupprFond_= new BuButton();
    btSupprFond_.setText(getS("Supprimer"));
    btSupprFond_.setIcon(BuResource.BU.getIcon("detruire"));
    btSupprFond_.setName("btSUPPRIMER");
    btSupprFond_.setActionCommand("SUPPRIMER_FOND");
    btSupprFond_.addActionListener(this);
    btImportFond_= new BuButton();
    btImportFond_.setText(getS("Importer"));
    btImportFond_.setIcon(BuResource.BU.getIcon("importer"));
    btImportFond_.setName("btImport");
    btImportFond_.setActionCommand("IMPORTER_FOND");
    btImportFond_.addActionListener(this);
    graphe_= new Hydraulique1dTableauCasierSemiPointsGraphe();
    BuBorderLayout bdLayout= new BuBorderLayout(10, 10);
    BuPanel pnGraphe= new BuPanel();
    pnGraphe.setLayout(bdLayout);
    pnGraphe.add(graphe_, BuBorderLayout.CENTER);
    BuPanel pnRepresentationEnPlan= new BuPanel();
    Border titleBorder=
      BorderFactory.createTitledBorder(
        BorderFactory.createEtchedBorder(),
        getS("Repr�sentation en plan"));
    pnRepresentationEnPlan.setBorder(
      BorderFactory.createCompoundBorder(titleBorder, borderInt));
    pnRepresentationEnPlan.setLayout(new BuVerticalLayout(0));
    BuPanel pnImageFond= new BuPanel();
    pnImageFond.add(new BuLabel(getS("Image de fond")));
    pnImageFond.add(btSupprFond_);
    pnImageFond.add(btImportFond_);
    pnRepresentationEnPlan.add(pnImageFond);
    BuPanel pn2BtsGraphe= new BuPanel();
    pn2BtsGraphe.add(btEditAxes_);
    pn2BtsGraphe.add(btPersoGraphe_);
    pnRepresentationEnPlan.add(pn2BtsGraphe);
    pnGraphe.add(pnRepresentationEnPlan, BuBorderLayout.NORTH);
    JSplitPane sptVert=
      new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnWest, pnGraphe);
    sptVert.setOneTouchExpandable(true);
    pnCasierEditor_.add(sptVert, BuBorderLayout.CENTER);
    Container pnMain_= getContentPane();
    pnMain_.add(pnCasierEditor_, BorderLayout.CENTER);
    pnTableauPtsFrontieres_.addTableModelListener(graphe_);
    pnTableauPtsInterieurs_.addTableModelListener(graphe_);
    setNavPanel(EbliPreferences.DIALOG.VALIDER|EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      String messageTableauFront= pnTableauPtsFrontieres_.validationDonnees();
      String messageTableauInter= pnTableauPtsInterieurs_.validationDonnees();
      double plusPetitEcartZPtsInter=
        pnTableauPtsInterieurs_.getPlusPetitEcartCote();
      double pasPlanim= ((Double)tfPasPlanim_.getValue()).doubleValue();
      String messageEcartZ= "";
      if (plusPetitEcartZPtsInter != Double.MAX_VALUE) {
        if (pasPlanim < plusPetitEcartZPtsInter) {
          messageEcartZ =getS("Le pas de planim�trage")+" (" + pasPlanim + ") "+getS("doit �tre sup�rieur au");
          messageEcartZ+= "\n"+getS("plus petit �cart entre les cotes des points int�rieurs")+" ("
            + plusPetitEcartZPtsInter
            + ")";
        }
      }
      if (messageTableauFront.isEmpty()
        && messageTableauInter.isEmpty()
        && messageEcartZ.isEmpty()) {
        if (getValeurs()) {
          firePropertyChange("casier", null, param_);
        }
        fermer();
      } else {
        int resp= new Hydraulique1dDialogContraintes(
          (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
          ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
            .getInformationsSoftware(),
          getS("Contraintes non v�rifi�es")+" :\n"
            + messageTableauFront
            + "\n\n"
            + messageTableauInter
            + "\n\n"
            + messageEcartZ,true)
          .activate();
        if (resp==Hydraulique1dDialogContraintes.IGNORER) fermer();
      }
    } else if ("EDITER_AXES".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          getS("EDITER LES AXES"),
          graphe_.getEditeurAxes());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else if ("PERSONNALISER_GRAPHE".equals(cmd)) {
      BuCommonInterface app=
        (BuCommonInterface)Hydraulique1dBaseApplication.FRAME;
      BDialogContent dial=
        new BDialogContent(
          app,
          this,
          getS("PERSONNALISER LE GRAPHE SURFACE"),
          graphe_.getPersonnaliseurGraphe());
      dial.setNavPanel(EbliPreferences.DIALOG.FERMER);
      dial.pack();
      dial.show();
    } else if ("IMPORTER_FOND".equals(cmd)) {
      graphe_.importImageFond();
    } else if ("SUPPRIMER_FOND".equals(cmd)) {
      graphe_.supprimeImageFond();
    } else {
      /*  setValeurs();
        graphe_.refresh(pnTableauPts);
        graphe_.repaint();*/
        super.actionPerformed(_evt);
    }
  }
  @Override
  protected boolean getValeurs() {
    boolean changed= false;
    try {
      double pas= ((Double)tfPasPlanim_.getValue()).doubleValue();
      if (pas != param_.getPasPlanimetrage()) {
        param_.setPasPlanimetrage(pas);
        changed= true;
      }
      boolean option=
        ((String)cmbOptionCalcul_.getSelectedItem()).equals(OPTIONS_CALCUL[1]);
      if (option != param_.isSurfaceDependCote()) {
        param_.setSurfaceDependCote(option);
        changed= true;
      }
      boolean changeTabFront= pnTableauPtsFrontieres_.getValeurs();
      if (changeTabFront) {
        changed= true;
      }
      boolean changeTabInterieur= pnTableauPtsInterieurs_.getValeurs();
      if (changeTabInterieur) {
        changed= true;
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }
    return changed;
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
    if (_n instanceof MetierNuagePointsCasier) {
      MetierNuagePointsCasier param= (MetierNuagePointsCasier)_n;
      param_= param;
      pnTableauPtsFrontieres_.setModel(param_);
      pnTableauPtsInterieurs_.setModel(param_);
      setValeurs();

    }
  }
  @Override
  public int print(Graphics _g, PageFormat _format, int _page) {
    return graphe_.print(_g, _format, _page);
  }
  public String[] getEnabledActions() {
    String[] r= new String[] { "IMPRIMER", "PREVISUALISER", "MISEENPAGE" };
    return r;
  }
  @Override
  protected void setValeurs() {
    pnTableauPtsFrontieres_.setValeurs();
    pnTableauPtsInterieurs_.setValeurs();

    tfPasPlanim_.setValue(new Double(param_.getPasPlanimetrage()));
    if (param_.isSurfaceDependCote()) {
      cmbOptionCalcul_.setSelectedItem(OPTIONS_CALCUL[1]);
    } else {
      cmbOptionCalcul_.setSelectedItem(OPTIONS_CALCUL[0]);
    }
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void cut() {
    if (pnTableauPtsFrontieres_.hasFocus()) {
      pnTableauPtsFrontieres_.cut();
    }
    if (pnTableauPtsInterieurs_.hasFocus()) {
      pnTableauPtsInterieurs_.cut();
    }
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void copy() {
    if (pnTableauPtsFrontieres_.hasFocus()) {
      pnTableauPtsFrontieres_.copy();
    }
    if (pnTableauPtsInterieurs_.hasFocus()) {
      pnTableauPtsInterieurs_.copy();
    }
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void paste() {
    if (pnTableauPtsFrontieres_.hasFocus()) {
      pnTableauPtsFrontieres_.paste();
    }
    if (pnTableauPtsInterieurs_.hasFocus()) {
      pnTableauPtsInterieurs_.paste();
    }
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void duplicate() {
    if (pnTableauPtsFrontieres_.hasFocus()) {
      pnTableauPtsFrontieres_.duplicate();
    }
    if (pnTableauPtsInterieurs_.hasFocus()) {
      pnTableauPtsInterieurs_.duplicate();
    }
  }
  private BuPanel createPanelCalculPlanimetrage() {
    BuPanel pnCalculPlanimetrage= new BuPanel();
    BuPanel pnParametresPlanimetrage= new BuPanel();
    Border bdTitle=
      BorderFactory.createTitledBorder(
        BorderFactory.createEtchedBorder(),
        getS("Calcul du planim�trage"));
    Border bdVide= BorderFactory.createEmptyBorder(10, 10, 10, 10);
    pnCalculPlanimetrage.setBorder(
      BorderFactory.createCompoundBorder(bdTitle, bdVide));
    cmbOptionCalcul_= new BuComboBox(OPTIONS_CALCUL);
    cmbOptionCalcul_.setActionCommand("OPTIONS_CALCUL");
    cmbOptionCalcul_.setSelectedItem(OPTIONS_CALCUL[0]);
    tfPasPlanim_= BuTextField.createDoubleField();
    lbparamsAvances_= new BuMultiLabel(getS("La cote des points fronti�res est utilis�e pour")+"\n"+getS("calculer le nombre de pas de planim�trage"));
    Font font = new Font("Arial",Font.ITALIC,12);
    lbparamsAvances_.setFont(font);
    Icon icon=	BuResource.BU.getIcon("astuce_22.gif");
    lbparamsAvances_.setIcon(icon);
    pnParametresPlanimetrage.setLayout(new GridBagLayout());
    pnCalculPlanimetrage.setLayout(new BuBorderLayout(5,5));
    Insets inset3_5= new Insets(3, 5, 0, 0);
    // label et combo box option de calcul
    BuLabel lbOptionCalcul= new BuLabel(getS("option de calcul"));
    pnParametresPlanimetrage.add(
      lbOptionCalcul,
      new GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    pnParametresPlanimetrage.add(
      cmbOptionCalcul_,
      new GridBagConstraints(
        1,
        0,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    // label et champ de saisie pas de planim�trage
    BuLabel lbPasPlanim= new BuLabel(getS("pas de planim�trage"));
    pnParametresPlanimetrage.add(
      lbPasPlanim,
      new GridBagConstraints(
        0,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    pnParametresPlanimetrage.add(
      tfPasPlanim_,
      new GridBagConstraints(
        1,
        1,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    pnCalculPlanimetrage.add(pnParametresPlanimetrage,BuBorderLayout.CENTER);
    pnCalculPlanimetrage.add(lbparamsAvances_,BuBorderLayout.SOUTH);


    return pnCalculPlanimetrage;
  }
}
