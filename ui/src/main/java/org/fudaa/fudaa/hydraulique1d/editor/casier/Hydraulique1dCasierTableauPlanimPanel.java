/*
 * @file         Hydraulique1dCasierTableauPlanimPanel.java
 * @creation     2003-06-10
 * @modification $Date: 2007-11-20 11:43:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelListener;

import org.fudaa.dodico.hydraulique1d.metier.casier.MetierPlanimetrageCasier;
import org.fudaa.fudaa.hydraulique1d.editor.casier.tableau.Hydraulique1dCasierTabPlanim;
import org.fudaa.fudaa.hydraulique1d.editor.casier.tableau.Hydraulique1dCasierTabPlanimModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Panneau contenant le tableau de la planimétrie d'un casier.
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:43:29 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierTableauPlanimPanel
  extends BuPanel
  implements ActionListener, BuCutCopyPasteInterface {
  private BuTextField tfCoteFond_, tfPasPlanim_;
  private BuButton btNouvelleLigne_, btSuppression_;
  private Hydraulique1dCasierTabPlanim table_;
  private MetierPlanimetrageCasier model_;
  public Hydraulique1dCasierTableauPlanimPanel() {
    this(null);
  }
  public Hydraulique1dCasierTableauPlanimPanel(MetierPlanimetrageCasier model) {
    super();
    setLayout(new BuBorderLayout(10, 10));
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    table_=
      new Hydraulique1dCasierTabPlanim(
        new Hydraulique1dCasierTabPlanimModel(model));
    // *********** construction du panel Nord   ********************************
    Insets inset3_5= new Insets(3, 5, 0, 0);
    BuPanel pnNord= new BuPanel();
    pnNord.setLayout(new GridBagLayout());
    // label et champ de saisie "cote fond (m)"
    BuLabel lbCote= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("cote fond")+" (m)");
    pnNord.add(
      lbCote,
      new GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    tfCoteFond_= BuTextField.createDoubleField();
    tfCoteFond_.setColumns(10);
    tfCoteFond_.setActionCommand("CoteFond");
    tfCoteFond_.addActionListener(table_);
    pnNord.add(
      tfCoteFond_,
      new GridBagConstraints(
        1,
        0,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    // label et champ de saisie "pas de planimétrage (m)"
    BuLabel lbPasPlanim= new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("pas de planimétrage")+" (m)");
    pnNord.add(
      lbPasPlanim,
      new GridBagConstraints(
        0,
        1,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.NONE,
        inset3_5,
        10,
        0));
    tfPasPlanim_= BuTextField.createDoubleField();
    tfPasPlanim_.setColumns(10);
    tfPasPlanim_.setActionCommand("PasPlanim");
    tfPasPlanim_.addActionListener(table_);
    pnNord.add(
      tfPasPlanim_,
      new GridBagConstraints(
        1,
        1,
        1,
        1,
        1.0,
        0.0,
        GridBagConstraints.WEST,
        GridBagConstraints.HORIZONTAL,
        inset3_5,
        10,
        0));
    // *********** FIN construction du panel Nord   ****************************
    // *********** construction du panel Centre  *******************************
    JScrollPane sp= new JScrollPane(table_);
    // *********** FIN construction du panel Centre ****************************
    // *********** construction du panel Sud   *********************************
    BuPanel pnSud= new BuPanel();
    pnSud.setLayout(new BuBorderLayout());
    BuPanel pnBoutons= new BuPanel();
    btNouvelleLigne_= new BuButton();
    btNouvelleLigne_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Créer"));
    btNouvelleLigne_.setIcon(BuResource.BU.getIcon("creer"));
    btNouvelleLigne_.setName("btNouvelleLigne");
    btNouvelleLigne_.setActionCommand("CREER");
    btNouvelleLigne_.addActionListener(this);
    pnBoutons.add(btNouvelleLigne_);
    btSuppression_= new BuButton();
    btSuppression_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Supprimer"));
    btSuppression_.setIcon(BuResource.BU.getIcon("detruire"));
    btSuppression_.setName("btSUPPRIMER");
    btSuppression_.setActionCommand("SUPPRIMER");
    btSuppression_.addActionListener(this);
    pnBoutons.add(btSuppression_);
    pnSud.add(pnBoutons, BuBorderLayout.NORTH);
    // *********** FIN construction du panel Sud   ****************************
    add(pnNord, BuBorderLayout.NORTH);
    add(sp, BuBorderLayout.CENTER);
    add(pnSud, BuBorderLayout.SOUTH);
    setModel(model);
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equals("CREER")) {
      table_.ajouterLigne();
    } else if (cmd.equals("SUPPRIMER")) {
      table_.supprimeLignesSelectionnees();
    }
  }
  public void addTableModelListener(TableModelListener listener) {
    table_.getModel().addTableModelListener(listener);
  }
  boolean getValeurs() {
    boolean changed= false;
    try {
      double cote= ((Double)tfCoteFond_.getValue()).doubleValue();
      if (cote != model_.coteInitiale()) {
        model_.coteInitiale(cote);
        changed= true;
      }
      double pasPlanim= ((Double)tfPasPlanim_.getValue()).doubleValue();
      if (pasPlanim != model_.getPasPlanimetrage()) {
        model_.setPasPlanimetrage(pasPlanim);
        changed= true;
      }
      boolean changtabe= table_.getValeurs();
      if (changtabe) {
        changed= true;
      }
    } catch (NullPointerException ex) {
      ex.printStackTrace();
      changed= false;
    }
    return changed;
  }
  public void setModel(MetierPlanimetrageCasier model) {
    model_= model;
    ((Hydraulique1dCasierTabPlanimModel)table_.getModel()).setModelMetier(
      model_);
    setValeurs();
  }
  void setValeurs() {
    if (model_ == null)
      return;
    tfCoteFond_.setValue(new Double(model_.coteInitiale()));
    tfPasPlanim_.setValue(new Double(model_.getPasPlanimetrage()));
    table_.setValeurs();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void cut() {
    table_.cut();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void copy() {
    table_.copy();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void paste() {
    table_.paste();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void duplicate() {
    table_.duplicate();
  }

  public static void main(String[] arg) {
    BuPanel panel= new Hydraulique1dCasierTableauPlanimPanel();
    JFrame fenetre= new JFrame("test Hydraulique1dCasierTableauPlanimPanel");
    fenetre.getContentPane().add(panel);
    fenetre.pack();
    fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fenetre.setVisible(true);
  }
}
