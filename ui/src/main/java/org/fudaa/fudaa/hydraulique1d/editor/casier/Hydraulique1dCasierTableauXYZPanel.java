/*
 * @creation     2003-06-30
 * @modification $Date: 2007-11-20 11:43:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.fr
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.event.TableModelListener;

import org.fudaa.dodico.hydraulique1d.metier.casier.MetierNuagePointsCasier;
import org.fudaa.fudaa.hydraulique1d.editor.casier.tableau.Hydraulique1dCasierTabXYZ;
import org.fudaa.fudaa.hydraulique1d.editor.casier.tableau.Hydraulique1dCasierTabXYZModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Panneau contenant le tableau des points 3D int�rieurs ou ext�rieurs de la g�om�trie d'un casier.
 * @version      $Revision: 1.11 $ $Date: 2007-11-20 11:43:27 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierTableauXYZPanel
  extends BuPanel
  implements ActionListener, BuCutCopyPasteInterface {
  public final static int POINTS_FRONTIERES= 0;
  public final static int POINTS_INTERIEURS= 1;
  private BuButton btNouvelleLigne_, btSuppression_, btImport_;
  private Hydraulique1dCasierTabXYZ table_;
  private int mode_= POINTS_FRONTIERES;
  private MetierNuagePointsCasier model_;
  public Hydraulique1dCasierTableauXYZPanel(int mode) {
    this(null, mode);
  }
  public Hydraulique1dCasierTableauXYZPanel(
    MetierNuagePointsCasier model,
    int mode) {
    super();
    mode_= mode;
    setLayout(new BuBorderLayout(10, 10));
    String nomPanel= "";
    if (mode_ == POINTS_FRONTIERES) {
      nomPanel= Hydraulique1dResource.HYDRAULIQUE1D.getString("Points fronti�res");
    } else {
      nomPanel= Hydraulique1dResource.HYDRAULIQUE1D.getString("Points int�rieurs");
    }
    Border bdTitle=
      BorderFactory.createTitledBorder(
        BorderFactory.createEtchedBorder(),
        nomPanel);
    Border bdVide= BorderFactory.createEmptyBorder(10, 10, 10, 10);
    setBorder(BorderFactory.createCompoundBorder(bdTitle, bdVide));
    table_=
      new Hydraulique1dCasierTabXYZ(
        new Hydraulique1dCasierTabXYZModel(model, mode));
    // *********** construction du panel Nord   ********************************
    // *********** construction du panel Centre  *******************************
    JScrollPane sp= new JScrollPane(table_);
    // *********** FIN construction du panel Centre ****************************
    // *********** construction du panel Sud   *********************************
    BuPanel pnSud= new BuPanel();
    pnSud.setLayout(new BuBorderLayout());
    BuPanel pnBoutons= new BuPanel();
    btNouvelleLigne_= new BuButton();
    btNouvelleLigne_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Cr�er"));
    btNouvelleLigne_.setIcon(BuResource.BU.getIcon("creer"));
    btNouvelleLigne_.setName("btNouvelleLigne");
    btNouvelleLigne_.setActionCommand("CREER");
    btNouvelleLigne_.addActionListener(this);
    pnBoutons.add(btNouvelleLigne_);
    btSuppression_= new BuButton();
    btSuppression_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Supprimer"));
    btSuppression_.setIcon(BuResource.BU.getIcon("detruire"));
    btSuppression_.setName("btSUPPRIMER");
    btSuppression_.setActionCommand("SUPPRIMER");
    btSuppression_.addActionListener(this);
    pnBoutons.add(btSuppression_);
    btImport_= new BuButton();
    btImport_.setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Importer"));
    btImport_.setIcon(BuResource.BU.getIcon("importer"));
    btImport_.setName("btImport");
    btImport_.setActionCommand("IMPORTER");
    btImport_.addActionListener(this);
    pnBoutons.add(btImport_);
    pnSud.add(pnBoutons, BuBorderLayout.NORTH);
    // *********** FIN construction du panel Sud   ****************************
    add(sp, BuBorderLayout.CENTER);
    add(pnSud, BuBorderLayout.SOUTH);
    setModel(model);
    setPreferredSize(new Dimension(100, 200));
  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    if (cmd.equals("CREER")) {
      table_.ajouterLigne();
    } else if (cmd.equals("SUPPRIMER")) {
      table_.supprimeLignesSelectionnees();
    } else if (cmd.equals("IMPORTER")) {
      table_.importer();
    }
  }
  public void addTableModelListener(TableModelListener listener) {
    table_.getModel().addTableModelListener(listener);
  }
  boolean getValeurs() {
    boolean changed= false;
    try {
      boolean changtabe= table_.getValeurs();
      if (changtabe) {
        changed= true;
      }
    } catch (NullPointerException ex) {
      System.out.println("Valeur non initialis�e");
      ex.printStackTrace();
      changed= false;
    }
    return changed;
  }
  public void setModel(MetierNuagePointsCasier model) {
    model_= model;
    ((Hydraulique1dCasierTabXYZModel)table_.getModel()).setModelMetier(model_);
    setValeurs();
  }
  public String validationDonnees() {
    if (model_ == null)
      return null;
    return ((Hydraulique1dCasierTabXYZModel)table_.getModel())
      .validationDonnees();
  }
  public double getPlusPetitEcartCote() {
    if (model_ == null)
      return Double.NaN;
    return ((Hydraulique1dCasierTabXYZModel)table_.getModel())
      .getPlusPetitEcartCote();
  }
  void setValeurs() {
    if (model_ == null)
      return;
    table_.setValeurs();

  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void cut() {
    table_.cut();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void copy() {
    table_.copy();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void paste() {
    table_.paste();
  }
  /**
   * implementation de BuCutCopyPasteInterface
   */
  @Override
  public void duplicate() {
    table_.duplicate();
  }
  public static void main(String[] arg) {
    BuPanel panel=
      new Hydraulique1dCasierTableauXYZPanel(
        Hydraulique1dCasierTableauXYZPanel.POINTS_FRONTIERES);
    JFrame fenetre= new JFrame("test Hydraulique1dCasierTableauXYZPanel");
    fenetre.getContentPane().add(panel);
    fenetre.pack();
    fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fenetre.setVisible(true);
  }
}
