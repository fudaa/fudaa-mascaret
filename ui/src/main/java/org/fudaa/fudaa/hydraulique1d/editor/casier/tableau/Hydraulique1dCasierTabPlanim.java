/*
 * @file         Hydraulique1dCasierTabPlanim.java
 * @creation     2003-06-25
 * @modification $Date: 2006-09-12 08:36:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier.tableau;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.TableModel;

import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;

import com.memoire.bu.BuTextField;
/**
 * Composant graphique tableau (JTable) de la planimétrie d'un casier.
 * @version      $Revision: 1.7 $ $Date: 2006-09-12 08:36:41 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierTabPlanim
  extends Hydraulique1dTableauReel
  implements ActionListener {
  public Hydraulique1dCasierTabPlanim() {
    this(new Hydraulique1dCasierTabPlanimModel());
  }
  public Hydraulique1dCasierTabPlanim(Hydraulique1dCasierTabPlanimModel model) {
    super(model);

  }
  @Override
  public void actionPerformed(ActionEvent _evt) {
    String cmd= _evt.getActionCommand();
    Object source= _evt.getSource();
    if (cmd.equals("CoteFond")) {
      TableModel m= getModel();
      if (m instanceof Hydraulique1dCasierTabPlanimModel) {
        Hydraulique1dCasierTabPlanimModel modele=
          (Hydraulique1dCasierTabPlanimModel)m;
        if (source instanceof BuTextField) {
          BuTextField tf= (BuTextField)source;
          double cote= ((Double)tf.getValue()).doubleValue();
          modele.setCoteInitiale(cote);
        }
      }
    } else if (cmd.equals("PasPlanim")) {
      TableModel m= getModel();
      if (m instanceof Hydraulique1dCasierTabPlanimModel) {
        Hydraulique1dCasierTabPlanimModel modele=
          (Hydraulique1dCasierTabPlanimModel)m;
        if (source instanceof BuTextField) {
          BuTextField tf= (BuTextField)source;
          double pas= ((Double)tf.getValue()).doubleValue();
          modele.setPas(pas);
        }
      }
    }
  }
}
