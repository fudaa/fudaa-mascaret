/*
 * @file         Hydraulique1dCasierTabPlanimModel.java
 * @creation     2003-06-26
 * @modification $Date: 2007-11-20 11:42:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier.tableau;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.casier.MetierPlanimetrageCasier;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint2D;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneReelTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReelModel;
/**
 * Mod�le du composant graphique tableau (JTable) de la planim�trie d'un casier.
 * @version      $Revision: 1.10 $ $Date: 2007-11-20 11:42:35 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierTabPlanimModel extends Hydraulique1dTableauReelModel {
  private final static String[] COLUMNS_NAMES= { getS("Cote"), getS("Surface"), getS("Volume") };
  private MetierPlanimetrageCasier modelMetier_;
  private double coteInitiale_= 0;
  private double pas_= 1;
  public Hydraulique1dCasierTabPlanimModel() {
    this(null);
  }
  public Hydraulique1dCasierTabPlanimModel(MetierPlanimetrageCasier modelMetier) {
    super(COLUMNS_NAMES,20);
    setModelMetier(modelMetier);
  }
  @Override
  public boolean isCellEditable(int row, int col) {
    if (col == 0)
      return false;
    if ((col == 2) && (row == 0))
      return false;
    return true;
  }
  public void setModelMetier(MetierPlanimetrageCasier iplanim) {
    modelMetier_= iplanim;
    setValeurs();
  }
  @Override
  public Object getValueAt(int row, int col) {
    if (col == 0) {
      return new Double(coteInitiale_ + (row * pas_));
    }
    SurfVol pt= (SurfVol)listePts_.get(row);
    if (col == 1) {
      return pt.surf();
    }
    return pt.vol();
  }
  /**
   * Modifie la valeur d'une cellule du tableau.
   * @param value La nouvelle valeur (Double ou null).
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   */
  @Override
  public void setValueAt(Object value, int row, int col) {
    Double valeur= (Double)value;
    SurfVol lig= (SurfVol)listePts_.get(row);
    if (col == 1) {
      lig.surf(valeur);
    }
    else if (col == 2) {
      lig.vol(valeur);
    }
    fireTableDataChanged();
  }
  public double[][] getTabDoubleSurf() {
    List listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      SurfVol pt= (SurfVol)ite.next();
      if (pt.surf() != null) {
        listeTmp.add(pt);
      }
    }
    int taille= listeTmp.size();
    double[][] res= new double[2][taille];
    for (int i= 0; i < taille; i++) {
      res[0][i]= coteInitiale_ + (i * pas_);
      res[1][i]= ((SurfVol)listeTmp.get(i)).s();
    }
    return res;
  }
  public double[][] getTabDoubleVol() {
    List listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      SurfVol pt= (SurfVol)ite.next();
      if (pt.vol() != null) {
        listeTmp.add(pt);
      }
    }
    int taille= listeTmp.size();
    double[][] res= new double[2][taille];
    for (int i= 0; i < taille; i++) {
      res[0][i]= coteInitiale_ + (i * pas_);
      res[1][i]= ((SurfVol)listeTmp.get(i)).v();
    }
    return res;
  }
  void setCoteInitiale(double cote) {
    if (coteInitiale_ != cote) {
      coteInitiale_ = cote;
      fireTableDataChanged();
    }
  }
  void setPas(double pas) {
    if (pas_!= pas) {
      pas_ = pas;
      fireTableDataChanged();
    }
  }

/**
 * R�cup�re les donn�es de l'objet m�tier et les tranferts vers le mod�le de tableau.
 */
  @Override
  public void setValeurs() {
    MetierPoint2D[] points= new MetierPoint2D[0];
    if (modelMetier_ != null) {
      points= modelMetier_.getPointsPlanimetrage();
      pas_= modelMetier_.getPasPlanimetrage();
      coteInitiale_= modelMetier_.coteInitiale();
    }
    listePts_= new ArrayList();
    for (int i= 0; i < points.length; i++) {
      SurfVol pt= new SurfVol(points[i].x, points[i].y);
      listePts_.add(pt);
    }
    for (int i= 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(new SurfVol());
    }
    ((SurfVol)listePts_.get(0)).vol(new Double(0));
    fireTableDataChanged();
  }

  /**
   * Transferts les donn�es du tableau vers l'objet m�tier.
   * @return vrai s'il existe des diff�rences, faux sinon.
   */
  @Override
  public boolean getValeurs() {
    if (modelMetier_ != null) {
      List listeTmp = getListePtsComplets();
      MetierPoint2D[] points = modelMetier_.getPointsPlanimetrage();
      boolean existeDifference = false;
      if (listeTmp.size() != points.length) {
        existeDifference = true;
      }
      MetierPoint2D[] pointsTmp = new MetierPoint2D[listeTmp.size()];
      int tailleMin = Math.min(listeTmp.size(), points.length);
      for (int i = 0; i < tailleMin; i++) {
        SurfVol pt = (SurfVol) listeTmp.get(i);
        pointsTmp[i] = points[i];
        if (!pt.equals(pointsTmp[i])) {
          existeDifference = true;
          pointsTmp[i] = new MetierPoint2D(pt.s(),pt.v());
        }
      }
      if (listeTmp.size() > points.length) {
        existeDifference = true;
        MetierPoint2D[] pt2D = new MetierPoint2D[listeTmp.size() -
            points.length];
        int iPt2D = 0;
        for (int i = tailleMin; i < pointsTmp.length; i++) {
          SurfVol pt = (SurfVol) listeTmp.get(i);
          pointsTmp[i] = pt2D[iPt2D];
          if (pointsTmp[i] == null) {
            pointsTmp[i] = new MetierPoint2D(pt.s(),pt.v());
          }
          iPt2D++;
        }
      }
      else if (listeTmp.size() < points.length) {
        existeDifference = true;
      }
      if (existeDifference) {
        modelMetier_.setPointsPlanimetrage(pointsTmp);
      }
      if (coteInitiale_ != modelMetier_.coteInitiale()) {
        modelMetier_.coteInitiale(coteInitiale_);
        existeDifference = true;
      }
      if (pas_ != modelMetier_.getPasPlanimetrage()) {
        modelMetier_.setPasPlanimetrage(pas_);
        existeDifference = true;
      }
      return existeDifference;
    }
    return true;
  }

  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de SurfVol.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new SurfVol();
  }
  /**
   * Ajoute une ligne vide la fin du tableau.
   */
  @Override
  public void ajouterLigne() {
    listePts_.add(new SurfVol());
    fireTableDataChanged();
  }

  /**
   * Ajoute une ligne vide dans le tableau.
   * @param row Indice de la ligne � ajouter.
   */
  @Override
  public void ajouterLigne(int row) {
    listePts_.add(row,new SurfVol());
    fireTableDataChanged();
  }
}
class SurfVol extends Hydraulique1dLigneReelTableau{
  SurfVol() {
    super(2);
  }
  SurfVol(double s, double v) {
    super(2);
    super.setValue(0, s);
    super.setValue(1, v);
  }
  Double surf() {
    return super.getValue(0);
  }
  void surf(Double surf) {
    super.setValue(0, surf);
  }
  void vol(Double vol) {
    super.setValue(1, vol);
  }
  Double vol() {
    return super.getValue(1);
  }
  double s() {
    return super.value(0);
  }
  double v() {
    return super.value(1);
  }
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof SurfVol) {
      SurfVol l =(SurfVol)obj;
      return (l.v()==v())&&(l.s()==s());
    }
    else if (obj instanceof MetierPoint2D){
      MetierPoint2D l = (MetierPoint2D) obj;
      return (l.x == s()) && (l.y == v());
    }
    return false;
  }
}
