/*
 * @file         Hydraulique1dCasierTabXYZ.java
 * @creation     2003-06-30
 * @modification $Date: 2007-11-20 11:42:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier.tableau;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.table.TableModel;

import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel;

import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuFileFilter;
/**
 * Composant graphique tableau (JTable) des points intérieurs ou frontières d'un casier.
 * @version      $Revision: 1.9 $ $Date: 2007-11-20 11:42:35 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierTabXYZ extends Hydraulique1dTableauReel {
  public Hydraulique1dCasierTabXYZ() {
    this(new Hydraulique1dCasierTabXYZModel());
  }
  public Hydraulique1dCasierTabXYZ(Hydraulique1dCasierTabXYZModel model) {
    super(model);
  }

  public void importer() {
    BuFileChooser chooser= new BuFileChooser();
    chooser.addChoosableFileFilter(
      new BuFileFilter("sx", getS("Fichier au format sinusX XYZ")));
    chooser.addChoosableFileFilter(
      new BuFileFilter("txt", getS("Fichier texte 3 colonnes XYZ")));
    chooser.addChoosableFileFilter(
      new BuFileFilter("csv", getS("Fichier csv 3 colonnes XYZ")));
    chooser.setCurrentDirectory(Hydraulique1dResource.lastImportDir);
    int returnVal= chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File fichier= chooser.getSelectedFile();
      try {
        MetierPoint[] tabPts= Hydraulique1dImport.pointsXYZ(fichier);
        if (tabPts != null) {
          TableModel m= getModel();
          if (m instanceof Hydraulique1dCasierTabXYZModel) {
            Hydraulique1dCasierTabXYZModel modele=
              (Hydraulique1dCasierTabXYZModel)m;
            modele.setPoints(tabPts);
          }
        }
      } catch (Exception exception) {
        System.err.println("$$$ " + exception);
        exception.printStackTrace();
      }
      Hydraulique1dResource.lastImportDir= chooser.getCurrentDirectory();
    }
  }
}
