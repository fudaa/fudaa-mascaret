/*
 * @file         Hydraulique1dCasierTabXYZModel.java
 * @creation     2003-06-30
 * @modification $Date: 2007-11-20 11:42:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.casier.tableau;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.casier.MetierNuagePointsCasier;
import org.fudaa.dodico.hydraulique1d.metier.geometrie.MetierPoint;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneReelTableau;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReelModel;
/**
 * Mod�le du composant graphique tableau (JTable) des points int�rieurs ou fronti�res d'un casier.
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:42:35 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dCasierTabXYZModel extends Hydraulique1dTableauReelModel {
  public final static int POINTS_FRONTIERES= 0;
  public final static int POINTS_INTERIEURS= 1;
  private MetierNuagePointsCasier modelMetier_;
  private int mode_= POINTS_FRONTIERES;
  private String message= "";
  public Hydraulique1dCasierTabXYZModel() {
    this(null, POINTS_FRONTIERES);
  }
  public Hydraulique1dCasierTabXYZModel(
    MetierNuagePointsCasier modelMetier,
    int mode) {
    super();
    mode_= mode;
    setModelMetier(modelMetier);
  }

  public void setModelMetier(MetierNuagePointsCasier modele) {
    modelMetier_= modele;
    setValeurs();
  }

  @Override
  public void setValeurs() {
    MetierPoint[] points= new MetierPoint[0];
    if (modelMetier_ != null) {
      if (mode_ == POINTS_FRONTIERES) {
        points= modelMetier_.pointsFrontiere();
      } else {
        points= modelMetier_.pointsInterieur();
      }
    }
    listePts_= new ArrayList();
    for (int i= 0; i < points.length; i++) {
      Hydraulique1dLigneReelTableau lig= new Hydraulique1dLigneReelTableau(points[i].x, points[i].y, points[i].z);
      listePts_.add(lig);
    }
    for (int i= 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }
  public void setPoints(MetierPoint[] points) {
    if (mode_ == POINTS_FRONTIERES) {
      modelMetier_.pointsFrontiere(points);
    } else {
      modelMetier_.pointsInterieur(points);
    }
    setValeurs();
  }
  public boolean isPointsFrontieres() {
    return (mode_ == POINTS_FRONTIERES);
  }
  public boolean isPointsInterieurs() {
    return (mode_ == POINTS_INTERIEURS);
  }
  @Override
  public boolean getValeurs() {
    List listeTmp= getListePtsComplets();
    MetierPoint[] pts= new MetierPoint[listeTmp.size()];
    for (int i= 0; i < pts.length; i++) {
      Hydraulique1dLigneReelTableau pt= (Hydraulique1dLigneReelTableau)listeTmp.get(i);
      pts[i]=
        new MetierPoint(pt.x(), pt.y(), pt.z());
    }
    if (modelMetier_ != null) {
      if (mode_ == POINTS_FRONTIERES) {
        modelMetier_.pointsFrontiere(pts);
      } else {
        modelMetier_.pointsInterieur(pts);
      }
    }
    return true;
  }
  public String validationDonnees() {
    List listePts= getListePtsComplets();
    String res= "";
    if (mode_ == POINTS_FRONTIERES) {
      if (listePts.size() < 3) {
        res += getS("Le nombre de points fronti�re doit �tre sup�rieur � 3");
      } else {
        if (pointsAlignes(listePts)) {
          res
            += getS("Des points fronti�res suivant sont align�s dans la plan")+" (XY) :";
          res += "\t" + message;
        }
      }
    } else if (mode_ == POINTS_INTERIEURS) {
      if (listePts.size() < 1) {
        res += getS("Le nombre de points int�rieur doit �tre sup�rieur � 1");
      }
    }
    return res;
  }
  public double getPlusPetitEcartCote() {
    double plusPetitEcart= Double.MAX_VALUE;
    List listePts= getListePtsComplets();
    if (listePts.size() > 2) {
      Iterator ite= listePts.iterator();
      double zPrec= ((Hydraulique1dLigneReelTableau)ite.next()).z();
      while (ite.hasNext()) {
        double z= ((Hydraulique1dLigneReelTableau)ite.next()).z();
        double ecart= Math.abs(z - zPrec);
        plusPetitEcart= Math.min(plusPetitEcart, ecart);
        zPrec= z;
      }
    }
    return plusPetitEcart;
  }
  private boolean pointsAlignes(List listePts) {
    boolean res= false;
    if (listePts.size() >= 3) {
      ArrayList listeDroites= new ArrayList();
      for (int i= 0; i < (listePts.size() - 1); i++) {
        Hydraulique1dLigneReelTableau pti= (Hydraulique1dLigneReelTableau)listePts.get(i);
        Hydraulique1dLigneReelTableau ptiPlus1= (Hydraulique1dLigneReelTableau)listePts.get(i + 1);
        Droite d= new Droite();
        d.pt1= new XY(pti);
        d.pt2= new XY(ptiPlus1);
        listeDroites.add(d);
      }
      for (int i= 0; i < listeDroites.size(); i++) {
        Droite di= (Droite)listeDroites.get(i);
        for (int j= (i + 1); j < listeDroites.size(); j++) {
          Droite dj= (Droite)listeDroites.get(j);
          if (di.alignee(dj)) {
            message= di.toString() + " et " + dj.toString();
            return true;
          }
        }
      }
    }
    return res;
  }
/*  private static final double determinant(Hydraulique1dLigneReelTableau v1, Hydraulique1dLigneReelTableau v2) {
    return (v1.x() * v2.y()) - (v2.x() * v1.y());
  }*/
}
class Droite {
  XY pt1;
  XY pt2;
  //coefficient de la droite y = ax+b
  private double a= Double.NaN;
  private double b= Double.NaN;
  public boolean alignee(Droite d2) {
    boolean vert= vertical();
    boolean d2Vert= d2.vertical();
    if ((vert && !d2Vert) || (!vert && d2Vert))
      return false;
    if (parallele(d2)) {
      return existeIntersection(d2);
    }
    return false;
  }
  /**
   * @return true s'il existe une intersection entre 2 droites parall�les this et d2
   */
  private boolean existeIntersection(Droite d2) {
    boolean vert= vertical();
    boolean d2Vert= d2.vertical();
    if (vert && d2Vert) { // 2 droites verticales
      return  (pt1.x == d2.pt1.x);
    } else if (
      (vert && !d2Vert) || (!vert && d2Vert)) { // 1 seule droite verticale
      return true;
    } else {
      return (d2.pt1.y == calculOrdonnee(d2.pt1.x));
    }
  }
  public double calculOrdonnee(double x) {
    calculCoefs();
    return (a * x) + b;
  }
  private void calculCoefs() {
    a= (pt1.y - pt2.y) / (pt1.x - pt2.x);
    b= pt1.y - (a * pt1.x);
  }
  public boolean parallele(Droite d2) {
    XY vec= vecteur();
    XY vec2= d2.vecteur();
    return (XY.determinant(vec, vec2) == 0);
  }
  public XY vecteur() {
    XY vec= new XY();
    vec.x= pt2.x - pt1.x;
    vec.y= pt2.y - pt1.y;
    return vec;
  }
  public boolean vertical() {
    XY vec= vecteur();
    if (vec.x == 0.)
      return true;
    return false;
  }
  @Override
  public String toString() {
    return pt1.toString() + "-" + pt2.toString();
  }
}
class XY {
  double x= Double.NaN;
  double y= Double.NaN;
  XY() {}
  XY(Hydraulique1dLigneReelTableau xyz) {
    x= xyz.X().doubleValue();
    y= xyz.Y().doubleValue();
  }
  XY(double _x, double _y) {
    x= _x;
    y= _y;
  }
  @Override
  public String toString() {
    return "(" + x + "," + y + ")";
  }
  public static final double determinant(XY a, XY b) {
    return (a.x * b.y) - (b.x * a.y);
  }

}
