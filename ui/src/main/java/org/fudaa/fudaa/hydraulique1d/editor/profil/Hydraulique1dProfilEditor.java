/*
 * @file         Hydraulique1dProfilEditor.java
 * @creation     1999-05-27
 * @modification $Date: 2007-11-20 11:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.profil;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.util.Map;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.ebli.dialog.BPanneauNavigation;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dPreferences;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizerImprimable;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuSelectFindReplaceInterface;
import com.memoire.bu.BuUndoRedoInterface;

/**
 * Editeur d'un profil quelconque (DProfil).<br>
 * Appeler si l'utilisateur clic sur le bouton "Editer" de l'�diteur de bief.<br>
 * Lancer � partir de l'�diteur de bief avec l'IHM helper Hydraulique1dIHM_Profil
 * et � partir des graphiques r�sultats avec Hydraulique1dIHMRepository.getInstance().PROFIL_RESULTATS().<br>
 * Diff�rents modes d'utilisation :
 * <ul>
 *   <li> mode 1 : EDITER ou VOIR
 *   <li> mode 2 : PROFILS ou CALAGE
 * </ul>
 * @version      $Revision: 1.22 $ $Date: 2007-11-20 11:43:26 $ by $Author: bmarchan $
 * @author       Axel von Arnim / Bertrand Marchand
 */
public class Hydraulique1dProfilEditor extends Hydraulique1dCustomizerImprimable
    implements BuCutCopyPasteInterface, BuUndoRedoInterface, BuSelectFindReplaceInterface {

  /** Mode d'affichage */
  public static final int EDITER=0x01;
  public static final int VOIR=0x02;

  /** Mode d'affichage de la partie edition */
  public static final int PROFILS=Hydraulique1dResource.PROFIL.PROFILS;
  public static final int CALAGE=Hydraulique1dResource.PROFIL.CALAGE;

  /** Pour avancer/reculer dans les profils */
  private static final int PROFIL_RAPIDE=Hydraulique1dPreferences.HYDRAULIQUE1D.getIntegerProperty("profil.rapide", 5);
  /** Le panneau consacr� au graphe */
  Hydraulique1dProfilPane pnProfs_;
  BuButton btAvancer_;
  BuButton btAvancerVite_;
  BuButton btReculer_;
  BuButton btReculerVite_;
  
  /**
   * Contructeur minimal
   * @param modeEdition_ EDITER (edition de profils) ou VOIR (r�sultats)
   * @param mode2 PROFILS ou CALAGE
   */
  public Hydraulique1dProfilEditor(int _modeEdition, int _mode2) {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Edition de profil"));
    
    pnProfs_=new Hydraulique1dProfilPane(_modeEdition, _mode2);
    pnProfs_.setProfilsModeleBief(new Hydraulique1dProfilModel[]{new Hydraulique1dProfilModel()});
    pnProfs_.updateState();

    getContentPane().add(pnProfs_,BorderLayout.CENTER);
    
    setActionPanel(null);
    if (_modeEdition==EDITER) {
      setNavPanel(BPanneauNavigation.RECULER_VITE|BPanneauNavigation.RECULER|BPanneauNavigation.AVANCER|
          BPanneauNavigation.AVANCER_VITE|BPanneauNavigation.VALIDER|BPanneauNavigation.ALIGN_RIGHT);
    }
    if (_modeEdition==VOIR) {
      setNavPanel(BPanneauNavigation.RECULER_VITE|BPanneauNavigation.RECULER|BPanneauNavigation.AVANCER|
          BPanneauNavigation.AVANCER_VITE|BPanneauNavigation.FERMER|BPanneauNavigation.ALIGN_RIGHT);
    }
    
    btAvancer_ = getNavPanel().getBtNavAvancer();
    btAvancerVite_ = getNavPanel().getBtNavAvancerVite();
    btReculer_ = getNavPanel().getBtNavReculer();
    btReculerVite_ = getNavPanel().getBtNavReculerVite();
    
    updateButtons();
    pack();
  }
  
  public void updateButtons() {
    btAvancer_.setEnabled(pnProfs_.getProfilCourant()!=pnProfs_.getProfilsModeleBief().length-1);
    btAvancerVite_.setEnabled(pnProfs_.getProfilCourant()!=pnProfs_.getProfilsModeleBief().length-1);
    btReculer_.setEnabled(pnProfs_.getProfilCourant()!=0);
    btReculerVite_.setEnabled(pnProfs_.getProfilCourant()!=0);
  }
  
  public Hydraulique1dProfilPane getPane() {
    return pnProfs_;
  }
  
  public void setEtude(MetierEtude1d _res) {
    pnProfs_.setEtude(_res);
    updateButtons();
  }

  /**
   * Affecte le tableau des profils.
   * @param _profils Les profils.
   */
  public void setProfilsModeleBief(Hydraulique1dProfilModel[] _profils) {
    pnProfs_.setProfilsModeleBief(_profils);
    updateButtons();
  }

  /**
   * Affecte le profil courant.
   * @param _profil Le profil courant.
   */
  public void setProfilModel(Hydraulique1dProfilModel _profil) {
    pnProfs_.setProfilModel(_profil);
    updateButtons();
  }

  /**
   * Definit si l'�dition se fait en mode transcritique
   * @param b true : Mode transcritique.
   */
  public void setIsTranscritique(boolean b) {
    pnProfs_.setIsTranscritique(b);
  }

  /** Inutile */
  @Override
  public void setObject(MetierHydraulique1d _n) {}

  /** Inutile */
  @Override
  protected void setValeurs() {}

  /** Inutile */
  @Override
  protected boolean getValeurs() { return true; }

  //--- Interface BuBuUndoRedoInterface  ---------------------------------------

  /**
   * Annule la derniere op�ration de modification.
   */
  @Override
  public void undo() {
    pnProfs_.undo();
  }

  /**
   * Refait la derni�re op�ration de modification annul�e.
   */
  @Override
  public void redo() {
    pnProfs_.redo();
  }

  //--- Interface BuSelectFindReplaceInterface  ---------------------------------------

  /**
   * Select All.
   */
  @Override
  public void select() {
    pnProfs_.selectAllPoints();
  }

  /** Rien */
  @Override
  public void find() {}

  /** Rien */
  @Override
  public void replace() {}

  //--- Interface BuCutCopyPasteInterface  ---------------------------------------

  /**
   * Cut
   */
  @Override
  public void cut() {
    pnProfs_.cut();
  }

  /**
   * Copy
   */
  @Override
  public void copy() {
    pnProfs_.copy();
  }

  /**
   * Paste
   */
  @Override
  public void paste() {
    pnProfs_.paste();
  }

  @Override
  public void duplicate() {
    pnProfs_.duplicate();
  }

  /**
   * Gestion des actions sur les boutons.
   * @param e ActionEvent
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    String cmd=e.getActionCommand();
    if (cmd==null) {
      cmd="";
    }
    if ("FERMER".equals(cmd)) {
      super.fermer();
    }
    else if ("VALIDER".equals(cmd)) {
      // verifier les contraintes: -les deux rives ont ete selectionnees
      //                           -pas de pente verticale
      //                           -...
      boolean ok=pnProfs_.verifieContraintes();
      if (ok) {
        // Suppression des points nuls des profils.
        Hydraulique1dProfilModel[] profs=pnProfs_.getProfilsModeleBief();
        for (int i=0; i<profs.length; i++) profs[i].supprimePointsNuls();
        super.fermer();
      }
    }
    else if ("RECULERVITE".equals(cmd)) {
      pnProfs_.setProfilCourant(pnProfs_.getProfilCourant()-PROFIL_RAPIDE);
    }
    else if ("RECULER".equals(cmd)) {
      pnProfs_.setProfilCourant(pnProfs_.getProfilCourant()-1);
    }
    else if ("AVANCER".equals(cmd)) {
      pnProfs_.setProfilCourant(pnProfs_.getProfilCourant()+1);
    }
    else if ("AVANCERVITE".equals(cmd)) {
      pnProfs_.setProfilCourant(pnProfs_.getProfilCourant()+PROFIL_RAPIDE);
    }
    
    updateButtons();
  }

  /**
   * @return l'image produite dans la taille courante du composant.
   */
  @Override
  public BufferedImage produceImage(Map _params) {
    return pnProfs_.produceImage(_params);
  }

  /**
   * Le nombre de pages : 2. La premi�re pour le graphique, la deuxi�me pour le tableau.
   * @return Le nombre de pages.
   */
  @Override
  public int getNumberOfPages() {
    return pnProfs_.getNumberOfPages();
  }

  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   *
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe,
   *         sinon <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _page) {
    return pnProfs_.print(_g, _format, _page);
  }
}
