/*
 * @file         Hydraulique1dProfilFormeAucun.java
 * @creation     1999-12-30
 * @modification $Date: 2006-09-12 08:36:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.profil;
import java.beans.PropertyChangeListener;

import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Forme aucun d'un profil de forme simple.
 * @version      $Revision: 1.7 $ $Date: 2006-09-12 08:36:42 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public class Hydraulique1dProfilFormeAucun
  implements Hydraulique1dProfilFormeSimple {
  GrPoint startPoint_;
  public Hydraulique1dProfilFormeAucun() {
    startPoint_= new GrPoint(0, 0, 0);
  }
  @Override
  public void setConnectMode(int mode) {}
  @Override
  public BDialog getEditor() {
    return null;
  }
  @Override
  public GrPoint[] getPoints() {
    return new GrPoint[0];
  }
  @Override
  public void setStartPoint(GrPoint p) {
    startPoint_= p;
  }
  @Override
  public GrPoint getEndPoint() {
    return startPoint_;
  }
  @Override
  public String getName() {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString("Aucun");
  }
  @Override
  public boolean isEditable() {
    return false;
  }
  @Override
  public void addPropertyChangeListener(PropertyChangeListener l) {}
  @Override
  public void removePropertyChangeListener(PropertyChangeListener l) {}
  @Override
  public void setParent(IDialogInterface d) {}
}
