/*
 * @file         Hydraulique1dProfilFormeRectangle.java
 * @creation     1999-12-28
 * @modification $Date: 2007-02-21 16:33:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.profil;

import java.beans.*;
import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import org.fudaa.ebli.dialog.*;
import org.fudaa.ebli.geometrie.*;
import org.fudaa.fudaa.hydraulique1d.*;
import com.memoire.bu.*;

/**
 * Profil simple de forme buse arche.
 * @version      $Revision: 1.2 $ $Date: 2007-02-21 16:33:51 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class Hydraulique1dProfilFormeBuseArche implements Hydraulique1dProfilFormeSimple {

  PropertyChangeSupport prop_=new PropertyChangeSupport(this);
  IDialogInterface parent_;

  /** L'�diteur li� a cette forme */
  Hydraulique1dProfilFormeBuseArcheEditor editor_=null;
  /** Le point de d�part de d�finition du profil */
  GrPoint startPoint_=new GrPoint(0, 0, 0);
  /** La fleche de buse */
  double fleche_=0;
  /** La portee de buse */
  double portee_=0;
  /** La largeur de la fente Preissman */
  double fente_=0;
  /** La couverture */
  double couverture_=0;

  /**
   * Le mode de connexion
   * Inutilis�
   */
  @Override
  public void setConnectMode(int mode) {}

  @Override
  public void setParent(IDialogInterface p) {
    parent_=p;
  }

  @Override
  public BDialog getEditor() {
    if (editor_==null) {
      editor_=new Hydraulique1dProfilFormeBuseArcheEditor(parent_, this);
      if (parent_!=null) {
        editor_.setLocationRelativeTo(parent_.getComponent());
      }
    }
    editor_.setModal(true);
    return editor_;
  }

  /**
   * Les points de construction de ce profil simple
   * @return GrPoint[] Les points
   */
  @Override
  public GrPoint[] getPoints() {
    GrPoint[] ps=null;
    Vector<GrPoint> v=new Vector<GrPoint>();
    GrPoint pt=startPoint_;

    double r=portee_/2.;
    double a=r;
    double b=fleche_-r;

    // Angle de fente
    double af=Math.asin(fente_/portee_);
    if (fente_>=portee_) af=Math.PI/2.;

    v.add(new GrPoint(pt.x_,pt.y_,pt.z_));
    v.add(new GrPoint(pt.x_+(portee_-fente_)/2.+1.0,pt.y_,pt.z_));
    for (int i=0; i<8; i++) {
      double ang=i*(Math.PI-2*af)/16+af;
      v.add(new GrPoint(pt.x_+r*(1.-Math.sin(ang))+1.0,pt.y_-fleche_-couverture_+b+r*Math.cos(ang),pt.z_));
    }
    for (int i=0; i<8; i++) {
      double ang=i*Math.PI/16;
      v.add(new GrPoint(pt.x_+a*(1.-Math.cos(ang))+1.0,pt.y_-fleche_-couverture_+b*(1-Math.sin(ang)),pt.z_));
    }
    for (int i=0; i<8; i++) {
      double ang=i*Math.PI/16;
      v.add(new GrPoint(pt.x_+a*(1.+Math.sin(ang))+1.0,pt.y_-fleche_-couverture_+b*(1-Math.cos(ang)),pt.z_));
    }
    for (int i=0; i<9; i++) {
      double ang=i*(Math.PI-2*af)/16;
      v.add(new GrPoint(pt.x_+r*(1.+Math.cos(ang))+1.0,pt.y_-fleche_-couverture_+b+r*Math.sin(ang),pt.z_));
    }
    v.add(new GrPoint(pt.x_+(portee_+fente_)/2.+1.0,pt.y_,pt.z_));
    v.add(new GrPoint(pt.x_+portee_+2.0,pt.y_,pt.z_));

    ps=v.toArray(new GrPoint[v.size()]);
    return ps;
  }

  /**
   * Le point origine du profil.
   * @param p Le point origine
   */
  @Override
  public void setStartPoint(GrPoint p) {
    startPoint_=p;
  }

  /**
   * Le dernier point du profil.
   * @return Le point.
   */
  @Override
  public GrPoint getEndPoint() {
    return new GrPoint(startPoint_.x_+portee_+2.0,startPoint_.y_,startPoint_.z_);
  }

  /**
   * Le texte utilis� dans la liste d�roulante et dans le titre de l'�diteur.
   * @return String
   */
  @Override
  public String getName() {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString("Buse arche");
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener l) {
    prop_.addPropertyChangeListener(l);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener l) {
    prop_.removePropertyChangeListener(l);
  }

  /**
   * Portee de la buse
   * @param p La portee de la buse
   */
  public void setPortee(double p) {
    if (portee_==p) return;

    double vp=portee_;
    portee_=p;
    prop_.firePropertyChange("portee", new Double(vp), new Double(portee_));
  }

  /**
   * Retourne la portee de la buse
   * @return la portee de la buse
   */
  public double getPortee() {
    return portee_;
  }

  /**
   * Fleche de la buse
   * @param f La fleche de la buse
   */
  public void setFleche(double f) {
    if (fleche_==f) return;

    double vp=fleche_;
    fleche_=f;
    prop_.firePropertyChange("fleche", new Double(vp), new Double(portee_));
  }

  /**
   * Retourne la fleche de la buse
   * @return la fleche de la buse
   */
  public double getFleche() {
    return fleche_;
  }

  /**
   * La largeur de la fente Preissman
   * @param _l La largeur
   */
  public void setFente(double _l) {
    if (fente_==_l) return;

    double vp=fente_;
    fente_=_l;
    prop_.firePropertyChange("fente", new Double(vp), new Double(fente_));
  }

  /**
   * Retourne la largeur de la fente Preissman
   * @return La largeur de la fente Preissman
   */
  public double getFente() {
    return fente_;
  }

  /**
   * La couverture
   * @param _c La couverture
   */
  public void setCouverture(double _c) {
    if (couverture_==_c) return;

    double vp=couverture_;
    couverture_=_c;
    prop_.firePropertyChange("couverture", new Double(vp), new Double(couverture_));
  }

  /**
   * Retourne la couverture
   * @return La couverture
   */
  public double getCouverture() {
    return couverture_;
  }
}

/**
 * Editeur de profil Dalot Rectangulaire
 * @author Bertrand Marchand
 * @version 1.0
 */
class Hydraulique1dProfilFormeBuseArcheEditor extends BDialog implements ActionListener {

  Hydraulique1dProfilFormeBuseArche forme_;

  BuTextField tfFleche_;
  BuTextField tfPortee_;
  BuTextField tfFente_;
  BuTextField tfCouverture_;

  public Hydraulique1dProfilFormeBuseArcheEditor(IDialogInterface parent, Hydraulique1dProfilFormeBuseArche f) {
    super(null, parent);
    setTitle(f.getName());
    setModal(true);

    forme_=f;
    Container cp=getContentPane();
    cp.setLayout(new BorderLayout());

    JPanel pnCenter=new JPanel();
    pnCenter.setLayout(new BorderLayout(8,8));
    pnCenter.setBorder(new CompoundBorder(new EtchedBorder(), new EmptyBorder(10,10,10,10)));

    int n=0;
    BuPanel pnEdit=new BuPanel();
    pnEdit.setLayout(new BuGridLayout(3, 5, 1, true, true));
    pnEdit.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Fl�che la buse")+" (F):"), n++);
    tfFleche_=BuTextField.createDoubleField();
    tfFleche_.setColumns(5);
    pnEdit.add(tfFleche_, n++);
    pnEdit.add(new BuLabel("m"), n++);
    pnEdit.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Port�e de la buse")+" (P):"), n++);
    tfPortee_=BuTextField.createDoubleField();
    tfPortee_.setColumns(5);
    pnEdit.add(tfPortee_, n++);
    pnEdit.add(new BuLabel("m"), n++);
    pnEdit.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Largeur fente Preissman")+" (Lf):"), n++);
    tfFente_=BuTextField.createDoubleField();
    tfFente_.setColumns(5);
    pnEdit.add(tfFente_, n++);
    pnEdit.add(new BuLabel("m"), n++);
    pnEdit.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Couverture")+" (C):"), n++);
    tfCouverture_=BuTextField.createDoubleField();
    tfCouverture_.setColumns(5);
    pnEdit.add(tfCouverture_, n++);
    pnEdit.add(new BuLabel("m"), n++);
    pnCenter.add(BorderLayout.NORTH, pnEdit);

    JLabel lbImage=new JLabel();
    lbImage.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("hydraulique1dbusearche.gif"));
    lbImage.setHorizontalAlignment(JLabel.CENTER);
    pnCenter.add(BorderLayout.CENTER, lbImage);

    JButton btOk=new JButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    btOk.setHorizontalAlignment(JButton.CENTER);
    btOk.setActionCommand("VALIDER");
    btOk.addActionListener(this);

    cp.add(BorderLayout.SOUTH, btOk);
    cp.add(BorderLayout.CENTER, pnCenter);

    pack();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String cmd=e.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      Double val=(Double)tfFleche_.getValue();
      if (val!=null) forme_.setFleche(val.doubleValue());

      val=(Double)tfPortee_.getValue();
      if (val!=null) forme_.setPortee(val.doubleValue());

      val=(Double)tfFente_.getValue();
      if (val!=null) forme_.setFente(val.doubleValue());

      val=(Double)tfCouverture_.getValue();
      if (val!=null) forme_.setCouverture(val.doubleValue());

      dispose();
    }
  }
}
