/*
 * @file         Hydraulique1dProfilFormeRectangle.java
 * @creation     1999-12-28
 * @modification $Date: 2007-02-21 16:33:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.profil;

import java.beans.*;
import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import org.fudaa.ebli.dialog.*;
import org.fudaa.ebli.geometrie.*;
import org.fudaa.fudaa.hydraulique1d.*;
import com.memoire.bu.*;

/**
 * Profil simple de forme buse circulaire.
 * @version      $Revision: 1.2 $ $Date: 2007-02-21 16:33:51 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class Hydraulique1dProfilFormeBuseCirculaire implements Hydraulique1dProfilFormeSimple {

  PropertyChangeSupport prop_=new PropertyChangeSupport(this);
  IDialogInterface parent_;

  /** L'�diteur li� a cette forme */
  Hydraulique1dProfilFormeBuseCirculaireEditor editor_=null;
  /** Le point de d�part de d�finition du profil */
  GrPoint startPoint_=new GrPoint(0, 0, 0);
  /** Le diametre de buse */
  double diametre_=0;
  /** La largeur de la fente Preissman */
  double fente_=0;
  /** La couverture */
  double couverture_=0;

  /**
   * Le mode de connexion
   * Inutilis�
   */
  @Override
  public void setConnectMode(int mode) {}

  @Override
  public void setParent(IDialogInterface p) {
    parent_=p;
  }

  @Override
  public BDialog getEditor() {
    if (editor_==null) {
      editor_=new Hydraulique1dProfilFormeBuseCirculaireEditor(parent_, this);
      if (parent_!=null) {
        editor_.setLocationRelativeTo(parent_.getComponent());
      }
    }
    editor_.setModal(true);
    return editor_;
  }

  /**
   * Les points de construction de ce profil simple
   * @return GrPoint[] Les points
   */
  @Override
  public GrPoint[] getPoints() {
    GrPoint[] ps=null;
    Vector<GrPoint> v=new Vector<GrPoint>();
    GrPoint pt=startPoint_;

    // Angle de fente
    double af=Math.asin(fente_/diametre_);
    if (fente_>=diametre_) af=Math.PI/2.;

    v.add(new GrPoint(pt.x_,pt.y_,pt.z_));
    v.add(new GrPoint(pt.x_+(diametre_-fente_)/2.+1.0,pt.y_,pt.z_));
    for (int i=0; i<35; i++) {
      double a=i*(Math.PI-af)/17+af;
      v.add(new GrPoint(pt.x_+diametre_/2.*(1.-Math.sin(a))+1.0,pt.y_-couverture_-diametre_/2.*(1.-Math.cos(a)),pt.z_));
    }
    v.add(new GrPoint(pt.x_+(diametre_+fente_)/2.+1.0,pt.y_,pt.z_));
    v.add(new GrPoint(pt.x_+diametre_+2.0,pt.y_,pt.z_));

    ps=v.toArray(new GrPoint[v.size()]);
    return ps;
  }

  /**
   * Le point origine du profil.
   * @param p Le point origine
   */
  @Override
  public void setStartPoint(GrPoint p) {
    startPoint_=p;
  }

  /**
   * Le dernier point du profil.
   * @return Le point.
   */
  @Override
  public GrPoint getEndPoint() {
    return new GrPoint(startPoint_.x_+diametre_+2.0,startPoint_.y_,startPoint_.z_);
  }

  /**
   * Le texte utilis� dans la liste d�roulante et dans le titre de l'�diteur.
   * @return String
   */
  @Override
  public String getName() {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString("Buse circulaire");
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener l) {
    prop_.addPropertyChangeListener(l);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener l) {
    prop_.removePropertyChangeListener(l);
  }

  /**
   * Diametre de la buse
   * @param h Le diametre de la buse
   */
  public void setDiametre(double h) {
    if (diametre_==h) return;

    double vp=diametre_;
    diametre_=h;
    prop_.firePropertyChange("diametre", new Double(vp), new Double(diametre_));
  }

  /**
   * Retourne le diametre de la buse
   * @return le diametre de la buse
   */
  public double getDiametre() {
    return diametre_;
  }

  /**
   * La largeur de la fente Preissman
   * @param _l La largeur
   */
  public void setFente(double _l) {
    if (fente_==_l) return;

    double vp=fente_;
    fente_=_l;
    prop_.firePropertyChange("fente", new Double(vp), new Double(fente_));
  }

  /**
   * Retourne la largeur de la fente Preissman
   * @return La largeur de la fente Preissman
   */
  public double getFente() {
    return fente_;
  }

  /**
   * La couverture
   * @param _c La couverture
   */
  public void setCouverture(double _c) {
    if (couverture_==_c) return;

    double vp=couverture_;
    couverture_=_c;
    prop_.firePropertyChange("couverture", new Double(vp), new Double(couverture_));
  }

  /**
   * Retourne la couverture
   * @return La couverture
   */
  public double getCouverture() {
    return couverture_;
  }
}

/**
 * Editeur de profil Dalot Rectangulaire
 * @author Bertrand Marchand
 * @version 1.0
 */
class Hydraulique1dProfilFormeBuseCirculaireEditor extends BDialog implements ActionListener {

  Hydraulique1dProfilFormeBuseCirculaire forme_;

  BuTextField tfDiametre_;
  BuTextField tfFente_;
  BuTextField tfCouverture_;

  public Hydraulique1dProfilFormeBuseCirculaireEditor(IDialogInterface parent, Hydraulique1dProfilFormeBuseCirculaire f) {
    super(null, parent);
    setTitle(f.getName());
    setModal(true);

    forme_=f;
    Container cp=getContentPane();
    cp.setLayout(new BorderLayout());

    JPanel pnCenter=new JPanel();
    pnCenter.setLayout(new BorderLayout(8,8));
    pnCenter.setBorder(new CompoundBorder(new EtchedBorder(), new EmptyBorder(10,10,10,10)));

    int n=0;
    BuPanel pnEdit=new BuPanel();
    pnEdit.setLayout(new BuGridLayout(3, 5, 1, true, true));
    pnEdit.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Diam�tre de la buse")+" (D):"), n++);
    tfDiametre_=BuTextField.createDoubleField();
    tfDiametre_.setColumns(5);
    pnEdit.add(tfDiametre_, n++);
    pnEdit.add(new BuLabel("m"), n++);
    pnEdit.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Largeur fente Preissman")+" (Lf):"), n++);
    tfFente_=BuTextField.createDoubleField();
    tfFente_.setColumns(5);
    pnEdit.add(tfFente_, n++);
    pnEdit.add(new BuLabel("m"), n++);
    pnEdit.add(new BuLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("Couverture")+" (C):"), n++);
    tfCouverture_=BuTextField.createDoubleField();
    tfCouverture_.setColumns(5);
    pnEdit.add(tfCouverture_, n++);
    pnEdit.add(new BuLabel("m"), n++);
    pnCenter.add(BorderLayout.NORTH, pnEdit);

    JLabel lbImage=new JLabel();
    lbImage.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("hydraulique1dbusecirculaire.gif"));
    lbImage.setHorizontalAlignment(JLabel.CENTER);
    pnCenter.add(BorderLayout.CENTER, lbImage);

    JButton btOk=new JButton(Hydraulique1dResource.HYDRAULIQUE1D.getString("Valider"));
    btOk.setHorizontalAlignment(JButton.CENTER);
    btOk.setActionCommand("VALIDER");
    btOk.addActionListener(this);

    cp.add(BorderLayout.SOUTH, btOk);
    cp.add(BorderLayout.CENTER, pnCenter);

    pack();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String cmd=e.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      Double val=(Double)tfDiametre_.getValue();
      if (val!=null) forme_.setDiametre(val.doubleValue());

      val=(Double)tfFente_.getValue();
      if (val!=null) forme_.setFente(val.doubleValue());

      val=(Double)tfCouverture_.getValue();
      if (val!=null) forme_.setCouverture(val.doubleValue());

      dispose();
    }
  }
}
