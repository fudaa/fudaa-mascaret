/*
 * @file         Hydraulique1dProfilFormeSimple.java
 * @creation     1999-12-28
 * @modification $Date: 2006-09-12 08:36:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.profil;
import java.beans.PropertyChangeListener;

import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * Interface d'un profil de forme simple.
 * @version      $Revision: 1.7 $ $Date: 2006-09-12 08:36:42 $ by $Author: opasteur $
 * @author       Axel von Arnim
 */
public interface Hydraulique1dProfilFormeSimple {
  int NO_CONNECT= 0;
  int CONNECT_RIGHT= 1;
  int CONNECT_LEFT= 2;
  void setConnectMode(int mode);
  BDialog getEditor();
  GrPoint[] getPoints();
  void setStartPoint(GrPoint p);
  GrPoint getEndPoint();
  String getName();
  boolean isEditable();
  void addPropertyChangeListener(PropertyChangeListener l);
  void removePropertyChangeListener(PropertyChangeListener l);
  void setParent(IDialogInterface d);
}
