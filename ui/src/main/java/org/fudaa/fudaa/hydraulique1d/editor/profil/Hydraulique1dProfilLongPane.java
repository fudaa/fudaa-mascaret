
package org.fudaa.fudaa.hydraulique1d.editor.profil;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilPane.TracerItemCombo;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheProfil;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheProfilLongDataset;
import org.fudaa.fudaa.hydraulique1d.graphe.MascaretXYAreaRenderer;
import org.fudaa.fudaa.hydraulique1d.graphe.MascaretXYAreaRendererProfilLong;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.ui.TextAnchor;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;



/**
 * Visualisation d'un profil en long.
 * @author       Adrien Hadoux
 */
public class Hydraulique1dProfilLongPane extends Hydraulique1dProfilPane {

	private static final long serialVersionUID = 1L;

	BuButton buttonUp, buttonDown;
	
	MoveListener moveUpLstener , moveDownListener;
	/**
	 * Listener used to move  profils into bulist.
	 * @author Adrien
	 *
	 */
	class MoveListener implements ActionListener {
		DefaultListModel<Hydraulique1dProfilModel> modelList;
		boolean moveUp = true;


		public MoveListener(DefaultListModel<Hydraulique1dProfilModel> modelList, boolean moveUp) {
			this.modelList = modelList;
			this.moveUp = moveUp;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			int current = getCurrent();
			if(current == -1) {
				new BuDialogMessage(
						null,
						null,
						Hydraulique1dResource.HYDRAULIQUE1D.getString("Vous devez s�lectionner un �l�ment pour pouvoir le d�placer")).
						activate();
				return;
			}
			System.out.println("move listner , up? ="+moveUp + " , current? = " + current);
			if(moveUp && current-1>=0) {
				Hydraulique1dProfilModel data =  modelList.get(current);
				modelList.set(current,  modelList.get(current-1));
				modelList.set(current-1,data);
				lsProfils_.setSelectedIndex(current-1);
			} else if(!moveUp && current+1<modelList.getSize()) {
				Hydraulique1dProfilModel data =  modelList.get(current);
				modelList.set(current,  modelList.get(current+1));
				modelList.set(current+1,data);
				lsProfils_.setSelectedIndex(current+1);
			}

		}

		public boolean isMoveUp() {
			return moveUp;
		}

		public void setMoveUp(boolean moveUp) {
			this.moveUp = moveUp;
		}

		public int getCurrent() {
			return lsProfils_.getSelectedIndex();
		}



	}
	/**
	 * Renderer pour la liste de profils.
	 * @author Adrien
	 *
	 */
	class ProfilCellRenderer implements ListCellRenderer<Hydraulique1dProfilModel> {
		Color bleuSelected=new Color(54,138,191);
		Color defaultColor = null;
		Color defaultColorWhite = Color.white.brighter();
		BuPanel panel;
		BuLabel nomProfil;



		public ProfilCellRenderer(DefaultListModel<Hydraulique1dProfilModel> modelList) {
			panel = new BuPanel(new FlowLayout(FlowLayout.LEFT));
			nomProfil = new BuLabel("");
			panel.add(nomProfil);
			defaultColor = panel.getBackground();
		}

		@Override
		public Component getListCellRendererComponent(
				JList<? extends Hydraulique1dProfilModel> list,
				Hydraulique1dProfilModel value, int index, boolean isSelected,
				boolean cellHasFocus) {
			nomProfil.setText(value.nom());
			if(isSelected) {
				panel.setBackground(bleuSelected);
				nomProfil.setForeground(Color.white);
			}
			else {
				panel.setBackground(index%2==0?defaultColor:defaultColorWhite);
				nomProfil.setForeground(Color.black.brighter());
			}
			return panel;
		}

	}

	public Hydraulique1dProfilLongPane(int mode, int mode2) {
		super(mode, mode2);

	}

	protected BuList lsProfils_;
	protected DefaultListModel<Hydraulique1dProfilModel>  modelProfilList ;
	protected JCheckBox checkboxMajAuto ;

	private void changeProfilsSelection() {
		modelProfilList.clear();
		
		for(Hydraulique1dProfilModel model: profilsBief_) {
			modelProfilList.addElement(model);
		}

	}
	@Override
	protected JPanel buildLeftPane() {
		JPanel pn=new JPanel();
		pn.setLayout(new BorderLayout());

		lsProfils_ = new BuList();
		lsProfils_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		modelProfilList = new DefaultListModel<Hydraulique1dProfilModel>();
		lsProfils_.setModel(modelProfilList);
		lsProfils_.setCellRenderer(new ProfilCellRenderer(modelProfilList));

		lsProfils_.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				fireDatasetProfilsChanged();
			}
		});

		JScrollPane scrollLis = new JScrollPane(lsProfils_);
		scrollLis.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.getS("Profils du Bief")));

		BuPanel configProfils = new BuPanel();
		buttonUp = new BuButton(FudaaResource.FUDAA.getIcon("monter_16"));
		buttonDown = new BuButton(FudaaResource.FUDAA.getIcon("descendre_16"));
		moveUpLstener = new MoveListener(modelProfilList, true);
		moveDownListener = new MoveListener(modelProfilList, false);
		buttonUp.addActionListener(moveUpLstener);
		buttonDown.addActionListener(moveDownListener);

		configProfils.add(new BuLabel(Hydraulique1dResource.getS("Ordre du profil s�lectionn�")));
		configProfils.add(buttonUp);
		configProfils.add(buttonDown);
		BuPanel panelProfils = new BuPanel(new BorderLayout());
		panelProfils.add(scrollLis, BorderLayout.CENTER);
		
		JPanel grid = new JPanel(new GridLayout(2,1));
		grid.add(configProfils);
		
		JPanel showProfilLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel showAutomatiquePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel btnPanel = new JPanel(new BorderLayout());
		btnPanel.add(showProfilLinePanel,BorderLayout.CENTER);
		btnPanel.add(showAutomatiquePanel,BorderLayout.SOUTH);
		
		grid.add(btnPanel);
		
		final JCheckBox boxDisplayProfils  = new JCheckBox();
		boxDisplayProfils.setText(Hydraulique1dResource.getS("Afficher le nom des profils"));
		showProfilLinePanel.add(boxDisplayProfils);
		boxDisplayProfils.setSelected(false);
		boxDisplayProfils.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
					if(graphe_ != null && graphe_.getGraphe_() != null 
					&& graphe_.getGraphe_().getXYPlot().getRenderer() instanceof MascaretXYAreaRendererProfilLong){
						MascaretXYAreaRendererProfilLong renderer = (MascaretXYAreaRendererProfilLong)graphe_.getGraphe_().getXYPlot().getRenderer();
						boolean isSelected = boxDisplayProfils.isSelected();
						renderer.setDrawProfileLine(isSelected);
						updateGraphe();
					}
			}	
		});
		
		checkboxMajAuto = new JCheckBox(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mise � jour automatique"));
		showAutomatiquePanel.add(checkboxMajAuto);
		checkboxMajAuto.setSelected(true);
		checkboxMajAuto.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {				
					if(graphe_ != null && graphe_.getGraphe_() != null && checkboxMajAuto.isSelected()){						
						fireDatasetProfilsChanged();
					}
			}	
		});
		
		panelProfils.add(grid,BorderLayout.SOUTH);
		
		
		
		//-- add combo for  tracers --//
				JPanel panelTracer = new JPanel(new BorderLayout());		
				if(CGlobal.AVEC_QUALITE_DEAU) {
					coTracer_.setEnabled(CGlobal.AVEC_QUALITE_DEAU);
					panelTracer.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.getS("Concentrations (kg/m3)")));
					JPanel panelCOmboTracer = new JPanel();
					panelCOmboTracer.add(coTracer_);
					panelTracer.add(panelCOmboTracer,BorderLayout.NORTH);
					
					//-- ajout de la l�gende --//
					legendeColoration = new JPanel(new BorderLayout());
					panelTracer.add(legendeColoration,BorderLayout.CENTER);
							
					 contentConcentration = new JPanel(new BorderLayout());
					contentConcentration.add(new JLabel(
							Hydraulique1dResource.HYDRAULIQUE1D.getString("R�sultats qualit� d'eau")),
							BorderLayout.NORTH);			
					contentConcentration.add(panelTracer,BorderLayout.CENTER);
					JSplitPane paneSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,panelProfils,contentConcentration );
					pn.add(paneSplit,BorderLayout.CENTER);
					paneSplit.setDividerLocation(0.6);
					paneSplit.setResizeWeight(0.5);			
				
				}else {
					panelTracer.setVisible(false);
					//-- add the panel of profils --//
					pn.add(panelProfils,BorderLayout.CENTER);
				}
		
		

		if (modeEdition_==VOIR) {
			lsBief_ = new BuList();
			lsBief_.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
			lsBief_.addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					changeBiefsSelection();
					changeProfilsSelection();
					//-- by default select all profils --//
					lsProfils_.setSelectionInterval(0, lsProfils_.getModel().getSize()-1);
					
				}


			});
			BuScrollPane spBiefs_= new BuScrollPane(lsBief_);
			spBiefs_.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.getS("Biefs")));
			spBiefs_.setPreferredHeight(70);

			pn.add(spBiefs_,BorderLayout.NORTH);
		}

		return pn;
	}
	
	protected void updateGrapheProfilModel(Hydraulique1dProfilModel[] _profils) {
		if(checkboxMajAuto.isSelected())  
			graphe_.setBiefProfilModels(_profils);
	  }
	

	 protected void setLabelGraphe(JPanel pnRight) {
		 //no title
	 }
	
	public Hydraulique1dGrapheProfilLongDataset getDataset() {
		return (Hydraulique1dGrapheProfilLongDataset)graphe_.getDataset_();
	}


	public void fireDatasetProfilsChanged() {
		fireDatasetProfilsChanged(checkboxMajAuto.isSelected());
	}
	
	public void fireDatasetProfilsChanged(boolean maj) {
		
		int[] indices = lsProfils_.getSelectedIndices();
		
		setDatasetProfils(indices, maj);
	}
	
	
	

	private void setDatasetProfils(int[] indices, boolean maj) {
		if(!maj || indices == null || indices.length ==0 || lsBief_.getSelectedIndex() == -1)
			return;

		List<Hydraulique1dProfilModel> profilsList = new ArrayList<Hydraulique1dProfilModel>();
		for(int i=0;i<indices.length;i++){
			Hydraulique1dProfilModel profil= modelProfilList.get(indices[i]);
			profilsList.add(profil);		
			//--calcul niveau d'eau au pas de temps t=0; --//
			double z = calculeCoteZProfil(lsBief_.getSelectedIndex(), indices[i], 0);
			profil.setNiveauEauInitial_(z);
		}
		
		getDataset().setBiefProfilModels(profilsList.toArray(new Hydraulique1dProfilModel[profilsList.size()]));
		
		//-- compute again les concentrations car on change de profil.--//
		if(isTracerComputation()) {
			computeTracerValues();
		}
		
		updateGraphe();
		
		
	}


	public void updateGraphe() {
		if(checkboxMajAuto.isSelected()) {
			graphe_.getGraphe_().getXYPlot().clearDomainMarkers();
			graphe_.getGraphe_().getXYPlot().clearRangeMarkers();
			getDataset().updateData();
			
			displayProfilMarker();
		}
		
	}
	
	 protected void updateTpsGraphe() {
		 if(checkboxMajAuto.isSelected()) { 
			 graphe_.updateGraphe();
		 }
	  }
	
	private void displayProfilMarker() {
		/* ne correspond pas au rendu
		for(int i=0;i<getDataset().getItemCount(0);i++) {
			double x = getDataset().getXValue(0,i);
			
		ValueMarker mkNivEau=new ValueMarker(x);

		mkNivEau.setLabel("P"+i);
		mkNivEau.setLabelPaint(Color.black);
		mkNivEau.setPaint(Color.black);
		mkNivEau.setStroke(new BasicStroke(1));
		mkNivEau.setLabelAnchor(RectangleAnchor.TOP_LEFT);
		mkNivEau.setLabelTextAnchor(TextAnchor.BOTTOM_LEFT);

		graphe_.getGraphe_().getXYPlot().addDomainMarker(mkNivEau,Layer.BACKGROUND);
		}
		*/
	}
	public void setGrapheRenderer() {
//		XYAreaRenderer2 renderer=new XYAreaRenderer2();
//		graphe_.getGraphe_().getXYPlot().setRenderer(renderer);
		
		XYLineAndShapeRenderer renderer=new XYLineAndShapeRenderer();
		MascaretXYAreaRendererProfilLong rendererZone = new MascaretXYAreaRendererProfilLong(renderer);
		renderer.setSeriesShape(0,new java.awt.geom.Rectangle2D.Double(-2,-2,4,4));
		//renderer.setSeriesPaint(0,Color.RED);
		//-- couleur de la ligne d'eau du dessus --//
		renderer.setSeriesPaint(0,new Color(132,166,188));
		
		//-- couleur de la ligne du bas --//
		renderer.setSeriesPaint(1,Color.RED.darker());
		
		//-- couleur de la ligne t=0 pointill�e --//
		renderer.setSeriesPaint(2,Color.BLACK.darker());

        renderer.setBasePositiveItemLabelPosition(
            new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER));
        renderer.setBaseItemLabelFont(
            renderer.getBaseItemLabelFont().deriveFont(14f));
    
        rendererZone.setBasePositiveItemLabelPosition(
            new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER));
        rendererZone.setBaseItemLabelFont(
        		rendererZone.getBaseItemLabelFont().deriveFont(14f));
        
        
        //renderer.setSeriesLinesVisible(0,true);
		//renderer.setSeriesShapesFilled(0,true);
		//renderer.setSeriesStroke(0,  new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));
	
		renderer.setSeriesStroke(1, 
	            new BasicStroke(2.0f));
		renderer.setSeriesStroke(2, 
	            new BasicStroke(
		                1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND
		                ,1.0f, new float[] {10.0f, 6.0f}, 0.0f)
		            );
		graphe_.getGraphe_().getXYPlot().setRenderer(rendererZone);
	}
	
	
	@Override
	protected void initGraphe() {

		graphe_=new Hydraulique1dGrapheProfil(new Hydraulique1dGrapheProfilLongDataset(),false);
		graphe_.setBiefProfilModels(profilsBief_);
		graphe_.setPreferredSize(new Dimension(400, 400));
		
		setGrapheRenderer();
	}


	
	
	/**
	 * Methode qui calcule la concentration en traceur pour le bief, pas de temps et tracer selectionn�.
	 * Des min et max sont calcul�s afin d'�tablir une �chelle de valeur repr�sent� par une l�gende.
	 */
	protected void computeTracerValues() {
		
		//-- reinit by default  legend + graph coloration --//
		legendeColoration.removeAll();
		legendeColoration.setVisible(false);
		MascaretXYAreaRendererProfilLong renderer = (MascaretXYAreaRendererProfilLong)graphe_.getGraphe_().getXYPlot().getRenderer();
		renderer.setPaintConcentration(false);
		
		int selectiontracer = coTracer_.getSelectedIndex();
		if(selectiontracer ==0 || selectiontracer == -1){
			currentConcentrations = null;
			updateConcentrationForRenderer();
			return;
		}

		
		int sbief = lsBief_.getSelectedIndex();
		int pasTemps = coTemps_.getSelectedIndex();
		int[] indicesProfils = lsProfils_.getSelectedIndices();
		
		if(indicesProfils == null || indicesProfils.length ==0) {
			return;
		}
		
		//-- attention! l'indice de la variable doit correspondre � l'indice parmi toutes les variables et non a l'indice de la combo list --//
		int trueIndiceVariable =-1;
		for(int i=0;i<getResultTracer().descriptionVariables().length;i++) {
			MetierDescriptionVariable var = getResultTracer().descriptionVariables()[i];
			MetierDescriptionVariable selectrac = ((TracerItemCombo) coTracer_.getModel().getElementAt(selectiontracer)).var;
			if(var == selectrac)
				trueIndiceVariable =i;
		}
		if( trueIndiceVariable == -1 || sbief == -1 || pasTemps == -1
				|| getResultTracer().resultatsBiefs().length<=sbief || getResultTracer().resultatsBiefs()[sbief].valeursVariables().length
				<=trueIndiceVariable || getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable).length
				<=pasTemps) {
			System.out.println("la concentration ne sera pas affich�e: Hydraulique1dProfilLongPane.computeTracerValues():"
					+ "sbief="+sbief+ " pasTemps="+pasTemps+" indiceVar="+trueIndiceVariable
					);
			/*+"et les tailles sont: sbief="+getResultTracer().resultatsBiefs().length+ " pasTemps="
					+getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable).length+
					" indiceVar="+getResultTracer().resultatsBiefs()[sbief].valeursVariables().length);
			*/
			currentConcentrations = null;
			updateConcentrationForRenderer();
			return;
		}
		currentConcentrations = getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable)[pasTemps];

		if(currentConcentrations == null || currentConcentrations.length ==0){
			currentConcentrations = null;
			return;
		}

		//-- find min, max of this current Concentrations --//
		//-- ATTENTION! contrairement au profils travers 
		//-- ici on cherche uniquement les min et max parmis les concentrations s�lectionn�es --//
		minC = currentConcentrations[indicesProfils[0]];
		maxC  = currentConcentrations[indicesProfils[0]];
		for(int pdt=0;pdt<getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable).length;pdt++) {
			double[] crt = getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable)[pdt];
			
			for(int i=0;i<indicesProfils.length;i++){
				if(crt[indicesProfils[i]]>maxC)
					maxC = crt[indicesProfils[i]];
				if(crt[indicesProfils[i]]<minC)
					minC = crt[indicesProfils[i]];
			}
		}
		//-- si toutes les valeurs sont identiques --//
		if(minC == maxC)
			numberOFDifferentsConcentrations =1;
		else  if(numberOFDifferentsConcentrations == 1) {
			numberOFDifferentsConcentrations=DEFAULT_NB_CONCENTRATION;
		}

		colorConcentrations = new Color[numberOFDifferentsConcentrations];
		//colorConcentrations[0] = new Color(132,166,188);
		colorConcentrations[0] = DEFAULT_COLOR_RANGE[0];
		int cptColor =0;
		for(int i=1;i<numberOFDifferentsConcentrations;i++) {
			if(i%3 ==0 && cptColor < DEFAULT_COLOR_RANGE.length-1){
				cptColor++;
				colorConcentrations[i] =DEFAULT_COLOR_RANGE[cptColor];
			}else
				if(i<3 || (i>=6 &&i<9) )
				colorConcentrations[i] =colorConcentrations[i-1].brighter();
				else
					colorConcentrations[i] =colorConcentrations[i-1].darker();
		}
		//-- creation de la legende --//
		/*
		Box legende = Box.createVerticalBox();
		legende.setBackground(Color.white);
		JPanel content;
		JLabel colorLabel, textLabel;
		DecimalFormat myFormatter = new DecimalFormat("0.0000");
		double var =  (maxC - minC)/colorConcentrations.length;	
		for(int i=0;i<numberOFDifferentsConcentrations;i++) {
			content = new JPanel(new FlowLayout(FlowLayout.LEFT));
			content.setBackground(Color.white);
			colorLabel = new JLabel("  ");
			colorLabel.setOpaque(true);
			colorLabel.setBackground(colorConcentrations[i]);
			colorLabel.setPreferredSize(new Dimension(16,16));
			content.add(colorLabel);
			textLabel= new JLabel( myFormatter.format(minC + (i)*var) +"< X "+"< " + myFormatter.format(minC + (i+1)*var));
			content.add(textLabel);
			legende.add(content);
		}
		*/
		
			JPanel legende = new JPanel(new GridLayout(numberOFDifferentsConcentrations,2,0,0)){
			
			public void paintComponent(Graphics g) {
		        super.paintComponent(g);
	      
		        
		        DecimalFormat legendFormatter = new DecimalFormat("0.0000");
				double var =  (maxC - minC)/colorConcentrations.length;	
				int x=10,y=10, width = 32, height=32;
				for(int i=numberOFDifferentsConcentrations-1;i>=0;i--) {
					g.setColor(colorConcentrations[i]);
					g.fillRect(x, y, width, height);
					g.setColor(Color.black);
					g.drawLine(x+26, y+32, x+31, y+32);
					g.drawLine(11, y+32, 16, y+32);
					//if(y !=10) {
						g.drawLine(x+26, y, x+31, y);
						g.drawLine(11, y, 16, y);
				//	}
					g.drawString(legendFormatter.format(minC + (i)*var), x +35, y+35);
					if(i == (numberOFDifferentsConcentrations-1)) {
						g.drawString(legendFormatter.format(minC + (i+1)*var), x +35, y+5);					
					}
					y =y +32;
				}
		        
		    }
			};
			legende.setBackground(Color.white);
		
		legende.setPreferredSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		legende.setSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		legende.setMaximumSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		legendeColoration.removeAll();
		
		legendeColoration.setLayout(new BorderLayout());
		legendeColoration.add(new JScrollPane(legende), BorderLayout.CENTER);
		legendeColoration.setVisible(true);

		legendeColoration.setPreferredSize(new Dimension(200,25*numberOFDifferentsConcentrations));
		legendeColoration.setSize(new Dimension(200,25*numberOFDifferentsConcentrations));
		legendeColoration.setMaximumSize(new Dimension(200,25*numberOFDifferentsConcentrations));
		legendeColoration.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.HYDRAULIQUE1D.getString("L�gende")));
		updateConcentrationForRenderer();
		graphe_.updateGraphe();
	}


	/**
	 * contrairement aux profils en travers, ici on cherche � construire un tableau de couleurs pour mettre en couleurs chaque polygone.
	 */
	public void updateConcentrationForRenderer () {
		
		
		//-- envoies les infos au renderer du profil --//
		MascaretXYAreaRendererProfilLong renderer = (MascaretXYAreaRendererProfilLong)graphe_.getGraphe_().getXYPlot().getRenderer();
		
		if(currentConcentrations == null){
			renderer.setPaintConcentration(false);
			return;
		}
		
		if(renderer != null) {
			int[] indicesProfils = lsProfils_.getSelectedIndices();
			
			if(indicesProfils == null || indicesProfils.length ==0) {
				//desactivate concentraton mode
				renderer.setPaintConcentration(false);
			}
			
			//-- si on a 3 indicesProfils s�lectionn�s, on a 2 polygones � dessinner --//
			ColorAndRangePolygone[][] colorationPolygoneConcentration = new ColorAndRangePolygone[indicesProfils.length-1][];
			double variation =  (maxC - minC)/colorConcentrations.length;		
			for(int i=0;i<indicesProfils.length-1;i++) {
				
				Hydraulique1dProfilModel profilModel1 =  modelProfilList.get(indicesProfils[i]);
				Hydraulique1dProfilModel profilModel2 =  modelProfilList.get(indicesProfils[i+1]);
				int indiceProfil1 = findSectionBiefCloseToProfilAbscisse(profilModel1);
				int indiceProfil2 = findSectionBiefCloseToProfilAbscisse(profilModel2);
				if(indiceProfil1>indiceProfil1){
					int temp = indiceProfil1;
					indiceProfil1 = indiceProfil2;
					indiceProfil2 = temp;
				}
										
				double concentration1 = currentConcentrations[indiceProfil1];
				double concentration2 = currentConcentrations[indiceProfil2];
				
				//-- interpolation lin�aire entre les concentrations --//
				//double concentration = (concentration1+concentration2)/2.0d;
				//Color colorConcentration = getColorForConcentrationValue(concentration, colorConcentrations, minC, maxC);
				int colorConcentrationDegradeDebut = getIndiceColorForConcentrationValue(concentration1, colorConcentrations, minC, maxC);
				int colorConcentrationDegradeFin = getIndiceColorForConcentrationValue(concentration2, colorConcentrations, minC, maxC);
				
				int temp = colorConcentrationDegradeDebut;
				boolean concentrationDecroissante = false;
				if(colorConcentrationDegradeFin<colorConcentrationDegradeDebut){
					colorConcentrationDegradeDebut = colorConcentrationDegradeFin;
					colorConcentrationDegradeFin = temp;
					concentrationDecroissante = true;
				}
				
				
				
				//-- on remplit un tableau de degrad� de couleurs avec l'ensemble des couleurs de la l�gende --// 
				int nbPolygones = (colorConcentrationDegradeFin-colorConcentrationDegradeDebut +1);
				ColorAndRangePolygone[] numberDegradeColor = new ColorAndRangePolygone[nbPolygones];
				int cpt =0;
				int degDecroissant = colorConcentrationDegradeFin;
				for(int deg=colorConcentrationDegradeDebut; deg<=colorConcentrationDegradeFin;deg++) {
					ColorAndRangePolygone cr = new ColorAndRangePolygone();
					//--Adrien - ACHTUNG!!!!! SI la concentration est d�croissante, il faut inverser les couleurs � afficher!!! --//
					if(!concentrationDecroissante){ 
						cr.c = colorConcentrations[deg];
						cr.minLegende = minC + deg*variation;
						cr.maxLegende = minC + (deg+1)*variation;
					}
					else {
						cr.c = colorConcentrations[degDecroissant];
						cr.minLegende = minC + degDecroissant*variation;
						cr.maxLegende = minC + (degDecroissant+1)*variation;
						degDecroissant--;
					}
					
					if(concentration1 < concentration2) {
						cr.polygoneMinconcentration = concentration1;
						cr.polygoneMaxconcentration = concentration2;
					} else {
						cr.polygoneMinconcentration = concentration2;
						cr.polygoneMaxconcentration = concentration1;
					}
					cr.computeTaillePolygone();
					numberDegradeColor[cpt++] = cr;
				}
				
				adaptPolygoneSize(numberDegradeColor, nbPolygones);
				colorationPolygoneConcentration[i] = numberDegradeColor;
			}
			
			renderer.setColorConcentrations(colorationPolygoneConcentration);
			renderer.setPaintConcentration(true);
			
		}
	}
	
	/**
	 * Methode qui ajuste le pourcentage des polygones par rapport au nombre de polygones � dessinner.
	 * en effet la methode ColorAndRangePolygone.computeTaillePolygone () calcule la taille en % des polygones sans prendre en compte le nb total de polygones � dessinner.
	 * Cette methode va r�-ajuster la taille en % par rapport au nombre tota de polygones = 100%
	 * @param colorationPolygoneConcentration
	 * @param nbpolygones
	 */
	public void adaptPolygoneSize(ColorAndRangePolygone[] colorationPolygoneConcentration, int nbpolygones) {
		double sommePourcentageTaillePolygones = 0;
		for(ColorAndRangePolygone c: colorationPolygoneConcentration) {
			sommePourcentageTaillePolygones = sommePourcentageTaillePolygones + c.taillePolygone;
		}
		for(ColorAndRangePolygone c: colorationPolygoneConcentration) {
			c.taillePolygone = c.taillePolygone  / sommePourcentageTaillePolygones;
		}
	}
	
	
	public static class ColorAndRangePolygone{
		public Color c;
		public double minLegende;
		public double maxLegende;
		public double polygoneMinconcentration;
		public double polygoneMaxconcentration;
		/** taille en % pour le polygone en particulier compte tenu de sa valeur de concentration par rapport � l'�chelle.*/
		public double taillePolygone =1;
		
		/**
		 * Methode qui calcule la taille en pourcentage des polygones sans prendre en compte le nombre de polygones.
		 * @author Adrien Hadoux
		 */
		public void computeTaillePolygone() {

			
			if(minLegende>=polygoneMinconcentration && maxLegende <=polygoneMaxconcentration) {
				//-- dans ce cas la range de concentration de la l�gende est inclue entre les concentrations des profils donc il a une taille de 100% --//
				taillePolygone = 1;
				return;
			}else 
				if(minLegende <=polygoneMinconcentration && maxLegende >=polygoneMaxconcentration ) {
					//-- dans ce cas tout le polygone est inclus dans  la range de concentration de la l�gende  donc il a une taille de 100% --//
					taillePolygone = 1;
					return;
				} else 
					if(minLegende <polygoneMinconcentration ) {
						//-- cas ou le min de la l�gende n'est pas inclus, on calcule le % couvert par ce polygone --//
						double ecartLegende = Math.abs(minLegende - maxLegende);
						double ecartReel = Math.abs(polygoneMinconcentration - maxLegende);
						taillePolygone = ecartReel/ecartLegende;
					} else 
						if(maxLegende >polygoneMaxconcentration ) {
							//-- cas ou le max de la l�gende n'est pas inclus, on calcule le % couvert par ce polygone --//
							double ecartLegende = Math.abs(maxLegende - minLegende );
							double ecartReel = Math.abs(polygoneMaxconcentration - minLegende);
							taillePolygone = ecartReel/ecartLegende;
						} else 
							taillePolygone =1;

		}
		
		
	}
	
	
	
	 protected BuPanel buildToolBar() {
		 return buildToolBar(false);
	 }
	public void visualiser(boolean calculBorne, boolean silentMode) {
		fireDatasetProfilsChanged(true);
		
	}
	
	

}
