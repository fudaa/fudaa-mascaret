/*
 * @file         Hydraulique1dProfilEditor.java
 * @creation     1999-05-27
 * @modification $Date: 2007-11-20 11:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.profil;

import gnu.trove.TDoubleArrayList;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatialBief;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEventListener;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.ebli.animation.EbliAnimationAction;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dDialogContraintes;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dPreferences;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilDataEvent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilDataListener;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilSelectionEvent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilSelectionListener;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dBornesGrapheEditor;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dFiltrageProfilEditor;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dInsererProfilEditor;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dLissageProfilEditor;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dTranslaterProfilEditor;
import org.fudaa.fudaa.hydraulique1d.graphe.CurveOptionPane;
import org.fudaa.fudaa.hydraulique1d.graphe.CurveOptionTargetI;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheProfil;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheProfilDataset;
import org.fudaa.fudaa.hydraulique1d.graphe.MascaretXYAreaRenderer;
import org.fudaa.fudaa.hydraulique1d.graphe.MascaretXYAreaRendererProfilLong;
import org.jfree.chart.LegendItem;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.LegendItemSource;
import org.jfree.chart.title.LegendTitle;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuUndoRedoInterface;

/**
 * Le panneau pour la visu/edition d'un profil en travers.
 * 
 * @version      $Revision: 1.22 $ $Date: 2007-11-20 11:43:26 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class Hydraulique1dProfilPane extends JPanel implements CurveOptionTargetI,
ActionListener, Hydraulique1dProfilSelectionListener, Hydraulique1dProfilDataListener,
BuCutCopyPasteInterface, BuUndoRedoInterface {



	/**
	 * 
	 */
	private static final long serialVersionUID = 2955126533515491634L;

	/**
	 * Une classe d'animation du graphe.
	 * @author Bertrand Marchand (marchand@deltacad.fr)
	 */
	class GrapheAnimation implements EbliAnimationSourceInterface {

		private Hydraulique1dProfilPane panel_;

		GrapheAnimation(Hydraulique1dProfilPane editeur) {
			panel_ = editeur;
		}

		/**
		 * @return le nombre de pas de temps a parcourir
		 */
		@Override
		public int getNbTimeStep() {
			return panel_.coTemps_.getItemCount();
		}

		/**
		 * @param _idx indice du pas de temps [0;getNbTimeStep()[
		 * @return representation de ce pas de temps
		 */
		@Override
		public String getTimeStep(int _idx) {
			return panel_.coTemps_.getItemAt(_idx).toString();
		}

		@Override
		public double getTimeStepValueSec(int _idx) {
			try {
				return Double.parseDouble(getTimeStep(_idx));
			} catch (NumberFormatException numberFormatException) {
			}
			return 0;
		}

		/**
		 * @param _idx l'indice a afficher
		 */
		@Override
		public void setTimeStep(int _idx) {
			panel_.coTemps_.setSelectedIndex(_idx);
		}

		@Override
		public String getTitle() {
			return panel_.getTitle();
		}

		/**
		 * Permet d'avertir le client que la video est en cours
		 *
		 * @param _b true si video en cours
		 */
		@Override
		public void setVideoMode(boolean _b) {
		}

		/**
		 * @return le composant contenant l'affichage (optionnel)
		 */
		@Override
		public Component getComponent() {
			return null;

		}

		/**
		 * @return l'image produite dans la taille courant du composant.
		 */
		@Override
		public BufferedImage produceImage(Map _params) {
			return panel_.produceImage(null);
		}

		@Override
		public BufferedImage produceImage(int _w, int _h, Map _params) {
			return panel_.produceImage(_w, _h, null);
		}

		/**
		 * @return les dimensions de l'image produite
		 */
		@Override
		public Dimension getDefaultImageDimension() {
			return panel_.graphe_.getSize();
		}
	}

	class TpsComboboxModel implements ComboBoxModel  {
		HashSet<ListDataListener> hlisteners=new HashSet<ListDataListener>();
		Object selected;

		@Override
		public int getSize() {
			double[] valPas=etude_.resultatsGeneraux().resultatsTemporelSpatial().valPas();
			return valPas==null ? 0:valPas.length;
		}

		@Override
		public Object getElementAt(int index) {
			return Double.toString(etude_.resultatsGeneraux().resultatsTemporelSpatial().valPas()[index]);
		}

		@Override
		public void addListDataListener(ListDataListener l) {
			hlisteners.add(l);
		}

		@Override
		public void removeListDataListener(ListDataListener l) {
			hlisteners.remove(l);
		}

		@Override
		public void setSelectedItem(Object anItem) {
			if (selected==null && anItem!=null || selected!=null && !selected.equals(anItem)) {
				selected=anItem;
				fireSelectionChanged();
			}
		}

		@Override
		public Object getSelectedItem() {
			return selected;
		}

		private void fireSelectionChanged() {
			ListDataEvent e = null;
			for (ListDataListener l : hlisteners) {
				if (e == null) {
					e = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, -1, -1);
				}
				l.contentsChanged(e);
			}
		}
	}  

	/** Mode d'affichage */
	public static final int EDITER=0x01;
	public static final int VOIR=0x02;

	/** Mode d'affichage de la partie edition */
	public static final int PROFILS=Hydraulique1dResource.PROFIL.PROFILS;
	public static final int CALAGE=Hydraulique1dResource.PROFIL.CALAGE;

	private static final int NB_OPERATIONS_MODIF=10;

	/** Pour avancer/reculer dans les profils */
	private static final int PROFIL_RAPIDE=Hydraulique1dPreferences.HYDRAULIQUE1D.getIntegerProperty("profil.rapide", 5);
	/** Tous les profils du bief */
	protected Hydraulique1dProfilModel[] profilsBief_;
	/** L'indice du profil courant dans la table des profils. */
	private int iprofilCourant_=0;
	/** Les profils pour le undo/redo */
	private Vector<Hydraulique1dProfilModel> voldProfils_=new Vector<Hydraulique1dProfilModel>();
	/** L'index sur les profils modifi�s */
	private int ioldProfils_=0;

	protected Hydraulique1dGrapheProfil graphe_;
	protected Hydraulique1dProfilTableauPanel pnTableau_;
	private JLabel lbTitle_;

	protected int modeEdition_;
	private int mode2_;
	private boolean isTranscritique_=true;

	private JToggleButton btSelect_=new JToggleButton();
	private JButton btRestaurer_=new JButton();
	private JToggleButton btZoom_=new JToggleButton();
	private JToggleButton btPan_=new JToggleButton();
	private JButton btCreer_=new JButton();
	private JButton btDetruire_=new JButton();
	private JButton btInverser_=new JButton();
	private JButton btFiltrer_=new JButton();
	private JButton btLisser_=new JButton();
	private JButton btImporter_=new JButton();
	private JButton btTranslater_=new JButton();
	private JToggleButton btVoirSuivantPrecedent_=new JToggleButton();
	private JButton btAlignement_=new JButton();
	private JToggleButton btStockGauche_=new JToggleButton();
	private JToggleButton btRiveGauche_=new JToggleButton();
	private JToggleButton btRiveDroite_=new JToggleButton();
	private JToggleButton btStockDroit_=new JToggleButton();
	private JToggleButton btVoirRives_=new JToggleButton();
	private JButton btFixerBornes_=new JButton();
	private JToggleButton btAjusterBornes_=new JToggleButton();
	protected JComboBox coTemps_=new JComboBox();
	private JToggleButton btCoteMinMax_=new JToggleButton();
	private BuCheckBox displayGrid = new BuCheckBox(Hydraulique1dResource.getS("Afficher la Grille")) ;

	protected JComboBox coTracer_=new JComboBox();


	/** Fenetre de lissage */
	private Hydraulique1dLissageProfilEditor fnLissage_=null;
	/** Fenetre de filtrage */
	private Hydraulique1dFiltrageProfilEditor fnFiltrage_=null;
	/** Fenetre de bornes fixes */
	private Hydraulique1dBornesGrapheEditor fnBornes_=null;
	/** Fenetre d'insertion de profil */
	private Hydraulique1dInsererProfilEditor fnInserer_=null;
	/** Fenetre de translation de points de profil */
	private Hydraulique1dTranslaterProfilEditor fnTranslater_=null;
	/** Fenetre d'alignmenet des profils */
	private CtuluDialog fnAlignOption_=null;

	protected MetierEtude1d etude_;
	protected ListModel mdlBief_;
	protected BuList lsBief_;

	//protected MetierResultatsTemporelSpatial resultTracer;


	/**
	 * Contructeur minimal
	 * @param mode EDITER (edition de profils) ou VOIR (r�sultats)
	 * @param mode2 PROFILS ou CALAGE
	 */
	public Hydraulique1dProfilPane(int mode, int mode2) {
		modeEdition_=mode;
		mode2_=mode2;
		init();
	}


	protected void initGraphe() {
		graphe_=new Hydraulique1dGrapheProfil(new Hydraulique1dGrapheProfilDataset(),true, modeEdition_);
		graphe_.setBiefProfilModels(profilsBief_);
		graphe_.setPreferredSize(new Dimension(400, 400));
	}


	protected void setLabelGraphe(JPanel pnRight) {
		pnRight.add(lbTitle_,BorderLayout.NORTH);
	}

	private void init() {
		setLayout(new BorderLayout());

		profilsBief_=new Hydraulique1dProfilModel[]{new Hydraulique1dProfilModel()};

		lbTitle_=new JLabel();
		lbTitle_.setHorizontalAlignment(JLabel.LEFT);
		lbTitle_.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));

		initGraphe();

		JPanel pnRight=new JPanel();
		pnRight.setLayout(new BorderLayout());
		setLabelGraphe(pnRight);
		pnRight.add(graphe_,BorderLayout.CENTER);

		JSplitPane spt=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, buildLeftPane(), pnRight);
		spt.setOneTouchExpandable(true);
		add(BorderLayout.CENTER, spt);
		add(BorderLayout.EAST, buildRightPanel());

		add(BorderLayout.NORTH,buildToolBar());

		updateState();
	
	}



	/**
	 * Right panel only used for water quality
	 * @return
	 */
	protected Component buildRightPanel() {

		JPanel panel = new JPanel(new BorderLayout());
		/*
		if(CGlobal.AVEC_QUALITE_DEAU) {
			panel.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.getS("Concentrations")));
			//-- add combo for  tracers --//
			JPanel panelTracer = new JPanel(new BorderLayout());
			//panelTracer.add(displayGrid,BorderLayout.NORTH);
			JPanel panelCbo = new JPanel(new FlowLayout(FlowLayout.LEFT));
			panelCbo.add(coTracer_);
			panelTracer.add(panelCbo,BorderLayout.NORTH);
			panel.add(panelTracer,BorderLayout.CENTER);
			coTracer_.setEnabled(CGlobal.AVEC_QUALITE_DEAU);

			//-- ajout de la l�gende --//
			legendeColoration = new JPanel(new BorderLayout());
			panelTracer.add(legendeColoration,BorderLayout.CENTER);
			//legendeColoration.setVisible(false);
		}*/
		return panel;
	}


	protected JPanel buildLeftPane() {
		JPanel pn=new JPanel();
		pn.setLayout(new BorderLayout());

		pnTableau_=new Hydraulique1dProfilTableauPanel();
		pnTableau_.setEditable(modeEdition_==EDITER);
		pnTableau_.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.getS("Points du profil")));
		
		
		JPanel panelTab = new JPanel(new BorderLayout());
		
		panelTab.add(pnTableau_,BorderLayout.CENTER);
		panelTab.add(displayGrid,BorderLayout.SOUTH);
		
		
		
		
		if (modeEdition_==VOIR) {
			lsBief_ = new BuList();
			lsBief_.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
			lsBief_.addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					changeBiefsSelection();
				}
			});
			BuScrollPane spBiefs_= new BuScrollPane(lsBief_);
			spBiefs_.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.getS("Biefs")));
			spBiefs_.setPreferredHeight(70);

			pn.add(spBiefs_,BorderLayout.NORTH);
		}

		displayGrid.setSelected(true);
		displayGrid.addActionListener(this);

		
	
		
		
		//pn.add(displayGrid,BorderLayout.SOUTH);

		//-- add combo for  tracers --//
		JPanel panelTracer = new JPanel(new BorderLayout());		
		if(CGlobal.AVEC_QUALITE_DEAU) {
			coTracer_.setEnabled(CGlobal.AVEC_QUALITE_DEAU);
			panelTracer.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.getS("Concentrations (kg/m3)")));
			JPanel panelCOmboTracer = new JPanel();
			panelCOmboTracer.add(coTracer_);
			panelTracer.add(panelCOmboTracer,BorderLayout.NORTH);
			
			//-- ajout de la l�gende --//
			legendeColoration = new JPanel(new BorderLayout());
			panelTracer.add(legendeColoration,BorderLayout.CENTER);
					
			 contentConcentration = new JPanel(new BorderLayout());
			contentConcentration.add(new JLabel(
					Hydraulique1dResource.HYDRAULIQUE1D.getString("R�sultats qualit� d'eau")),
					BorderLayout.NORTH);			
			contentConcentration.add(panelTracer,BorderLayout.CENTER);
			JSplitPane paneSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,panelTab,contentConcentration );
			pn.add(paneSplit,BorderLayout.CENTER);
			paneSplit.setDividerLocation(0.6);
			paneSplit.setResizeWeight(0.5);			
		
		}else {
			panelTracer.setVisible(false);
			//-- on affiche pas le panel des concentrations --//
			pn.add(panelTab,BorderLayout.CENTER);
		}
		
		
		
		
		
		
		return pn;
	}
	protected JPanel contentConcentration ;
	
	public void setEtude(MetierEtude1d _etude) {
		etude_=_etude;
		isTranscritique_=etude_.paramGeneraux().regime().value() == EnumMetierRegime._TRANSCRITIQUE;


		mdlBief_=new AbstractListModel() {

			@Override
			public int getSize() {
				return etude_.reseau().biefs().length;
			}

			@Override
			public Object getElementAt(int index) {
				return Hydraulique1dResource.getS("Bief")+" "+(index+1);
			}
		};

		lsBief_.setModel(mdlBief_);
		coTemps_.setModel(new TpsComboboxModel());

		lsBief_.setSelectedIndex(0);
		coTemps_.setSelectedIndex(0);

		if(CGlobal.AVEC_QUALITE_DEAU) {
			
			initTracerList();
		}

	}


	public static class TracerItemCombo{
		public MetierDescriptionVariable var;
		public TracerItemCombo(MetierDescriptionVariable var) {
			this.var=var;
		}
		@Override
		public String toString() {
			String desc = var.description();
			if(desc != null)
			return Hydraulique1dResource.HYDRAULIQUE1D.getString(var.description());
			else return null;
		}
		
		
		
	}
	
	/**
	 * Methode qui initialise les tracers dans la liste et la m�canique des donn�es 
	 * @author Adrien Hadoux
	 */
	protected void initTracerList() {

		List<TracerItemCombo> lstr = new ArrayList<TracerItemCombo>();
		if(getResultTracer() != null && getResultTracer().descriptionVariables()!= null){
			for(MetierDescriptionVariable dv : getResultTracer().descriptionVariables()) {
				if(dv.description().contains("Concentration")) {
					lstr.add(new TracerItemCombo(dv));
				}
			}
		}
		
		if(lstr.size()>0) {
		lstr.add(0,null);
		coTracer_.setModel(new DefaultComboBoxModel(lstr.toArray()));

		coTracer_.setEnabled(CGlobal.AVEC_QUALITE_DEAU);
		if(CGlobal.AVEC_QUALITE_DEAU) {
			coTracer_.addItemListener(new ItemListener() {				

				public void itemStateChanged(ItemEvent e) {
					computeTracerValues();

				}
			});
			
		}
		}else {
			if(contentConcentration != null)
				contentConcentration.setVisible(false);
		}

	}

	public boolean isTracerComputation(){
		return CGlobal.AVEC_QUALITE_DEAU && coTracer_.isEnabled() && coTracer_.getSelectedIndex() != -1 && coTracer_.getSelectedIndex() !=0;
	}
	
	
	
	public int getBiefCourant(){
		return lsBief_.getSelectedIndex();
	}
	/**
	 * Methode qui calcule la concentration en traceur pour le bief, pas de temps et tracer selectionn�.
	 * Des min et max sont calcul�s afin d'�tablir une �chelle de valeur repr�sent� par une l�gende.
	 */
	protected void computeTracerValues() {
		
		legendeColoration.removeAll();
		legendeColoration.setVisible(false);
		
		int selectiontracer = coTracer_.getSelectedIndex();
		if(selectiontracer ==0 || selectiontracer == -1){
			currentConcentrations = null;
			return;
		}

		//selectiontracer = selectiontracer -1;

		int sbief = getBiefCourant();
		int pasTemps = coTemps_.getSelectedIndex();

		//-- attention! l'indice de la variable doit correspondre � l'indice parmi toutes les variables et non a l'indice de la combo list --//
		int trueIndiceVariable =-1;
		for(int i=0;i<getResultTracer().descriptionVariables().length;i++) {
			MetierDescriptionVariable var = getResultTracer().descriptionVariables()[i];
			MetierDescriptionVariable selectrac = ((TracerItemCombo) coTracer_.getModel().getElementAt(selectiontracer)).var;
			if(var == selectrac)
				trueIndiceVariable =i;
		}
		if(trueIndiceVariable == -1 || sbief == -1 || pasTemps == -1
				|| getResultTracer().resultatsBiefs().length<=sbief || getResultTracer().resultatsBiefs()[sbief].valeursVariables().length
				<=trueIndiceVariable || getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable).length
				<=pasTemps) {
			System.out.println("la concentration ne sera pas affich�e: Hydraulique1dProfilLongPane.computeTracerValues():"
					+ "sbief="+sbief+ " pasTemps="+pasTemps+" indiceVar="+trueIndiceVariable
					);
			/*+"et les tailles sont: sbief="+getResultTracer().resultatsBiefs().length+ " pasTemps="
					+getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable).length+
					" indiceVar="+getResultTracer().resultatsBiefs()[sbief].valeursVariables().length);
			*/
			currentConcentrations = null;
			updateConcentrationForRenderer();
			return;
		}
		
		currentConcentrations = getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable)[pasTemps];

		if(currentConcentrations == null || currentConcentrations.length ==0){
			currentConcentrations = null;
			return;
		}

		//-- find min, max of this current Concentrations --//
		minC = currentConcentrations[0];
		maxC  = currentConcentrations[0];
		
		for(int pdt=0;pdt<getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable).length;pdt++) {
			double[] crt = getResultTracer().resultatsBiefs()[sbief].valeursVariable(trueIndiceVariable)[pdt];
			for(int i=0;i<crt.length;i++){
				if(crt[i]>maxC)
					maxC = crt[i];
				if(crt[i]<minC)
					minC = crt[i];
			}
		}
		//-- si toutes les valeurs sont identiques --//
		if(minC == maxC)
			numberOFDifferentsConcentrations =1;
		else  if(numberOFDifferentsConcentrations == 1) {
			numberOFDifferentsConcentrations=DEFAULT_NB_CONCENTRATION;
		}

		colorConcentrations = new Color[numberOFDifferentsConcentrations];
		//colorConcentrations[0] = new Color(132,166,188);
		colorConcentrations[0] = DEFAULT_COLOR_RANGE[0];
		int cptColor =0;
		for(int i=1;i<numberOFDifferentsConcentrations;i++) {
			if(i%3 ==0 && cptColor < DEFAULT_COLOR_RANGE.length-1){
				cptColor++;
				colorConcentrations[i] =DEFAULT_COLOR_RANGE[cptColor];
			}else
				if(i<3 || (i>=6 &&i<9) )
				colorConcentrations[i] =colorConcentrations[i-1].brighter();
				else
					colorConcentrations[i] =colorConcentrations[i-1].darker();
		}

		//-- creation de la legende --//
		/*
		Box legende = Box.createVerticalBox();
		
		
		legende.setBackground(Color.white);
		JPanel content;
		JLabel colorLabel, textLabel;
		DecimalFormat legendFormatter = new DecimalFormat("0.0000");
		double var =  (maxC - minC)/colorConcentrations.length;	
		for(int i=0;i<numberOFDifferentsConcentrations;i++) {
			content = new JPanel(new FlowLayout(FlowLayout.LEFT));
			content.setBackground(Color.white);
			colorLabel = new JLabel("  ");
			colorLabel.setOpaque(true);
			colorLabel.setBackground(colorConcentrations[i]);
			colorLabel.setPreferredSize(new Dimension(16,16));
			content.add(colorLabel);
			textLabel= new JLabel( legendFormatter.format(minC + (i)*var) +"< X "+"< " + legendFormatter.format(minC + (i+1)*var));
			content.add(textLabel);
			legende.add(content);
		}
		*/
		JPanel legende = new JPanel(new GridLayout(numberOFDifferentsConcentrations,2,0,0)){
			
			public void paintComponent(Graphics g) {
		        super.paintComponent(g);
	      
		        
		        DecimalFormat legendFormatter = new DecimalFormat("0.0000");
				double var =  (maxC - minC)/colorConcentrations.length;	
				int x=10,y=10, width = 32, height=32;
				for(int i=numberOFDifferentsConcentrations-1;i>=0;i--) {
					g.setColor(colorConcentrations[i]);
					g.fillRect(x, y, width, height);
					g.setColor(Color.black);
					g.drawLine(x+26, y+32, x+31, y+32);
					g.drawLine(11, y+32, 16, y+32);
					//if(y !=10) {
						g.drawLine(x+26, y, x+31, y);
						g.drawLine(11, y, 16, y);
				//	}
					g.drawString(legendFormatter.format(minC + (i)*var), x +35, y+35);
					if(i == (numberOFDifferentsConcentrations-1)) {
						g.drawString(legendFormatter.format(minC + (i+1)*var), x +35, y+5);					
					}
					y =y +32;
				}
		        
		    }
			};
			legende.setBackground(Color.white);
		
		legende.setPreferredSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		legende.setSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		legende.setMaximumSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		legendeColoration.removeAll();
		
		legendeColoration.setLayout(new BorderLayout());
		legendeColoration.add(new JScrollPane(legende), BorderLayout.CENTER);
		legendeColoration.setVisible(true);

		legendeColoration.setPreferredSize(new Dimension(200,25*numberOFDifferentsConcentrations));
		legendeColoration.setSize(new Dimension(200,25*numberOFDifferentsConcentrations));
		legendeColoration.setMaximumSize(new Dimension(200,25*numberOFDifferentsConcentrations));
		legendeColoration.setBorder(BorderFactory.createTitledBorder(Hydraulique1dResource.HYDRAULIQUE1D.getString("L�gende")));
		updateConcentrationForRenderer();
		
		//TO BE REMOVED
		//constructJfreechartLegend();
		
		
		graphe_.updateGraphe();
	}

	
	public  int findSectionBiefCloseToProfilAbscisse(Hydraulique1dProfilModel modelDisplayed){
		int bief = getBiefCourant();
		double abscisse = modelDisplayed.getAbscisse();
		
		for(int i=0;i<getResultTracer().resultatsBiefs()[bief].abscissesSections().length;i++) {
			/*if(getResultTracer().resultatsBiefs()[bief].abscissesSections()[i]==abscisse ) 
				return i;
				else*/
			if(getResultTracer().resultatsBiefs()[bief].abscissesSections()[i]>abscisse ) {
				return i!=0?i-1:0;
			}
				
				
		}
		return getResultTracer().resultatsBiefs()[bief].abscissesSections().length-1;
	}

	public void updateConcentrationForRenderer () {
		//-- envoies les infos au renderer du profil --//
		MascaretXYAreaRenderer renderer = graphe_.getDefaultRenderer();
		if(renderer != null) {
			if(currentConcentrations == null){
				renderer.setDefaultColor();
				return;
			}
			
			int indiceProfil = findSectionBiefCloseToProfilAbscisse(profilsBief_[getProfilCourant()]);
			double concentration = currentConcentrations[indiceProfil];
			Color colorConcentration = getColorForConcentrationValue(concentration, colorConcentrations, minC, maxC);
			renderer.changeColorConcentration(colorConcentration);
			
		}
	}

	/**
	 * Nombre de colorations diff�rentes pour les concentrations.
	 */
	protected static int DEFAULT_NB_CONCENTRATION = 10;
	protected int numberOFDifferentsConcentrations=DEFAULT_NB_CONCENTRATION;
	protected double[] currentConcentrations;
	protected double minC , maxC;  
	protected static Color[] DEFAULT_COLOR_RANGE = new Color[]{ new Color(31,120,188).darker(), Color.GREEN, new Color(251,208,151).darker().darker(),Color.RED.brighter().brighter().brighter().brighter()};
	protected Color[] colorConcentrations ;
	protected JPanel legendeColoration = new JPanel();

	/**
	 * To know what color to display for the concentration.
	 * @param value
	 * @param colorConcentrations
	 * @param minC
	 * @param maxC
	 * @return
	 */
	public static Color getColorForConcentrationValue(double value, Color[] colorConcentrations,double minC , double maxC){
		double var =  (maxC - minC)/colorConcentrations.length;		
		for(int i=0;i<colorConcentrations.length;i++) {
			if(value < minC + i*var)
				return colorConcentrations[i-1];
		}		
		return colorConcentrations[colorConcentrations.length-1];
	}
	
	public static int getIndiceColorForConcentrationValue(double value, Color[] colorConcentrations,double minC , double maxC){
		double var =  (maxC - minC)/colorConcentrations.length;		
		for(int i=0;i<colorConcentrations.length;i++) {
			if(value < minC + i*var)
				return i-1;
		}		
		return colorConcentrations.length-1;
	}


	private void changeTempsSelection(int _itps) {
		if (_itps==-1) return;

		int[] ibiefs=lsBief_.getSelectedIndices();

		MetierReseau res = etude_.reseau();
		int icpt=0;
		for (int i = 0; i < ibiefs.length; i++) {
			MetierBief b = res.biefs()[ibiefs[i]];
			int nbProfil = b.profils().length;
			for (int iprof = 0; iprof < nbProfil; iprof++) {
				Hydraulique1dProfilModel pModel= profilsBief_[icpt++];
				double z=calculeCoteZProfil(ibiefs[i], iprof,_itps);
				pModel.setNiveauEau(z);
			}
		}
		
		if(isTracerComputation()) {
			computeTracerValues();
		}
		

		updateTpsGraphe();

	}

	protected void updateTpsGraphe() {
		graphe_.updateGraphe();
	}

	protected void changeBiefsSelection() {
		int[] ibiefs=lsBief_.getSelectedIndices();
		if (ibiefs.length==0) return;

		// initialisation les modeles de profils
		ArrayList<Hydraulique1dProfilModel> listeProfils_ = new ArrayList<Hydraulique1dProfilModel>();

		MetierReseau res = etude_.reseau();
		for (int i = 0; i < ibiefs.length; i++) {
			MetierBief b = res.biefs()[ibiefs[i]];
			int nbProfil = b.profils().length;
			for (int iprof = 0; iprof < nbProfil; iprof++) {
				Hydraulique1dProfilModel pModel= new Hydraulique1dProfilModel(b.profils()[iprof]);
				initCoteMinMaxProfil(pModel, b, ibiefs[i], iprof);
				listeProfils_.add(pModel);
			}
		}

		setProfilsModeleBief(listeProfils_.toArray(new Hydraulique1dProfilModel[listeProfils_.size()]));

		changeTempsSelection(coTemps_.getSelectedIndex());
	}

	/**
	 * Initialise sur les modeles de profil la cote min/max pour tous les pas de temps.
	 * @param _prof Le model de profil
	 * @param _bief Le bief
	 * @param _ibief L'indice de bief dans le reseau.
	 * @param _iprofil L'indice de profil dans le bief.
	 */
	private void initCoteMinMaxProfil(Hydraulique1dProfilModel _prof, MetierBief _bief, int _ibief, int _iprofil) {
		try {
			MetierResultatsTemporelSpatial resTempo=etude_.resultatsGeneraux().resultatsTemporelSpatial();

			TDoubleArrayList listeCotes=new TDoubleArrayList(resTempo.valPas().length);
			for (int itps=0; itps<resTempo.valPas().length; itps++) {
				listeCotes.add(calculeCoteZProfil(_ibief, _iprofil, itps));
			}

			_prof.setNiveauEauMin(listeCotes.min());
			_prof.setNiveauEauMax(listeCotes.max());
		}
		catch (Exception ex) {
			ex.printStackTrace();
			_prof.setNiveauEauMin(Double.NaN);
			_prof.setNiveauEauMax(Double.NaN);
		}
	}

	/**
	 * Calcul de la cote Z pour le profil donn�. Peut �tre interpol� entre 2 sections.
	 * @param _ibief L'indice du bief dans le reseau.
	 * @param _iprofil L'indice du profil dans le bief.
	 * @param _itps L'indice du pas de temps dans les r�sultats.
	 * @return Le Z pour le profil, ou NaN si une erreur de calcul s'est produite pour une raison quelconque.
	 */
	protected double calculeCoteZProfil(int _ibief, int _iprofil, int _itps) {
		MetierBief bief=etude_.reseau().biefs()[_ibief];
		MetierResultatsTemporelSpatial res=etude_.resultatsGeneraux().resultatsTemporelSpatial();
		MetierResultatsTemporelSpatialBief resBief=res.resultatsBiefs()[_ibief];
		int indiceVarCoteEau = res.getIndiceVariable("Z");

		// extraction de l'abscisse de d�but du r�sultat du bief
		double[] sectionRes = resBief.abscissesSections();

		double xDebutRes = sectionRes[0];
		// calcul du d�calage �ventuelle des abscisses entre r�sultat et
		// g�om�trie.
		double diffAbscisse = xDebutRes - bief.extrAmont().profilRattache().abscisse();
		double abscProfilCorrige = bief.profils()[_iprofil].abscisse() + diffAbscisse;
		int ifound = Arrays.binarySearch(sectionRes, abscProfilCorrige);
		if (ifound >= 0) {
			double[] cotes = resBief.valeursVariables()[indiceVarCoteEau][_itps];
			return cotes[ifound];
		}
		else {
			// Le nombre de sections de calcul peut parfois etre inferieur a 2.
			if (sectionRes.length<2)
				return Double.NaN;

			int iResSup = -ifound - 1;
			if (iResSup >= sectionRes.length) {
				iResSup = sectionRes.length - 1;
			}
			int iResInf = iResSup - 1;
			double x2 = sectionRes[iResSup];
			double x1 = sectionRes[iResInf];

			double[] cotes = resBief.valeursVariables()[indiceVarCoteEau][_itps];
			double y1 = cotes[iResInf];
			double y2 = cotes[iResSup];

			if (x1==abscProfilCorrige)
				return y1;
			else if (x2==abscProfilCorrige)
				return y2;
			else if (x1==x2)
				throw new IllegalArgumentException("interpolation impossible");
			else
				return  ((y1-y2)*(abscProfilCorrige-x2)/(x1-x2))+y2;
		}
	}

	/**
	 * Met a jour la fenetre en fonction de sont etat courant.
	 */
	public void updateState() {
		int[] isels=profilsBief_[iprofilCourant_].getSelected();
		int[] iselNonNuls=profilsBief_[iprofilCourant_].getSelectedNonNuls();

		btDetruire_.setEnabled(isels.length!=0);
		btTranslater_.setEnabled(iselNonNuls.length!=0);
		btStockGauche_.setEnabled(isels.length==1 && iselNonNuls.length==1 && !isTranscritique_);
		btRiveGauche_.setEnabled(isels.length==1 && iselNonNuls.length==1);
		btRiveDroite_.setEnabled(isels.length==1 && iselNonNuls.length==1);
		btStockDroit_.setEnabled(isels.length==1 && iselNonNuls.length==1 && !isTranscritique_);

		if (isels.length==1 && iselNonNuls.length==1) {
			btStockGauche_.setSelected(profilsBief_[iprofilCourant_].getIndiceLitMajGa()==iselNonNuls[0]);
			btRiveGauche_.setSelected(profilsBief_[iprofilCourant_].getIndiceLitMinGa()==iselNonNuls[0]);
			btRiveDroite_.setSelected(profilsBief_[iprofilCourant_].getIndiceLitMinDr()==iselNonNuls[0]);
			btStockDroit_.setSelected(profilsBief_[iprofilCourant_].getIndiceLitMajDr()==iselNonNuls[0]);
		}
		else {
			btStockGauche_.setSelected(false);
			btRiveGauche_.setSelected(false);
			btRiveDroite_.setSelected(false);
			btStockDroit_.setSelected(false);
		}
		//
		//    btAvancer_.setEnabled(iprofilCourant_!=profilsBief_.length-1);
		//    btAvancerVite_.setEnabled(iprofilCourant_!=profilsBief_.length-1);
		//    btReculer_.setEnabled(iprofilCourant_!=0);
		//    btReculerVite_.setEnabled(iprofilCourant_!=0);
	}


	protected BuPanel buildToolBar() {
		return buildToolBar(true);
	}

	/**
	 * Construction de la barre d'outils.
	 * @return La barre d'outils.
	 */
	protected BuPanel buildToolBar(boolean profilTravers) {
		BuPanel pn=new BuPanel();
		pn.setLayout(new FlowLayout(FlowLayout.LEFT,0,3));
		pn.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));

		ButtonGroup bg=new ButtonGroup();

		if (modeEdition_==EDITER) {
			btSelect_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icselect.gif"));
			btSelect_.setToolTipText(Hydraulique1dResource.getS("S�lectionner des points"));
			btSelect_.setMargin(new Insets(1,1,1,1));
			btSelect_.setSelected(graphe_.getModeInteractif()==Hydraulique1dGrapheProfil.MODE_INTERACTIF_SELECTION);
			btSelect_.addActionListener(this);
			bg.add(btSelect_);
			pn.add(btSelect_);

			pn.add(Box.createHorizontalStrut(5));
		}

		btRestaurer_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icloupeetendue.gif"));
		btRestaurer_.setToolTipText(Hydraulique1dResource.getS("Restaurer"));
		btRestaurer_.setMargin(new Insets(1,1,1,1));
		btRestaurer_.addActionListener(this);
		pn.add(btRestaurer_);

		btZoom_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icloupe.gif"));
		btZoom_.setToolTipText(Hydraulique1dResource.getS("Zoom"));
		btZoom_.setMargin(new Insets(1,1,1,1));
		btZoom_.setSelected(graphe_.getModeInteractif()==Hydraulique1dGrapheProfil.MODE_INTERACTIF_ZOOM);
		btZoom_.addActionListener(this);
		bg.add(btZoom_);
		pn.add(btZoom_);

		btPan_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icdeplacer.gif"));
		btPan_.setToolTipText(Hydraulique1dResource.getS("D�placer la vue"));
		btPan_.setMargin(new Insets(1,1,1,1));
		btPan_.setSelected(graphe_.getModeInteractif()==Hydraulique1dGrapheProfil.MODE_INTERACTIF_PAN);
		btPan_.addActionListener(this);
		bg.add(btPan_);
		pn.add(btPan_);

		pn.add(Box.createHorizontalStrut(5));

		if (modeEdition_==EDITER) {
			btCreer_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("iccreer.gif"));
			btCreer_.setToolTipText(Hydraulique1dResource.getS("Cr�er un point vide apr�s celui s�lectionn�"));
			btCreer_.setMargin(new Insets(1,1,1,1));
			btCreer_.addActionListener(this);
			pn.add(btCreer_);

			btDetruire_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icsupprimer.gif"));
			btDetruire_.setToolTipText(Hydraulique1dResource.getS("Supprimer les points s�lectionn�s"));
			btDetruire_.setMargin(new Insets(1,1,1,1));
			btDetruire_.addActionListener(this);
			pn.add(btDetruire_);

			pn.add(Box.createHorizontalStrut(5));

			btInverser_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icinverser.gif"));
			btInverser_.setToolTipText(Hydraulique1dResource.getS("Inverser le profil"));
			btInverser_.setMargin(new Insets(1,1,1,1));
			btInverser_.addActionListener(this);
			pn.add(btInverser_);

			btFiltrer_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icpointsanguleux.png"));
			btFiltrer_.setToolTipText(Hydraulique1dResource.getS("Filtrer les points"));
			btFiltrer_.setMargin(new Insets(1,1,1,1));
			btFiltrer_.addActionListener(this);
			pn.add(btFiltrer_);

			btLisser_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("iclissage.png"));
			btLisser_.setToolTipText(Hydraulique1dResource.getS("Lisser les points par moyenne")+"./"+Hydraulique1dResource.getS("mediane glissante"));
			btLisser_.setMargin(new Insets(1,1,1,1));
			btLisser_.addActionListener(this);
			pn.add(btLisser_);

			btImporter_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icimportprofil.gif"));
			btImporter_.setToolTipText(Hydraulique1dResource.getS("Importer un profil"));
			btImporter_.setMargin(new Insets(1,1,1,1));
			btImporter_.addActionListener(this);
			pn.add(btImporter_);

			btTranslater_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("ictranslater.png"));
			btTranslater_.setToolTipText(Hydraulique1dResource.getS("Translater les points s�lectionn�s"));
			btTranslater_.setMargin(new Insets(1,1,1,1));
			btTranslater_.addActionListener(this);
			pn.add(btTranslater_);

			pn.add(Box.createHorizontalStrut(5));
		}

		if (modeEdition_==EDITER) {
			btStockGauche_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icstockagegauche.png"));
			btStockGauche_.setToolTipText(Hydraulique1dResource.getS("Positionner la limite de zone de stockage gauche"));
			btStockGauche_.setMargin(new Insets(1, 1, 1, 1));
			btStockGauche_.addActionListener(this);
			pn.add(btStockGauche_);

			if (mode2_==PROFILS) {
				btRiveGauche_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icrivegauche.png"));
				btRiveGauche_.setToolTipText(Hydraulique1dResource.getS("Positionner la limite de zone de rive gauche"));
				btRiveGauche_.setMargin(new Insets(1, 1, 1, 1));
				btRiveGauche_.addActionListener(this);
				pn.add(btRiveGauche_);

				btRiveDroite_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icrivedroite.png"));
				btRiveDroite_.setToolTipText(Hydraulique1dResource.getS("Positionner la limite de zone de rive droite"));
				btRiveDroite_.setMargin(new Insets(1, 1, 1, 1));
				btRiveDroite_.addActionListener(this);
				pn.add(btRiveDroite_);
			}

			btStockDroit_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icstockagedroit.png"));
			btStockDroit_.setToolTipText(Hydraulique1dResource.getS("Positionner la limite de zone de stockage droite"));
			btStockDroit_.setMargin(new Insets(1, 1, 1, 1));
			btStockDroit_.addActionListener(this);
			pn.add(btStockDroit_);

			pn.add(Box.createHorizontalStrut(5));
		}

		// Bouton d'alignement.
		//    btAlignement_ = new EbliActionPaletteAbstract(Hydraulique1dResource.getS("Alignement des profils"), Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icalignerprofils"), "ALIGNMENT") {
		//      @Override
		//      protected JComponent buildContentPane() {
		//        return new CurveOptionPane(Hydraulique1dProfilEditor.this);
		//      }
		//    };
		//    pn.add(btAlignement_.buildToolButton(EbliComponentFactory.INSTANCE));
		if(profilTravers) {
			btAlignement_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icalignerprofils.gif"));
			btAlignement_.setToolTipText(Hydraulique1dResource.getS("Aligner les profils"));
			btAlignement_.setMargin(new Insets(1,1,1,1));
			btAlignement_.addActionListener(this);
			pn.add(btAlignement_);

			btVoirSuivantPrecedent_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icmultiprofils.gif"));
			btVoirSuivantPrecedent_.setToolTipText(Hydraulique1dResource.getS("Visualiser les profils pr�c�dent/suivant"));
			btVoirSuivantPrecedent_.setMargin(new Insets(1,1,1,1));
			btVoirSuivantPrecedent_.addActionListener(this);
			pn.add(btVoirSuivantPrecedent_);

			btVoirRives_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icafficherrives.gif"));
			btVoirRives_.setToolTipText(Hydraulique1dResource.getS("Voir les rives"));
			btVoirRives_.setMargin(new Insets(1,1,1,1));
			btVoirRives_.addActionListener(this);
			btVoirRives_.setSelected(graphe_.isRivesVisible());
			pn.add(btVoirRives_);

			btFixerBornes_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icfixerbornes.gif"));
			btFixerBornes_.setToolTipText(Hydraulique1dResource.getS("Fixer les bornes du graphique"));
			btFixerBornes_.setMargin(new Insets(1,1,1,1));
			btFixerBornes_.addActionListener(this);
			pn.add(btFixerBornes_);

			btAjusterBornes_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icajusterbornes.gif"));
			btAjusterBornes_.setToolTipText(Hydraulique1dResource.getS("Ajuster le graphique suivant la cote"));
			btAjusterBornes_.setMargin(new Insets(1,1,1,1));
			btAjusterBornes_.addActionListener(this);
			btAjusterBornes_.setSelected(!graphe_.isRatioXY1());
			pn.add(btAjusterBornes_);
		}
		if (modeEdition_==VOIR) {
			if(profilTravers) {
				btCoteMinMax_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("icvoirminmax.gif"));
				btCoteMinMax_.setToolTipText(Hydraulique1dResource.getS("Voir les cotes min/max"));
				btCoteMinMax_.setMargin(new Insets(1,1,1,1));
				btCoteMinMax_.addActionListener(this);
				pn.add(btCoteMinMax_);
			}
			pn.add(Box.createHorizontalStrut(5));
			coTemps_.setToolTipText(Hydraulique1dResource.getS("Temps courant"));
			coTemps_.setPreferredSize(new Dimension(120,coTemps_.getPreferredSize().height));
			coTemps_.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange()==ItemEvent.DESELECTED) return;

					changeTempsSelection(coTemps_.getSelectedIndex());

				}
			});
			pn.add(coTemps_);

			EbliAnimationAction act=new EbliAnimationAction(new GrapheAnimation(this));
			AbstractButton btAnimation=act.buildButton(EbliComponentFactory.INSTANCE);
			btAnimation.setMargin(new Insets(1,1,1,1));
			btAnimation.setEnabled(true);
			pn.add(btAnimation);
		}

		return pn;
	}

	/**
	 * Controle l'�tat du profil avant de sortir de la fenetre.
	 * @return true Des erreurs sont apparues.
	 */
	public boolean verifieContraintes() {
		Hydraulique1dProfilModel prf=profilsBief_[iprofilCourant_];
		int[] indsNonNuls=prf.getIndicesNonNuls();
		boolean can_ignore=true;
		String alert="";
		// verif du nombre de points
		if (indsNonNuls.length<=0) {
			alert+=Hydraulique1dResource.getS("Le profil ne contient aucun point")+"\n\n";
		}
		else {
			if (mode2_==PROFILS) {
				// verification des limites du lit mineur
				if (CGlobal.egale(prf.getPoint(prf.getIndiceLitMinGa()).x(),prf.getPoint(prf.getIndiceLitMinDr()).x())) {
					alert+=Hydraulique1dResource.getS("Les rives sont confondues")+"\n\n";
				}
				else if (prf.getPoint(prf.getIndiceLitMinGa()).x()>prf.getPoint(prf.getIndiceLitMinDr()).x()) {
					alert+=Hydraulique1dResource.getS("Les rives sont invers�es")+"\n\n";
					can_ignore=false;
				}
				if (prf.getIndiceLitMinGa()==indsNonNuls[0]) {
					alert+=Hydraulique1dResource.getS("La rive gauche est fix�e au premier point du profil")+"\n\n";
				}
				if (prf.getIndiceLitMinDr()==indsNonNuls[indsNonNuls.length-1]) {
					alert+=Hydraulique1dResource.getS("La rive droite est fix�e au dernier point du profil")+"\n\n";
				}
			}
			if (!isTranscritique_&&((mode2_==CALAGE)||(mode2_==PROFILS))) {
				// verification des limites du lit majeur
				if (CGlobal.egale(prf.getPoint(prf.getIndiceLitMajGa()).x(), prf.getPoint(prf.getIndiceLitMajDr()).x())) {
					alert+=Hydraulique1dResource.getS("Les zones de stockage sont confondues")+"\n\n";
				}
				if (prf.getPoint(prf.getIndiceLitMajGa()).x()>prf.getPoint(prf.getIndiceLitMajDr()).x()) {
					alert+=Hydraulique1dResource.getS("Les zones de stockage sont invers�es")+"\n\n";
					can_ignore=false;
				}
				if (prf.getIndiceLitMajGa()==indsNonNuls[0]) {
					alert+=Hydraulique1dResource.getS("La zone stockage gauche n'existe pas")+"\n\n";
				}
				if (prf.getIndiceLitMajDr()==indsNonNuls[indsNonNuls.length-1]) {
					alert+=Hydraulique1dResource.getS("La zone stockage droite n'existe pas")+"\n\n";
				}
			}
			// verification des rives/zones stockage
			if (prf.getPoint(prf.getIndiceLitMajGa()).x()>prf.getPoint(prf.getIndiceLitMinGa()).x()) {
				alert+=Hydraulique1dResource.getS("Les zone de stockage et rive gauches sont invers�es")+"\n\n";
				can_ignore=false;
			}
			if (prf.getPoint(prf.getIndiceLitMajDr()).x()<prf.getPoint(prf.getIndiceLitMinDr()).x()) {
				alert+=Hydraulique1dResource.getS("Les zone de stockage et rive droites sont invers�es")+"\n\n";
				can_ignore=false;
			}
			if (mode2_==PROFILS) {
				// verification des parois verticales
				int[] vert=new int[indsNonNuls.length];
				int n=0;
				vert[0]=-1;
				for (int i=1; i<indsNonNuls.length; i++) {
					vert[i]=-1; // affectation par defaut
					if (prf.getPoint(indsNonNuls[i-1]).x()==prf.getPoint(indsNonNuls[i]).x()) {
						vert[n++]=i;
					}
				}
				if (vert[0]!=-1) {
					alert+=Hydraulique1dResource.getS("D�tection de parois verticales sur les points")+":\n";
				}
				for (int i=0; i<vert.length; i++) {
					if (vert[i]!=-1) {
						alert+="  "+(indsNonNuls[vert[i]-1]+1)+"-"+(indsNonNuls[vert[i]]+1)+"\n";
					}
				}
			}
		}
		if (alert.isEmpty()) {
			return true;
		}
		int resp=new Hydraulique1dDialogContraintes((BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
				((BuCommonInterface)Hydraulique1dBaseApplication.FRAME).getImplementation().getInformationsSoftware(), (Hydraulique1dResource.getS("Attention")+":\n\n"+alert), can_ignore).activate();
		if (resp==Hydraulique1dDialogContraintes.IGNORER) {
			return true;
		}
		return false;
	}

	/**
	 * Affecte le tableau des profils.
	 * @param _profils Les profils.
	 */
	public void setProfilsModeleBief(Hydraulique1dProfilModel[] _profils) {
		profilsBief_[iprofilCourant_].removeSelectionListener(this);
		profilsBief_[iprofilCourant_].removeDataListener(this);

		profilsBief_=_profils;

		iprofilCourant_=0;
		setProfilCourant(0);

		updateGrapheProfilModel(_profils);
	}

	protected void updateGrapheProfilModel(Hydraulique1dProfilModel[] _profils) {
		graphe_.setBiefProfilModels(_profils);
	}


	public Hydraulique1dProfilModel[] getProfilsModeleBief() {
		return profilsBief_;
	}

	/**
	 * Change le profil courant.
	 * @param _ind L'indice du profil courant.
	 */
	public void setProfilCourant(int _ind) {
		int indiceMax=profilsBief_.length-1;
		_ind=Math.min(_ind, indiceMax);
		_ind=Math.max(_ind, 0);

		profilsBief_[iprofilCourant_].removeSelectionListener(this);
		profilsBief_[iprofilCourant_].removeDataListener(this);

		iprofilCourant_=_ind;

		profilsBief_[iprofilCourant_].addSelectionListener(this);
		profilsBief_[iprofilCourant_].addDataListener(this);

		clearOperations();

		
		if(isTracerComputation()) {
			updateConcentrationForRenderer();
		}
		
		graphe_.setCurrentProfil(iprofilCourant_);
		if(pnTableau_ != null)
			pnTableau_.setProfilModel(profilsBief_[iprofilCourant_]);

		setTitle(Hydraulique1dResource.getS("Profil")+" "+profilsBief_[iprofilCourant_].nom()+" "+Hydraulique1dResource.getS("Abscisse")+"="+profilsBief_[iprofilCourant_].getAbscisse());

		if (isTranscritique_) {
			profilsBief_[iprofilCourant_].setIndiceLitMajGa(0);
			profilsBief_[iprofilCourant_].setIndiceLitMajDr(profilsBief_[iprofilCourant_].getListePoints().size()-1);
		}

		updateState();
	}

	public int getProfilCourant() {
		return iprofilCourant_;
	}

	public String getTitle() {
		return lbTitle_.getText();
	}

	public void setTitle(String _s) {
		lbTitle_.setText(_s);
	}

	/**
	 * Remplace le profil courant par le contenu d'un autre profil.
	 * @param _p Le profil
	 */
	private void remplaceProfilCourant(Hydraulique1dProfilModel _p) {
		profilsBief_[iprofilCourant_].removeDataListener(this);
		profilsBief_[iprofilCourant_].setPoints(_p.getPoints());
		profilsBief_[iprofilCourant_].setIndiceLitMajGa(_p.getIndiceLitMajGa());
		profilsBief_[iprofilCourant_].setIndiceLitMinGa(_p.getIndiceLitMinGa());
		profilsBief_[iprofilCourant_].setIndiceLitMinDr(_p.getIndiceLitMinDr());
		profilsBief_[iprofilCourant_].setIndiceLitMajDr(_p.getIndiceLitMajDr());
		profilsBief_[iprofilCourant_].addDataListener(this);
		graphe_.updateGraphe();
	}

	/**
	 * Affecte le profil courant.
	 * @param _profil Le profil courant.
	 */
	public void setProfilModel(Hydraulique1dProfilModel _profil) {
		for (int i=0; i<profilsBief_.length; i++) {
			if (profilsBief_[i]==_profil) {
				setProfilCourant(i);
				break;
			}
		}
	}

	/**
	 * Definit si l'�dition se fait en mode transcritique
	 * @param b true : Mode transcritique.
	 */
	public void setIsTranscritique(boolean b) {
		isTranscritique_=b;
		updateState();
	}

	//--- Interface Hydraulique1dHydraulique1dProfilSelectionListener  ---------------------------------------

	@Override
	public void pointSelectionChanged(Hydraulique1dProfilSelectionEvent _evt) {
		updateState();
	}

	//--- Interface Hydraulique1dHydraulique1dProfilDataListener  ---------------------------------------

	@Override
	public void pointModified(Hydraulique1dProfilDataEvent _evt) {
		if (!graphe_.isPointsMoving()) doOperation();
	}

	@Override
	public void pointDeleted(Hydraulique1dProfilDataEvent _evt) {
		doOperation();
	}

	@Override
	public void pointInserted(Hydraulique1dProfilDataEvent _evt) {
		doOperation();
	}

	//--- Interface BuBuUndoRedoInterface  ---------------------------------------

	/**
	 * Annule la derniere op�ration de modification.
	 */
	@Override
	public void undo() {
		if (modeEdition_!=EDITER) return;
		if (ioldProfils_==0) return;
		ioldProfils_--;

		remplaceProfilCourant(voldProfils_.get(ioldProfils_).clone());
	}

	/**
	 * Refait la derni�re op�ration de modification annul�e.
	 */
	@Override
	public void redo() {
		if (modeEdition_!=EDITER) return;
		if (ioldProfils_==voldProfils_.size()-1) return;
		ioldProfils_++;

		remplaceProfilCourant(voldProfils_.get(ioldProfils_).clone());
	}

	/**
	 * Stocke la derni�re op�ration de modification du profil.
	 */
	private void doOperation() {
		if (ioldProfils_<voldProfils_.size()-1) {
			for (int i=voldProfils_.size()-1; i>ioldProfils_; i--)
				voldProfils_.remove(i);
		}
		voldProfils_.add(profilsBief_[iprofilCourant_].clone());
		if (voldProfils_.size()>NB_OPERATIONS_MODIF+1) voldProfils_.remove(0);

		ioldProfils_=voldProfils_.size()-1;
	}

	/**
	 * Nettoye les op�rations stock�es.
	 */
	private void clearOperations() {
		voldProfils_.clear();
		ioldProfils_=-1;

		doOperation();  // Stockage profil courant.
	}

	/**
	 * Select All.
	 */
	public void selectAllPoints() {
		profilsBief_[iprofilCourant_].setAllSelected();
	}

	//--- Interface BuCutCopyPasteInterface  ---------------------------------------

	/**
	 * Cut
	 */
	@Override
	public void cut() {
		if (modeEdition_!=EDITER) return;
		profilsBief_[iprofilCourant_].cut(profilsBief_[iprofilCourant_].getSelected());
	}

	/**
	 * Copy
	 */
	@Override
	public void copy() {
		if (modeEdition_!=EDITER) return;
		profilsBief_[iprofilCourant_].copy(profilsBief_[iprofilCourant_].getSelected());
	}

	/**
	 * Paste
	 */
	@Override
	public void paste() {
		if (modeEdition_!=EDITER) return;
		profilsBief_[iprofilCourant_].paste(profilsBief_[iprofilCourant_].getSelected());
	}

	/** Rien */
	@Override
	public void duplicate() {}

	/**
	 * Gestion des actions sur les boutons.
	 * @param e ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd=e.getActionCommand();
		if (cmd==null) {
			cmd="";
		}
		Object src=e.getSource();
		if ("FERMER".equals(cmd)) {
			// TODO A voir
			//      super.fermer();
		}
		else if ("VALIDER".equals(cmd)) {
			// verifier les contraintes: -les deux rives ont ete selectionnees
			//                           -pas de pente verticale
			//                           -...
			boolean ok=verifieContraintes();
			if (ok) {
				// Suppression des points nuls des profils.
				for (int i=0; i<profilsBief_.length; i++) profilsBief_[i].supprimePointsNuls();
				// TODO A voir
				//        super.fermer();
			}
		}
		else if ("RECULERVITE".equals(cmd)) {
			setProfilCourant(iprofilCourant_-PROFIL_RAPIDE);
		}
		else if ("RECULER".equals(cmd)) {
			setProfilCourant(iprofilCourant_-1);
		}
		else if ("AVANCER".equals(cmd)) {
			setProfilCourant(iprofilCourant_+1);
		}
		else if ("AVANCERVITE".equals(cmd)) {
			setProfilCourant(iprofilCourant_+PROFIL_RAPIDE);
		}
		else if (src==btVoirRives_) {
			graphe_.setRivesVisible(btVoirRives_.isSelected());
		}
		else if (src==btZoom_) {
			graphe_.setModeInteractif(Hydraulique1dGrapheProfil.MODE_INTERACTIF_ZOOM);
		}
		else if (src==btSelect_) {
			graphe_.setModeInteractif(Hydraulique1dGrapheProfil.MODE_INTERACTIF_SELECTION);
		}
		else if (src==btPan_) {
			graphe_.setModeInteractif(Hydraulique1dGrapheProfil.MODE_INTERACTIF_PAN);
		}
		else if (src==btFixerBornes_) {
			if (fnBornes_==null) fnBornes_=new Hydraulique1dBornesGrapheEditor(Hydraulique1dBaseApplication.FRAME);
			fnBornes_.setBornes(graphe_.getZoom());
			fnBornes_.setVisible(true);
			if (fnBornes_.reponse==1) { // Ok pour fixer les bornes
				graphe_.setZoom(fnBornes_.getBornes());
			}
		}
		else if (src==btAjusterBornes_) {
			graphe_.setRatioXY1(!btAjusterBornes_.isSelected());
		}
		else if (src==btRestaurer_) {
			graphe_.setZoomAuto();
		}
		else if (src==btInverser_) {
			profilsBief_[iprofilCourant_].inverserProfil();
		}
		else if (src==btLisser_) {
			if (fnLissage_==null) fnLissage_=new Hydraulique1dLissageProfilEditor(Hydraulique1dBaseApplication.FRAME);
			fnLissage_.setVisible(true);
			if (fnLissage_.reponse==1) {  // Ok pour le lissage
				profilsBief_[iprofilCourant_].lisser(fnLissage_.getMethode(),fnLissage_.getLargeur());
			}
		}
		else if (src==btFiltrer_) {
			if (fnFiltrage_==null) fnFiltrage_=new Hydraulique1dFiltrageProfilEditor(Hydraulique1dBaseApplication.FRAME);
			fnFiltrage_.setVisible(true);
			if (fnFiltrage_.reponse==1) {  // Ok pour le filtrage
				profilsBief_[iprofilCourant_].filtrer(fnFiltrage_.getDistance());
			}
		}
		else if (src==btImporter_) {
			if (fnInserer_==null) fnInserer_=new Hydraulique1dInsererProfilEditor(Hydraulique1dBaseApplication.FRAME);
			fnInserer_.clearProfil();
			fnInserer_.setVisible(true);
			if (fnInserer_.reponse==1) {  // Ok pour l'insertion
				profilsBief_[iprofilCourant_].inserer(fnInserer_.getProfil(),
						fnInserer_.getMasqueZones(),
						fnInserer_.getCoteMin(),
						fnInserer_.getCoteMax());
			}
		}
		else if (src==btTranslater_) {
			if (fnTranslater_==null) fnTranslater_=new Hydraulique1dTranslaterProfilEditor(Hydraulique1dBaseApplication.FRAME);
			fnTranslater_.setVisible(true);
			if (fnTranslater_.reponse==1) {  // Ok pour la translation
				double[] txy=fnTranslater_.getTranslation();
				if (txy!=null) {
					profilsBief_[iprofilCourant_].movePoints(profilsBief_[iprofilCourant_].getSelectedNonNuls(),
							txy[0],txy[1]);
				}
			}
		}
		else if (src==btStockGauche_) {
			int isel=profilsBief_[iprofilCourant_].getSelected()[0];
			if (isel==profilsBief_[iprofilCourant_].getIndiceLitMajGa()) return;

			profilsBief_[iprofilCourant_].setIndiceLitMajGa(isel);
			doOperation();
			graphe_.updateGraphe();
		}
		else if (src==btRiveGauche_) {
			int isel=profilsBief_[iprofilCourant_].getSelected()[0];
			if (isel==profilsBief_[iprofilCourant_].getIndiceLitMinGa()) return;

			profilsBief_[iprofilCourant_].setIndiceLitMinGa(isel);
			doOperation();
			graphe_.updateGraphe();
		}
		else if (src==btRiveDroite_) {
			int isel=profilsBief_[iprofilCourant_].getSelected()[0];
			if (isel==profilsBief_[iprofilCourant_].getIndiceLitMinDr()) return;

			profilsBief_[iprofilCourant_].setIndiceLitMinDr(isel);
			doOperation();
			graphe_.updateGraphe();
		}
		else if (src==btStockDroit_) {
			int isel=profilsBief_[iprofilCourant_].getSelected()[0];
			if (isel==profilsBief_[iprofilCourant_].getIndiceLitMajDr()) return;

			profilsBief_[iprofilCourant_].setIndiceLitMajDr(isel);
			doOperation();
			graphe_.updateGraphe();
		}else if( src == displayGrid) {
			graphe_.updateGraphe(displayGrid.isSelected());
		}
		else if (src==btDetruire_) {
			profilsBief_[iprofilCourant_].supprimePoints(profilsBief_[iprofilCourant_].getSelected());
		}
		else if (src==btCreer_) {
			int[] isels=profilsBief_[iprofilCourant_].getSelected();
			int ind=-1;
			if (isels.length>0) {
				Arrays.sort(isels);
				ind=isels[isels.length-1];
			}

			profilsBief_[iprofilCourant_].creeEmptyPoint(ind);
		}
		else if (src==btAlignement_) {
			if (fnAlignOption_==null) {
				CurveOptionPane pn=new CurveOptionPane(this);
				fnAlignOption_=new CtuluDialog(pn);
				fnAlignOption_.setTitle(Hydraulique1dResource.getS("Alignement des profils"));
				fnAlignOption_.setOption(CtuluDialog.ZERO_OPTION);
			}
			fnAlignOption_.afficheDialogModal();
		}
		else if (src==btVoirSuivantPrecedent_) {
			graphe_.setPreviousNextVisible(btVoirSuivantPrecedent_.isSelected());
		}
		else if (src==btCoteMinMax_) {
			graphe_.setMinMaxVisible(btCoteMinMax_.isSelected());
		}
	}

	/**
	 * @return l'image produite dans la taille courante du composant.
	 */
	public BufferedImage produceImage(Map _params) {
		//    if (pnTableau_.hasFocus()) {
		//      BufferedImage i=new BufferedImage(pnTableau_.getTableau().getWidth(), pnTableau_.getTableau().getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		//      pnTableau_.getTableau().paint(i.createGraphics());
		//      return i;
		//    }
		return graphe_.produceImage(null);
	}

	/**
	 * @return l'image produite pour une taille donnee.
	 */
	public BufferedImage produceImage(int _w, int _h, Map _params) {
		return graphe_.produceImage(_w, _h, null);
	}

	/**
	 * Le nombre de pages : 2. La premi�re pour le graphique, la deuxi�me pour le tableau.
	 * @return Le nombre de pages.
	 */
	public int getNumberOfPages() {
		return 2;
	}

	/**
	 * La methode centrale qui permet d'imprimer (idem que celle de l'interface
	 * printable). Le format <code>_format</code> sera celui donne par la methode
	 * <code>Pageable.getPageFormat(int)</code>.
	 *
	 * @return <code>Printable.PAGE_EXISTS</code> si la page existe,
	 *         sinon <code>Printable.NO_SUCH_PAGE</code>.
	 */
	public int print(Graphics _g, PageFormat _format, int _page) {
		if (_page==0) {
			if(isTracerComputation()) {
				return printWithTracerLegend(_g, _format, _page);
			}else
			return graphe_.print(_g, _format, _page);
		}
		else {
			return EbliPrinter.printComponent(_g, _format, pnTableau_.getTableau(), true, 0);
		}
	}
	
	
	/**
	 * Construit la l�gende jfreechart.
	 * @author Adrien Hadoux
	 */
	protected void showJfreechartLegend() {
		final LegendItemCollection collection = new LegendItemCollection();
		double var =  (maxC - minC)/colorConcentrations.length;	
		DecimalFormat legendFormatter = new DecimalFormat("0.0000");
		
		for(int i=0;i<numberOFDifferentsConcentrations;i++) {
			
			Color fill = colorConcentrations[i];
			String label = legendFormatter.format(minC + (i)*var) +"< X "+"< " + legendFormatter.format(minC + (i+1)*var);
			LegendItem item = new LegendItem(label, null, null, null, new Rectangle2D.Double(-4.0, -4.0, 8.0,
	                8.0),fill);
			collection.add(item);
		}
		
		
		LegendItemSource source = new LegendItemSource(){
			public LegendItemCollection getLegendItems() {				
				return collection;
			}			
		};
		LegendTitle legend = new LegendTitle(source);
		graphe_.getChart().addLegend(legend);
	}
	protected void hideJfreechartLegend() {
		
		graphe_.getChart().removeLegend();
	}
	
	protected int printWithTracerLegend(Graphics _g, PageFormat _format, int _page){
		hideJfreechartLegend();		
		showJfreechartLegend();		
		int val = graphe_.print(_g, _format, _page);
		hideJfreechartLegend();		
		return val;
	}

	@Override
	public void setXAlignment(Alignment _align) {
		graphe_.setXAlignment(_align);
	}

	@Override
	public void setZAlignment(Alignment _align) {
		graphe_.setZAlignment(_align);
	}

	@Override
	public boolean hasAxeHydraulique() {
		return false;
	}


	public MetierResultatsTemporelSpatial getResultTracer() {
		if(!CGlobal.AVEC_QUALITE_DEAU) {
			return null;
		}
		return  etude_.resultatsGeneraux().resultatsTemporelTracer();
		
	}




	
}
