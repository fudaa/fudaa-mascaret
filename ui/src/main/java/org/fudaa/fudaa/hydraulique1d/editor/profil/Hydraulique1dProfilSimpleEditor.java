/*
 * @file         Hydraulique1dProfilSimpleEditor.java
 * @creation     1999-12-28
 * @modification $Date: 2007-11-20 11:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.profil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.util.Map;
import java.util.Vector;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.hydraulique1d.*;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizerImprimable;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheProfil;
import org.fudaa.fudaa.hydraulique1d.graphe.Hydraulique1dGrapheProfilDataset;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.bu.BuLib;
import com.memoire.fu.FuLog;
/**
 * Editeur d'un profil de forme simple (Hydraulique1dProfilModel).<br>
 * @version      $Revision: 1.15 $ $Date: 2007-11-20 11:43:26 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class Hydraulique1dProfilSimpleEditor extends Hydraulique1dCustomizerImprimable {
  private final static int LITMAJG= 0;
  private final static int LITMIN= 1;
  private final static int LITMAJD= 2;
  private final static int NBSECTIONS= 3;
  Hydraulique1dProfilModel profil_;
  Hydraulique1dGrapheProfil graphe_;
  FormeComboBox cm_[];
  BuButton bt_[];
  BuTextField tfFond_;
  JCheckBox cbRives_;
  static String tit_[]=
    { Hydraulique1dResource.HYDRAULIQUE1D.getString("Lit majeur rive gauche"),
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Lit mineur"),
      Hydraulique1dResource.HYDRAULIQUE1D.getString("Lit majeur rive droite") };

  public Hydraulique1dProfilSimpleEditor() {
    super(Hydraulique1dResource.HYDRAULIQUE1D.getString("Creation de profil simple"));
    init();
  }
  public Hydraulique1dProfilSimpleEditor(BDialogContent _parent) {
    super(_parent, Hydraulique1dResource.HYDRAULIQUE1D.getString("Creation de profil simple"));
    init();
  }
  private void init() {
    Container cp= getContentPane();
    Hydraulique1dProfilFormeSimple[] formes_= null;
    graphe_= new Hydraulique1dGrapheProfil(new Hydraulique1dGrapheProfilDataset());
    graphe_.setPreferredSize(new Dimension(400, 400));
    graphe_.setRivesVisible(true);
//    addPropertyChangeListenerJF(graphe_);
    int n= 0;
    BuPanel pnCombos= new BuPanel();
    pnCombos.setLayout(new BuVerticalLayout(5, true, false));
    pnCombos.setBorder(
      new CompoundBorder(
        new EmptyBorder(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE),
        new EtchedBorder()));
    BuPanel pnLit;
    cm_= new FormeComboBox[NBSECTIONS];
    bt_= new BuButton[NBSECTIONS];
    for (int l= 0; l < NBSECTIONS; l++) {
      pnCombos.add(new BuLabel(tit_[l]), n++);
      pnLit= new BuPanel();
      pnLit.setLayout(new BuHorizontalLayout(5, false, false));
      cm_[l]= new FormeComboBox();
      formes_= getPossibleForms(l);
      for (int i= 0; i < formes_.length; i++) {
        formes_[i].setParent(getDialog());
        cm_[l].addItem(formes_[i]);
      }
      cm_[l].addActionListener(this);
      pnLit.add(cm_[l], 0);
      bt_[l]= new BuButton("E");
      bt_[l].setEnabled(
        ((Hydraulique1dProfilFormeSimple)cm_[l].getSelectedItem())
          .isEditable());
      bt_[l].addActionListener(this);
      pnLit.add(bt_[l], 1);
      pnCombos.add(pnLit, n++);
    }
    pnCombos.add(new BuLabel(getS("Cote du fond")), n++);
    tfFond_= BuTextField.createDoubleField();
    tfFond_.setValue(new Double(0.));
    tfFond_.addActionListener(this);
    pnCombos.add(tfFond_, n++);
    cbRives_= new JCheckBox(getS("Affichage des rives"));
    cbRives_.addActionListener(this);
    cbRives_.setSelected(graphe_.isRivesVisible());
    pnCombos.add(cbRives_, n++);
    cp.add(BorderLayout.WEST, pnCombos);
    cp.add(BorderLayout.CENTER, graphe_);
    setNavPanel(EbliPreferences.DIALOG.VALIDER);

    BuLib.adjustPreferredWidth(cm_);
    pack();
  }

  /**
   * Retourne les formes possibles (et leurs editeurs) en fonction du choix de la section.
   * @param _sect La section (LIT_MIN, LIT_MAJG, LIT_MAJD)
   * @return Les formes possibles.
   */
  public Hydraulique1dProfilFormeSimple[] getPossibleForms(int _sect) {
    switch (_sect) {
      case LITMIN:
        return new Hydraulique1dProfilFormeSimple[] {
            new Hydraulique1dProfilFormeAucun(),
            new Hydraulique1dProfilFormeRectangle(),
            new Hydraulique1dProfilFormeTrapeze(),
            new Hydraulique1dProfilFormeDalot(),
            new Hydraulique1dProfilFormeBuseCirculaire(),
            new Hydraulique1dProfilFormeBuseDemiCirculaire(),
            new Hydraulique1dProfilFormeBuseArche()};

      default:
        return new Hydraulique1dProfilFormeSimple[] {
            new Hydraulique1dProfilFormeAucun(),
            new Hydraulique1dProfilFormeRectangle(),
            new Hydraulique1dProfilFormeTrapeze()};
    }
  }

  @Override
  public void setObject(MetierHydraulique1d _n) {
/*    if (!(_n instanceof DProfil))
      return;
    if (_n == profil_)
      return;
    profil_= (DProfil)_n;

    graphe_.setProfil(profil_);*/
  }
  public void setProfilModel(Hydraulique1dProfilModel profil) {
    profil_ = profil;
    graphe_.setBiefProfilModels(profil_);
    graphe_.setCurrentProfil(0);
  }
  @Override
  protected boolean getValeurs() {
    redessine();
    return true;
  }
  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   *
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe,
   *         sinon <code>Printable.NO_SUCH_PAGE</code>.
   */
  @Override
  public int print(Graphics _g, PageFormat _format, int _page) {
    return graphe_.print(_g, _format, _page);
  }
  /**
   * @return l'image produite dans la taille courante du composant.
   */
  @Override
  public BufferedImage produceImage(Map _params) {
    return graphe_.produceImage(null);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String cmd= e.getActionCommand();
    Object src= e.getSource();
    if ("VALIDER".equals(cmd)) {
      getValeurs();
      fermer();
    } else {
      for (int l= 0; l < NBSECTIONS; l++) {
        Hydraulique1dProfilFormeSimple choix=
          (Hydraulique1dProfilFormeSimple)cm_[l].getSelectedItem();
        if (src == bt_[l]) {
          if (choix.isEditable())
            choix.getEditor().show();
        }
        if (src == cm_[l]) {
          bt_[l].setEnabled(choix.isEditable());
        }
      }
      if (src == cbRives_) {
        graphe_.setRivesVisible(cbRives_.isSelected());
      }

      /**
       * 4 instructions imondes pour mettre � jour le graphe.
       */
      redessine();
      graphe_.setRivesVisible(! cbRives_.isSelected());
      graphe_.setRivesVisible(cbRives_.isSelected());
      redessine();
    }

  }
  private void redessine() {
    // reconstruction du profil
    FuLog.debug("Hydraulique1dProfilSimpleEditor.redessine()");
    double coteFond= 0., minCote= Double.POSITIVE_INFINITY;
    Double fond= (Double)tfFond_.getValue();
    if (fond != null)
      coteFond= fond.doubleValue();
    Vector points= new Vector();
    GrPoint sp= new GrPoint();
    profil_.setIndiceLitMinGa(0);
    profil_.setIndiceLitMinDr(0);
    profil_.setIndiceLitMajGa(0);
    profil_.setIndiceLitMajDr(0);
    Hydraulique1dProfilFormeSimple forme=
      (Hydraulique1dProfilFormeSimple)cm_[LITMAJG].getSelectedItem();
    forme.setConnectMode(Hydraulique1dProfilFormeSimple.CONNECT_RIGHT);
    GrPoint[] pts= forme.getPoints();
    if (pts != null) {
      for (int i= 0; i < pts.length; i++) {
        points.add(pts[i]);
        if (pts[i].y_ < minCote)
          minCote= pts[i].y_;
      }
      if (points.size() > 0)
        sp= (GrPoint)points.lastElement();
    }
    forme= (Hydraulique1dProfilFormeSimple)cm_[LITMIN].getSelectedItem();
    forme.setConnectMode(Hydraulique1dProfilFormeSimple.NO_CONNECT);
    forme.setStartPoint(sp);
    pts= forme.getPoints();
    if (pts != null) {
      int s= 0;
      if (points.size() > 0) {
        s= 1;
        if (pts.length == 0) {
          //**        profil_.indiceLitMinGa(forme.getEndPoint().x);
          profil_.setIndiceLitMinGa(0);
          //**        profil_.indiceLitMinDr(forme.getEndPoint().x);
          profil_.setIndiceLitMinDr(points.size() - 1);
        } else {
          //**        profil_.indiceLitMinGa(sp.x);
          profil_.setIndiceLitMinGa(points.size() - 1);
          //**        profil_.indiceLitMinDr(forme.getEndPoint().x);
          profil_.setIndiceLitMinDr(points.size() - 1 + pts.length - 1);
        }
      }
      for (int i= s; i < pts.length; i++) {
        points.add(pts[i]);
        if (pts[i].y_ < minCote)
          minCote= pts[i].y_;
      }
      if (points.size() > 0)
        sp= (GrPoint)points.lastElement();
    }
    forme= (Hydraulique1dProfilFormeSimple)cm_[LITMAJD].getSelectedItem();
    forme.setConnectMode(Hydraulique1dProfilFormeSimple.CONNECT_LEFT);
    forme.setStartPoint(sp);
    pts= forme.getPoints();
    if (pts != null) {
      for (int i= 1; i < pts.length; i++) {
        points.add(pts[i]);
        if (pts[i].y_ < minCote)
          minCote= pts[i].y_;
      }
    }
    Hydraulique1dPoint[] ptsTmp= new Hydraulique1dPoint[points.size()];
    for (int i= 0; i < points.size(); i++) {
      ptsTmp[i]= new Hydraulique1dPoint();
      ptsTmp[i].x(((GrPoint)points.get(i)).x_);
      ptsTmp[i].y(((GrPoint)points.get(i)).y_ - minCote + coteFond);
    }
    if (ptsTmp.length > 0) {
      profil_.setIndiceLitMajGa(0);
      profil_.setIndiceLitMajDr(ptsTmp.length - 1);
      if (profil_.getIndiceLitMinDr() == 0)
        profil_.setIndiceLitMinDr(ptsTmp.length - 1);
    }
    profil_.setPoints(ptsTmp);
  }

  @Override
  protected void setValeurs() {}
}
class FormeComboBox extends JComboBox {
  public FormeComboBox() {
    super();
    setRenderer(new FormeComboBoxRenderer());
  }
}
class FormeComboBoxRenderer extends DefaultListCellRenderer {
  @Override
  public Component getListCellRendererComponent(
    javax.swing.JList list,
    java.lang.Object value,
    int index,
    boolean isSelected,
    boolean cellHasFocus) {
    Component comp=
      super.getListCellRendererComponent(
        list,
        value,
        index,
        isSelected,
        cellHasFocus);
    if ((comp instanceof JLabel)) {
      ((JLabel)comp).setText(((Hydraulique1dProfilFormeSimple)value).getName());
    }
    return comp;
  }
}
