/*
 * @file         Hydraulique1dProfilTableauPanel.java
 * @modification $Date: 2007-02-21 16:33:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2005 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.editor.profil;

import java.util.*;
import java.awt.Dimension;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

import org.fudaa.fudaa.hydraulique1d.*;
import com.memoire.bu.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

/**
 * Panneau contenant un tableau des points d'un profil (Hydraulique1dProfilModel).<br>
 * @version      $Revision: 1.8 $ $Date: 2007-02-21 16:33:50 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dProfilTableauPanel
       extends BuPanel
       implements Hydraulique1dProfilSelectionListener {

  private BuTable tableau_;
  /** Le profil */
  Hydraulique1dProfilModel profil_;
  /** Evenement selection en cours */
  boolean benableEvents_=true;
  /** Le tableau est editable */
  boolean beditable_=true;

  /**
   * Constructeur avec Hydraulique1dTableauReelModel comme modele.
   */
  public Hydraulique1dProfilTableauPanel() {
    super();
    setLayout(new BuBorderLayout(2, 2));

    //tableau_ = new BuTable(new TableProfilModel());
    tableau_ = new BuTable(new TableProfilModel()) {
    	//Implement table cell tool tips.
      @Override
    	public String getToolTipText(MouseEvent e) {
    	String tip = null;
    	java.awt.Point p = e.getPoint();
    	int rowIndex = rowAtPoint(p);
    	int colIndex = columnAtPoint(p);
    	int realColumnIndex = convertColumnIndexToModel(colIndex);
    	TableProfilModel model = (TableProfilModel)getModel();
    	if (model.avecGeoreferencement()){
	    	model.getGeoRefXAtRow(rowIndex);
	    	model.getGeoRefYAtRow(rowIndex);
	    	tip = Hydraulique1dResource.HYDRAULIQUE1D.getString("Coordonn�es Point")+" "+rowIndex+": "+model.getGeoRefXAtRow(rowIndex)+","+model.getGeoRefYAtRow(rowIndex);
    	}else{
    		tip = Hydraulique1dResource.HYDRAULIQUE1D.getString("Pas de G�oR�f�rencement");
    		//tip = super.getToolTipText(e);
    	}

    	return tip;
    	}
    	//Implement table header tool tips.
    	/*protected JTableHeader createDefaultTableHeader() {
    	return new JTableHeader(columnModel) {
    	public String getToolTipText(MouseEvent e) {
    	String tip = null;
    	java.awt.Point p = e.getPoint();
    	int index = columnModel.getColumnIndexAtX(p.x);
    	int realIndex = columnModel.getColumn(index).getModelIndex();
    	return "ffff"+realIndex;
    	}
    	};
    	}*/
    	};


    // Modification des actions mappees, conflictuelles avec la fenetre.
    tableau_.getActionMap().put("paste", new AbstractAction("paste") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {
        if (beditable_) profil_.paste(profil_.getSelected());
      };
    });
    tableau_.getActionMap().put("copy", new AbstractAction("copy") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {
        if (beditable_) profil_.copy(profil_.getSelected());
        tableau_.copy();
      };
    });
    tableau_.getActionMap().put("cut", new AbstractAction("cut") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {
        if (beditable_) profil_.cut(profil_.getSelected());
      };
    });
    tableau_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    tableau_.getColumnModel().getColumn(0).setPreferredWidth(25);
    // Selection des lignes uniquement.


tableau_.setCellSelectionEnabled(false);
    tableau_.setRowSelectionAllowed(true);
    tableau_.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tableauSelectionChanged(e);
      }
    });

    JScrollPane sp= new JScrollPane(tableau_);
    sp.setPreferredSize(new Dimension(200,400));

    add(sp,BuBorderLayout.CENTER);
  }


  /**
   * Lorsque la selection est modifi�e.
   */
  private void tableauSelectionChanged(ListSelectionEvent e) {
    if (!benableEvents_) return;
    benableEvents_=false;

    int[] isels=tableau_.getSelectedRows();
    profil_.setSelected(isels);

    benableEvents_=true;
  }

  /**
   * Definit le profil associ�.
   * @param profilModel Le profil.
   */
  public void setProfilModel(Hydraulique1dProfilModel _profil) {
    if (profil_!=null) profil_.removeSelectionListener(this);
    profil_=_profil;
    if (profil_!=null) profil_.addSelectionListener(this);

    ((TableProfilModel)tableau_.getModel()).setProfil(_profil);
  }

  /**
   * Definit si le tableau est �ditable.
   * @param _b true : Le tableau est editable.
   */
  public void setEditable(boolean _b) {
    beditable_=_b;
    ((TableProfilModel)tableau_.getModel()).setEditable(beditable_);
  }

  /**
   * @return le tableau.
   */
  JTable getTableau() {
    return tableau_;
  }

  // --- Hydraulique1dProfilSelectionListener ------------------------------------------------

  /**
   * Selection chang�e sur le profil.
   * @param _evt Hydraulique1dProfilSelectionEvent
   */
  @Override
  public void pointSelectionChanged(Hydraulique1dProfilSelectionEvent _evt) {
    if (!benableEvents_) return;
    benableEvents_=false;

    int[] isels=_evt.getIndices();
    ListSelectionModel mdsel=tableau_.getSelectionModel();
    mdsel.setValueIsAdjusting(true);
    mdsel.clearSelection();
    for (int i=0; i<isels.length; i++) mdsel.addSelectionInterval(isels[i],isels[i]);
    mdsel.setValueIsAdjusting(false);

    benableEvents_=true;
  }
}

/**
 * Classe de modele pour le tableau.
 */
class TableProfilModel extends AbstractTableModel implements Hydraulique1dProfilDataListener {

  private final static String[] NOMS_COLONNES = {Hydraulique1dResource.HYDRAULIQUE1D.getString("N�"),
                                                 Hydraulique1dResource.HYDRAULIQUE1D.getString("Abscisses"),
                                                 Hydraulique1dResource.HYDRAULIQUE1D.getString("Cotes")};

  /** Le profil */
  private Hydraulique1dProfilModel profil_=new Hydraulique1dProfilModel();
  /** Le modele est editable ou non. */
  private boolean editable_=true;

  /**
   * Definition du profil, et modifications ecout�es par le listener pour mise a jour du tableau.
   * @param _profil Le profil.
   */
  public void setProfil(Hydraulique1dProfilModel _profil) {
    if (profil_!=null) profil_.removeDataListener(this);
    profil_=_profil;
    if (profil_!=null) profil_.addDataListener(this);
    fireTableDataChanged();
  }

  /**
   * Le tableau est editable ou non.
   */
  public void setEditable(boolean editable) {
    editable_= editable;
  }

  //--- AbstractTableModel -----------------------------------------

  @Override
  public int getColumnCount() {
    return NOMS_COLONNES.length;
  }

  @Override
  public int getRowCount() {
    return profil_.getListePoints().size();
  }

  @Override
  public Object getValueAt(int row, int col) {
    Hydraulique1dPoint pt=profil_.getPoint(row);
    if      (col==0) return new Integer(row+1);
    else if (col==1) return pt.X();
    else             return pt.Y();
  }

  @Override
  public void setValueAt(Object obj, int row, int col) {
    if (col==0) return;
    if (obj !=null && !(obj instanceof Double)) return;

    Hydraulique1dPoint pt=profil_.getPoint(row);
    if (col==1) pt.X((Double)obj);
    else        pt.Y((Double)obj);

    profil_.modifiePoint(row,pt.X(),pt.Y());
  }

  @Override
  public String getColumnName(int column) {
    return NOMS_COLONNES[column];
  }

  /**
   * Retourne la classe (Class) de type Double
   * @param col l'indice de la colonne
   * @return la classe Double
   */
  @Override
  public Class getColumnClass(int col) {
    if (col==0) return Integer.class;
    else        return Double.class;
  }

  /**
   * Retourne si la cellule est �ditable.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return Vrai.
   */
  @Override
  public boolean isCellEditable(int row, int col) {
    if (col==0) return false;
    else        return editable_;
  }

  //--- Hydraulique1dProfilDataListener -----------------------------------------

  @Override
  public void pointModified(Hydraulique1dProfilDataEvent _evt) {
    int[] inds=_evt.getIndices();
    if (inds!=null) {
      Arrays.sort(inds);
      fireTableRowsUpdated(inds[0], inds[inds.length-1]);
    }
    else fireTableDataChanged();
  }

  @Override
  public void pointDeleted(Hydraulique1dProfilDataEvent _evt) {
    int[] inds=_evt.getIndices();
    Arrays.sort(inds);
    fireTableRowsDeleted(inds[0],inds[inds.length-1]);
  }

  @Override
  public void pointInserted(Hydraulique1dProfilDataEvent _evt) {
    int[] inds=_evt.getIndices();
    Arrays.sort(inds);
    fireTableRowsInserted(inds[0],inds[inds.length-1]);
  }

  public JToolTip createToolTip() {
      JToolTip tip = createToolTip();
      tip.setToolTipText("test");
      return tip;
    }

  public Double getGeoRefXAtRow(int row) {
	  return profil_.getPoint(row).CX();
	  }
  public Double getGeoRefYAtRow(int row) {
	  return profil_.getPoint(row).CY();
	  }
  public boolean avecGeoreferencement() {
	  return profil_.avecGeoreferencement();
	  }
}
