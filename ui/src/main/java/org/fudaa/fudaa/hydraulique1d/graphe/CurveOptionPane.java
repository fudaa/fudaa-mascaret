/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.hydraulique1d.graphe;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.graphe.CurveOptionTargetI.Alignment;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau d'options pour l'alignement des courbes.
 * @author marchand@deltacad.fr
 */
public class CurveOptionPane extends CtuluDialogPanel {
  CurveOptionTargetI view_;
  
  public CurveOptionPane(CurveOptionTargetI _view) {
    view_=_view;
    
    buildPanel();
  }
  
  private void buildPanel() {
    setLayout(new BuVerticalLayout(3,true,false));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    
    JPanel pnXAlign=new JPanel();
    pnXAlign.setLayout(new BorderLayout(3,3));
    JLabel lbXAlign=new JLabel(Hydraulique1dResource.getS("Alignement en X"));
    
    BuComboBox coXAlign=new BuComboBox();
    coXAlign.addItem(Hydraulique1dResource.getS("Sans alignement"));
    coXAlign.addItem(Hydraulique1dResource.getS("Abscisse min"));
    coXAlign.addItem(Hydraulique1dResource.getS("Abscisse max"));
    coXAlign.addItem(Hydraulique1dResource.getS("Point de Z min"));

    // Ajout des limites
    coXAlign.addItem(Hydraulique1dResource.getS("Stockage gauche"));
    coXAlign.addItem(Hydraulique1dResource.getS("Rive gauche"));
    coXAlign.addItem(Hydraulique1dResource.getS("Rive droite"));
    coXAlign.addItem(Hydraulique1dResource.getS("Stockage droit"));

    // Ajout de l'axe
    if (view_.hasAxeHydraulique())
      coXAlign.addItem(Hydraulique1dResource.getS("Axe hydraulique"));
    
    coXAlign.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int indsel=((BuComboBox)e.getSource()).getSelectedIndex();
        
        if (indsel==0) {
          view_.setXAlignment(Alignment.X_IDENT);
        }
        else if (indsel==1) {
          view_.setXAlignment(Alignment.LEFT);
        }
        else if (indsel==2) {
          view_.setXAlignment(Alignment.RIGHT);
        }
        else if (indsel==3) {
          view_.setXAlignment(Alignment.X_ZMIN);
        }
        else if (indsel==4) {
          view_.setXAlignment(Alignment.LEFT_STORE);
        }
        else if (indsel==5) {
          view_.setXAlignment(Alignment.LEFT_BANK);
        }
        else if (indsel==6) {
          view_.setXAlignment(Alignment.RIGHT_BANK);
        }
        else if (indsel==7) {
          view_.setXAlignment(Alignment.RIGHT_STORE);
        }
        else if (indsel==8) {
          view_.setXAlignment(Alignment.AXIS);
        }
      }
    });
    
    pnXAlign.add(lbXAlign,BorderLayout.WEST);
    pnXAlign.add(coXAlign,BorderLayout.CENTER);
    
    add(pnXAlign);
    
    JPanel pnZAlign=new JPanel();
    pnZAlign.setLayout(new BorderLayout(3,3));
    JLabel lbZAlign=new JLabel(Hydraulique1dResource.getS("Alignement en Z"));
    
    BuComboBox coZAlign=new BuComboBox();
    coZAlign.addItem(Hydraulique1dResource.getS("Sans alignement"));
    coZAlign.addItem(Hydraulique1dResource.getS("Sur le Z min"));
    coZAlign.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (((BuComboBox)e.getSource()).getSelectedIndex()==0) {
          view_.setZAlignment(Alignment.Z_IDENT);
        }
        else
          view_.setZAlignment(Alignment.Z_MIN);
      }
    });
    
    pnZAlign.add(lbZAlign,BorderLayout.WEST);
    pnZAlign.add(coZAlign,BorderLayout.CENTER);
    
    add(pnZAlign);
  }  
}
