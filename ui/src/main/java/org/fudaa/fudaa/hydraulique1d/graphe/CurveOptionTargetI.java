package org.fudaa.fudaa.hydraulique1d.graphe;

public interface CurveOptionTargetI {
  /**
   * Enum�ration pour l'alignement des profils suivant X et Z.
   */
  public enum Alignment {

    /**
     * Alignement X par defaut (tous les profils dans le m�me systeme)
     */
    X_IDENT,
    /**
     * Alignement X sur l'abscisse 0
     */
    LEFT,
    /**
     * Alignement X sur la zone de stockage gauche
     */
    LEFT_STORE,
    /**
     * Alignement X sur la rive gauche
     */
    LEFT_BANK,
    /**
     * Alignement X sur l'abscisse maxi
     */
    RIGHT,
    /**
     * Alignement X sur la zone de stockage droit
     */
    RIGHT_STORE,
    /**
     * Alignement X sur la rive droite
     */
    RIGHT_BANK,
    /**
     * Alignement X sur l'axe hydraulique
     */
    AXIS,
    /**
     * Alignement X sur le point de Z min
     */
    X_ZMIN,
    /**
     * Alignement Z par defaut (tous les profils dans le m�me systeme)
     */
    Z_IDENT,
    /**
     * Alignement Z sur les Z min
     */
    Z_MIN
  }
  
  /**
   * @param _align Alignement des courbes suivant X
   */
  public void setXAlignment(Alignment _align);
  
  /**
   * @param _align Alignement des courbes suivant Z
   */
  public void setZAlignment(Alignment _align);
  
  /**
   * @return True si la cible possede un axe hydraulique.
   */
  public boolean hasAxeHydraulique();

}
