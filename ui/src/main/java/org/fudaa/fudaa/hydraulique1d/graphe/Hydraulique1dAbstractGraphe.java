/*
 * @file         Hydraulique1dAbstractGraphe.java
 * @creation     2001-16-11
 * @modification $Date: 2006-09-12 08:36:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.JFileChooser;

import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
/**
 * Classe m�re de tous les graphe d'hydraulique1d sous celui des profils.<br>
 * Les m�thodes afficheAvecCalculBorne et afficheSansCalculBorne sont abstraites.
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:36:41 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public abstract class Hydraulique1dAbstractGraphe
  extends BGraphe
  implements PropertyChangeListener, ActionListener {
  protected boolean thumbnail_= false;
  protected String titreGraphe_= "";
  protected String titreX_= "";
  protected String uniteX_= "";
  protected String titreY_= "";
  protected String uniteY_= "";
  protected Axe axeX_, axeY_;

  /**
   * Editeur des axes du graphe.
   */
  protected BGrapheEditeurAxes editeurAxes_;
  /**
   * Personnaliseur du graphe.
   */
  protected BGraphePersonnaliseur personGraphe_;

  /**
   * Palette de couleur utilis�e pour les courbes.
   */
  public static Color[] PALETTE=
    {
      Color.red,
      Color.blue,
      Color.gray,
      Color.magenta,
      Color.pink,
      Color.orange,
      Color.black,
      Color.cyan,
      Color.darkGray,
      Color.yellow,
      Color.lightGray,
      Color.green };

  /**
   * Constructeur par d�faut.
   */
  public Hydraulique1dAbstractGraphe() {
    super();
    super.setInteractif(true);
    setPreferredSize(new Dimension(400, 200));
    editeurAxes_= null;
    personGraphe_= null;
    axeX_= new Axe();
    axeX_.maximum_= 2;
    axeX_.minimum_= 0;
    axeX_.vertical_= false;
    axeX_.titre_= "x";
    axeY_= new Axe();
    axeY_.maximum_= 1;
    axeY_.minimum_= 0;
    axeY_.vertical_= true;
    axeY_.titre_= "y";
    Graphe g= new MascaretGridGraphe();
    g.ajoute(axeX_);
    g.ajoute(axeY_);
    setGraphe(g);
  }
  // PropertyChangeListener
  @Override
  public void propertyChange(PropertyChangeEvent e) {
    if ("tableCellEditor".equals(e.getPropertyName())) {
      afficheAvecCalculBorne();
    }
  }

  /**
   * Si la source est l'�diteur de graphes, on affiche les courbes.
   * @param _evt ActionEvent
   */
  @Override
  public void actionPerformed(ActionEvent _evt) {
    Object src= _evt.getSource();
    if (src instanceof BGrapheEditeurAxes) {
      BGrapheEditeurAxes ed= (BGrapheEditeurAxes)src;
      Double[][] bornesAxes= ed.getAxes();
      if (bornesAxes[0][1] == null) {
        afficheAvecCalculBorne();
      } else {
        axeX_.maximum_= bornesAxes[0][1].doubleValue();
        axeX_.minimum_= bornesAxes[0][0].doubleValue();
        axeY_.maximum_= bornesAxes[1][1].doubleValue();
        axeY_.minimum_= bornesAxes[1][0].doubleValue();
        afficheSansCalculBorne();
      }
    }
  }

  /**
   * Retourne l'�diteur des axes.
   * @return BGrapheEditeurAxes
   */
  public BGrapheEditeurAxes getEditeurAxes() {
    if (editeurAxes_ == null)
      initEditeurAxes();
    return editeurAxes_;
  }

  /**
   * Initialise l'�diteur des axes.
   * Ce composant devient �couteurs de l'�diteur des axes.
   * l'�diteur des axes devient �couteur des changement de propri�t� de ce composant.
   */
  protected void initEditeurAxes() {
    editeurAxes_= new BGrapheEditeurAxes(this);
    editeurAxes_.addActionListener(this);
    this.addPropertyChangeListener(editeurAxes_);
  }

  /**
   * Retourne le personaliseur de ce graphe.
   * @return BGraphePersonnaliseur
   */
  public BGraphePersonnaliseur getPersonnaliseurGraphe() {
    if (personGraphe_ == null)
      initPersonnaliseurGraphe();
    return personGraphe_;
  }

  /**
   * Importation d'un image en image au fond du graphe.
   */
  public void importImageFond() {
    BGraphePersonnaliseur personGraphe= getPersonnaliseurGraphe();
    JFileChooser fc= new JFileChooser();
    fc.setCurrentDirectory(Hydraulique1dResource.lastImportDir);
    fc.setFileFilter(new javax.swing.filechooser.FileFilter() {
      @Override
      public boolean accept(File f) {
        return f.isDirectory()
          || f.getName().endsWith(".gif")
          || f.getName().endsWith(".GIF")
          || f.getName().endsWith(".jpg")
          || f.getName().endsWith(".JPG")
          || f.getName().endsWith(".png")
          || f.getName().endsWith(".PNG");
      }
      @Override
      public String getDescription() {
        return "Images GIF,JPG,PNG";
      }
    });
    int returnVal= fc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      String file= fc.getSelectedFile().getAbsolutePath();
      Image img= getToolkit().createImage(file);
      MediaTracker tracker= new MediaTracker(personGraphe);
      tracker.addImage(img, 0);
      try {
        tracker.waitForID(0);
      } catch (InterruptedException ie) {}
      getGraphe().fond_= img;
      fullRepaint();
      Hydraulique1dResource.lastImportDir= fc.getCurrentDirectory();
    }
  }

  /**
   * Supprimme l'�ventuel image de fond du graphe.
   */
  public void supprimeImageFond() {
    Graphe g= getGraphe();
    g.fond_= null;
    fullRepaint();
  }

  /**
   * Initialise personaliseur de ce graphe.
   * Ce composant devient �couteur des changement de propri�t� du personaliseur.
   * le personnaliseur devient �couteur des changement de propri�t� de ce composant.
   */
  protected void initPersonnaliseurGraphe() {
    personGraphe_= new BGraphePersonnaliseur(this.getGraphe());
    personGraphe_.addPropertyChangeListener(this);
    this.addPropertyChangeListener(personGraphe_);
  }

  /**
   * Redimensionne le graphe �ventuellement en mode timbre poste (thumbnail).
   * @param t Vrai ppur obtenir le mode timbre poste.
   * @param width nouvelle largeur
   * @param height nouvelle hauteur
   */
  public void setThumbnail(boolean t, int width, int height) {
    thumbnail_= t;
    setPreferredSize(new Dimension(width, height));
    super.setInteractif(!thumbnail_);
    initLabels();
  }

  /**
   * Modifie les titres du graphe.
   * @param titreGraphe Le titre g�n�ral du graphe.
   * @param titreX Titre des abscisses
   * @param titreY Titre des ordonn�es
   * @param uniteX Unit� des abscisses
   * @param uniteY Unit� des ordonn�es
   */
  public void setLabels(
    String titreGraphe,
    String titreX,
    String titreY,
    String uniteX,
    String uniteY) {
    if (titreGraphe != null)
      titreGraphe_= titreGraphe;
    if (titreX != null)
      titreX_= titreX;
    if (titreY != null)
      titreY_= titreY;
    if (uniteX != null)
      uniteX_= uniteX;
    if (uniteY != null)
      uniteY_= uniteY;
    initLabels();
  }

  /**
   * Initialise les labels.
   * Tient compte du mode "timbre poste" (thumbnail) c'est � dire sans titre.
   */
  protected void initLabels() {
    Graphe g= getGraphe();
    if (thumbnail_) {
      g.titre_= "";
      axeX_.titre_= "";
      axeX_.unite_= "";
      axeY_.titre_= "";
      axeY_.unite_= "";
    } else {
      g.titre_= titreGraphe_;
      axeX_.titre_= titreX_;
      axeX_.unite_= uniteX_;
      axeY_.titre_= titreY_;
      axeY_.unite_= uniteY_;
    }
    this.firePropertyChange("GRAPHE_TITRE", null, g.titre_);
    this.firePropertyChange("AXEX_LABEL", null, axeX_);
    this.firePropertyChange("AXEY_LABEL", null, axeY_);
    fullRepaint();
  }

  /**
   * Met � jour les min, max et pas des axes.
   * @param minX valeur minimum des abscisses
   * @param minY valeur minimum des ordonn�es
   * @param maxX valeur maximum des abscisses
   * @param maxY double maximum des ordonn�es
   */
  protected void calculBorne(
    double minX,
    double minY,
    double maxX,
    double maxY) {
    axeX_.pas_= pas(minX, maxX);
    if (minX == maxX) {
      if (minX == 0)
        axeX_.pas_= 1;
      axeX_.minimum_= minX - axeX_.pas_;
      axeX_.maximum_= minX + axeX_.pas_;
    } else {
      axeX_.minimum_= minX - axeX_.pas_;
      axeX_.maximum_= maxX + axeX_.pas_;
    }
    axeY_.pas_= pas(minY, maxY);
    if (minY == maxY) {
      if (minY == 0)
        axeY_.pas_= 1;
      axeY_.minimum_= minY - axeY_.pas_;
      axeY_.maximum_= minY + axeY_.pas_;
    } else {
      axeY_.minimum_= minY - axeY_.pas_;
      axeY_.maximum_= maxY + axeY_.pas_;
    }
    this.firePropertyChange("AXEX", null, axeX_);
    this.firePropertyChange("AXEY", null, axeY_);
  }

  /**
   * Doit ajouter les courbes (ebli.graphe.Courbe) au graphe (ebli.graphe.Graphe).
   * puis calculer les bornes avec la m�thode calculBorne(minX,minY,maxX,maxY).
   * puis repeindre le composant avec la m�thode fullRepaint().
   */
  abstract void afficheAvecCalculBorne();

  /**
   * Doit ajouter les courbes (ebli.graphe.Courbe) au graphe (ebli.graphe.Graphe).
   * Ne pas  calculer les bornes des axes.
   * puis repeindre le composant avec la m�thode fullRepaint().
   */
  abstract void afficheSansCalculBorne();

  /**
   * Initialise le graphe.
   * Redimensionne les marges et enl�ve les courbes.
   */
  protected void initGraphe() {
    Graphe g= getGraphe();
    Marges m= g.marges_;
    if (thumbnail_) {
      m.gauche_= 5;
      m.droite_= 5;
      m.haut_= 5;
      m.bas_= 5;
    } else {
      m.gauche_= 60;
      m.droite_= 120;
      m.haut_= 45;
      m.bas_= 30;
    }
    g.legende_= !thumbnail_;
    g.copyright_= false;
    axeX_.graduations_= !thumbnail_;
    axeY_.graduations_= !thumbnail_;
    g.animation_= false;
    axeX_.vertical_= false;
    axeY_.vertical_= true;
    // efface toute les courbes actuelles
    Object[] lesCourbes= g.getElementsParType((new CourbeDefault()).getClass());
    for (int i= 0; i < lesCourbes.length; i++) {
      g.enleve(lesCourbes[i]);
    }
    this.firePropertyChange("GRAPHE_LABEL", null, g);
    this.firePropertyChange("AXEX_GRADUATION", null, axeX_);
    this.firePropertyChange("AXEY_GRADUATION", null, axeY_);
    this.firePropertyChange("MARGE", null, m);
    repaint();
  }

  /**
   * Retourne environ le dixi�me de la diff�rence (max - min).
   * utilis� pour d�finir le pas d'un axe.
   * @param min Le minimum
   * @param max Le maximum
   * @return environ le dixi�me de la diff�rence (max - min).
   */
  protected static double pas(double min, double max) {
    double diff= max - min;
    if (min == max)
      diff= min;
    double log= Math.log(diff) / Math.log(10);
    double puissance10= Math.pow(10, Math.floor(log + 0.5) - 1);
    double nbPas= diff / puissance10;
    if (nbPas >= 10)
      return (puissance10 * 5);
    return puissance10;
  }
  protected String getS(final String _s) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(_s);
  }


  protected String getS(final String _s, final String _v0) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(_s, _v0);
  }

  protected String getS(final String _s, final String _v0, final String _v1) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(_s, _v0, _v1);
  }
}
