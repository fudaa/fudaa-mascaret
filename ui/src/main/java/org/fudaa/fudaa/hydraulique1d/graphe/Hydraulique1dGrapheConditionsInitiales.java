/*
 * @file         Hydraulique1dGrapheConditionsInitiales.java
 * @creation     2001-11-15
 * @modification $Date: 2006-04-12 15:31:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Valeur;
/**
 * Le graphe des conditions initiales.
 * Le profil en long d'un bief avec la cote du fond, les zones s�ches et la ligne d'eau initiale.<br>
 * Utilis� par l'�diteur Hydraulique1dVisuInitialeEditor.<br>
 * @see org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dVisuInitialeEditor
 * @version      $Revision: 1.11 $ $Date: 2006-04-12 15:31:11 $ by $Author: deniger $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dGrapheConditionsInitiales
  extends Hydraulique1dAbstractGraphe {
  CourbeDefault[] courbes_;
  public Hydraulique1dGrapheConditionsInitiales() {
    super();
    courbes_= null;
  }
  @Override
  void afficheAvecCalculBorne() {
    affiche(courbes_, true);
  }
  @Override
  void afficheSansCalculBorne() {
    affiche(courbes_, false);
  }
  // PropertyChangeListener
  @Override
  public void propertyChange(PropertyChangeEvent e) {
    super.propertyChange(e);
    fullRepaint();
  }
  public void affiche(CourbeDefault[] courbes) {
    courbes_= courbes;
    afficheAvecCalculBorne();
  }
  protected void affiche(CourbeDefault[] courbes, boolean calculBorne) {
    setLabels(getS("Profil en long")+" - "+getS("Condition initiale"), "X", getS("Cote"), "m", "m");
    initGraphe();
    Graphe g= getGraphe();
    Vector crbs= new Vector(courbes.length);
    
  //-- 2.3.4 - visualisation des zones s�ches: changement affichage --//
    courbes = displayZoneSeche(courbes);
    
    for (int i= 0; i < courbes.length; i++) {
      if (courbes[i] != null) {
        crbs.add(courbes[i]);
        g.ajoute(courbes[i]);
      }
    }
    if (calculBorne) {
      double minX= Double.POSITIVE_INFINITY;
      double minY= Double.POSITIVE_INFINITY;
      double maxX= Double.NEGATIVE_INFINITY;
      double maxY= Double.NEGATIVE_INFINITY;
      for (int i= 0; i < crbs.size(); i++) {
        List vals= ((CourbeDefault)crbs.get(i)).valeurs_;
        for (int j= 0; j < vals.size(); j++) {
          double x= ((Valeur)vals.get(j)).s_;
          double y= ((Valeur)vals.get(j)).v_;
          if (minX > x)
            minX= x;
          if (maxX < x)
            maxX= x;
          if (minY > y)
            minY= y;
          if (maxY < y)
            maxY= y;
        }
      }
      super.calculBorne(minX, minY, maxX, maxY);
    }
    this.firePropertyChange("COURBES", null, g);
    fullRepaint();
  }
  
  
  /**
   * Compute la nouvelle repr�sentaiton de la zone seche
   * qui viendra prendre automatiquement les coordonn�es de la courbe de fond et superposera sa couleur verte au noir.
   * @param courbes
   * @return
   */
private CourbeDefault[] displayZoneSeche(CourbeDefault[] courbes) {
	List<CourbeDefault> results = new ArrayList<CourbeDefault>();
	CourbeDefault courbeZoneSeche = null;
	CourbeDefault courbeFond = null;
	CourbeDefault courbeCoteEau = null;
	int debutZoneSeche =0;
	//-- step 1 detecter la courbe de zone seche si elle existe --//
	for(CourbeDefault courbe:courbes) {
		if("Fond".equals(courbe.getTitre())) {
			courbeFond = courbe;
		}else if("zones s�ches".equals(courbe.getTitre())) {
			courbeZoneSeche = courbe;
		}else if("Cote ligne d'eau initiale".equals(courbe.getTitre())) {
			courbeCoteEau = courbe;
		}
	}
	
	if( courbeZoneSeche == null || courbeFond == null)
		return courbes;
	
	boolean trouve = false;
	double startXzoneSeche = courbeZoneSeche.getX(0),startYzoneSeche, coeff, cst;
	//-- step 2 - retrouver pour chaque x de la zone seche le y de la courbe de fond --//
	for(int i=0;!trouve && i<courbeFond.getNbPoint();i++) {
		if(courbeFond.getX(i)>= startXzoneSeche) {
			debutZoneSeche =i;
			trouve = true;
		}
	}
	double firstXzoneSeche, firstYzoneSeche,Xa,Ya,Xb,Yb;
	List<Valeur> listeValeurZoneSeche = new ArrayList<Valeur>(), listeValeurZoneFond = new ArrayList<Valeur>(), listeValeurCoteEau = new ArrayList<Valeur>() ;
	//-- step 3 - calculer le premier point a placer pour la zone seche en calculant la droite d'equation s�parant les 2 points --//
	if(debutZoneSeche >0 ) {		
		Xa = courbeFond.getX(debutZoneSeche-1);
		Ya = courbeFond.getY(debutZoneSeche-1);
		Xb = courbeFond.getX(debutZoneSeche);
		Yb = courbeFond.getY(debutZoneSeche);
		//-- calcul de la droite d'equation --//
		coeff = (double)(Ya - Yb)/ (double)(Xa - Xb);
		cst = Ya - coeff*Xa;
		startYzoneSeche = startXzoneSeche*coeff + cst  ;
		listeValeurZoneSeche.add(new Valeur(startXzoneSeche,startYzoneSeche));
	}

	//-- step 4: remove point from zone fond et add sur zone seche --//
	for(int i=debutZoneSeche;i<courbeFond.getNbPoint();i++){
		listeValeurZoneSeche.add(new Valeur(courbeFond.getX(i),courbeFond.getY(i)));
	}
	for(int i=0;i<debutZoneSeche; i++) {
		listeValeurZoneFond.add(new Valeur(courbeFond.getX(i),courbeFond.getY(i)));
	}
	
	//-- cote d'eau: on affiche jusqu'au d�but de la zone seche --//
	if(courbeCoteEau != null) {
		for(int i=0;i<courbeCoteEau.getNbPoint(); i++) {
			if(courbeCoteEau.getX(i) <=startXzoneSeche)
				listeValeurCoteEau.add(new Valeur(courbeCoteEau.getX(i),courbeCoteEau.getY(i)));
		}
	}
	
	
	courbeZoneSeche.valeurs_ = listeValeurZoneSeche;
	courbeFond.valeurs_ = listeValeurZoneFond;
	courbeCoteEau.valeurs_ = listeValeurCoteEau;
	
	return courbes;
}
}
