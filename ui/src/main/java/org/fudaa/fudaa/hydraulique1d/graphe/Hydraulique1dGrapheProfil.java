/*
 * @file         Hydraulique1dGrapheProfil.java
 * @creation     1999-12-10
 * @modification $Date: 2007-02-21 16:33:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.text.NumberFormat;
import java.util.Map;

import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dPoint;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilDataEvent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilDataListener;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilSelectionEvent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilSelectionListener;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilEditor;
import org.fudaa.fudaa.hydraulique1d.graphe.CurveOptionTargetI.Alignment;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

/**
 * Un composant pour afficher un profil et eventuellement ses precedents/suivants.
 * Utilis� par les �diteurs Hydraulique1dProfilEditor et Hydraulique1dProfilSimpleEditor.<br>
 * @see org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilEditor
 * @see org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilSimpleEditor
 * @see Hydraulique1dProfilModel
 * @version      $Revision: 1.21 $ $Date: 2007-02-21 16:33:52 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class Hydraulique1dGrapheProfil extends ChartPanel
implements Hydraulique1dProfilSelectionListener, Hydraulique1dProfilDataListener {

	public static final int MODE_INTERACTIF_SELECTION=0;
	public static final int MODE_INTERACTIF_ZOOM=1;
	public static final int MODE_INTERACTIF_PAN=2;

	/** Mod�le pour l'affichage du profil */
	private final Hydraulique1dGrapheProfilDataset dataset_;
	/** Le composant de trac� des courbes */
	private JFreeChart graphe_;
	/** Les rives sont visible/invisibles */
	private boolean brivesVisibles_;
	/** Les rives sont visible/invisibles */
	private boolean bminmaxVisibles_;
	/** Le mode interactif */
	private int modeInteractif_=MODE_INTERACTIF_ZOOM;
	/** Ratio X/Y forc� � 1 ? */
	private boolean bratioXY1_;
	/** La position x courante ecran de la souris. */
	private double xcurrent_;
	/** La position y courante ecran de la souris. */
	private double ycurrent_;
	/** Si le deplacement peut commencer. */
	private boolean bdepl_=false;
	/** Les points sont en deplacement */
	private boolean bPointsMoving_=false;

	MascaretXYAreaRenderer defaultRenderer;
	private final boolean showMarker ;
	
	public Hydraulique1dGrapheProfil(Hydraulique1dGrapheProfilDataset dataset) {
		this(dataset,true);
		
	}
	public Hydraulique1dGrapheProfil(Hydraulique1dGrapheProfilDataset dataset,boolean showMarker) {
		this(dataset,showMarker, Hydraulique1dProfilEditor.VOIR);
		
	}
	/**
	 * Constructeur. Bas� sur chartPanel.
	 */
	public Hydraulique1dGrapheProfil(Hydraulique1dGrapheProfilDataset dataset,boolean showMarker, int modeEdition) {
		super(ChartFactory.createXYLineChart("",Hydraulique1dResource.HYDRAULIQUE1D.getString("abscisse")+" [m]",
				Hydraulique1dResource.HYDRAULIQUE1D.getString("cote")+" [m]",null,PlotOrientation.VERTICAL,false,true,false));
		this.showMarker = showMarker;
		dataset_ = dataset;
		XYLineAndShapeRenderer renderer=new XYLineAndShapeRenderer();
		
		renderer.setBaseShapesFilled(true);

		// Current curve
		renderer.setSeriesShape(0,new java.awt.geom.Rectangle2D.Double(-2,-2,4,4));
		renderer.setSeriesPaint(0,Color.blue);
		renderer.setSeriesLinesVisible(0,false);
		renderer.setSeriesShapesFilled(0,true);
		
		renderer.setSeriesShapesVisible(1,false);
		renderer.setSeriesPaint(1,Color.red);
		renderer.setSeriesStroke(1, new BasicStroke(2));
		renderer.setSeriesShape(2,new java.awt.geom.Rectangle2D.Double(-1,-1,2,2));
		renderer.setSeriesPaint(2,Color.black);
		renderer.setSeriesLinesVisible(2,false);

		// Next curve
		renderer.setSeriesPaint(4,Color.GREEN.darker());
		renderer.setSeriesStroke(4,new BasicStroke(1.f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.f,new float[]{5.f,3.0f},0.f));
		renderer.setSeriesShapesVisible(4,false);

		renderer.setBaseToolTipGenerator(new XYToolTipGenerator() {
			@Override
			public String generateToolTip(XYDataset xYDataset, int _iser, int _item) {
				return dataset_.getToolTip(_iser,_item);
			}
		});

		graphe_=getChart();
		
		defaultRenderer = new MascaretXYAreaRenderer(renderer);
		
		if(modeEdition == Hydraulique1dProfilEditor.EDITER)
			graphe_.getXYPlot().setRenderer(renderer);		
		else
			graphe_.getXYPlot().setRenderer(defaultRenderer);		
		//graphe_.getXYPlot().setRenderer(defaultRenderer);		
		
		graphe_.getXYPlot().setBackgroundPaint(Color.WHITE);
		graphe_.getXYPlot().setDomainGridlinePaint(Color.GRAY);
		graphe_.getXYPlot().setRangeGridlinePaint(Color.GRAY);

		// Acc�l�re le trac� du graphe quand la fenetre est de dimensions importantes.
		graphe_.setAntiAlias(true);
		graphe_.getXYPlot().setDataset(dataset_);

		brivesVisibles_= true;
		setRatioXY1(false);

		setPreferredSize(new Dimension(640, 480));
		// Pour l'affichage des tooltips.
		setInitialDelay(10);
		setReshowDelay(10);
	}





	public void setBiefProfilModels(Hydraulique1dProfilModel... _profs) {
		dataset_.setBiefProfilModels(_profs);
	}

	/**
	 * Definit le profil associ�.
	 * @param _ind L'indice de profil dans la liste des profils.
	 */
	public void setCurrentProfil(int _ind) {

		Hydraulique1dProfilModel pold=dataset_.getCurrentProfil();
		if (pold!=null) {
			pold.removeSelectionListener(this);
			pold.removeDataListener(this);
		}
		dataset_.setCurrentProfil(_ind);
		Hydraulique1dProfilModel p=dataset_.getCurrentProfil();
		if (p!=null) {
			p.addSelectionListener(this);
			p.addDataListener(this);
		}

		updateGraphe();
	}

	//--- Hydraulique1dProfilSelectionListener --------------------------------------------------------

	@Override
	public void pointSelectionChanged(Hydraulique1dProfilSelectionEvent _evt) {
		updateGraphe();
	}

	//--- Hydraulique1dProfilDataListener -------------------------------------------------------------

	@Override
	public void pointModified(Hydraulique1dProfilDataEvent _evt) {
		updateGraphe();
	}

	@Override
	public void pointDeleted(Hydraulique1dProfilDataEvent _evt) {
		updateGraphe();
	}

	@Override
	public void pointInserted(Hydraulique1dProfilDataEvent _evt) {
		updateGraphe();
	}

	//--- Surchage des m�thodes �venementielles souris { ----------------------------------------------

	/**
	 * Mouse pressed.
	 * Si zoom : Passe a la main a la classe mere qui le gere.
	 * Si pan : Enregistrement de la position courante.
	 * Si select : Recup�ration de l'entit�.
	 * @param _evt L'evenement souris
	 */
	@Override
	public void mousePressed(MouseEvent _evt) {
		// Zoom
		if (modeInteractif_==MODE_INTERACTIF_ZOOM || _evt.isPopupTrigger()) {
			super.mousePressed(_evt);
		}

		// Selection
		else if (modeInteractif_==MODE_INTERACTIF_SELECTION) {
			xcurrent_=_evt.getX();
			ycurrent_=_evt.getY();
			bdepl_=false;

			EntityCollection entities=getChartRenderingInfo().getEntityCollection();
			if (entities!=null) {

				// X et Y corrig�s
				Insets insets = getInsets();
				int x=(int)((_evt.getX()-insets.left)/getScaleX());
				int y=(int)((_evt.getY()-insets.top)/getScaleY());

				XYItemEntity entity=(XYItemEntity)entities.getEntity(x,y);
				if (entity!=null) {
					int item=entity.getItem();
					int iser=entity.getSeriesIndex();
					int ind=dataset_.getSelectedForSerie(iser,item);

					if (dataset_.getCurrentProfil().isSelected(ind)) { // D�ja s�lectionn�.
						if ((_evt.getModifiers()&MouseEvent.CTRL_MASK)==MouseEvent.CTRL_MASK) dataset_.getCurrentProfil().removeFromSelection(ind);
					}
					else { // Pas encore s�lectionn�.
						if (!((_evt.getModifiers()&MouseEvent.SHIFT_MASK)==MouseEvent.SHIFT_MASK) &&
								!((_evt.getModifiers()&MouseEvent.CTRL_MASK)==MouseEvent.CTRL_MASK)) dataset_.getCurrentProfil().clearSelection();
						dataset_.getCurrentProfil().addToSelection(ind);
					}
				}
				else if (!((_evt.getModifiers()&MouseEvent.SHIFT_MASK)==MouseEvent.SHIFT_MASK) && // Pas de s�lection.
						!((_evt.getModifiers()&MouseEvent.CTRL_MASK)==MouseEvent.CTRL_MASK)) {
					dataset_.getCurrentProfil().clearSelection();
				}
			}
		}

		// Pan
		else {
			xcurrent_=_evt.getX();
			ycurrent_=_evt.getY();
		}
	}

	/**
	 * Mouse pressed.
	 * Si zoom : Passe a la main a la classe mere qui le gere.
	 * Si pan : Modification des bornes du graphique pour suivre la souris.
	 * Si select : Recup�ration de l'entit�.
	 * @param _evt L'evenement souris
	 */
	@Override
	public void mouseDragged(MouseEvent _evt) {
		// Zoom
		if (modeInteractif_==MODE_INTERACTIF_ZOOM || _evt.isPopupTrigger()) {
			super.mouseDragged(_evt);
		}

		// Selection
		else if (modeInteractif_==MODE_INTERACTIF_SELECTION) {
			// Le point peut-il �tre d�plac� (distance minimale de d�placement en pixels).
			if (!bdepl_) {
				bdepl_=Math.sqrt((_evt.getX()-xcurrent_)*(_evt.getX()-xcurrent_)+(_evt.getY()-ycurrent_)*(_evt.getY()-ycurrent_))>2.;
			}
			if (!bdepl_) return;

			int[] inds=dataset_.getCurrentProfil().getSelected();
			if (inds.length==0) return;

			// Delta d�placement sur X et Y
			XYPlot plot=(XYPlot)getChart().getPlot();
			double txrel=plot.getDomainAxis().java2DToValue(_evt.getX(),getScreenDataArea(),plot.getDomainAxisEdge())-
					plot.getDomainAxis().java2DToValue(xcurrent_,getScreenDataArea(),plot.getDomainAxisEdge());
			double tyrel=plot.getRangeAxis().java2DToValue(_evt.getY(),getScreenDataArea(),plot.getRangeAxisEdge())-
					plot.getRangeAxis().java2DToValue(ycurrent_,getScreenDataArea(),plot.getRangeAxisEdge());

			bPointsMoving_=true;
			dataset_.getCurrentProfil().movePoints(inds,txrel,tyrel);

			xcurrent_=_evt.getX();
			ycurrent_=_evt.getY();
		}

		// Pan
		else {
			Rectangle2D rect=getScreenDataArea();
			rect.setRect(rect.getX()-_evt.getX()+xcurrent_,rect.getY()-_evt.getY()+ycurrent_,rect.getWidth(),rect.getHeight());
			zoom(rect);

			xcurrent_=_evt.getX();
			ycurrent_=_evt.getY();
		}
	}

	/**
	 * Mouse released.
	 * Si zoom : Passe a la main a la classe mere qui le gere.
	 * Si pan : Rien.
	 * Si select : Rien.
	 * @param _evt L'evenement souris
	 */
	@Override
	public void mouseReleased(MouseEvent _evt) {
		// Zoom
		if (modeInteractif_==MODE_INTERACTIF_ZOOM || _evt.isPopupTrigger()) {
			super.mouseReleased(_evt);
		/*
			Plot plot = graphe_.getPlot();
			if (plot instanceof XYPlot)    //I'm using a Category Plot - should be ok with other types as well (but not all)
			{
			   ((XYPlot) plot).getRangeAxis().setLowerBound(0.0);
			   ((XYPlot) plot).getRangeAxis().resizeRange(1.0, (((XYPlot) plot).getRangeAxis().getUpperBound()-((XYPlot) plot).getRangeAxis().getLowerBound())/2);
			}
			*/
		}

		// Selection
		else if (modeInteractif_==MODE_INTERACTIF_SELECTION) {
			if (bPointsMoving_) {
				bPointsMoving_=false;
				dataset_.getCurrentProfil().fireDataChanged(Hydraulique1dProfilDataEvent.MODIFICATION,dataset_.getCurrentProfil().getSelected());
			}
		}
	}

	//--- } Surchage des m�thodes �venementielles souris ----------------------------------------------

	/**
	 * @return l'image produite dans la taille courante du composant.
	 */
	public BufferedImage produceImage(Map _params) {
		return CtuluLibImage.produceImageForComponent(this, _params);
	}

	/**
	 * @return l'image produite pour une taille donnee.
	 */
	public BufferedImage produceImage(int _w, int _h, Map _params) {
		return CtuluLibImage.produceImageForComponent(this, _w, _h, _params);
	}

	/**
	 * Definit le mode interactif du graphique.
	 * @param _mode Le mode interactif.
	 */
	public void setModeInteractif(int _mode) {
		modeInteractif_=_mode;
	}

	/**
	 * Retourne le mode interactif courant.
	 * @return Le mode interactif.
	 */
	public int getModeInteractif() {
		return modeInteractif_;
	}

	/**
	 * D�finit si le ratio X/Y est egale � 1, c'est a dire si l'echelle est constante.
	 * @param _b true si l'echelle doit �tre respect�e entre X et Y.
	 */
	public void setRatioXY1(boolean _b) {
		bratioXY1_=_b;
		((NumberAxis)graphe_.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(_b);
	}

	/**
	 * Le ratio X/Y est-il egal a 1 ?
	 * @return true si l'echelle doit �tre respect�e entre X et Y
	 */
	public boolean isRatioXY1() {
		return bratioXY1_;
	}

	/**
	 * Le graphe est-il en d�placement de points ?
	 */
	public boolean isPointsMoving() {
		return bPointsMoving_;
	}

	/**
	 * Definit si les rives sont visibles
	 * @param v true si visibles.
	 */
	public void setRivesVisible(boolean v) {
		if (brivesVisibles_==v) return;
		brivesVisibles_=v;
		updateGraphe();
	}

	public void setPreviousNextVisible(boolean _b) {
		dataset_.setNextPreviousVisible(_b);
	}

	public void setXAlignment(Alignment _align) {
		dataset_.setXAlignment(_align);
	}

	public void setZAlignment(Alignment _align) {
		dataset_.setZAlignment(_align);
	}

	public void setMinMaxVisible(boolean _b) {
		if (bminmaxVisibles_==_b) return;
		bminmaxVisibles_=_b;
		updateGraphe();
	}

	/**
	 * Les rives sont elles visibles
	 * @param true si visibles.
	 */
	public boolean isRivesVisible() {
		return brivesVisibles_;
	}

	/**
	 * Definit les bornes du graphe.
	 * @param Les bornes, dans l'ordre xmin,xmax,ymin,xmax. Si null, aucun effet. Si les bornes sont erron�es, aucun effet.
	 */
	public void setZoom(double[] _bornes) {
		if (_bornes==null || _bornes.length!=4 || _bornes[0]>=_bornes[1] || _bornes[2]>=_bornes[3]) return;

		ValueAxis xaxe=graphe_.getXYPlot().getDomainAxis();
		ValueAxis yaxe=graphe_.getXYPlot().getRangeAxis();
		xaxe.setRange(_bornes[0],_bornes[1]);
		yaxe.setRange(_bornes[2],_bornes[3]);
	}

	/**
	 * Retourne les bornes courante du graphe.
	 */
	public double[] getZoom() {
		ValueAxis xaxe=graphe_.getXYPlot().getDomainAxis();
		ValueAxis yaxe=graphe_.getXYPlot().getRangeAxis();

		return new double[]{xaxe.getLowerBound(),xaxe.getUpperBound(),yaxe.getLowerBound(),yaxe.getUpperBound()};
	}

	/**
	 * Le zoom est automatique.
	 */
	public void setZoomAuto() {
		restoreAutoBounds();
	}
	
	public void updateGraphe() {
		this.updateGraphe(true);
	}

	/**
	 * Mise a jour du graphe. Les profils sont mis a jour automatiquement depuis le XYDataset.
	 */
	public void updateGraphe(boolean displayGrid) {
		NumberFormat nf_=NumberFormat.getInstance();
		nf_.setMaximumFractionDigits(Hydraulique1dResource.getFractionDigits());
		nf_.setGroupingUsed(false);

		Hydraulique1dProfilModel prf=dataset_.getCurrentProfil();
		Hydraulique1dPoint pt;

		graphe_.getXYPlot().clearDomainMarkers();
		graphe_.getXYPlot().clearRangeMarkers();
		if(!showMarker) return;
		
		if (prf.getIndicesNonNuls().length==0) return;

		if (brivesVisibles_) {
			// Limite stockage gauche
			int indLitMajGa=prf.getIndiceLitMajGa();
			pt=prf.getPoint(indLitMajGa);
			if (pt!=null && pt.X()!=null) {
				ValueMarker mkLitMajGa=new ValueMarker(pt.x());
				mkLitMajGa.setLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("SG"));
				mkLitMajGa.setLabelPaint(Color.green);
				mkLitMajGa.setPaint(Color.green);
				mkLitMajGa.setStroke(new BasicStroke(1));
				mkLitMajGa.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
				mkLitMajGa.setLabelTextAnchor(TextAnchor.TOP_LEFT);
				graphe_.getXYPlot().addDomainMarker(mkLitMajGa,Layer.BACKGROUND);
			}

			// Limite rive gauche
			int indLitMajDr=prf.getIndiceLitMajDr();
			pt=prf.getPoint(indLitMajDr);
			if (pt!=null && pt.X()!=null) {
				ValueMarker mkLitMajDr=new ValueMarker(pt.x());
				mkLitMajDr.setLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("SD"));
				mkLitMajDr.setLabelPaint(Color.green);
				mkLitMajDr.setPaint(Color.green);
				mkLitMajDr.setStroke(new BasicStroke(1));
				mkLitMajDr.setLabelAnchor(RectangleAnchor.TOP_LEFT);
				mkLitMajDr.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
				graphe_.getXYPlot().addDomainMarker(mkLitMajDr,Layer.BACKGROUND);
			}

			// Limite rive droite
			int indLitMinDr=prf.getIndiceLitMinDr();
			pt=prf.getPoint(indLitMinDr);
			if (pt!=null && pt.X()!=null) {
				ValueMarker mkLitMinDr=new ValueMarker(pt.x());
				mkLitMinDr.setLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("RD"));
				mkLitMinDr.setLabelPaint(Color.blue);
				mkLitMinDr.setPaint(Color.blue);
				mkLitMinDr.setStroke(new BasicStroke(1));
				mkLitMinDr.setLabelAnchor(RectangleAnchor.TOP_LEFT);
				mkLitMinDr.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
				graphe_.getXYPlot().addDomainMarker(mkLitMinDr,Layer.BACKGROUND);
			}

			// Limite stockage droit
			int indLitMinGa=prf.getIndiceLitMinGa();
			pt=prf.getPoint(indLitMinGa);
			if (pt!=null && pt.X()!=null) {
				ValueMarker mkLitMinGa=new ValueMarker(pt.x());
				mkLitMinGa.setLabel(Hydraulique1dResource.HYDRAULIQUE1D.getString("RG"));
				mkLitMinGa.setLabelPaint(Color.blue);
				mkLitMinGa.setPaint(Color.blue);
				mkLitMinGa.setStroke(new BasicStroke(1));
				mkLitMinGa.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
				mkLitMinGa.setLabelTextAnchor(TextAnchor.TOP_LEFT);
				graphe_.getXYPlot().addDomainMarker(mkLitMinGa, Layer.BACKGROUND);
			}
		}

		// Niveaux d'eau en resultats.
		if (!Double.isNaN(prf.getNiveauEau())) {
			

			fillGrapheWaterLevel(nf_,prf.getNiveauEau());

		}

		// Niveaux d'eau mini en resultats.
		if (bminmaxVisibles_ && !Double.isNaN(prf.getNiveauEauMin())) {
			ValueMarker mkNivEauMin = new ValueMarker(prf.getNiveauEauMin());

			mkNivEauMin.setLabel("Zmin=" + nf_.format(prf.getNiveauEauMin()));
			mkNivEauMin.setLabelPaint(new Color(0xAA, 0x00, 0xFF));
			mkNivEauMin.setPaint(new Color(0xAA, 0x00, 0xFF));
			mkNivEauMin.setStroke(new BasicStroke(1));
			mkNivEauMin.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
			mkNivEauMin.setLabelTextAnchor(TextAnchor.TOP_RIGHT);

			graphe_.getXYPlot().addRangeMarker(mkNivEauMin, Layer.BACKGROUND);
		}
		// Niveaux d'eau maxi en resultats.
		if (bminmaxVisibles_ && !Double.isNaN(prf.getNiveauEauMax())) {
			ValueMarker mkNivEauMax = new ValueMarker(prf.getNiveauEauMax());

			mkNivEauMax.setLabel("Zmax=" + nf_.format(prf.getNiveauEauMax()));
			mkNivEauMax.setLabelPaint(new Color(0xAA, 0x00, 0xFF));
			mkNivEauMax.setPaint(new Color(0xAA, 0x00, 0xFF));
			mkNivEauMax.setStroke(new BasicStroke(1));
			mkNivEauMax.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
			mkNivEauMax.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);

			graphe_.getXYPlot().addRangeMarker(mkNivEauMax, Layer.BACKGROUND);
		}
		
		
		graphe_.getXYPlot().setDomainGridlinesVisible(displayGrid);
		graphe_.getXYPlot().setRangeGridlinesVisible(displayGrid);
		
		

	}




/**
 * Try to fill the curve with color using a XYAreaRenderer
 * @author Adrien
 * @param prf
 */
	private void fillGrapheWaterLevel(NumberFormat nf,double niveauEau) {
		
		//-- current case to be removed: draw a line --//
		ValueMarker mkNivEau=new ValueMarker(niveauEau);

		mkNivEau.setLabel("Z="+nf.format(niveauEau));
		mkNivEau.setLabelPaint(Color.blue);
		mkNivEau.setPaint(Color.white);
		mkNivEau.setStroke(new BasicStroke(0));
		mkNivEau.setLabelAnchor(RectangleAnchor.TOP_LEFT);
		mkNivEau.setLabelTextAnchor(TextAnchor.BOTTOM_LEFT);

		graphe_.getXYPlot().addRangeMarker(mkNivEau,Layer.BACKGROUND);
		
		//-- objectives 2.3.6 -  add area to renderer  filling color --//
		
		defaultRenderer.setNiveauEau(true);
		defaultRenderer.setValNiveauEau(niveauEau);
		
		

	}





public Hydraulique1dGrapheProfilDataset getDataset_() {
	return dataset_;
}





public JFreeChart getGraphe_() {
	return graphe_;
}
public MascaretXYAreaRenderer getDefaultRenderer() {
	return defaultRenderer;
}
public void setDefaultRenderer(MascaretXYAreaRenderer defaultRenderer) {
	this.defaultRenderer = defaultRenderer;
}



}
