/*
 * @file         Hydraulique1dGrapheProfilDataset.java
 * @creation     2006-12-05
 * @modification $Date: 2007-02-21 16:33:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;

import java.text.*;
import java.util.*;
import org.jfree.data.*;
import org.jfree.data.general.*;
import org.jfree.data.xy.*;
import org.fudaa.fudaa.hydraulique1d.*;
import org.fudaa.fudaa.hydraulique1d.graphe.CurveOptionTargetI.Alignment;

/**
 * Un dataset pour le composant GrapheProfil d�riv� de JFreeChart. Il s'appuie sur un ensemble de profils ProfilModel. Il ne travaille
 * que sur les points non nuls.
 * @version      $Revision: 1.2 $ $Date: 2007-02-21 16:33:52 $ by $Author: bmarchan $
 * @author       Bertrand MARCHAND
 */
public class Hydraulique1dGrapheProfilDataset implements XYDataset, Hydraulique1dProfilDataListener  {
  /** Les profils du bief */
  Hydraulique1dProfilModel[] profils_=new Hydraulique1dProfilModel[]{new Hydraulique1dProfilModel()};
  /** L'indice du profil associ� */
  int iprofil_=0;
  /** Le profil associ� */
//  Hydraulique1dProfilModel profil_=new Hydraulique1dProfilModel();
  /** Les listeners */
  HashSet<DatasetChangeListener> hlisteners_=new HashSet<DatasetChangeListener>();
  /** La visibilit� ou des des profils suivant et pr�c�dent */
  boolean isPreviousNextVisble_=false;
  /** L'offset de d�calage pour l'alignement en X. Utilis� pour la repr�sentation */
  double xprevOffSet_;
  /** L'offset de d�calage pour l'alignement en Z. Utilis� pour la repr�sentation */
  double zprevOffSet_;
  /** L'offset de d�calage pour l'alignement en X. Utilis� pour la repr�sentation */
  double xnextOffSet_;
  /** L'offset de d�calage pour l'alignement en Z. Utilis� pour la repr�sentation */
  double znextOffSet_;
  /** X alignment */
  Alignment xalign_=Alignment.X_IDENT;
  /** Z alignment */
  Alignment zalign_=Alignment.Z_IDENT;
  
  

  /**
   * Constructeur.
   */
  public Hydraulique1dGrapheProfilDataset() {}
  
  /**
   * Les modeles des profils du bief
   */
  public void setBiefProfilModels(Hydraulique1dProfilModel... _profils) {
    profils_=_profils;
    setCurrentProfil(0);
  }

  /**
   * Association du profil.
   * @param _profil Le profil.
   */
  public void setCurrentProfil(int _ind) {
    if (_ind<0 || _ind>profils_.length)
      return;
    
    if (iprofil_>=0 && iprofil_<profils_.length) {
      profils_[iprofil_].removeDataListener(this);
    }
    iprofil_=_ind;
    if (iprofil_>=0 && iprofil_<profils_.length) {
      profils_[iprofil_].addDataListener(this);
    }
    
    updateOffsets();
  }

  /**
   * @param _pf Le profil � d�caler suivant Z.
   * @param _pfRef Le profil r�f�rence.
   * @return L'offset de d�calage sur Z pour un calage des courbes sur l'alignement d�fini.
   */
  private double computeZOffset(Alignment _align, Hydraulique1dProfilI _pf, Hydraulique1dProfilI _pfRef) {
    switch (_align) {
      case Z_IDENT:
      default:
        return 0;
      case Z_MIN:
        return _pf.getZ(_pf.getIndiceZMin()) - _pfRef.getZ(_pfRef.getIndiceZMin());
    }
  }

  /**
   * @param _pf Le profil � d�caler suivant X.
   * @param _pfRef Le profil r�f�rence.
   * @return L'offset de d�calage sur X pour un calage des courbes sur l'alignement d�finit.
   */
  private double computeXOffset(Alignment _align, Hydraulique1dProfilI _pf, Hydraulique1dProfilI _pfRef) {
    switch (_align) {
      case X_IDENT:
      default:
        return 0;
        
      case LEFT:
        return _pf.getCurv(_pf.getIndiceXMin()) - _pfRef.getCurv(_pfRef.getIndiceXMin());
        
      case LEFT_STORE:
        return _pf.getCurv(_pf.getIndiceLitMajGa()) - _pfRef.getCurv(_pfRef.getIndiceLitMajGa());

      case LEFT_BANK:
        return _pf.getCurv(_pf.getIndiceLitMinGa()) - _pfRef.getCurv(_pfRef.getIndiceLitMinGa());

      case RIGHT:
        return _pf.getCurv(_pf.getIndiceXMax()) - _pfRef.getCurv(_pfRef.getIndiceXMax());

      case RIGHT_STORE:
        return _pf.getCurv(_pf.getIndiceLitMajDr()) - _pfRef.getCurv(_pfRef.getIndiceLitMajDr());

      case RIGHT_BANK:
        return _pf.getCurv(_pf.getIndiceLitMinDr()) - _pfRef.getCurv(_pfRef.getIndiceLitMinDr());

      case AXIS:
        return _pf.getCurvAxeHydrauliqueOnProfil() - _pfRef.getCurvAxeHydrauliqueOnProfil();

      case X_ZMIN:
        return _pf.getCurv(_pf.getIndiceZMin()) - _pfRef.getCurv(_pfRef.getIndiceZMin());
    }
  }
  
  /**
   * Retourne le profil courant associ�.
   * @return Le profil associ�
   */
  public Hydraulique1dProfilModel getCurrentProfil() {
    return profils_[iprofil_];
  }
  
  /**
   * Retourne le profil precedent associ�.
   * @return Le profil associ�
   */
  public Hydraulique1dProfilModel getPreviousProfil() {
    if (iprofil_>0)
      return profils_[iprofil_-1];
    
    return null;
  }
  
  /**
   * Retourne le profil suivant associ�.
   * @return Le profil associ�
   */
  public Hydraulique1dProfilModel getNextProfil() {
    if (iprofil_<profils_.length-1)
      return profils_[iprofil_+1];
    
    return null;
  }
  
  /**
   * @param _b True Les profils precedent/suivant sont visibles
   */
  public void setNextPreviousVisible(boolean _b) {
    isPreviousNextVisble_=_b;
    fireDataChanged();
  }

  public void setXAlignment(Alignment _align) {
    xalign_=_align;
    updateOffsets();
  }

  public void setZAlignment(Alignment _align) {
    zalign_=_align;
    updateOffsets();
  }
  
  /**
   * Met a jour les decalages en X et Z pour les profils precedent/suivant.
   */
  public void updateOffsets() {
    if (getPreviousProfil()!=null) {
      xprevOffSet_=computeXOffset(xalign_, getPreviousProfil(), getCurrentProfil());
      zprevOffSet_=computeZOffset(zalign_, getPreviousProfil(), getCurrentProfil());
    }
    if (getNextProfil()!=null) {
      xnextOffSet_=computeXOffset(xalign_, getNextProfil(), getCurrentProfil());
      znextOffSet_=computeZOffset(zalign_, getNextProfil(), getCurrentProfil());
    }
    fireDataChanged();
  }

  
  public void updateData() {
	  fireDataChanged();
  }
  
  /**
   * Retourne l'indice du point pour une s�rie et un item donn�.
   * @param _iser Le num�ro de s�rie
   * @param _item Le num�ro de point dans la s�rie.
   */
  public int getSelectedForSerie(int _iser, int _item) {
    if (_iser==0)
      return profils_[iprofil_].getSelectedNonNuls()[_item];
    else if (_iser<=2)
      return getCurrentProfil().getIndicesNonNuls()[_item];
    else
      return -1;
  }

  /**
   * @param _iser Le num�ro de s�rie
   * @param _item Le num�ro de point dans la s�rie.
   * @return un tooltip pour le point d�sign�.
   */
  public String getToolTip(int _iser, int _item) {
    DecimalFormat df=new DecimalFormat("0.00");
    String r;

    int ind;

    if (_iser==0)
      ind=getCurrentProfil().getSelectedNonNuls()[_item];
    else if (_iser<=2)
      ind=getCurrentProfil().getIndicesNonNuls()[_item];
    else return null;

    if (getCurrentProfil().avecGeoreferencement())r=Hydraulique1dResource.HYDRAULIQUE1D.getString("Point")+": "+(ind+1)+
                                         ", "+Hydraulique1dResource.HYDRAULIQUE1D.getString("abscisse")+"="+df.format(getCurrentProfil().getPoint(ind).x())+
                                         ", "+Hydraulique1dResource.HYDRAULIQUE1D.getString("cote")+"="+df.format(getCurrentProfil().getPoint(ind).y())+
                                         ", "+Hydraulique1dResource.HYDRAULIQUE1D.getString("coordonn�es")+"=("+df.format(getCurrentProfil().getPoint(ind).Cx())+
                                         ","+df.format(getCurrentProfil().getPoint(ind).Cy())+")";
    else  r=Hydraulique1dResource.HYDRAULIQUE1D.getString("Point")+": "+(ind+1)+
            ", "+Hydraulique1dResource.HYDRAULIQUE1D.getString("abscisse")+"="+df.format(getCurrentProfil().getPoint(ind).x())+
            ", "+Hydraulique1dResource.HYDRAULIQUE1D.getString("cote")+"="+df.format(getCurrentProfil().getPoint(ind).y());

    return r;
  }

  @Override
  public DomainOrder getDomainOrder() {
    return DomainOrder.ASCENDING;
  }

  @Override
  public int getItemCount(int _iser) {
    int r;
    if (_iser==0)
      r=getCurrentProfil().getSelectedNonNuls().length;
    // Current serie
    else if (_iser<=2)
      r=getCurrentProfil().getIndicesNonNuls().length;
    // Previous serie
    else if (_iser==3)
      r=iprofil_==0 ? 0:getPreviousProfil().getIndicesNonNuls().length;
    // Next serie
    else
      r=iprofil_==profils_.length-1 ? 0:getNextProfil().getIndicesNonNuls().length;
    
    return r;
  }

  @Override
  public Number getX(int _iser, int _item) {
    return new Double(getXValue(_iser,_item));
  }

  @Override
  public double getXValue(int _iser, int _item) {
    double r;

    if (_iser==0)
      r=getCurrentProfil().getPoint(getCurrentProfil().getSelectedNonNuls()[_item]).x();
    else if (_iser<=2)
      r=getCurrentProfil().getPoint(getCurrentProfil().getIndicesNonNuls()[_item]).x();
    else if (_iser==3)
      r=iprofil_==0 ? 0:getPreviousProfil().getPoint(getPreviousProfil().getIndicesNonNuls()[_item]).x() - xprevOffSet_;
    else
      r=iprofil_==profils_.length-1 ? 0:getNextProfil().getPoint(getNextProfil().getIndicesNonNuls()[_item]).x() - xnextOffSet_;

    return r;
  }

  @Override
  public Number getY(int _iser, int _item) {
    return new Double(getYValue(_iser,_item));
  }

  @Override
  public double getYValue(int _iser, int _item) {
    double r;

    if (_iser==0)
      r=getCurrentProfil().getPoint(getCurrentProfil().getSelectedNonNuls()[_item]).y();
    else if (_iser<=2)
      r=getCurrentProfil().getPoint(getCurrentProfil().getIndicesNonNuls()[_item]).y();
    else if (_iser==3)
      r=iprofil_==0 ? 0:getPreviousProfil().getPoint(getPreviousProfil().getIndicesNonNuls()[_item]).y() - zprevOffSet_;
    else
      r=iprofil_==profils_.length-1 ? 0:getNextProfil().getPoint(getNextProfil().getIndicesNonNuls()[_item]).y() - znextOffSet_;

    return r;
  }

  @Override
  public void addChangeListener(DatasetChangeListener _listener) {
    hlisteners_.add(_listener);
  }

  @Override
  public void removeChangeListener(DatasetChangeListener _listener) {
    hlisteners_.remove(_listener);
  }

  protected void fireDataChanged() {
    for (Iterator<DatasetChangeListener> i=hlisteners_.iterator(); i.hasNext();) {
      i.next().datasetChanged(new DatasetChangeEvent(this,this));
    }
  }

  @Override
  public DatasetGroup getGroup() {
    return null;
  }

  @Override
  public void setGroup(DatasetGroup group) {}

  /**
   * Nombre de series.
   * 0 : Les points s�lectionn�s de la serie courante.
   * 1 : Les lignes de la serie courante.
   * 2 : Les points de la serie courante.
   * 3 : La serie previous
   * 4 : La serie next
   * @return Le nombre de series.
   */
  @Override
  public int getSeriesCount() {
    if (!isPreviousNextVisble_)
      return 3;
    else
      return 5;
  }

  @Override
  public Comparable<String> getSeriesKey(int _iser) {
    return getCurrentProfil().nom();
  }

  @Override
  @SuppressWarnings("rawtypes") // Librairie Java 1.5
  public int indexOf(Comparable seriesKey) {
    return 0;
  }

  @Override
  public void pointModified(Hydraulique1dProfilDataEvent _evt) {
    fireDataChanged();
  }

  @Override
  public void pointDeleted(Hydraulique1dProfilDataEvent _evt) {
    fireDataChanged();
  }

  @Override
  public void pointInserted(Hydraulique1dProfilDataEvent _evt) {
    fireDataChanged();
  }

}
