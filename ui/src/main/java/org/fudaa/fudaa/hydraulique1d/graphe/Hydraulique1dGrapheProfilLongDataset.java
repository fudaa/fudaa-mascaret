package org.fudaa.fudaa.hydraulique1d.graphe;

import org.fudaa.fudaa.hydraulique1d.Hydraulique1dPoint;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;

/**
 * Data set du profil long pour jfreechart.
 * @author Adrien Hadoux
 *
 */
public class Hydraulique1dGrapheProfilLongDataset extends Hydraulique1dGrapheProfilDataset{

	

	/**
	 * one serie to display.
	 */
	 public int getSeriesCount() {
		 return 3;
	 }
	
	 /**
	  * Item count de la courbe : on affiche autant de points que de profils selectionn�es.
	  * Si aucun item selectionn�s, on affiche rien.
	  */
	public int getItemCount(int _iser) {
		return profils_ != null ?profils_.length:0;
	}

	
	public void setCurrentProfil(int _ind) {
		//do nothing
		fireDataChanged();
	}
	
	
	public void setBiefProfilModels(Hydraulique1dProfilModel... _profils) {
		super.setBiefProfilModels(_profils);
	}
	
	public String getProfilName(int item) {
		if(profils_.length>item &&  profils_[item] !=null &&  profils_[item].nom() !=null)
		return profils_[item].nom();
		return "P"+item;
	}

	public double getXValue(int serie, int _item) {
		if(profils_ != null && profils_.length>_item)
			return profils_[_item].getAbscisse();
		else return 0;


	}


	public double getYValue(int serie, int _item) {
		if(serie == 0) {
		if(profils_ != null && profils_.length>_item)
			return profils_[_item].getNiveauEau();
		else return 0;
		}
		else if(serie == 1)  return getNiveauBas(serie, _item);
		else {
			//-- ligne d'eau � t=0 --//
			return  profils_[_item].getNiveauEauInitial_();
		}
	}
	
	public double getNiveauBas(int serie, int _item) {
		if(profils_ != null && profils_.length>_item) {
			 Hydraulique1dProfilModel model = profils_[_item];
			 Hydraulique1dPoint min = model.getPoint(0);
			 if(min == null)
				 return 0;
			 for(Hydraulique1dPoint point: model.getPoints()){
				 if( point.y()<min.y())
					 min = point;
			 }
			 return min.y();
		}
		 return 0;
	}


}
