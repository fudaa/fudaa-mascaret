/*
 * @file         Hydraulique1dGrapheResuBief.java
 * @creation     2001-09-25
 * @modification $Date: 2006-04-12 15:31:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.fudaa.ebli.graphe.Contrainte;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.GrapheComponent;
import org.fudaa.ebli.graphe.Valeur;
/**
 * Graphe pour afficher les résultats graphique du calcul.
 * Utilisé par l'éditeur Hydraulique1dResultatsEditor.<br>
 * Permet aussi d'afficher les laisses de crues.<br>
 * @see org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dResultatsGenerauxEditor
 * @version      $Revision: 1.14 $ $Date: 2006-04-12 15:31:11 $ by $Author: deniger $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dGrapheResuBief extends Hydraulique1dGrapheTableau {
  List laisse_= null;
  public Hydraulique1dGrapheResuBief() {
    super();
  }
  public void affiche(
    double[][] valeurs,
    String[] titreCourbes,
    List laisse) {
    affiche(valeurs, titreCourbes, laisse, true);
  }
  public void affiche(
    double[][] valeurs,
    String[] titreCourbes,
    List laisse,
    boolean calculBorne) {

  laisse_ = laisse;
  if (laisse.isEmpty()) {
    affiche(valeurs, titreCourbes, calculBorne);
    return;
  }
  if ( (titreCourbes != null) && (valeurs != null)) {
    if ( (valeurs.length - 1) != titreCourbes.length) {
      return;
    }
  }
  if (valeurs != null)
    valeurs_ = valeurs;
  titreCourbes_ = titreCourbes;
  int nbCourbe;
  if (valeurs_ != null)
    nbCourbe = valeurs_.length - 1;
  else
    nbCourbe = 0;
  initGraphe();
  double minX = Double.POSITIVE_INFINITY;
  double minY = Double.POSITIVE_INFINITY;
  double maxX = Double.NEGATIVE_INFINITY;
  double maxY = Double.NEGATIVE_INFINITY;
  Graphe g = getGraphe();
  
 
  //On supprime les contraintes précédentes
  int nbGrapheComponent = g.getNbGrapheComponent();
  for (int i = nbGrapheComponent-1; i >= 0; i--) {
    GrapheComponent component = g.getGrapheComponent(i);
    if (component instanceof Contrainte) {
      Contrainte contrainte = (Contrainte) component;
      g.enleve(contrainte);
    }
  }
  for (int i = 0; i < nbCourbe; i++) {
    CourbeDefault c = new CourbeDefault();
    if (titreCourbes_ != null)
      c.titre_ = titreCourbes_[i];
    c.marqueurs_ = false;
    c.aspect_.contour_ = PALETTE[i % PALETTE.length];
    int indiceY = i + 1;
    Vector vals = new Vector(valeurs_[0].length);
    for (int j = 0; j < valeurs_[0].length; j++) {
      Valeur v = new Valeur();
      double x = valeurs_[0][j];
      double y = valeurs_[indiceY][j];
      if (calculBorne) {
        if (minX > x)
          minX = x;
        if (maxX < x)
          maxX = x;
        if (minY > y)
          minY = y;
        if (maxY < y)
          maxY = y;
      }
      v.s_ = x;
      v.v_ = y;
      vals.add(v);
    }
    c.valeurs_ = vals;
    g.ajoute(c);
  }

    // pour les laisses
    for(Iterator iter = laisse_.iterator(); iter.hasNext(); ) {
      Object item = iter.next();
      if (item instanceof CourbeDefault) {
        if (calculBorne) {
          List vlaisse = ((CourbeDefault)item).valeurs_;
          for (int i = 0; i < vlaisse.size(); i++) {
            Valeur v = (Valeur) vlaisse.get(i);
            if (minX > v.s_)
              minX = v.s_;
            if (maxX < v.s_)
              maxX = v.s_;
            if (minY > v.v_)
              minY = v.v_;
            if (maxY < v.v_)
              maxY = v.v_;
          }
        }
      }else if(item instanceof Contrainte){
        if (calculBorne) {
          double valeur = ((Contrainte)item).v_;
          if (minY >valeur )
            minY = valeur;
          if (maxY < valeur)
            maxY = valeur;
        }
      }
      g.ajoute(item);
    }
    if (calculBorne) {
      if (minX != Double.POSITIVE_INFINITY)
        calculBorne(minX, minY, maxX, maxY);
    }
    this.firePropertyChange("COURBES", null, g);
    fullRepaint();
  }
  @Override
  void afficheAvecCalculBorne() {
    affiche(valeurs_, titreCourbes_, laisse_, true);
  }
  @Override
  void afficheSansCalculBorne() {
    affiche(valeurs_, titreCourbes_, laisse_, false);
  }
}
