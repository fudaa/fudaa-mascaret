/*
 * @file         Hydraulique1dGrapheTableau.java
 * @creation     2001-05-10
 * @modification $Date: 2008-03-07 09:47:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Valeur;
import org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReelModel;

import com.memoire.fu.FuLib;
/**
 * Graphe qui se met � jour par rapport au contenu d'un tableau.
 * @version      $Revision: 1.22 $ $Date: 2008-03-07 09:47:03 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dGrapheTableau
  extends Hydraulique1dAbstractGraphe
  implements PropertyChangeListener, ActionListener, TableModelListener {
  protected double[][] valeurs_;
  protected String[] titreCourbes_;
  public Hydraulique1dGrapheTableau() {
    super();
    valeurs_= null;
  }
  @Override
  void afficheAvecCalculBorne() {
    affiche(valeurs_, titreCourbes_, true);
  }
  @Override
  void afficheSansCalculBorne() {
    affiche(valeurs_, titreCourbes_, false);
  }
  // PropertyChangeListener
  @Override
  public void propertyChange(PropertyChangeEvent e) {
    super.propertyChange(e);
    fullRepaint();
  }
  @Override
  public void tableChanged(TableModelEvent evt) {
    Hydraulique1dTableauReelModel model=
      (Hydraulique1dTableauReelModel)evt.getSource();
    valeurs_= CtuluLibArray.transpose(model.getTabDouble()) ;
    afficheAvecCalculBorne();
  }
  public void affiche(double[][] valeurs, String[] titreCourbes) {
    affiche(valeurs, titreCourbes, true);
  }
  public void exportTxt(File fichier) {
    if ((titreCourbes_ != null) && (valeurs_ != null)) {
      if ((valeurs_.length - 1) != titreCourbes_.length) {
        System.err.println("Hydraulique1dGrapheTableau exportTxt() Valeurs et titreCourbe pas coh�rent !");
        return;
      }
      try {
        PrintWriter f=
          new PrintWriter(new BufferedWriter(new FileWriter(fichier)));
        System.out.println("Creation du fichier " + fichier.getName());
        for (int i= 0; i < valeurs_.length; i++) {
          if (i == 0)
            f.print(titreX_ + " (" + uniteX_ + ")");
          else
            f.print("\t" + titreCourbes_[i - 1]);
        }
        f.println();
        int nbLig= valeurs_[0].length;
        for (int i= 0; i < nbLig; i++) {
          for (int j= 0; j < (valeurs_.length - 1); j++) {
            f.print(valeurs_[j][i] + "\t");
          }
          f.println(valeurs_[valeurs_.length - 1][i]);
        }
        f.close();
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

  public void exportExcel(File fichier) {
    WritableFont arial10ptBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
    WritableCellFormat arial10BoldFormatNoBorder = new WritableCellFormat(arial10ptBold);
    if ((titreCourbes_ != null) && (valeurs_ != null)) {
      if ((valeurs_.length - 1) != titreCourbes_.length) {
        System.err.println("Hydraulique1dGrapheTableau exportXls() Valeurs et titreCourbe pas coh�rent !");
        return;
      }
      try {
        WritableWorkbook classeur = Workbook.createWorkbook(fichier);
        WritableSheet feuille1 = classeur.createSheet("Resultat",0);

        for (int i= 0; i < valeurs_.length; i++) {
          String titre="";
          if (i == 0) {
            titre=titreX_ + " (" + uniteX_ + ") ";
          }
          else {
            titre = titreCourbes_[i - 1];
          }
          feuille1.addCell(new Label(i, 0, titre, arial10BoldFormatNoBorder));
          feuille1.setColumnView(i, titre.length());
        }
        int nbLig= valeurs_[0].length;
        for (int i= 0; i < nbLig; i++) {
          for (int j= 0; j < valeurs_.length; j++) {
            feuille1.addCell(new jxl.write.Number(j,i+1,valeurs_[j][i]));
          }
         }
         classeur.write();
         classeur.close();
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  public String lanceExcel() throws IOException {
    String sortie ="";
    if (FuLib.isWindows()) {
      File fichierTemp = File.createTempFile("Fudaa", ".xls");
      fichierTemp.deleteOnExit();
      exportExcel(fichierTemp);
      String[] cmds = new String[4];
      cmds[0]="cmd";
      cmds[1]="/c";
      cmds[2]="start";
      cmds[3]=fichierTemp.getAbsolutePath();
      sortie = FuLib.runProgram(cmds);
    }
    return sortie;
  }

  /**
   * V�rifie les bonnes dimension du tableau.
   * @param tableau de r�els [indice colonne][indice ligne].
   * @return boolean Vrai si OK
   */
  private boolean verifieDimTableau(double[][] tableau) {
    for (int i = 1; i < tableau.length; i++) {
      if (tableau[0].length != tableau[i].length) {
        return false;
      }
    }
    return true;
  }

  public void affiche(
    double[][] tab,
    String[] titreCourbes,
    boolean calculBorne) {
//    double[][] valeurs = CtuluLibArray.transpose(tab);
    double[][] valeurs = tab;
    if ((titreCourbes != null) && (valeurs != null)) {
      if ((valeurs.length - 1) != titreCourbes.length) {
        System.err.println("Hydraulique1dGrapheTableau affiche(double[][] tab,String[] titreCourbes,boolean calculBorne) Valeurs et titreCourbe pas coh�rent !");
        return;
      }
    }
    if (valeurs != null) {
      if (!verifieDimTableau(valeurs)) {
        System.err.println("taille des X n'est pas coh�rent avec les Y !");
        return;
      }
      valeurs_ = valeurs;
    }
    titreCourbes_= titreCourbes;
    int nbCourbe;
    if (valeurs_ != null)
      nbCourbe= valeurs_.length - 1;
    else
      nbCourbe= 0;
    initGraphe();
    double minX= Double.POSITIVE_INFINITY;
    double minY= Double.POSITIVE_INFINITY;
    double maxX= Double.NEGATIVE_INFINITY;
    double maxY= Double.NEGATIVE_INFINITY;
    Graphe g= getGraphe();
    for (int i= 0; i < nbCourbe; i++) {
      CourbeDefault c= new CourbeDefault();
      if (titreCourbes_ != null)
        c.titre_= titreCourbes_[i];
      c.marqueurs_= false;
      c.aspect_.contour_= PALETTE[i % PALETTE.length];
      int indiceY= i + 1;
      Vector vals= new Vector(valeurs_[0].length);
      for (int j= 0; j < valeurs_[0].length; j++) {
        Valeur v= new Valeur();
        double x= valeurs_[0][j];
        double y= valeurs_[indiceY][j];
        if (calculBorne) {
          if (minX > x)
            minX= x;
          if (maxX < x)
            maxX= x;
          if (minY > y)
            minY= y;
          if (maxY < y)
            maxY= y;
        }
        v.s_= x;
        v.v_= y;
        vals.add(v);
      }
      c.valeurs_= vals;
      g.ajoute(c);
    }
    if (calculBorne) {
      if (minX != Double.POSITIVE_INFINITY)
        calculBorne(minX, minY, maxX, maxY);
    }
    this.firePropertyChange("COURBES", null, g);
    fullRepaint();
  }
  public static void main(String[] args) {
    final double[][] val1= { { 0, 60, 120, 180 }, {
        0, 10, 30, 10 }, {
        0, 40, 80, 40 }
    };
    final double[][] val2= { { 0, 5, 12.54545, 54.847 }, {
        -0.54654, 1, 3, -3 }
    };
    final double[][] val3= { { 0, 100 }, {
        10, 10 }
    };
    final double[][] val4= { { 0, 100 }, {
        0, 0 }
    };
    final String[] titresCourbes1=
      { "titresCourbes1 courbe1", "titresCourbes1 courbe2" };
    final String[] titresCourbes2= { "titresCourbes2 courbe1" };
    final String[] titresCourbes3= { "constant 10" };
    final String[] titresCourbes4= { "constant 0" };
    final Hydraulique1dGrapheTableau grapheTest=
      new Hydraulique1dGrapheTableau();
    System.out.println("pas(0,54.847)=" + Hydraulique1dAbstractGraphe.pas(0, 54.847));
    System.out.println("pas(0,100)=" + Hydraulique1dAbstractGraphe.pas(0, 100));
    System.out.println("pas(0,499)=" + Hydraulique1dAbstractGraphe.pas(0, 499));
    System.out.println("pas(-3,3)=" + Hydraulique1dAbstractGraphe.pas(-3, 3));
    System.out.println("pas(0,180)=" + Hydraulique1dAbstractGraphe.pas(0, 180));
    System.out.println("pas(0,40)=" + Hydraulique1dAbstractGraphe.pas(0, 40));
    System.out.println("pas(40,40)=" + Hydraulique1dAbstractGraphe.pas(40, 40));
    BGrapheEditeurAxes editAxes= grapheTest.getEditeurAxes();
    BGraphePersonnaliseur personGraphe= grapheTest.getPersonnaliseurGraphe();
    JPanel pnWest= new JPanel();
    pnWest.add(editAxes);
    pnWest.add(personGraphe);
    JButton bouton= new JButton("CHANGE");
    bouton.addActionListener(new ActionListener() {
      int numero_= 1;
      @Override
      public void actionPerformed(ActionEvent evt) {
        if (numero_ == 1) {
          grapheTest.setThumbnail(false, 400, 280);
          grapheTest.affiche(val2, titresCourbes2);
          numero_= 2;
        } else if (numero_ == 2) {
          grapheTest.affiche(val1, titresCourbes1);
          numero_= 3;
        } else if (numero_ == 3) {
          grapheTest.setThumbnail(true, 64, 64);
          grapheTest.affiche(null, null);
          numero_= 4;
        } else if (numero_ == 4) {
          grapheTest.setThumbnail(false, 400, 280);
          grapheTest.affiche(val3, titresCourbes3);
          numero_= 5;
        } else if (numero_ == 5) {
          grapheTest.affiche(val4, titresCourbes4);
          numero_= 1;
        }
      }
    });
    grapheTest.setLabels("TITRE GRAPHE", "x", "y", "unit� x", "unit� x");
    grapheTest.affiche(val1, titresCourbes1);
    JFrame fenetre= new JFrame("TEST Hydraulique1dGrapheTableau");
    fenetre.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
    fenetre.getContentPane().add(grapheTest, BorderLayout.CENTER);
    fenetre.getContentPane().add(bouton, BorderLayout.SOUTH);
    fenetre.getContentPane().add(pnWest, BorderLayout.WEST);
    fenetre.pack();
    fenetre.setVisible(true);
  }
}
