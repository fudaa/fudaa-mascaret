/*
 * @file         Hydraulique1dTableauCasierPlanimSurGraphe.java
 * @creation     2003-06-26
 * @modification $Date: 2005-08-16 13:48:20 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.fudaa.fudaa.hydraulique1d.editor.casier.tableau.Hydraulique1dCasierTabPlanimModel;
/**
 * Graphe de la surface utilis� dans l'�diteur de g�om�trie casier en planim�trie (Hydraulique1dCasierPlanimEditor).
 * @see org.fudaa.fudaa.hydraulique1d.editor.casier.Hydraulique1dCasierPlanimEditor
 * @see org.fudaa.fudaa.hydraulique1d.editor.casier.tableau.Hydraulique1dCasierTabPlanimModel
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 13:48:20 $ by $Author: deniger $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dTableauCasierPlanimSurGraphe
  extends Hydraulique1dGrapheTableau
  implements TableModelListener {
  public Hydraulique1dTableauCasierPlanimSurGraphe() {
    super();
    super.setLabels("SURFACE", getS("Cote"), "Surf.", "m", "m2");
  }
  @Override
  public void tableChanged(TableModelEvent evt) {
    System.out.println(
      "Hydraulique1dTableauCasierPlanimSurGraphe tableChanged(TableModelEvent evt)");
    Hydraulique1dCasierTabPlanimModel model=
      (Hydraulique1dCasierTabPlanimModel)evt.getSource();
    valeurs_= model.getTabDoubleSurf();
    afficheAvecCalculBorne();
  }
}
