/*
 * @file         Hydraulique1dTableauCasierSemiPointsGraphe.java
 * @creation     2003-06-26
 * @modification $Date: 2006-09-12 08:36:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.util.Vector;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Valeur;
import org.fudaa.fudaa.hydraulique1d.editor.casier.tableau.Hydraulique1dCasierTabXYZModel;
/**
 * Graphe utilis� dans l'�diteur de g�om�trie casier en semi de point (Hydraulique1dCasierSemiPointsEditor).
 * @see org.fudaa.fudaa.hydraulique1d.editor.casier.Hydraulique1dCasierSemiPointsEditor
 * @see org.fudaa.fudaa.hydraulique1d.editor.casier.tableau.Hydraulique1dCasierTabXYZModel
 * @version      $Revision: 1.12 $ $Date: 2006-09-12 08:36:41 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dTableauCasierSemiPointsGraphe
  extends Hydraulique1dAbstractGraphe
  implements TableModelListener {
  private CourbeDefault cbFrontieres_;
  private CourbeDefault cbInterieurs_;
  private double minX_= Double.POSITIVE_INFINITY;
  private double minY_= Double.POSITIVE_INFINITY;
  private double maxX_= Double.NEGATIVE_INFINITY;
  private double maxY_= Double.NEGATIVE_INFINITY;
  public Hydraulique1dTableauCasierSemiPointsGraphe() {
    super();
    cbFrontieres_= new CourbeDefault();
    cbFrontieres_.titre_= "Fronti�re";
    cbFrontieres_.marqueurs_= false;
    cbFrontieres_.aspect_.contour_= Color.blue;
    cbInterieurs_= new CourbeDefault();
    cbInterieurs_.titre_= "Points int�rieurs";
    cbInterieurs_.marqueurs_= true;
    cbInterieurs_.type_= "nuage";
    Graphe g= getGraphe();
    g.ajoute(cbFrontieres_);
    g.ajoute(cbInterieurs_);
    g.copyright_= false;
    g.legende_= false;
    g.marges_.droite_= 30;
    g.marges_.gauche_= 40;
    g.marges_.haut_= 20;
    g.marges_.bas_= 20;
    super.axeX_.grille_= true;
    super.axeY_.grille_= true;
    setPreferredSize(new Dimension(400, 400));
    super.setLabels("SEMIS DE POINTS", "X", "Y", "m", "m");
  }
  @Override
  void afficheAvecCalculBorne() {
    affiche(true);
  }
  @Override
  void afficheSansCalculBorne() {
    affiche(false);
  }
  // PropertyChangeListener
  @Override
  public void propertyChange(PropertyChangeEvent e) {
    fullRepaint();
  }
  @Override
  public void tableChanged(TableModelEvent evt) {
    Hydraulique1dCasierTabXYZModel model=
      (Hydraulique1dCasierTabXYZModel)evt.getSource();
    if (model.isPointsFrontieres()) {
      double[][] valeursCbPtsFrontieres= model.getTabDouble();
      tabDoubleToCourbe(CtuluLibArray.transpose(valeursCbPtsFrontieres) , cbFrontieres_, true);
    } else if (model.isPointsInterieurs()) {
      double[][] valeursCbPtsInterieurs= model.getTabDouble();
      tabDoubleToCourbe(CtuluLibArray.transpose(valeursCbPtsInterieurs), cbInterieurs_, false);
    }
    afficheAvecCalculBorne();
  }


  protected void tabDoubleToCourbe(
    double[][] valeurs,
    CourbeDefault cb,
    boolean ptsFrontiere) {
    double minX= Double.POSITIVE_INFINITY;
    double minY= Double.POSITIVE_INFINITY;
    double maxX= Double.NEGATIVE_INFINITY;
    double maxY= Double.NEGATIVE_INFINITY;
    int taille=0;
    if (valeurs.length != 0) {
        taille = valeurs[0].length;
    }
    Vector vals= new Vector(taille);
    for (int i= 0; i < taille; i++) {
      Valeur v= new Valeur();
      v.s_= valeurs[0][i];
      v.v_= valeurs[1][i];
      if ((!ptsFrontiere) && (valeurs[2][i] != Double.POSITIVE_INFINITY)) {
        v.titre_= "" + valeurs[2][i];
      }
      vals.add(v);
      minX= Math.min(minX, v.s_);
      maxX= Math.max(maxX, v.s_);
      minY= Math.min(minY, v.v_);
      maxY= Math.max(maxY, v.v_);
    }
    minX_= Math.min(minX, minX_);
    minY_= Math.min(minY, minY_);
    maxX_= Math.max(maxX, maxX_);
    maxY_= Math.max(maxY, maxY_);
    if ((ptsFrontiere) && (taille >= 3)) {
      Valeur v= new Valeur();
      v.s_= valeurs[0][0];
      v.v_= valeurs[1][0];
      vals.add(v);
    }
    cb.valeurs_= vals;
  }
  protected void affiche(boolean calculBorne) {
    if (calculBorne) {
      if (minX_ != Double.POSITIVE_INFINITY)
        calculBorne(minX_, minY_, maxX_, maxY_);
    }
    this.firePropertyChange("COURBES", null, getGraphe());
    fullRepaint();
  }
}
