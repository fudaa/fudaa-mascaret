/*
 * @file         Hydraulique1dTableauLoiSeuilGraphe.java
 * @creation     2001-03-29
 * @modification $Date: 2007-11-20 11:43:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.graphe;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Graphe utilis� dans la libraires des lois hydraulique pour repr�senter le graphe d'un loi seuil (MetierSeuilLoi).
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:43:31 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dTableauLoiSeuilGraphe
  extends Hydraulique1dGrapheTableau {
  public Hydraulique1dTableauLoiSeuilGraphe() {
    super();
    super.setLabels("Loi Seuil", "d�bit", "Cote amont", "m3/s", "m");
  }
  public void setTitresCourbes(String[] titresCourbes) {
    return; // ne doit pas �tre utilis�
  }
}
class GereAction2 implements ActionListener {
  int numero_= 1;
  Hydraulique1dTableauLoiSeuilGraphe grapheTest_;
  double[][] val1_;
  double[][] val2_;
  public GereAction2(
    Hydraulique1dTableauLoiSeuilGraphe _grapheTest,
    double[][] _val1,
    double[][] _val2) {
    grapheTest_= _grapheTest;
    val1_= _val1;
    val2_= _val2;
  }
  @Override
  public void actionPerformed(ActionEvent evt) {
    if (numero_ == 1) {
      grapheTest_.setThumbnail(false, 400, 280);
      grapheTest_.affiche(val2_, null);
      numero_= 2;
    } else if (numero_ == 2) {
      grapheTest_.affiche(val1_, null);
      numero_= 3;
    } else if (numero_ == 3) {
      grapheTest_.setThumbnail(true, 64, 64);
      numero_= 1;
    }
  }
}
