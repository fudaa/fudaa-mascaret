package org.fudaa.fudaa.hydraulique1d.graphe;

import java.awt.Graphics;
import java.awt.image.ImageObserver;
import org.fudaa.ebli.graphe.Graphe;
/**
 * Mascaret grid graphe extemds ebli graph and display grid .
 * @author Adrien
 *
 */
public class MascaretGridGraphe extends Graphe{

	private boolean displayGrid= true;

	public void dessine(final Graphics _g, final int _x, final int _y, final int _w, final int _h, final int _nr, final ImageObserver _applet) {
		getHorizontalAxe().grille_ = displayGrid;
		getFirstVerticalAxe().grille_=displayGrid;
		super.dessine(_g, _x, _y, _w, _h, _nr, _applet);
	}


	public boolean isDisplayGrid() {
		return displayGrid;
	}

	public void setDisplayGrid(boolean displayGrid) {
		this.displayGrid = displayGrid;
		
	}

}
