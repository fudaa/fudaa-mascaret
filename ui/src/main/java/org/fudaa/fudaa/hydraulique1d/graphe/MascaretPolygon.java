package org.fudaa.fudaa.hydraulique1d.graphe;

import java.awt.Polygon;

import org.fudaa.fudaa.hydraulique1d.graphe.MascaretXYAreaRendererProfilLong.AreaPoint;

/**
 * Polygone qui garde une trace des points doubles
 * @author Adrien
 *
 */
public class MascaretPolygon extends Polygon{
	AreaPoint hg, hd, bd, bg ;

	public AreaPoint getHg() {
		return hg;
	}

	public void setHg(AreaPoint hg) {
		this.hg = hg;
	}

	public AreaPoint getHd() {
		return hd;
	}

	public void setHd(AreaPoint hd) {
		this.hd = hd;
	}

	public AreaPoint getBd() {
		return bd;
	}

	public void setBd(AreaPoint bd) {
		this.bd = bd;
	}

	public AreaPoint getBg() {
		return bg;
	}

	public void setBg(AreaPoint bg) {
		this.bg = bg;
	}
	
}
