package org.fudaa.fudaa.hydraulique1d.graphe;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer2;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
/**
 * Renderer pour remplir une zone sp�cifique.
 * @author Adrien Hadoux
 *
 */
public class MascaretXYAreaRenderer extends XYAreaRenderer2{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8095398246623317631L;

	public static class AreaPoint {
		public double x;
		public AreaPoint(double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}
		public double y;
	}
	
	XYLineAndShapeRenderer defaultRenderer;
	boolean isNiveauEau = true;
	double valNiveauEau;
	private List<Integer> itemLineToDraw;
	final static int CURVE_SERIE = 1;
	
	public MascaretXYAreaRenderer(XYLineAndShapeRenderer defaultRenderer) {
		this.defaultRenderer = defaultRenderer;
		setDefaultColor();
		//setStroke(new BasicStroke(4));
	}

	
	public void setDefaultColor() {
		setPaint(new Color(132,166,188));
	}
	public void changeColorConcentration(Color colorFond) {
		setPaint(colorFond);
	}
	
	private boolean mustDrawPolygone(double x1, double y1) {
		return isNiveauEau && y1 <= valNiveauEau;
	}
	
	/**
	 * Build the polygon corresponding to the shape between the curve and the level f water.
	 * @param domainAxis
	 * @param rangeAxis
	 * @param dataset
	 * @return
	 */
	public List<Shape> buildShapeProfilTravers(ValueAxis domainAxis, ValueAxis rangeAxis,XYDataset dataset, XYPlot plot, Rectangle2D dataArea) {
		List<List<AreaPoint>> listPoly = new ArrayList<List<AreaPoint>>();

		List<AreaPoint> list = new ArrayList<AreaPoint>();
		itemLineToDraw = new ArrayList<Integer>();
		boolean first = true;
		boolean last = false;
		List<AreaPoint> firstsA = new ArrayList<AreaPoint>();
		List<AreaPoint> lastsA = new ArrayList<AreaPoint>();
		List<AreaPoint> firstsB = new ArrayList<AreaPoint>();
		List<AreaPoint> lastsB = new ArrayList<AreaPoint>();
		
		AreaPoint firstA=null, firstB=null, lastA = null, lastB = null; 
		//-- step 1 - determine all the point under the level --//
		for(int item=0; item< dataset.getItemCount(CURVE_SERIE);item++){
			double x = dataset.getXValue(CURVE_SERIE, item);
			double y = dataset.getYValue(CURVE_SERIE, item);
			
			if(last) {
				//-- si last = true, on a termin� un polygone car la courbe est repass� au dessus du niveau de l'eau--//
				//-- il faut donc en cr�er un nouveau si la courbe venait a repasser en dessous. --//
				first = true;
				last= false;
				listPoly.add(list);
				list = new ArrayList<AreaPoint>();
				firstsA.add(firstA);
				lastsA.add(lastA);
				firstsB.add(firstB);
				lastsB.add(lastB);
				firstA = null;
				firstB= null;
			}
			
			if(y<=valNiveauEau ) {
				AreaPoint p = new AreaPoint(x,y);
				itemLineToDraw.add(item);
				list.add(p);
				if(first) {
					if(item !=0)
						firstA = new AreaPoint(dataset.getXValue(CURVE_SERIE, item-1),dataset.getYValue(CURVE_SERIE, item-1));
					firstB = p;
					first = false;
				}
			} else {
				//--check if we go up the level --//
				if(!first && !last) {
					//-- the pevious is the last ! --//
					if(item !=0) {
						lastA = new AreaPoint(dataset.getXValue(CURVE_SERIE, item-1),dataset.getYValue(CURVE_SERIE, item-1));
						lastB = new AreaPoint(x,y);
					}
					last = true;
					
				}
			}
		}
		
		if(!listPoly.contains(list)){
			listPoly.add(list);
			firstsA.add(firstA);
			lastsA.add(lastA);
			firstsB.add(firstB);
			lastsB.add(lastB);
		}
		
		List<Shape> shapes = new ArrayList<Shape>();
		for(int i=0;i<listPoly.size();i++) {
		
			firstA = firstsA.get(i);
			lastA = lastsA.get(i);
			firstB = firstsB.get(i);
			lastB = lastsB.get(i);
			list = listPoly.get(i);
			
			//-- step 2 - determine first point --//
			double coeff , cst, xIntersect;
			if(firstA !=null && firstB != null) {
				
				if(firstA.x - firstB.x != 0) {
					coeff = (double)(firstA.y - firstB.y)/ (double)(firstA.x - firstB.x);
					cst = firstA.y - coeff*firstA.x;
					xIntersect = (double)(valNiveauEau - cst  )/coeff;
					list.add(0,new AreaPoint(xIntersect,valNiveauEau));
				} else {
					//-- warning droite perpendiculaire a l'axe des absisces --//
					list.add(0,new AreaPoint(firstA.x,valNiveauEau));
				}
				
			}
			
			
			//-- step 3 - determine last point --//
			if(lastA !=null && lastB != null) {
				if(lastA.x - lastB.x != 0) {
				coeff = (double)(lastA.y - lastB.y)/ (double)(lastA.x - lastB.x);
				cst = lastA.y - coeff*lastA.x;
				xIntersect = (double)(valNiveauEau - cst  )/coeff;
				list.add(new AreaPoint(xIntersect,valNiveauEau));
				} else {
					//-- warning droite perpendiculaire a l'axe des absisces --//
					list.add(new AreaPoint(lastA.x,valNiveauEau));
				}
			}
			
			//-- step 4 - construct poly --//		
			Polygon poly = new Polygon();
			for(AreaPoint p: list) {
				poly.addPoint(
						(int)domainAxis.valueToJava2D(p.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(p.y, dataArea, plot.getRangeAxisEdge()));
			}
			shapes.add(poly);
		}
		
		return shapes;
		
	}
	
	public void drawItem(Graphics2D g2,
			XYItemRendererState state,
			Rectangle2D dataArea,
			PlotRenderingInfo info,
			XYPlot plot,
			ValueAxis domainAxis,
			ValueAxis rangeAxis,
			XYDataset dataset,
			int series,
			int item,
			CrosshairState crosshairState,
			int pass) {

		if (!getItemVisible(series, item)) {
			return;
		}
		// get the data point...
		double x1 = dataset.getXValue(series, item);
		double y1 = dataset.getYValue(series, item);
		if (Double.isNaN(y1)) {
			y1 = 0.0;
		}

		List<Shape> shapes = null;
		
		
		if( series == CURVE_SERIE && mustDrawPolygone(x1,y1)) {
			shapes = buildShapeProfilTravers(domainAxis, rangeAxis, dataset,plot,dataArea);
		}
		
		
		Paint paint = getItemPaint(series, item);
		Stroke stroke = getItemStroke(series, item);
		g2.setPaint(paint);
		g2.setStroke(stroke);


		if( series == CURVE_SERIE && mustDrawPolygone(x1,y1)) {	
			for(Shape shape:shapes)
			g2.fill(shape);
			//-- warning: must draw he prevous line curve again to draw onto the polygon shape --//
			if(itemLineToDraw != null) 
				for(Integer itemLine:itemLineToDraw)
				defaultRenderer.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, itemLine, crosshairState, pass);
		}else {
			//-- render line default renderer --//
			defaultRenderer.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item, crosshairState, pass);
		}
	
	}

	


	public boolean isNiveauEau() {
		return isNiveauEau;
	}

	public void setNiveauEau(boolean isNiveauEau) {
		this.isNiveauEau = isNiveauEau;
	}

	public double getValNiveauEau() {
		return valNiveauEau;
	}

	public void setValNiveauEau(double valNiveauEau) {
		this.valNiveauEau = valNiveauEau;
	}


}
