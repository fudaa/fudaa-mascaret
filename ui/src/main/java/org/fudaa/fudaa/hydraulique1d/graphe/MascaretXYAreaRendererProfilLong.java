package org.fudaa.fudaa.hydraulique1d.graphe;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilLongPane.ColorAndRangePolygone;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer2;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
/**
 * Renderer pour remplir la zone du profil long
 * @author Adrien Hadoux
 *
 */
public class MascaretXYAreaRendererProfilLong extends XYAreaRenderer2{



	private static final long serialVersionUID = 1L;


	public static class AreaPoint {
		public double x;
		public double y;
		public AreaPoint() {
			
		}
		public AreaPoint(double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}
		
	}

	XYLineAndShapeRenderer defaultRenderer;
	boolean isNiveauEau = true;

	boolean traceProfil = true;

	private List<Integer> itemLineToDraw;
	private boolean drawProfileLine=false;
	final static int CURVE_SERIE = 0;
	final static int CURVE_BOTTOM = 1;
	final static int CURVE_Z_INITIAL = 2;
	
	final static int End_CURVE_SERIE = 0;

	public MascaretXYAreaRendererProfilLong(XYLineAndShapeRenderer defaultRenderer) {
		this.defaultRenderer = defaultRenderer;
		setPaint(new Color(132,166,188));
		//setStroke(new BasicStroke(4));
	}


	private boolean mustDrawPolygone(double x1, double y1,XYDataset dataset, int item) {
		
		
		
		double valNiveauEau = ((Hydraulique1dGrapheProfilLongDataset)dataset).getNiveauBas(CURVE_SERIE, item);
		//return isNiveauEau && y1 >= valNiveauEau;
		return true;
	}

	
	public static class Profilshape extends Line2D.Double {

		String label;
		public Profilshape(String label) {
			super();
			this.label = label;

		}

		public Profilshape(double arg0, double arg1, double arg2, double arg3, String label) {
			super(arg0, arg1, arg2, arg3);
			this.label = label;

		}

		public Profilshape(Point2D arg0, Point2D arg1, String label) {
			super(arg0, arg1);
			this.label = label;

		}
		
		
		
	}
	
	/**
	 * Build the polygon corresponding to the shape between the curve and the level f water.
	 * @param domainAxis
	 * @param rangeAxis
	 * @param dataset
	 * @return
	 */
	public List<Shape> buildShapeProfilLong(ValueAxis domainAxis, ValueAxis rangeAxis,XYDataset dataset, XYPlot plot, Rectangle2D dataArea) {
		List<AreaPoint> listcoordonnees = new ArrayList<AreaPoint>();
		List<AreaPoint> listniveauEau = new ArrayList<AreaPoint>();
		itemLineToDraw = new ArrayList<Integer>();
		List<Shape>listShape = new ArrayList<Shape>();
		boolean first = true;
		boolean last = false;
		AreaPoint firstA=null, firstB=null, lastA = null, lastB = null; 
		//-- step 1 - determine all the point under the level --//
		for(int item=0; item< dataset.getItemCount(CURVE_SERIE);item++){
			double x = dataset.getXValue(CURVE_SERIE, item);
			double y = dataset.getYValue(CURVE_SERIE, item);
			double valNiveauEau = ((Hydraulique1dGrapheProfilLongDataset)dataset).getNiveauBas(CURVE_SERIE, item);
			if(y>=valNiveauEau) {
				AreaPoint p = new AreaPoint(x,y);
				itemLineToDraw.add(item);
				//-- on ajoute a la liste des coordonn�es le point p mais aussi le point niveau d'eau --//
				String nomProfil = ((Hydraulique1dGrapheProfilLongDataset)dataset).getProfilName(item);
				Profilshape shape = new Profilshape((int)domainAxis.valueToJava2D(p.x, dataArea, plot.getDomainAxisEdge())
						,(int)rangeAxis.valueToJava2D(p.y, dataArea, plot.getRangeAxisEdge()),
						(int)domainAxis.valueToJava2D(p.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(((Hydraulique1dGrapheProfilLongDataset)dataset).getNiveauBas(0, item), dataArea, plot.getRangeAxisEdge()),nomProfil);
				listShape.add(shape);


				listcoordonnees.add(p);
				listniveauEau.add(new AreaPoint(x,valNiveauEau));

			} 
		}


		//-- step 4 - construct poly --//
		//if(!paintConcentration){
			Polygon polygonGlobal = new Polygon();
			for(AreaPoint p: listcoordonnees) {
				polygonGlobal.addPoint(
						(int)domainAxis.valueToJava2D(p.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(p.y, dataArea, plot.getRangeAxisEdge()));
			}
			//-- ensuite on termine par la liste des points d'eau dans l'ordre inverse pour avoir l'enveloppe convexe--//
			for(int i=listniveauEau.size()-1;i>=0;i--) {
				AreaPoint p = listniveauEau.get(i);
				polygonGlobal.addPoint(
						(int)domainAxis.valueToJava2D(p.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(p.y, dataArea, plot.getRangeAxisEdge()));
			}
	
			listShape.add(0,polygonGlobal);
		//}else {
		if(paintConcentration)	{
			listConcentrations = new ArrayList<Shape>();
			for(int i=0;i<listniveauEau.size()-1;i++) {
				MascaretPolygon poly = new MascaretPolygon();
				AreaPoint p = listcoordonnees.get(i);
				AreaPoint p2 = listcoordonnees.get(i+1);
				AreaPoint fond1 = listniveauEau.get(i);
				AreaPoint fond2 = listniveauEau.get(i+1);
				poly.addPoint(
						(int)domainAxis.valueToJava2D(p.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(p.y, dataArea, plot.getRangeAxisEdge()));
				poly.addPoint(
						(int)domainAxis.valueToJava2D(p2.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(p2.y, dataArea, plot.getRangeAxisEdge()));
				poly.addPoint(
						(int)domainAxis.valueToJava2D(fond2.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(fond2.y, dataArea, plot.getRangeAxisEdge()));
				poly.addPoint(
						(int)domainAxis.valueToJava2D(fond1.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(fond1.y, dataArea, plot.getRangeAxisEdge()));
				
				poly.setHg(p);
				poly.setHd(p2);
				poly.setBd(fond2);
				poly.setBg(fond1);
				
				listConcentrations.add(poly);
			}
		}
		return listShape;

	}
	
	private boolean paintConcentration = false;
	private List<Shape> listConcentrations = new ArrayList<Shape>();
	private ColorAndRangePolygone[][] colorConcentrations ; 

	
	private int previousSerie=-1; 
	private int previousItem=-1; 
	
	public void drawItem(Graphics2D g2,
			XYItemRendererState state,
			Rectangle2D dataArea,
			PlotRenderingInfo info,
			XYPlot plot,
			ValueAxis domainAxis,
			ValueAxis rangeAxis,
			XYDataset dataset,
			int series,
			int item,
			CrosshairState crosshairState,
			int pass) {

		
		
		if (!getItemVisible(series, item)) {
			return;
		}
		
		//-- adrien - optimisation du rendu --//
		if(item == previousItem +1 && previousSerie == series) {
			previousItem = item;		
			return ;
		} else {
			previousItem = item;
			previousSerie = series;
		}
		
		// get the data point...
		double x1 = dataset.getXValue(series, item);
		double y1 = dataset.getYValue(series, item);
		if (Double.isNaN(y1)) {
			y1 = 0.0;
		}

		List<Shape> hotspot = null;
		

		if( series == CURVE_SERIE && mustDrawPolygone(x1,y1,dataset, item)) {
			hotspot = buildShapeProfilLong(domainAxis, rangeAxis, dataset,plot,dataArea);

		}


		Paint paint = getItemPaint(series, item);
		Stroke stroke = getItemStroke(series, item);
		g2.setPaint(paint);
		g2.setStroke(stroke);


		if( series == CURVE_SERIE && mustDrawPolygone(x1,y1,dataset, item)) {
			if(hotspot.size() >0){
			    if(!paintConcentration)
			    	g2.fill(hotspot.get(0));
			    else {
			    	//-- draw all polygon representing concentration --//
			    	Paint previousPaint = g2.getPaint();
			    	for(int i=0;i<listConcentrations.size();i++) {
			    		MascaretPolygon poly = (MascaretPolygon)listConcentrations.get(i);
			    		if(colorConcentrations != null && colorConcentrations.length>i) {
			    			//TODO split en plusieurs concentrations
			    			//g2.setPaint(colorConcentrations[i]);
			    		  ColorAndRangePolygone[] degradeCouleurs = colorConcentrations[i];
			    		  int nbPolygones = degradeCouleurs.length;
			    		  
			    		  //-- on doit d�couper le polygone poly en plusieurs polygones et leur affecter la couleur de d�grad� . --//
			    		  //-- chaque poly est constitu� de 4 points, 1 point par extremit� : HG,HD,BD,BG --//
			    		  AreaPoint pHG = poly.hg;
			    		  AreaPoint pHD = poly.hd;
			    		  AreaPoint pBD = poly.bd;
			    		  AreaPoint pBG = poly.bg;
			    		  
			    		  
			    		  //-- il faut trouver nbPolygones points entre pHG et PHD --//
			    		  double tailleHaut = Math.abs(pHG.x - pHD.x);
			    		  double tailleBas = Math.abs(pBG.x - pBD.x);
			    		  
			    		  //-- els points par lesquels on commence le trac� des sous - polygones --// 
			    		  AreaPoint previousHG = pHG;
			    		  AreaPoint previousBG = pBG;
			    		  for(int p=0;p<nbPolygones;p++) {
			    			  Color colorPolygone = degradeCouleurs[p].c;
			    			  AreaPoint hg = previousHG;
			    			  AreaPoint bg = previousBG;
			    			  
			    			  AreaPoint hd = new AreaPoint();
			    			  AreaPoint bd = new AreaPoint();
			    			  // le  x vaut l'incr�ment  multipli� par p
			    			  double xHaut = hg.x + tailleHaut *degradeCouleurs[p].taillePolygone;
			    			  double xBas = bg.x + tailleBas *degradeCouleurs[p].taillePolygone;
			    			  
			    			  hd.x = xHaut;
			    			  bd.x = xBas;
			    			  hd.y = interpole(xHaut, pHG, pHD); // droite d'�quation pHG pHD
			    			  bd.y = interpole(xBas, pBG, pBD); // droite d'�quation pBG pBD
			    			  
			    			  Polygon subPolygon = new Polygon();
			    			 
			    			  /*
			    			   * poly.addPoint(
						(int)domainAxis.valueToJava2D(fond1.x, dataArea, plot.getDomainAxisEdge()),
						(int)rangeAxis.valueToJava2D(fond1.y, dataArea, plot.getRangeAxisEdge()));
			    			   */
			    			  subPolygon.addPoint(
				  						(int)domainAxis.valueToJava2D(hg.x, dataArea, plot.getDomainAxisEdge()),
				  						(int)rangeAxis.valueToJava2D(hg.y, dataArea, plot.getRangeAxisEdge()));
			    			  subPolygon.addPoint(
				  						(int)domainAxis.valueToJava2D(hd.x, dataArea, plot.getDomainAxisEdge()),
				  						(int)rangeAxis.valueToJava2D(hd.y, dataArea, plot.getRangeAxisEdge()));
			    			  subPolygon.addPoint(
				  						(int)domainAxis.valueToJava2D(bd.x, dataArea, plot.getDomainAxisEdge()),
				  						(int)rangeAxis.valueToJava2D(bd.y, dataArea, plot.getRangeAxisEdge()));
				    			  
			    			  subPolygon.addPoint(
				  						(int)domainAxis.valueToJava2D(bg.x, dataArea, plot.getDomainAxisEdge()),
				  						(int)rangeAxis.valueToJava2D(bg.y, dataArea, plot.getRangeAxisEdge()));
				    			
			    			  g2.setColor(colorPolygone);			    			  
			    			  g2.fill(subPolygon);
			    			  
			    			  //-- les previous deviennent les point de droite pour construire les polygones � partir de la suite --//
			    			  previousHG = hd ;
			    			  previousBG = bd;
			    			  
			    		  }
			    		  
			    		}
			    		else { 
			    			g2.setPaint(previousPaint);
			    			g2.fill(poly);
			    		}
			    	}
			    	g2.setPaint(previousPaint);
			    }
			}
			if(drawProfileLine) {
				Color previous = g2.getColor();
				Stroke prevStroke = g2.getStroke();

				Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
				g2.setStroke(dashed);
				g2.setColor(Color.BLACK);
				for(int i=1;i<hotspot.size();i++) {
					g2.draw(hotspot.get(i));
					
					if( hotspot.get(i) instanceof Profilshape){
						Profilshape shape = (Profilshape) hotspot.get(i);
						Font previousFont = g2.getFont();
						Font font = new Font( "SansSerif", Font.PLAIN, 10 );
						g2.setFont(font );
						//-- before rotation 90 --//
						if(shape.label != null) {
							char[] label = shape.label.toCharArray();
							Line2D.Double f = ((Line2D.Double)hotspot.get(i));
							double hauteur = 0;//Math.abs(f.y1-f.y2)*0.2;
							//g2.drawString(""+label[0], (int)f.x1, (int)(f.y2+hauteur));
							int cpt=2;//0;
							FontMetrics fm=g2.getFontMetrics(font);
							int height = fm.getHeight();
							for(char letter:label) {
								g2.drawString(""+letter, (int)f.x1, (int)(f.y2+hauteur +(cpt*height)));
								cpt++;
							}
						}
						//--change translation to display vertically the text --//
						/*
						AffineTransform previousTo90 = g2.getTransform();
						AffineTransform at = new AffineTransform();
						at.rotate(Math.PI / 2);						 
						g2.setTransform(at);						
						Line2D.Double f = ((Line2D.Double)hotspot.get(i));
						double xf = f.x1;
						double yf = f.y2;//valueToJava2D
						//-- on remet value normmale --//
						
						g2.drawString(shape.label, (int)yf, - (int)xf );						
						g2.setTransform(previousTo90);	
						g2.setFont(previousFont);
						*/
						
						
					}
				}
				g2.setColor(previous);	
				g2.setStroke(prevStroke);	
			}
			
			
			
			//-- warning: must draw the prevous line curve again to draw onto the polygon shape --//
			if(itemLineToDraw != null) 
				for(Integer itemLine:itemLineToDraw) {
				
					defaultRenderer.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, CURVE_BOTTOM, itemLine, crosshairState, pass);
					defaultRenderer.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, CURVE_Z_INITIAL, itemLine, crosshairState, pass);
					
				}
		}else {
			//-- render line default renderer --//
			//defaultRenderer.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item, crosshairState, pass);
			defaultRenderer.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, CURVE_BOTTOM, item, crosshairState, pass);
			defaultRenderer.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, CURVE_Z_INITIAL, item, crosshairState, pass);
			
		}

	}

	

	private double[] computePolygoneSizeWithConcentration(int nbPolygones,
			ColorAndRangePolygone[] degradeCouleurs, AreaPoint pHG, AreaPoint pHD,
			AreaPoint pBD, AreaPoint pBG) {
		// TODO Auto-generated method stub
		return null;
	}


	public double interpole(double x, AreaPoint a, AreaPoint b) {
		if((a.x -b.x) == 0 ) {
			return (a.y +b.y)/2;
		}
		double coeff = (a.y-b.y)/(a.x-b.x);
		double cst = a.y - coeff*a.x;
		double y = x*coeff + cst;
		
		if(y < Math.min(a.y, b.y))
			return Math.min(a.y, b.y);
		else if(y > Math.max(a.y, b.y))
			return Math.max(a.y,b.y);
		else return y;
			 
		
	}

	
	public boolean isNiveauEau() {
		return isNiveauEau;
	}

	public void setNiveauEau(boolean isNiveauEau) {
		this.isNiveauEau = isNiveauEau;
	}


	public boolean isDrawProfileLine() {
		return drawProfileLine;
	}


	public void setDrawProfileLine(boolean drawProfileLine) {
		this.drawProfileLine = drawProfileLine;
	}


	public boolean isPaintConcentration() {
		return paintConcentration;
	}


	public void setPaintConcentration(boolean paintConcentration) {
		this.paintConcentration = paintConcentration;
	}


	public List<Shape> getListConcentrations() {
		return listConcentrations;
	}


	public void setListConcentrations(List<Shape> listConcentrations) {
		this.listConcentrations = listConcentrations;
	}


	public ColorAndRangePolygone[][] getColorConcentrations() {
		return colorConcentrations;
	}


	public void setColorConcentrations(ColorAndRangePolygone[][] colorConcentrations) {
		this.colorConcentrations = colorConcentrations;
	}



}
