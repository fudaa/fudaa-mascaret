package org.fudaa.fudaa.hydraulique1d.graphe3D;

import java.awt.BorderLayout;
import java.awt.Dimension;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceProfilesModel;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceProfilesModel.EbliGraphDataStructure;
import org.fudaa.ebli.graphe3D.ui.panel.EG3dBtnPanel;
import org.fudaa.ebli.graphe3D.ui.panel.EG3dGraphPanel;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuPanel;

/**
 * 
 * Repr�sentation 3d des profils du bief.
 * @author Adrien Hadoux
 *
 */
public class PanelGraphe3D extends BuPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MetierBief bief;
	private final  BuPanel grapheContainer;
	public PanelGraphe3D() {
		super();
		
		setLayout(new BorderLayout());
		grapheContainer = new BuPanel(new BorderLayout());
		//setLayout(new BorderLayout());
		add(grapheContainer);
		grapheContainer.setPreferredSize(new Dimension(600,600));
		//add(graphe,BorderLayout.CENTER);
	}


	public void setObject(MetierHydraulique1d _n) {
		if(_n instanceof MetierBief)
			bief = (MetierBief) _n;
		
	}

	public void feedGraph() {
		setValeurs();
	}
	
	
	protected void setValeurs() {
		if(bief == null)
			return;
		
        //-- creation du model de graphe 3d
		EG3dSurfaceProfilesModel dataModel = new EG3dSurfaceProfilesModel();
		
		int nbProfils = bief.profils().length;	
		int nbMaxPoints = 0;
		
		
		
		for(int prof=0;prof<nbProfils; prof++) {
			MetierProfil profil = bief.profils()[prof];
			if( nbMaxPoints<profil.points().length) 
				nbMaxPoints = profil.points().length;
		}
		int nbPointReach = nbMaxPoints;
		int nbProfilsReach = nbProfils;
		if(nbMaxPoints <nbProfils )	
			nbPointReach = nbProfils;
		
		if(nbProfilsReach<nbMaxPoints)
			nbProfilsReach = nbMaxPoints;
		//-- x data : les pt.x des profils --//
		float[][] xData = new float[nbPointReach][nbProfilsReach]; //les x des courbes il faut ramener les x entre -10 et 10
		
		//-- listeProfils data : les pt.y des profils pour chaque abscisse--//
		float[][] listeProfils = new float[nbPointReach][nbProfilsReach];
		
		//-- abscisseData data : les abscisses des profils --//
		float[] abscisseData =  new float[nbProfilsReach]; // les absicces des biefs, il faut ramener les abscisses entre -10 et 10.
		
		for(int prof=0;prof<nbProfils; prof++) {
			MetierProfil profil = bief.profils()[prof];
			abscisseData[prof] = (float)profil.abscisse();
			
			for(int j=0 ; j<profil.points().length;j++) {
				listeProfils[j][prof] = (float)profil.points()[j].y;
				
				
				//if(nbMaxPoints == profil.points().length)
					xData[j][prof] = (float)profil.points()[j].x;
			}
			if(profil.points().length<nbPointReach) {
				//-- ahadoux il va manquer des points --//
				for(int i=profil.points().length;i<nbPointReach;i++){
					listeProfils[i][prof] = listeProfils[i-1][prof] ;
					xData[i][prof] = xData[i-1][prof];
				}
			}
				
		}
		
		if(nbProfils<nbProfilsReach) {
			for(int prof=nbProfils;prof<nbProfilsReach; prof++) {
				
				abscisseData[prof] = abscisseData[prof-1];
				
				for(int j=0 ; j<nbPointReach;j++) {
					listeProfils[j][prof] = listeProfils[j][prof-1];
					xData[j][prof] = xData[j][prof-1] ;
				}
				
			}
		}
		
		/*
		if(nbMaxPoints<nbProfils) {
			for(int i=nbMaxPoints;i<nbProfils; i++) {
				xData[i] = xData[i-1];
				
				for(int prof=0;prof<nbProfils; prof++) {
					listeProfils[i][prof]=listeProfils[i-1][prof];
				}
			}
		}
		*/
		
		EbliGraphDataStructure s = new EbliGraphDataStructure(xData, listeProfils,abscisseData,nbProfils);
		
		dataModel.setValues(s);
		
		EG3dGraphPanel jsp = new EG3dGraphPanel();
		jsp.setPreferredSize(new Dimension(600,400));
		translate3dGraph(jsp.getBtnPanel());
		
		grapheContainer.removeAll();
		grapheContainer.add(jsp, BorderLayout.CENTER);
		
		jsp.getView().addData(dataModel);
/*		
		JFrame jf = new JFrame("test");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.getContentPane().add(jsp, BorderLayout.CENTER);
		jf.pack();
		jf.setVisible(true);

		jsp.setModel(dataModel);
	*/	
		
	}

	public void translate3dGraph(EG3dBtnPanel p){
		p.getDensityType().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mode densit�"));
		p.getWireframeType().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Type Wireframe"));
		p.getContourType().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mode contour"));
		p.getSurfaceType().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Type Surface"));
		p.getWireframePTType().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Type Nuage de points"));
		p.getFogMode().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mode Fog"));
		p.getDualShadeMode().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nuance"));
		p.getGrayScaleMode().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Nuance de gris"));
		p.getSpectrumMode().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mode spectre"));
		p.getHiddenMode().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mode cach�"));
		p.getMesh().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Voir le maillage"));
		p.getScaleBox().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Redimensionner"));
		p.getBtnDisplayGrids().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Afficher la grille"));
		p.getBtnDisplayZ().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Afficher l'axe Z"));
		p.getBtnDisplayXY().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Afficher les axes X et Y"));
		p.getBtnShowBox().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Afficher les extr�mit�s"));
		p.getDensityType().setText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Mode densit�"));
	}
	
	
	protected boolean getValeurs() {
		// TODO Auto-generated method stub
		return false;
	}


	
}
