package org.fudaa.fudaa.hydraulique1d.graphe3D;

import java.awt.BorderLayout;
import java.awt.Dimension;




import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceProfilesModel;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceProfilesModel.EbliGraphDataStructure;

/*
import javax.swing.JComponent;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Range;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.builder.concrete.OrthonormalGrid;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;
*/
import com.memoire.bu.BuPanel;

/**
 * 
 * Représentation 3d des profils du bief.
 * @author Adrien Hadoux
 *
 */
public class PanelGraphe3Dzyx extends BuPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MetierBief bief;
	private final  BuPanel grapheContainer;
	
	public PanelGraphe3Dzyx() {
		super();
		
		setLayout(new BorderLayout());
		grapheContainer = new BuPanel(new BorderLayout());
		//setLayout(new BorderLayout());
		add(grapheContainer);
		grapheContainer.setPreferredSize(new Dimension(600,600));
		//add(graphe,BorderLayout.CENTER);
	}


	public void setObject(MetierHydraulique1d _n) {
		if(_n instanceof MetierBief)
			bief = (MetierBief) _n;
		
	}

	public void feedGraph() {
		setValeurs();
	}
	
	
	protected void setValeurs() {
		if(bief == null)
			return;
		
        //-- creation du model de graphe 3d
		EG3dSurfaceProfilesModel dataModel = new EG3dSurfaceProfilesModel();
		
		int nbProfils = bief.profils().length;	
		int nbMaxPoints = 0;
		
		
		
		for(int prof=0;prof<nbProfils; prof++) {
			MetierProfil profil = bief.profils()[prof];
			if( nbMaxPoints<profil.points().length) 
				nbMaxPoints = profil.points().length;
		}
		int nbPointReach = nbMaxPoints;
		if(nbMaxPoints <nbProfils )	
			nbPointReach = nbProfils;
		
		
		//-- x data : les pt.x des profils --//
		final float[][] xData = new float[nbPointReach][nbProfils]; //les x des courbes il faut ramener les x entre -10 et 10
		
		//-- listeProfils data : les pt.y des profils pour chaque abscisse--//
		final float[][] listeProfils = new float[nbPointReach][nbProfils];
		
		//-- abscisseData data : les abscisses des profils --//
		final float[] abscisseData =  new float[nbProfils]; // les absicces des biefs, il faut ramener les abscisses entre -10 et 10.
		
		for(int prof=0;prof<nbProfils; prof++) {
			MetierProfil profil = bief.profils()[prof];
			abscisseData[prof] = (float)profil.abscisse();
			
			for(int j=0 ; j<profil.points().length;j++) {
				listeProfils[j][prof] = (float)profil.points()[j].y;
				
				
				//if(nbMaxPoints == profil.points().length)
					xData[j][prof] = (float)profil.points()[j].x;
			}
			if(profil.points().length<nbPointReach) {
				//-- ahadoux il va manquer des points --//
				for(int i=profil.points().length;i<nbPointReach;i++){
					listeProfils[i][prof] = listeProfils[i-1][prof] ;
					xData[i][prof] = xData[i-1][prof];
				}
			}
				
		}
		
		
		EbliGraphDataStructure s = new EbliGraphDataStructure(xData, listeProfils,abscisseData,nbProfils);
		/*
		
		//-- Propre a xyz --//
		
		 Mapper mapper = new Mapper() {
	            public double f(double x, double y) {
	                
	            	//if((int)x < listeProfils.length)
	            		//Wif((int)y < listeProfils[(int)x].length)
	            	return listeProfils[(int)x][(int)y];
	            }
	        };
	        
	        

	        // Define range and precision for the function to plot
	        Range xrange = new Range(0, nbPointReach-1);
	        Range yrange = new Range(0, nbProfils-1);
	        int xSteps = nbPointReach;
	        int ySteps = nbProfils;

	        
	        // Create the object to represent the function over the given range.
	        OrthonormalGrid grid = new OrthonormalGrid(xrange, xSteps, yrange, ySteps);
	        final Shape surface = Builder.buildOrthonormal(grid, mapper);
	        surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(), surface.getBounds().getZmax(), new Color(1, 1, 1, .5f)));
	        surface.setFaceDisplayed(true);
	        surface.setWireframeDisplayed(true);
	        
	        // Create a chart
	        org.jzy3d.chart.Chart chart = AWTChartComponentFactory.chart(Quality.Advanced, "swing");
	        chart.getScene().getGraph().add(surface);
	        chart.setAxeDisplayed(true);
	        chart.getView().setAxeBoxDisplayed(true);
	        //chart.getView().setBackgroundColor(Color.GRAY);
		
		
		
		
		grapheContainer.removeAll();
		grapheContainer.add((JComponent) chart.getCanvas(), BorderLayout.CENTER);
		
		*/

		
	}

	
	protected boolean getValeurs() {
		// TODO Auto-generated method stub
		return false;
	}


	
}
