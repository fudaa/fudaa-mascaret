/*
 * @file         Hydraulique1dIHMRepository.java
 * @creation     1999-07-26
 * @modification $Date: 2007-11-20 11:43:17 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;

/**
 * Classe qui g�re les IHM helper et donc les �diteurs.<br>
 * En particulier, on utilise toujours sa m�thode ��getIhmHelper�� pour afficher un �diteur.<br>
 * Respecte le "design pattern" singleton (retourne l'instance unique avec getInstance()).<br>
 * @version      $Revision: 1.25 $ $Date: 2007-11-20 11:43:17 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class Hydraulique1dIHMRepository implements FudaaProjetListener {
  /**
   * Intance unique d'elle-m�me.
   */
  private static Hydraulique1dIHMRepository instance_=null;

  /**
   * R�f�rence vers le projet qui g�re les fichiers '.masc'.
   */
  private FudaaProjet projet_;

  /**
   * R�f�rence vers l'�tude 1D m�tier.
   */
  private MetierEtude1d etude_;

  /**
   * Map entre le nom de l'ihmHelper d�j� utilis� (la cl�) et l'instance de l'ihmHelper.
   */
  private Map<String, Hydraulique1dIHM_Base> ihmHelpers_;

  /**
   * Constructeur par d�faut qui initialise la Map ihmHelpers_.
   */
  private Hydraulique1dIHMRepository() {
    init();
  }

  /**
   * Retourne l'instance unique de cette classe ("design pattern" singleton).
   * @return Hydraulique1dIHMRepository
   */
  public static Hydraulique1dIHMRepository getInstance() {
    if (instance_ == null) {
      instance_ = new Hydraulique1dIHMRepository();
    }
    return instance_;
  }

  /**
   * Initialise le projet.
   * @param p FudaaProjet
   */
  public void setProjet(FudaaProjet p) {
    if (p == projet_)
      return;
    if (projet_ != null)
      projet_.removeFudaaProjetListener(this);
    projet_= p;
    projet_.addFudaaProjetListener(this);
  }

  /**
   * Initialise l'�tude m�tier.
   * @param e MetierEtude1d
   */
  public void setEtude(MetierEtude1d e) {
    etude_= e;
    Iterator<Hydraulique1dIHM_Base> ite = ihmHelpers_.values().iterator();
    while(ite.hasNext()) {
       ite.next().setEtude(etude_);
    }
  }

  /**
   * Ferme toutes les fenetres ihm
   */
  public void fermer() {
    Iterator<Hydraulique1dIHM_Base> ite = ihmHelpers_.values().iterator();
    while(ite.hasNext()) {
       ite.next().fermer();
    }
  }

  /**
   * Ferme toutes les fenetres resultat
   */
  public void fermerResultats() {
    if (!ihmHelpers_.containsKey("ResultatsGeneraux")) {
      ((Hydraulique1dIHM_Base)ihmHelpers_.get("ResultatsGeneraux")).fermer();
    }
    if (!ihmHelpers_.containsKey("GraphesResultats")) {
      ((Hydraulique1dIHM_Base)ihmHelpers_.get("GraphesResultats")).fermer();
    }
  }

  /**
   * Initialise la Map ihmHelpers_ a une taille de 50.
   */
  public void init() {
    ihmHelpers_ = new HashMap<String,Hydraulique1dIHM_Base>(50);
  }
  // Impl�mentation de l'interface FudaaProjetListener
  /**
   * En cas de changement du projet (RESULTS_CLEARED), on ferme les fen�tres r�sultats.
   * @param e FudaaProjetEvent
   */
  @Override
  public void dataChanged(FudaaProjetEvent e) {
    //FudaaProjet p= (FudaaProjet)e.getSource();
    switch (e.getID()) {
      case FudaaProjetEvent.PARAM_ADDED :
      case FudaaProjetEvent.PARAM_IMPORTED :
        {
          //**      setProjetParametres(p);
        }
      case FudaaProjetEvent.RESULT_ADDED :
      case FudaaProjetEvent.RESULTS_CLEARED :
        {
          fermerResultats();
          //**      setProjetResultats(p);
          break;
        }
    }
  }

  /**
   * Si le projet est ferm� ou ouvert, on ferme toutes les fen�tres.
   * @param e FudaaProjetEvent
   */
  @Override
  public void statusChanged(FudaaProjetEvent e) {
//    FudaaProjet p= (FudaaProjet)e.getSource();
    if ((e.getID() == FudaaProjetEvent.PROJECT_OPENED)
      || (e.getID() == FudaaProjetEvent.PROJECT_CLOSED))
      fermer();
    //**      setProjetParametres(p);
    //**      setProjetResultats(p);
  }

  /**
   * Permet de r�cup�rer une instance unique de l'IhmHelper dont le nom est pass� en argument.
   * @param nomIhmHelper Le nom de l'IhmHelper (Nom de la Classe sans le pr�fix Hydraulique1dIHM_.
   * @return Hydraulique1dIHM_Base
   * 
   * @deprecated Not safe. Use {@link #getIhmHelper(Class)} instead
   */
  public Hydraulique1dIHM_Base getIhmHelper(String nomIhmHelper) {
    try {
        @SuppressWarnings("unchecked")
        Class<Hydraulique1dIHM_Base> clazz = (Class<Hydraulique1dIHM_Base>)Class.forName(
            "org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_" +
            nomIhmHelper);
        return getIhmHelper(clazz);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    return null;
  }
  

  /**
   * Permet de r�cup�rer une instance unique de la classe pass�e en argument.
   * @param _clazz La classe de l'instance.
   * @return L'instance.
   */
  @SuppressWarnings("unchecked")
  public <T extends Hydraulique1dIHM_Base> T getIhmHelper(Class<T> _clazz) {
    String nomIhmHelper=_clazz.getName().substring("org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_".length());
    
    try {
      if (!ihmHelpers_.containsKey(nomIhmHelper)) {
        Constructor<?>[] constructeurs = _clazz.getConstructors();
        for (int i = 0; i < constructeurs.length; i++) {
          Class<?>[] classesParamConstruc = constructeurs[i].getParameterTypes();
          if (classesParamConstruc.length == 1) {
            if (classesParamConstruc[0].equals(MetierEtude1d.class)) {
              Object[] tmp = new Object[1];
              tmp[0] = etude_;
              Hydraulique1dIHM_Base ihmHelper = (Hydraulique1dIHM_Base)
                  constructeurs[i].newInstance(tmp);
              ihmHelpers_.put(nomIhmHelper, ihmHelper);
            }
          }
        }
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    return (T)ihmHelpers_.get(nomIhmHelper);
  }
  
  public Hydraulique1dIHM_Apport2 APPORT2() {
    return (Hydraulique1dIHM_Apport2) getIhmHelper("Apport2");
  }
  public Hydraulique1dIHM_Casier CASIER() {
    return (Hydraulique1dIHM_Casier) getIhmHelper("Casier");
  }
  public Hydraulique1dIHM_CasierLiaison CASIER_LIAISON() {
    return (Hydraulique1dIHM_CasierLiaison) getIhmHelper("CasierLiaison");
  }
  public Hydraulique1dIHM_CasierPlanim CASIER_PLANIM() {
    return (Hydraulique1dIHM_CasierPlanim) getIhmHelper("CasierPlanim");
  }
  public Hydraulique1dIHM_CasierSemiPoints CASIER_SEMI_POINTS() {
    return (Hydraulique1dIHM_CasierSemiPoints) getIhmHelper("CasierSemiPoints");
  }
  public Hydraulique1dIHM_ConditionsInitiales CONDITION_INITIALE() {
    return (Hydraulique1dIHM_ConditionsInitiales) getIhmHelper("ConditionsInitiales");
  }
  public Hydraulique1dIHM_CruesCalage CRUES_CALAGE() {
    return (Hydraulique1dIHM_CruesCalage) getIhmHelper("CruesCalage");
  }
  public Hydraulique1dIHM_Deversoir DEVERSOIR() {
    return (Hydraulique1dIHM_Deversoir) getIhmHelper("Deversoir");
  }
  public Hydraulique1dIHM_ExtremiteLibre EXTREMITE_LIBRE() {
    return (Hydraulique1dIHM_ExtremiteLibre) getIhmHelper("ExtremiteLibre");
  }
  public Hydraulique1dIHM_GraphesResultats GRAPHES_RESULTATS() {
    return (Hydraulique1dIHM_GraphesResultats) getIhmHelper("GraphesResultats");
  }
  public Hydraulique1dIHM_Laisse LAISSE() {
    return (Hydraulique1dIHM_Laisse) getIhmHelper("Laisse");
  }
  public Hydraulique1dIHM_LoiHydraulique2 LOI_HYDRAULIQUE2() {
    return (Hydraulique1dIHM_LoiHydraulique2) getIhmHelper("LoiHydraulique2");
  }


  public Hydraulique1dIHM_LibraryLoiHydraulique LIBRARY_LOI() {
    return (Hydraulique1dIHM_LibraryLoiHydraulique) getIhmHelper("LibraryLoiHydraulique");
  }
  public Hydraulique1dIHM_LigneInitiale LIGNE_INITIALE() {
    return (Hydraulique1dIHM_LigneInitiale) getIhmHelper("LigneInitiale");
  }
  public Hydraulique1dIHM_ConcentrationsInitiales CONCENTRATIONS_INITIALES() {
  return (Hydraulique1dIHM_ConcentrationsInitiales) getIhmHelper("ConcentrationsInitiales");
}

  public Hydraulique1dIHM_ListingsCalage LISTINGS_CALAGE() {
    return (Hydraulique1dIHM_ListingsCalage) getIhmHelper("ListingsCalage");
  }
  public Hydraulique1dIHM_Maillage2 MAILLAGE2() {
    return (Hydraulique1dIHM_Maillage2) getIhmHelper("Maillage2");
  }
  public Hydraulique1dIHM_Meteo METEO() {
  return (Hydraulique1dIHM_Meteo) getIhmHelper("Meteo");
  }
  public Hydraulique1dIHM_ModeleQEau MODELE_QUALITE_EAU() {
    return (Hydraulique1dIHM_ModeleQEau) getIhmHelper("ModeleQEau");
  }
  public Hydraulique1dIHM_Noyau NOYAU() {
    return (Hydraulique1dIHM_Noyau) getIhmHelper("Noyau");
  }
  public Hydraulique1dIHM_ParamCalage PARAM_CALAGE() {
    return (Hydraulique1dIHM_ParamCalage) getIhmHelper("ParamCalage");
  }
  public Hydraulique1dIHM_ParamGeneraux PARAM_GENERAUX() {
    return (Hydraulique1dIHM_ParamGeneraux) getIhmHelper("ParamGeneraux");
  }
  public Hydraulique1dIHM_ParamsGenerauxQualiteDEau PARAMS_GENERAUX_TRACER() {
      return (Hydraulique1dIHM_ParamsGenerauxQualiteDEau) getIhmHelper(
              "ParamsGenerauxQualiteDEau");
  }

  public Hydraulique1dIHM_ParamsPhysiqueQE PARAMS_PHYSIQUES_QE() {
      return (Hydraulique1dIHM_ParamsPhysiqueQE) getIhmHelper(
              "ParamsPhysiqueQE");
  }
  public Hydraulique1dIHM_ParamReprise PARAM_REPRISE() {
    return (Hydraulique1dIHM_ParamReprise) getIhmHelper("ParamReprise");
  }
  public Hydraulique1dIHM_ParamResultat PARAM_RESULTAT() {
    return (Hydraulique1dIHM_ParamResultat) getIhmHelper("ParamResultat");
  }
  public Hydraulique1dIHM_ParamResultatQualiteDEau PARAM_RESULTAT_QUALITE_EAU() {
  return (Hydraulique1dIHM_ParamResultatQualiteDEau) getIhmHelper("ParamResultatQualiteDEau");
}

  public Hydraulique1dIHM_ParamTemporel PARAM_TEMPOREL() {
    return (Hydraulique1dIHM_ParamTemporel) getIhmHelper("ParamTemporel");
  }
  public Hydraulique1dIHM_Planimetrage2 PLANIMETRAGE2() {
    return (Hydraulique1dIHM_Planimetrage2) getIhmHelper("Planimetrage2");
  }
  public Hydraulique1dIHM_Bief BIEF() {
    return (Hydraulique1dIHM_Bief) getIhmHelper("Bief");
  }
  public Hydraulique1dIHM_ResultatsGeneraux RESULTATS_GENERAUX() {
    return (Hydraulique1dIHM_ResultatsGeneraux) getIhmHelper("ResultatsGeneraux");
  }
  public Hydraulique1dIHM_SectionsParSections SECTIONS_PAR_SECTIONS() {
    return (Hydraulique1dIHM_SectionsParSections) getIhmHelper("SectionsParSections");
  }
  public Hydraulique1dIHM_SectionsParSerie2 SECTIONS_PAR_SERIE2() {
    return (Hydraulique1dIHM_SectionsParSerie2) getIhmHelper("SectionsParSerie2");
  }
 /* public Hydraulique1dIHM_SeuilTrans SEUIL_TRANS() { //les SEUIL_TRANS deviennent des SEUIL_LOI
    return (Hydraulique1dIHM_SeuilTrans) getIhmHelper("SeuilTrans");
  }*/
  public Hydraulique1dIHM_SeuilLoi SEUIL_LOI() {
  return (Hydraulique1dIHM_SeuilLoi) getIhmHelper("SeuilLoi");
  }
  public Hydraulique1dIHM_SeuilVanne SEUIL_VANNE() {
    return (Hydraulique1dIHM_SeuilVanne) getIhmHelper("SeuilVanne");
  }
  public Hydraulique1dIHM_SeuilAvecLoi SEUIL_AVEC_LOI() {
    return (Hydraulique1dIHM_SeuilAvecLoi) getIhmHelper("SeuilAvecLoi");
  }
  public Hydraulique1dIHM_SitesStockage SITES_STOCKAGE() {
    return (Hydraulique1dIHM_SitesStockage) getIhmHelper("SitesStockage");
  }
  public Hydraulique1dIHM_Source SOURCE() {
  return (Hydraulique1dIHM_Source) getIhmHelper("Source");
 }
  public Hydraulique1dIHM_Tableaux TABLEAUX() {
    return (Hydraulique1dIHM_Tableaux) getIhmHelper("Tableaux");
  }
  public Hydraulique1dIHM_VariableResultat VARIABLE_RESULTAT() {
    return (Hydraulique1dIHM_VariableResultat) getIhmHelper("VariableResultat");
  }
  public Hydraulique1dIHM_VisuInitiale VISU_INITIALE() {
    return (Hydraulique1dIHM_VisuInitiale) getIhmHelper("VisuInitiale");
  }
  public Hydraulique1dIHM_ZonesFrottement ZONES_FROTTEMENT() {
    return (Hydraulique1dIHM_ZonesFrottement) getIhmHelper("ZonesFrottement");
  }
  public Hydraulique1dIHM_ZonesFrottementACaler ZONES_FROTTEMENT_A_CALER() {
    return (Hydraulique1dIHM_ZonesFrottementACaler) getIhmHelper("ZonesFrottementACaler");
  }
  public Hydraulique1dIHM_ZonesFrottementCalees ZONES_FROTTEMENT_CALEES() {
    return (Hydraulique1dIHM_ZonesFrottementCalees) getIhmHelper("ZonesFrottementCalees");
  }
  public Hydraulique1dIHM_ZonesSeches ZONES_SECHES() {
    return (Hydraulique1dIHM_ZonesSeches) getIhmHelper("ZonesSeches");
  }
  public Hydraulique1dIHM_ParamsSediment PARAMS_SEDIMENT() {
    return getIhmHelper(Hydraulique1dIHM_ParamsSediment.class);
  }
}
