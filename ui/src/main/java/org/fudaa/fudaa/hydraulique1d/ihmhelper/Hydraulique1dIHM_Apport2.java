/*
 * @file         Hydraulique1dIHM_Apport2.java
 * @creation     2001-04-03
 * @modification $Date: 2007-11-20 11:43:15 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dApportEditor;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des apports de d�bit et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par Hydraulique1dTableauxEditor et Hydraulique1dReseauMouseAdapter.<br>
 * @version      $Revision: 1.11 $ $Date: 2007-11-20 11:43:15 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_Apport2 extends Hydraulique1dIHM_Base {
  private Hydraulique1dApportEditor edit_;
  private MetierBief bief_;
  private MetierApport apport_;
  public Hydraulique1dIHM_Apport2(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dApportEditor();
      installContextHelp(edit_);
      edit_.setDonneesHydrauliques(etude_.donneesHydro());
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.setBiefParent(bief_);
    edit_.setObject(apport_);
    edit_.show();
  }
  @Override
  public void objetCree(H1dObjetEvent e) {
    if (edit_ != null)
      edit_.objetCree(e);
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
    if (edit_ != null)
      edit_.objetSupprime(e);
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
    if (edit_ != null)
      edit_.objetModifie(e);
  }
  public void setBief(MetierBief _bief) {
    bief_= _bief;
  }
  public void setApport(MetierApport _apport) {
    apport_= _apport;
  }
  protected void installContextHelp(Hydraulique1dCustomizer e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/singularites.html");
  }
}
