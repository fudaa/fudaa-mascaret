/*
 * @file         Hydraulique1dIHM_Base.java
 * @creation     1999-07-26
 * @modification $Date: 2007-11-20 11:43:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import java.util.Vector;

import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEventListener;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Classe abstraite de tous les autres IHM Helper.
 * Elle �coute tous les �v�nements du mod�le m�tier Hydraulique1d.<br>
 * Contient une m�thode abstraite editer() et une r�f�rence vers l'�tude et vers un �diteur.<br>
 * @see org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer
 * @see MetierEtude1d
 * @version      $Revision: 1.16 $ $Date: 2007-11-20 11:43:13 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
abstract public class Hydraulique1dIHM_Base implements H1dObjetEventListener {
  protected Vector editors_;
//  protected DObjetEventListenerSupport evtSupport_;
  protected MetierEtude1d etude_;
  public Hydraulique1dIHM_Base(MetierEtude1d _e) {
    editors_= new Vector();
    setEtude(_e);
  }
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    String className= getClass().getName();
    int index= className.lastIndexOf('.');
    if (index > 0)
      className= className.substring(index + 1);
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e, className + ".html");
  }
  public void fermer() {
    for (int i= 0; i < editors_.size(); i++) {
      Hydraulique1dCustomizer edit= (Hydraulique1dCustomizer)editors_.get(i);
      if (edit.isShowing())
        edit.fermer();
    }
    editors_.clear();
  }
  protected void addEditor(Hydraulique1dCustomizer e) {
    if (!editors_.contains(e))
      editors_.add(e);
  }
  protected void closeEditors() {
    for (int i= 0; i < editors_.size(); i++)
       ((Hydraulique1dCustomizer)editors_.get(i)).fermer();
  }
  public void setEtude(MetierEtude1d _e) {
    etude_= _e;
/*    evtSupport_= DObjetEventListenerSupport.createEventSupport();
    evtSupport_.clientListener(this);
    UsineLib.findUsine().addObjetEventListener(
      (DHydraulique1dEventListenerSupport)evtSupport_.tie());*/
    Notifieur.getNotifieur().addObjetEventListener(this);
    reinit();
  }
  protected void reinit() {
    boolean showing= false;
    closeEditors();
    if (showing)
      editer();
  }
  abstract public void editer();
  protected void listenToEditor(Hydraulique1dCustomizer e) {
    addEditor(e);
  }
  @Override
  public void objetCree(H1dObjetEvent e) {
//    System.out.println("Hydraulique1dIHM_BASE "+this.getClass().getName());
//    System.out.println("objetCree(H1dObjetEvent e) e.getSource().enChaine()="+e.getSource().enChaine());
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
//    System.out.println("Hydraulique1dIHM_BASE "+this.getClass().getName());
//    System.out.println("objetSupprime(H1dObjetEvent e) e.getSource().enChaine()="+e.getSource().enChaine());
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
//    System.out.println("Hydraulique1dIHM_BASE "+this.getClass().getName());
//    System.out.println("objetModifie(H1dObjetEvent e) e.getSource().enChaine()="+e.getSource().enChaine());
  }
  public final static String getS(final String _s) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(_s);
  }
}
