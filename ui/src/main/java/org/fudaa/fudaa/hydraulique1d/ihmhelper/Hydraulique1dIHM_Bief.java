/*
 * @file         Hydraulique1dIHM_Bief.java
 * @creation     1999-07-26
 * @modification $Date: 2007-11-20 11:43:15 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dBiefEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des biefs et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par Hydraulique1dTableauxEditor et Hydraulique1dReseauMouseAdapter.<br>
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:43:15 $ by $Author: bmarchan $
 * @author       Axel von Arnim
 */
public class Hydraulique1dIHM_Bief extends Hydraulique1dIHM_Base {
  private Hydraulique1dBiefEditor edit_;
  private MetierBief bief_;
  public Hydraulique1dIHM_Bief(MetierEtude1d _e) {
    super(_e);
  }
  public void setBief(MetierBief bief) {
    bief_= bief;
  }

  public void setEtude(MetierEtude1d _e) {
	  etude_ =_e;
	  
	  final MetierBief[] datas = _e.reseau().biefs();
	  final DefaultComboBoxModel<MetierBief> model = new DefaultComboBoxModel<MetierBief>(datas) ;
	  if(edit_ != null)
		  edit_.getListBiefs().setModel(model);
	  
  }
  
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_ = new Hydraulique1dBiefEditor(getS("Bief n�X"));
      edit_.getListBiefs().addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			changeBiefSelection();
			
		}

		private void changeBiefSelection() {
				
		    MetierBief selected = (MetierBief) edit_.getListBiefs().getSelectedItem();
		    setBief(selected);
		    editer();
			
		}
	});
      edit_.setObject(etude_);
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass = Hydraulique1dResource.getAssistant();
      if (ass != null) {
        ass.addEmitters(edit_);
      }
    }
    if (bief_ != null) {
      edit_.setObject(bief_);
      edit_.setTitle(getS("Bief n�") + (bief_.indice()+1));
    }
    
    //-- aha - feed planimetrage --//
    edit_.getPlanimetrage().setObject(etude_.reseau());   
    listenToEditor(edit_.getPlanimetrage());
    
    edit_.getPanel3d().setObject(bief_);
    //edit_.getPanel3dxyz().setObject(bief_);
    edit_.show();
  }

  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null) {
      return;
    }
    ( (Hydraulique1dBaseApplication) Hydraulique1dBaseApplication.FRAME).
        getImplementation().installContextHelp(e.getRootPane(),
                                               "mascaret/geom.html");
  }

}
