/*
 * @file         Hydraulique1dIHM_CasierLiaison.java
 * @creation     2003-06-10
 * @modification $Date: 2007-11-20 11:43:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.casier.Hydraulique1dCasierLiaisonEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des liaisons avec des casiers et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par Hydraulique1dTableauxEditor et Hydraulique1dReseauMouseAdapter.<br>
 * @version      $Revision: 1.6 $ $Date: 2007-11-20 11:43:13 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_CasierLiaison extends Hydraulique1dIHM_Base {
  Hydraulique1dCasierLiaisonEditor edit_;
  MetierLiaison liaison_;
  public Hydraulique1dIHM_CasierLiaison(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dCasierLiaisonEditor();
      edit_.setObject(liaison_);
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.show();
  }
  public void setLiaison(MetierLiaison liaison) {
    liaison_= liaison;
    if (edit_ != null)
      edit_.setObject(liaison_);
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/liaison_casier.html");
  }
}
