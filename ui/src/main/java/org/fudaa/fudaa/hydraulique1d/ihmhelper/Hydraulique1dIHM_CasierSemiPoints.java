/*
 * @file         Hydraulique1dIHM_CasierSemiPoints.java
 * @creation     2003-06-27
 * @modification $Date: 2007-11-20 11:43:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.casier.Hydraulique1dCasierSemiPointsEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur de g�om�trie casier en semis de points et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par Hydraulique1dCasierEditor.<br>
 * @version      $Revision: 1.7 $ $Date: 2007-11-20 11:43:14 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_CasierSemiPoints extends Hydraulique1dIHM_Base {
  Hydraulique1dCasierSemiPointsEditor edit_;
  MetierCasier casier_;
  public Hydraulique1dIHM_CasierSemiPoints(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dCasierSemiPointsEditor();
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.setObject(casier_.geometrie());
    edit_.show();
  }
  public void setCasier(MetierCasier casier) {
    casier_= casier;
    if (edit_ != null) {
      edit_.setObject(casier_.geometrie());
    }
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/casier.html");
  }
}
