/*
 * @file         Hydraulique1dIHM_ConditionsInitiales.java
 * @creation     2001-09-20
 * @modification $Date: 2007-11-20 11:43:15 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dConditionsInitialesEditor;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuCommonImplementation;
/**
 * Classe faisant le lien entre l'�diteur des conditions initiales et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      $Revision: 1.9 $ $Date: 2007-11-20 11:43:15 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_ConditionsInitiales
  extends Hydraulique1dIHM_Base {
  Hydraulique1dConditionsInitialesEditor edit_;
  public Hydraulique1dIHM_ConditionsInitiales(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dConditionsInitialesEditor();
      BuCommonImplementation impl=
        ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
          .getImplementation();
      edit_.btLigneInitiale_.addActionListener(impl);
      edit_.btZonesSeches_.addActionListener(impl);
      edit_.btFichierReprise_.addActionListener(impl);
      edit_.btVisuInit_.addActionListener(impl);
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    if (etude_.paramGeneraux().regime().value() == EnumMetierRegime._TRANSCRITIQUE) {
      edit_.btZonesSeches_.setEnabled(true);
    } else
      edit_.btZonesSeches_.setEnabled(false);
    edit_.setObject(etude_.paramGeneraux());
    edit_.setObject(etude_.donneesHydro().conditionsInitiales());
    edit_.show();
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/condition_initiale.html");
  }
}
