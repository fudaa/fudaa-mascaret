/*
 * @file         Hydraulique1dIHM_CruesCalage.java
 * @creation     2001-09-20
 * @modification $Date: 2007-11-20 11:43:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCruesCalageEditor;

import com.memoire.bu.BuAssistant;

/**
 * Classe faisant le lien entre l'�diteur des crues de calage et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      $Revision: 1.4 $ $Date: 2007-11-20 11:43:16 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class Hydraulique1dIHM_CruesCalage extends Hydraulique1dIHM_Base {
  Hydraulique1dCruesCalageEditor edit_;

  public Hydraulique1dIHM_CruesCalage(MetierEtude1d e) {
    super(e);
  }

  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dCruesCalageEditor();
      edit_.setObject(etude_);
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.show();
  }

  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/calage/crues_calage.html");
  }
}
