/*
 * @file         Hydraulique1dIHM_Deversoir.java
 * @creation     2001-04-02
 * @modification $Date: 2007-11-20 11:43:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dDeversoirEditor;
import org.fudaa.fudaa.hydraulique1d.reseau.Hydraulique1dReseauDeversoir;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur de d�versoir lat�ral et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par Hydraulique1dTableauxEditor et Hydraulique1dReseauMouseAdapter.<br>
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:43:18 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_Deversoir extends Hydraulique1dIHM_Base {
  private Hydraulique1dDeversoirEditor edit_;
  private Hydraulique1dReseauDeversoir deversoirGraphique_;
  private MetierBief biefParent_;
  private MetierDeversoir deversoir_;
  public Hydraulique1dIHM_Deversoir(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dDeversoirEditor();
      edit_.setDonneesHydrauliques(etude_.donneesHydro());
      listenToEditor(edit_);
      installContextHelp(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.setDeversoirGraphique(deversoirGraphique_);
    edit_.setObject(deversoir_);
    edit_.setBiefParent(biefParent_);
    edit_.show();
  }
  /**
   * @todo
   * @param e H1dObjetEvent
   */
  @Override
  public void objetCree(H1dObjetEvent e) {
  /*
    A supprimer probablement
    */
/*   if (e.getSource() instanceof MetierDeversoirComportementLoi) {
       MetierDeversoirComportementLoi idev = (MetierDeversoirComportementLoi)e.getSource();
       deversoirGraphique_.putData("singularite", idev);
   }
   else if (e.getSource() instanceof MetierDeversoirComportementZCoefQ) {
       MetierDeversoirComportementZCoefQ idev = (MetierDeversoirComportementZCoefQ)e.getSource();
       deversoirGraphique_.putData("singularite", idev);
   }*/
   /**
    * ---------------------------------------------------------------
    */

    if (edit_ != null)
      edit_.objetCree(e);
  }
  @Override
  public void objetSupprime(H1dObjetEvent e) {
    if (edit_ != null)
      edit_.objetSupprime(e);
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
    if (edit_ != null)
      edit_.objetModifie(e);
  }
  public void setBiefParent(MetierBief _biefParent) {
    biefParent_= _biefParent;
  }
  public void setDeversoir(MetierDeversoir _deversoir) {
    deversoir_= _deversoir;
  }
  public void setDeversoirGraphique(Hydraulique1dReseauDeversoir _deversoir) {
    deversoirGraphique_= _deversoir;
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/singularites.html");
  }
}
