/*
 * @file         Hydraulique1dIHM_GraphesResultats.java
 * @creation     2001-09-25
 * @modification $Date: 2007-11-20 11:43:17 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dGraphesResultatsEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des r�sultats graphiques et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:43:17 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_GraphesResultats extends Hydraulique1dIHM_Base {
  Hydraulique1dGraphesResultatsEditor edit_;
  public Hydraulique1dIHM_GraphesResultats(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    editer(null,false,etude_);
  }
  public void editer(MetierResultatsTemporelSpatial res, final boolean resultCalage, MetierEtude1d e)  {
    if(e !=null)
    	etude_ = e;
	  if (edit_ == null) {
      edit_= new Hydraulique1dGraphesResultatsEditor(null, resultCalage);
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    if (res == null) {
      edit_.setObject(etude_.donneesHydro());
      edit_.setObject(etude_.reseau());
      edit_.setObject(etude_);
     
      //Test des casiers et liaisons
      if (etude_.reseau().casiers().length > 0) {
        edit_.setResultatsCasier(
            etude_.resultatsGeneraux().resultatsTemporelCasier());
        edit_.setResultatsLiaison(
            etude_.resultatsGeneraux().resultatsTemporelLiaison());
      }
      else {
        edit_.setResultatsCasier(null);
        edit_.setResultatsLiaison(null);
      }
      
      //Si  il y a de la qualit� d'eau
      if (etude_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs() && (etude_.resultatsGeneraux().resultatsTemporelTracer()!=null)){
			edit_.setResultatsQE(etude_.resultatsGeneraux().resultatsTemporelTracer());
			edit_.setObject(etude_.resultatsGeneraux().resultatsTemporelSpatial());
      }else{
      //Si pas de qualit� d'eau
      edit_.setResultatsQE(null);
      edit_.setObject(etude_.resultatsGeneraux().resultatsTemporelSpatial());
      }
    } else {
      edit_.setObject(res);
      if(resultCalage)
    	  edit_.setObject(etude_.calageAuto());
      
    }
    edit_.show();
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/graphes_resultats.html");
  }
}
