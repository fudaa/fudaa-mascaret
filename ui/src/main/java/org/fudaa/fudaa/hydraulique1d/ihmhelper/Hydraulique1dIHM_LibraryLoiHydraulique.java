/*
 * @file         Hydraulique1dIHM_LibraryLoiHydraulique.java
 * @creation     2001-03-29
 * @modification $Date: 2007-11-20 11:43:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseImplementation;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dLoiHydrauliqueLibraryEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des lois hydrauliques (la librairie) et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation, Hydraulique1dApportEditor, Hydraulique1dCasierEditor,<br>
 * Hydraulique1dDeversoirEditor, Hydraulique1dExtremLibreEditor, Hydraulique1dSeuilAvecLoiEditor,<br>
 * Hydraulique1dSeuilTransEditor, Hydraulique1dSeuilVanneEditor, Hydraulique1dEditor.<br>
 * @version      $Revision: 1.9 $ $Date: 2007-11-20 11:43:14 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_LibraryLoiHydraulique
  extends Hydraulique1dIHM_Base {
  Hydraulique1dLoiHydrauliqueLibraryEditor edit_;
  boolean qualiteDEau_ = false;
  public Hydraulique1dIHM_LibraryLoiHydraulique(MetierEtude1d e) {
    super(e);
  }
  public void setQualiteDEau(boolean b) {
      qualiteDEau_ = b;
  }

  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dLoiHydrauliqueLibraryEditor();
      edit_.setObject(etude_);
      listenToEditor(edit_);
      installContextHelp(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
      Hydraulique1dBaseImplementation implementation=
        (Hydraulique1dBaseImplementation)
          ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication
          .FRAME)
          .getImplementation();
      implementation.setEnableMenu(
        Hydraulique1dBaseImplementation.AUTORISE_MAILLAGE);
    }
    edit_.setQualiteDEau(qualiteDEau_);
    edit_.show();
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/lois_hydrauliques.html");
  }
}
