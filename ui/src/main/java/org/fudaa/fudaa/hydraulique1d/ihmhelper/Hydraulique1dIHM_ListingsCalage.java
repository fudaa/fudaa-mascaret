/*
 * @file         Hydraulique1dIHM_ListingsCalage.java
 * @creation     2001-09-25
 * @modification $Date: 2007-11-20 11:43:15 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dListingsCalageEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des listings calage et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      $Revision: 1.4 $ $Date: 2007-11-20 11:43:15 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class Hydraulique1dIHM_ListingsCalage extends Hydraulique1dIHM_Base {
  Hydraulique1dListingsCalageEditor edit_;

  public Hydraulique1dIHM_ListingsCalage(MetierEtude1d e) {
    super(e);
  }

  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dListingsCalageEditor();
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.setObject(etude_.calageAuto().resultats());
    edit_.show();
  }

  @Override
  public void objetModifie(H1dObjetEvent e) {
    if (edit_==null || etude_==null || etude_.calageAuto()==null ||
        etude_.calageAuto().resultats()==null) return;

    if (e.getSource() == null) {
      System.out.println("Hydraulique1dIHM_ListingsCalage.objetModifie(e) :");
      System.out.println("e.getSource() == null");
      return;
    }

    if ("resultatsCalage".equals(e.getSource().enChaine())) {
      if (e.getChamp().equals("listingMascaret")) {
        edit_.setListingMascaret(etude_.calageAuto().resultats().listingMascaret());
//      } else if (e.getChamp().equals("listingDamocles")) {
//        edit_.setListingDamocle(etude_.calageAuto().resultats().listingDamocles());
      } else if (e.getChamp().equals("messagesEcran")) {
        edit_.setMessagesEcran(etude_.calageAuto().resultats().messagesEcran());
      } else if (e.getChamp().equals("messagesEcranErreur")) {
        edit_.setMessagesEcranErreur(etude_.calageAuto().resultats().messagesEcranErreur());
      } else if (e.getChamp().equals("listingCalage")) {
        edit_.setListingCalage(etude_.calageAuto().resultats().listingCalage());
      }
    }
  }

  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/calage/listings_calage.html");
  }
}
