/*
 * @file         Hydraulique1dIHM_LoiHydraulique2.java
 * @creation     2001-03-19
 * @modification $Date: 2007-11-20 11:43:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JDialog;

import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLoiHydraulique;
import org.fudaa.dodico.hydraulique1d.metier.loi.MetierLoiSeuil;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresModeleQualiteEau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dLoiHydrauliqueChooser;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dLoiHydrauliqueEditor2;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dLoiSeuilEditor2;
/**
 * Classe faisant le lien entre l'�diteur des lois hydrauliques et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par Hydraulique1dTableauxEditor et Hydraulique1dLoiHydrauliqueLibraryEditor.<br>
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:43:14 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_LoiHydraulique2 extends Hydraulique1dIHM_Base {
  MetierDonneesHydrauliques donneesHydro_;
  MetierParametresModeleQualiteEau modeleQE_;

  Hydraulique1dCustomizer edit_;
  boolean qualiteDEau_ = false;
  Vector propertyChangeListeners_= new Vector();
  public Hydraulique1dIHM_LoiHydraulique2(MetierEtude1d e) {
    super(e);
    donneesHydro_= e.donneesHydro();
    modeleQE_=e.qualiteDEau().parametresModeleQualiteEau();
  }
  public void editer(MetierLoiHydraulique loi) {
    edit_= null;
    if (loi instanceof MetierLoiSeuil) {
      edit_= new Hydraulique1dLoiSeuilEditor2();
    } else {
      edit_= new Hydraulique1dLoiHydrauliqueEditor2();
    }
    edit_.setObject(donneesHydro_);
    edit_.setObject(loi);
    edit_.setObject(modeleQE_);
    //edit_.setEstNouvelleLoi(false);
    installContextHelp(edit_);
    edit_.pack();
    listenToEditor(edit_);
    installContextHelp(edit_);
    for (int i= 0; i < propertyChangeListeners_.size(); i++) {
      edit_.addPropertyChangeListener(
        (PropertyChangeListener)propertyChangeListeners_.get(i));
    }
    edit_.show();
    edit_.repaint();
  }
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    propertyChangeListeners_.add(listener);
  }
  @Override
  public void editer() {

      if (qualiteDEau_){
            MetierLoiHydraulique loi = donneesHydro_.creeLoiTracer(modeleQE_.nbTraceur()+1);
            edit_ = new Hydraulique1dLoiHydrauliqueEditor2();
            Hydraulique1dLoiHydrauliqueEditor2 editCaste = (Hydraulique1dLoiHydrauliqueEditor2) edit_;
            editCaste.setEstNouvelleLoi(true);
            edit_.setTitle("Loi Tracer");
            edit_.setObject(donneesHydro_);
            edit_.setObject(loi);
            edit_.setObject(modeleQE_);
            installContextHelp(edit_);
            edit_.pack();
            listenToEditor(edit_);
           for (int i = 0; i < propertyChangeListeners_.size(); i++) {
               edit_.addPropertyChangeListener(
                       (PropertyChangeListener) propertyChangeListeners_.
                       get(i));
           }
           edit_.show();
           edit_.repaint();

      }else{
          Hydraulique1dLoiHydrauliqueChooser ch =
                  new Hydraulique1dLoiHydrauliqueChooser(
                          Hydraulique1dLoiHydrauliqueChooser.MASCARET);
          ch.addActionListener(new ActionListener() {
        @Override
              public void actionPerformed(ActionEvent che) {
                  String cmd = che.getActionCommand();
                  ((JDialog) ((JComponent) che.getSource()).getTopLevelAncestor())
                          .dispose();
                  if (cmd != null) {
                      MetierLoiHydraulique loi = null;
                      edit_ = null;
                      if ("SEUIL".equals(cmd)) {
                          edit_ = new Hydraulique1dLoiSeuilEditor2();
                          Hydraulique1dLoiSeuilEditor2 editCaste = (Hydraulique1dLoiSeuilEditor2) edit_;
                          editCaste.setEstNouvelleLoi(true);
                          loi = donneesHydro_.creeLoiSeuil();
                          edit_.setObject(donneesHydro_);
                          edit_.setObject(loi);
                          edit_.setObject(modeleQE_);
                         installContextHelp(edit_);
                         edit_.pack();
                         listenToEditor(edit_);
                         for (int i = 0; i < propertyChangeListeners_.size(); i++) {
                             edit_.addPropertyChangeListener(
                                     (PropertyChangeListener) propertyChangeListeners_.
                                     get(i));
                         }
                         edit_.show();
                         edit_.repaint();

                      } else {
                          edit_ = new Hydraulique1dLoiHydrauliqueEditor2();
                          if ("TARAGE".equals(cmd)) {
                              loi = donneesHydro_.creeLoiTarage();
                          } else if ("LIMNI".equals(cmd)) {
                              loi = donneesHydro_.creeLoiLimnigramme();
                          } else if ("HYDRO".equals(cmd)) {
                              loi = donneesHydro_.creeLoiHydrogramme();
                          } else if ("GEOMETRIQUE".equals(cmd)) {
                              loi = donneesHydro_.creeLoiGeometrique();
                          } else if ("LIMNIHYDRO".equals(cmd)) {
                              loi = donneesHydro_.creeLoiLimniHydrogramme();
                          } else if ("VANNE".equals(cmd)) {
                              loi = donneesHydro_.creeLoiOuvertureVanne();
                          } else if ("REGULATION".equals(cmd)) {
                              loi = donneesHydro_.creeLoiRegulation();
                          }
              
                      edit_.setObject(donneesHydro_);
                      edit_.setObject(loi);
                      Hydraulique1dLoiHydrauliqueEditor2 editCaste = (Hydraulique1dLoiHydrauliqueEditor2) edit_;
                      editCaste.setEstNouvelleLoi(true);
                      edit_.setObject(modeleQE_);
                      installContextHelp(edit_);
                      edit_.pack();
                      listenToEditor(edit_);

                      for (int i = 0; i < propertyChangeListeners_.size(); i++) {
                          edit_.addPropertyChangeListener(
                                  (PropertyChangeListener) propertyChangeListeners_.
                                  get(i));
                      }
                      edit_.show();
                      edit_.repaint();
                      }
                  }
              }
          }
          );
          ch.show();
      }
  }
  public void setQualiteDEau(boolean b) {
  qualiteDEau_ = b;
}


  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/lois_hydrauliques.html");
  }
}
