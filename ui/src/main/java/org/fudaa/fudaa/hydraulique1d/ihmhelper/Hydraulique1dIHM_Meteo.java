/*
 * @file         Hydraulique1dIHM_Meteo.java
 * @creation     2006-07-05
 * @modification $Date: 2007-11-20 11:43:15 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */

package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dMeteoEditor;

import com.memoire.bu.BuAssistant;


public class Hydraulique1dIHM_Meteo  extends Hydraulique1dIHM_Base {
  Hydraulique1dMeteoEditor edit_;
  public Hydraulique1dIHM_Meteo(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dMeteoEditor();
      edit_.setObject(etude_.qualiteDEau().parametresGenerauxQualiteDEau());
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.show();
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/meteo.html");
  }

}
