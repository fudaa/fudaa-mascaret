/*
 * @file         Hydraulique1dIHM_ParamGeneraux.java
 * @creation     2001-09-20
 * @modification $Date: 2007-11-20 11:43:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dParametresGenerauxEditor;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuCommonImplementation;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Classe faisant le lien entre l'�diteur des param�tres g�n�raux et l'aide. G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 *
 * @version $Revision: 1.9 $ $Date: 2007-11-20 11:43:13 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_ParamGeneraux extends Hydraulique1dIHM_Base {

  Hydraulique1dParametresGenerauxEditor edit_;

  public Hydraulique1dIHM_ParamGeneraux(MetierEtude1d e) {
    super(e);
  }

  @Override
  public void editer() {
    if (edit_ == null) {
      edit_ = new Hydraulique1dParametresGenerauxEditor();
      BuCommonImplementation impl
              = ((Hydraulique1dBaseApplication) Hydraulique1dBaseApplication.FRAME)
              .getImplementation();
      
      edit_.btZonesFrot_.addActionListener(impl);
      //edit_.btCasier_.addActionListener(impl);
      edit_.setObject(etude_.paramGeneraux());
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass = Hydraulique1dResource.getAssistant();
      if (ass != null) {
        ass.addEmitters(edit_);
      }
    }
    edit_.setHasCasier(!CtuluLibArray.isEmpty(etude_.reseau().casiers()));
    /*if (etude
     edi_.reseau().casiers().length == 0) {
     edit_.btCasier_.setEnabled(false);
     } else {
     edit_.btCasier_.setEnabled(true);
     }*/
    edit_.show();
  }

  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null) {
      return;
    }
    ((Hydraulique1dBaseApplication) Hydraulique1dBaseApplication.FRAME)
            .getImplementation()
            .installContextHelp(e.getRootPane(), "mascaret/parametres_generaux.html");
  }

public Hydraulique1dParametresGenerauxEditor getEdit_() {
	return edit_;
}
}
