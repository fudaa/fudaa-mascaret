/*
 * @file         Hydraulique1dIHM_ParamReprise.java
 * @creation     2001-10-12
 * @modification $Date: 2007-11-20 11:43:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dConditionsInitialesEditor;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dParametresRepriseEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des param�tres de reprise et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      $Revision: 1.10 $ $Date: 2007-11-20 11:43:16 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_ParamReprise extends Hydraulique1dIHM_Base {
  Hydraulique1dParametresRepriseEditor edit_;
  Hydraulique1dConditionsInitialesEditor parent_;
  public Hydraulique1dIHM_ParamReprise(MetierEtude1d e) {
    super(e);
  }
  public void setParentEditor(Hydraulique1dConditionsInitialesEditor parent) {
    parent_= parent;
  }
  @Override
  public void editer() {
     if (edit_ == null) {
      edit_= new Hydraulique1dParametresRepriseEditor(parent_);
      if (etude_.donneesHydro().conditionsInitiales().ligneEauInitiale()
        == null)
        etude_.donneesHydro().conditionsInitiales().creeLigneInitiale();
      if (etude_.donneesHydro().conditionsInitiales().paramsReprise() == null)
        etude_.donneesHydro().conditionsInitiales().creeParamsReprise();
      edit_.setObject(
        etude_.donneesHydro().conditionsInitiales().ligneEauInitiale());
      edit_.setObject(etude_.reseau());
      edit_.setObject(etude_.resultatsGeneraux());
      edit_.setObject(
        etude_.donneesHydro().conditionsInitiales().paramsReprise());
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
      /*    else if ("FICHIER_REPRISE".equals(cmd)) {
            Hydraulique1dParametresRepriseEditor editeur = new Hydraulique1dParametresRepriseEditor(this);
            if (conditionsInitiales_.paramsReprise()==null) conditionsInitiales_.creeParamsReprise();
            editeur.setTextFieldFichier(tfFichierReprise_ );
            editeur.setObject(conditionsInitiales_.ligneEauInitiale());
            editeur.setObject(conditionsInitiales_.paramsReprise());
            editeur.pack();
            editeur.show();
          }*/
    }
    edit_.show();
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/parametres_reprise.html");
  }
}
