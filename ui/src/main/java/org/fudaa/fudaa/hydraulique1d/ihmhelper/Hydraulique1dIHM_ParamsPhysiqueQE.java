/*
 * @file         Hydraulique1dIHM_ParamsPhysiqueQE.java
 * @creation     2006-07-05
 * @modification $Date: 2007-11-20 11:43:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dParamsPhysiqueQEEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des parametres physiques et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      1.0
 * @author       Olivier Pasteur
 */
public class Hydraulique1dIHM_ParamsPhysiqueQE  extends Hydraulique1dIHM_Base {
  Hydraulique1dParamsPhysiqueQEEditor edit_;

  public Hydraulique1dIHM_ParamsPhysiqueQE(MetierEtude1d e) {
    super(e);
  }

  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dParamsPhysiqueQEEditor();
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }

    edit_.setObject(etude_.qualiteDEau());
    edit_.show();
  }


  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/ParamsPhysiqueQE.html");
  }

}
