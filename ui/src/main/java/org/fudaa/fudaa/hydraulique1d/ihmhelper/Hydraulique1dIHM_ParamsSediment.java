/*
 * @file         Hydraulique1dIHM_ParamCalage.java
 * @creation     2001-09-20
 * @modification $Date: 2007-11-20 11:43:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierCalculSediment;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierFormuleSediment;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierSediment;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseImplementation;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dParametresSedimentEditor;

import com.memoire.bu.BuAssistant;

/**
 * Classe faisant le lien entre l'�diteur des param�tres de sediment et l'aide.
 * 
 * @version      $Id$
 * @author       Bertrand Marchand (marchand@deltacad.fr)
 */
public class Hydraulique1dIHM_ParamsSediment extends Hydraulique1dIHM_Base {
  Hydraulique1dParametresSedimentEditor edit_;

  public Hydraulique1dIHM_ParamsSediment(MetierEtude1d e) {
    super(e);
  }

  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dParametresSedimentEditor();
      edit_.setParametres(etude_.sediment().parametres());
      edit_.setEtude(etude_);
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.show();
  }

  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/sediment/parametres_sediment.html");
  }
}
