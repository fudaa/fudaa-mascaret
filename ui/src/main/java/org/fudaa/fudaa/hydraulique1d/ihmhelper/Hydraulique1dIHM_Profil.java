/*
 * @file         Hydraulique1dIHM_Profil.java
 * @creation     2004-08-17
 * @modification $Date: 2007-11-20 11:43:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilEditor;
import org.fudaa.fudaa.hydraulique1d.editor.profil.Hydraulique1dProfilSimpleEditor;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
/**
 * Classe faisant le lien entre l'�diteur de profil et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par Hydraulique1dBiefEditor.<br>
 * @version      $Revision: 1.16 $ $Date: 2007-11-20 11:43:16 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_Profil extends Hydraulique1dIHM_Base {
  private Hydraulique1dProfilEditor edit_;
  private Hydraulique1dProfilSimpleEditor editSimple_;
  private Hydraulique1dProfilModel[] profilsModeleBief_;
  private Hydraulique1dProfilModel profilModeleCourant_;
  public Hydraulique1dIHM_Profil(MetierEtude1d e) {
    super(e);
  }
  public void setProfilsModeleBief(Hydraulique1dProfilModel[] profils) {
    profilsModeleBief_ = profils;
  }
  public void setProfilModeleCourant(Hydraulique1dProfilModel profilModele) {
    profilModeleCourant_ = profilModele;
  }

  @Override
  public void editer() {
    if (profilModeleCourant_.pointsNonNuls().length == 0) {
      int res=
        new BuDialogConfirmation(
          (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
          ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
            .getInformationsSoftware(),
          getS("Ce profil ne contient aucun point")+".\n"
          + getS("Cr�ation profil simple ?")+"\n"
          + "\t+ "+getS("Oui")+"\t"+getS("Forme classique (trap�ze, rectangle, buse, etc.)")+"\n"
          + "\t- "+getS("Non")+"\t"+getS("Forme quelconque")+"\n"
             )
          .activate();
      if (res == JOptionPane.OK_OPTION) {
        editSimple_ = new Hydraulique1dProfilSimpleEditor();
        editSimple_.setProfilModel(profilModeleCourant_);
        listenToEditor(editSimple_);
        editSimple_.show();
        return;
      }

    }
    if (edit_ == null) {
      edit_= new Hydraulique1dProfilEditor(
                                           Hydraulique1dProfilEditor.EDITER,
                                           Hydraulique1dProfilEditor.PROFILS);
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }

    if (etude_.paramGeneraux().regime().value() == EnumMetierRegime._TRANSCRITIQUE)
        edit_.setIsTranscritique(true);
    else
        edit_.setIsTranscritique(false);

    edit_.setProfilsModeleBief(profilsModeleBief_);
    edit_.setProfilModel(profilModeleCourant_);
    edit_.show();
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/geom.html");
  }
}
