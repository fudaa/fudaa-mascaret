/*
 * @file         Hydraulique1dIHM_ResultatsGeneraux.java
 * @creation     2001-09-25
 * @modification $Date: 2008-02-29 16:47:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.*;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dResultatsGenerauxEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des r�sultats g�n�raux et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      $Revision: 1.15 $ $Date: 2008-02-29 16:47:10 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_ResultatsGeneraux extends Hydraulique1dIHM_Base {
  Hydraulique1dResultatsGenerauxEditor edit_;
  public Hydraulique1dIHM_ResultatsGeneraux(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dResultatsGenerauxEditor();
      edit_.setObject(etude_.resultatsGeneraux());
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.show();
  }
  @Override
  public void objetModifie(H1dObjetEvent e) {
    if (edit_ == null)
      return;
    if (etude_ == null)
      return;
    if (etude_.resultatsGeneraux() == null)
      return;
    if (e.getSource() == null) {
      System.out.println("Hydraulique1dIHM_ResultatsGeneraux.objetModifie(e) :");
      System.out.println("e.getSource() == null");
      return;
    }
    if ("resultatsGeneraux".equals(e.getSource().enChaine())) {
      if (e.getChamp().equals("listing")) {
        edit_.setListing(etude_.resultatsGeneraux().listing());
//      } else if (e.getChamp().equals("listingDamocles")) {
//        edit_.setListingDamocle(etude_.resultatsGeneraux().listingDamocles());
      } else if (e.getChamp().equals("Avertissements")) {
          edit_.setAvertissements(etude_.resultatsGeneraux().avertissements());
      } else if (e.getChamp().equals("messagesEcran")) {
        edit_.setMessagesEcran(etude_.resultatsGeneraux().messagesEcran());
      } else if (e.getChamp().equals("messagesEcranErreur")) {
        edit_.setMessagesEcranErreur(etude_.resultatsGeneraux().messagesEcranErreur());
      } else if (e.getChamp().equals("listingCasier")) {
        edit_.setListingCasier(etude_.resultatsGeneraux().listingCasier());
      } else if (e.getChamp().equals("listingLiaison")) {
        edit_.setListingLiaison(etude_.resultatsGeneraux().listingLiaison());
      } else if (e.getChamp().equals("listingTracer")) {
    	  if (CGlobal.AVEC_QUALITE_DEAU) {
    		  edit_.setListingTracer(etude_.resultatsGeneraux().listingTracer());
    	  }
      }
    }
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/resultats_generaux.html");
  }
}
