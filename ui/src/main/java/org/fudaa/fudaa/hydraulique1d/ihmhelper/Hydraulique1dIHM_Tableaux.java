/*
 * @file         Hydraulique1dIHM_Tableaux.java
 * @creation     2004-09-15
 * @modification $Date: 2007-11-20 11:43:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dTableauxEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur des tableaux de synth�se et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      $Revision: 1.5 $ $Date: 2007-11-20 11:43:14 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_Tableaux extends Hydraulique1dIHM_Base {
  private Hydraulique1dTableauxEditor edit_;
  public Hydraulique1dIHM_Tableaux(MetierEtude1d e) {
    super(e);
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dTableauxEditor();
      edit_.setObject(etude_);
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.pack();
    edit_.show();
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/tableaux_syntheses.html");
  }
}
