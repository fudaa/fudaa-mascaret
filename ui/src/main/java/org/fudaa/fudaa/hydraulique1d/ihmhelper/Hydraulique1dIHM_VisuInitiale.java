/*
 * @file         Hydraulique1dIHM_VisuInitiale.java
 * @creation     2001-09-20
 * @modification $Date: 2007-11-20 11:43:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.ihmhelper;
import javax.swing.JComponent;

import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dVisuInitialeEditor;

import com.memoire.bu.BuAssistant;
/**
 * Classe faisant le lien entre l'�diteur de visualisation graphiques des conditions initiales et l'aide.
 * G�r� par Hydraulique1dIHMRepository.<br>
 * Utilis� par MascaretImplementation.<br>
 * @version      $Revision: 1.9 $ $Date: 2007-11-20 11:43:13 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dIHM_VisuInitiale extends Hydraulique1dIHM_Base {
  Hydraulique1dCustomizer edit_;
  BDialogContent parent_;
  public Hydraulique1dIHM_VisuInitiale(MetierEtude1d e) {
    super(e);
  }
  public void setParentEditor(BDialogContent parent) {
    parent_= parent;
  }
  @Override
  public void editer() {
    if (edit_ == null) {
      edit_= new Hydraulique1dVisuInitialeEditor(parent_);
      edit_.setObject(etude_.reseau());
      edit_.setObject(etude_.donneesHydro().conditionsInitiales());
      installContextHelp(edit_);
      listenToEditor(edit_);
      BuAssistant ass= Hydraulique1dResource.getAssistant();
      if (ass != null)
        ass.addEmitters(edit_);
    }
    edit_.setObject(etude_.reseau());
    edit_.setObject(etude_.donneesHydro().conditionsInitiales());
    edit_.show();
  }
  @Override
  protected void installContextHelp(JComponent e) {
    if (e == null)
      return;
    ((Hydraulique1dBaseApplication)Hydraulique1dBaseApplication.FRAME)
      .getImplementation()
      .installContextHelp(e.getRootPane(), "mascaret/condition_initiale.html");
  }
}
