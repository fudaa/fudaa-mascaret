package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.LineChoiceBorder;

/**
 * Multiple radio panel.
 * @author Adrien
 *
 */
public class MascaretRadioButton  extends AbstractButton {

      private static final long serialVersionUID = 1L;
	  AbstractButton btOui_;
	  AbstractButton btNon_;
	  private JLabel label_;
	  BuRadioButton  btAucun_ ;
	  
	  public MascaretRadioButton() {
	    this(new BuLabel(CtuluLibString.EMPTY_STRING), new BuRadioButton(EbliLib.getS("Oui")), new BuRadioButton(
	        EbliLib.getS("Non")));
	  }

	
	  public MascaretRadioButton(final String _text) {
	    this(new BuLabel(_text), null, null);
	  }

	
	  public MascaretRadioButton(final JLabel _label) {
	    this(_label, null, null);
	  }

	  public MascaretRadioButton(final AbstractButton _buttonTrue, final AbstractButton _buttonFalse) {
	    this(new BuLabel(CtuluLibString.EMPTY_STRING), _buttonTrue, _buttonFalse);
	  }

	
	  public MascaretRadioButton(final JLabel _label, final AbstractButton _buttonTrue, final AbstractButton _buttonFalse) {
	    if (_label == null) {
	      label_ = new BuLabel(CtuluLibString.EMPTY_STRING);
	    } else {
	      label_ = _label;
	      label_.setHorizontalAlignment(SwingConstants.RIGHT);
	    }

	    if (_buttonTrue == null) {
	      btOui_ = new BuRadioButton(EbliLib.getS("Oui"));
	    } else {
	      btOui_ = _buttonTrue;
	    }

	    if (_buttonFalse == null) {
	      btNon_ = new BuRadioButton(EbliLib.getS("Non"));
	    } else {
	      btNon_ = _buttonFalse;
	    }
	    
	    ActionListener lisetenerCom = new ActionListener() {
		      public void actionPerformed(final ActionEvent _e){
		        if (_e.getSource() == btNon_) {
		          btNon_.setSelected(true);
		          btOui_.setSelected(false);
		          btAucun_.setSelected(false);
		        }else
		        if (_e.getSource() == btOui_) {
		          btOui_.setSelected(true);
		          btNon_.setSelected(false);
		          btAucun_.setSelected(false);
		        }else
		        if (_e.getSource() == btAucun_) {
		        	btAucun_.setSelected(true);
		        	btOui_.setSelected(false);
		        	btNon_.setSelected(false);
			     }
		      }
		    };
	    
	    btOui_.addActionListener(lisetenerCom);
	    btNon_.addActionListener(lisetenerCom);
	    
	    btAucun_ = new BuRadioButton(EbliLib.getS("Aucun"));
	    btAucun_.addActionListener(lisetenerCom);
	    
	    
	    final BuPanel pnBoutons = new BuPanel();
	    pnBoutons.setLayout(new GridLayout(3, 1));
	    pnBoutons.setBorder(new LineChoiceBorder(false, false, false, true, false, false));
	    pnBoutons.add(btAucun_);
	    pnBoutons.add(btOui_);
	    pnBoutons.add(btNon_);

	    final BuPanel pnLabel = new BuPanel();
	    pnLabel.setLayout(new GridLayout());
	    pnLabel.add(label_);

	    setLayout(new GridLayout(1, 2));
	    add(pnLabel);
	    add(pnBoutons);
	  }

	 
	  @Override
	  public void setText(final String _texte){
	    label_.setText(_texte);
	  }

	  @Override
	  public String getText(){
	    return label_.getText();
	  }

	
	  public void setTextButtonTrue(final String _texte){
	    btOui_.setText(_texte);
	  }

	  
	  public String getTextButtonTrue(){
	    return btOui_.getText();
	  }

	 
	  public void setTextButtonFalse(final String _texte){
	    btNon_.setText(_texte);
	  }

	
	  public String getTextButtonFalse(){
	    return btNon_.getText();
	  }

	 
	  public void setAucunSelected(){
		  btAucun_.setSelected(true);
		  btNon_.setSelected(false);
		  btOui_.setSelected(false);
  }

  public void setAcunEnabled(boolean b) {
    btAucun_.setEnabled(b);
  }
	  
	  @Override
	  public void setSelected(final boolean _selection){
	    if (_selection) {
	      if (!btOui_.isSelected()) {
	        btOui_.setSelected(true);
	      }
	      if (btNon_.isSelected()) {
	        btNon_.setSelected(false);
	      }
	    }
	    else {
	      if (btOui_.isSelected()) {
	        btOui_.setSelected(false);
	      }
	      if (!btNon_.isSelected()) {
	        btNon_.setSelected(true);
	      }
	    }
	  }

	  /**
	   * @return la valeur de la s�lection du bouton du haut.
	   */
	  @Override
	  public boolean isSelected(){
	    return btOui_.isSelected();
	  }
	  
	  public boolean isAucunSelected(){
		    return btAucun_.isSelected();
		  }

	  /**
	   * Ajoute un nouveau �couteur de changement de valeur de s�lection.
	   * @param _l Le nouveau �couteur.
	   */
	  @Override
	  public void addChangeListener(final ChangeListener _l){
	    btOui_.addChangeListener(_l);
	  }

	  /**
	   * Supprime un �couteur de changement de valeur de s�lection.
	   * @param _l L'�couteur � supprimer.
	   */
	  @Override
	  public void removeChangeListener(final ChangeListener _l){
	    btOui_.removeChangeListener(_l);
	  }

	  /**
	   * Ajoute un nouveau �couteur d'action sur l'un des 2 boutons.
	   * @param _l Le nouveau �couteur.
	   */
	  @Override
	  public void addActionListener(final ActionListener _l){
	    btOui_.addActionListener(_l);
	    btNon_.addActionListener(_l);
	    btAucun_.addActionListener(_l);
	  }

	  /**
	   * Supprime un �couteur d'action sur les 2 boutons.
	   * @param _l L'�couteur � supprimer.
	   */
	  @Override
	  public void removeActionListener(final ActionListener _l){
	    btOui_.removeActionListener(_l);
	    btNon_.removeActionListener(_l);
	    btAucun_.removeActionListener(_l);
	  }

	  @Override
	  public void setEnabled(final boolean _enable){
	    btOui_.setEnabled(_enable);
	    btNon_.setEnabled(_enable);
	    btAucun_.setEnabled(_enable);
	    
	    label_.setEnabled(_enable);
	  }

	  
	  @Override
	  public boolean isEnabled(){
	    return btOui_.isEnabled();
	  }


	}

