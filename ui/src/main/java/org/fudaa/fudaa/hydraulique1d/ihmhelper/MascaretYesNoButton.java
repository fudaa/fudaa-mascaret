package org.fudaa.fudaa.hydraulique1d.ihmhelper;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.LineChoiceBorder;


public class MascaretYesNoButton extends AbstractButton {

	  AbstractButton btOui_;
	  AbstractButton btNon_;
	  private JLabel label_;

	  /**
	   * Contructeur par d�faut avec es 2 boutons sont des radios boutons "oui" en haut
	   * et "non" en bas et un label vide � droite.
	   */
	  public MascaretYesNoButton() {
	    this(new BuLabel(CtuluLibString.EMPTY_STRING), new BuRadioButton(EbliLib.getS("Oui")), new BuRadioButton(
	        EbliLib.getS("Non")));
	  }

	  /**
	   * Constructeur avec pr�cision du texte du label.
	   * @param _text Le texte du label � gauche.
	   */
	  public MascaretYesNoButton(final String _text) {
	    this(new BuLabel(_text), null, null);
	  }

	  /**
	   * Constructeur avec pr�cision du label.
	   * @param _label Le label � gauche, si null BuLabel avec chaine vide.
	   */
	  public MascaretYesNoButton(final JLabel _label) {
	    this(_label, null, null);
	  }

	  /**
	   * Constructeur avec pr�cision des 2 boutons.
	   * @param _buttonTrue Le bouton du haut � droite, si null radio bouton "oui".
	   * @param _buttonFalse Le bouton du bas � droite, si null radio bouton "non".
	   */
	  public MascaretYesNoButton(final AbstractButton _buttonTrue, final AbstractButton _buttonFalse) {
	    this(new BuLabel(CtuluLibString.EMPTY_STRING), _buttonTrue, _buttonFalse);
	  }

	  /**
	   * Constructeur avec pr�cision des 2 boutons et du label.
	   * @param _label Le label � gauche, si null BuLabel avec chaine vide.
	   * @param _buttonTrue Le bouton du haut � droite, si null radio bouton "oui".
	   * @param _buttonFalse Le bouton du bas � droite, si null radio bouton "non".
	   */
	  public MascaretYesNoButton(final JLabel _label, final AbstractButton _buttonTrue, final AbstractButton _buttonFalse) {
	    if (_label == null) {
	      label_ = new BuLabel(CtuluLibString.EMPTY_STRING);
	    } else {
	      label_ = _label;
	      label_.setHorizontalAlignment(SwingConstants.RIGHT);
	    }

	    if (_buttonTrue == null) {
	      btOui_ = new BuRadioButton(EbliLib.getS("Oui"));
	    } else {
	      btOui_ = _buttonTrue;
	    }

	    if (_buttonFalse == null) {
	      btNon_ = new BuRadioButton(EbliLib.getS("Non"));
	    } else {
	      btNon_ = _buttonFalse;
	    }
	    btOui_.addActionListener(new ActionListener() {

	      public void actionPerformed(final ActionEvent _e){
	        if (!btOui_.isSelected()) {
	          btOui_.setSelected(true);
	        }
	        if (btNon_.isSelected()) {
	          btNon_.setSelected(false);
	        }
	      }
	    });
	    btNon_.addActionListener(new ActionListener() {

	      public void actionPerformed(final ActionEvent _e){
	        if (!btNon_.isSelected()) {
	          btNon_.setSelected(true);
	        }
	        if (btOui_.isSelected()) {
	          btOui_.setSelected(false);
	        }
	      }
	    });

	   
	    int n=0;
	    setBorder(new LineChoiceBorder(false, false, false, true, false, false));
	    setLayout(new BuVerticalLayout(1, true, true));
	    add(btOui_,n++);
	    add(btNon_,n++);
	  }

	  /**
	   * Modifie le texte du label de gauche.
	   * @param _texte Le nouveau texte.
	   */
	  @Override
	  public void setText(final String _texte){
	    label_.setText(_texte);
	  }

	  /**
	   * Retourne le texte du label.
	   * @return Le texte du label de gauche.
	   */
	  @Override
	  public String getText(){
	    return label_.getText();
	  }

	  /**
	   * Modifie le texte du bouton du haut � droite.
	   * @param _texte Le nouveau texte.
	   */
	  public void setTextButtonTrue(final String _texte){
	    btOui_.setText(_texte);
	  }

	  /**
	   * Retourne le texte du bouton en haut � droite.
	   * @return Le texte du bouton.
	   */
	  public String getTextButtonTrue(){
	    return btOui_.getText();
	  }

	  /**
	   * Modifie le texte du bouton en bas � droite.
	   * @param _texte Le texte du bouton.
	   */
	  public void setTextButtonFalse(final String _texte){
	    btNon_.setText(_texte);
	  }

	  /**
	   * Retourne le texte du bouton en bas � droite.
	   * @return Le texte du bouton.
	   */
	  public String getTextButtonFalse(){
	    return btNon_.getText();
	  }

	  /**
	   * Modifie la selection du bouton.
	   * @param _selection La nouvelle s�lection.
	   * <br> si vrai s�lectionne le bouton du haut et d�sectionnne le bouton du bas.
	   * <br> si faux s�lectionne le bouton du bas et d�sectionnne le bouton du haut.
	   */
	  @Override
	  public void setSelected(final boolean _selection){
	    if (_selection) {
	      if (!btOui_.isSelected()) {
	        btOui_.setSelected(true);
	      }
	      if (btNon_.isSelected()) {
	        btNon_.setSelected(false);
	      }
	    }
	    else {
	      if (btOui_.isSelected()) {
	        btOui_.setSelected(false);
	      }
	      if (!btNon_.isSelected()) {
	        btNon_.setSelected(true);
	      }
	    }
	  }

	  /**
	   * @return la valeur de la s�lection du bouton du haut.
	   */
	  @Override
	  public boolean isSelected(){
	    return btOui_.isSelected();
	  }

	  /**
	   * Ajoute un nouveau �couteur de changement de valeur de s�lection.
	   * @param _l Le nouveau �couteur.
	   */
	  @Override
	  public void addChangeListener(final ChangeListener _l){
	    btOui_.addChangeListener(_l);
	  }

	  /**
	   * Supprime un �couteur de changement de valeur de s�lection.
	   * @param _l L'�couteur � supprimer.
	   */
	  @Override
	  public void removeChangeListener(final ChangeListener _l){
	    btOui_.removeChangeListener(_l);
	  }

	  /**
	   * Ajoute un nouveau �couteur d'action sur l'un des 2 boutons.
	   * @param _l Le nouveau �couteur.
	   */
	  @Override
	  public void addActionListener(final ActionListener _l){
	    btOui_.addActionListener(_l);
	    btNon_.addActionListener(_l);
	  }

	  /**
	   * Supprime un �couteur d'action sur les 2 boutons.
	   * @param _l L'�couteur � supprimer.
	   */
	  @Override
	  public void removeActionListener(final ActionListener _l){
	    btOui_.removeActionListener(_l);
	    btNon_.removeActionListener(_l);
	  }

	  /**
	   * Enables (or disables) the button.
	   * @param _enable  true to enable the button, otherwise false
	   */
	  @Override
	  public void setEnabled(final boolean _enable){
	    btOui_.setEnabled(_enable);
	    btNon_.setEnabled(_enable);
	    label_.setEnabled(_enable);
	  }

	  /**
	   * Determines whether this component is enabled. An enabled component
	   * can respond to user input and generate events. Components are
	   * enabled initially by default. A component may be enabled or disabled by
	   * calling its <code>setEnabled</code> method.
	   * @return <code>true</code> if the component is enabled,
	   * 		<code>false</code> otherwise
	   * @see #setEnabled setEnabled
	   */
	  @Override
	  public boolean isEnabled(){
	    return btOui_.isEnabled();
	  }


	


}
