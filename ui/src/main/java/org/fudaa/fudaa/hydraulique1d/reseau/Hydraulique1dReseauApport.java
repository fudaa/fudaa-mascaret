/*
 * @file         Hydraulique1dReseauApport.java
 * @creation     2000-11-16
 * @modification $Date: 2007-11-20 11:42:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;

import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuIcon;

/**
 * Composant graphique du r�seau hydraulique repr�sentant un apport de d�bit.
 *
 * @see MetierApport
 * @version $Revision: 1.8 $ $Date: 2007-11-20 11:42:41 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dReseauApport extends Hydraulique1dReseauSingularite {

  private final static BuIcon ICON = Hydraulique1dResource.HYDRAULIQUE1D.getIcon("reseau/reseau_apport.png");

  public Hydraulique1dReseauApport(MetierApport apport) {
    super(null);

    if (apport != null) {
      putData("singularite", apport);
    }
  }

  public Hydraulique1dReseauApport() {
    this(null);
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauApport r = (Hydraulique1dReseauApport) super.clone();
    MetierReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
    MetierApport apport = reseau.creeApport();
    apport.initialise((MetierApport) getData("singularite"));
    r.putData("singularite", apport);
    return r;
  }

  @Override
  BuIcon getBuIcon() {
    return ICON;
  }
}
