/*
 * @file         Hydraulique1dReseauBarragePrincipal.java
 * @creation     2000-11-16
 * @modification $Date: 2007-11-20 11:42:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
import java.awt.Color;
import java.awt.Graphics;

import org.fudaa.dodico.hydraulique1d.metier.MetierBarragePrincipal;

import com.memoire.dja.DjaAttach;
import com.memoire.dja.DjaGraphics;
import com.memoire.dja.DjaLink;
/**
 * Composant graphique du réseau hydraulique représentant un barrage principal.
 * PAS UTILISE.
 * @version      $Revision: 1.9 $ $Date: 2007-11-20 11:42:41 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauBarragePrincipal
  extends DjaLink
  implements Hydraulique1dReseauElementInterf {
  public Hydraulique1dReseauBarragePrincipal(MetierBarragePrincipal barragePrincipal) {
    super();
    setWidth(13);
    super.setBackground(Color.red);
    super.setColor(Color.red);
    if (barragePrincipal != null)
      putData("barragePrincipal", barragePrincipal);
  }
  public Hydraulique1dReseauBarragePrincipal() {
    this(null);
  }
  @Override
  public void setWidth(int _w) {
    super.setHeight(_w);
    super.setWidth(_w);
  }
  @Override
  public void setHeight(int _h) {
    super.setWidth(_h);
    super.setHeight(_h);
  }
  @Override
  public void paintObject(Graphics _g) {
    updateXYO();
    int x= getX();
    int y= getY();
    int w= getWidth();
    int h= getHeight();
    Color bg= getBackground();
    Color fg= getForeground();
    if (bg != null) {
      _g.setColor(bg);
      _g.fillOval(x, y, w - 1, h - 1);
    }
    if (fg != null) {
      _g.setColor(fg);
      DjaGraphics.BresenhamParams bp= DjaGraphics.getBresenhamParams(this);
      DjaGraphics.drawOval(_g, x, y, w - 1, h - 1, bp);
    }
    super.paintObject(_g);
  }
  @Override
  public DjaAttach[] getAttachs() {
    updateXYO();
    DjaAttach[] r= new DjaAttach[1];
    r[0]= new DjaAttach(this, 0, begin_, obegin_, getX(), getY());
    return r;
  }
  @Override
  public int getX() {
    updateXYO();
    return xbegin_;
  }
  @Override
  public int getY() {
    updateXYO();
    return ybegin_;
  }
  @Override
  public int getWidth() {
    return 13;
  }
  @Override
  public int getHeight() {
    return 13;
  }
  @Override
  public String[] getInfos() {
    MetierBarragePrincipal iobjet= (MetierBarragePrincipal)getData("barragePrincipal");
    return iobjet.getInfos();
  }
}
