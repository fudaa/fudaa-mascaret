/*
 * @file         Hydraulique1dReseauBiefCourbe.java
 * @creation     2000-09-04
 * @modification $Date: 2007-11-20 11:42:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.Polygon;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet;

import com.memoire.dja.DjaAnchor;
import com.memoire.dja.DjaArcArrow;
import com.memoire.dja.DjaMatrixHermite;
import com.memoire.dja.DjaText;
import org.fudaa.dodico.boony.BoonyDeserializationAware;

/**
 * Composant graphique du r�seau hydraulique repr�sentant un bief. A la cr�ation, il est courb�.
 *
 * @see MetierBief
 * @version $Revision: 1.11 $ $Date: 2007-11-20 11:42:40 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dReseauBiefCourbe
        extends DjaArcArrow
        implements Hydraulique1dReseauElementInterf, BoonyDeserializationAware {

  protected int nbAnchors = 9;

  Hydraulique1dReseauBiefCourbe(MetierBief bief) {
    super();
    if (bief != null) {
      super.addText(Integer.toString(bief.indice() + 1));
    }
    super.setEndType(5); // fleche pleine
    super.setEndO(0); // fleche vers le bas
    super.setHeight(100);
    super.setBeginX(super.getEndX()); // vertical
    // symetrie horizontale
    int YBegin = super.getBeginY();
    int YEnd = super.getEndY();
    super.setBeginY(YEnd);
    super.setEndY(YBegin);
    if (bief != null) {
      putData("bief", bief);
    }
  }

  Hydraulique1dReseauBiefCourbe() {
    this(null);
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauBiefCourbe r = (Hydraulique1dReseauBiefCourbe) super.clone();
    MetierReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
    MetierBief bief = reseau.creeBief();
    bief.initialise((MetierBief) getData("bief"));
    r.putData("bief", bief);
    return r;
  }

  protected final void clearCourbure() {
    this.putProperty("courbure", "0.");
  }

  @Override
  public void endDeserialization() {
    clearCourbure();
  }

  public void setNbAnchors(int nbAnchors_) {
    nbAnchors = nbAnchors_;
  }

  @Override
  public DjaText[] getTexts() {
    DjaText[] r = super.getTexts();
    if (r.length > 0) {
      r[0].setPosition(RELATIVE_ANCHOR);
      r[0].setReference(nbAnchors / 2);
    }
    return r;
  }

  @Override
  public DjaAnchor[] getAnchors() {
    DjaAnchor[] r = new DjaAnchor[0];
    double c = getCourbure();
    if (Math.abs(c) > 0.001) {
      try {
        int vx0 = (int) ((yr0 - yr1) * c);
        int vy0 = (int) ((xr1 - xr0) * c);
        int vx1 = (int) ((yr1 - yr0) * c);
        int vy1 = (int) ((xr0 - xr1) * c);
        Polygon p
                = DjaMatrixHermite.arc2polyline(xr0, yr0, xr1, yr1, vx0, vy0, vx1, vy1);
        int nbPoints = p.npoints;
        nbAnchors = Math.min(nbPoints, nbAnchors);
        int pas = (nbPoints - 1) / (nbAnchors + 1);
        pas = Math.max(pas, 1);
//	Unused	      int fin= Math.min(nbPoints - 1, pas * (nbAnchors + 1));
        r = new DjaAnchor[nbAnchors/*Math.round((fin / pas) + 0.499999f) - 1*/];
        //System.out.println("fin "+fin+"; pas "+pas +"; nbpoints "+nbPoints+"; a49 "+a49+"; a4999 "+a4999 );
        int j = 0;
        for (int i = pas; j < nbAnchors; i += pas) {//erreur lorsque i>nbPoints
          r[j] = new DjaAnchor(this, j, ANY, p.xpoints[i], p.ypoints[i]);
          j++;
        }
      } catch (Throwable e) {
        System.out.println("Erreur dans Hydraulique1dReseauBiefCourbe.getAnchors");
      }
    } else {
      r = new DjaAnchor[nbAnchors];
      float pasX = (xr1 - xr0) / (nbAnchors + 1f);
      float pasY = (yr1 - yr0) / (nbAnchors + 1f);
      for (int i = 1; i <= nbAnchors; i++) {
        r[i - 1]
                = new DjaAnchor(
                        this,
                        i - 1,
                        ANY,
                        Math.round(xr0 + i * pasX),
                        Math.round(yr0 + i * pasY));
      }
    }
    return r;
  }

  @Override
  public String[] getInfos() {
    MetierHydraulique1d iobjet = (MetierHydraulique1d) getData("bief");
    return iobjet.getInfos();
  }
}
