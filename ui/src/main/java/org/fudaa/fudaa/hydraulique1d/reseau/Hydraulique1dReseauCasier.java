/*
 * @file         Hydraulique1dReseauCasier.java
 * @creation     2003-06-06
 * @modification $Date: 2007-11-20 11:42:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2003 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
import java.awt.Color;

import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet;

import com.memoire.dja.DjaRoundBox;
/**
 * Composant graphique du réseau hydraulique représentant un casier.
 * @see MetierCasier
 * @version      $Revision: 1.9 $ $Date: 2007-11-20 11:42:40 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauCasier
  extends DjaRoundBox
  implements Hydraulique1dReseauElementInterf {
  Hydraulique1dReseauCasier(MetierCasier icasier) {
    super();
    setWidth(40);
    setHeight(20);
    if (icasier != null)
      super.addText("" + icasier.numero());
    super.setForeground(Color.blue);
    if (icasier != null)
      putData("casier", icasier);
  }
  Hydraulique1dReseauCasier() {
    this(null);
  }
  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauCasier r=(Hydraulique1dReseauCasier)super.clone();
    MetierReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
    MetierCasier casier= reseau.ajouterCasier();
    casier.initialise((MetierCasier)getData("casier"));
    r.putData("casier", casier);
    return r;
  }
  @Override
  public String[] getInfos() {
    MetierHydraulique1d iobjet= (MetierHydraulique1d)getData("casier");
    return iobjet.getInfos();
  }
}
