/*
 * @file         Hydraulique1dReseauDeversoir.java
 * @creation     2000-11-16
 * @modification $Date: 2007-11-20 11:42:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuIcon;
/**
 * Composant graphique du r�seau hydraulique repr�sentant un d�versoir lat�ral.
 * @see MetierDeversoir
 * @version      $Revision: 1.10 $ $Date: 2007-11-20 11:42:41 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauDeversoir
  extends Hydraulique1dReseauSingularite {
  private final static BuIcon ICON = Hydraulique1dResource.HYDRAULIQUE1D.getIcon("reseau/reseau_deversoir.png");
  public Hydraulique1dReseauDeversoir(MetierDeversoir deversoir) {
    super(null);
    if (deversoir != null)
      putData("singularite", deversoir);
  }
  public Hydraulique1dReseauDeversoir() {
    this(null);
  }
  @Override
  public Object clone() throws CloneNotSupportedException {
    System.out.println("this="+this.toString()+"  clone()");
    Hydraulique1dReseauDeversoir r=(Hydraulique1dReseauDeversoir)super.clone();
//    DReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
//    MetierDeversoir deversoir= reseau.creeDeversoir();
//    deversoir.initialise((MetierDeversoir)getData("singularite"));
    MetierDeversoir deversoir= (MetierDeversoir)((MetierDeversoir)getData("singularite")).creeClone();
    r.putData("singularite", deversoir);
    return r;
  }
/*
  public void putData(String _key, Object _value)
  {
      System.out.println("this="+this.toString()+"  putData(String _key="+_key+", Object _value="+_value+")");
      if (getData("singularite") == null) {
          super.putData(_key, _value);
      }
      else {
          System.out.println("bizarre");
      }
  }

  public void removeData(String _key)
  {
      System.out.println("this="+this.toString()+"  removeData(String _key="+_key+")");
      super.removeData(_key);
  }
*/
  @Override
  BuIcon getBuIcon()  {
    return ICON;
  }
}
