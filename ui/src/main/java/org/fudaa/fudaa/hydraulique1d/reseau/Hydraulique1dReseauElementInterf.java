/*
 * @file         Hydraulique1dReseauElementInterf.java
 * @creation     2003-06-17
 * @modification $Date: 2005-07-01 16:51:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2003 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
/**
 * Interface commune � tous les composants graphiques du r�seau pour r�cup�rer des informations.
 * Ces informations sont affich�s dans la barre d'�tat de la fen�tre du r�seau.
 * @version      $Revision: 1.3 $ $Date: 2005-07-01 16:51:30 $ by $Author: jm_lacombe $
 * @author       Jean-Marc Lacombe
 */
public interface Hydraulique1dReseauElementInterf {
  public String[] getInfos();
}
