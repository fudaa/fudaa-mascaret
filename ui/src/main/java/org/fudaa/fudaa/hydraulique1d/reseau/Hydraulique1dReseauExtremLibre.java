/*
 * @file         Hydraulique1dReseauExtremLibre.java
 * @creation     2000-11-02
 * @modification $Date: 2007-11-20 11:42:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
import java.awt.Color;

import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

import com.memoire.dja.DjaAnchor;
import com.memoire.dja.DjaCircle;
/**
 * Composant graphique du r�seau hydraulique repr�sentant une extr�mit� libre d'un bief.
 * @see MetierExtremite
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:42:42 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauExtremLibre
  extends DjaCircle
  implements Hydraulique1dReseauElementInterf {
  Hydraulique1dReseauExtremLibre(MetierExtremite extremite, boolean presenceQualiteDEau,int numeroAffiche) {
    super();
    super.setWidth(15);
    super.setColor(Color.red);
    if (extremite!=null) {
      super.addText(Integer.toString(numeroAffiche));
      putData("extremite", extremite);
      if (extremite.conditionLimite()==null)
        extremite.creeLimite();
      // if (extremite.creeLimiteQualiteDEau() == null)
      if (extremite.conditionLimiteQualiteDEau()==null)
        extremite.creeLimiteQualiteDEau();
    }
  }
  Hydraulique1dReseauExtremLibre() {
    this(null,false,-1);
  }
  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauExtremLibre r=(Hydraulique1dReseauExtremLibre)super.clone();
    r.removeData("singularite");
    return r;
  }
  @Override
  public DjaAnchor[] getAnchors() {
    int x= getX();
    int y= getY();
    int w= getWidth();
    int h= getHeight();
    DjaAnchor[] r= new DjaAnchor[1];
    r[0]= new DjaAnchor(this, 0, ANY, x + w / 2, y + h / 2);
    return r;
  }
  @Override
  public String[] getInfos() {
    MetierHydraulique1d iobjet= (MetierHydraulique1d)getData("extremite");
    return iobjet.getInfos();
  }
}
