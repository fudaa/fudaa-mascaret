/*
 * @file         Hydraulique1dReseauFrame.java
 * @creation     04/09/2000
 * @modification $Date: 2008-03-07 14:35:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.border.BevelBorder;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierSensDebitLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierCaracteristiqueLiaison;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEventListener;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSource;
import org.fudaa.ebli.impression.EbliDjaFrame;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dSeuilChooser;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;

import EDU.auburn.VGJ.algorithm.GraphUpdate;
import EDU.auburn.VGJ.algorithm.shawn.Spring;
import EDU.auburn.VGJ.graph.GMLobject;
import EDU.auburn.VGJ.graph.Graph;
import EDU.auburn.VGJ.graph.Node;
import EDU.auburn.VGJ.util.DPoint;
import EDU.auburn.VGJ.util.DRect;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuColumn;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPopupButton;
import com.memoire.dja.DjaAnchor;
import com.memoire.dja.DjaForm;
import com.memoire.dja.DjaFrame;
import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaLink;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaOptions;
import com.memoire.dja.DjaPaletteStroke;
import com.memoire.dja.DjaPaletteThickness;
import com.memoire.dja.DjaResource;
import com.memoire.dja.DjaText;
import com.memoire.dja.DjaVector;
import java.awt.Color;
import java.util.Map;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.fudaa.mascaret.MascaretImplementation;

/**
 * La fen�tre contenant le r�seau hydraulique.
 *
 * @see com.memoire.dja.DjaGrid
 * @see com.memoire.dja.DjaGridInteractive
 * @version $Revision: 1.36 $ $Date: 2008-03-07 14:35:00 $ by $Author: jm_lacombe $
 * @author Jean-Marc Lacombe
 */
public final class Hydraulique1dReseauFrame
        extends EbliDjaFrame implements H1dObjetEventListener {

//  private BuToggleButton pbReseau_;
  MetierEtude1d etude_;
  MetierReseau reseau_;
  Hydraulique1dIHMRepository ihmP_;
  BuLabel lbStatus1_, lbStatus2_;

  Hydraulique1dReseauPalette srcReseau_;
  Hydraulique1dReseauPaletteBracket srcbrcks_;
  DjaPaletteStroke srcstrks_;
  DjaPaletteThickness srcthkns_;
  BuButton btPositionner_;

  private JToggleButton btZoom_;
  private JToggleButton btMoveView_;
//  private final String cmdZoomIncrease_ = "ZOOMINCREASE";
  public final static String ZOOM_ACTION = "ZOOM";
  public final static String MOVE_ACTION = "MOVE";

//  private DObjetEventListenerSupport evtSupport_;
  /**
   * Un constructeur simplifi�.
   *
   * @param _app
   * @param _file
   * @param _etude
   */
  public Hydraulique1dReseauFrame(BuCommonImplementation _app, String _file, MetierEtude1d _etude) {
    this(_app, _file, new MascaretGridInteractive(), _etude);
    Hydraulique1dReseauMouseAdapter mouseAdapter = new Hydraulique1dReseauMouseAdapter(this,_etude);
    getGrid().addMouseListener(mouseAdapter);
    getGrid().addMouseMotionListener(mouseAdapter);
    getGrid().addGridListener(new Hydraulique1dReseauGridAdapter(_etude.reseau()));
  }

  public Hydraulique1dReseauFrame(
          BuCommonImplementation _app,
          String _file,
          MascaretGridInteractive _grid,
          MetierEtude1d _etude) {
    this(_app, _file, _grid, _etude, null);
  }

  public Hydraulique1dReseauFrame(
          BuCommonImplementation _app,
          String _file,
          MascaretGridInteractive _grid,
          MetierEtude1d _etude,
          Hydraulique1dIHMRepository _ihmP) {
    super(_app, _file, _grid, false);
    etude_ = _etude;
    reseau_ = _etude.reseau();
    ihmP_ = _ihmP;

    Notifieur.getNotifieur().addObjetEventListener(this);
    _grid.setParentFrame(this);
    //
    srcReseau_ = new Hydraulique1dReseauPalette(this);

    grid_.addPropertyChangeListener("interactive", new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        displayHidePaletteNetwork();
      }
    });
// 
    displayHidePaletteNetwork();
    //-- aha - 2.3.3 move position of frame--//
//    pbReseau_.setPaletteLocation(getLocation().x+150,getLocation().y+20);
    srcbrcks_ = new Hydraulique1dReseauPaletteBracket(this);
    pbBrcks_ = new BuPopupButton("Extr�mit�s", srcbrcks_);
    pbBrcks_.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Palette d'extr�mit�s"));
    pbBrcks_.setIcon(DjaResource.DJA.getIcon("dja-links"));
    pbBrcks_.setEnabled(grid_.isInteractive());
    srcstrks_ = new DjaPaletteStroke(this);
    pbStrks_ = new BuPopupButton("Traits", srcstrks_);
    pbStrks_.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Palette de traits"));
    pbStrks_.setIcon(DjaResource.DJA.getIcon("dja-strokes"));
    pbStrks_.setEnabled(grid_.isInteractive());
    srcthkns_ = new DjaPaletteThickness(this);
    pbThkns_ = new BuPopupButton("Epaisseurs", srcthkns_);
    pbThkns_.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Palette d'�paisseurs"));
    pbThkns_.setIcon(DjaResource.DJA.getIcon("dja-thickness"));
    pbThkns_.setEnabled(grid_.isInteractive());

    btPositionner_ = new BuButton();
    btPositionner_.setIcon(Hydraulique1dResource.HYDRAULIQUE1D.getIcon("hydraulique1drepositionner"));
    btPositionner_.setActionCommand("REPOSITIONNER");
    btPositionner_.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Positionnement automatique"));
    btPositionner_.setName("btRECENTER");
    btPositionner_.setRequestFocusEnabled(false);
    btPositionner_.setEnabled(grid_ instanceof DjaGridInteractive);
    btPositionner_.addActionListener(this);

    //-- add zoom buttons --//
    btZoom_ = new JToggleButton();
    btZoom_.setIcon(EbliResource.EBLI.getIcon("zoom-layer"));
    btZoom_.setActionCommand(ZOOM_ACTION);
    btZoom_.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("Zoom"));
    btZoom_.setName("bt" + ZOOM_ACTION);
    btZoom_.setRequestFocusEnabled(false);
    btZoom_.addActionListener(this);

    btMoveView_ = new JToggleButton();
    btMoveView_.setIcon(EbliResource.EBLI.getIcon("main"));
    btMoveView_.setActionCommand(MOVE_ACTION);
    btMoveView_.setToolTipText(Hydraulique1dResource.HYDRAULIQUE1D.getString("move"));
    btMoveView_.setName("bt" + MOVE_ACTION);
    btMoveView_.setRequestFocusEnabled(false);
    btMoveView_.addActionListener(this);

    //--add zoom mouse wheel listener --//
    addZoomMouseWheelListener();

    lbStatus1_ = new BuLabel();
    lbStatus1_.setMinimumSize(new Dimension(40, 20));
    lbStatus1_.setBorder(new BevelBorder(BevelBorder.LOWERED));
    lbStatus2_ = new BuLabel();
    lbStatus2_.setPreferredSize(new Dimension(250, 20));
    lbStatus2_.setBorder(new BevelBorder(BevelBorder.LOWERED));
    JSplitPane sptVert
            = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, lbStatus1_, lbStatus2_);
    sptVert.setOneTouchExpandable(true);
    sptVert.setDividerLocation(160);
    sptVert.setPreferredSize(new Dimension(400, 25));

    JComponent content = (JComponent) getContentPane();
    content.add(sptVert, BuBorderLayout.SOUTH);
    Dimension ps = new Dimension(400, 400);
    setPreferredSize(ps);
    setSize(ps);
    _grid.setParentFrame(this);
    setBackground(Color.WHITE);
    scrollPane_.setBackground(Color.WHITE);
    scrollPane_.setOpaque(true);
    content.setDoubleBuffered(false);
    grid_.setDoubleBuffered(false);
    scrollPane_.setDoubleBuffered(false);
    setDoubleBuffered(false);
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return CtuluLibImage.produceImageForComponent(grid_, _w, _h, _params);
  }

  /**
   * R�implementation de BuCutCopyPasteInterface pour d�sactiver la fonction
   */
  @Override
  public void copy() {
  }

  public void displayHidePaletteNetwork() {
//    pbReseau_.setEnabled(grid_.isInteractive());
    final BuColumn leftColumn = app_.getMainPanel().getLeftColumn();
    if (grid_.isInteractive()) {
      ((MascaretImplementation) app_).ensureRigthColumnIsVisible(leftColumn);
      if (srcReseau_.getParent() != leftColumn) {
        removeExistingPalette(leftColumn);
        leftColumn.add(srcReseau_);
      }
    }
    srcReseau_.setVisible(grid_.isInteractive());
    leftColumn.revalidate();
//    app_.getMainPanel().updateSplits();
    app_.getMainPanel().doLayout();
    leftColumn.doLayout();

  }

  private void removeExistingPalette(BuColumn buComp) {
    for (Component comp : buComp.getComponents()) {
      if (comp instanceof Hydraulique1dReseauPalette) {
        buComp.remove(comp);
      }
    }
  }

  /**
   * R�implementation de BuCutCopyPasteInterface pour d�sactiver la fonction
   */
  @Override
  public void cut() {
  }

  /**
   * R�implementation de BuCutCopyPasteInterface pour d�sactiver la fonction
   */
  @Override
  public void paste() {
  }

  /**
   * R�implementation de BuCutCopyPasteInterface pour d�sactiver la fonction
   */
  @Override
  public void duplicate() {
  }

  /**
   * Repositionnement automatique des objets suivant l'algorithme de Springlayout.
   *
   * @return True si l'action s'est bien pass�e. Sinon, erreur.
   */
  public boolean repositionner() {
    GraphUpdate graphUpdate = new GraphUpdate() {
      @Override
      public void center() {
      }

      @Override
      public Frame getFrame() {
        return Hydraulique1dReseauFrame.this.getApp().getFrame();
      }

      @Override
      public double getHSpacing() { // Semble ne servir a rien.
        return 100;
      }

      @Override
      public Node getSelectedNode() {
        return null;
      }

      @Override
      public double getVSpacing() { // Semble ne servir a rien.
        return 100;
      }

      @Override
      public void scale(double arg0) {
      }

      @Override
      public void update(boolean arg0) {
      }

      @Override
      public DRect windowRect() {
        int size = Math.min(scrollPane_.getWidth(), scrollPane_.getHeight());
        return new DRect(0, 0, size * 1.8, size * 1.8);
      }
    };

    // Alimentation du graphe.
    Graph graph = new Graph();
    Hashtable hid2dja = new Hashtable(); // Identifiant vers objet dja
    Hashtable hdja2vgj = new Hashtable(); // Dja objet vers vgj node
    Hashtable hdjaanc2vgj = new Hashtable(); // Dja ancre vers vgj node
    DjaVector objs = grid_.getObjects();
    int id = 0;

    // Les noeuds
    for (Enumeration e = objs.elements(); e.hasMoreElements();) {
      DjaObject gobj = (DjaObject) e.nextElement();

      if (gobj instanceof Hydraulique1dReseauExtremLibre || // Noeud extremite libre
              gobj instanceof Hydraulique1dReseauCasier || // Noeud casier
              gobj instanceof Hydraulique1dReseauNoeud) {       // Noeud confluent
        int ind = graph.insertNode();
        Node nd = graph.getNodeFromIndex(ind);
        nd.setPosition(new DPoint(gobj.getX() + gobj.getWidth() / 2, gobj.getY() + gobj.getHeight() / 2));
        if (gobj instanceof Hydraulique1dReseauExtremLibre) {
          nd.setLabel("Ext" + ind);
          nd.setShape(Node.OVAL);
          nd.setBoundingBox(gobj.getWidth(), gobj.getHeight());
        } else if (gobj instanceof Hydraulique1dReseauCasier) {
          nd.setLabel("Cas" + ind);
          nd.setShape(Node.RECTANGLE);
          nd.setBoundingBox(gobj.getWidth(), gobj.getHeight());
        } else if (gobj instanceof Hydraulique1dReseauNoeud) {
          nd.setLabel("Cnf" + ind);
          nd.setShape(Node.OVAL);
          nd.setBoundingBox(gobj.getWidth(), gobj.getHeight());
        }

        nd.setId(id);
        hid2dja.put(new Integer(id), gobj);
        hdja2vgj.put(gobj, nd);
        id++;
      } // On rajoute un noeud sur le bief pour l'ancre concern�e
      else if (gobj instanceof Hydraulique1dReseauLiaisonCasier) {
        Hydraulique1dReseauLiaisonCasier gliai = (Hydraulique1dReseauLiaisonCasier) gobj;
        if (gliai.getBeginObject() == null) {
          return false;
        }
        if (gliai.getBeginObject() instanceof Hydraulique1dReseauBiefCourbe) {
          Hydraulique1dReseauBiefCourbe gbief = (Hydraulique1dReseauBiefCourbe) gliai.getBeginObject();
          int pos = gliai.getBeginPosition();
          DjaAnchor anc = gbief.getAnchors()[pos];
          String key = gbief.getData("bief").toString() + "-" + pos;
          int ind = graph.insertNode();
          Node nd = graph.getNodeFromIndex(ind);
          nd.setLabel("Fic" + ind);
          nd.setId(id);
          nd.setPosition(new DPoint(anc.getX(), anc.getY()));
          hdjaanc2vgj.put(key, nd);
          id++;
        }
        if (gliai.getEndObject() == null) {
          return false;
        }
        if (gliai.getEndObject() instanceof Hydraulique1dReseauBiefCourbe) {
          Hydraulique1dReseauBiefCourbe gbief = (Hydraulique1dReseauBiefCourbe) gliai.getEndObject();
          int pos = gliai.getEndPosition();
          DjaAnchor anc = gbief.getAnchors()[pos];
          String key = gbief.getData("bief").toString() + "-" + pos;
          int ind = graph.insertNode();
          Node nd = graph.getNodeFromIndex(ind);
          nd.setLabel("Fic" + ind);
          nd.setId(id);
          nd.setPosition(new DPoint(anc.getX(), anc.getY()));
          hdjaanc2vgj.put(key, nd);
          id++;
        }
      }
    }

    // Les edges
    for (Enumeration e = objs.elements(); e.hasMoreElements();) {
      DjaObject gobj = (DjaObject) e.nextElement();

      if (gobj instanceof Hydraulique1dReseauBiefCourbe) { // Bief
        Hydraulique1dReseauBiefCourbe gbief = (Hydraulique1dReseauBiefCourbe) gobj;
        if (gbief.getBeginObject() == null) {
          return false;
        }
        Node nd1 = (Node) hdja2vgj.get(gbief.getBeginObject());
        if (nd1 == null) {
          return false;
        }

        Node nd2;
        DjaAnchor[] ancs = gbief.getAnchors();
        for (int i = 0; i < ancs.length; i++) {
          String key = gbief.getData("bief").toString() + "-" + i;
          if (hdjaanc2vgj.get(key) != null) { // 1 noeud est sur cette ancre => Edge entre noeud 1 et noeud 2
            nd2 = (Node) hdjaanc2vgj.get(key);
            graph.insertEdge(graph.getIndexFromNode(nd1), graph.getIndexFromNode(nd2));
            nd1 = nd2;
          }
        }
        if (gbief.getEndObject() == null) {
          return false;
        }
        nd2 = (Node) hdja2vgj.get(gbief.getEndObject());
        if (nd2 == null) {
          return false;
        }
        graph.insertEdge(graph.getIndexFromNode(nd1), graph.getIndexFromNode(nd2));
      } else if (gobj instanceof Hydraulique1dReseauLiaisonCasier) { // Liaison casier
        Hydraulique1dReseauLiaisonCasier gliai = (Hydraulique1dReseauLiaisonCasier) gobj;
        Node nd1;
        Node nd2;
        if (gliai.getBeginObject() instanceof Hydraulique1dReseauBiefCourbe) {
          Hydraulique1dReseauBiefCourbe gbief = (Hydraulique1dReseauBiefCourbe) gliai.getBeginObject();
          String key = gbief.getData("bief").toString() + "-" + gliai.getBeginPosition();
          nd1 = (Node) hdjaanc2vgj.get(key);
        } else {
          nd1 = (Node) hdja2vgj.get(gliai.getBeginObject());
        }
        if (gliai.getEndObject() instanceof Hydraulique1dReseauBiefCourbe) {
          Hydraulique1dReseauBiefCourbe gbief = (Hydraulique1dReseauBiefCourbe) gliai.getEndObject();
          String key = gbief.getData("bief").toString() + "-" + gliai.getEndPosition();
          nd2 = (Node) hdjaanc2vgj.get(key);
        } else {
          nd2 = (Node) hdja2vgj.get(gliai.getEndObject());
        }
        graph.insertEdge(graph.getIndexFromNode(nd1), graph.getIndexFromNode(nd2));
      }
    }

    // Layout
    Spring lySpring = new Spring();
    lySpring.compute(graph, graphUpdate);

    // R�initialisation de la position des objets Dja
    for (Enumeration e = hid2dja.keys(); e.hasMoreElements();) {
      Integer key = (Integer) e.nextElement();
      id = key.intValue();
      DjaObject gobj = (DjaObject) hid2dja.get(key);
      if (gobj instanceof Hydraulique1dReseauExtremLibre || // Noeud extremite libre
              gobj instanceof Hydraulique1dReseauCasier || // Noeud casier
              gobj instanceof Hydraulique1dReseauNoeud) {       // Noeud confluent

        DPoint pos = graph.getNodeFromId(id).getPosition();
        gobj.setX((int) pos.x - gobj.getWidth() / 2);
        gobj.setY((int) pos.y - gobj.getHeight() / 2);
      }
    }

    repositionnerAretes();
    repaint();
    grid_.recenter();

    if (false) { // Pour debug, cr�ation d'un fichier GML contenant le graph, qui peut �tre relu par l'appli VGJ.
      PrintStream ps;
      try {
        ps = new PrintStream(new FileOutputStream("out.gml"));
        GMLobject GMLfile_ = new GMLobject(null, GMLobject.GMLfile);
        GMLfile_.addObjectToEnd(new GMLobject("graph", GMLobject.GMLlist));
        GMLobject gmlgraph = GMLfile_.getGMLSubObject("graph", GMLobject.GMLlist, false);
        graph.setGMLvalues(gmlgraph);
        gmlgraph.prune();

        ps.println(GMLfile_.toString(0));
        ps.close();
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }
    }

    return true;
  }

  /**
   * Repositionne les edges sur les bonnes ancres des noeuds. Les edges sont les biefs ou les liaisons, les noeuds sont les confluents ou les casiers.
   */
  private void repositionnerAretes() {
    DjaVector objs = grid_.getObjects();

    for (Enumeration e = objs.elements(); e.hasMoreElements();) {
      DjaObject gobj = (DjaObject) e.nextElement();

      if (gobj instanceof Hydraulique1dReseauLiaisonCasier || // On limite le repositionnement aux biefs et liaisons.
              gobj instanceof Hydraulique1dReseauBiefCourbe) {
        DjaLink glien = (DjaLink) gobj;

        DjaObject gobj1 = glien.getBeginObject();
        int pos1 = glien.getBeginPosition();
        int x1;
        int y1;
        if (gobj1 instanceof DjaLink) { // Connect� � un lien
          x1 = gobj1.getAnchors()[pos1].getX();
          y1 = gobj1.getAnchors()[pos1].getY();
        } else if (gobj1 instanceof DjaForm) { /// Connect� � une forme.
          x1 = gobj1.getX() + gobj1.getWidth() / 2;
          y1 = gobj1.getY() + gobj1.getHeight() / 2;
        } else { // Connect� � rien (gobj=null)
          x1 = glien.getBeginX();
          y1 = glien.getBeginY();
        }

        DjaObject gobj2 = glien.getEndObject();
        int pos2 = glien.getEndPosition();
        int x2;
        int y2;
        if (gobj2 instanceof DjaLink) { // Connect� � un lien
          x2 = gobj2.getAnchors()[pos2].getX();
          y2 = gobj2.getAnchors()[pos2].getY();
        } else if (gobj2 instanceof DjaForm) { /// Connect� � une forme.
          x2 = gobj2.getX() + gobj2.getWidth() / 2;
          y2 = gobj2.getY() + gobj2.getHeight() / 2;
        } else { // Connect� � rien (gobj=null)
          x2 = glien.getEndX();
          y2 = glien.getEndY();
        }

        if (gobj1 instanceof DjaForm) {
          DjaAnchor anc = getAncre(gobj1, x2, y2);
          glien.setBeginPosition(anc.getPosition());
        }
        if (gobj2 instanceof DjaForm) {
          DjaAnchor anc = getAncre(gobj2, x1, y1);
          glien.setEndPosition(anc.getPosition());
        }
      }
    }
  }

  /**
   * Initialisation depuis le reseau. Cette methode nettoie la fenetre, et reconstruit les entites reseau de mani�re automatique.
   */
  public void initFromEtude(MetierEtude1d _etd) {
    HashMap hnum2Noeud = new HashMap();

    // Mapping bief vers sa representation graphique
    Hashtable hbief2graph = new Hashtable();
    // Mapping casier vers sa representation graphique
    Hashtable hcasier2graph = new Hashtable();

    MetierBief[] biefs = reseau_.biefs();
    for (int i = 0; i < biefs.length; i++) {
      // Creation des biefs
      Hydraulique1dReseauBiefCourbe gbief = new Hydraulique1dReseauBiefDroit(biefs[i]);
      hbief2graph.put(biefs[i], gbief);
      // Recherche si d'autres biefs ont meme extremites. Auquel cas, le bief sera courb�.
      int nbMemeExtrs = 0;
      for (int j = 0; j < i; j++) {
        if (biefs[j].extrAmont().noeudRattache() != null
                && biefs[j].extrAmont().noeudRattache() == biefs[i].extrAmont().noeudRattache()
                && biefs[j].extrAval().noeudRattache() != null
                && biefs[j].extrAval().noeudRattache() == biefs[i].extrAval().noeudRattache()) {
          nbMemeExtrs++;
        }
      }
//      decalage(gbief,biefs[i].indice());
      gbief.setCourbure(gbief.getMiddleX(), gbief.getMiddleY() + 10 * (nbMemeExtrs % 2 * 2 - 1) * ((nbMemeExtrs + 1) / 2));
      grid_.add(gbief);

      // L'extremite amont
      MetierExtremite extrAmont = biefs[i].extrAmont();
      if (extrAmont.noeudRattache() == null) {
        Hydraulique1dReseauExtremLibre gextr = new Hydraulique1dReseauExtremLibre(extrAmont, etude_.qualiteDEau().parametresModeleQualiteEau().
                presenceTraceurs(), extrAmont.numero());
        gextr.setX(gbief.getBeginX());
        gextr.setY(gbief.getBeginY());
        gbief.setBegin(gextr, 0, 0);
        grid_.add(gextr);
      } // Ou le noeud s'il n'est deja cr�e
      else {
        MetierNoeud nd = extrAmont.noeudRattache();
        Hydraulique1dReseauNoeud gnd = (Hydraulique1dReseauNoeud) hnum2Noeud.get(new Integer(nd.numero()));
        if (gnd == null) {
          gnd = new Hydraulique1dReseauNoeud(nd);
          hnum2Noeud.put(new Integer(nd.numero()), gnd);
          gnd.setX(gbief.getBeginX());
          gnd.setY(gbief.getBeginY());
          grid_.add(gnd);
        }
        gbief.setBegin(gnd, 0, 0);
      }

      // L'extremite aval
      MetierExtremite extrAval = biefs[i].extrAval();
      if (extrAval.noeudRattache() == null) {
        Hydraulique1dReseauExtremLibre gextr = new Hydraulique1dReseauExtremLibre(extrAval, etude_.qualiteDEau().parametresModeleQualiteEau().
                presenceTraceurs(), extrAval.numero());
        gextr.setX(gbief.getEndX());
        gextr.setY(gbief.getEndY());
        gbief.setEnd(gextr, 0, 0);
        grid_.add(gextr);
      } // Ou le noeud s'il n'est deja cr�e
      else {
        MetierNoeud nd = extrAval.noeudRattache();
        Hydraulique1dReseauNoeud gnd = (Hydraulique1dReseauNoeud) hnum2Noeud.get(new Integer(nd.numero()));
        if (gnd == null) {
          gnd = new Hydraulique1dReseauNoeud(nd);
          hnum2Noeud.put(new Integer(nd.numero()), gnd);
          gnd.setX(gbief.getEndX());
          gnd.setY(gbief.getEndY());
          grid_.add(gnd);
        }
        gbief.setEnd(gnd, 0, 0);
      }

      // Les deversoirs pour le bief
      MetierDeversoir[] devers = biefs[i].deversoirs();

      for (int j = 0; j < devers.length; j++) {
        // Recherche de l'ancre la plus proche de l'abscisse
        DjaAnchor ancre = getAncre(gbief, devers[j].abscisse());

        Hydraulique1dReseauDeversoir gdevers = new Hydraulique1dReseauDeversoir(devers[j]);
        gdevers.setEndObject(ancre.getParent());
        gdevers.setEndPosition(ancre.getPosition());
        gdevers.setBeginObject(ancre.getParent());
        gdevers.setBeginPosition(ancre.getPosition());

        grid_.add(gdevers);
      }

      // Les apports pour le bief
      MetierApport[] apports = biefs[i].apports();

      for (int j = 0; j < apports.length; j++) {
        // Recherche de l'ancre la plus proche de l'abscisse
        DjaAnchor ancre = getAncre(gbief, apports[j].abscisse());

        Hydraulique1dReseauApport gapport = new Hydraulique1dReseauApport(apports[j]);
        gapport.setEndObject(ancre.getParent());
        gapport.setEndPosition(ancre.getPosition());
        gapport.setBeginObject(ancre.getParent());
        gapport.setBeginPosition(ancre.getPosition());

        grid_.add(gapport);
      }

      // Les seuils pour le bief
      MetierSeuil[] seuils = biefs[i].seuils();

      for (int j = 0; j < seuils.length; j++) {
        // Recherche de l'ancre la plus proche de l'abscisse
        DjaAnchor ancre = getAncre(gbief, seuils[j].abscisse());

        Hydraulique1dReseauSeuil gseuil = new Hydraulique1dReseauSeuil(seuils[j]);
        gseuil.setEndObject(ancre.getParent());
        gseuil.setEndPosition(ancre.getPosition());
        gseuil.setBeginObject(ancre.getParent());
        gseuil.setBeginPosition(ancre.getPosition());

        grid_.add(gseuil);
      }

      // Les pertes de charge pour le bief
      MetierPerteCharge[] pertes = biefs[i].pertesCharges();

      for (int j = 0; j < pertes.length; j++) {
        // Recherche de l'ancre la plus proche de l'abscisse
        DjaAnchor ancre = getAncre(gbief, pertes[j].abscisse());

        Hydraulique1dReseauPerteCharge gperte = new Hydraulique1dReseauPerteCharge(pertes[j]);
        gperte.setEndObject(ancre.getParent());
        gperte.setEndPosition(ancre.getPosition());
        gperte.setBeginObject(ancre.getParent());
        gperte.setBeginPosition(ancre.getPosition());

        grid_.add(gperte);
      }

      // Les sources tracer pour le bief
      MetierSource[] sources = biefs[i].sources();

      for (int j = 0; j < sources.length; j++) {
        // Recherche de l'ancre la plus proche de l'abscisse
        DjaAnchor ancre = getAncre(gbief, sources[j].abscisse());

        Hydraulique1dReseauSource gsource = new Hydraulique1dReseauSource(sources[j]);
        gsource.setEndObject(ancre.getParent());
        gsource.setEndPosition(ancre.getPosition());
        gsource.setBeginObject(ancre.getParent());
        gsource.setBeginPosition(ancre.getPosition());

        grid_.add(gsource);
      }
    }

    // Les casiers.
    MetierCasier[] casiers = reseau_.casiers();
    for (int i = 0; i < casiers.length; i++) {
      Hydraulique1dReseauCasier gcasier = new Hydraulique1dReseauCasier(casiers[i]);
      hcasier2graph.put(casiers[i], gcasier);
//      decalage(gcasier, casiers[i].numero() - 1);
      grid_.add(gcasier);
    }

    // Les liaisons
    MetierLiaison[] liaisons = reseau_.liaisons();
    for (int i = 0; i < liaisons.length; i++) {
      Hydraulique1dReseauLiaisonCasier gliaison = new Hydraulique1dReseauLiaisonCasier(liaisons[i]);
      if (liaisons[i].isRiviereCasier()) {
        // Accrochage au bief => Recherche de l'ancre la plus proche de l'abscisse
        Hydraulique1dReseauBiefCourbe gbief = (Hydraulique1dReseauBiefCourbe) hbief2graph.get(liaisons[i].getBiefRattache());
        DjaAnchor ancre1 = getAncre(gbief, liaisons[i].getAbscisse());
        gliaison.setBeginObject(ancre1.getParent());
        gliaison.setBeginPosition(ancre1.getPosition());

        // Accrochage au casier => Recherche de l'ancre la plus proche.
        Hydraulique1dReseauCasier gcasier = (Hydraulique1dReseauCasier) hcasier2graph.get(liaisons[i].getCasierRattache());
        DjaAnchor ancre = getAncre(gcasier, ancre1.getX(), ancre1.getY());
        gliaison.setEndObject(ancre.getParent());
        gliaison.setEndPosition(ancre.getPosition());
      } else {
        Hydraulique1dReseauCasier gcasier1 = (Hydraulique1dReseauCasier) hcasier2graph.get(liaisons[i].getCasierAmontRattache());
        Hydraulique1dReseauCasier gcasier2 = (Hydraulique1dReseauCasier) hcasier2graph.get(liaisons[i].getCasierAvalRattache());
        // Accrochage au casier amont => Recherche de l'ancre la plus proche.
        DjaAnchor ancre1 = getAncre(gcasier1, gcasier2.getX(), gcasier2.getY());
        gliaison.setBeginObject(ancre1.getParent());
        gliaison.setBeginPosition(ancre1.getPosition());

        // Accrochage au casier aval => Recherche de l'ancre la plus proche.
        DjaAnchor ancre2 = getAncre(gcasier2, ancre1.getX(), ancre1.getY());
        gliaison.setEndObject(ancre2.getParent());
        gliaison.setEndPosition(ancre2.getPosition());
      }

      gliaison.maj(); // La mise a jour depend des accrochages.
      grid_.add(gliaison);
    }
  }

  /**
   * Retourne l'ancre d'un bief la plus proche d'une abscisse donn�e
   *
   * @param _gobj Le bief
   * @param _abs L'abscisse.
   * @return L'ancre.
   */
  public DjaAnchor getAncre(Hydraulique1dReseauBiefCourbe _gobj, double _abs) {
    MetierBief bief = (MetierBief) _gobj.getData("bief");
    DjaAnchor[] ancres = _gobj.getAnchors();

    // Recherche de l'ancre la plus pres.
    double absdeb = bief.extrAmont().profilRattache().abscisse();
    double absfin = bief.extrAval().profilRattache().abscisse();
    int ind = (int) ((ancres.length + 1) * (_abs - absdeb) / (absfin - absdeb) + 0.5) - 1;
    ind = Math.max(ind, 0);
    ind = Math.min(ind, ancres.length - 1);

    return ancres[ind];
  }

  /**
   * Retourne l'ancre d'un objet la plus proche d'une position donnee
   *
   * @param _gobj L'objet
   * @param _x la position x
   * @param _y la position y
   * @return L'ancre.
   */
  public DjaAnchor getAncre(DjaObject _gobj, int _x, int _y) {
    DjaAnchor[] ancres = _gobj.getAnchors();
    double dist = Double.POSITIVE_INFINITY;
    int ind = 0;
    for (int i = 0; i < ancres.length; i++) {
      double d = Math.sqrt((ancres[i].getX() - _x) * (ancres[i].getX() - _x) + (ancres[i].getY() - _y) * (ancres[i].getY() - _y));
      if (d < dist) {
        ind = i;
        dist = d;
      }
    }

    return ancres[ind];
  }

  public void addBief(String type) {
    MetierBief ibief = reseau_.creeBief();
    Hydraulique1dReseauBiefCourbe bief;
    if (type.equalsIgnoreCase("courbe")) {
      bief = new Hydraulique1dReseauBiefCourbe(ibief);
    } else {
      bief = new Hydraulique1dReseauBiefDroit(ibief);
    }
    decalage(bief, ibief.indice());
    grid_.add(bief);
    repaint();
  }

  public void setStatus(String partieWest, String partieCenter) {
    lbStatus1_.setText(partieWest);
    lbStatus2_.setText(partieCenter);
  }

  public void addBief() {
    addBief("droit");
  }

  public void addBiefCourbe() {
    addBief("courbe");
  }

  public void addBiefDroit() {
    addBief("droit");
  }

  public void addNoeud() {
    MetierNoeud in = reseau_.creeNoeud();
    Hydraulique1dReseauNoeud noeud = new Hydraulique1dReseauNoeud(in);
    decalage(noeud, in.numero() - 1);
    grid_.add(noeud);
    repaint();
  }

  public void addQApport() {
    MetierApport iapport = reseau_.creeApport();
    Hydraulique1dReseauApport apport = new Hydraulique1dReseauApport(iapport);
    decalage(apport, iapport.numero() - 1);
    grid_.add(apport);
    initNumerosSingularites();
    repaint();
  }

  public void addSourceTraceur() {
    if (sourceAutoriser()) {
      MetierSource isource = reseau_.creeSource();
      Hydraulique1dReseauSource source = new Hydraulique1dReseauSource(isource);
      decalage(source, isource.numero() - 1);
      grid_.add(source);
      initNumerosSingularites();
      repaint();
    }
  }

  public void addPerteCharge() {
    MetierPerteCharge iperteCharge = reseau_.creePerteCharge();
    Hydraulique1dReseauPerteCharge perteCharge
            = new Hydraulique1dReseauPerteCharge(iperteCharge);
    decalage(perteCharge, iperteCharge.numero() - 1);
    grid_.add(perteCharge);
    initNumerosSingularites();
    repaint();
  }

  public void addSeuil() {
    MetierSeuil iseuil;
    boolean transcritique
            = (etude_.paramGeneraux().regime().value() == EnumMetierRegime._TRANSCRITIQUE);
    iseuil = new Hydraulique1dSeuilChooser(reseau_, transcritique).activate();
    if (iseuil != null) {
      Hydraulique1dReseauSeuil seuil = new Hydraulique1dReseauSeuil(iseuil);
      decalage(seuil, iseuil.numero() - 1);
      grid_.add(seuil);
      initNumerosSingularites();
      repaint();
    }
  }

  public void addDeversoir() {
    MetierDeversoir ideversoir = reseau_.creeDeversoir();
    Hydraulique1dReseauDeversoir deversoir
            = new Hydraulique1dReseauDeversoir(ideversoir);
    decalage(deversoir, ideversoir.numero() - 1);
    grid_.add(deversoir);
    initNumerosSingularites();
    repaint();
  }

  public void addCasier() {
    if (casierAutoriser()) {
      MetierCasier icasier = reseau_.ajouterCasier();
      Hydraulique1dReseauCasier casier = new Hydraulique1dReseauCasier(icasier);
      decalage(casier, icasier.numero() - 1);
      grid_.add(casier);
      repaint();
    }
  }

  public void addLiaisonCasier() {
    if (casierAutoriser()) {
      MetierLiaison iliaison = reseau_.ajouterLiaison();
      Hydraulique1dReseauLiaisonCasier liaison
              = new Hydraulique1dReseauLiaisonCasier(iliaison);
      decalage(liaison, iliaison.numero() - 1);
      grid_.add(liaison);
      repaint();
    }
  }

  public BuCommonImplementation getApp() {
    return app_;
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String action = _evt.getActionCommand();
    String arg = "";
    String MessageAvertissement = "";
    //HashSet ListObjetNonConnectes =new HashSet();
    int i = action.indexOf('(');
    if (i >= 0) {
      arg = action.substring(i + 1, action.length() - 1);
      action = action.substring(0, i);
    }

    if ("DJA_TOGGLE_INTERACTIVE".equals(action)) {
      DjaGridInteractive grille = (DjaGridInteractive) grid_;
      if (tbInteractive_.isSelected()) {
        btMoveView_.setSelected(false);
        btZoom_.setSelected(false);
        ((MascaretGridInteractive) grid_).setZoom(false);
        ((MascaretGridInteractive) grid_).setTranslationActive(false);
      }
      if (grille.isInteractive()) {

//        if (pbReseau_.isSelected()) {
//          pbReseau_.doClick();
//        }
        if (pbBrcks_.isSelected()) {
          pbBrcks_.doClick();
        }
        if (pbStrks_.isSelected()) {
          pbStrks_.doClick();
        }
        if (pbThkns_.isSelected()) {
          pbThkns_.doClick();
        }

        MessageAvertissement = ConstructionLiaisonsMetiers();

        if (!MessageAvertissement.isEmpty()) {
          BuDialogMessage dial
                  = new BuDialogMessage(app_, null, MessageAvertissement);
          dial.pack();
          dial.setLocationRelativeTo(grid_);
          dial.setModal(true);
          dial.show();
        }
      } // fin if (grille.isInteractive())
      else {

      }
      if (MessageAvertissement.isEmpty()) {
        super.actionPerformed(_evt);
      } else {
        throw new RuntimeException(MessageAvertissement);
      }
    } // fin if("DJA_TOGGLE_INTERACTIVE".equals(action))
    else if ("HYDRAULIQUE1D_CREATE_RESEAU".equals(action)) {
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("Bief").equals(arg)) {
        addBief();
      }
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("Confluent").equals(arg)) {
        addNoeud();
      }
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("D�bit d'apport").equals(arg)) {
        addQApport();
      }
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("Perte de charge locale").equals(arg)) {
        addPerteCharge();
      }
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("Barrage/Seuil").equals(arg)) {
        addSeuil();
      }
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("D�versoir lat�ral").equals(arg)) {
        addDeversoir();
      }
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("Casier").equals(arg)) {
        addCasier();
      }
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("Liaison casier").equals(arg)) {
        addLiaisonCasier();
      }
      if (Hydraulique1dResource.HYDRAULIQUE1D.getString("Source de traceur").equals(arg)) {
        addSourceTraceur();
      }
    } // Repoistionnement automatique
    else if ("REPOSITIONNER".equals(action)) {
      if (!repositionner()) {
        BuDialogMessage dial = new BuDialogMessage(app_, null,
                "Impossible de repositionner : Le r�seau n'est probablement pas correctement connect�.");
        dial.pack();
        dial.setLocationRelativeTo(grid_);
        dial.setModal(true);
        dial.show();
      }
    } else if (ZOOM_ACTION.equals(action)) {
      if (btZoom_.isSelected()) {
        btMoveView_.setSelected(false);
      }
      ((MascaretGridInteractive) grid_).setZoom(btZoom_.isSelected());

    } else if (MOVE_ACTION.equals(action)) {
      if (btMoveView_.isSelected()) {
        btZoom_.setSelected(false);
      }
      ((MascaretGridInteractive) grid_).setTranslationActive(btMoveView_.isSelected());
    } else {
      super.actionPerformed(_evt);
    }
  }

  /**
   * set mouse wheel listener to capture the wheel action and perform a zoom automatically.
   */
  public void addZoomMouseWheelListener() {
    grid_.addMouseWheelListener(new MouseAdapter() {
      public void mouseWheelMoved(MouseWheelEvent e) {
        ((MascaretGridInteractive) grid_).zoomOn(e, e.getPreciseWheelRotation() < 0);
        repaint(0);
      }

    });
  }

  /**
   * Cette fonction ajoute dans le mod�le m�tier les liens entre objet (bief-singularit�, bief-noeud...)
   */
  public String ConstructionLiaisonsMetiers() {
    DjaGridInteractive grille = (DjaGridInteractive) grid_;
    HashSet ListObjetNonConnectes = new HashSet();
    String MessageAvertissement = "";

    //Verification du reseau et de la grille
    Object[] objetsNonConnecte;//= Hydraulique1dReseauVerificateur.verifierBijectionMetierGraph(etude_,this);
    Enumeration elemEnum = grille.getObjects().elements();
    while (elemEnum.hasMoreElements()) {
      Object element = elemEnum.nextElement();
      if (element instanceof Hydraulique1dReseauNoeud) {
        MetierNoeud in
                = (MetierNoeud) ((Hydraulique1dReseauNoeud) element).getData("noeud");
        in.extremites(new MetierExtremite[0]);
      } else if (element instanceof Hydraulique1dReseauBiefCourbe) {
        MetierBief ib
                = (MetierBief) ((Hydraulique1dReseauBiefCourbe) element).getData(
                        "bief");
        ib.singularites(new MetierSingularite[0]);
      }
    }
    elemEnum = grille.getObjects().elements();
    while (elemEnum.hasMoreElements()) {
      Object element = elemEnum.nextElement();
      if (element instanceof Hydraulique1dReseauBiefCourbe) {
        Hydraulique1dReseauBiefCourbe biefGraph
                = (Hydraulique1dReseauBiefCourbe) element;
        DjaObject debut = biefGraph.getBeginObject();
        DjaObject fin = biefGraph.getEndObject();
        MetierBief ib = (MetierBief) biefGraph.getData("bief");
        if (debut == null) {
          Hydraulique1dReseauExtremLibre extremAmont
                  = new Hydraulique1dReseauExtremLibre(ib.extrAmont(), etude_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs(), (ib.
                          indice() * 2 + 1));
          extremAmont.setX(biefGraph.getBeginX());
          extremAmont.setY(biefGraph.getBeginY());
          biefGraph.setBegin(extremAmont, 0, 0);
          grid_.add(extremAmont);
          if (ib.extrAmont() != null) {
            ib.extrAmont().noeudRattache(null);
          }
          repaint();
        } else if (debut instanceof Hydraulique1dReseauNoeud) {
          MetierNoeud in
                  = (MetierNoeud) ((Hydraulique1dReseauNoeud) debut).getData("noeud");
          ib.extrAmont().noeudRattache(in);
          ib.extrAmont().conditionLimite(null);
          in.ajouteExtremite(ib.extrAmont());

        } else if (debut instanceof Hydraulique1dReseauExtremLibre) {
          if (ib.extrAmont() != null) {
            ib.extrAmont().noeudRattache(null);
          }
          //Cas anormale ou l'objet graphique est rattach� � un objet m�tier  null
          if (((Hydraulique1dReseauExtremLibre) debut).getData("extremite") == null) {
            //On supprime
            grid_.remove(debut);
            //On reconstruit
            Hydraulique1dReseauExtremLibre extremAmont
                    = new Hydraulique1dReseauExtremLibre(ib.extrAmont(), etude_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs(), (ib.
                            indice() * 2 + 1));
            extremAmont.setX(biefGraph.getBeginX());
            extremAmont.setY(biefGraph.getBeginY());
            biefGraph.setBegin(extremAmont, 0, 0);
            grid_.add(extremAmont);
            if (ib.extrAmont() != null) {
              ib.extrAmont().noeudRattache(null);
            }
            repaint();
          }

        }
        if (fin == null) {
          Hydraulique1dReseauExtremLibre extremAval
                  = new Hydraulique1dReseauExtremLibre(ib.extrAval(), etude_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs(), (ib.
                          indice() * 2 + 2));
          extremAval.setX(biefGraph.getEndX());
          extremAval.setY(biefGraph.getEndY());
          biefGraph.setEnd(extremAval, 0, 0);
          grid_.add(extremAval);
          if (ib.extrAval() != null) {
            ib.extrAval().noeudRattache(null);
          }
          repaint();
        } else if (fin instanceof Hydraulique1dReseauNoeud) {
          MetierNoeud in
                  = (MetierNoeud) ((Hydraulique1dReseauNoeud) fin).getData("noeud");
          ib.extrAval().noeudRattache(in);
          ib.extrAval().conditionLimite(null);
          in.ajouteExtremite(ib.extrAval());

        } else if (fin instanceof Hydraulique1dReseauExtremLibre) {
          if (ib.extrAval() != null) {
            ib.extrAval().noeudRattache(null);
          }
          //Cas anormale ou l'objet graphique est rattach� � un objet m�tier  null
          if (((Hydraulique1dReseauExtremLibre) fin).getData("extremite") == null) {
            //On supprime
            grid_.remove(fin);
            //On reconstruit
            Hydraulique1dReseauExtremLibre extremAval
                    = new Hydraulique1dReseauExtremLibre(ib.extrAval(), etude_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs(), (ib.
                            indice() * 2 + 2));
            extremAval.setX(biefGraph.getEndX());
            extremAval.setY(biefGraph.getEndY());
            biefGraph.setEnd(extremAval, 0, 0);
            grid_.add(extremAval);
            if (ib.extrAval() != null) {
              ib.extrAval().noeudRattache(null);
            }
            repaint();
          }

        }
      } else if (element instanceof Hydraulique1dReseauSingularite) {
        Hydraulique1dReseauSingularite singulGraph
                = (Hydraulique1dReseauSingularite) element;
        DjaObject objetAttache = singulGraph.getBeginObject();
        MetierSingularite is = (MetierSingularite) singulGraph.getData("singularite");
        if (objetAttache instanceof Hydraulique1dReseauBiefCourbe) {
          MetierBief ib
                  = (MetierBief) (((Hydraulique1dReseauBiefCourbe) objetAttache)
                  .getData("bief"));
          ib.ajouteSingularite(is);

        } else {
          ListObjetNonConnectes.add(is);
          MessageAvertissement
                  = MessageAvertissement
                  + "La singularit� `"
                  + is.nom()
                  + "` doit �tre rattach�e � un bief\n";
        }
      } else if (element instanceof Hydraulique1dReseauLiaisonCasier) {
        Hydraulique1dReseauLiaisonCasier liaisonGraph
                = (Hydraulique1dReseauLiaisonCasier) element;
        DjaObject objetAttacheDebut = liaisonGraph.getBeginObject();
        DjaObject objetAttacheFin = liaisonGraph.getEndObject();
        MetierLiaison il = (MetierLiaison) liaisonGraph.getData("liaison");
        if (objetAttacheDebut instanceof Hydraulique1dReseauBiefCourbe) {
          il.toRiviereCasier();
          MetierBief ib
                  = (MetierBief) (((Hydraulique1dReseauBiefCourbe) objetAttacheDebut)
                  .getData("bief"));
          il.setBiefRattache(ib);
          if (objetAttacheFin instanceof Hydraulique1dReseauCasier) {
            MetierCasier ic
                    = (MetierCasier) (((Hydraulique1dReseauCasier) objetAttacheFin)
                    .getData("casier"));
            il.setCasierRattache(ic);
          }
        } else if (objetAttacheFin instanceof Hydraulique1dReseauBiefCourbe) {
          il.toRiviereCasier();
          MetierBief ib
                  = (MetierBief) (((Hydraulique1dReseauBiefCourbe) objetAttacheFin)
                  .getData("bief"));
          il.setBiefRattache(ib);
          if (objetAttacheDebut instanceof Hydraulique1dReseauCasier) {
            MetierCasier ic
                    = (MetierCasier) (((Hydraulique1dReseauCasier) objetAttacheDebut)
                    .getData("casier"));
            il.setCasierRattache(ic);
          }
        } else if ((objetAttacheDebut instanceof Hydraulique1dReseauCasier)
                && (objetAttacheFin instanceof Hydraulique1dReseauCasier)) {
          il.toCasierCasier();
          MetierCasier icAmont
                  = (MetierCasier) (((Hydraulique1dReseauCasier) objetAttacheDebut)
                  .getData("casier"));
          MetierCasier icAval
                  = (MetierCasier) (((Hydraulique1dReseauCasier) objetAttacheFin)
                  .getData("casier"));
          il.setCasierAmontRattache(icAmont);
          il.setCasierAvalRattache(icAval);
        } else {
          ListObjetNonConnectes.add(il);
          MessageAvertissement
                  = MessageAvertissement
                  + "La liaison `"
                  + il.getInfos()[0]
                  + "` doit �tre rattach�e � un bief et un casier ou 2 casiers\n";
        }
        if (il.isOrifice()) {
          il.setSensDebit(getSensLiaison(liaisonGraph));
        }
      } //fin else if element instanceof .....
    } // fin while (enum.hasMoreElements())

    //Verification du reseau et de la grille
    objetsNonConnecte = Hydraulique1dReseauVerificateur.verifierBijectionMetierGraph(etude_, this);

    if (objetsNonConnecte != null) {
      for (int j = 0; j < objetsNonConnecte.length; j++) {
        if (ListObjetNonConnectes.add(objetsNonConnecte[j])) {
          MessageAvertissement
                  = MessageAvertissement
                  + "L'objet `"
                  + ((MetierHydraulique1d) objetsNonConnecte[j]).getInfos()[0]
                  + "` doit �tre rattach� � un bief\n";
        }
      }
    }

    return MessageAvertissement;

  }

  @Override
  public JComponent[] getSpecificTools() {
    JComponent[] r;
    pbStrks_.setDesktop((BuDesktop) getDesktopPane());
    pbThkns_.setDesktop((BuDesktop) getDesktopPane());
    pbBrcks_.setDesktop((BuDesktop) getDesktopPane());
    r = new JComponent[20];
    int buttonPosition = 0;

    r[buttonPosition++] = tbInteractive_;
    r[buttonPosition++] = null;
    r[buttonPosition++] = btRecenter_;
    r[buttonPosition++] = btPositionner_;
    r[buttonPosition++] = btFront_;
    r[buttonPosition++] = btBack_;
    r[buttonPosition++] = null;
    r[buttonPosition++] = btForeground_;
    r[buttonPosition++] = btBackground_;
    r[buttonPosition++] = btTextColor_;
    r[buttonPosition++] = null;
    r[buttonPosition++] = btAddText_;
    r[buttonPosition++] = btRemoveText_;
    r[buttonPosition++] = null;
    r[buttonPosition++] = pbStrks_;
    r[buttonPosition++] = pbThkns_;
    r[buttonPosition++] = pbBrcks_;
    r[buttonPosition++] = null;
//    r[buttonPosition++] = pbReseau_;

//    r[buttonPosition++] = null;
    r[buttonPosition++] = btZoom_;
    r[buttonPosition++] = btMoveView_;

    DjaFrame.setAlwaysEnable(btRecenter_);
    DjaFrame.setAlwaysEnable(btZoom_);
    DjaFrame.setAlwaysEnable(btMoveView_);
    updateInteractiveTools(r);

    return r;
  }

  /**
   * Implementation de ObjetEventListener appel� en cas de cr�ation.
   *
   * @param e l'�vement corba � traiter en cas de cr�ation.
   */
  @Override
  public void objetCree(H1dObjetEvent e) {
    /*	  MetierHydraulique1d source = e.getSource();

     String champ = e.getChamp();
     if ( ("numero".equals(champ)) &&
     (source instanceof DNoeud)) {
     DNoeud inoeud = (DNoeud) source;
     Hydraulique1dReseauNoeud noeudGraphique = rechercheNoeud(inoeud);
     try {
     DjaText texte0 = noeudGraphique.getTexts()[0];
     Integer.parseInt(texte0.getText().trim());
     // s'il n'y a pas eu d'exception NullPointer4Exception ou NumberFormatException
     texte0.setText("" + inoeud.numero());
     }
     catch (Exception ex) {
     // si il n'y a pas NullPointerException ou NumberFormatException
     //			  le texte du bief n'est pas mis � jour
     //			  Donc Rien � Faire !
     }
     }
     else if ( ("numero".equals(champ)) &&
     (source instanceof MetierExtremite)) {
     MetierExtremite iextremite = (MetierExtremite) source;
     Hydraulique1dReseauExtremLibre extremiteGraphique = rechercheExtremite(
     iextremite);
     try {
     DjaText texte0 = extremiteGraphique.getTexts()[0];
     Integer.parseInt(texte0.getText().trim());
     // s'il n'y a pas eu d'exception NullPointer4Exception ou NumberFormatException
     texte0.setText("" + iextremite.numero());
     }
     catch (Exception ex) {
     // si il n'y a pas NullPointerException ou NumberFormatException
     //			  le texte du bief n'est pas mis � jour
     //			  Donc Rien � Faire !
     }
     }
     else if ( ("numero".equals(champ)) &&
     (source instanceof MetierCasier)) {
     MetierCasier icasier = (MetierCasier) source;
     Hydraulique1dReseauCasier casierGraphique = rechercheCasier(icasier);
     try {
     DjaText texte0 = casierGraphique.getTexts()[0];
     Integer.parseInt(texte0.getText().trim());
     // s'il n'y a pas eu d'exception NullPointer4Exception ou NumberFormatException
     texte0.setText("" + icasier.numero());
     }
     catch (Exception ex) {
     // si il n'y a pas NullPointerException ou NumberFormatException
     //			  le texte du bief n'est pas mis � jour
     //			  Donc Rien � Faire !
     }
     }
     else if ( ("indice".equals(champ)) &&
     (source instanceof MetierBief)) {
     MetierBief ibief = (MetierBief) source;
     Hydraulique1dReseauBiefCourbe biefGraphique = rechercheBief(ibief);
     try {
     DjaText texte0 = biefGraphique.getTexts()[0];
     Integer.parseInt(texte0.getText().trim());
     // s'il n'y a pas eu d'exception NullPointer4Exception ou NumberFormatException
     texte0.setText("" + (ibief.indice() + 1));
     }
     catch (Exception ex) {
     // si il n'y a pas NullPointerException ou NumberFormatException
     //			  le texte du bief n'est pas mis � jour
     //			  Donc Rien � Faire !
     }
     }*/
  }

  /**
   * Implementation de ObjetEventListener appel� en cas de suppression.
   *
   * @param e l'�vement corba � traiter en cas de suppression.
   */
  @Override
  public void objetSupprime(H1dObjetEvent e) {
  }

  /**
   * Implementation de ObjetEventListener appel� en cas de modification.
   *
   * @param e l'�vement corba � traiter en cas de modification.
   */
  @Override
  public void objetModifie(H1dObjetEvent e) {
    MetierHydraulique1d source = e.getSource();
    String champ = e.getChamp();
    if (("numero".equals(champ))
            && (source instanceof MetierNoeud)) {
      MetierNoeud inoeud = (MetierNoeud) source;
      Hydraulique1dReseauNoeud noeudGraphique = rechercheNoeud(inoeud);
      try {
        DjaText texte0 = noeudGraphique.getTexts()[0];
        Integer.parseInt(texte0.getText().trim());
        // s'il n'y a pas eu d'exception NullPointer4Exception ou NumberFormatException
        texte0.setText("" + inoeud.numero());
      } catch (Exception ex) {
        // si il n'y a pas NullPointerException ou NumberFormatException
        // le texte du bief n'est pas mis � jour
        // Donc Rien � Faire !
      }
    } else if (("numero".equals(champ))
            && (source instanceof MetierExtremite)) {
      MetierExtremite iextremite = (MetierExtremite) source;
      Hydraulique1dReseauExtremLibre extremiteGraphique = rechercheExtremite(
              iextremite);
      try {
        DjaText texte0 = extremiteGraphique.getTexts()[0];
        Integer.parseInt(texte0.getText().trim());
        // s'il n'y a pas eu d'exception NullPointer4Exception ou NumberFormatException
        texte0.setText("" + iextremite.numero());
      } catch (Exception ex) {
        // si il n'y a pas NullPointerException ou NumberFormatException
        // le texte du bief n'est pas mis � jour
        // Donc Rien � Faire !
      }
    } else if (("numero".equals(champ))
            && (source instanceof MetierCasier)) {
      MetierCasier icasier = (MetierCasier) source;
      Hydraulique1dReseauCasier casierGraphique = rechercheCasier(icasier);
      try {
        DjaText texte0 = casierGraphique.getTexts()[0];
        Integer.parseInt(texte0.getText().trim());
        // s'il n'y a pas eu d'exception NullPointer4Exception ou NumberFormatException
        texte0.setText("" + icasier.numero());
      } catch (Exception ex) {
        // si il n'y a pas NullPointerException ou NumberFormatException
        // le texte du bief n'est pas mis � jour
        // Donc Rien � Faire !
      }
    } else if (("indice".equals(champ))
            && (source instanceof MetierBief)) {
      MetierBief ibief = (MetierBief) source;
      Hydraulique1dReseauBiefCourbe biefGraphique = rechercheBief(ibief);
      try {
        DjaText texte0 = biefGraphique.getTexts()[0];
        Integer.parseInt(texte0.getText().trim());
        // s'il n'y a pas eu d'exception NullPointer4Exception ou NumberFormatException
        texte0.setText("" + (ibief.indice() + 1));
      } catch (Exception ex) {
        // si il n'y a pas NullPointerException ou NumberFormatException
        // le texte du bief n'est pas mis � jour
        // Donc Rien � Faire !
      }
    } else if (("sensDebit".equals(champ)) && (source instanceof MetierCaracteristiqueLiaison)) {
      MetierCaracteristiqueLiaison icaracLiaison = (MetierCaracteristiqueLiaison) source;
      Hydraulique1dReseauLiaisonCasier liaisonGraphique = rechercheLiaison(icaracLiaison);
      if (liaisonGraphique == null) {
        return;
      }

      liaisonGraphique.maj();
    } else if (("caracteristiques".equals(champ)) && (source instanceof MetierLiaison)) {
      MetierLiaison iliaison = (MetierLiaison) source;
      Hydraulique1dReseauLiaisonCasier liaisonGraph = rechercheLiaison(iliaison);
      if (liaisonGraph == null) {
        return;
      }

      liaisonGraph.maj();
    }
  }

  public void initNumerosSingularites() {
    DjaGridInteractive grille = (DjaGridInteractive) grid_;
    Enumeration objEnum = grille.getObjects().
            enumerate(Hydraulique1dReseauSingularite.class);
    int numero = 1;
    while (objEnum.hasMoreElements()) {
      Hydraulique1dReseauSingularite s = (Hydraulique1dReseauSingularite) objEnum.
              nextElement();
      MetierSingularite is = (MetierSingularite) s.getData("singularite");
      is.numero(numero++);
    }
  }

  private Hydraulique1dReseauNoeud rechercheNoeud(MetierNoeud inoeud) {
    DjaGridInteractive grille = (DjaGridInteractive) grid_;
    Enumeration objEnum = grille.getObjects().
            enumerate(Hydraulique1dReseauNoeud.class);
    while (objEnum.hasMoreElements()) {
      Hydraulique1dReseauNoeud extremiteGraph
              = (Hydraulique1dReseauNoeud) objEnum.nextElement();
      MetierNoeud noeud = (MetierNoeud) extremiteGraph.getData("noeud");
      if (inoeud.numero() == noeud.numero()) {
        return extremiteGraph;
      }
    }
    return null;
  }

  private Hydraulique1dReseauExtremLibre rechercheExtremite(
          MetierExtremite iextremite) {
    DjaGridInteractive grille = (DjaGridInteractive) grid_;
    Enumeration objEnum = grille.getObjects().
            enumerate(Hydraulique1dReseauExtremLibre.class);
    while (objEnum.hasMoreElements()) {
      Hydraulique1dReseauExtremLibre extremiteGraph
              = (Hydraulique1dReseauExtremLibre) objEnum.nextElement();
      MetierExtremite extremite = (MetierExtremite) extremiteGraph.getData("extremite");
      if (iextremite.numero() == extremite.numero()) {
        return extremiteGraph;
      }
    }
    return null;
  }

  private Hydraulique1dReseauLiaisonCasier rechercheLiaison(
          MetierCaracteristiqueLiaison icaracLiai) {
    DjaGridInteractive grille = (DjaGridInteractive) grid_;
    Enumeration objEnum = grille.getObjects().
            enumerate(Hydraulique1dReseauLiaisonCasier.class);
    while (objEnum.hasMoreElements()) {
      Hydraulique1dReseauLiaisonCasier liaisonGraph
              = (Hydraulique1dReseauLiaisonCasier) objEnum.nextElement();
      MetierLiaison iliaison = (MetierLiaison) liaisonGraph.getData("liaison");
      if (icaracLiai.id() == iliaison.caracteristiques().id()) {
        return liaisonGraph;
      }
    }
    return null;
  }

  private Hydraulique1dReseauBiefCourbe rechercheBief(MetierBief iBief) {
    DjaGridInteractive grille = (DjaGridInteractive) grid_;
    Enumeration objEnum = grille.getObjects().
            enumerate(Hydraulique1dReseauBiefCourbe.class);
    while (objEnum.hasMoreElements()) {
      Hydraulique1dReseauBiefCourbe biefGraph
              = (Hydraulique1dReseauBiefCourbe) objEnum.nextElement();
      MetierBief bief = (MetierBief) biefGraph.getData("bief");
      if (bief == iBief) {
        return biefGraph;
      }
    }
    return null;
  }

  private Hydraulique1dReseauLiaisonCasier rechercheLiaison(MetierLiaison iLiaison) {
    DjaGridInteractive grille = (DjaGridInteractive) grid_;
    Enumeration objEnum = grille.getObjects().
            enumerate(Hydraulique1dReseauLiaisonCasier.class);
    while (objEnum.hasMoreElements()) {
      Hydraulique1dReseauLiaisonCasier liaisonGraph
              = (Hydraulique1dReseauLiaisonCasier) objEnum.nextElement();
      MetierLiaison liaison = (MetierLiaison) liaisonGraph.getData("liaison");
      if (liaison.enChaine().equals(iLiaison.enChaine())) {
        return liaisonGraph;
      }
    }
    return null;
  }

  private Hydraulique1dReseauCasier rechercheCasier(MetierCasier iCasier) {
    DjaGridInteractive grille = (DjaGridInteractive) grid_;
    Enumeration objEnum = grille.getObjects().
            enumerate(Hydraulique1dReseauCasier.class);
    while (objEnum.hasMoreElements()) {
      Hydraulique1dReseauCasier casierGraph
              = (Hydraulique1dReseauCasier) objEnum.nextElement();
      MetierCasier casier = (MetierCasier) casierGraph.getData("casier");
      try {

        if (casier != null && casier.numero() == iCasier.numero()) {
          return casierGraph;
        }
      } catch (NullPointerException e) {
        System.err.println("$$$ " + e);
        e.printStackTrace();
      }
    }
    return null;
  }

  private boolean casierAutoriser() {
    if (etude_.paramGeneraux().regime().value()
            == EnumMetierRegime._FLUVIAL_PERMANENT) {
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              Hydraulique1dResource.HYDRAULIQUE1D.getString("L'utilisation de casiers n'est pas possible") + "\n"
              + Hydraulique1dResource.HYDRAULIQUE1D.getString("avec un noyau de calcul fluvial permanent"))
              .activate();
      return false;
    }
    if (etude_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs() && CGlobal.AVEC_QUALITE_DEAU) {
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              Hydraulique1dResource.HYDRAULIQUE1D.getString("L'utilisation de casiers n'est pas compatible avec un modele") + "\n "
              + Hydraulique1dResource.HYDRAULIQUE1D.getString("de qualit� d'eau"))
              .activate();
      return false;
    }
    return true;
  }

  private boolean sourceAutoriser() {
    if (!etude_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs()) {
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              Hydraulique1dResource.HYDRAULIQUE1D.getString("L'utilisation de sources n'est possible que dans le cas d'une �tude") + "\n"
              + Hydraulique1dResource.HYDRAULIQUE1D.getString("de qualit� d'eau"))
              .activate();
      return false;
    }
    return true;
  }

  private void decalage(DjaLink djaLink, int indice) {
    int beginX = djaLink.getBeginX();
    int beginY = djaLink.getBeginY();
    int endX = djaLink.getEndX();
    int endY = djaLink.getEndY();
    int DX = (DjaOptions.deltaX * indice)
            % (getWidth() - beginX - 10);
    int DY = (DjaOptions.deltaY * indice)
            % (getHeight() - endY - 30);
    djaLink.setBeginX(beginX + DX);
    djaLink.setBeginY(beginY + DY);
    djaLink.setEndX(endX + DX);
    djaLink.setEndY(endY + DY);
  }

  private void decalage(DjaForm djaForm, int indice) {
    int X = djaForm.getX();
    int Y = djaForm.getY();
    int DX = (DjaOptions.deltaX * indice)
            % (getWidth() - X - 10);
    int DY = (DjaOptions.deltaY * indice)
            % (getHeight() - Y - 30);
    djaForm.setX(X + DX);
    djaForm.setY(Y + DY);
  }

  /**
   * Retourne une valeur representant l'orientation de l'extremit� de la fl�che.
   *
   * @param i type d'extremit� de la liaison.
   * @return un entier (0,1 ou 2) representant l'orientation de l'extremit� de la fl�che.
   * <BR>
   * 0 = pas de fleche significative <BR>
   * 1 = fleche vers l'interieur de la liaison <BR>
   * 2 = fleche vers l'exterieur de la liaison
   */
  private int typeExtremite2TypeSimplifie(int i) {
    if (i == 0) {
      return 0;
    }
    if ((i >= 1) && (i <= 3)) {
      return 1;
    }
    if ((i >= 4) && (i <= 6)) {
      return 2;
    }
    return 0;
  }

  /**
   * Retourne le sens de debit de la liaison orifice.
   *
   * @param liaison La liaison orifice graphique.
   * @return Le sens de debit de la liaison orifice.
   */
  private EnumMetierSensDebitLiaison getSensLiaison(Hydraulique1dReseauLiaisonCasier liaison) {
    int typeDebut = typeExtremite2TypeSimplifie(liaison.getBeginType());
    int typeFin = typeExtremite2TypeSimplifie(liaison.getEndType());

    MetierLiaison il = (MetierLiaison) liaison.getData("liaison");

    if (il.isCasierCasier()) {
      if ((typeDebut == 1) && (typeFin == 2)) {
        return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
      }
      if ((typeDebut == 1) && (typeFin == 0)) {
        return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
      }
      if ((typeDebut == 2) && (typeFin == 1)) {
        return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
      }
      if ((typeDebut == 2) && (typeFin == 0)) {
        return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
      }
      if ((typeDebut == 0) && (typeFin == 1)) {
        return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
      }
      if ((typeDebut == 0) && (typeFin == 2)) {
        return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
      }
      return EnumMetierSensDebitLiaison.DEUX_SENS;
    }

    if (il.isRiviereCasier()) {
      if (liaison.getBeginObject() instanceof Hydraulique1dReseauCasier) {
        if ((typeDebut == 2) && (typeFin == 1)) {
          return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
        }
        if ((typeDebut == 2) && (typeFin == 0)) {
          return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
        }
        if ((typeDebut == 1) && (typeFin == 2)) {
          return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
        }
        if ((typeDebut == 1) && (typeFin == 0)) {
          return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
        }
        if ((typeDebut == 0) && (typeFin == 2)) {
          return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
        }
        if ((typeDebut == 0) && (typeFin == 1)) {
          return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
        }
        return EnumMetierSensDebitLiaison.DEUX_SENS;
      }

      if (liaison.getBeginObject() instanceof Hydraulique1dReseauBiefCourbe) {
        if ((typeDebut == 2) && (typeFin == 1)) {
          return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
        }
        if ((typeDebut == 2) && (typeFin == 0)) {
          return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
        }
        if ((typeDebut == 1) && (typeFin == 2)) {
          return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
        }
        if ((typeDebut == 1) && (typeFin == 0)) {
          return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
        }
        if ((typeDebut == 0) && (typeFin == 2)) {
          return EnumMetierSensDebitLiaison.BIEF_VERS_CASIER_OU_VERS_AMONT;
        }
        if ((typeDebut == 0) && (typeFin == 1)) {
          return EnumMetierSensDebitLiaison.CASIER_VERS_BIEF_OU_VERS_AVAL;
        }
        return EnumMetierSensDebitLiaison.DEUX_SENS;
      }
    }

    return EnumMetierSensDebitLiaison.DEUX_SENS;
  }

  @Override
  public void dispose() {
    //-- remove palette if opened --//
    if (app_ != null && app_.getMainPanel() != null && app_.getMainPanel().getLeftColumn() != null) {
      removeExistingPalette(app_.getMainPanel().getLeftColumn());
    }

    super.dispose();
  }
}
