/*
 * @file         Hydraulique1dReseauGridAdapter.java
 * @creation     2001-25-10
 * @modification $Date: 2007-11-20 11:42:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
import java.util.Enumeration;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;

import com.memoire.dja.DjaGridAdapter;
import com.memoire.dja.DjaGridEvent;
import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaLink;
import com.memoire.dja.DjaObject;
/**
 * Ecouteur des �v�nements g�n�rer par la grille dela fen�tre du r�seau.<br>
 * Agit lors de la suppression d'un �l�ment du r�seau.<br>
 * Agit lors de la connexion d'une singularit� ou d'une liaison.<br>
 * @see com.memoire.dja.DjaGrid
 * @see com.memoire.dja.DjaGridInteractive
 * @version      $Revision: 1.14 $ $Date: 2007-11-20 11:42:40 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauGridAdapter extends DjaGridAdapter {
  private MetierReseau reseau_;
  public Hydraulique1dReseauGridAdapter(MetierReseau reseau) {
    reseau_= reseau;
  }
  @Override
  public void objectRemoved(DjaGridEvent evt) {
    if (evt.getObject() instanceof Hydraulique1dReseauBiefCourbe) {
      Hydraulique1dReseauBiefCourbe bief=
        (Hydraulique1dReseauBiefCourbe)evt.getObject();
      DjaObject debut= bief.getBeginObject();
      if (debut instanceof Hydraulique1dReseauExtremLibre) {
        if (evt.getSource() instanceof DjaGridInteractive) {
          ((DjaGridInteractive)evt.getSource()).remove(debut);
        }
      }
      DjaObject fin= bief.getEndObject();
      if (fin instanceof Hydraulique1dReseauExtremLibre) {
        if (evt.getSource() instanceof DjaGridInteractive) {
          ((DjaGridInteractive)evt.getSource()).remove(fin);
        }
      }
      MetierBief[] ibiefs= new MetierBief[1];
      ibiefs[0]= (MetierBief)bief.getData("bief");
      reseau_.supprimeBiefs(ibiefs);
    } else if (evt.getObject() instanceof Hydraulique1dReseauCasier) {
      Hydraulique1dReseauCasier casier=
        (Hydraulique1dReseauCasier)evt.getObject();
      MetierCasier[] icasiers= new MetierCasier[1];
      icasiers[0]= (MetierCasier)casier.getData("casier");
      reseau_.supprimeCasiers(icasiers);
    } else if (evt.getObject() instanceof Hydraulique1dReseauLiaisonCasier) {
      Hydraulique1dReseauLiaisonCasier liaison=
        (Hydraulique1dReseauLiaisonCasier)evt.getObject();
      MetierLiaison[] iliaisons= new MetierLiaison[1];
      iliaisons[0]= (MetierLiaison)liaison.getData("liaison");
      reseau_.supprimeLiaisons(iliaisons);
    } else if (evt.getObject() instanceof Hydraulique1dReseauExtremLibre) {
      Hydraulique1dReseauExtremLibre extremLibre =
        (Hydraulique1dReseauExtremLibre)evt.getObject();
      reseau_.supprimeExtremite((MetierExtremite)extremLibre.getData("extremite"));
    } else if (evt.getObject() instanceof Hydraulique1dReseauSingularite) {
      Hydraulique1dReseauSingularite sing =
        (Hydraulique1dReseauSingularite)evt.getObject();
      MetierSingularite[] isingularites= new MetierSingularite[1];
      isingularites[0]=(MetierSingularite)sing.getData("singularite");
      reseau_.supprimeSingularites(isingularites);
    } else if (evt.getObject() instanceof Hydraulique1dReseauNoeud) {
      Hydraulique1dReseauNoeud noeud =
          (Hydraulique1dReseauNoeud) evt.getObject();
      MetierNoeud inoeud = (MetierNoeud) noeud.getData("noeud");
      reseau_.supprimeNoeud(inoeud);
    }
  }
  @Override
  public void objectConnected(DjaGridEvent evt) {
    if (  (evt.getObject() instanceof Hydraulique1dReseauSingularite)
       || (evt.getObject() instanceof Hydraulique1dReseauLiaisonCasier) )  {
      DjaLink lien=(DjaLink)evt.getObject();
      Hydraulique1dReseauBiefCourbe leBief= null;
      if (lien.getBeginObject() instanceof Hydraulique1dReseauBiefCourbe) {
        leBief=(Hydraulique1dReseauBiefCourbe)lien.getBeginObject();
      } else if (lien.getEndObject() instanceof Hydraulique1dReseauBiefCourbe) {
        leBief=(Hydraulique1dReseauBiefCourbe)lien.getEndObject();
      }
      if (leBief == null) return;
      Enumeration eleEnum= evt.getGrid().getObjects().elements();
      int nbLienSurLeBief= 0;
      while (eleEnum.hasMoreElements()) {
        Object o= eleEnum.nextElement();
        if (  (o instanceof Hydraulique1dReseauSingularite)
           || (o instanceof Hydraulique1dReseauLiaisonCasier) ) {
          DjaLink l= (DjaLink)o;
          if ((l.getBeginObject() == leBief)||(l.getEndObject() == leBief)) {
            nbLienSurLeBief++;
          }
        }
      }
      if (nbLienSurLeBief == leBief.nbAnchors) {
        leBief.setNbAnchors(nbLienSurLeBief + 1);
        leBief.paintAnchors(evt.getGrid().getGraphics());
      }
    }
    reseau_.initIndiceNumero();
  }
  @Override
  public void objectDisconnected(DjaGridEvent evt) {
    reseau_.initIndiceNumero();
  }
  @Override
  public void objectModified(DjaGridEvent _evt) {
    System.out.println("objectModified(DjaGridEvent _evt)="+_evt);
  }
  @Override
  public void objectSelected(DjaGridEvent _evt) {
    System.err.println("objectSelected EVT: "+_evt);
  }
  @Override
  public void objectUnselected(DjaGridEvent _evt) {
    System.err.println("objectUnselected EVT: "+_evt);
  }

}
