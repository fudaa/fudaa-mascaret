/*
 * @file         Hydraulique1dReseauLiaisonCasier.java
 * @creation     2003-06-06
 * @modification $Date: 2007-11-20 11:42:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2003 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
import java.awt.Color;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierSensDebitLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet;

import com.memoire.dja.DjaAnchor;
import com.memoire.dja.DjaBrokenArrow;
/**
 * Composant graphique du r�seau hydraulique repr�sentant une liaison avec un casier.
 * @see MetierLiaison
 * @version      $Revision: 1.7 $ $Date: 2007-11-20 11:42:40 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauLiaisonCasier
  extends DjaBrokenArrow
  implements Hydraulique1dReseauElementInterf {
  Hydraulique1dReseauLiaisonCasier(MetierLiaison iliaison) {
    super();
    super.setForeground(Color.blue);
    super.setEndType(0); // pas de fleche � l'extr�mit� de fin
    if (iliaison != null) {
      putData("liaison", iliaison);
      maj();
    }
  }
  Hydraulique1dReseauLiaisonCasier() {
    this(null);
  }
  @Override
  public DjaAnchor[] getAnchors() {
    DjaAnchor[] r= new DjaAnchor[0];
    return r;
  }
  @Override
  public String[] getInfos() {
    MetierHydraulique1d iobjet= (MetierHydraulique1d)getData("liaison");
    return iobjet.getInfos();
  }
  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauLiaisonCasier r=(Hydraulique1dReseauLiaisonCasier)super.clone();
    MetierReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
    MetierLiaison liaison= reseau.ajouterLiaison();
    liaison.initialise((MetierLiaison)getData("liaison"));
    r.putData("liaison", liaison);
    return r;
  }
  
  /**
   * Mise a jour du composant graphique
   */
  public void maj() {
    MetierLiaison liaison=(MetierLiaison)getData("liaison");

    // Sens du debit pour les liaisons orifice.
    
    if (liaison.isOrifice()) {
      int sensDebit=liaison.caracteristiques().getSensDebit().value();
      if (liaison.isCasierCasier()) {
        switch (sensDebit) {
        case EnumMetierSensDebitLiaison._DEUX_SENS:
          setBeginType(0);
          setEndType(0);
          break;
        case EnumMetierSensDebitLiaison._BIEF_VERS_CASIER_OU_VERS_AMONT:
          setBeginType(5);
          setEndType(0);
          break;
        case EnumMetierSensDebitLiaison._CASIER_VERS_BIEF_OU_VERS_AVAL:
          setBeginType(0);
          setEndType(5);
          break;
        }
      }
      else {
        boolean biefAmont=false;
        if (getBeginObject() instanceof Hydraulique1dReseauBiefCourbe)
          biefAmont=true;
        switch (sensDebit) {
        case EnumMetierSensDebitLiaison._DEUX_SENS:
          setBeginType(0);
          setEndType(0);
          break;
        case EnumMetierSensDebitLiaison._BIEF_VERS_CASIER_OU_VERS_AMONT:
          if (!biefAmont) {
            setBeginType(5);
            setEndType(0);
          }
          else {
            setBeginType(0);
            setEndType(5);
          }
          break;
        case EnumMetierSensDebitLiaison._CASIER_VERS_BIEF_OU_VERS_AVAL:
          if (biefAmont) {
            setBeginType(5);
            setEndType(0);
          }
          else {
            setBeginType(0);
            setEndType(5);
          }
          break;
        }
      }
    }
    
    // La couleur
    
    if (liaison.isChenal()) {
      setBeginType(0);
      setEndType(0);
      setForeground(Color.green);
    }
    else if (liaison.isOrifice()) {
      setForeground(Color.orange);
    }
    else if (liaison.isSeuil()) {
      setBeginType(0);
      setEndType(0);
      setForeground(Color.blue);
    }
    else if (liaison.isSiphon()) {
      setBeginType(0);
      setEndType(0);
      setForeground(Color.red);
    }
  }
}
