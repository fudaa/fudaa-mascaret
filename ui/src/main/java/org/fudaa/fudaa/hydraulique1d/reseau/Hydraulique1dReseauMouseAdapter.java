/*
 * @file         Hydraulique1dReseauMouseAdapter.java
 * @creation     2000-09-07
 * @modification $Date: 2007-11-20 11:42:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierDeversoir;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTranscritique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilVanne;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSource;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dCustomizer;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dNoeudFluvEditor;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dNoeudTransEditor;
import org.fudaa.fudaa.hydraulique1d.editor.Hydraulique1dPerteChargeEditor;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Apport2;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Bief;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Casier;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_CasierLiaison;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Deversoir;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_ExtremiteLibre;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_SeuilAvecLoi;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_SeuilLoi;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_SeuilVanne;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Source;

import com.memoire.bu.BuDialog;
import com.memoire.bu.BuDialogMessage;
import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaVector;

/**
 * Ecouteur des �v�nements souris de la fen�tre du r�seau.<br>
 * Permet l'affichage de l'�diteur du composant graphique cliquer.<br>
 * Permet l'affichage des informations du composant graphique survol�.<br>
 *
 * @version $Revision: 1.17 $ $Date: 2007-11-20 11:42:41 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dReseauMouseAdapter
        extends MouseAdapter
        implements MouseMotionListener {

  private MascaretGridInteractive grille;
  private Hydraulique1dReseauFrame fenetre;
  private MetierEtude1d etude;

  public Hydraulique1dReseauMouseAdapter(Hydraulique1dReseauFrame fenetre_, MetierEtude1d etude) {
    fenetre = fenetre_;
    this.etude = etude;
    grille = (MascaretGridInteractive) fenetre.getGrid();
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    if (grille.isInteractive() || grille.isZoomOrMoveActivated()) {
      return;
    }
    final int x = grille.getTransform().getScaledX(e);
    final int y = grille.getTransform().getScaledY(e);
    java.util.Enumeration objEnum = grille.getObjects().elements();
    while (objEnum.hasMoreElements()) {
      Object o = objEnum.nextElement();

      if (o instanceof Hydraulique1dReseauSeuil) {
        Hydraulique1dReseauSeuil seuil = (Hydraulique1dReseauSeuil) o;
        if (seuil.contains(x, y)) {
// Unused          Hydraulique1dCustomizer editeur;
          if (seuil.getData("singularite") instanceof MetierSeuilTranscritique) {
            Hydraulique1dIHM_SeuilLoi ihmSeuilTrans
                    = Hydraulique1dIHMRepository.getInstance().SEUIL_LOI();
            ihmSeuilTrans.setBief(
                    (MetierBief) seuil.getBeginObject().getData("bief"));
            ihmSeuilTrans.setSeuil(
                    (MetierSeuilLoi) seuil.getData("singularite"));
            ihmSeuilTrans.editer();
            return;
          } else if (seuil.getData("singularite") instanceof MetierSeuilLoi) {
            Hydraulique1dIHM_SeuilLoi ihmSeuilLoi
                    = Hydraulique1dIHMRepository.getInstance().SEUIL_LOI();
            ihmSeuilLoi.setBief(
                    (MetierBief) seuil.getBeginObject().getData("bief"));
            ihmSeuilLoi.setSeuil(
                    (MetierSeuilLoi) seuil.getData("singularite"));
            ihmSeuilLoi.editer();

            return;
          } else if (seuil.getData("singularite") instanceof MetierSeuilVanne) {
            Hydraulique1dIHM_SeuilVanne ihmSeuilVanne
                    = Hydraulique1dIHMRepository.getInstance().SEUIL_VANNE();
            ihmSeuilVanne.setBief(
                    (MetierBief) seuil.getBeginObject().getData("bief"));
            ihmSeuilVanne.setSeuil((MetierSeuil) seuil.getData("singularite"));
            ihmSeuilVanne.editer();
            return;
          } else {
            Hydraulique1dIHM_SeuilAvecLoi ihmSeuilAvecLoi
                    = Hydraulique1dIHMRepository.getInstance().SEUIL_AVEC_LOI();
            ihmSeuilAvecLoi.setBief(
                    (MetierBief) seuil.getBeginObject().getData("bief"));
            ihmSeuilAvecLoi.setSeuil((MetierSeuil) seuil.getData("singularite"));
            ihmSeuilAvecLoi.editer();
            return;
          }
        }
      } else if (o instanceof Hydraulique1dReseauDeversoir) {
        Hydraulique1dReseauDeversoir dev = (Hydraulique1dReseauDeversoir) o;
        if (dev.contains(x, y)) {
          Hydraulique1dIHM_Deversoir ihmDeversoir
                  = Hydraulique1dIHMRepository.getInstance().DEVERSOIR();
          ihmDeversoir.setBiefParent(
                  (MetierBief) dev.getBeginObject().getData("bief"));
          ihmDeversoir.setDeversoir((MetierDeversoir) dev.getData("singularite"));
          ihmDeversoir.setDeversoirGraphique(dev);
          ihmDeversoir.editer();
          return;
        }
      } else if (o instanceof Hydraulique1dReseauPerteCharge) {
        Hydraulique1dReseauPerteCharge perte = (Hydraulique1dReseauPerteCharge) o;
        if (perte.contains(x, y)) {
          Hydraulique1dPerteChargeEditor editeur
                  = new Hydraulique1dPerteChargeEditor();
          MetierBief biefParent = (MetierBief) perte.getBeginObject().getData("bief");
          editeur.setBiefParent(biefParent);
          editeur.setObject((MetierPerteCharge) perte.getData("singularite"));
          afficheEditor(editeur);
          return;
        }
      } else if (o instanceof Hydraulique1dReseauApport) {
        Hydraulique1dReseauApport app = (Hydraulique1dReseauApport) o;
        if (app.contains(x, y)) {
          Hydraulique1dIHM_Apport2 ihmApport
                  = Hydraulique1dIHMRepository.getInstance().APPORT2();
          ihmApport.setBief((MetierBief) app.getBeginObject().getData("bief"));
          ihmApport.setApport((MetierApport) app.getData("singularite"));
          ihmApport.editer();
          return;
        }
      } else if (o instanceof Hydraulique1dReseauSource) {
        Hydraulique1dReseauSource app = (Hydraulique1dReseauSource) o;
        if (app.contains(x, y)) {
          Hydraulique1dIHM_Source ihmSource
                  = Hydraulique1dIHMRepository.getInstance().SOURCE();
          ihmSource.setBief((MetierBief) app.getBeginObject().getData("bief"));
          ihmSource.setSource((MetierSource) app.getData("singularite"));
          ihmSource.editer();
          return;
        }
      } else if (o instanceof Hydraulique1dReseauExtremLibre) {
        Hydraulique1dReseauExtremLibre extr = (Hydraulique1dReseauExtremLibre) o;
        if (extr.contains(x, y)) {
          Hydraulique1dIHM_ExtremiteLibre ihmExtremite
                  = Hydraulique1dIHMRepository.getInstance().EXTREMITE_LIBRE();
          ihmExtremite.setExtremite((MetierExtremite) extr.getData("extremite"));
          ihmExtremite.editer();
          return;
        }
      } else if (o instanceof Hydraulique1dReseauNoeud) {
        Hydraulique1dReseauNoeud n = (Hydraulique1dReseauNoeud) o;
        if (n.contains(x, y)) {
          if (fenetre.etude_.paramGeneraux().regime().value()
                  == EnumMetierRegime._TRANSCRITIQUE) {
            dialogueNoeudTrans((MetierNoeud) n.getData("noeud"));
          } else {
            dialogueNoeud((MetierNoeud) n.getData("noeud"));
          }
          return;
        }
      } else if (o instanceof Hydraulique1dReseauLiaisonCasier) {
        Hydraulique1dReseauLiaisonCasier l = (Hydraulique1dReseauLiaisonCasier) o;
        if (l.contains(x, y)) {
          Hydraulique1dIHM_CasierLiaison ihmLiaison
                  = Hydraulique1dIHMRepository.getInstance().CASIER_LIAISON();
          MetierLiaison iliaison = (MetierLiaison) l.getData("liaison");
          ihmLiaison.setLiaison(iliaison);
          ihmLiaison.editer();
          return;
        }
      } else if (o instanceof Hydraulique1dReseauCasier) {
        Hydraulique1dReseauCasier c = (Hydraulique1dReseauCasier) o;
        if (c.contains(x, y)) {
          Hydraulique1dIHM_Casier ihmCasier
                  = Hydraulique1dIHMRepository.getInstance().CASIER();
          MetierCasier icasier = (MetierCasier) c.getData("casier");
          ihmCasier.setCasier(icasier);
          ihmCasier.editer();
          return;
        }
      } else if (o instanceof Hydraulique1dReseauBiefCourbe) {
        Hydraulique1dReseauBiefCourbe b = (Hydraulique1dReseauBiefCourbe) o;
        if (b.contains(x, y)) {
          Hydraulique1dIHM_Bief ihmBief
                  = Hydraulique1dIHMRepository.getInstance().BIEF();
          ihmBief.setBief((MetierBief) b.getData("bief"));
          ihmBief.editer();
          ihmBief.setEtude(etude);
          return;
        }
      }
    } // fin while
  }

  @Override
  public void mouseDragged(MouseEvent e) {
  }

  @Override
  public void mouseMoved(MouseEvent e) {
    try {
      if (!fenetre.isSelected()) {
        // System.out.println("non selectionne");
        return;
      }
      int x = grille.getTransform().getScaledX(e);
      int y = grille.getTransform().getScaledY(e);
      DjaVector vec = grille.getObjects();
      for (int i = 0; i < vec.size(); i++) {
        DjaObject o = (DjaObject) vec.elementAt(i);
        if (o.contains(x, y)) {
          if (o instanceof Hydraulique1dReseauElementInterf) {
            Hydraulique1dReseauElementInterf b
                    = (Hydraulique1dReseauElementInterf) o;
            String[] infos = b.getInfos();
            fenetre.setStatus(infos[0], infos[1]);
          }
          return;
        }
        fenetre.setStatus("", "");
      }
    } catch (Exception exp) {
      exp.printStackTrace();
    };
  }

  void dialogueNoeudTrans(MetierNoeud n) {
    BuDialog dial;
    if (n.extremites().length != 3) {
      dial
              = new BuDialogMessage(
                      fenetre.getApp(),
                      null,
                      "Avec le noyau transcritique,\n le nombre de liaison avec les biefs doit �tre �gale � 3");
      afficheDialog(dial);
    } else {
      Hydraulique1dNoeudTransEditor editeur
              = new Hydraulique1dNoeudTransEditor();
      editeur.setObject(n);
      afficheEditor(editeur);
    }
  }

  void dialogueNoeud(MetierNoeud n) {
    BuDialog dial;
    Hydraulique1dCustomizer d;
    if ((2 > n.extremites().length) || (n.extremites().length > 5)) {
      dial
              = new BuDialogMessage(
                      fenetre.getApp(),
                      null,
                      "Avec le noyau fluvial,\n le nombre de liaison avec les biefs doit �tre compris entre 2 et 5");
      afficheDialog(dial);
    } else {
      d = new Hydraulique1dNoeudFluvEditor();
      d.setObject(fenetre.etude_.paramGeneraux());
      d.setObject(n);
      afficheEditor(d);
    }
  }

  private void afficheDialog(BuDialog dial) {
    dial.pack();
    dial.setLocationRelativeTo(grille);
    dial.setModal(true);
    dial.show();
  }

  private void afficheEditor(BDialogContent dial) {
    dial.pack();
    dial.setModal(true);
    dial.show();
  }
}
