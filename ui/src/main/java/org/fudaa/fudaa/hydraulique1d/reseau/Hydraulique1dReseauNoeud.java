/*
 * @file         Hydraulique1dReseauNoeud.java
 * @creation     2000-09-05
 * @modification $Date: 2007-11-20 11:42:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.Color;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet;

import com.memoire.dja.DjaCircle;
import org.fudaa.dodico.boony.BoonyDeserializationAware;

/**
 * Composant graphique du réseau hydraulique représentant un confluent (noeud).
 *
 * @see MetierNoeud
 * @version $Revision: 1.9 $ $Date: 2007-11-20 11:42:41 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dReseauNoeud
        extends DjaCircle
        implements Hydraulique1dReseauElementInterf, BoonyDeserializationAware {

  Hydraulique1dReseauNoeud(MetierNoeud noeud) {
    super();
    if (noeud != null) {
      super.addText(Integer.toString(noeud.numero()));
    }
    super.setWidth(20);
    endDeserialization();
    if (noeud != null) {
      putData("noeud", noeud);
    }
  }

  @Override
  public final void endDeserialization() {
    super.setBackground(new Color(170, 252, 255));
  }

  Hydraulique1dReseauNoeud() {
    this(null);
  }

  @Override
  public String[] getInfos() {
    MetierHydraulique1d iobjet = (MetierHydraulique1d) getData("noeud");
    return iobjet.getInfos();
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauNoeud r = (Hydraulique1dReseauNoeud) super.clone();
    MetierReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
    MetierNoeud noeud = reseau.creeNoeud();
    noeud.initialise((MetierNoeud) getData("noeud"));
    r.putData("noeud", noeud);
    return r;
  }
}
