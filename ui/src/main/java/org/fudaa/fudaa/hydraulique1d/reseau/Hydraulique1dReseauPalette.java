/*
 * @file         Hydraulique1dReseauPalette.java
 * @creation     2000-11-14
 * @modification $Date: 2007-11-20 11:42:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.border.EmptyBorder;

import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.jdesktop.swingx.VerticalLayout;

/**
 * Palette du r�seau hydraulique 1D compos�s de boutons pour la cr�ation de bief, confluent, apport, etc. Utilis� par la fen�tre du reseau
 * Hydraulique1dReseauFrame.
 *
 * @version $Revision: 1.12 $ $Date: 2007-11-20 11:42:40 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public class Hydraulique1dReseauPalette extends BuPanel {
  
  JButton[] buttons_;
  
  String[] namesAvecQE
          = {
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Bief"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Confluent"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("D�bit d'apport"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Perte de charge locale"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Barrage/Seuil"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("D�versoir lat�ral"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Casier"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Liaison casier"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Source de traceur")};
  
  String[] namesSansQE
          = {
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Bief"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Confluent"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("D�bit d'apport"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Perte de charge locale"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Barrage/Seuil"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("D�versoir lat�ral"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Casier"),
            Hydraulique1dResource.HYDRAULIQUE1D.getString("Liaison casier")};
  
  String[] names = namesSansQE;

  //  String[] icons={ "bief", "confluent", "apport", "pertecharge",
  //		   "seuil", "deversoir", "barrageprincipal"};
  String[] iconsAvecQE
          = {
            "bief",
            "confluent",
            "apport",
            "pertecharge",
            "seuil",
            "deversoir",
            "-casier",
            "-liaisoncasier",
            "Source"};
  
  String[] iconsSansQE
          = {
            "bief",
            "confluent",
            "apport",
            "pertecharge",
            "seuil",
            "deversoir",
            "-casier",
            "-liaisoncasier"};
  
  String[] icons = iconsSansQE;
  
  public Hydraulique1dReseauPalette(ActionListener _al) {
    super();
    if (CGlobal.AVEC_QUALITE_DEAU) {
      names = namesAvecQE;
      icons = iconsAvecQE;
    }
    //layout_= new BuGridLayout(3, 2, 2);
//    GridLayout layout = new GridLayout(9, 1);
//    layout.s
    //layout_.setCfilled(false);
    BuGridLayout layout = new BuGridLayout(1, 0, 2);
    layout.setHfilled(true);
    layout.setFlowMode(true);
    setLayout(layout);
    setBorder(new EmptyBorder(2, 2, 2, 2));
    buttons_ = new JButton[names.length];
    for (int i = 0; i < buttons_.length; i++) {
      BuIcon icon
              = Hydraulique1dResource.HYDRAULIQUE1D.getIcon("hydraulique1d" + icons[i]);
      buttons_[i] = new JButton();
      buttons_[i].setText(names[i]);
      buttons_[i].setHorizontalAlignment(SwingConstants.LEFT);
      buttons_[i].setIcon(icon);
      buttons_[i].setMargin(new Insets(1, 1, 1, 1));
      buttons_[i].setRequestFocusEnabled(false);
      buttons_[i].setToolTipText(names[i]);
      buttons_[i].setActionCommand(
              "HYDRAULIQUE1D_CREATE_RESEAU(" + names[i] + ")");
      //-- aha - 2.3.3 display buttons + text --//
//    BuPanel  panel = new BuPanel(new FlowLayout(FlowLayout.LEFT));
//      panel.add(buttons_[i]);
      final BuLabel label = new BuLabel(names[i]);
      label.setFont(CtuluLibSwing.getMiniFont());
//      panel.add(label);
      add(buttons_[i]);
      buttons_[i].addActionListener(_al);
    }
    
  }
}
