/*
 * @file         Hydraulique1dReseauPaletteBracket.java
 * @creation     2005-02-01
 * @modification $Date: 2006-09-12 08:36:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2005 EDF/OPP
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.dja.DjaLink;
import com.memoire.dja.DjaOptions;
import com.memoire.dja.DjaVector;

/**
 * Palette des extr�mit�s des liens (DjaLink).
 * Utilis� par la fen�tre du reseau Hydraulique1dReseauFrame.
 * @see Hydraulique1dReseauFrame
 * @see DjaLink
 * @version      $Revision: 1.3 $ $Date: 2006-09-12 08:36:40 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauPaletteBracket extends BuPanel
       implements DjaOptions, ListSelectionListener, ActionListener {

    final static int NB_TYPE=7;
    JComboBox cbt,cet;
    JList     lbt,let;
    ActionListener al_;

    class XCR
          implements ListCellRenderer
    {
      ListCellRenderer r_;
      int              o_;

      public XCR(ListCellRenderer _r, int _o)
      {
        r_=_r;
        o_=_o;
      }

    @Override
      public Component getListCellRendererComponent(
           JList list,
           Object value,
           int index,
           boolean isSelected,
           boolean cellHasFocus)
      {
        Component r=r_.getListCellRendererComponent
          (list,value,index,isSelected,cellHasFocus);

        if(value instanceof Integer)
        {
          int i=((Integer)value).intValue();
          ((JLabel)r).setIcon(new BracketIcon(i,o_,null));
          ((JLabel)r).setText(null); // ""+i);
        }

        return r;
      }
    }

    class XLM implements ListModel
    {
      DjaVector l=new DjaVector(2);
    @Override
      public void addListDataListener(ListDataListener _l) { l.addElement(_l); }
    @Override
      public void removeListDataListener(ListDataListener _l) { l.removeElement(_l); }
    @Override
      public Object getElementAt(int index) { return new Integer(index); }
    @Override
      public int    getSize() { return NB_TYPE; }
    }

    public Hydraulique1dReseauPaletteBracket(ActionListener _al)
    {
      super();

      al_=_al;

      BuGridLayout layout_=new BuGridLayout(2,2,2);
      layout_.setCfilled(false);
      setLayout(layout_);
      // setLayout(new BuHorizontalLayout(2,false,true));
      setBorder(new EmptyBorder(2,2,2,2));

      ListModel mt=new XLM();

      lbt=new JList();
      lbt.setModel(mt);
      lbt.setCellRenderer(new XCR(lbt.getCellRenderer(),EAST));
      lbt.setRequestFocusEnabled(false);
      lbt.setSelectedIndex(0);
      lbt.addListSelectionListener(this);

      JScrollPane sbt=new JScrollPane(lbt);
      sbt.setPreferredSize(new Dimension(0,150));

      let=new JList();
      let.setModel(mt);
      let.setCellRenderer(new XCR(let.getCellRenderer(),WEST));
      let.setRequestFocusEnabled(false);
      let.setSelectedIndex(5);
      let.addListSelectionListener(this);

      JScrollPane set=new JScrollPane(let);
      set.setPreferredSize(new Dimension(0,150));

      cbt=new JComboBox();
      for(int i=0; i<NB_TYPE; i++)
        cbt.addItem(new Integer(i));
      cbt.setRenderer(new XCR(cbt.getRenderer(),EAST));
      cbt.setRequestFocusEnabled(false);
      cbt.setMaximumRowCount(5);
      cbt.addActionListener(this);

      cet=new JComboBox();
      for(int i=0; i<NB_TYPE; i++)
        cet.addItem(new Integer(i));
      cet.setRenderer(new XCR(cet.getRenderer(),WEST));
      cet.setRequestFocusEnabled(false);
      cet.setMaximumRowCount(5);
      cet.addActionListener(this);

      BuPanel begint=new BuPanel();
      begint.setLayout(new BuVerticalLayout(2,true,false));

      for(int i=0; i<NB_TYPE; i++)
      {
        Icon     icon  =new BracketIcon(i,EAST,lbt);
        BuButton button=new BuButton();

        button.setIcon(icon);
        button.setRolloverIcon(icon);
        button.setMargin(new Insets(1,1,1,1));
        button.setRequestFocusEnabled(false);
        button.setToolTipText(""+i);
        button.setActionCommand("DJA_BEGIN_TYPE("+i+")");

        begint.add(button);
        button.addActionListener(_al);
      }

      BuPanel endt=new BuPanel();
      endt.setLayout(new BuVerticalLayout(2,true,false));

      for(int i=0; i<NB_TYPE; i++)
      {
        Icon     icon  =new BracketIcon(i,WEST,let);
        BuButton button=new BuButton();

        button.setIcon(icon);
        button.setRolloverIcon(icon);
        button.setMargin(new Insets(1,1,1,1));
        button.setRequestFocusEnabled(false);
        button.setToolTipText(""+i);
        button.setActionCommand("DJA_END_TYPE("+i+")");

        endt.add(button);
        button.addActionListener(_al);
      }

      BuPanel begino=new BuPanel();
      begino.setLayout(new BuGridLayout(2,2,2));

      for(int i=0; i<4; i++)
      {
        Icon     icon  =new BracketIcon(6,i,lbt);
        BuButton button=new BuButton();

        button.setIcon(icon);
        button.setRolloverIcon(icon);
        button.setMargin(new Insets(1,1,1,1));
        button.setRequestFocusEnabled(false);
        button.setToolTipText(""+i);
        button.setActionCommand("DJA_BEGIN_ORIENTATION("+i+")");

        begino.add(button);
        button.addActionListener(_al);
      }

      BuPanel endo=new BuPanel();
      endo.setLayout(new BuGridLayout(2,2,2));

      for(int i=0; i<4; i++)
      {
        Icon     icon  =new BracketIcon(6,i,let);
        BuButton button=new BuButton();

        button.setIcon(icon);
        button.setRolloverIcon(icon);
        button.setMargin(new Insets(1,1,1,1));
        button.setRequestFocusEnabled(false);
        button.setToolTipText(""+i);
        button.setActionCommand("DJA_END_ORIENTATION("+i+")");

        endo.add(button);
        button.addActionListener(_al);
      }

      add(sbt);
      add(set);
      // add(cbt);
      // add(cet);
      // add(begint);
      // add(endt);
      add(begino);
      add(endo);
    }

  @Override
    public void valueChanged(ListSelectionEvent _evt)
    {
      if(_evt.getValueIsAdjusting()) return;

      Object source=_evt.getSource();

      if(source==lbt)
        al_.actionPerformed(new ActionEvent
                            (this,ActionEvent.ACTION_PERFORMED,
                             "DJA_BEGIN_TYPE("+lbt.getSelectedIndex()+")"));
      if(source==let)
        al_.actionPerformed(new ActionEvent
                            (this,ActionEvent.ACTION_PERFORMED,
                             "DJA_END_TYPE("+let.getSelectedIndex()+")"));

      repaint();
    }

  @Override
    public void actionPerformed(ActionEvent _evt)
    {
      Object source=_evt.getSource();
      System.out.println("PALBRK:"+_evt);

      if(source==cbt)
        al_.actionPerformed(new ActionEvent
                            (this,ActionEvent.ACTION_PERFORMED,
                             "DJA_BEGIN_TYPE("+cbt.getSelectedIndex()+")"));
      if(source==cet)
        al_.actionPerformed(new ActionEvent
                            (this,ActionEvent.ACTION_PERFORMED,
                             "DJA_END_TYPE("+cet.getSelectedIndex()+")"));
    }

    private class BracketIcon implements Icon
    {
      private int   t_;
      private int   o_;
      private JList l_;

      public BracketIcon(int _t, int _o, JList _l)
      {
        t_=_t;
        o_=_o;
        l_=_l;
      }

    @Override
      public int getIconWidth () { return 16; }
    @Override
      public int getIconHeight() { return 16; }

    @Override
      public void paintIcon(Component _c, Graphics _g, int x, int y)
      {
        x+=2;
        y+=2;

        int t=t_;

        if(l_!=null)
          t=l_.getSelectedIndex();

        Color fg=Color.black;
        Color bg=new Color(224,224,255);

        switch(o_)
        {
        case EAST:
          DjaLink.drawBracket(_g,t,o_,x,y,x+10,y+10,
                              fg,bg,1);
          break;
        case WEST:
          DjaLink.drawBracket(_g,t,o_,x+10,y,x,y+10,
                              fg,bg,1);
          break;
        case NORTH:
          DjaLink.drawBracket(_g,t,o_,x,y+10,x+10,y,
                              fg,bg,1);
          break;
        case SOUTH:
          DjaLink.drawBracket(_g,t,o_,x,y,x+10,y+10,
                              fg,bg,1);
          break;
        }

        if(l_!=null)
        {
          _g.setColor(Color.red);
          switch(o_)
          {
          case EAST : _g.fillRect(x-1   ,y-1+5 ,3,3); break;
          case WEST : _g.fillRect(x-1+10,y-1+5 ,3,3); break;
          case NORTH: _g.fillRect(x-1+5 ,y-1+10,3,3); break;
          case SOUTH: _g.fillRect(x-1+5 ,y-1   ,3,3); break;
          }
        }
      }
    }
  }
