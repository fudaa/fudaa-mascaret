/*
 * @file         Hydraulique1dReseauPerteCharge.java
 * @creation     2000-11-16
 * @modification $Date: 2007-11-20 11:42:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierPerteCharge;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuIcon;
/**
 * Composant graphique du r�seau hydraulique repr�sentant une perte de charge singuli�re (MetierPerteCharge).
 * @see MetierPerteCharge
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:42:40 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauPerteCharge
  extends Hydraulique1dReseauSingularite {
  private final static BuIcon ICON = Hydraulique1dResource.HYDRAULIQUE1D.getIcon("reseau/reseau_pertecharge.png");
  public Hydraulique1dReseauPerteCharge(MetierPerteCharge perteCharge) {
    super(null);
    if (perteCharge != null)
      putData("singularite", perteCharge);
  }
  public Hydraulique1dReseauPerteCharge() {
    this(null);
  }
  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauPerteCharge r=(Hydraulique1dReseauPerteCharge)super.clone();
    MetierReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
    MetierPerteCharge perte= reseau.creePerteCharge();
    perte.initialise((MetierPerteCharge)getData("singularite"));
    r.putData("singularite", perte);
    return r;
  }
  @Override
  BuIcon getBuIcon()  {
    return ICON;
  }

}
