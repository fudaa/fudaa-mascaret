/*
 * @file         Hydraulique1dReseauSeuil.java
 * @creation     2000-11-16
 * @modification $Date: 2007-11-20 11:42:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuil;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuIcon;
/**
 * Composant graphique du réseau hydraulique représentant un seuil (MetierSeuil).
 * @see MetierSeuil
 * @version      $Revision: 1.8 $ $Date: 2007-11-20 11:42:40 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dReseauSeuil extends Hydraulique1dReseauSingularite {
  private final static BuIcon ICON = Hydraulique1dResource.HYDRAULIQUE1D.getIcon("reseau/reseau_seuil.png");
  public Hydraulique1dReseauSeuil(MetierSeuil seuil) {
    super(null);
//    super.setBackground(Color.green);
//    super.setColor(Color.green);
    if (seuil != null)
      putData("singularite", seuil);
  }
  public Hydraulique1dReseauSeuil() {
    this(null);
  }
  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauSeuil r=(Hydraulique1dReseauSeuil)super.clone();
//    DReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
    MetierSeuil seuil= (MetierSeuil)((MetierSeuil)getData("singularite")).creeClone();
//    seuil.initialise((MetierSeuil)getData("singularite"));
    r.putData("singularite", seuil);
    return r;
  }
  @Override
  BuIcon getBuIcon()  {
    return ICON;
  }

}
