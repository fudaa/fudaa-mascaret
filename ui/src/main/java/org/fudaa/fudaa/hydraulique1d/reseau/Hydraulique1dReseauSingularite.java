/*
 * @file         Hydraulique1dReseauSingularite.java
 * @creation     2000-11-16
 * @modification $Date: 2007-11-20 11:42:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.Color;
import java.awt.Graphics;

import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;

import com.memoire.bu.BuIcon;
import com.memoire.dja.DjaAttach;
import com.memoire.dja.DjaGraphics;
import com.memoire.dja.DjaLink;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluLibImage;

/**
 * Composant graphique abstrait du réseau hydraulique représentant les singularités.
 *
 * @version $Revision: 1.12 $ $Date: 2007-11-20 11:42:42 $ by $Author: bmarchan $
 * @author Jean-Marc Lacombe
 */
public abstract class Hydraulique1dReseauSingularite
        extends DjaLink
        implements Hydraulique1dReseauElementInterf {

  public Hydraulique1dReseauSingularite(String _text) {
    super();
    if (_text != null) {
      addText(_text);
    }
  }
  /*  public void propertyChange(PropertyChangeEvent evt) {
   if (evt.getPropertyName().equals("singularite")) {
   putData("singularite", evt.getNewValue());
   }
   }*/

  @Override
  public void setWidth(int _w) {
    super.setHeight(_w);
    super.setWidth(_w);
  }

  @Override
  public void setHeight(int _h) {
    super.setWidth(_h);
    super.setHeight(_h);
  }

  @Override
  public void paintObject(Graphics _g) {
    updateXYO();
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();
    Color bg = getBackground();
    Color fg = getForeground();
    if (bg != null && isSelected()) {
      DjaGraphics.setColor(_g, bg);
      DjaGraphics.fillRect(_g, x, y, w - 1, h - 1);
    }
    if (fg != null) {
      DjaGraphics.getBresenhamParams(this);
      DjaGraphics.setColor(_g, fg);
    }
    if (getBuIcon() != null) {
      CtuluLibImage.setBestQuality((Graphics2D) _g);
      _g.drawImage(getBuIcon().getImage(), x, y, w, h, HELPER);
    }

    super.paintObject(_g);
  }

  @Override
  public DjaAttach[] getAttachs() {
    updateXYO();
    DjaAttach[] r = new DjaAttach[1];
    r[0] = new DjaAttach(this, 0, begin_, obegin_, getX(), getY());
    return r;
  }

  @Override
  public int getX() {
    updateXYO();
    return xbegin_;
  }

  @Override
  public int getY() {
    updateXYO();
    return ybegin_;
  }

  @Override
  public int getWidth() {
    return 16;
  }

  @Override
  public int getHeight() {
    return 16;
  }

  @Override
  public String[] getInfos() {
    MetierHydraulique1d iobjet = (MetierHydraulique1d) getData("singularite");
    if (iobjet != null) {
      return iobjet.getInfos();
    } else {
      String[] isNull = new String[2];
      isNull[0] = "Objet null !";
      isNull[1] = "Pas d'infos disponible";
      return isNull;
    }
  }

  abstract BuIcon getBuIcon();
}
