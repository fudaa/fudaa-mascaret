package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.Color;
import java.awt.Graphics;

import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierApport;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSource;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuIcon;
import com.memoire.dja.DjaGraphics;
import static com.memoire.dja.DjaOptions.HELPER;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluLibImage;

/**
 * Composant graphique du réseau hydraulique représentant une source de tracer.
 *
 * @see MetierApport
 * @version $Revision: 1.4 $ $Date: 2007-11-20 11:42:40 $ by $Author: bmarchan $
 * @author Olivier Pasteur
 */
public class Hydraulique1dReseauSource extends Hydraulique1dReseauSingularite {

  private final static BuIcon ICON = Hydraulique1dResource.HYDRAULIQUE1D.getIcon("reseau/reseau_source.png");

  public Hydraulique1dReseauSource(MetierSource source) {
    super(null);
    //this.setX(getX()-getWidth());
    //this.setY(getY()-getHeight());
    if (source != null) {
      putData("singularite", source);
    }
  }

  public Hydraulique1dReseauSource() {
    this(null);
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    Hydraulique1dReseauSource r = (Hydraulique1dReseauSource) super.clone();
    MetierReseau reseau = Hydraulique1dProjet.getInstance().getEtude1d().reseau();
    MetierSource source = reseau.creeSource();
    source.initialise((MetierSource) getData("singularite"));
    r.putData("singularite", source);
    return r;
  }

  @Override
  BuIcon getBuIcon() {
    return ICON;
  }
  /* public DjaAttach[] getAttachs() {
   updateXYO();
   DjaAttach[] r= new DjaAttach[1];
   r[0]= new DjaAttach(this, 0, begin_, obegin_, getX(), getY());
	   
   return r;
   }*/

  @Override
  public void paintObject(Graphics _g) {
    updateXYO();
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();
    Color bg = getBackground();
    Color fg = getForeground();
    if (bg != null && isSelected()) {
      DjaGraphics.setColor(_g, bg);
      DjaGraphics.fillRect(_g, x - w, y - h, w - 1, h - 1);
    }
    if (fg != null) {
      DjaGraphics.getBresenhamParams(this);
      DjaGraphics.setColor(_g, fg);
    }
    if (getBuIcon() != null) {
      CtuluLibImage.setBestQuality((Graphics2D) _g);
      _g.drawImage(getBuIcon().getImage(), x, y, w, h, HELPER);
    }

  }

  @Override
  public boolean contains(int _x, int _y) {
    int w = getWidth();
    int h = getHeight();
    return getBounds().contains(_x + w, _y + h);
  }

}
