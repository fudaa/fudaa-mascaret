package org.fudaa.fudaa.hydraulique1d.reseau;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;

import javax.swing.JOptionPane;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierExtremite;
import org.fudaa.dodico.hydraulique1d.metier.MetierHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierLiaison;
import org.fudaa.dodico.hydraulique1d.metier.MetierNoeud;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierSingularite;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilDenoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilGeometrique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLimniAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilLoi;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilNoye;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAmont;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilTarageAval;
import org.fudaa.dodico.hydraulique1d.metier.singularite.
    MetierSeuilTranscritique;
import org.fudaa.dodico.hydraulique1d.metier.singularite.MetierSeuilVanne;

import com.memoire.dja.DjaFrame;
import com.memoire.dja.DjaGrid;
import com.memoire.dja.DjaObject;

/**
 * Classe permettant de v�rifier la bijection entre le mod�le m�tier et le mod�le graphique.
 *
 * @see MetierBief
 * @version      $Revision$ $Date$ by $Author$
 * @author       Olivier PASTEUR
 */
public final class Hydraulique1dReseauVerificateur {

  private static MetierReseau reseau_;
  private static DjaGrid grid_;

  private Hydraulique1dReseauVerificateur() {

  }


  /*
   * Cette fonction v�rifie la bijection entre le reseau (objets m�tier ) et la grille (objets graphiques)
   * Les objets graphiques null ou en double ne sont pas trait�s (trace d'erreur)
   * Les objets graphiques pointant vers le meme objet m�tier ou vers un objet m�tier null se voient attribu� un nouvel objet m�tier
   * Les objets m�tiers isol�s (non associ�s � un �l�ment graphique) sont supprim�s
   * Enfin la liste des objets m�tiers rattach�s � un objet graphique mais absent de DReseau (correspondant � des objets graphiques non rattach�s) est renvoy�e en sortie de la fonction
   */
  public static Object[] verifierBijectionMetierGraph(MetierEtude1d etude,
      DjaFrame frame) {
    System.err.println("Verification du reseau et de la grille ...");
    reseau_ = etude.reseau();
    grid_ = frame.getGrid();

    HashSet hashSetMetier = new HashSet();
    HashSet hashSetGraph = new HashSet();
    ArrayList listObjetsMetierModifies = new ArrayList();
    //HashSet hashSetObjetMetiersIsoles = construireSetObjetMetiersReseauIsoles();

    boolean ErreursNonCorrigees = false;
    boolean bExtremiteLibreNonAttachee = false;

    boolean etatUsineMuet = Notifieur.getNotifieur().getEventMuet();
    Notifieur.getNotifieur().setEventMuet(false);

    //************************************
    // V�rification de la grille
    //************************************
    Enumeration elemEnum = grid_.getObjects().elements();

    try {
      while (elemEnum.hasMoreElements()) {
        DjaObject element = (DjaObject) elemEnum.nextElement();

        //Cas d'un element graphique null
        if (element == null) {
          System.err.println("!!! verifierBijectionMetierGraph - Erreur : element graphique null d�tect� dans la grille !!!");
          ErreursNonCorrigees = true;
        } else {
          //On v�rifie l'unicit� de l'objet graphique
          if (!hashSetGraph.add(element)) {
            //Cas d'un element graphique en double
            System.err.println(
                "!!! verifierBijectionMetierGraph - Erreur : l' element graphique " +
                element.toString() +
                " est d�tect� en double dans la grille !!!");
            ErreursNonCorrigees = true;
            break;
          }
          //On r�cup�re l'objet m�tier
          String key = getObjetGraphiqueKey(element);
          MetierHydraulique1d objetMetier = (MetierHydraulique1d) element.
                                            getData(
                                                key);
          //Cas d'un element m�tier null ou en double dans un objet graphique
          if (objetMetier == null || !hashSetMetier.add(objetMetier)) {
            MetierHydraulique1d newObjetMetier =
                reconstruireLiaisonGraphiqueMetiers(null, element); // null : hashSetObjetMetiersIsoles
            if (newObjetMetier == null) {
              System.err.println("Un Objet graphique de type extr�mit� libre ou de type inconnu pointe vers un objet m�tier null ou en double: l'erreur n'a pu etre corrig�e");
              bExtremiteLibreNonAttachee = true;
              ErreursNonCorrigees = true;
            } else {
              newObjetMetier.initialise(objetMetier);
              reseau_.initIndiceNumero();
              element.putData(key, newObjetMetier);
              if (!hashSetMetier.add(newObjetMetier)) {
                System.err.println(
                    "Erreur : le nouvel objet n'a pu etre ajout� au hashSet m�tier\n");
              }
              listObjetsMetierModifies.add(newObjetMetier);
              System.err.println(
                  "Un objet graphique pointait vers un objet m�tier null ou d�j� utilis� \n" +
                  "Le nouvel objet m�tier " + newObjetMetier.getInfos()[0] +
                  " a �t� cr�� ou r�associ�");
            }
          }
        } //fin du cas ou l'�l�ment graphique n'est pas nul
      }
    } catch (Throwable ex) {
      ex.printStackTrace();
      return null;
    }
    //Fin du parcours de la grille

    //*********************************************************************
    // V�rification de la liste des objets m�tiers presents dans le r�seau
    //*********************************************************************

    //Si un objet du DReseau n'apparait dans la hashTable, c'est un objet m�tier flotant sans attache graphique on le supprime
    //Si on le trouve dans le hashTable, on le supprime de la hashTable ce qui permet � la fin d'identifier les �l�ments graphiques isol�s du reseau car n'apparaissant pas dans  DReseau ( sauf peut-etre dans nouveauxNoeud_ et nouvellesSing_)

    //Parcourt des biefs
    MetierBief[] biefs = reseau_.biefs();
    ArrayList listBiefsASupprimer = new ArrayList();
    try {
      for (int i = 0; i < biefs.length; i++) {
        //Si l'objet metier du reseau n'apparait pas dans la hashTable
        if (!hashSetMetier.contains(biefs[i])) {
          listBiefsASupprimer.add(biefs[i]);
          System.err.println("L' objet m�tier bief :" + biefs[i].getInfos()[0] +
                             "  n'�tait pas rattach� � un objet graphique. Il a �t� supprim�");

        }
        //Si l'objet metier du reseau apparait  dans la hashTable, on le supprime de la table
        else {
          hashSetMetier.remove(biefs[i]);
        }
      }
      if (listBiefsASupprimer.size() > 0) {
        reseau_.supprimeBiefs((MetierBief[]) listBiefsASupprimer.toArray(new
            MetierBief[listBiefsASupprimer.size()]));
        //System.err.println(listBiefsASupprimer.size()+" objets m�tiers 'bief' non rattach�s � un objet graphique ont �t� supprim�es");

      }

      //Parcourt des noeuds raccroch� � un bief
      MetierNoeud[] noeuds = reseau_.noeudsConnectesBiefs();
      for (int i = 0; i < noeuds.length; i++) {
        //Si l'objet metier du reseau n'apparait pas dans la hashTable
        if (!hashSetMetier.contains(noeuds[i])) {
          reseau_.supprimeNoeud(noeuds[i]);
          System.err.println("L' objet m�tier noeud :" + noeuds[i].getInfos()[0] +
                             "  n'�tait pas rattach� � un objet graphique. Il a �t� supprim�");
        }
        //Si l'objet metier du reseau apparait  dans la hashTable, on le supprime de la table
        else {
          hashSetMetier.remove(noeuds[i]);
        }
      }

      //Parcourt des extremit�s libres
      MetierExtremite[] extremitesLibres = reseau_.extremitesLibres();
      for (int i = 0; i < extremitesLibres.length; i++) {
        //Si l'objet metier du reseau n'apparait pas dans la hashTable
        if (!hashSetMetier.contains(extremitesLibres[i])) {
          reseau_.supprimeExtremite(extremitesLibres[i]);
          System.err.println("L' objet m�tier extremit� libre :" +
                             extremitesLibres[i].getInfos()[0] +
                             "  n'�tait pas rattach� � un objet graphique. Il a �t� supprim�");
        }
        //Si l'objet metier du reseau apparait  dans la hashTable, on le supprime de la table
        else {
          hashSetMetier.remove(extremitesLibres[i]);
        }
      }

      //Parcourt des singularit� raccroch� � un bief
      MetierSingularite[] singularites = reseau_.singularites();
      ArrayList listSingularitesASupprimer = new ArrayList();
      for (int i = 0; i < singularites.length; i++) {
        //Si l'objet metier du reseau n'apparait pas dans la hashTable
        if (!hashSetMetier.contains(singularites[i])) {
          listSingularitesASupprimer.add(singularites[i]);
          System.err.println("L' objet m�tier singularite :" +
                             singularites[i].getInfos()[0] +
                             "  n'�tait pas rattach� � un objet graphique. Il a �t� supprim�");
        }
        //Si l'objet metier du reseau apparait  dans la hashTable, on le supprime de la table
        else {
          hashSetMetier.remove(singularites[i]);
        }
      }
      if (listSingularitesASupprimer.size() > 0) {
        reseau_.supprimeSingularites((MetierSingularite[])
                                     listSingularitesASupprimer.toArray(new
            MetierSingularite[listSingularitesASupprimer.size()]));
        //System.err.println(listSingularitesASupprimer.size()+" objets m�tiers 'singularit�' non rattach�s � un objet graphique ont �t� supprim�es");

      }

      //Parcourt des casiers
      MetierCasier[] casiers = reseau_.casiers();
      ArrayList listCasierASupprimer = new ArrayList();
      for (int i = 0; i < casiers.length; i++) {
        //Si l'objet metier du reseau n'apparait pas dans la hashTable
        if (!hashSetMetier.contains(casiers[i])) {
          listCasierASupprimer.add(casiers[i]);
          System.err.println("L' objet m�tier casiers :" +
                             casiers[i].getInfos()[0] +
                             "  n'�tait pas rattach� � un objet graphique. Il a �t� supprim�");
        }
        //Si l'objet metier du reseau apparait  dans la hashTable, on le supprime de la table
        else {
          hashSetMetier.remove(casiers[i]);
        }
      }
      if (listCasierASupprimer.size() > 0) {
        reseau_.supprimeCasiers((MetierCasier[]) listCasierASupprimer.toArray(new
            MetierCasier[listCasierASupprimer.size()]));
        //System.err.println(listCasierASupprimer.size()+" objets m�tiers 'casier' non rattach�s � un objet graphique ont �t� supprim�es");
      }

      //Parcourt des liaisons
      MetierLiaison[] liaisons = reseau_.liaisons();
      ArrayList listLiaisonsASupprimer = new ArrayList();
      for (int i = 0; i < liaisons.length; i++) {
        //Si l'objet metier du reseau n'apparait pas dans la hashTable
        if (!hashSetMetier.contains(liaisons[i])) {
          listLiaisonsASupprimer.add(liaisons[i]);
          System.err.println("L' objet m�tier liaison :" +
                             liaisons[i].getInfos()[0] +
                             "  n'�tait pas rattach� � un objet graphique. Il a �t� supprim�");
        }
        //Si l'objet metier du reseau apparait  dans la hashTable, on le supprime de la table
        else {
          hashSetMetier.remove(liaisons[i]);
        }
      }
      if (listLiaisonsASupprimer.size() > 0) {
        reseau_.supprimeLiaisons((MetierLiaison[]) listLiaisonsASupprimer.
                                 toArray(new MetierLiaison[listLiaisonsASupprimer.size()]));
        //System.err.println(listLiaisonsASupprimer.size()+" objets m�tiers 'liaison' non rattach�s � un objet graphique ont �t� supprim�es");
      }
    } catch (Throwable ex1) {
      ex1.printStackTrace();
      return null;
    }

    String message = "";

    if (!listObjetsMetierModifies.isEmpty()) {
      message = "Des �v�nements se sont produits lors de la v�rification du reseau:\n\n" + message;
      for (int i = 0; i < listObjetsMetierModifies.toArray().length; i++) {
        message = message + "L' objet " + ((MetierHydraulique1d) listObjetsMetierModifies.toArray()[i]).
                  getInfos()[0] + " a du �tre r�associ� ou r�initialis� - Veuillez v�rifier ses param�tres !\n";
      }
      if (bExtremiteLibreNonAttachee) {
        message = message + "Une ou plusieurs extremit�s libres non attach�es ont �t� d�t�ct�, supprimez l� !\n";
      }
      if (ErreursNonCorrigees) {
        message = message + "\nAttention ! Certaines incoh�rences dans le r�seau n'ont pu �tre corrig�es! (Consultez la console)\n";
      }
      JOptionPane.showConfirmDialog(null, message, "Attention !",
                                    JOptionPane.WARNING_MESSAGE);
    } else if (ErreursNonCorrigees || bExtremiteLibreNonAttachee) {
      if (bExtremiteLibreNonAttachee) {
        message = message + "Une ou plusieurs extremit�s libres non attach�es ont �t� d�t�ct�es, supprimez les !\n";
      }
      if (ErreursNonCorrigees) {
        message = message + "\nAttention ! Certaines incoh�rences dans le r�seau n'ont pu �tre corrig�es! (Consultez la console)\n";
      }
      JOptionPane.showConfirmDialog(null, message, "Attention !",
                                    JOptionPane.WARNING_MESSAGE);

    } else {
      System.out.println("V�rification du reseau et de la grille OK !");
    }

    Notifieur.getNotifieur().setEventMuet(etatUsineMuet);

    //On renvoie la liste des objets graphiques non ratach�s
    if (hashSetMetier.isEmpty()) {
      return null;
    } else {
      System.err.println(hashSetMetier.size() + " objets non rattach�s trouv�s");
      for (int i = 0; i < hashSetMetier.toArray().length; i++) {
        System.err.println(hashSetMetier.toArray()[0].toString());
        System.err.println(((MetierHydraulique1d) hashSetMetier.toArray()[0]).
                           getInfos()[0]);
        System.err.println(((MetierHydraulique1d) hashSetMetier.toArray()[0]).
                           getInfos()[1]);
      }
      return hashSetMetier.toArray();
    }

  }


  //Construit un set contenant tous les objets m�tiers pr�sent dans le reseau mais non rattach� � un objet graphique
  public static HashSet construireSetObjetMetiersReseauIsoles() {

    HashSet hashSetObjetsMetierGrille = new HashSet();
    HashSet hashSetObjetsMetierReseauIsole = new HashSet();

    //Parcours de la grille pour remplir hashSetObjetsMetierGrille
    Enumeration elemEnum = grid_.getObjects().elements();

    while (elemEnum.hasMoreElements()) {
      DjaObject element = (DjaObject) elemEnum.nextElement();
//        	On r�cup�re l'objet m�tier
      String key = getObjetGraphiqueKey(element);
      MetierHydraulique1d objetMetier = (MetierHydraulique1d) element.getData(
          key);
      //Cas d'un element m�tier null dans un objet graphique existant
      if (objetMetier != null) {
        hashSetObjetsMetierGrille.add(objetMetier);
      }
    }

//      Parcours du r�seau pour remplir hashSetObjetsMetierReseauIsole
    MetierBief[] biefs = reseau_.biefs();
    for (int i = 0; i < biefs.length; i++) {
      //Si l'objet metier du reseau n'apparait pas dans la hashTable
      if (!hashSetObjetsMetierGrille.contains(biefs[i])) {
        hashSetObjetsMetierReseauIsole.add(biefs[i]);
      }
    }
    //Parcourt des noeuds raccroch� � un bief
    MetierNoeud[] noeuds = reseau_.noeudsConnectesBiefs();
    for (int i = 0; i < noeuds.length; i++) {
      //Si l'objet metier du reseau n'apparait pas dans la hashTable
      if (!hashSetObjetsMetierGrille.contains(noeuds[i])) {
        hashSetObjetsMetierReseauIsole.add(noeuds[i]);
      }
    }
    //Parcourt des extremit�s libres
    MetierExtremite[] extremitesLibres = reseau_.extremitesLibres();
    for (int i = 0; i < extremitesLibres.length; i++) {
      //Si l'objet metier du reseau n'apparait pas dans la hashTable
      if (!hashSetObjetsMetierGrille.contains(extremitesLibres[i])) {
        hashSetObjetsMetierReseauIsole.add(extremitesLibres[i]);
      }
    }
    //Parcourt des singularit� raccroch� � un bief
    MetierSingularite[] singularites = reseau_.singularites();
    for (int i = 0; i < singularites.length; i++) {
      //Si l'objet metier du reseau n'apparait pas dans la hashTable
      if (!hashSetObjetsMetierGrille.contains(singularites[i])) {
        hashSetObjetsMetierReseauIsole.add(singularites[i]);
      }
    }
    //Parcourt des casiers
    MetierCasier[] casiers = reseau_.casiers();
    for (int i = 0; i < casiers.length; i++) {
      //Si l'objet metier du reseau n'apparait pas dans la hashTable
      if (!hashSetObjetsMetierGrille.contains(casiers[i])) {
        hashSetObjetsMetierReseauIsole.add(casiers[i]);
      }
    }
    //Parcourt des liaisons
    MetierLiaison[] liaisons = reseau_.liaisons();
    for (int i = 0; i < liaisons.length; i++) {
      //Si l'objet metier du reseau n'apparait pas dans la hashTable
      if (!hashSetObjetsMetierGrille.contains(liaisons[i])) {
        hashSetObjetsMetierReseauIsole.add(liaisons[i]);
      }
    }

    return hashSetObjetsMetierReseauIsole;
  }


  //Permet de d�terminer si un objet m�tier de la liste hashSetObjetsMetierReseauIsole correspond � l'objet graphique element
  public static MetierHydraulique1d reconstruireLiaisonGraphiqueMetiers(HashSet
      hashSetObjetsMetierReseauIsole, DjaObject element) {
    MetierHydraulique1d objetMetier = null;
    /*int nbObjetMemeClasseRencontre=0;
        if (hashSetObjetsMetierReseauIsole==null) return creerNouvelObjetMetierVide(element,null,reseau_);

        //TODO rechercher l'objet
        Iterator it = hashSetObjetsMetierReseauIsole.iterator();
        while (it.hasNext()){
      MetierHydraulique1d objMetierCourant = (MetierHydraulique1d) it.next();


     if (element instanceof Hydraulique1dReseauNoeud) {
     if (objMetierCourant.getClass()==DNoeud.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauBiefCourbe) {
     if (objMetierCourant.getClass()==MetierBief.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauExtremLibre) {
      if (objMetierCourant.getClass()==MetierExtremite.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauApport) {
      if (objMetierCourant.getClass()==MetierApport.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauDeversoir) {
      if (objMetierCourant.getClass()==MetierDeversoir.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauPerteCharge) {
      if (objMetierCourant.getClass()==MetierPerteCharge.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauSeuil) {
      if (objMetierCourant.getClass()==MetierSeuil.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauSource) {
      if (objMetierCourant.getClass()==MetierSource.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauCasier) {
      if (objMetierCourant.getClass()==MetierCasier.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
     else if (element instanceof Hydraulique1dReseauLiaisonCasier) {
      if (objMetierCourant.getClass()==MetierLiaison.class) objetMetier=objMetierCourant;
      nbObjetMemeClasseRencontre++;
     }
        }
        if (nbObjetMemeClasseRencontre==1)  {
     hashSetObjetsMetierReseauIsole.remove(objetMetier);
     return objetMetier;
        } else{
     objetMetier=null;
        }*/

    //Si l'objet metier perdu ne peut etre retrouve
    if (objetMetier == null) {
      objetMetier = creerNouvelObjetMetierVide(element, null, reseau_);
    }

    return objetMetier;
  }


  //Renvoie la clef de l'objet m�tier, c'est-�-dire le type d'objet sous forme de string (correspondant � la hashTable data du DjaObject)
  private static String getObjetGraphiqueKey(DjaObject objetGraphique) {
    if (objetGraphique instanceof Hydraulique1dReseauNoeud) {
      return "noeud";
    } else if (objetGraphique instanceof Hydraulique1dReseauBiefCourbe) {
      return "bief";
    } else if (objetGraphique instanceof Hydraulique1dReseauExtremLibre) {
      return "extremite";
    } else if (objetGraphique instanceof Hydraulique1dReseauSingularite) {
      return "singularite";
    } else if (objetGraphique instanceof Hydraulique1dReseauCasier) {
      return "casier";
    } else if (objetGraphique instanceof Hydraulique1dReseauLiaisonCasier) {
      return "liaison";
    } else {
      return "";
    }
  }


  //Renvoie l'objet m�tier correspondant au type indiqu� en argument
  private static MetierHydraulique1d creerNouvelObjetMetierVide(DjaObject
      objetGraphique, MetierHydraulique1d objetMetier, MetierReseau reseau) {
    if (objetGraphique instanceof Hydraulique1dReseauNoeud) {
      return new MetierNoeud();
    } else if (objetGraphique instanceof Hydraulique1dReseauBiefCourbe) {
      return reseau.creeBief();
    } else if (objetGraphique instanceof Hydraulique1dReseauExtremLibre) {
      return null; //cas non trait� car difficile de cr�er une nouvelle extremit� libre
    } else if (objetGraphique instanceof Hydraulique1dReseauCasier) {
      return reseau.ajouterCasier();
    } else if (objetGraphique instanceof Hydraulique1dReseauLiaisonCasier) {
      return reseau.ajouterLiaison();
    } else if (objetGraphique instanceof Hydraulique1dReseauApport) {
      return reseau.creeApport();
    } else if (objetGraphique instanceof Hydraulique1dReseauDeversoir) {
      return reseau.creeDeversoir();
    } else if (objetGraphique instanceof Hydraulique1dReseauSource) {
      return reseau.creeSource();
    } else if (objetGraphique instanceof Hydraulique1dReseauPerteCharge) {
      return reseau.creePerteCharge();
    } else if (objetGraphique instanceof Hydraulique1dReseauSeuil) {
      if (objetMetier == null) {
        return reseau.creeSeuilLoi();
      } else if (objetMetier instanceof MetierSeuilDenoye) {
        return reseau.creeSeuilDenoye();
      } else if (objetMetier instanceof MetierSeuilGeometrique) {
        return reseau.creeSeuilGeometrique();
      } else if (objetMetier instanceof MetierSeuilLimniAmont) {
        return reseau.creeSeuilLimniAmont();
      } else if (objetMetier instanceof MetierSeuilLoi) {
        return reseau.creeSeuilLoi();
      } else if (objetMetier instanceof MetierSeuilNoye) {
        return reseau.creeSeuilNoye();
      } else if (objetMetier instanceof MetierSeuilTarageAmont) {
        return reseau.creeSeuilTarageAmont();
      } else if (objetMetier instanceof MetierSeuilTarageAval) {
        return reseau.creeSeuilTarageAval();
      } else if (objetMetier instanceof MetierSeuilTranscritique) {
        return reseau.creeSeuilTranscritique();
      } else if (objetMetier instanceof MetierSeuilVanne) {
        return reseau.creeSeuilVanne();
      } else {
        return reseau.creeSeuilLoi();
      }
    } else {
      return null;
    }
  }


  public static void SupprimerObjetNonConnectes(Object[] objetsNonConnectes,
                                                DjaFrame frame) {
    grid_ = frame.getGrid();
    Enumeration elemEnum = grid_.getObjects().elements();
    while (elemEnum.hasMoreElements()) {
      DjaObject element = (DjaObject) elemEnum.nextElement();

      //Cas d'un element graphique null
      if (element == null) {
        System.err.println("!!! SupprimerObjetNonConnectes - Erreur : element null d�tect� dans la grille !!!");
      } else {
        //On r�cup�re l'objet m�tier
        String key = getObjetGraphiqueKey(element);
        MetierHydraulique1d objetMetier = (MetierHydraulique1d) element.getData(
            key);

        for (int i = 0; i < objetsNonConnectes.length; i++) {
          if (objetMetier == null ||
              (objetMetier.egale((MetierHydraulique1d) objetsNonConnectes[i]))) {
            grid_.remove(element);
          }
        }
      }
    }
  }
}

