package org.fudaa.fudaa.hydraulique1d.reseau;

import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaVector;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

/**
 * a custom grid interactive that allow zoom and dnd
 */
public class MascaretGridInteractive extends DjaGridInteractive {
  
  private static final long serialVersionUID = 1L;
  private MascaretScaledGridMouseListener mouseScaleListener;
  private Hydraulique1dReseauFrame parentFrame;
  
  public MascaretGridInteractive(boolean b, DjaVector djaVector) {
    super(b, djaVector);
    init();
  }
  
  private final void init() {
    setOpaque(true);
    setBackground(Color.WHITE);
    mouseScaleListener = new MascaretScaledGridMouseListener(this);
    addMouseListener(mouseScaleListener);
    addMouseMotionListener(mouseScaleListener);
    setDoubleBuffered(false);
    setPaintRect(false);
  }
  
  public void setParentFrame(Hydraulique1dReseauFrame parentFrame) {
    this.parentFrame = parentFrame;
  }
  
  public Hydraulique1dReseauFrame getParentFrame() {
    return parentFrame;
  }
  
  public MascaretScaledGridMouseListener getMouseScaleListener() {
    return mouseScaleListener;
  }
  
  public MascaretGridInteractive() {
    super(false);
    init();
  }
  
  public void setScale(float scale) {
    getTransform().setScale(scale);
  }
  
  public void setTranslationActive(boolean active) {
    mouseScaleListener.setMoveActionEnabled(active);
  }
  
  public void setZoom(boolean active) {
    mouseScaleListener.setZoomActionEnabled(active);
  }
  
  @Override
  public void paint(Graphics g) {
    final Dimension size = getSize();
    Rectangle clip = new Rectangle(0, 0, size.width, size.height);
    if (isOpaque()) {
      g.setColor(getBackground());
      g.fillRect(clip.x, clip.y, clip.width, clip.height);
    }
    super.paint(g);
  }
  
  @Override
  public void setInteractive(boolean _s
  ) {
    super.setInteractive(_s);
    if (_s) {
      mouseScaleListener.setZoomActionEnabled(false);
      mouseScaleListener.setMoveActionEnabled(false);
    }
  }
  
  public void zoomOn(MouseEvent evt, boolean zoomIn) {
    
    getTransform().zoom(evt, zoomIn);
    getParentFrame().repaint(0);
    repaint(0);
    
  }

//  @Override
//  public Insets getInsets() {
//
//    Insets insets = super.getInsets();
//    int tx = getTransform().getTranslateX();
//    int ty = getTransform().getTranslateY();
//    Insets translated = new Insets((int) (insets.top - Math.abs(ty)), (int) (insets.left - Math.abs(tx)),
//            (int) (insets.bottom + ty), (int) (insets.right + tx));
//    //System.out.println("insets" +translated);
//    return translated;
//
//  }
  boolean isZoomOrMoveActivated() {
    return mouseScaleListener.isMoveActionEnabled() || mouseScaleListener.isZoomActionEnabled();
  }
  
}
