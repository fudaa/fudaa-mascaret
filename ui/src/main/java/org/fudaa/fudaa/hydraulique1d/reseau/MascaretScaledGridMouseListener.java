package org.fudaa.fudaa.hydraulique1d.reseau;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

import java.awt.event.MouseAdapter;

/**
 * mouse listner specifics for mascaret grid. take into account the scale to recalculate the mouse coordinate (translation done on coordinates)
 *
 * @author Adrien Hadoux
 */
public class MascaretScaledGridMouseListener extends MouseAdapter {

  /**
   * if true then drag and drop will translate screen.
   */
  boolean moveActionEnabled = false;
  boolean zoomActionEnabled = false;
  int actionStartPointX;
  int actionStartPointY;
  final MascaretGridInteractive grid;

  int lastXDrawn;
  int lastYDrawn;
  int lastWDrawn;
  int lastHDrawn;

  public MascaretScaledGridMouseListener(MascaretGridInteractive _grid) {
    this.grid = _grid;

  }

  protected int getScaledX(MouseEvent _evt) {
    return grid.getTransform().getScaledX(_evt);
  }

  protected int getScaledX(MouseEvent _evt, boolean translation) {
    return grid.getTransform().getScaledX(_evt, translation);
  }

  protected int getScaledY(MouseEvent _evt, boolean translation) {
    return grid.getTransform().getScaledY(_evt, translation);
  }

  protected int getScaledY(MouseEvent _evt) {
    return grid.getTransform().getScaledY(_evt);
  }

  public int getTranslateX() {
    return (int) grid.getTransform().getTranslateX();
  }

  public int getTranslateY() {
    return (int) grid.getTransform().getTranslateY();
  }

  public void translate(MouseEvent _evt) {
    int x = getScaledX(_evt, false);
    int y = getScaledY(_evt, false);
    grid.getTransform().setTranslateX(x - this.actionStartPointX);
    grid.getTransform().setTranslateY(y - this.actionStartPointY);
    repaint();
  }

  boolean zoomOnRectangle;

  @Override
  public void mousePressed(MouseEvent _evt) {
    zoomOnRectangle = false;
    grid.repaint(0);
    if (moveActionEnabled || zoomActionEnabled) {
      actionStartPointX = getScaledX(_evt);
      actionStartPointY = getScaledY(_evt);
      lastXDrawn = _evt.getX();
      lastYDrawn = _evt.getY();
    }

  }

  boolean erase;

  private void erase() {
    Graphics g = grid.getGraphics();
    g.setXORMode(Color.white);
    if (erase) {
      g.drawRect(lastXDrawn, lastYDrawn, lastWDrawn, lastHDrawn);
      erase = false;
    }
  }

  @Override
  public void mouseDragged(MouseEvent _evt) {

    if (moveActionEnabled) {
      translate(_evt);
    } else if (zoomActionEnabled) {
      if (!zoomOnRectangle && (_evt.getX() - lastXDrawn) < 3 && (_evt.getY() - lastYDrawn) < 3) {
        return;
      }

      zoomOnRectangle = true;
      erase();
      lastWDrawn = _evt.getX() - lastXDrawn;
      lastHDrawn = _evt.getY() - lastYDrawn;
      Graphics g = grid.getGraphics();
      g.setXORMode(Color.white);
      g.drawRect(lastXDrawn, lastYDrawn, lastWDrawn, lastHDrawn);
      erase = true;

    }
  }

  @Override
  public void mouseReleased(MouseEvent evt) {
    if (zoomActionEnabled) {
      if (zoomOnRectangle) {
        erase();
        grid.getTransform().zoom(lastXDrawn, lastYDrawn, evt.getX(), evt.getY(), grid.getWidth(), grid.getHeight());
        repaint();
      } else {
        grid.getTransform().zoom(evt, !evt.isControlDown());
        repaint();
      }

    }
    zoomOnRectangle = false;
  }

  protected void repaint() {
    grid.getParentFrame().getContentPane().repaint(0);
    grid.repaint(0);
  }

  public boolean isMoveActionEnabled() {
    return moveActionEnabled;
  }

  public boolean isZoomActionEnabled() {
    return zoomActionEnabled;
  }

  public void setZoomActionEnabled(boolean zoomActionEnabled) {
    this.zoomActionEnabled = zoomActionEnabled;
    if (zoomActionEnabled) {
      moveActionEnabled = false;
      this.grid.getGrid().setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    }
    grid.setInteractiveEnable(!moveActionEnabled && !zoomActionEnabled);
  }

  public void setMoveActionEnabled(boolean moveActionEnabled) {
    this.moveActionEnabled = moveActionEnabled;
    grid.setInteractiveEnable(!moveActionEnabled && !zoomActionEnabled);
    if (moveActionEnabled) {
      zoomActionEnabled = false;
      this.grid.getGrid().setCursor(new Cursor(Cursor.MOVE_CURSOR));
    } else {
      this.grid.getGrid().setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    }
  }

}
