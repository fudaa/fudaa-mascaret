/*
 * @file         ActionDoubleClickEvent.java
 * @creation     2004-06-29
 * @modification $Date: 2005-08-16 13:53:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.EventObject;

/**
 * Definit un �v�nement lors d'un "double click" sur une ligne de tableau de r�els .
 * @see org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigneReelTableau
 * @see org.fudaa.fudaa.hydraulique1d.tableau.ActionDoubleClickListener
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.5 $ $Date: 2005-08-16 13:53:01 $ by $Author: deniger $
 */
public class ActionDoubleClickEvent extends EventObject {
  private Hydraulique1dLigneReelTableau ligne_=null;
  private int indexeLigne_=-1;

  /**
   * Contructeur de l'�v�nement.
   * @param source Le tableau sue lequel l'utilisateur � "double-cliquer".
   * @param ligne La ligne de tableau sur lequel l'utilisateur � "double-cliquer".
   */
  public ActionDoubleClickEvent(Object source,
                                Hydraulique1dLigneReelTableau ligne) {
    super(source);
    ligne_ = ligne;
  }
  /**
   * Contructeur de l'�v�nement.
   * @param source Le tableau sue lequel l'utilisateur � "double-cliquer".
   * @param indexeLigne L'indice de la ligne du tableau sur lequel l'utilisateur � "double-cliquer".
   */
  public ActionDoubleClickEvent(Object source, int indexeLigne) {
    super(source);
    indexeLigne_ = indexeLigne;
  }

  /**
   * @return La ligne de tableau sur lequel l'utilisateur � double-cliquer.
   */
  public Hydraulique1dLigneReelTableau getLigne() {
    return ligne_;
  }
  /**
   * @return L'indice de la ligne du tableau sur lequel l'utilisateur � double-cliquer.
   */
  public int getIndexeLigne() {
    return indexeLigne_;
  }

  /**
   * Returns a String representation of this ActionDoubleClickEvent.
   *
   * @return  A a String representation of this ActionDoubleClickEvent.
   */
  @Override
  public String toString() {
    String res=getClass().getName() + "[source=" + source;
    if (ligne_ != null) {
      res +="; ligne=" + ligne_ +"]";
    } else {
      res +="; indexeLigne=" + indexeLigne_ +"]";
    }
    return res;
  }
}
