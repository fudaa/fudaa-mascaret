/*
 * @file         Hydraulique1dLigne1EntierEtReelsTableau.java
 * @creation     2004-06-29
 * @modification $Date: 2006-07-07 12:26:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
/**
 * Definit le mod�le d'une ligne de tableau commen�ant par un entier puis des r�els.
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.7 $ $Date: 2006-07-07 12:26:24 $ by $Author: opasteur $
 */
public class Hydraulique1dLigne1EntierEtReelsTableau extends Hydraulique1dLigneReelTableau {

   /**
    * L'entier en d�but de ligne (peut �tre null).
    */
   private Integer entier_;
  /**
   * Constructeur le plus g�n�ral pr�cisant la taille de la ligne.
   * @param taille le nombre de r�els de la ligne.
   * Au d�part les r�els sont initialis�s � null.
   */
  public Hydraulique1dLigne1EntierEtReelsTableau(int taille) {
    super(taille);
  }
  /**
   * Constructeur pour une ligne initialis�e par 3 r�els primitifs.
   * @param i entier premier �l�ment de la ligne.
   * @param x r�el deuxi�me �l�ment de la ligne.
   * @param y r�el troisi�me �l�ment de la ligne.
   * @param z r�el quatri�me �l�ment de la ligne.
   */
  public Hydraulique1dLigne1EntierEtReelsTableau(int i, double x, double y, double z) {
    this(intValue(i), doubleValue(x), doubleValue(y), doubleValue(z));
  }
  /**
   * Constructeur pour une ligne initialis�e par 3 r�els non primitifs.
   * @param i entier premier �l�ment de la ligne.
   * @param x r�el deuxi�me �l�ment de la ligne.
   * @param y r�el troisi�me �l�ment de la ligne.
   * @param z r�el quatri�me �l�ment de la ligne.
   */
  public Hydraulique1dLigne1EntierEtReelsTableau(Integer i , Double x, Double y, Double z) {
    super(x, y, z);
    entier_=i;
  }
  /**
 * Constructeur pour des concentrations initiales avec n r�els primitifs.
 * @param i entier premier �l�ment de la ligne.
 * @param c tableau de r�els tous les elements suivants de la ligne.
 */
public Hydraulique1dLigne1EntierEtReelsTableau(int i,double[] c) {
   this(intValue(i), doubleValue(c));

}
  /**
 * Constructeur pour des concentrations initiales avec n r�els non primitifs.
 * @param i entier premier �l�ment de la ligne.
 * @param c tableau de r�els tous les elements suivants de la ligne.
 */
public Hydraulique1dLigne1EntierEtReelsTableau(Integer i,Double[] c) {
  super(c);
  entier_=i;

}

  /**
   * @return la valeur enti�re de la ligne (1�re colonne).
   * peut �tre nulle si la cellule est vide.
   */
  public Integer entier() {
    return entier_;
  }
  /**
   * @return la valeur enti�re (type primitif) de la ligne (1�re colonne).
   * peut �tre Integer.MAX_VALUE si la cellule est vide.
   */
  public int i() {
    return intValue(entier_);
  }


  /**
   * Initialise le premier �lement de la ligne.
   * @param entier La nouvelle valeur enti�re de la ligne (peut �tre nulle).
   */
  public void entier(Integer entier) {
    entier_=entier;
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param entier La nouvelle valeur enti�re de la ligne.
   */
  public void i(int entier) {
    entier_ = intValue(entier);
  }

  /**
   * @return les valeurs reelles (type primitif) de la ligne (2eme colonne et au-dela).
   */
  public double[] ListeDoubles() {
      return this.getTab();
  }

  /**
   Initialise les  �lements "double"  de la ligne.
   * @param double[] Les nouvelles valeurs doubles de la ligne.
   */
  public void ListeDoubles(double[] d) {
      for (int i = 0; i < d.length; i++) {
           this.setValue(i,d[i]);
      }
  }

  /**
   Initialise l' �lement "double" � la position i de la ligne.
   * @param double,int La nouvelle valeur  double  et sa position dans la ligne.
   */
  public void ListeDoubles(int i,double d) {
      this.setValue(i,d);
  }
  /**
   * @return le nombre d'�l�ment possible de la ligne.
   */
  @Override
  public int getTaille() {
    return super.getTaille()+1;
  }
  /**
   * @return Vrai, s'il existe une valeur null (cellule vide), Faux sinon.
   */
  @Override
  public boolean isExisteNulle() {
    boolean res = super.isExisteNulle();
    if (res) return true;
    else if (entier_ == null) return true;
    return false;
  }
  /**
   * @return Vrai, s'il y a que des valeurs null (cellule vide), Faux sinon.
   */
  @Override
  public boolean isToutNulle() {
    boolean res = super.isToutNulle();
    if (!res) return false;
    else if (entier_ == null) return true;
    return false;
  }
  /**
   * Efface la ligne en mettant ques des valeurs nulles.
   * Met l'entier � null.
   */
  @Override
  public void effaceLigne() {
    super.effaceLigne();
    entier_ = null;
  }
  /**
   * @return les diff�rents �l�ments de la ligne s�par�e par un espace.
   */
  @Override
  public String toString() {
    if (entier_ == null) {
      return " vide , "+super.toString();
    }
    return entier_ + ", " + super.toString();
  }
  /**
   * Compares this object with the specified object for order.  Returns a
   * negative integer, zero, or a positive integer as this object is less
   * than, equal to, or greater than the specified object.<p>
   *
   * In the foregoing description, the notation
   * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
   * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
   * <tt>0</tt>, or <tt>1</tt> according to whether the value of <i>expression</i>
   * is negative, zero or positive.
   *
   * The implementor must ensure <tt>sgn(x.compareTo(y)) ==
   * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
   * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
   * <tt>y.compareTo(x)</tt> throws an exception.)<p>
   *
   * The implementor must also ensure that the relation is transitive:
   * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
   * <tt>x.compareTo(z)&gt;0</tt>.<p>
   *
   * Finally, the implementer must ensure that <tt>x.compareTo(y)==0</tt>
   * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
   * all <tt>z</tt>.<p>
   *
   * It is strongly recommended, but <i>not</i> strictly required that
   * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
   * class that implements the <tt>Comparable</tt> interface and violates
   * this condition should clearly indicate this fact.  The recommended
   * language is "Note: this class has a natural ordering that is
   * inconsistent with equals."
   *
   * @param   o the Object to be compared.
   * @return  a negative integer, zero, or a positive integer as this object
   *		is less than, equal to, or greater than the specified object.
   *
   * @throws ClassCastException if the specified object's type prevents it
   *         from being compared to this Object.
   */
  @Override
  public int compareTo(Object o) {
    Hydraulique1dLigne1EntierEtReelsTableau l = (Hydraulique1dLigne1EntierEtReelsTableau)o;
    if (getTaille() == 0) return 0;
    if ((entier() == null) && (l.entier()==null) ){
      return 0;
    }
    else if (entier() == null) {
      return 1;
    }
    else if (l.entier() == null) {
      return -1;
    }
    else {
      int compEntier = entier().compareTo(l.entier());
      if (compEntier == 0) return super.compareTo(l);
      return compEntier;
    }
  }


  protected final static int intValue(Integer x) {
    if (x == null) return Integer.MAX_VALUE;
    return x.intValue();
  }
  protected final static Integer intValue(int x) {
    if (x == Integer.MAX_VALUE) return null;
    return new Integer(x);
  }


}
