/*
 * @file         Hydraulique1dLigneBooleensTableau.java
 * @creation     2006-07-03
 * @modification $Date: 2007-05-04 13:59:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import java.util.ArrayList;
import java.util.List;
/**
 * Definit le mod�le d'une ligne de tableau de booleens.
 * @see
 * @author Olivier Pasteur
 * @version 1.0
 */

public class Hydraulique1dLigneBooleensTableau implements Comparable {

    /**
      * le tableau contenant les diff�rents reels de la ligne
      */
     private List listeBooleens_;
     /**
      * Constructeur le plus g�n�ral pr�cisant la taille de la ligne.
      * @param taille le nombre de booleens de la ligne.
      * Au d�part les booleens sont initialis�s � null.
      */
     public Hydraulique1dLigneBooleensTableau(int taille) {
       if (taille>=0) {
         listeBooleens_ = new ArrayList(taille);
         for (int i = 0; i < taille; i++) {
           listeBooleens_.add(null);
         }
       } else {
         throw new IllegalStateException("Taille de la ligne incorrecte : "+taille);
       }
     }
     /**
      * Constructeur pour une ligne initialis�e par 2 booleens primitifs.
      * @param x premier �l�ment de la ligne.
      * @param y deuxi�me �l�ment de la ligne.
      */
     public Hydraulique1dLigneBooleensTableau(boolean x, boolean y) {
       this(Boolean.valueOf(x), Boolean.valueOf(y));
     }
     /**
      * Constructeur pour une ligne initialis�e par 2 booleens non primitifs.
      * @param x premier �l�ment de la ligne, peut �tre null (cellule vide).
      * @param y deuxi�me �l�ment de la ligne, peut �tre null (cellule vide).
      */
     public Hydraulique1dLigneBooleensTableau(Boolean x, Boolean y) {
       this(2);
       listeBooleens_.set(0,x);
       listeBooleens_.set(1,y);
     }


     /**
    * Constructeur pour une ligne initialis�e par n booleens primitifs.
    * @param c �l�ments de la ligne.
    */
   public Hydraulique1dLigneBooleensTableau(boolean[] c) {

       this(booleanValue(c));
   }

     /**
      * Constructeur pour une ligne initialis�e par n booleens non primitifs.
      * @param c  �l�ments de la ligne, peut �tre null (cellule vide).
      */
     public Hydraulique1dLigneBooleensTableau(Boolean[] c) {
       this(c.length);
       for (int i = 0; i < c.length; i++) {
           listeBooleens_.set(i,c[i]);
       }

     }
     /**
      * Compares this object with the specified object for order.  Returns a
      * negative integer, zero, or a positive integer as this object is less
      * than, equal to, or greater than the specified object.<p>
      *
      * In the foregoing description, the notation
      * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
      * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
      * <tt>0</tt>, or <tt>1</tt> according to whether the value of <i>expression</i>
      * is negative, zero or positive.
      *
      * The implementor must ensure <tt>sgn(x.compareTo(y)) ==
      * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
      * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
      * <tt>y.compareTo(x)</tt> throws an exception.)<p>
      *
      * The implementor must also ensure that the relation is transitive:
      * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
      * <tt>x.compareTo(z)&gt;0</tt>.<p>
      *
      * Finally, the implementer must ensure that <tt>x.compareTo(y)==0</tt>
      * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
      * all <tt>z</tt>.<p>
      *
      * It is strongly recommended, but <i>not</i> strictly required that
      * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
      * class that implements the <tt>Comparable</tt> interface and violates
      * this condition should clearly indicate this fact.  The recommended
      * language is "Note: this class has a natural ordering that is
      * inconsistent with equals."
      *
      * @param   o the Object to be compared.
      * @return  a negative integer, zero, or a positive integer as this object
      *		is less than, equal to, or greater than the specified object.
      *
      * @throws ClassCastException if the specified object's type prevents it
      *         from being compared to this Object.
      */
  @Override
     public int compareTo(Object o) {
       Hydraulique1dLigneBooleensTableau l = (Hydraulique1dLigneBooleensTableau)o;
       if (listeBooleens_.isEmpty()) return 0;
       if ((getValue(0) == null) && (l.getValue(0)==null) ){
         return 0;
       }
       else if (getValue(0) == null) {
         return 1;
       }
       else if (l.getValue(0) == null) {
         return -1;
       }
       else {
         return 0;
       }
     }

     /**
      * Retourne la i�me valeur (double primitif) de la ligne.
      * @param i Indice de la cellule.
      * @return la i�me valeur de la ligne.
      * Si elle est nulle, alors retourne Boolean.POSITIVE_INFINITY.
      */
     public boolean value(int i) {
       return booleanValue(listeBooleens_.get(i));
     }
     /**
      * Retourne la i�me valeur (double non primitif) de la ligne.
      * @param i Indice de la cellule.
      * @return la i�me valeur de la ligne. Elle peut �tre nulle.
      */
     public Boolean getValue(int i) {
       return (Boolean)listeBooleens_.get(i);
     }
     /**
      * Initialise la i�me valeur de la ligne � partir d'un booleen primitif.
      * @param i Indice de la cellule.
      * alors la cellule sera vide.
      */
     public void setValue(int i, boolean valeur) {
       setValue(i,booleanValue(valeur));
     }
     /**
      * Initialise la i�me valeur de la ligne � partir d'un booleen non primitif.
      * @param i Indice de la cellule.
      * @param valeur La nouvelle valeur de la cellule, peut �tre null pour une cellule vide.
      */
     public void setValue(int i, Boolean valeur) {
       listeBooleens_.set(i, valeur);
     }
     /**
      * @return la valeur du premier �lement (booleen non primitif) de la ligne.
      */
     public Boolean X() {
       return (Boolean)listeBooleens_.get(0);
     }
     /**
      * @return la valeur du premier �lement (booleen primitif) de la ligne.
      */
     public boolean x() {
       return value(0);
     }
     /**
      * Initialise le premier �lement de la ligne.
      * @param X La nouvelle valeur du premier �lement (peut �tre null).
      */
     public void X(Boolean X) {
       setValue(0, X);
     }
     /**
      * Initialise le premier �lement de la ligne.
      * @param x La nouvelle valeur du premier �lement.
      */
     public void x(boolean x) {
       setValue(0, x);
     }
     /**
      * @return la valeur du deuxi�me �lement (boolean non primitif) de la ligne.
      */
     public Boolean Y() {
       return (Boolean)listeBooleens_.get(1);
     }
     /**
      * @return la valeur du deuxi�me �lement (boolean primitif) de la ligne.
      */
     public boolean y() {
       return value(1);
     }
     /**
      * Initialise le deuxi�me �lement de la ligne.
      * @param Y La nouvelle valeur du deuxi�me �lement (peut �tre null).
      */
     public void Y(Boolean Y) {
       setValue(1, Y);
     }
     /**
      * Initialise le deuxi�me �lement de la ligne.
      * @param y La nouvelle valeur du deuxi�me �lement.
      */
     public void y(boolean y) {
       setValue(1, y);
     }

     /**
      * R�cup�re les valeurs de la ligne sous forme d'un tableau de booleen primitifs..
      * @return z le tableau des �l�ments de la ligne.
      */
     public boolean[] getTab() {
       boolean[] res = new boolean[listeBooleens_.size()];
       for (int i = 0; i < listeBooleens_.size(); i++) {
         res[i] = value(i);
       }
       return res;
     }
     /**
      * @return le nombre d'�l�ment possible de la ligne.
      */
     public int getTaille() {
       return listeBooleens_.size();
     }
     /**
      * @return Vrai, s'il existe une valeur null (cellule vide), Faux sinon.
      */
     public boolean isExisteNulle() {
       for (int i = 0; i < listeBooleens_.size(); i++) {
         if (listeBooleens_.get(i) == null) return true;
       }
       return false;
     }
     /**
      * @return Vrai, s'il y a que des valeurs null (cellule vide), Faux sinon.
      */
     public boolean isToutNulle() {
       for (int i = 0; i < listeBooleens_.size(); i++) {
         if (listeBooleens_.get(i) != null) return false;
       }
       return true;
     }
     /**
      * Diminue le nombre d'�l�ment de la ligne en supprimant une cellule � l'indexe indiqu�.
      * @param indexe L'indexe de la cellule a supprimmer.
      */
     public void supprimeCellule(int indexe) {
       listeBooleens_.remove(indexe);
     }
     /**
      * Augmente le nombre d'�l�ment de la ligne en ins�rant une cellule vide � l'indexe indiqu�.
      * @param indexe L'indexe de la cellule vide rajout�.
      */
     public void ajoutCelluleVide(int indexe) {
       listeBooleens_.add(indexe, null);
     }
     /**
      * Augmente le nombre d'�l�ment de la ligne en ins�rant une cellule vide � fin.
      */
     public void ajoutCelluleVide() {
       listeBooleens_.add(null);
     }

     /**
      * Efface la ligne en mettant ques des valeurs nulles.
      * Doit �tre surcharg� pour les classes filles.
      */
     public void effaceLigne() {
       for (int i = 0; i < listeBooleens_.size(); i++) {
         listeBooleens_.set(i,null);
       }
     }
     /**
      * @return les diff�rents �l�ments de la ligne s�par�e par un espace.
      */
  @Override
     public String toString() {
       String res="";
       for (int i = 0; i < listeBooleens_.size(); i++) {
         if (listeBooleens_.get(i) == null) {
           res+=" vide ";
         } else {
           res += listeBooleens_.get(i) + ", ";
         }
       }
       res+="\n";
       return res;
     }
     final static boolean booleanValue(Object x) {
       return ((Boolean)x).booleanValue();
     }
     final static Boolean booleanValue(boolean x) {
       //fred
       return Boolean.valueOf(x);
     }
     final static Boolean[] booleanValue(boolean[] x) {
         Boolean[] newC= new Boolean[x.length];
         for (int i = 0; i < x.length; i++) {
         newC[i]=booleanValue(x[i]);
       }
       return newC;
   }

   public boolean[] listeBooleens(){
       return this.getTab();
   }

   public void listeBooleens(boolean[] d){
       for (int i = 0; i < d.length; i++) {
                 this.setValue(i,d[i]);
      }
   }
}
