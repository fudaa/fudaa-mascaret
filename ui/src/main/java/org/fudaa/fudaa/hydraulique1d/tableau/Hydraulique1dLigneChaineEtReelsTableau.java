/*
 * @file         Hydraulique1dLigneChaineEtReelsTableau.java
 * @creation     2004-07-16
 * @modification $Date: 2006-09-08 16:04:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;


/**
 * Definit le mod�le d'une ligne de tableau commen�ant par une chaine puis des r�els.
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.7 $ $Date: 2006-09-08 16:04:25 $ by $Author: opasteur $
 */
public class Hydraulique1dLigneChaineEtReelsTableau extends Hydraulique1dLigneReelTableau {

   /**
    * La chaine en d�but de ligne (peut �tre null).
    */
   private String chaine_;
  /**
   * Constructeur le plus g�n�ral pr�cisant la taille de la ligne.
   * @param taille le nombre de r�els de la ligne.
   * Au d�part les r�els sont initialis�s � null.
   */
  public Hydraulique1dLigneChaineEtReelsTableau(int taille) {
    super(taille);
  }
  /**
   * Constructeur pour une ligne initialis�e par 3 r�els primitifs.
   * @param chaine premier �l�ment de la ligne.
   * @param x r�el deuxi�me �l�ment de la ligne.
   * @param y r�el troisi�me �l�ment de la ligne.
   * @param z r�el quatri�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneChaineEtReelsTableau(String chaine, double x, double y, double z) {
    this(chaine, doubleValue(x), doubleValue(y), doubleValue(z));
  }
  /**
   * Constructeur pour une ligne initialis�e par 3 r�els non primitifs.
   * @param chaine premier �l�ment de la ligne.
   * @param x r�el deuxi�me �l�ment de la ligne.
   * @param y r�el troisi�me �l�ment de la ligne.
   * @param z r�el quatri�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneChaineEtReelsTableau(String chaine , Double x, Double y, Double z) {
    super(x, y, z);
    chaine(chaine);
  }

  /**
 * Constructeur pour une ligne initialis�e par 1 r�el primitif.
 * @param chaine premier �l�ment de la ligne.
 * @param x r�el deuxi�me �l�ment de la ligne.
 */
public Hydraulique1dLigneChaineEtReelsTableau(String chaine, double x) {
  this(chaine, doubleValue(x));
}
/**
 * Constructeur pour une ligne initialis�e par 1 r�el non primitif.
 * @param chaine premier �l�ment de la ligne.
 * @param x r�el deuxi�me �l�ment de la ligne.
 */
public Hydraulique1dLigneChaineEtReelsTableau(String chaine , Double x) {
  super(x);
  chaine(chaine);
}

  /**
   * @return la valeur chaine de caract�re de la ligne (1�re colonne).
   * peut �tre nulle si la cellule est vide.
   */
  public String chaine() {
    return chaine_;
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param chaine La nouvelle valeur enti�re de la ligne (peut �tre nulle).
   */
  public void chaine(String chaine) {
    chaine_=chaine;
  }
  /**
   * Efface la ligne en mettant ques des valeurs nulles.
   * Met la chaine � nulle.
   */
  @Override
  public void effaceLigne() {
    super.effaceLigne();
    chaine(null);
  }
  /**
   * @return le nombre d'�l�ment possible de la ligne.
   */
  @Override
  public int getTaille() {
    return super.getTaille()+1;
  }
  /**
   * @return Vrai, s'il existe une valeur null (cellule vide), Faux sinon.
   */
  @Override
  public boolean isExisteNulle() {
    boolean res = super.isExisteNulle();
    if (res) return true;
    else if (chaine_ == null) return true;
    return false;
  }
  /**
   * @return Vrai, s'il y a que des valeurs null (cellule vide), Faux sinon.
   */
  @Override
  public boolean isToutNulle() {
    boolean res = super.isToutNulle();
    if (!res) return false;
    else if (chaine_ == null) return true;
    return false;
  }
  /**
   * @return les diff�rents �l�ments de la ligne s�par�e par un espace.
   */
  @Override
  public String toString() {
    if (chaine_ == null) {
      return " vide , "+super.toString();
    }
    return chaine_ + ", " + super.toString();
  }

  /**
  * @return les valeurs reelles (type primitif) de la ligne (2eme colonne et au-dela).
  */
 public double[] ListeDoubles() {
     return this.getTab();
 }

 /**
  Initialise les  �lements "double"  de la ligne.
  * @param double[] Les nouvelles valeurs doubles de la ligne.
  */
 public void ListeDoubles(double[] d) {
     for (int i = 0; i < d.length; i++) {
          this.setValue(i,d[i]);
     }
 }
 

}
