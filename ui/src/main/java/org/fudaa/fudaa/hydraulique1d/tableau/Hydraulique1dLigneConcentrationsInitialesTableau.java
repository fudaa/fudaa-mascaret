package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatialBief;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierConcentrationInitiale;

/**
 * Definit le mod�le d'une ligne du tableau repr�sentant les concentrations initiales.
 * @see Hydraulique1dLigne1EntierEtReelsTableau
 * @author Olivier Pasteur
 * @version $Revision: 1.3 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 */
public class Hydraulique1dLigneConcentrationsInitialesTableau extends
        Hydraulique1dLigne1EntierEtReelsTableau {

    /**
     * Constructeur le plus g�n�ral pr�cisant la taille de la ligne.
     * @param taille le nombre de r�els de la ligne.
     * Au d�part les r�els sont initialis�s � null.
     */
    public Hydraulique1dLigneConcentrationsInitialesTableau(int taille) {
      super(taille);
    }
    /**
     * Constructeur pour une ligne initialis�e par 3 r�els primitifs.
     * @param i entier premier �l�ment de la ligne.
     * @param x r�el deuxi�me �l�ment de la ligne.
     * @param y r�el troisi�me �l�ment de la ligne.
     * @param z r�el quatri�me �l�ment de la ligne.
     */
    public Hydraulique1dLigneConcentrationsInitialesTableau(int i, double x, double y, double z) {
      super( i,  x,  y,  z);
    }
    /**
     * Constructeur pour une ligne initialis�e par 3 r�els non primitifs.
     * @param i entier premier �l�ment de la ligne.
     * @param x r�el deuxi�me �l�ment de la ligne.
     * @param y r�el troisi�me �l�ment de la ligne.
     * @param z r�el quatri�me �l�ment de la ligne.
     */
    public Hydraulique1dLigneConcentrationsInitialesTableau(Integer i , Double x, Double y, Double z) {
      super(i,x, y, z);
    }
    /**
   * Constructeur pour des concentrations initiales avec n r�els primitifs.
   * @param i entier premier �l�ment de la ligne.
   * @param c tableau de r�els tous les elements suivants de la ligne.
   */
  public Hydraulique1dLigneConcentrationsInitialesTableau(int i,double[] c) {
     super(i,c);

  }
    /**
   * Constructeur pour des concentrations initiales avec n r�els non primitifs.
   * @param i entier premier �l�ment de la ligne.
   * @param c tableau de r�els tous les elements suivants de la ligne.
   */
  public Hydraulique1dLigneConcentrationsInitialesTableau(Integer i,Double[] c) {
    super(i,c);

  }

  public int iBief() {
  return i();
   }

   public void iBief(int i) {
       super.entier(intValue(i));
   }

   public double absc() {
       return ListeDoubles()[0];
   }

   public void absc(double d) {
   double[] liste = ListeDoubles();
   liste[0]=d;
   this.ListeDoubles(liste);
}


   public double[] concentrations() {
       double[] concsEtAbscisse = this.ListeDoubles();
      double[] newConcs = new double[concsEtAbscisse.length - 1];
      for (int i = 0; i < newConcs.length; i++) {
          newConcs[i] = concsEtAbscisse[i + 1];
      }

     return newConcs;
  }

  public boolean concentrations(double[] newConcs) {
   if (newConcs.length!=(ListeDoubles().length-1)) return false;

   double[] concsEtAbscisse = this.ListeDoubles();
    System.arraycopy(newConcs, 0, concsEtAbscisse, 1, newConcs.length);
  this.ListeDoubles(concsEtAbscisse);
  return true;
}


    public void setDConcentrationInitiale(MetierConcentrationInitiale concInit){
        concInit.numeroBief(iBief());
        concInit.abscisse(absc());
        concInit.concentrations(concentrations());
    }

    /**
      * Conversion des resultats m�tier en un tableau de lignes de concentrations initiales.
      * R�cup�re les r�sultats aus dernier pas de temps.
      * @param ires MetierResultatsTemporelSpatial
      * @return Hydraulique1dLigneConcentrationsInitialesTableau[]
      */
     public final static Hydraulique1dLigneConcentrationsInitialesTableau[] convertitResultatsTemporelSpatial_LigneConcentrationsInitialesTableau(MetierResultatsTemporelSpatial ires,String[] nomConcentrations) {
         if (ires.pasTemps().length == 0)
             return null;
         int iDernierPasTps = ires.pasTemps().length - 1;

         // recherche des indices des variables
         int[] iNomVariable = new int[nomConcentrations.length]; // indices de chaque variable
         for (int i = 0; i < iNomVariable.length; i++) {
             iNomVariable[i] = -1;

         }
         MetierDescriptionVariable[] descrVar = ires.descriptionVariables();
         for (int var = 0; var < iNomVariable.length; var++) {
             for (int i = 0; i < descrVar.length; i++) {
                 if (nomConcentrations[var].equalsIgnoreCase(descrVar[i].nom())) {
                     iNomVariable[var] = i;
                 }
             }
         }

         String varNonTrouvee = "";
         for (int u = 0; u < iNomVariable.length; u++) {
             if (iNomVariable[u] == -1)
                 varNonTrouvee = varNonTrouvee + nomConcentrations[u];
         }
         boolean bContinuer;
         if (varNonTrouvee != "") {
             int choix = JOptionPane.showConfirmDialog(null, "Les concentrations des traceurs "+varNonTrouvee+"  n'apparaissent pas dans le fichier que vous tentez d'importer !\n Souhaitez-vous continuer l'importation ?",
                                                       "Attention !",
                                                       JOptionPane.YES_NO_OPTION,
                                                       JOptionPane.QUESTION_MESSAGE);
             if (choix == JOptionPane.YES_OPTION)
                 bContinuer = true;
             else
                 bContinuer = false;
         } else {
             bContinuer = true;
         }

         if (bContinuer) {

             List lignes = new ArrayList();
             // Parcours des resulats bief par bief
             MetierResultatsTemporelSpatialBief[] iTabResBief = ires.resultatsBiefs();
             for (int i = 0; i < iTabResBief.length; i++) {
                 MetierResultatsTemporelSpatialBief iResBief = iTabResBief[i];
                 double[] xSections = iResBief.abscissesSections();
                 for (int j = 0; j < xSections.length; j++) {
                     Hydraulique1dLigneConcentrationsInitialesTableau lig = new Hydraulique1dLigneConcentrationsInitialesTableau(nomConcentrations.length + 1);
                     lig.iBief(i + 1);
                     lig.absc(xSections[j]);
                     double[][][] valeurs = iResBief.valeursVariables();
                     double[] concs = new double[iNomVariable.length];
                     for (int var = 0; var < iNomVariable.length; var++) {
                         if (iNomVariable[var] != -1) {
                             concs[var] = valeurs[iNomVariable[var]][iDernierPasTps][
                                          j];
                         } else {
                             concs[var] = Double.POSITIVE_INFINITY;
                         }
                     }
                     if (!(lig.concentrations(concs)))
                         return null;
                     lignes.add(lig);
                 }
             }

             return (Hydraulique1dLigneConcentrationsInitialesTableau[]) lignes.
                     toArray(new
                             Hydraulique1dLigneConcentrationsInitialesTableau[lignes.size()]);
         }
              return null;
     }

}
