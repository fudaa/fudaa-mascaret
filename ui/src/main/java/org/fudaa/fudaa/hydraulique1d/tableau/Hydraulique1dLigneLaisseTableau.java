/*
 * @file         Hydraulique1dLigneLaisseTableau.java
 * @creation     2004-07-15
 * @modification $Date: 2007-11-20 11:43:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import org.fudaa.dodico.hydraulique1d.metier.MetierLaisse;
/**
 * Definit le mod�le d'une ligne du tableau repr�sentant une laisse de crue.
 * les colonnes sont : "n� bief", "abscisse", "cote" et "nom".
 * @see org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigne1EntierEtReelsTableau
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.5 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 */
public final class Hydraulique1dLigneLaisseTableau extends Hydraulique1dLigneSiteTableau {
  /**
   * Le nom de la laisse de crue.
   */
  private String nom_;
  /**
   * La laisse de crue m�tier.
   */
  private MetierLaisse laisse_;
  /**
   * Constructeur par d�faut d'une ligne de cellule vide.
   */
  public Hydraulique1dLigneLaisseTableau() {
    super(2);
  }

  protected void setMetier(MetierLaisse laisse) {
    if (laisse == null) return;
    laisse_ = laisse;
    cote(laisse.cote());
    nom(laisse.nom());
    super.setSite(laisse.site());
  }

  protected MetierLaisse getMetier() {
    return laisse_;
  }

  /**
   * @return le nombre d'�l�ment possible de la ligne.
   */
  @Override
  public int getTaille() {
    return super.getTaille()+1;
  }
  /**
   * Efface la ligne en mettant ques des valeurs nulles.
   * Met le nom � null.
   */
  @Override
  public void effaceLigne() {
    super.effaceLigne();
    nom(null);
  }
  /**
   * @return Vrai, s'il existe une valeur null (cellule vide), Faux sinon.
   */
  @Override
  public boolean isExisteNulle() {
    boolean res = super.isExisteNulle();
    if (res) return true;
    else if (nom_ == null) return true;
    return false;
  }
  /**
   * @return Vrai, s'il y a que des valeurs null (cellule vide), Faux sinon.
   */
  @Override
  public boolean isToutNulle() {
    boolean res = super.isToutNulle();
    if (!res) return false;
    else if (nom_ == null) return true;
    return false;
  }
  /**
   * @return les diff�rents �l�ments de la ligne s�par�e par un espace.
   */
  @Override
  public String toString() {
    return enChaine(numBief())+", "+enChaine(new Double(absc()))+", "+
           enChaine(new Double(cote()))+", "+enChaine(nom())+"\n";
  }
  private String enChaine(Object o) {
    if (o==null) {
      return " vide ";
    }
    return o.toString();
  }
  /**
   * Initialise la cote.
   * @param Y La nouvelle valeur de la cote (peut �tre null).
   */
  public void cote(Double Y) {
    super.Y(Y);
  }
  /**
   * @return La valeur de la cote.
   */
  public double cote() {
    return super.y();
  }
  /**
   * Initialise la cote.
   * @param y La nouvelle valeur de la cote.
   */
  public void cote(double y) {
    super.y(y);
  }
  /**
   * Initialise le nom.
   * @param nom La nouvelle valeur du nom (peut �tre null).
   */
  public void nom(String nom) {
    nom_ = nom;
  }
  /**
   * @return La valeur du nom.
   */
  public String nom() {
    return nom_;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Hydraulique1dLigneLaisseTableau) {
      Hydraulique1dLigneLaisseTableau l =(Hydraulique1dLigneLaisseTableau)obj;
      return ( (l.iBief() == iBief()) && (l.absc() == absc())&&
               (l.cote()==cote()) && (l.nom().equals(nom())));
    }
    else if (obj instanceof MetierLaisse) {
      MetierLaisse ilaisse = (MetierLaisse)obj;
      if (ilaisse.cote() != cote()) return false;
      if (!ilaisse.nom().equals(nom())) return false;
      if (ilaisse.site() == null) return false;
      return (super.equals(ilaisse.site()));

    }
    return false;
  }
}
