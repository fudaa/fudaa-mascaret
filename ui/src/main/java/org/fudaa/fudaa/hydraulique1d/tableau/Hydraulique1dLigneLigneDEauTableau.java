/*
 * @file         Hydraulique1dLigneLigneDEauTableau.java
 * @creation     2004-04-01
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.EnumMetierTypeNombre;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierUnite;
import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauPoint;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatialBief;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
/**
 * Definit le mod�le d'une ligne du tableau repr�sentant une ligne d'eau initiale.
 * @see Hydraulique1dLigneReelTableau
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.9 $ $Date: 2007-11-20 11:43:11 $ by $Author: bmarchan $
 */
public final class Hydraulique1dLigneLigneDEauTableau extends Hydraulique1dLigne1EntierEtReelsTableau {
  /**
   * Constructeur par d�faut d'une ligne de cellule vide.
   */
  public Hydraulique1dLigneLigneDEauTableau() {
    super(3);
  }
  /**
   * Constructeur � partir d'une ligne d'eau m�tier.
   * @param iligne La ligne d'eau m�tier qui initialise la ligne du tableau.
   */
  public Hydraulique1dLigneLigneDEauTableau(MetierLigneEauPoint iligne) {
    super(iligne.numeroBief(),iligne.abscisse(),iligne.cote(),iligne.debit());
  }
  /**
   * Constructeur pour une ligne initialis�e par 1 entier primitif et 3 r�els primitifs.
   * @param indiceBief premier �l�ment de la ligne.
   * @param abscisse deuxi�me �l�ment de la ligne.
   * @param cote troisi�me �l�ment de la ligne.
   * @param debit quatri�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneLigneDEauTableau(int indiceBief, double abscisse,
                                            double cote, double debit) {
    super(intValue(indiceBief), doubleValue(abscisse), doubleValue(cote), doubleValue(debit));
  }
  /**
   * Constructeur pour une ligne initialis�e par 1 entier et 3 r�els non primitifs.
   * @param indiceBief premier �l�ment de la ligne.
   * @param abscisse deuxi�me �l�ment de la ligne.
   * @param cote troisi�me �l�ment de la ligne.
   * @param debit quatri�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneLigneDEauTableau(Integer indiceBief, Double abscisse, Double cote, Double debit) {
    super(indiceBief, abscisse, cote, debit);
  }

  /**
   * @return la valeur de l'indice du bief (1�re colonne).
   * peut �tre nulle si la cellule est vide.
   */
  public Integer indiceBief() {
    return entier();
  }
  /**
   * @return la valeur de l'indice du bief (1�re colonne).
   * peut �tre Integer.MAX_VALUE si la cellule est vide.
   */
  public int iBief() {
    return i();
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param indiceBief La nouvelle valeur de l'indice du bief (peut �tre nulle).
   */
  public void indiceBief(Integer indiceBief) {
    super.entier(indiceBief);
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param indiceBief La nouvelle valeur de l'indice du bief.
   */
  public void iBief(int indiceBief) {
    super.i(indiceBief);
  }
  /**
   * Initialise l'abscisse.
   * @param X La nouvelle valeur de l'abscisse (peut �tre null).
   */
  public void absc(Double X) {
    super.X(X);
  }
  /**
   * @return La valeur de l'abcisse.
   */
  public double absc() {
    return super.x();
  }
  /**
   * Initialise l'abscisse.
   * @param x La nouvelle valeur de l'abscisse.
   */
  public void absc(double x) {
    super.x(x);
  }
  /**
   * Initialise la cote.
   * @param cote La nouvelle valeur de la cote (peut �tre null).
   */
  public void cote(Double cote) {
    super.Y(cote);
  }
  /**
   * @return La valeur de la cote.
   */
  public double cote() {
    return super.y();
  }
  /**
   * Initialise la cote.
   * @param cote La nouvelle valeur de la cote.
   */
  public void cote(double cote) {
    super.y(cote);
  }
  /**
   * Initialise le d�bit.
   * @param debit La nouvelle valeur du d�bit (peut �tre null).
   */
  public void debit(Double debit) {
    super.Z(debit);
  }
  /**
   * @return La valeur du d�bit.
   */
  public double debit() {
    return super.z();
  }
  /**
   * Initialise le d�bit.
   * @param debit La nouvelle valeur du d�bit.
   */
  public void debit(double debit) {
    super.z(debit);
  }

  /**
   * Initialise la ligne � partir d'un objet m�tier Corba.
   * @param iligne L'objet m�tier qui initialise la ligne.
   */
  public void setIligneEauPoint(MetierLigneEauPoint iligne) {
    iBief(iligne.numeroBief());
    absc(iligne.abscisse());
    cote(iligne.cote());
    debit(iligne.debit());
  }
  /**
   * Initialise l'objet m�tier Corba � partir de la ligne courante.
   * @param iligne L'objet m�tier qui sera initialis� par la ligne.
   */
  public void getIligneEauPoint(MetierLigneEauPoint iligne) {
    iligne.numeroBief(iBief());
    iligne.abscisse(absc());
    iligne.cote(cote());
    iligne.debit(debit());
  }
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Hydraulique1dLigneLigneDEauTableau) {
      Hydraulique1dLigneLigneDEauTableau l =(Hydraulique1dLigneLigneDEauTableau)obj;
      return ((l.iBief()==iBief())&&(l.absc()==absc())&&
              (l.cote()==cote())&&(l.debit()==debit()));
    } else if (obj instanceof MetierLigneEauPoint){
      MetierLigneEauPoint l = (MetierLigneEauPoint)obj;
      return ((l.numeroBief()==iBief())&&(l.abscisse()==absc())&&
             (l.cote()==cote())&&(l.debit()==debit()));
    }
    return false;
  }

  /**
   * Conversion des resultats m�tier en un tableau de lignes de ligne d'eau initiale.
   * R�cup�re les r�sultats aus dernier pas de temps.
   * @param ires MetierResultatsTemporelSpatial
   * @return Hydraulique1dLigneLigneDEauTableau[]
   */
  public final static Hydraulique1dLigneLigneDEauTableau[] convertitResultatsTemporelSpatial_LigneLigneDEauTableau(MetierResultatsTemporelSpatial ires) {
    if (ires.pasTemps().length ==0) return null;
    int iDernierPasTps = ires.pasTemps().length - 1;

    // recherche des indices des variables
    int iZ   =-1; // indice de la cote
    int iQmin=-1; // indice d�bit mineur
    int iQmaj=-1; // indice d�bit majeur
    int iQ   =-1; // indice d�bit
    MetierDescriptionVariable[] descrVar = ires.descriptionVariables();
    for (int i = 0; i < descrVar.length; i++) {
      if ("Z".equalsIgnoreCase(descrVar[i].nom())) {
        iZ=i;
      }

      if ("QMIN".equalsIgnoreCase(descrVar[i].nom())) {
        iQmin=i;
      }

      if ("QMAJ".equalsIgnoreCase(descrVar[i].nom())) {
        iQmaj=i;
      }
      if ("Q".equalsIgnoreCase(descrVar[i].nom())) {
        iQ=i;
      }
    }

    boolean Qexist = (iQ!=-1);
    List lignes = new ArrayList();
    // Parcours des resulats bief par bief
    MetierResultatsTemporelSpatialBief[] iTabResBief = ires.resultatsBiefs();
    for (int i = 0; i < iTabResBief.length; i++) {
      MetierResultatsTemporelSpatialBief iResBief = iTabResBief[i];
      double[] xSections = iResBief.abscissesSections();
      for (int j = 0; j < xSections.length; j++) {
        Hydraulique1dLigneLigneDEauTableau lig = new Hydraulique1dLigneLigneDEauTableau();
        lig.iBief(i+1);
        lig.absc(xSections[j]);

        double[][][] valeurs = iResBief.valeursVariables();
        lig.cote(valeurs[iZ][iDernierPasTps][j]);
        double Q;
        if (!Qexist) {
          // le d�bit initiale est �gale � la somme du d�bit mineur avec le d�bit majeur
          Q =valeurs[iQmin][iDernierPasTps][j]+valeurs[iQmaj][iDernierPasTps][j];
        } else {
          Q =valeurs[iQ][iDernierPasTps][j];
        }
        lig.debit(Q);

        lignes.add(lig);
      }
    }

    return (Hydraulique1dLigneLigneDEauTableau[])lignes.toArray(new Hydraulique1dLigneLigneDEauTableau[lignes.size()]);
  }

  /**
   * Conversion un tableau de lignes de ligne d'eau initiale en resultats m�tier.
   * @param lignes Hydraulique1dLigneLigneDEauTableau[]
   * @return MetierResultatsTemporelSpatial
   */
  public final static  MetierResultatsTemporelSpatial convertitLigneLigneDEauTableau_ResultatsTemporelSpatial(Hydraulique1dLigneLigneDEauTableau[] lignes) {
    if (lignes == null) return null;
    if (lignes.length == 0) return null;
    Notifieur.getNotifieur().setEventMuet(true);
    MetierResultatsTemporelSpatial ires = new MetierResultatsTemporelSpatial();

    ires.resultatsCasier(false);
    ires.resultatsLiaison(false);
    ires.resultatsCalageAuto(false);
    ires.resultatsTracer(false);
    ires.resultatsPermanent(true);

    // Un seul pas de temps : 0
    double[] pasTps = {0};
    ires.pasTemps(pasTps);
    MetierDescriptionVariable[] descrVar = new MetierDescriptionVariable[2];

    // description des 3 variables
    // description des cotes.
    descrVar[0] = new MetierDescriptionVariable();
    descrVar[0].nom("Z");
    descrVar[0].description("Cote de l eau");
    descrVar[0].nbDecimales(3);
    descrVar[0].type(EnumMetierTypeNombre.REEL);
    descrVar[0].unite(EnumMetierUnite.M);

    // description des d�bit.
    descrVar[1] = new MetierDescriptionVariable();
    descrVar[1].nom("Q");
    descrVar[1].description("D�bit");
    descrVar[1].nbDecimales(3);
    descrVar[1].type(EnumMetierTypeNombre.REEL);
    descrVar[1].unite(EnumMetierUnite.M3_PAR_S);

    ires.descriptionVariables(descrVar);

    List listeIResTempoSpatialBief = new ArrayList();
    MetierResultatsTemporelSpatialBief iresBief= null;
    List listeSection = null;
    List listeValeursZ = null;
    List listeValeursQ = null;
    int iBiefPre = -5;
    for (int i = 0; i < lignes.length; i++) {
      int iBief = lignes[i].iBief();
      if (iBiefPre != iBief) {
        if ((iresBief != null)&&(listeSection != null)&&(listeValeursZ != null)&&(listeValeursQ != null)) {
          iresBief.abscissesSections(liste2Abscisse(listeSection));
          iresBief.valeursVariables(listes2valeurs(listeValeursZ,listeValeursQ));
        }
        listeSection = new ArrayList();
        listeValeursZ = new ArrayList();
        listeValeursQ = new ArrayList();
        iresBief = new MetierResultatsTemporelSpatialBief();
        listeIResTempoSpatialBief.add(iresBief);
      }
      iBiefPre = iBief;
      listeSection.add(lignes[i].getValue(0));
      listeValeursZ.add(lignes[i].getValue(1));
      listeValeursQ.add(lignes[i].getValue(2));
    }
    if ((iresBief != null)&&(listeSection != null)&&(listeValeursZ != null)&&(listeValeursQ != null)) {
      iresBief.abscissesSections(liste2Abscisse(listeSection));
      iresBief.valeursVariables(listes2valeurs(listeValeursZ,listeValeursQ));
    }
    ires.resultatsBiefs((MetierResultatsTemporelSpatialBief[])
       listeIResTempoSpatialBief.toArray(new MetierResultatsTemporelSpatialBief[listeIResTempoSpatialBief.size()]));
    Notifieur.getNotifieur().setEventMuet(false);
    return ires;
  }

  private final static double[] liste2Abscisse(List listeSection) {
    int taille = listeSection.size();
    double[] absc = new double[taille];
    for (int i = 0; i < absc.length; i++) {
      absc[i] = ((Double)listeSection.get(i)).doubleValue();
    }
    return absc;
  }
  private final static double[][][] listes2valeurs(List listeValeursZ, List listeValeursQ) {
    int taille = listeValeursZ.size();
    double[][][] vals = new double[2][1][taille];
    for (int i = 0; i < taille; i++) {
      vals[0][0][i] = ((Double)listeValeursZ.get(i)).doubleValue();
      vals[1][0][i] = ((Double)listeValeursQ.get(i)).doubleValue();
    }
    return vals;
  }
}
