package org.fudaa.fudaa.hydraulique1d.tableau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierOptionTraceur;
/**
 * Definit le mod�le d'une ligne du tableau repr�sentant les options d'un traceur.
 * @author Olivier Pasteur
 * @version 1.0
 */
public class Hydraulique1dLigneOptionsTraceurTableau extends
        Hydraulique1dLigneChaineEtBooleensTableau {

    /**
     * Constructeur le plus g�n�ral pr�cisant la taille de la ligne.
     * @param taille le nombre de bool�ens de la ligne.
     * Au d�part les bool�ens sont initialis�s � null.
     */
    public Hydraulique1dLigneOptionsTraceurTableau(int taille) {
      super(taille);
    }
    /**
     * Constructeur pour une ligne initialis�e par 3 bool�ens primitifs.
     * @param i entier premier �l�ment de la ligne.
     * @param x bool�en deuxi�me �l�ment de la ligne.
     * @param y bool�en troisi�me �l�ment de la ligne.
     */
    public Hydraulique1dLigneOptionsTraceurTableau(String i, boolean x, boolean y) {
      super( i,  x,  y);
    }
    /**
     * Constructeur pour une ligne initialis�e par 3 bool�ens non primitifs.
     * @param i entier premier �l�ment de la ligne.
     * @param x bool�en deuxi�me �l�ment de la ligne.
     * @param y bool�en troisi�me �l�ment de la ligne.
     */
    public Hydraulique1dLigneOptionsTraceurTableau(String i , Boolean x, Boolean y) {
      super(i,x, y);
    }



    public boolean diffusionDuTraceur() {
         return listeBooleens()[0];
     }

     public void diffusionDuTraceur(boolean d) {
     boolean[] liste = listeBooleens();
     liste[0]=d;
     this.listeBooleens(liste);
  }


   public boolean convectionDuTraceur() {
       return listeBooleens()[1];
   }

   public void convectionDuTraceur(boolean d) {
   boolean[] liste = listeBooleens();
   liste[1]=d;
   this.listeBooleens(liste);
}




    public void setIOptionTracer(MetierOptionTraceur optionTraceur){
        optionTraceur.convectionDuTraceur(convectionDuTraceur());
        optionTraceur.diffusionDuTraceur(diffusionDuTraceur());
    }



}
