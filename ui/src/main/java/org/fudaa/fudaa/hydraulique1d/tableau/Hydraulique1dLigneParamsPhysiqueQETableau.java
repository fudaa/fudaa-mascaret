package org.fudaa.fudaa.hydraulique1d.tableau;

import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParamPhysTracer;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Definit le mod�le d'une ligne du tableau repr�sentant les param�tres physiques.
 * @see Hydraulique1dLigne1ChaineEtReelsTableau
 * @author Olivier Pasteur
 * @version $Revision: 1.0
 */

public class Hydraulique1dLigneParamsPhysiqueQETableau  extends
        Hydraulique1dLigneChaineEtReelsTableau {

      private String nomPasTraduit_="";

    /**
     * Constructeur le plus g�n�ral pr�cisant la taille de la ligne.
     * @param taille le nombre de r�els de la ligne.
     * Au d�part les r�els sont initialis�s � null.
     */
    public Hydraulique1dLigneParamsPhysiqueQETableau(int taille) {
      super(taille);
    }
    /**
     * Constructeur pour une ligne initialis�e par 1 r�el primitif.
     * @param i chaine premier �l�ment de la ligne.
     * @param x r�el deuxi�me �l�ment de la ligne.

     */
    public Hydraulique1dLigneParamsPhysiqueQETableau(String i, double x) {
      super( i,  x);
      nomPasTraduit_ = i;
      nom(Hydraulique1dResource.HYDRAULIQUE1D.getString(i));
    }
    /**
     * Constructeur pour une ligne initialis�e par 1 r�el non primitif.
     * @param i chaine premier �l�ment de la ligne.
     * @param x r�el deuxi�me �l�ment de la ligne.
     */
    public Hydraulique1dLigneParamsPhysiqueQETableau(String i , Double x) {
      super(i,x);
      nomPasTraduit_ = i;
      nom(Hydraulique1dResource.HYDRAULIQUE1D.getString(i));
    }
    /**
   * Constructeur pour des concentrations initiales avec n r�els primitifs.
   * @param i entier premier �l�ment de la ligne.
   * @param c tableau de r�els tous les elements suivants de la ligne.
   */

  public String nomPasTraduit() {
    return nomPasTraduit_;
  }
  public void nomPasTraduit(String nomPasTraduit) {
    nomPasTraduit_ =nomPasTraduit;
  }
  public String nom() {
  return chaine();
   }

   public void nom(String n) {
       super.chaine(n);
   }

   public double valeur() {
       return ListeDoubles()[0];
   }

   public void valeur(double d) {
   double[] liste = ListeDoubles();
   liste[0]=d;
   this.ListeDoubles(liste);
}

    public void setDParamPhysTracer(MetierParamPhysTracer params){
        params.nomParamPhys(nomPasTraduit());
        params.valeurParamPhys(valeur());
    }

  @Override
    public boolean equals(Object obj) {
        if (obj instanceof Hydraulique1dLigneParamsPhysiqueQETableau) {
        	Hydraulique1dLigneParamsPhysiqueQETableau l =(Hydraulique1dLigneParamsPhysiqueQETableau)obj;
          return (l.nomPasTraduit()==nomPasTraduit())&&(l.valeur()==valeur());
        }
        else if (obj instanceof MetierParamPhysTracer) {
        	MetierParamPhysTracer iparam = (MetierParamPhysTracer)obj;
          return (iparam.nomParamPhys()==nomPasTraduit())&&(iparam.valeurParamPhys()==valeur());
        }
        return false;
      }

  }
