/*
 * @file         Hydraulique1dLigneProfilTableau.java
 * @creation     2004-07-16
 * @modification $Date: 2007-11-20 11:43:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2005 EDF/OPP
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;

/**
 * Definit le mod�le d'une ligne de tableau repr�sentant un profil (nom + abscisse).
 * Utilis� dans l'�diteur de Bief.
 * @see org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel
 * @see org.fudaa.dodico.hydraulique1d.metier.MetierProfil
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.8 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 */
public class Hydraulique1dLigneProfilTableau extends Hydraulique1dLigneChaineEtReelsTableau {
  private Hydraulique1dProfilModel profilModel_;
  public Hydraulique1dLigneProfilTableau() {
    this(null);
  }

  /**
   * Constructeur d'une ligne � partir d'un profil.
   * @param profil Hydraulique1dProfilModel
   */
  public Hydraulique1dLigneProfilTableau(Hydraulique1dProfilModel profil) {
    super(1);
    profilModel_ = profil;
    if (profilModel_ != null) {
      nom(profilModel_.nom());
      abscisse(profilModel_.getAbscisse());
    } else {
      profilModel_ = new Hydraulique1dProfilModel();
      profilModel_.nom(null);
      profilModel_.setAbscisse(Double.POSITIVE_INFINITY);
      nom(null);
      abscisse(null);
    }
  }

  /**
   * Retourne de mod�le de profil.
   * @return Hydraulique1dProfilModel
   */
  public Hydraulique1dProfilModel profilModel() {
    return profilModel_;
  }

  /**
   * Retourne le nom du profil.
   * @return String
   */
  public String nom() {
    return super.chaine();
  }

  
  /**
   * Retourne avecGeoReferencement.
   * @return boolean
   */
  public boolean avecGeoReferencement() {
    return profilModel_.avecGeoreferencement();
  }
  
  /**
   * Retourne infoGeoreferencement.
   * @return String
   */
  public String  infoGeoreferencement() {
    return profilModel_.infoGeoreferencement();
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param chaine La nouvelle valeur enti�re de la ligne (peut �tre nulle).
   */
  @Override
  public void chaine(String chaine) {
    super.chaine(chaine);
    profilModel_.nom(chaine);
  }

  /**
   * Modifie le nom du profil.
   * @param nom String
   */
  public void nom(String nom) {
    chaine(nom);
  }

  public double abscisse() {
    return super.value(0);
  }

  @Override
  public void setValue(int i, Double value) {
    super.setValue(i, value);
    if (i==0) {
      if (value == null) profilModel_.setAbscisse(Double.POSITIVE_INFINITY);
      else profilModel_.setAbscisse(value.doubleValue());
    }
  }
  public void abscisse(double abscisse) {
    super.setValue(0,abscisse);
  }

  public void abscisse(Double abscisse) {
    super.setValue(0,abscisse);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Hydraulique1dLigneProfilTableau) {
      Hydraulique1dLigneProfilTableau l =(Hydraulique1dLigneProfilTableau)obj;
      return ( (l.nom() == nom()) && (l.abscisse() == abscisse())
               && l.profilModel().equals(profilModel_));
    }
    else if (obj instanceof MetierProfil) {
      MetierProfil iprofil = (MetierProfil)obj;
      if (iprofil.abscisse() != abscisse()) return false;
      if (iprofil.avecGeoreferencement() != avecGeoReferencement()) return false;
      if ((iprofil.nom() == null)&&(nom()!=null)) return false;
      if ((iprofil.nom() != null)&&(nom()==null)) return false;
      if (!iprofil.nom().equals(nom())) return false;
      if (!profilModel_.equals(iprofil)) return false;
      return true;

    }
    else if (obj instanceof Hydraulique1dProfilModel) {
      Hydraulique1dProfilModel prof = (Hydraulique1dProfilModel)obj;
      return profilModel_.equals(prof);
    }
    return false;
  }

}
