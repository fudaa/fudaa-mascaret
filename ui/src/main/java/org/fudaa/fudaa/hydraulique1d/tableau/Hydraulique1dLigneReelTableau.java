/*
 * @file         Hydraulique1dLigneReelTableau.java
 * @creation     2003-11-28
 * @modification $Date: 2006-09-08 16:04:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import java.util.ArrayList;
import java.util.List;
/**
 * Definit le mod�le d'une ligne de tableau de r�els.
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.14 $ $Date: 2006-09-08 16:04:26 $ by $Author: opasteur $
 */
public class Hydraulique1dLigneReelTableau implements Comparable {
  /**
   * le tableau contenant les diff�rents reels de la ligne
   */
  private List listeDouble_;
  /**
   * Constructeur le plus g�n�ral pr�cisant la taille de la ligne.
   * @param taille le nombre de r�el de la ligne.
   * Au d�part les r�els sont initialis�s � null.
   */
  public Hydraulique1dLigneReelTableau(int taille) {
    if (taille>=0) {
      listeDouble_ = new ArrayList(taille);
      for (int i = 0; i < taille; i++) {
        listeDouble_.add(null);
      }
    } else {
      throw new IllegalStateException("Taille de la ligne incorrecte : "+taille);
    }
  }
  /**
   * Constructeur pour une ligne initialis�e par 3 r�els primitifs.
   * @param x premier �l�ment de la ligne.
   * @param y deuxi�me �l�ment de la ligne.
   * @param z troisi�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneReelTableau(double x, double y, double z) {
    this(doubleValue(x), doubleValue(y), doubleValue(z));
  }
  /**
   * Constructeur pour une ligne initialis�e par 3 r�els non primitifs.
   * @param x premier �l�ment de la ligne, peut �tre null (cellule vide).
   * @param y deuxi�me �l�ment de la ligne, peut �tre null (cellule vide).
   * @param z troisi�me �l�ment de la ligne, peut �tre null (cellule vide).
   */
  public Hydraulique1dLigneReelTableau(Double x, Double y, Double z) {
    this(3);
    listeDouble_.set(0,x);
    listeDouble_.set(1,y);
    listeDouble_.set(2,z);
  }

  /**
   * Constructeur pour une ligne initialis�e par 1 r�el non primitif.
   * @param x premier �l�ment de la ligne, peut �tre null (cellule vide).
   */
  public Hydraulique1dLigneReelTableau(Double x) {
    this(1);
    listeDouble_.set(0,x);
  }

  /**
 * Constructeur pour une ligne initialis�e par n r�els primitifs.
 * @param c �l�ments de la ligne.
 */
public Hydraulique1dLigneReelTableau(double[] c) {

    this(doubleValue(c));
}

  /**
   * Constructeur pour une ligne initialis�e par n r�els non primitifs.
   * @param c  �l�ments de la ligne, peut �tre null (cellule vide).
   */
  public Hydraulique1dLigneReelTableau(Double[] c) {
    this(c.length);
    for (int i = 0; i < c.length; i++) {
        listeDouble_.set(i,c[i]);
    }

  }
  /**
   * Compares this object with the specified object for order.  Returns a
   * negative integer, zero, or a positive integer as this object is less
   * than, equal to, or greater than the specified object.<p>
   *
   * In the foregoing description, the notation
   * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
   * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
   * <tt>0</tt>, or <tt>1</tt> according to whether the value of <i>expression</i>
   * is negative, zero or positive.
   *
   * The implementor must ensure <tt>sgn(x.compareTo(y)) ==
   * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
   * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
   * <tt>y.compareTo(x)</tt> throws an exception.)<p>
   *
   * The implementor must also ensure that the relation is transitive:
   * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
   * <tt>x.compareTo(z)&gt;0</tt>.<p>
   *
   * Finally, the implementer must ensure that <tt>x.compareTo(y)==0</tt>
   * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
   * all <tt>z</tt>.<p>
   *
   * It is strongly recommended, but <i>not</i> strictly required that
   * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
   * class that implements the <tt>Comparable</tt> interface and violates
   * this condition should clearly indicate this fact.  The recommended
   * language is "Note: this class has a natural ordering that is
   * inconsistent with equals."
   *
   * @param   o the Object to be compared.
   * @return  a negative integer, zero, or a positive integer as this object
   *		is less than, equal to, or greater than the specified object.
   *
   * @throws ClassCastException if the specified object's type prevents it
   *         from being compared to this Object.
   */
  @Override
  public int compareTo(Object o) {
    Hydraulique1dLigneReelTableau l = (Hydraulique1dLigneReelTableau)o;
    if (listeDouble_.isEmpty()) return 0;
    if ((getValue(0) == null) && (l.getValue(0)==null) ){
      return 0;
    }
    else if (getValue(0) == null) {
      return 1;
    }
    else if (l.getValue(0) == null) {
      return -1;
    }
    else {
      return getValue(0).compareTo(l.getValue(0));
    }
  }

  /**
   * Retourne la i�me valeur (double primitif) de la ligne.
   * @param i Indice de la cellule.
   * @return la i�me valeur de la ligne.
   * Si elle est nulle, alors retourne Double.POSITIVE_INFINITY.
   */
  public double value(int i) {
    return doubleValue(listeDouble_.get(i));
  }
  /**
   * Retourne la i�me valeur (double non primitif) de la ligne.
   * @param i Indice de la cellule.
   * @return la i�me valeur de la ligne. Elle peut �tre nulle.
   */
  public Double getValue(int i) {
    return (Double)listeDouble_.get(i);
  }
  /**
   * Initialise la i�me valeur de la ligne � partir d'un double primitif.
   * @param i Indice de la cellule.
   * @param valeur La nouvelle valeur de la cellule, si �gale � Double.POSITIVE_INFINITY
   * alors la cellule sera vide.
   */
  public void setValue(int i, double valeur) {
    setValue(i,doubleValue(valeur));
  }
  /**
   * Initialise la i�me valeur de la ligne � partir d'un double non primitif.
   * @param i Indice de la cellule.
   * @param valeur La nouvelle valeur de la cellule, peut �tre null pour une cellule vide.
   */
  public void setValue(int i, Double valeur) {
    listeDouble_.set(i, valeur);
  }
  /**
   * @return la valeur du premier �lement (double non primitif) de la ligne.
   */
  public Double X() {
    return (Double)listeDouble_.get(0);
  }
  /**
   * @return la valeur du premier �lement (double primitif) de la ligne.
   */
  public double x() {
    return value(0);
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param X La nouvelle valeur du premier �lement (peut �tre null).
   */
  public void X(Double X) {
    setValue(0, X);
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param x La nouvelle valeur du premier �lement.
   */
  public void x(double x) {
    setValue(0, x);
  }
  /**
   * @return la valeur du deuxi�me �lement (double non primitif) de la ligne.
   */
  public Double Y() {
    return (Double)listeDouble_.get(1);
  }
  /**
   * @return la valeur du deuxi�me �lement (double primitif) de la ligne.
   */
  public double y() {
    return value(1);
  }
  /**
   * Initialise le deuxi�me �lement de la ligne.
   * @param Y La nouvelle valeur du deuxi�me �lement (peut �tre null).
   */
  public void Y(Double Y) {
    setValue(1, Y);
  }
  /**
   * Initialise le deuxi�me �lement de la ligne.
   * @param y La nouvelle valeur du deuxi�me �lement.
   */
  public void y(double y) {
    setValue(1, y);
  }
  /**
   * @return la valeur du troisi�me �lement (double non primitif) de la ligne.
   */
  public Double Z() {
    return (Double)listeDouble_.get(2);
  }
  /**
   * @return la valeur du troisi�me �lement (double primitif) de la ligne.
   */
  public double z() {
    return value(2);
  }
  /**
   * Initialise le troisi�me �lement de la ligne.
   * @param Z La nouvelle valeur du deuxi�me �lement (peut �tre null).
   */
  public void Z(Double Z) {
    setValue(2, Z);
  }
  /**
   * Initialise le troisi�me �lement de la ligne.
   * @param z La nouvelle valeur du deuxi�me �lement.
   */
  public void z(double z) {
    setValue(2, z);
  }
  /**
   * R�cup�re les valeurs de la ligne sous forme d'un tableau de doubles primitifs..
   * @return z le tableau des �l�ments de la ligne.
   */
  public double[] getTab() {
    double[] res = new double[listeDouble_.size()];
    for (int i = 0; i < listeDouble_.size(); i++) {
      res[i] = value(i);
    }
    return res;
  }
  /**
   * @return le nombre d'�l�ment possible de la ligne.
   */
  public int getTaille() {
    return listeDouble_.size();
  }
  /**
   * @return Vrai, s'il existe une valeur null (cellule vide), Faux sinon.
   */
  public boolean isExisteNulle() {
    for (int i = 0; i < listeDouble_.size(); i++) {
      if (listeDouble_.get(i) == null) return true;
    }
    return false;
  }
  
  public boolean isExisteNulle(int size) {
	    for (int i = 0; i < size; i++) {
	      if (listeDouble_.get(i) == null) return true;
	    }
	    return false;
	  }
  
  /**
   * @return Vrai, s'il y a que des valeurs null (cellule vide), Faux sinon.
   */
  public boolean isToutNulle() {
    for (int i = 0; i < listeDouble_.size(); i++) {
      if (listeDouble_.get(i) != null) return false;
    }
    return true;
  }
  /**
   * Diminue le nombre d'�l�ment de la ligne en supprimant une cellule � l'indexe indiqu�.
   * @param indexe L'indexe de la cellule a supprimmer.
   */
  public void supprimeCellule(int indexe) {
    listeDouble_.remove(indexe);
  }
  /**
   * Augmente le nombre d'�l�ment de la ligne en ins�rant une cellule vide � l'indexe indiqu�.
   * @param indexe L'indexe de la cellule vide rajout�.
   */
  public void ajoutCelluleVide(int indexe) {
    listeDouble_.add(indexe, null);
  }
  /**
   * Augmente le nombre d'�l�ment de la ligne en ins�rant une cellule vide � fin.
   */
  public void ajoutCelluleVide() {
    listeDouble_.add(null);
  }

  /**
   * Efface la ligne en mettant ques des valeurs nulles.
   * Doit �tre surcharg� pour les classes filles.
   */
  public void effaceLigne() {
    for (int i = 0; i < listeDouble_.size(); i++) {
      listeDouble_.set(i,null);
    }
  }
  /**
   * @return les diff�rents �l�ments de la ligne s�par�e par un espace.
   */
  @Override
  public String toString() {
    String res="";
    for (int i = 0; i < listeDouble_.size(); i++) {
      if (listeDouble_.get(i) == null) {
        res+=" vide ";
      } else {
        res += listeDouble_.get(i) + ", ";
      }
    }
    res+="\n";
    return res;
  }
  final static double doubleValue(Object x) {
    if (x == null) return Double.POSITIVE_INFINITY;
    return ((Double)x).doubleValue();
  }
  final static Double doubleValue(double x) {
    if (x == Double.POSITIVE_INFINITY) return null;
    return new Double(x);
  }
  final static Double[] doubleValue(double[] x) {
      Double[] newC= new Double[x.length];
      for (int i = 0; i < x.length; i++) {
      newC[i]=doubleValue(x[i]);
    }
    return newC;
}


}
