/*
 * @file         Hydraulique1dLigneSiteTableau.java
 * @creation     2004-07-05
 * @modification $Date: 2007-11-20 11:43:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import org.fudaa.dodico.hydraulique1d.metier.MetierSite;
/**
 * Definit le mod�le d'une ligne du tableau repr�sentant une ligne d'eau initiale.
 * @see Hydraulique1dLigneReelTableau
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.3 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 */
public class Hydraulique1dLigneSiteTableau extends Hydraulique1dLigne1EntierEtReelsTableau {
  /**
   * Constructeur par d�faut d'une ligne de cellule vide.
   */
  public Hydraulique1dLigneSiteTableau() {
    super(1);
  }

  /**
   * Constructeur pour une ligne initialis�e par 1 entier primitif et 1 r�el primitif.
   * @param numBief premier �l�ment de la ligne.
   * @param abscisse deuxi�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneSiteTableau(int numBief, double abscisse) {
    this(intValue(numBief), doubleValue(abscisse));
  }
  /**
   * Constructeur pour une ligne initialis�e par 1 entier et 1 r�el non primitif.
   * @param numBief premier �l�ment de la ligne.
   * @param abscisse deuxi�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneSiteTableau(Integer numBief, Double abscisse) {
    super(1);
    entier(numBief);
    X(abscisse);
  }

  /**
   * Constructeur pour des classes filles contenant d'autres colonnes r�els.
   */
  public Hydraulique1dLigneSiteTableau(int nbReel) {
    super(nbReel);
    if (nbReel < 1) throw new IllegalArgumentException("nb r�el trop faible ! nbReel="+nbReel);
  }

  public void setSite(MetierSite site) {
    if (site == null) return;
    absc(site.abscisse());
    if (site.biefRattache() != null) {
      iBief(site.biefRattache().indice()+1);
    }
  }
  /**
   * @return la valeur du num�ro du bief (1�re colonne).
   * peut �tre nulle si la cellule est vide.
   */
  public Integer numBief() {
    return entier();
  }
  /**
   * @return la valeur du num�ro du bief (1�re colonne).
   * peut �tre Integer.MAX_VALUE si la cellule est vide.
   */
  public int iBief() {
    return i();
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param numBief La nouvelle valeur du num�ro du bief (peut �tre nulle).
   */
  public void numBief(Integer numBief) {
    super.entier(numBief);
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param numBief La nouvelle valeur du n� du bief.
   */
  public void iBief(int numBief) {
    super.i(numBief);
  }
  /**
   * Initialise l'abscisse.
   * @param X La nouvelle valeur de l'abscisse (peut �tre null).
   */
  public void absc(Double X) {
    super.X(X);
  }
  /**
   * @return La valeur de l'abcisse.
   */
  public double absc() {
    return super.x();
  }
  /**
   * Initialise l'abscisse.
   * @param x La nouvelle valeur de l'abscisse.
   */
  public void absc(double x) {
    super.x(x);
  }
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Hydraulique1dLigneSiteTableau) {
      Hydraulique1dLigneSiteTableau l =(Hydraulique1dLigneSiteTableau)obj;
      return ((l.iBief()==iBief())&&(l.absc()==absc()));
    }
    else if (obj instanceof MetierSite) {
      MetierSite isite = (MetierSite)obj;
      if (isite.abscisse() != absc()) return false;
      if (isite.biefRattache() == null) return false;
      return ((isite.biefRattache().indice()+1)==iBief());
    }
    return false;
  }
}
