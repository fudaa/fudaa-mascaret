/*
 * @file         Hydraulique1dLigneZoneFrottementTableau.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:43:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;
/**
 * Definit le mod�le d'une ligne du tableau repr�sentant une ligne d'eau initiale.
 * @see Hydraulique1dLigneReelTableau
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.3 $ $Date: 2007-11-20 11:43:09 $ by $Author: bmarchan $
 */
public final class Hydraulique1dLigneZoneFrottementTableau extends Hydraulique1dLigneZoneTableau {
  /**
   * Constructeur par d�faut d'une ligne de cellule vide.
   */
  public Hydraulique1dLigneZoneFrottementTableau() {
    super(4);
  }

  /**
   * Constructeur pour une ligne initialis�e par 1 entier primitif et 1 r�el primitif.
   * @param numBief premier �l�ment de la ligne.
   * @param abscDebut deuxi�me �l�ment de la ligne.
   * @param abscFin troisi�me �l�ment de la ligne.
   * @param coefMin quatri�me �l�ment de la ligne.
   * @param coefMaj cinqui�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneZoneFrottementTableau(int numBief, double abscDebut, double abscFin, double coefMin, double coefMaj) {
    this(intValue(numBief), doubleValue(abscDebut), doubleValue(abscFin), doubleValue(coefMin), doubleValue(coefMaj));
  }
  /**
   * Constructeur pour une ligne initialis�e par 1 entier et 1 r�el non primitif.
   * @param numBief premier �l�ment de la ligne.
   * @param abscDebut deuxi�me �l�ment de la ligne.
   * @param abscFin troisi�me �l�ment de la ligne.
   * @param coefMin quatri�me �l�ment de la ligne.
   * @param coefMaj cinqui�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneZoneFrottementTableau(Integer numBief, Double abscDebut, Double abscFin, Double coefMin, Double coefMaj) {
    super(4);
    entier(numBief);
    abscDebut(abscDebut);
    abscFin(abscFin);
    coefMin(coefMin);
    coefMaj(coefMaj);
  }

  public void setMetier(MetierZoneFrottement zone) {
    if (zone == null) return;
    super.setMetier(zone);
    coefMaj(zone.coefMajeur());
    coefMin(zone.coefMineur());
  }

  /**
   * Initialise le coefficient de frottement du lit mineur.
   * @param coefMin La nouvelle valeur du coefficient de frottement du lit mineur (peut �tre null).
   */
  public void coefMin(Double coefMin) {
    super.Z(coefMin);
  }
  /**
   * @return La valeur du coefficient de frottement du lit mineur.
   */
  public double coefMin() {
    return super.z();
  }
  /**
   * Initialise le coefficient de frottement du lit mineur.
   * @param coefMin La nouvelle valeur du coefficient de frottement du lit mineur.
   */
  public void coefMin(double coefMin) {
    super.z(coefMin);
  }
  /**
   * Initialise le coefficient de frottement du lit majeur.
   * @param coefMaj La nouvelle valeur du coefficient de frottement du lit majeur (peut �tre null).
   */
  public void coefMaj(Double coefMaj) {
    super.setValue(3, coefMaj);
  }
  /**
   * @return La valeur du coefficient de frottement du lit majeur.
   */
  public double coefMaj() {
    return super.value(3);
  }
  /**
   * Initialise le coefficient de frottement du lit majeur.
   * @param coefMaj La nouvelle valeur du coefficient de frottement du lit majeur.
   */
  public void coefMaj(double coefMaj) {
    super.setValue(3, coefMaj);
  }
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Hydraulique1dLigneZoneFrottementTableau) {
      Hydraulique1dLigneZoneFrottementTableau l =(Hydraulique1dLigneZoneFrottementTableau)obj;
      return ( super.equals(l) &&(l.coefMin()==coefMin())&&(l.coefMaj()==coefMaj()));
    }
    else if (obj instanceof MetierZoneFrottement) {
      MetierZoneFrottement izone = (MetierZoneFrottement)obj;
      return (super.equals(izone) &&(izone.coefMineur()==coefMin())&&(izone.coefMajeur()==coefMaj()));
    }
    return false;
  }
}
