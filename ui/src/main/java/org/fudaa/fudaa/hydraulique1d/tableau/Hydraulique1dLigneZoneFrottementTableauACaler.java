package org.fudaa.fudaa.hydraulique1d.tableau;

import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;

/**
 * mod�le d'une ligne � caler, qui reprend des param�tres d'une ligne de zone de frotttement avec en + 4 coefficients.
 * @author Adrien Hadoux
 */
public final class Hydraulique1dLigneZoneFrottementTableauACaler extends Hydraulique1dLigneZoneTableau {
	  /**
	   * Constructeur par d�faut d'une ligne de cellule vide.
	   */
	  public Hydraulique1dLigneZoneFrottementTableauACaler() {
	    super(8);
	  }

	 

	  public void setMetier(MetierZoneFrottement zone) {
	    if (zone == null) return;
	    super.setMetier(zone);
	    coefMaj(zone.coefMajeur());
	    coefMin(zone.coefMineur());
	    coefLitMajBinf(zone.coefLitMajBinf());
	    coefLitMajBsup(zone.coefLitMajBsup());
	    coefLitMinBinf(zone.coefLitMinBinf());
	    coefLitMinBsup(zone.coefLitMinBsup());
	  }

	  /**
	   * Initialise le coefficient de frottement du lit mineur.
	   * @param coefMin La nouvelle valeur du coefficient de frottement du lit mineur (peut �tre null).
	   */
	  public void coefMin(Double coefMin) {
	    super.Z(coefMin);
	  }
	  /**
	   * @return La valeur du coefficient de frottement du lit mineur.
	   */
	  public double coefMin() {
	    return super.z();
	  }
	  /**
	   * Initialise le coefficient de frottement du lit mineur.
	   * @param coefMin La nouvelle valeur du coefficient de frottement du lit mineur.
	   */
	  public void coefMin(double coefMin) {
	    super.z(coefMin);
	  }
	  /**
	   * Initialise le coefficient de frottement du lit majeur.
	   * @param coefMaj La nouvelle valeur du coefficient de frottement du lit majeur (peut �tre null).
	   */
	  public void coefMaj(Double coefMaj) {
	    super.setValue(3, coefMaj);
	  }
	  /**
	   * @return La valeur du coefficient de frottement du lit majeur.
	   */
	  public double coefMaj() {
	    return super.value(3);
	  }
	  /**
	   * Initialise le coefficient de frottement du lit majeur.
	   * @param coefMaj La nouvelle valeur du coefficient de frottement du lit majeur.
	   */
	  public void coefMaj(double coefMaj) {
	    super.setValue(3, coefMaj);
	  }
	  	  	  
	  public double coefLitMinBinf() {	
		  if(super.getValue(4) == null)
			  return 0;
	    return super.value(4);
	  }
	  public void coefLitMinBinf(double coeff) {
		  if(coeff ==0)
			  super.setValue(4, null);
		  else
		  super.setValue(4, coeff);
	  }
	  
	  public double coefLitMinBsup() {
		  if(super.getValue(5) == null)
			  return 0;
		  return super.value(5);
	  }
	  public void coefLitMinBsup(double coeff) {
		  if(coeff ==0)
			  super.setValue(5, null);
		  else
		  super.setValue(5, coeff);
	  }
	  
	  public double coefLitMajBinf() {
		  if(super.getValue(6) == null)
			  return 0;
		  
		  return super.value(6);
	  }
	  public void coefLitMajBinf(double coeff) {
		  if(coeff ==0)
			  super.setValue(6, null);
		  else
		  super.setValue(6, coeff);
	  }
	  
	  public double coefLitMajBsup() {
		  if(super.getValue(7) == null)
			  return 0;
		  return super.value(7);
	  }
	  public void coefLitMajBsup(double coeff) {
		  if(coeff ==0)
			  super.setValue(7, null);
		  else
		  super.setValue(7, coeff);
	  }
	  
	  
	  
	  
	  @Override
	  public boolean equals(Object obj) {
	    if (obj instanceof Hydraulique1dLigneZoneFrottementTableauACaler) {
	      Hydraulique1dLigneZoneFrottementTableauACaler l =(Hydraulique1dLigneZoneFrottementTableauACaler)obj;
	      return ( super.equals(l) &&(l.coefMin()==coefMin())&&(l.coefMaj()==coefMaj())
	    		  &&(l.coefLitMajBinf()==coefLitMajBinf()) &&(l.coefLitMajBsup()==coefLitMajBsup())
	    		  &&(l.coefLitMinBinf()==coefLitMinBinf())&&(l.coefLitMinBsup()==coefLitMinBsup())
	    		  );
	    }
	    else if (obj instanceof MetierZoneFrottement) {
	      MetierZoneFrottement izone = (MetierZoneFrottement)obj;
	      return (super.equals(izone) &&(izone.coefMineur()==coefMin())&&(izone.coefMajeur()==coefMaj())
	    		  &&(izone.coefLitMajBinf()==coefLitMajBinf()) &&(izone.coefLitMajBsup()==coefLitMajBsup())
	    		  &&(izone.coefLitMinBinf()==coefLitMinBinf())&&(izone.coefLitMinBsup()==coefLitMinBsup())
	    		  );
	    }
	    return false;
	  }
	}
