/*
 * @file         Hydraulique1dLigneZoneTableau.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:43:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import org.fudaa.dodico.hydraulique1d.metier.MetierZone;
/**
 * Definit le mod�le d'une ligne du tableau repr�sentant une zone.
 *
 * @see Hydraulique1dLigneReelTableau
 * @see Hydraulique1dLigneZoneTailleTableau
 * @see Hydraulique1dLigneZoneFrottementTableau
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.3 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 */
public class Hydraulique1dLigneZoneTableau extends Hydraulique1dLigne1EntierEtReelsTableau {
  /**
   * Constructeur par d�faut d'une ligne de cellule vide avec 2 r�els.
   */
  public Hydraulique1dLigneZoneTableau() {
    this(2);
  }
  /**
   * Constructeur d'une ligne de cellule vide.
   * @param nbReel le nombre de r�els sur la ligne.
   */
  public Hydraulique1dLigneZoneTableau(int nbReel) {
    super(nbReel);
  }

  protected void setMetier(MetierZone zone) {
    if (zone == null) return;
    abscDebut(zone.abscisseDebut());
    abscFin(zone.abscisseFin());
    if (zone.biefRattache() != null) {
      iBief(zone.biefRattache().indice()+1);
    }
  }

  /**
   * @return la valeur du num�ro du bief (1�re colonne).
   * peut �tre nulle si la cellule est vide.
   */
  public Integer numBief() {
    return entier();
  }
  /**
   * @return la valeur du num�ro du bief (1�re colonne).
   * peut �tre Integer.MAX_VALUE si la cellule est vide.
   */
  public int iBief() {
    return i();
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param numBief La nouvelle valeur du num�ro du bief (peut �tre nulle).
   */
  public void numBief(Integer numBief) {
    super.entier(numBief);
  }
  /**
   * Initialise le premier �lement de la ligne.
   * @param numBief La nouvelle valeur du n� du bief.
   */
  public void iBief(int numBief) {
    super.i(numBief);
  }
  /**
   * Initialise l'abscisse de d�but.
   * @param X La nouvelle valeur de l'abscisse de d�but(peut �tre null).
   */
  public void abscDebut(Double X) {
    super.X(X);
  }
  /**
   * @return La valeur de l'abcisse.
   */
  public double abscDebut() {
    return super.x();
  }
  /**
   * Initialise l'abscisse de d�but.
   * @param x La nouvelle valeur de l'abscisse.
   */
  public void abscDebut(double x) {
    super.x(x);
  }
  /**
   * Initialise l'abscisse de fin.
   * @param Y La nouvelle valeur de l'abscisse de d�but(peut �tre null).
   */
  public void abscFin(Double Y) {
    super.Y(Y);
  }
  /**
   * @return La valeur de l'abcisse de fin.
   */
  public double abscFin() {
    return super.y();
  }
  /**
   * Initialise l'abscisse de fin.
   * @param y La nouvelle valeur de l'abscisse de fin.
   */
  public void abscFin(double y) {
    super.y(y);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Hydraulique1dLigneZoneTableau) {
      Hydraulique1dLigneZoneTableau l =(Hydraulique1dLigneZoneTableau)obj;
      return ((l.iBief()==iBief())&&(l.abscDebut()==abscDebut())&&(l.abscFin()==abscFin()));
    }
    else if (obj instanceof MetierZone) {
      MetierZone izone = (MetierZone)obj;
      if (izone.abscisseDebut() != abscDebut()) return false;
      if (izone.abscisseFin() != abscFin()) return false;
      if (izone.biefRattache() == null) return false;
      return ((izone.biefRattache().indice()+1)==iBief());
    }
    return false;
  }
}
