/*
 * @file         Hydraulique1dLigneZoneTailleTableau.java
 * @creation     2004-07-06
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeriesUnitaire;
import org.fudaa.dodico.hydraulique1d.metier.MetierZone;
import org.fudaa.dodico.hydraulique1d.metier.MetierZonePlanimetrage;
/**
 * Definit le mod�le d'une ligne du tableau repr�sentant une zone et une taille.
 * @see Hydraulique1dLigneReelTableau
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.3 $ $Date: 2007-11-20 11:43:11 $ by $Author: bmarchan $
 */
public final class Hydraulique1dLigneZoneTailleTableau extends Hydraulique1dLigneZoneTableau {
  /**
   * Constructeur par d�faut d'une ligne de cellule vide.
   */
  public Hydraulique1dLigneZoneTailleTableau() {
    super(3);
  }

  /**
   * Constructeur pour une ligne initialis�e par 1 entier primitif et 1 r�el primitif.
   * @param numBief premier �l�ment de la ligne.
   * @param abscDebut deuxi�me �l�ment de la ligne.
   * @param abscFin troisi�me �l�ment de la ligne.
   * @param taille quatri�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneZoneTailleTableau(int numBief, double abscDebut, double abscFin, double taille) {
    this(intValue(numBief), doubleValue(abscDebut), doubleValue(abscFin), doubleValue(taille));
  }
  /**
   * Constructeur pour une ligne initialis�e par 1 entier et 1 r�el non primitif.
   * @param numBief premier �l�ment de la ligne.
   * @param abscDebut deuxi�me �l�ment de la ligne.
   * @param abscFin troisi�me �l�ment de la ligne.
   * @param taille quatri�me �l�ment de la ligne.
   */
  public Hydraulique1dLigneZoneTailleTableau(Integer numBief, Double abscDebut, Double abscFin, Double taille) {
    super(3);
    entier(numBief);
    abscDebut(abscDebut);
    abscFin(abscFin);
    taille(taille);
  }

  public void setMetier(MetierDefinitionSectionsParSeriesUnitaire def) {
    if (def == null) return;
    taille(def.pas());
    if (def.zone() != null) {
      MetierZone zone = def.zone();
      super.setMetier(zone);
    }
  }
  public void setMetier(MetierZonePlanimetrage zone) {
    if (zone == null) return;
    super.setMetier(zone);
    taille(zone.taillePas());
  }

  /**
   * Initialise la taille.
   * @param Z La nouvelle valeur de la taille (peut �tre null).
   */
  public void taille(Double Z) {
    super.Z(Z);
  }
  /**
   * @return La valeur de la taille.
   */
  public double taille() {
    return super.z();
  }
  /**
   * Initialise la taille.
   * @param z La nouvelle valeur de la taille.
   */
  public void taille(double z) {
    super.z(z);
  }
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Hydraulique1dLigneZoneTailleTableau) {
      Hydraulique1dLigneZoneTailleTableau l =(Hydraulique1dLigneZoneTailleTableau)obj;
      return (super.equals(l)&&(l.taille()==taille()));
    }
    else if (obj instanceof MetierZonePlanimetrage) {
      MetierZonePlanimetrage izone = (MetierZonePlanimetrage)obj;
      return (super.equals(izone)&&(izone.taillePas()==taille()));
    }
    else if (obj instanceof MetierDefinitionSectionsParSeriesUnitaire) {
      MetierDefinitionSectionsParSeriesUnitaire def = (MetierDefinitionSectionsParSeriesUnitaire)obj;
      if (def.pas() != taille()) return false;
      if (def.zone() == null) return false;
      MetierZone izone = def.zone();
      return (super.equals(izone));
    }
    return false;
  }
}
