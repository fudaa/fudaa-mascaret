/*
 * @file         Hydraulique1dTableau.java
 * @creation     2004-09-16
 * @modification $Date: 2006-09-28 13:21:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2005 EDF/OPP
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;


import com.memoire.bu.BuCutCopyPasteInterface;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

/**
 * Classe de base des tableaux d'hydraulique1d.
 * G�re le copier/couper/coller, composant de rendu unique, ajuste sa largeur...
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.10 $ $Date: 2006-09-28 13:21:06 $ by $Author: opasteur $
 */
public class Hydraulique1dTableau extends CtuluTable implements BuCutCopyPasteInterface {
  List listeDoubleClickListener_= new ArrayList(1);

  /**
   * Constructeur par d�faut avec Hydraulique1dTableauReelModel comme modele.
   */
  public Hydraulique1dTableau() {
    this(new Hydraulique1dTableauReelModel());
  }

  public final static String getS(final String _s) {
    return Hydraulique1dResource.HYDRAULIQUE1D.getString(_s);
  }
  /**
   * Constructeur avec un modele en argument.
   * @param model Hydraulique1dTableauReelModel
   */
  public Hydraulique1dTableau(Hydraulique1dAbstractTableauModel model) {
    super(model);
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          fireDoubleClickLigne();
        }
      }
    });
    this.getActionMap().put("cut",new AbstractAction("cut"){
      @Override
      public void actionPerformed(ActionEvent evt){
        cut();
      };
    });
    setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    setRowSelectionAllowed(true);
    setColumnSelectionAllowed(true);
  }

  private final TableCellRenderer TCR = new Hydraulique1dTableauCellRenderer();
  @Override
  public TableCellRenderer getDefaultRenderer(Class _columnClass) {
    return TCR;
  }


  /**
   * R�cup�re les donn�es de l'objet m�tier et les tranferts vers le mod�le de tableau.
   */
  public void setValeurs() {
    TableModel m= getModel();
    if (m instanceof Hydraulique1dAbstractTableauModel) {
      Hydraulique1dAbstractTableauModel modele= (Hydraulique1dAbstractTableauModel)m;
      modele.setValeurs();
    }
  }

  /**
   * Transferts les donn�es du tableau vers l'objet m�tier.
   * @return vrai s'il existe des diff�rences, faux sinon.
   */
  public boolean getValeurs() {
    TableModel m= getModel();
    if (m instanceof Hydraulique1dAbstractTableauModel) {
      Hydraulique1dAbstractTableauModel modele= (Hydraulique1dAbstractTableauModel)m;
      return modele.getValeurs();
    }
    return false;
  }

  /**
   * Ajoute un nouvelle �couteur d'�v�nement "double-click" sur une ligne.
   * @param listener ActionDoubleClickListener
   */
  public void addActionDoubleClickListener(ActionDoubleClickListener listener) {
    listeDoubleClickListener_.add(listener);
  }

  /**
   * Supprimme les �couteurs de "double-click" sur une ligne.
   */
  public void removeActionDoubleClickListener() {
    listeDoubleClickListener_ = new ArrayList(1);
  }

  /**
   * Notifie tous les �couteurs de "double click" sur une ligne.
   * la m�thode "actionDoubleClick(ActionDoubleClickEvent event)" des �couteurs est ex�cut�.
   */
  public void fireDoubleClickLigne() {
    int[] indexes= getSelectedRows();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dAbstractTableauModel) {
      if (indexes.length >0) {
        int indexeLigne= indexes[0];
        ActionDoubleClickEvent event = new ActionDoubleClickEvent(this, indexeLigne);
        Iterator ite = listeDoubleClickListener_.iterator();
        while(ite.hasNext()) {
          ((ActionDoubleClickListener)ite.next()).actionDoubleClick(event);
        }
      }
    }
  }
  @Override
  public void cut() {
    copy();
  }
  @Override
  public void duplicate() {
  }
  /**
   * Retourne la valeur la plus large � afficher pour une colonne.
   * @param col la valeur la plus large � afficher pour une colonne.
   * @return Object
   */
  public Object getLongValues(int col) {
    Class colonneClass = getColumnClass(col);
    if (colonneClass == Integer.class) {
      return new Integer(8888);
    }
    else if (colonneClass == Double.class) {
      return new Double(88888.88);
    }
    else if (colonneClass == String.class) {
      return "chaine longue";
    }
    else if (colonneClass == Boolean.class) {
      return Boolean.FALSE;
    }
    else {
      return null;
    }
  }

  public void initColumnSizes() {
    TableColumn column = null;
    Component comp = null;
    Component compHeader = null;
    int headerWidth = 0;
    int cellWidth = 0;
    TableModel model = this.getModel();
    for (int i = 0; i < model.getColumnCount(); i++) {
      column = getColumnModel().getColumn(i);
      compHeader = new JTextField(getColumnName(i));
      headerWidth = compHeader.getPreferredSize().width;
      try {
        comp =
            getDefaultRenderer(
            model.getColumnClass(i)).getTableCellRendererComponent(
            this,
            getLongValues(i),
            false,
            false,
            0,
            i);
      }
      catch (NullPointerException ex) {
        // getTableCellRendererComponent() peut planter � l'initialisation
        return;
      }
      cellWidth = comp.getPreferredSize().width;
      column.setPreferredWidth(Math.max(headerWidth, cellWidth));
    }
  }

  @Override
  public void tableChanged(TableModelEvent e) {
    super.tableChanged(e);
    initColumnSizes();
  }

  /**
   * Efface les �ventuelles lignes s�lectionn�es.
   * #####UNIQUEMENT POUR LES Hydraulique1dTableauChainesModel ######
   */
  public void effaceLignesSelectionnees() {
    int[] indexes= getSelectedRows();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauChainesModel) {
    	Hydraulique1dTableauChainesModel modele= (Hydraulique1dTableauChainesModel)m;
      for (int i= (indexes.length - 1); i >= 0; i--) {
        modele.effacerLigne(indexes[i]);
      }
    }

  }

  /**
   * Supprime les �ventuelles lignes s�lectionn�es.
   * #####UNIQUEMENT POUR LES Hydraulique1dTableauChainesModel ######
   */
  public void supprimeLignesSelectionnees() {
    int[] indexes= getSelectedRows();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauChainesModel) {
    	Hydraulique1dTableauChainesModel modele= (Hydraulique1dTableauChainesModel)m;
      for (int i= (indexes.length - 1); i >= 0; i--) {
        modele.supprimerLigne(indexes[i]);
      }
    }
  }

}
