/*
 * @file         Hydraulique1dTableau1EntierEtReelsModel.java
 * @creation     29/06/04
 * @modification $Date: 2005-08-16 13:53:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Mod�le de tableau avec la premi�re colonne enti�re et les suivantes r�elles.
 * @see org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dLigne1EntierEtReelsTableau
 * @see org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.4 $ $Date: 2005-08-16 13:53:01 $ by $Author: deniger $
 */
public class Hydraulique1dTableau1EntierEtReelsModel
    extends Hydraulique1dTableauReelModel {
  private final static String[] DEFAUT_COLUMN_NAMES = {
      getS("entier"), "X", "Y", "Z"};

  /**
   * Constructeur par d�faut avec 4 colonnes ("entier", "X", "Y" et "Z")
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableau1EntierEtReelsModel() {
    super(DEFAUT_COLUMN_NAMES, 20);
  }

  /**
   * Constructeur pr�cisant les noms de colonnes et le nombre de lignes vides � la fin.
   * @param columnNames le tableau des noms de colonnes.
   * @param nbLignesVideFin le nombre de lignes vides � la fin.
   */
  public Hydraulique1dTableau1EntierEtReelsModel(String[] columnNames, int nbLignesVideFin) {
    super(columnNames, nbLignesVideFin);
  }
  /**
   * Retourne la classe de chaque colonne.
   * @param c L'indice de la colonne
   * @return Integer.class si c vaut 0, Double.class sinon.
   */
  @Override
  public Class getColumnClass(int c) {
    if (c == 0) {
      return Integer.class;
    }
    return Double.class;
  }

  /**
   * Initialise les lignes du tableau et laisses des lignes vides � la fin.
   * @param lignes Les nouvelles lignes.
   */
  public void setTabLignes(Hydraulique1dLigne1EntierEtReelsTableau[] lignes) {
    listePts_ = new ArrayList(lignes.length + getNbLignesVideFin());
    listePts_.addAll(Arrays.asList(lignes));
    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule (Type Integer si col vaut 0, type Double sinon), si la cellule est vide retourne nulle.
   */
  @Override
  public Object getValueAt(int row, int col) {
    Hydraulique1dLigne1EntierEtReelsTableau lig = (
        Hydraulique1dLigne1EntierEtReelsTableau) listePts_.get(row);
    if (col == 0) {
      return lig.entier();
    }
    return lig.getValue(col - 1);
  }

  /**
   * Modifie la valeur d'une cellule du tableau.
   * @param value La nouvelle valeur (Integer, Double ou null).
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   */
  @Override
  public void setValueAt(Object value, int row, int col) {
    Hydraulique1dLigne1EntierEtReelsTableau lig = (
        Hydraulique1dLigne1EntierEtReelsTableau) listePts_.get(row);
    if (col == 0) {
      lig.entier( (Integer) value);
    }
    else {
      lig.setValue(col - 1, (Double) value);
    }
    fireTableDataChanged();
  }

  /**
   * Pas impl�ment� !
   * @return rien.
   * @throws UnsupportedOperationException
   */
  @Override
  public double[][] getTabDouble() {
    throw new UnsupportedOperationException(
        "Hydraulique1dTableau1EntierEtReelsModel : public double[][] getTabDouble()");
  }

  /**
   * Pas impl�ment� !
   * @param tableau double[][] Pas utilis�.
   * @throws UnsupportedOperationException
   */
  @Override
  public void setTabDouble(double[][] tableau) {
    throw new UnsupportedOperationException("Hydraulique1dTableau1EntierEtReelsModel : public void setTabDouble(double[][] tableau)");
  }

  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigne1EntierEtReelsTableau.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigne1EntierEtReelsTableau(getColumnCount() - 1);
  }
}
