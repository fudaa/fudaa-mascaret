/*
 * @file         Hydraulique1dTableauBooleenModel.java
 * @creation     2006-07-05
 * @modification $Date: 2006-09-08 16:04:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2003 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
/**
 * Mod�le de tableau contenant plusieurs colonnes de bool�ens �ditables.
 * @see Hydraulique1dTableauBooleenModel
 * @author Olivier Pasteur
 * @version 1.0
 */
public class Hydraulique1dTableauBooleenModel extends Hydraulique1dAbstractTableauModel {

  /**
   * Indique le nombre de ligne vide rajout� � la fin.
   * Par d�faut, ce nombre est �gale � 20.
   */
  private int nbLignesVideFin_=20;
  /**
   * La liste des lignes du tableau (Hydraulique1dLigneReelTableau).
   */
  protected List listePts_;
  /**
   * Constructeur par d�faut.
   * Initialise la liste de ligne.
   */
  public Hydraulique1dTableauBooleenModel() {
    super();
    listeColumnNames_.add(getS("Nom Traceur"));
    listeColumnNames_.add(getS("Diffusion"));
    listeColumnNames_.add(getS("Convection"));
    listePts_= new ArrayList();
  }

  /**
   * Constructeur pr�cisant les noms de colonnes et le nombre de lignes vides � la fin.
   * @param columnNames le tableau des noms de colonnes.
   * @param nbLignesVideFin le nombre de lignes vides � la fin.
   */
  public Hydraulique1dTableauBooleenModel(String[] columnNames, int nbLignesVideFin) {
    super(columnNames);
    listePts_= new ArrayList();
    nbLignesVideFin_=nbLignesVideFin;
  }
  /**
   * @return le nombre de ligne.
   */
  @Override
  public int getRowCount() {
    return listePts_.size();
  }

  /**
   * Retourne le nombre de lignes vides � la fin du tableau.
   * @return le nombre de lignes vides.
   */
  public int getNbLignesVideFin() {
    return nbLignesVideFin_;
  }

  /**
   * Retourne la classe (Class) de type Boolean
   * @param col l'indice de la colonne
   * @return la classe Double
   */
  @Override
  public Class getColumnClass(int col) {
    return Boolean.class;
  }
  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule (Type Double ou null si cellule vide).
   */
  @Override
  public Object getValueAt(int row, int col) {
    Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)listePts_.get(row);
    return lig.getValue(col);
  }
  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule .
   */
  public boolean valueAt(int row, int col) {
    return Hydraulique1dLigneBooleensTableau.booleanValue(getValueAt(row, col));
  }
  /**
   * Retourne si la cellule est �ditable.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return Vrai.
   */
  @Override
  public boolean isCellEditable(int row, int col) {
    return true;
  }
  /**
   * Modifie la valeur d'une cellule du tableau.
   * @param value La nouvelle valeur (Double ou null).
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   */
  @Override
  public void setValueAt(Object value, int row, int col) {
    Boolean valeur= (Boolean)value;
    Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)listePts_.get(row);
    lig.setValue(col, valeur);
    fireTableCellUpdated(row,col);
  }

  /**
   * Retourne un tableau de bool�ens avec les 2 premi�res colonnes non vide.
   * @return Le tableau de bool�en [indice colonne][indice ligne].
   */
  public boolean[][] getTabBoolean() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
      if ((lig.X() != null) && (lig.Y() != null)) {
        listeTmp.add(lig);
      }
    }
    int nbLigne= listeTmp.size();
    int nbColonne = getColumnCount();
    boolean[][] res= new boolean[nbLigne][nbColonne];
    for (int i= 0; i < nbLigne; i++) {
      Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)listeTmp.get(i);
      for (int j = 0; j < nbColonne; j++) {
        res[i][j]= lig.value(j);
      }
    }
    return res;
  }
  public boolean[] getTab1DBooleanComplet() {
    List listeTmp= getListePtsComplets();
    List listeRes = new ArrayList();
    int taille= listeTmp.size();
    for (int i= 0; i < taille; i++) {
      Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)listeTmp.get(i);
      for (int j = 0; j < lig.getTaille(); j++) {
        listeRes.add(lig.getValue(j));
      }
    }
    boolean[] tRes = new boolean[listeRes.size()];
    for (int i = 0; i < tRes.length; i++) {
      tRes[i] = ((Boolean)listeRes.get(i)).booleanValue();
    }
    return tRes;
  }
  /**
   * Retourne un tableau de bool�ens des lignes completes.
   * @return Le tableau de bool�ens [indice ligne][indice colonne].
   */
  public boolean[][] getTabBooleanComplet() {
    List listeTmp= getListePtsComplets();

    int nbLigne= listeTmp.size();
    int nbColonne = getColumnCount();
    boolean[][] res= new boolean[nbLigne][nbColonne];
    for (int i= 0; i < nbLigne; i++) {
      Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)listeTmp.get(i);
      for (int j = 0; j < nbColonne; j++) {
        res[i][j]= lig.value(j);
      }
    }
    return res;
  }
  /**
   * Cree une nouvelle ligne vide.
   * Doit �tre surcharg� dans le cas o� on utilise des lignes filles
   * de de la classe Hydraulique1dLigneBooleensTableau.
   * @return Hydraulique1dLigneBooleensTableau
   */
  public Hydraulique1dLigneBooleensTableau creerLigneVide() {
    return new Hydraulique1dLigneBooleensTableau(getColumnCount());
  }
  /**
   * V�rifie les bonnes dimension du tableau.
   * @param tableau de bool�ens [indice colonne][indice ligne].
   * @return boolean Vrai si OK
   */
  private boolean verifieDimTableau(boolean[][] tableau) {
    for (int i = 1; i < tableau.length; i++) {
      if (tableau[0].length != tableau[i].length) {
        return false;
      }
    }
    return true;
  }
  /**
   * Retourne un tableau de bool�ens avec les 2 premi�res colonnes non vide.
   * @param tableau de bool�ens [indice ligne][indice colonne].
   */
  public void setTabBoolean(boolean[][] tableau) {
    if (!verifieDimTableau(tableau)) return;
    int nbLigne = tableau.length;

    int nbColonne = getColumnCount();
    ArrayList liste= new ArrayList(nbLigne+getNbLignesVideFin());
    for (int i = 0; i < nbLigne; i++) {
      Hydraulique1dLigneBooleensTableau lig = creerLigneVide();
      liste.add(lig);
      for (int j = 0; j < nbColonne; j++) {
        lig.setValue(j, tableau[i][j]);
      }
    }
    for (int i = 0; i < getNbLignesVideFin(); i++) {
      Hydraulique1dLigneBooleensTableau lig = creerLigneVide();
      liste.add(lig);
    }
    listePts_=liste;
    fireTableStructureChanged();
  }
  /**
   * Retourne la liste de ligne contenant aucune cellule vide.
   * @return La liste de Hydraulique1dLigneBooleensTableau contenant aucune cellule vide.
   */
  public List getListePtsComplets() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)ite.next();
      if (!lig.isExisteNulle()) {
        listeTmp.add(lig);
      }
    }
    return listeTmp;
  }
  /**
   * Retourne la liste de ligne ne contenant pas que des cellules vides.
   * @return La liste de Hydraulique1dLigneBooleensTableau ne contenant pas que des cellules vides.
   */
  public List getListePtsPasToutVide() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)ite.next();
      if (!lig.isToutNulle()) {
        listeTmp.add(lig);
      }
    }
    return listeTmp;
  }
  /**
   * Ajoute une ligne vide la fin du tableau.
   */
  public void ajouterLigne() {
    listePts_.add(creerLigneVide());
    fireTableDataChanged();
  }
  /**
   * Ajoute une ligne vide dans le tableau.
   * @param row Indice de la ligne � ajouter.
   */
  public void ajouterLigne(int row) {
    listePts_.add(row, creerLigneVide());
    fireTableDataChanged();
  }
  /**
   * Supprime une ligne du tableau.
   * @param row Indice de la ligne � supprimer.
   */
  public void supprimerLigne(int row) {
    listePts_.remove(row);
    fireTableDataChanged();
  }
  /**
   * Efface une ligne du tableau.
   * La ligne doit alors contenir que des cellules vides.
   * @param row Indice de la ligne � effacer.
   */
  public void effacerLigne(int row) {
    ((Hydraulique1dLigneBooleensTableau)listePts_.get(row)).effaceLigne();
    fireTableDataChanged();
  }
  /**
   * Ajoute une colonne vide � droite du tableau.
   * @param nomColonne Le nom de la nouvelle colonne.
   */
  public void ajouterColonne(String nomColonne) {
    ajouterColonne(nomColonne, listeColumnNames_.size());
  }
  /**
   * Ajoute une colonne vide � l'indexe indiqu� du tableau.
   * @param nomColonne Le nom de la nouvelle colonne.
   * @param indexe L'indice de la colonne � rajouter.
   */
  public void ajouterColonne(String nomColonne, int indexe) {
    // ajout du nom
    listeColumnNames_.add(indexe, nomColonne);

    // ajout d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)ite.next();
      lig.ajoutCelluleVide(indexe);
    }
    fireTableStructureChanged();
  }
  /**
   * Supprime une colonne du tableau.
   * @param col Indice de la colonne � supprimer.
   */
  public void supprimerColonne(int col) {
    // ajout du nom
    listeColumnNames_.remove(col);

    // suppression d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)ite.next();
      lig.supprimeCellule(col);
    }
    fireTableStructureChanged();
  }
  /**
   * Efface une colonne du tableau.
   * La colonne doit alors contenir que des cellules vides.
   * @param col Indice de la colonne � effacer.
   */
  public void effacerColonne(int col) {
    // suppression d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneBooleensTableau lig= (Hydraulique1dLigneBooleensTableau)ite.next();
      lig.setValue(col, null);
    }
    fireTableDataChanged();
  }

  /**
   * Trie le tableau
   */
  public void trier() {
    Collections.sort(listePts_);
    fireTableDataChanged();
  }

}
