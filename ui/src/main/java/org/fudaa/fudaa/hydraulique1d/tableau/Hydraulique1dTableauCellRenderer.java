/*
 * @file         Hydraulique1dTableauCellRenderer.java
 * @creation     2004-07-12
 * @modification $Date: 2007-11-20 11:43:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.plaf.ColorUIResource;

import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;

import com.memoire.bu.BuLabel;
import org.fudaa.ctulu.table.CtuluTableCellRenderer;

/**
 * Afficheur de cellule de tableau mettant en italique les cellule non �ditable.
 * @see org.fudaa.fudaa.hydraulique1d.tableau.Hydraulique1dTableauReel
 * @version      $Revision: 1.12 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 * @author       Jean-Marc Lacombe
 */
public class Hydraulique1dTableauCellRenderer
  extends CtuluTableCellRenderer {
  private boolean italicUneditable_ = true;
  public Hydraulique1dTableauCellRenderer() {
    super();
    DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.US);
    DecimalFormat nf= (DecimalFormat)NumberFormat.getInstance();
    nf.setDecimalFormatSymbols(dfs);
    nf.setMaximumFractionDigits(Hydraulique1dResource.getFractionDigits());
    nf.setMinimumFractionDigits(1);
    nf.setGroupingUsed(false);
    setNumberFormat(nf);

    DecimalFormat nfI= (DecimalFormat)NumberFormat.getInstance();
    nfI.setDecimalFormatSymbols(dfs);
    nfI.setMaximumFractionDigits(0);
    nfI.setMinimumFractionDigits(0);
    nfI.setGroupingUsed(false);
    setIntegerFormat(nfI);
  }
  public void setItalicUneditable(boolean italicUneditable) {
    italicUneditable_ = italicUneditable;
  }
  public boolean isItalicUneditable() {
    return italicUneditable_;
  }
  /**
   * Retoune le composant utilis� pour dessiner la cellule.
   * Cette m�thode est utilis� pour configurer le "renderer" (composant de rendu graphique) avant le trac�.
   * @param _table La JTable qui demande au "renderer" de se dessiner ; peut �tre null.
   * @param _value La valeur de la cellule qui sera trac�.
   * @param _selected Vrai si la cellule � afficher est s�lctionn�e.
   * @param _focus Vrai si a le focus (mode edition).
   * @param _row L'indice de la ligne de la cellule.
   * @param _column L'indice de la colonne de la cellule.
   * @return le composant (Component) utilis� pour dessiner la cellule.
   */
  @Override
  public Component getTableCellRendererComponent(
    JTable _table,
    Object _value,
    boolean _selected,
    boolean _focus,
    int _row,
    int _column) {

    BuLabel r=
      (BuLabel)super.getTableCellRendererComponent(
        _table,
        _value,
        _selected,
        _focus,
        _row,
        _column);


    if (italicUneditable_) {
      if (!_table.getModel().isCellEditable(_row, _column)) {
        Font oldFont = r.getFont();
        Font fontNew = new Font(oldFont.getName(), Font.ITALIC, oldFont.getSize());
        r.setFont(fontNew);
      }

    }

    if (_value instanceof Boolean) {

      JCheckBox box = new JCheckBox();
      box.setSelected(((Boolean)_value).booleanValue());
      box.setHorizontalAlignment(SwingConstants.CENTER);
// Unused      Color fg = box.getForeground();
      Color bg = ColorUIResource.WHITE;
      if(_selected) {
          bg = blueify(bg);
      }
      if ((_row %2)!=0) {
          bg = darken(bg);
      }
      box.setBackground(bg);
      return box;
   }

    return r;
  }
  private static final Color darken(Color _c)
  {
    int r=_c.getRed();
    int g=_c.getGreen();
    int b=_c.getBlue();

    r=r*19/20;
    g=g*19/20;
    b=b*19/20;

    Color c=(_c instanceof ColorUIResource)
      ? new ColorUIResource(r,g,b)
      : new Color(r,g,b);

    return c;
  }

  private static final Color blueify(Color _c)
  {
    int r=_c.getRed();
    int g=_c.getGreen();
    int b=_c.getBlue();

    r=r*4/5;
    g=g*4/5;

    Color c=(_c instanceof ColorUIResource)
      ? new ColorUIResource(r,g,b)
      : new Color(r,g,b);

    return c;
  }
}
