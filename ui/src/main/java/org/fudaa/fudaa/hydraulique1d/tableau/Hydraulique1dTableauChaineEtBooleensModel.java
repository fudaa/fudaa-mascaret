/*
 * @file         Hydraulique1dTableauChaineEtBooleensModel.java
 * @creation     03/07/06
 * @modification $Date: 2006-07-07 12:26:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Mod�le de tableau avec la premi�re colonne une chaine et les suivantes bool�ennes.
 * @author Jean-Marc Lacombe
 * @version 1.0
 */

public class Hydraulique1dTableauChaineEtBooleensModel     extends Hydraulique1dTableauBooleenModel {
  private final static String[] DEFAUT_COLUMN_NAMES = {
      getS("intitul�"), getS("abscisse")};

  /**
   * Constructeur par d�faut avec 2 colonnes
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableauChaineEtBooleensModel() {
    super(DEFAUT_COLUMN_NAMES, 20);
  }

  /**
   * Constructeur pr�cisant les noms de colonnes et le nombre de lignes vides � la fin.
   * @param columnNames le tableau des noms de colonnes.
   * @param nbLignesVideFin le nombre de lignes vides � la fin.
   */
  public Hydraulique1dTableauChaineEtBooleensModel(String[] columnNames, int nbLignesVideFin) {
    super(columnNames, nbLignesVideFin);
  }
  /**
   * Retourne la classe de chaque colonne.
   * @param c L'indice de la colonne
   * @return String.class si c vaut 0, Boolean.class sinon.
   */
  @Override
  public Class getColumnClass(int c) {
    if (c == 0) {
      return String.class;
    }
    return Boolean.class;
  }

  /**
   * Initialise les lignes du tableau et laisses des lignes vides � la fin.
   * @param lignes Les nouvelles lignes.
   */
  public void setTabLignes(Hydraulique1dLigneChaineEtBooleensTableau[] lignes) {
    listePts_ = new ArrayList(lignes.length + getNbLignesVideFin());
    listePts_.addAll(Arrays.asList(lignes));
    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(new Hydraulique1dLigneChaineEtBooleensTableau(getColumnCount() -
          1));
    }
    fireTableDataChanged();
  }

  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule (Type Integer si col vaut 0, type Boolean sinon), si la cellule est vide retourne nulle.
   */
  @Override
  public Object getValueAt(int row, int col) {
    Hydraulique1dLigneChaineEtBooleensTableau lig = (
        Hydraulique1dLigneChaineEtBooleensTableau) listePts_.get(row);
    if (col == 0) {
      return lig.chaine();
    }
    return lig.getValue(col - 1);
  }

  /**
   * Modifie la valeur d'une cellule du tableau.
   * @param value La nouvelle valeur (Integer, Boolean ou null).
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   */
  @Override
  public void setValueAt(Object value, int row, int col) {
    Hydraulique1dLigneChaineEtBooleensTableau lig = (
        Hydraulique1dLigneChaineEtBooleensTableau) listePts_.get(row);
    if (col == 0) {
      lig.chaine( (String) value);
    }
    else {
      lig.setValue(col - 1, (Boolean) value);
    }
    fireTableDataChanged();
  }

  /**
   * Pas impl�ment� !
   * @return rien.
   * @throws UnsupportedOperationException
   */
  @Override
  public boolean[][] getTabBoolean() {
    throw new UnsupportedOperationException(
        "Hydraulique1dTableauChaineEtBooleensModel : public boolean[][] getTabBoolean()");
  }

  /**
   * Pas impl�ment� !
   * @param tableau boolean[][] Pas utilis�.
   * @throws UnsupportedOperationException
   */
  @Override
  public void setTabBoolean(boolean[][] tableau) {
    throw new UnsupportedOperationException("Hydraulique1dTableauChaineEtBooleensModel : public void setTabBoolean(boolean[][] tableau)");
  }

  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigneChaineEtReelsTableau.
   */
  @Override
  public Hydraulique1dLigneBooleensTableau creerLigneVide() {
    return new Hydraulique1dLigneChaineEtBooleensTableau(getColumnCount() - 1);
  }

}
