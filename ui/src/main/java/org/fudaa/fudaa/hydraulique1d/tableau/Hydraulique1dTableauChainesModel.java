/*
 * @file         Hydraulique1dTableauChainesModel.java
 * @creation     2006-03-23
 * @modification $Date: 2006-09-08 16:04:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2003 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
/**
 * Mod�le de tableau contenant plusieurs colonnes de chaines �ditable.
 * @see Hydraulique1dLigneChaineTableau
 * @author Olivier Pasteur
 * @version $Revision: 1.3 $ $Date: 2006-09-08 16:04:25 $ by $Author: opasteur $
 */
public class Hydraulique1dTableauChainesModel extends Hydraulique1dAbstractTableauModel {

  /**
   * Indique le nombre de ligne vide rajout� � la fin.
   * Par d�faut, ce nombre est �gale � 20.
   */
  private int nbLignesVideFin_=20;
  /**
   * La liste des lignes du tableau (Hydraulique1dLigneChaineTableau).
   */
  protected List listePts_;
  /**
   * Constructeur par d�faut.
   * Initialise la liste de ligne.
   */
  public Hydraulique1dTableauChainesModel() {
    super();
    listeColumnNames_.add(getS("Noms"));
    listePts_= new ArrayList();
  }

  /**
   * Constructeur pr�cisant les noms de colonnes et le nombre de lignes vides � la fin.
   * @param columnNames le tableau des noms de colonnes.
   * @param nbLignesVideFin le nombre de lignes vides � la fin.
   */
  public Hydraulique1dTableauChainesModel(String[] columnNames, int nbLignesVideFin) {
    super(columnNames);
    listePts_= new ArrayList();
    nbLignesVideFin_=nbLignesVideFin;
  }
  /**
   * @return le nombre de ligne.
   */
  @Override
  public int getRowCount() {
    return listePts_.size();
  }

  /**
   * Retourne le nombre de lignes vides � la fin du tableau.
   * @return le nombre de lignes vides.
   */
  public int getNbLignesVideFin() {
    return nbLignesVideFin_;
  }

  /**
   * Retourne la classe (Class) de type String
   * @param col l'indice de la colonne
   * @return la classe String
   */
  @Override
  public Class getColumnClass(int col) {
    return String.class;
  }
  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule (Type String ou null si cellule vide).
   */
  @Override
  public Object getValueAt(int row, int col) {
    Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)listePts_.get(row);
    return lig.getValue(col);
  }
  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule
   */
  public String valueAt(int row, int col) {
    return (String)getValueAt(row, col);
  }
  /**
   * Retourne si la cellule est �ditable.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return Vrai.
   */
  @Override
  public boolean isCellEditable(int row, int col) {
    return true;
  }
  /**
   * Modifie la valeur d'une cellule du tableau.
   * @param value La nouvelle valeur (String ou null).
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   */
  @Override
  public void setValueAt(Object value, int row, int col) {
    String valeur= (String)value;
    Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)listePts_.get(row);
    lig.setValue(col, valeur);
    fireTableCellUpdated(row,col);
  }

  public void setValeurs(String[][] valeurs) {
  int nbLignes=listePts_.size();
  if (nbLignes!=valeurs.length  || !verifieDimTableau(valeurs)) {
      System.out.println(
              "Erreur Dimension incorrecte dans le tableau des noms de traceurs"+" lignes: "+nbLignes+" "+valeurs.length+" Colonnes: "+listeColumnNames_.size()+" "+valeurs[0].length);
      return;
  }


  for (int i = 0; i <nbLignes; i++) {
       Hydraulique1dLigneChaineTableau ligne = (Hydraulique1dLigneChaineTableau)listePts_.get(i);
      for (int j = 0; j < ligne.getTaille(); j++) {
          ligne.setValue(j,valeurs[i][j]);
      }
  }
  fireTableDataChanged();
 }

 public String[][] getValeursTableau() {
     String[][] tab = new String[getRowCount()][getColumnCount()];
     for (int i = 0; i < getRowCount(); i++) {
         for (int j = 0; j < getColumnCount(); j++) {
             tab[i][j]=this.valueAt(i,j);
         }

     }
     return tab;
 }

 public byte[] getByte() {
    String[][] chaine = getValeursTableau();
    StringBuilder chaineComplete = new StringBuilder(getRowCount()*40);

    for (int i = 0; i < chaine.length; i++) {
        chaineComplete.append(chaine[i][0]);
        chaineComplete.append("\n");
    }

    return chaineComplete.toString().getBytes();
}

 /**
  * Initialise le mod�le � partir d'une grande chaine contenant des fin de lignes.
  * @param _largeString String
  */
 public void setLargeString(String _largeString) {
   //douteux ...
   System.gc();
   listePts_ = new ArrayList(_largeString.length()/80);
   StringTokenizer st = new StringTokenizer(_largeString,"\n\r\f");
   while (st.hasMoreTokens()) {
   Hydraulique1dLigneChaineTableau lig = new Hydraulique1dLigneChaineTableau(st.nextToken());
     listePts_.add(lig);
   }
   _largeString = null;
   ((ArrayList)listePts_).trimToSize();
   System.gc();
 }

 /**
  * Initialise le mod�le � partir d'une grande chaine contenant des fin de
  * lignes sous la forme d'un tableau d'octets.
  * @param _largeTabByte byte[]
  */
 public void setLargeTabByte(byte[] _largeTabByte) {
   if (_largeTabByte == null) _largeTabByte = new byte[0];
   String largeString = new String(_largeTabByte);
   //douteux et inutile
   _largeTabByte = null;
   setLargeString(largeString);
 }
 public void reDimentionner(int n) {
     int nbLignes = listePts_.size();
     if (nbLignes != n) {
         for (int i = nbLignes-1; i >=0 ; i--) {
             supprimerLigne(i);
         }
         for (int j = 0; j < n; j++) {
             ajouterLigne();
         }
     }
     fireTableDataChanged();
 }


  /**
   * Retourne un tableau de chaines avec les 2 premi�res colonnes non vide.
   * Si les colonnes suivants sont vides, la valeur sera null.
   * @return Le tableau de chaines [indice colonne][indice ligne].
   */
  public String[][] getTabString() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)ite.next();
      if ((lig.getValue(0) != null)) {
        listeTmp.add(lig);
      }
    }
    int nbLigne= listeTmp.size();
    int nbColonne = getColumnCount();
    String[][] res= new String[nbLigne][nbColonne];
    for (int i= 0; i < nbLigne; i++) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)listeTmp.get(i);
      for (int j = 0; j < nbColonne; j++) {
        res[i][j]= lig.value(j);
      }
    }
    return res;
  }
  public String[] getTab1DStringComplet() {
    List listeTmp= getListePtsComplets();
    List listeRes = new ArrayList();
    int taille= listeTmp.size();
    for (int i= 0; i < taille; i++) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)listeTmp.get(i);
      for (int j = 0; j < lig.getTaille(); j++) {
        listeRes.add(lig.getValue(j));
      }
    }
    String[] tRes = new String[listeRes.size()];
    for (int i = 0; i < tRes.length; i++) {
      tRes[i] = (String)listeRes.get(i);
    }
    return tRes;
  }
  /**
   * Retourne un tableau de chaines des lignes completes.
   * @return Le tableau de chaines [indice ligne][indice colonne].
   */
  public String[][] getTabStringComplet() {
    List listeTmp= getListePtsComplets();

    int nbLigne= listeTmp.size();
    int nbColonne = getColumnCount();
    String[][] res= new String[nbLigne][nbColonne];
    for (int i= 0; i < nbLigne; i++) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)listeTmp.get(i);
      for (int j = 0; j < nbColonne; j++) {
        res[i][j]= lig.value(j);
      }
    }
    return res;
  }
  /**
   * Cree une nouvelle ligne vide.
   * Doit �tre surcharg� dans le cas o� on utilise des lignes filles
   * de de la classe Hydraulique1dLigneChaineTableau.
   * @return Hydraulique1dLigneChaineTableau
   */
  public Hydraulique1dLigneChaineTableau creerLigneVide() {
    return new Hydraulique1dLigneChaineTableau(getColumnCount());
  }
  /**
   * V�rifie les bonnes dimension du tableau.
   * @param tableau de chaines [indice colonne][indice ligne].
   * @return boolean Vrai si OK
   */
  private boolean verifieDimTableau(String[][] tableau) {
    for (int i = 1; i < tableau.length; i++) {
      if (tableau[0].length != tableau[i].length) {
          //System.out.println(tableau[0].length +" "+ tableau[i].length);
        return false;
      }
    }
    return true;
  }
  /**
   * Retourne un tableau de chaines avec les 2 premi�res colonnes non vide.
   * Si les colonnes suivants sont vides, la valeur sera null.
   * @param tableau de chaines [indice ligne][indice colonne].
   */
  public void setTabString(String[][] tableau) {
    if (!verifieDimTableau(tableau)) return;
    int nbLigne = tableau.length;

    int nbColonne = getColumnCount();
    ArrayList liste= new ArrayList(nbLigne+getNbLignesVideFin());
    for (int i = 0; i < nbLigne; i++) {
      Hydraulique1dLigneChaineTableau lig = creerLigneVide();
      liste.add(lig);
      for (int j = 0; j < nbColonne; j++) {
        lig.setValue(j, tableau[i][j]);
      }
    }
    for (int i = 0; i < getNbLignesVideFin(); i++) {
      Hydraulique1dLigneChaineTableau lig = creerLigneVide();
      liste.add(lig);
    }
    listePts_=liste;
    fireTableStructureChanged();
  }
  /**
   * Retourne la liste de ligne contenant aucune cellule vide.
   * @return La liste de Hydraulique1dLigneChaineTableau contenant aucune cellule vide.
   */
  public List getListePtsComplets() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)ite.next();
      if (!lig.isExisteNulle()) {
        listeTmp.add(lig);
      }
    }
    return listeTmp;
  }
  /**
   * Retourne la liste de ligne ne contenant pas que des cellules vides.
   * @return La liste de Hydraulique1dLigneChaineTableau ne contenant pas que des cellules vides.
   */
  public List getListePtsPasToutVide() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)ite.next();
      if (!lig.isToutNulle()) {
        listeTmp.add(lig);
      }
    }
    return listeTmp;
  }
  /**
   * Ajoute une ligne vide la fin du tableau.
   */
  public void ajouterLigne() {
    listePts_.add(creerLigneVide());
    fireTableDataChanged();
  }
  /**
   * Ajoute une ligne vide dans le tableau.
   * @param row Indice de la ligne � ajouter.
   */
  public void ajouterLigne(int row) {
    listePts_.add(row, creerLigneVide());
    fireTableDataChanged();
  }
  /**
   * Supprime une ligne du tableau.
   * @param row Indice de la ligne � supprimer.
   */
  public void supprimerLigne(int row) {
    listePts_.remove(row);
    fireTableDataChanged();
  }
  /**
   * Efface une ligne du tableau.
   * La ligne doit alors contenir que des cellules vides.
   * @param row Indice de la ligne � effacer.
   */
  public void effacerLigne(int row) {
    ((Hydraulique1dLigneChaineTableau)listePts_.get(row)).effaceLigne();
    fireTableDataChanged();
  }
  /**
   * Ajoute une colonne vide � droite du tableau.
   * @param nomColonne Le nom de la nouvelle colonne.
   */
  public void ajouterColonne(String nomColonne) {
    ajouterColonne(nomColonne, listeColumnNames_.size());
  }
  /**
   * Ajoute une colonne vide � l'indexe indiqu� du tableau.
   * @param nomColonne Le nom de la nouvelle colonne.
   * @param indexe L'indice de la colonne � rajouter.
   */
  public void ajouterColonne(String nomColonne, int indexe) {
    // ajout du nom
    listeColumnNames_.add(indexe, nomColonne);

    // ajout d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)ite.next();
      lig.ajoutCelluleVide(indexe);
    }
    fireTableStructureChanged();
  }
  /**
   * Supprime une colonne du tableau.
   * @param col Indice de la colonne � supprimer.
   */
  public void supprimerColonne(int col) {
    // ajout du nom
    listeColumnNames_.remove(col);

    // suppression d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)ite.next();
      lig.supprimeCellule(col);
    }
    fireTableStructureChanged();
  }
  /**
   * Efface une colonne du tableau.
   * La colonne doit alors contenir que des cellules vides.
   * @param col Indice de la colonne � effacer.
   */
  public void effacerColonne(int col) {
    // suppression d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneChaineTableau lig= (Hydraulique1dLigneChaineTableau)ite.next();
      lig.setValue(col, null);
    }
    fireTableDataChanged();
  }

  /**
   * Trie le tableau
   */
  public void trier() {
    Collections.sort(listePts_);
    fireTableDataChanged();
  }


}
