package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierConcentrationInitiale;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresModeleQualiteEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresQualiteDEau;

/**
 * Mod�le du tableau des concentrations initiales.
 * Ent�te : "N� bief", "Abscisse", "C� Traceur1","C� Traceur2" ...
 * @see Hydraulique1dTableauReelModel
 * @author Olivier Pasteur
 * @version $Revision: 1.5 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 */
public class Hydraulique1dTableauConcInitsModel extends
        Hydraulique1dTableau1EntierEtReelsModel {
    private final static String[] COLUMN_NAMES={ getS("N� bief"), getS("Abscisse"), getS("C� Traceur1"),
                                     getS("C� Traceur2") };

 private MetierParametresQualiteDEau qualiteDEau_;

    /**
     * Constructeur par d�faut.
     */
    public Hydraulique1dTableauConcInitsModel() {
      super(COLUMN_NAMES, 20);
    }



     /**
    * Determine le nombre de reel dans le tableau
    *
    * @return int
    */
   public int nbReels() {
     return getColumnCount()-1;
   }

     /**
      * Cree une nouvelle ligne vide.
      * Surcharge de la classe m�re.
      * @return une instance de Hydraulique1dLigneConcentrationsInitialesTableau.
      */
  @Override
     public Hydraulique1dLigneReelTableau creerLigneVide() {

       return new Hydraulique1dLigneConcentrationsInitialesTableau(nbReels());
     }

     public void setValeurs(MetierParametresQualiteDEau qualiteDEau) {
         qualiteDEau_=qualiteDEau;
         MetierParametresModeleQualiteEau modeleQE = qualiteDEau.parametresModeleQualiteEau();
         MetierConcentrationInitiale[] concInits_= qualiteDEau.concentrationsInitiales();
         int nbTraceurs = modeleQE.nbTraceur();
         String[] columnNames= new String[nbTraceurs+2];
         columnNames[0] = getS("N� bief");
         columnNames[1] = getS("Abscisse");
         for (int i = 0; i < nbTraceurs; i++) {
             columnNames[i + 2] = "C� " + modeleQE.vvNomTracer()[i][0];
         }
         setColumnNames(columnNames);

       listePts_= new ArrayList();
       for (int i= 0; i < concInits_.length; i++) {
         Hydraulique1dLigneConcentrationsInitialesTableau lig= new Hydraulique1dLigneConcentrationsInitialesTableau(concInits_[i].numeroBief(),concatDoubleTabDouble(concInits_[i].abscisse(),concInits_[i].concentrations()));
         listePts_.add(lig);
       }
       for (int i= 0; i < getNbLignesVideFin(); i++) {
         listePts_.add(creerLigneVide());
       }
       fireTableDataChanged();
     }

  @Override
     public List getListePtsComplets() {
       List listeATrier = super.getListePtsComplets();
       Collections.sort(listeATrier);
       return listeATrier;
     }





     public double[] concatDoubleTabDouble(double d, double[] c) {
       double[] res = new double[c.length+1];
       res[0]=d;
    System.arraycopy(c, 0, res, 1, c.length);
       return res;
     }

  @Override
     public boolean getValeurs() {
         List listeTmp= getListePtsComplets();
         MetierConcentrationInitiale[] concInits = qualiteDEau_.concentrationsInitiales();
         boolean existeDifference = false;
          if (listeTmp.size() != concInits.length) {
            existeDifference = true;
          }
          MetierConcentrationInitiale[] concInitsTmp = new MetierConcentrationInitiale[listeTmp.size()];
          int tailleMin = Math.min(listeTmp.size(), concInits.length);
          for (int i = 0; i < tailleMin; i++) {
            Hydraulique1dLigneConcentrationsInitialesTableau conc= (Hydraulique1dLigneConcentrationsInitialesTableau)listeTmp.get(i);

            if (!conc.equals(concInits[i])) {
              existeDifference = true;
              concInitsTmp[i] = new MetierConcentrationInitiale();
              conc.setDConcentrationInitiale(concInitsTmp[i]);
            }else{

              concInitsTmp[i] = concInits[i];
            }
          }
          if (listeTmp.size()> concInits.length) {
            existeDifference = true;
            for (int i = tailleMin; i < concInitsTmp.length; i++) {
              MetierConcentrationInitiale concInit = new MetierConcentrationInitiale();
              Hydraulique1dLigneConcentrationsInitialesTableau conc= (Hydraulique1dLigneConcentrationsInitialesTableau)listeTmp.get(i);
              conc.setDConcentrationInitiale(concInit);
              concInitsTmp[i] = concInit;
            }
          }
          else if (listeTmp.size()< concInits.length) {
            existeDifference = true;
             for (int i = tailleMin; i < concInits.length; i++) {
               concInits[i].dispose();
             }
          }

          if (existeDifference) {
            qualiteDEau_.concentrationsInitiales(concInitsTmp);
          }
          return existeDifference;
        }
  }
