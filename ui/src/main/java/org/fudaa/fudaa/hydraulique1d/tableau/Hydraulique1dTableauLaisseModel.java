/*
 * @file         Hydraulique1dTableauLaisseModel.java
 * @creation     15/07/04
 * @modification $Date: 2007-11-20 11:43:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierMesureCrueCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.MetierDonneesHydrauliques;
import org.fudaa.dodico.hydraulique1d.metier.MetierLaisse;
import org.fudaa.dodico.hydraulique1d.metier.MetierSite;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;

/**
 * Mod�le de tableau liste des laisses : (indice bief, abscisse, cote, nom).
 * Utilis� pour �diter les laisses de crues
 * @see Hydraulique1dTableauSiteModel
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.9 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 */
public class Hydraulique1dTableauLaisseModel
    extends Hydraulique1dTableauSiteModel {
  private final static String[] NOMS_COL = {
      getS("n� bief"), getS("abscisse"), getS("cote"), getS("nom")};
  private MetierDonneesHydrauliques donnesHydrau_;
  private MetierCalageAuto calage_;

  /**
   * Constructeur par d�faut avec 2 colonnes ("n� bief", "abscisse", "cote" et "nom")
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableauLaisseModel() {
    super(NOMS_COL);
  }

  /**
   * Retourne la classe de chaque colonne.
   * @param c L'indice de la colonne
   * @return Integer.class si c vaut 0, Double.class sinon.
   */
  @Override
  public Class getColumnClass(int c) {
    if (c == 0) {
      return Integer.class;
    }
    else if (c == 3) {
      return String.class;
    }
    else {
      return Double.class;
    }
  }

  /**
   * Modifie la valeur d'une cellule du tableau.
   * @param value La nouvelle valeur (Integer, Double, String ou null).
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   */
  @Override
  public void setValueAt(Object value, int row, int col) {
    if (col ==3) {
      Hydraulique1dLigneLaisseTableau lig = (Hydraulique1dLigneLaisseTableau)
          listePts_.get(row);
      if (value == null) lig.nom("");
      else lig.nom(value.toString());
      fireTableDataChanged();

    } else {
      super.setValueAt(value, row, col);
    }
  }
  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule (Type Integer si col vaut 0, type Double si col vaut 1 et 2, type String si col vaut 3), si la cellule est vide retourne nulle.
   */
  @Override
  public Object getValueAt(int row, int col) {
    Hydraulique1dLigneLaisseTableau lig = (Hydraulique1dLigneLaisseTableau) listePts_.get(row);
    if (col < 3) {
      return super.getValueAt(row, col);
    }
    return lig.nom();
  }
  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigneLaisseTableau.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigneLaisseTableau();
  }

  /**
   * Initialise le mod�le m�tier � partir des laisses des donn�es hydrauliques.
   * @param donnesHydrau MetierDonneesHydrauliques.
   */
  public void setModelMetier(MetierDonneesHydrauliques donnesHydrau) {
    donnesHydrau_ = donnesHydrau;
  }

  /**
   * Affectation du calage automatique.
   */
  public void setCalageAuto(MetierCalageAuto _cal) {
    calage_=_cal;
  }

  /**
   * L'importation des laisses depuis le calage est-il possible ?
   */
  public boolean isImportationCruesPossible() {
    return calage_==null ? false:calage_.crues().length>0;
  }

  /**
   * Importation depuis les crues du calage.
   */
  public void transfererCruesCalage() {
    listePts_.clear();

    int ibief=reseau_.biefs()[0].indice()+1;

    MetierCrueCalageAuto[] crues=calage_.crues();
    for (int i=0; i<crues.length; i++) {
      MetierMesureCrueCalageAuto[] mesures=crues[i].mesures();
      for (int j=0; j<mesures.length; j++) {
        double abscisse=mesures[j].abscisse();
        double cote=mesures[j].cote();
        String nom="ZCRUE_"+(i+1)+"_MESURE_"+(j+1);
        Hydraulique1dLigneLaisseTableau lig = (Hydraulique1dLigneLaisseTableau)creerLigneVide();
        lig.iBief(ibief);
        lig.absc(abscisse);
        lig.cote(cote);
        lig.nom(nom);
        listePts_.add(lig);
      }
    }
    fireTableDataChanged();
  }

  @Override
  public void setValeurs() {
    listePts_ = new ArrayList();
    MetierLaisse[] laisses = getLaisses();

    for (int i = 0; i < laisses.length; i++) {
      Hydraulique1dLigneLaisseTableau lig = (Hydraulique1dLigneLaisseTableau)creerLigneVide();
      lig.setMetier(laisses[i]);
      listePts_.add(lig);
    }

    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  @Override
  public boolean getValeurs() {
    List listeTmp = getListeLignesCorrectes();
    MetierLaisse[] laisses = getLaisses();
    boolean existeDifference = false;
    if (listeTmp.size() != laisses.length) {
      existeDifference = true;
    }
    MetierLaisse[] laissesTmp = new MetierLaisse[listeTmp.size()];
    int tailleMin = Math.min(listeTmp.size(), laisses.length);
    for (int i = 0; i < tailleMin; i++) {
      Hydraulique1dLigneLaisseTableau laisseTab = (Hydraulique1dLigneLaisseTableau)
          listeTmp.get(i);
      laissesTmp[i] = laisses[i];
      if (!laisseTab.equals(laissesTmp[i])) {
        existeDifference = true;
        initDLaisse(laisseTab, laissesTmp[i]);
      }
    }
    if (listeTmp.size() > laisses.length) {
      existeDifference = true;
      MetierLaisse[] nouveauxDLaisse = creeLaisses(listeTmp.size() - laisses.length);
      int iNouveauxDLaisse = 0;
      for (int i = tailleMin; i < laissesTmp.length; i++) {
        Hydraulique1dLigneLaisseTableau laisseTab = (Hydraulique1dLigneLaisseTableau)
            listeTmp.get(i);
        laissesTmp[i] = nouveauxDLaisse[iNouveauxDLaisse];
        initDLaisse(laisseTab, laissesTmp[i]);
        iNouveauxDLaisse++;
      }
    }
    else if (listeTmp.size() < laisses.length) {
      existeDifference = true;
      MetierLaisse[] laissesASupprimer = new MetierLaisse[laisses.length - tailleMin];
      int iLaissesASupprimer = 0;
      for (int i = tailleMin; i < laisses.length; i++) {
        laissesASupprimer[iLaissesASupprimer] = laisses[i];
        iLaissesASupprimer++;
      }
      supprimeLaisses(laissesASupprimer);
    }
    if (existeDifference) {
      miseAJourModeleMetier(laissesTmp);
    }
    return existeDifference;
  }

  public void importer() {
    List liste = Hydraulique1dImport.importListeLaisses(reseau_);
    if (liste != null) {
      listePts_ = liste;
      fireTableDataChanged();
    }
  }

  /**
   * R�cup�re le tableau de MetierLaisse.
   * @return MetierLaisse[]
   */
  private MetierLaisse[] getLaisses() {
    MetierLaisse[] laisses = new MetierLaisse[0];
    if (donnesHydrau_ != null) {
      laisses = donnesHydrau_.laisses();
    }
    return laisses;
  }

  /**
   * Initialise la laisse m�tier � partir d'une ligne tableau.
   * @param laisseTab Hydraulique1dLigneLaisseTableau
   * @param ilaisse MetierLaisse
   */
  private void initDLaisse(Hydraulique1dLigneLaisseTableau laisseTab, MetierLaisse ilaisse) {
    ilaisse.cote(laisseTab.cote());
    ilaisse.nom(laisseTab.nom());
    if (ilaisse.site() == null) {
      ilaisse.site(new MetierSite());
    }
    super.initDSite(laisseTab, ilaisse.site());
  }

  /**
   * Mise � jour des laisses des donn�es hydrauliques.
   * @param laisses MetierLaisse[]
   */
  public void miseAJourModeleMetier(MetierLaisse[] laisses) {
    if (donnesHydrau_ != null) {
      donnesHydrau_.laisses(laisses);
    }
  }

  /**
   * supprime les laisses de crues
   *
   * @param laissesASupprimer Le tableau de laisses � supprimer.
   */
  private void supprimeLaisses(MetierLaisse[] laissesASupprimer) {
    if (donnesHydrau_ != null) {
      donnesHydrau_.supprimeLaisses(laissesASupprimer);
    }
  }

  /**
   * cree des laisses de crues
   *
   * @param nb Le nombre de MetierLaisse � cr�er;
   * @return MetierLaisse[]
   */
  private MetierLaisse[] creeLaisses(int nb) {
    MetierLaisse[] laisses = new MetierLaisse[nb];
    for (int i = 0; i < laisses.length; i++) {
      laisses[i] = new MetierLaisse();
    }
    return laisses;
  }

}
