/*
 * @file         Hydraulique1dTableauLigneEauModel.java
 * @creation     22/12/00
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauPoint;
/**
 * Mod�le du tableau d'une ligne d'eau initiale.
 * Ent�te : "N� bief", "Abscisse", "Cote (m)","D�bit (m3/s)".
 * @see Hydraulique1dLigneLigneDEauTableau
 * @see Hydraulique1dTableauReelModel
 * @see org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale
 * @see org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauPoint
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.11 $ $Date: 2007-11-20 11:43:11 $ by $Author: bmarchan $
 */
public class Hydraulique1dTableauLigneEauModel extends Hydraulique1dTableau1EntierEtReelsModel {
   private final static String[] COLUMN_NAMES={ getS("N� bief"), getS("Abscisse"), getS("Cote (m)"),
                                   getS("D�bit (m3/s)") };

  /**
   * L'objet metier corba.
   */
  private MetierLigneEauInitiale iLigneEauInitiale_;

  /**
   * Constructeur par d�faut.
   */
  public Hydraulique1dTableauLigneEauModel() {
    super(COLUMN_NAMES, 20);
  }

   public void setModelMetier(MetierLigneEauInitiale iLigneEauInitiale) {
     iLigneEauInitiale_= iLigneEauInitiale;
   }

   /**
    * Cree une nouvelle ligne vide.
    * Surcharge de la classe m�re.
    * @return une instance de Hydraulique1dLigneLigneDEauTableau.
    */
  @Override
   public Hydraulique1dLigneReelTableau creerLigneVide() {
     return new Hydraulique1dLigneLigneDEauTableau();
   }

  @Override
   public void setValeurs() {
     listePts_= new ArrayList();
     MetierLigneEauPoint[] points = iLigneEauInitiale_.points();
     for (int i= 0; i < points.length; i++) {
       Hydraulique1dLigneLigneDEauTableau lig= new Hydraulique1dLigneLigneDEauTableau(points[i]);
       listePts_.add(lig);
     }
     for (int i= 0; i < getNbLignesVideFin(); i++) {
       listePts_.add(creerLigneVide());
     }
     fireTableDataChanged();
   }

  @Override
   public List getListePtsComplets() {
     List listeATrier = super.getListePtsComplets();
     Collections.sort(listeATrier);
     return listeATrier;
   }

  @Override
   public boolean getValeurs() {
     List listeTmp= getListePtsComplets();
     MetierLigneEauPoint[] points = iLigneEauInitiale_.points();
     boolean existeDifference = false;
     if (listeTmp.size() != points.length) {
       existeDifference = true;
     }
     MetierLigneEauPoint[] pointsTmp = new MetierLigneEauPoint[listeTmp.size()];
     int tailleMin = Math.min(listeTmp.size(), points.length);
     for (int i = 0; i < tailleMin; i++) {
       Hydraulique1dLigneLigneDEauTableau pt= (Hydraulique1dLigneLigneDEauTableau)listeTmp.get(i);
       pointsTmp[i] = points[i];
       if (!pt.equals(pointsTmp[i])) {
         existeDifference = true;
         pt.getIligneEauPoint(pointsTmp[i]);
       }
     }
     if (listeTmp.size()> points.length) {
       existeDifference = true;
       MetierLigneEauPoint[] nouvellesILignePt =  iLigneEauInitiale_.creePoints(listeTmp.size()- points.length);
       int iNouvellesILignePt = 0;
       for (int i = tailleMin; i < pointsTmp.length; i++) {
         Hydraulique1dLigneLigneDEauTableau pt= (Hydraulique1dLigneLigneDEauTableau)listeTmp.get(i);
         pointsTmp[i] = nouvellesILignePt[iNouvellesILignePt];
         pt.getIligneEauPoint(pointsTmp[i]);
         iNouvellesILignePt++;
       }
     }
     else if (listeTmp.size()< points.length) {
       existeDifference = true;
       for (int i = tailleMin; i < points.length; i++) {
         iLigneEauInitiale_.supprimePoint(points[i]);
       }
     }
     if (existeDifference) {
       iLigneEauInitiale_.points(pointsTmp);
     }
     return existeDifference;
   }
}
