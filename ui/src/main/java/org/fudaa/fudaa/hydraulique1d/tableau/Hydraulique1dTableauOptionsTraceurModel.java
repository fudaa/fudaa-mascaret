package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierOptionTraceur;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresModeleQualiteEau;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresQualiteDEau;

/**
 * Mod�le du tableau des options traceurs.
 * Ent�te : "Nom du traceur","Diffusion","Convection" ...
 * @author Olivier Pasteur
 * @version 1.
 */
public class Hydraulique1dTableauOptionsTraceurModel extends
        Hydraulique1dTableauChaineEtBooleensModel {
    private final static String[] COLUMN_NAMES={ getS("Nom du traceur"), getS("Diffusion"), getS("Convection")};

    private MetierParametresQualiteDEau qualiteDEau_;

    /**
     * Constructeur par d�faut.
     */
    public Hydraulique1dTableauOptionsTraceurModel() {
      super(COLUMN_NAMES, 0);
    }



     /**
    * Determine le nombre de reel dans le tableau
    *
    * @return int
    */
   public int nbReels() {
     return getColumnCount()-1;
   }

     /**
      * Cree une nouvelle ligne vide.
      * Surcharge de la classe m�re.
      * @return une instance de Hydraulique1dLigneConcentrationsInitialesTableau.
      */
  @Override
     public Hydraulique1dLigneBooleensTableau creerLigneVide() {

       return new Hydraulique1dLigneOptionsTraceurTableau(nbReels());
     }

     public void setValeurs(MetierParametresQualiteDEau qualiteDEau) {
         qualiteDEau_=qualiteDEau;
         MetierParametresModeleQualiteEau modeleQE = qualiteDEau.parametresModeleQualiteEau();
         MetierOptionTraceur[] optTraceur_= qualiteDEau.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers();
         String[][] nomTraceurs = modeleQE.vvNomTracer();
         String[] columnNames= new String[3];
         columnNames[0] = getS("Nom du traceur");
         columnNames[1] = getS("Diffusion");
         columnNames[2] = getS("Convection");
         setColumnNames(columnNames);

       listePts_= new ArrayList();
       for (int i= 0; i < optTraceur_.length; i++) {
         Hydraulique1dLigneOptionsTraceurTableau lig= new Hydraulique1dLigneOptionsTraceurTableau(nomTraceurs[i][0],optTraceur_[i].diffusionDuTraceur(),optTraceur_[i].convectionDuTraceur());
         listePts_.add(lig);
       }
       for (int i= 0; i < getNbLignesVideFin(); i++) {
         listePts_.add(creerLigneVide());
       }
       fireTableDataChanged();
     }

  @Override
     public List getListePtsComplets() {
       List listeATrier = super.getListePtsComplets();
       Collections.sort(listeATrier);
       return listeATrier;
     }

     /**
      * Retourne si la cellule est �ditable.
      * @param row Indice de la ligne de la cellule.
      * @param col Indice de la colonne de la cellule.
      */
  @Override
     public boolean isCellEditable(int row, int col) {
       if (col == 0) return false;
       else return true;
     }

     public boolean[] concatBooleanTabBoolean(boolean d, boolean[] c) {
       boolean[] res = new boolean[c.length+1];
       res[0]=d;
    System.arraycopy(c, 0, res, 1, c.length);
       return res;
     }

  @Override
     public boolean getValeurs() {
         List listeTmp= getListePtsComplets();
         MetierOptionTraceur[] optTraceur = qualiteDEau_.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers();
         boolean existeDifference = false;
          if (listeTmp.size() != optTraceur.length) {
            existeDifference = true;
          }
          MetierOptionTraceur[] optTraceurTmp = new MetierOptionTraceur[listeTmp.size()];
          int tailleMin = Math.min(listeTmp.size(), optTraceur.length);
          for (int i = 0; i < tailleMin; i++) {
            Hydraulique1dLigneOptionsTraceurTableau optLigne= (Hydraulique1dLigneOptionsTraceurTableau)listeTmp.get(i);

            if (!optLigne.equals(optTraceur[i])) {
              existeDifference = true;
              optTraceurTmp[i] = new MetierOptionTraceur();
              optLigne.setIOptionTracer(optTraceurTmp[i]);
            }else{

              optTraceurTmp[i] = optTraceur[i];
            }
          }
          if (listeTmp.size()> optTraceur.length) {
            existeDifference = true;
            for (int i = tailleMin; i < optTraceurTmp.length; i++) {
              MetierOptionTraceur optTrac = new MetierOptionTraceur();
              Hydraulique1dLigneOptionsTraceurTableau optLigne= (Hydraulique1dLigneOptionsTraceurTableau)listeTmp.get(i);
              optLigne.setIOptionTracer(optTrac);
              optTraceurTmp[i] = optTrac;
            }
          }
          else if (listeTmp.size()< optTraceur.length) {
            existeDifference = true;
             for (int i = tailleMin; i < optTraceur.length; i++) {
                 optTraceur[i].dispose();
             }
          }

          if (existeDifference) {
            qualiteDEau_.parametresGenerauxQualiteDEau().parametresConvecDiffu().optionDesTracers(optTraceurTmp);
          }
          return existeDifference;
        }


}
