package org.fudaa.fudaa.hydraulique1d.tableau;


import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParamPhysTracer;
import org.fudaa.dodico.hydraulique1d.metier.qualitedeau.MetierParametresQualiteDEau;


/**
 * Mod�le du tableau des param�tres physiques pour la qualit� d'eau.
 * Ent�te : "Nom du param�tre", "Valeur" ...
 * @see Hydraulique1dTableauReelModel
 * @author Olivier Pasteur
 * @version 1.0
 */

public class Hydraulique1dTableauParamsPhysQEModel extends
        Hydraulique1dTableauChaineEtReelsModel {
    private final static String[] COLUMN_NAMES={ getS("Nom"), getS("Valeur")};

    private MetierParametresQualiteDEau qualiteDEau_;


    /**
     * Constructeur par d�faut.
     */
    public Hydraulique1dTableauParamsPhysQEModel() {
      super(COLUMN_NAMES, 0);
    }



     /**
    * Determine le nombre de reel dans le tableau
    *
    * @return int
    */
   public int nbReels() {
     return getColumnCount()-1;
   }

     /**
      * Cree une nouvelle ligne vide.
      * Surcharge de la classe m�re.
      * @return une instance de Hydraulique1dLigneParamsPhysiqueQETableau.
      */
  @Override
     public Hydraulique1dLigneReelTableau creerLigneVide() {

       return new Hydraulique1dLigneParamsPhysiqueQETableau(nbReels());
     }

     public void setValeurs(MetierParametresQualiteDEau params) {
    	 qualiteDEau_=params;
    	 MetierParamPhysTracer[] paramsPhyQE=params.parametresGenerauxQualiteDEau().paramsPhysTracer();
    	 setValeurs(paramsPhyQE);
     }

     public void setValeurs(MetierParamPhysTracer[] paramsPhyQE) {
         String[] columnNames= new String[2];
         columnNames[0] = "                                           "+getS("Nom")+"                                          ";
         columnNames[1] = getS("Valeur");
         setColumnNames(columnNames);

       listePts_= new ArrayList();
       for (int i= 0; i < paramsPhyQE.length; i++) {
         Hydraulique1dLigneParamsPhysiqueQETableau param= new Hydraulique1dLigneParamsPhysiqueQETableau(paramsPhyQE[i].nomParamPhys(),paramsPhyQE[i].valeurParamPhys());
                listePts_.add(param);
     }
       for (int i= 0; i < getNbLignesVideFin(); i++) {
         listePts_.add(creerLigneVide());
       }
       fireTableDataChanged();
     }

     public void ajouterLigne(String nom, double valeur) {

       listePts_.add(new Hydraulique1dLigneParamsPhysiqueQETableau(nom,valeur));
       fireTableDataChanged();
     }


     public void supprimerNDernieresLignes(int n) {
    	 for (int i = 0; i <n; i++) {
    	        listePts_.remove(listePts_.size()-1);
		}
      fireTableDataChanged();

     }


  @Override
     public List getListePtsComplets() {
       List listeATrier = super.getListePtsComplets();
       //Collections.sort(listeATrier);
       return listeATrier;
     }





     public double[] concatDoubleTabDouble(double d, double[] c) {
       double[] res = new double[c.length+1];
       res[0]=d;
    System.arraycopy(c, 0, res, 1, c.length);
       return res;
     }

  @Override
     public boolean getValeurs() {
         List listeTableauActuelle= getListePtsComplets();
         MetierParamPhysTracer[] paramsPhyQE=qualiteDEau_.parametresGenerauxQualiteDEau().paramsPhysTracer();
         boolean existeDifference = false;
          if (listeTableauActuelle.size() != paramsPhyQE.length) {
            existeDifference = true;
          }
          MetierParamPhysTracer[] newParamsPhysiques = new MetierParamPhysTracer[listeTableauActuelle.size()];
          int tailleMin = Math.min(listeTableauActuelle.size(), paramsPhyQE.length);
          for (int i = 0; i < tailleMin; i++) {
            Hydraulique1dLigneParamsPhysiqueQETableau param= (Hydraulique1dLigneParamsPhysiqueQETableau)listeTableauActuelle.get(i);

            if (!param.equals(paramsPhyQE[i])) {
              existeDifference = true;
              newParamsPhysiques[i] = new MetierParamPhysTracer();
              param.setDParamPhysTracer(newParamsPhysiques[i]);
            }else{

              newParamsPhysiques[i] = paramsPhyQE[i];
            }
          }
          if (listeTableauActuelle.size()> paramsPhyQE.length) {
            existeDifference = true;
            for (int i = tailleMin; i < newParamsPhysiques.length; i++) {
              MetierParamPhysTracer paramInit = new MetierParamPhysTracer();
              Hydraulique1dLigneParamsPhysiqueQETableau param= (Hydraulique1dLigneParamsPhysiqueQETableau)listeTableauActuelle.get(i);
              param.setDParamPhysTracer(paramInit);
              newParamsPhysiques[i] = paramInit;
            }
          }
          else if (listeTableauActuelle.size()< paramsPhyQE.length) {
            existeDifference = true;
             for (int i = tailleMin; i < paramsPhyQE.length; i++) {
                 paramsPhyQE[i].dispose();
             }
          }

          if (existeDifference) {
            qualiteDEau_.parametresGenerauxQualiteDEau().paramsPhysTracer(newParamsPhysiques);
          }
          return existeDifference;
        }


     /**
      * Retourne si la cellule est �ditable.
      * @param row Indice de la ligne de la cellule.
      * @param col Indice de la colonne de la cellule.
      */
  @Override
     public boolean isCellEditable(int row, int col) {
       if (col == 0) return false;
       else return true;
     }

}
