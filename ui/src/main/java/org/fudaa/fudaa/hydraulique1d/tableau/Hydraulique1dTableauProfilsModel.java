/*
 * @file         Hydraulique1dTableauProfilsModel.java
 * @creation     2004-07-16
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2005 EDF/OPP
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierProfil;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel;

/**
 * Definit le mod�le de tableau repr�sentant un profil (nom + abscisse). Utilis� dans l'�diteur de Bief.
 *
 * @see org.fudaa.fudaa.hydraulique1d.Hydraulique1dProfilModel
 * @see org.fudaa.dodico.hydraulique1d.metier.MetierBief
 * @see org.fudaa.dodico.hydraulique1d.metier.MetierProfil
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.12 $ $Date: 2007-11-20 11:43:11 $ by $Author: bmarchan $
 */
public class Hydraulique1dTableauProfilsModel extends Hydraulique1dTableauChaineEtReelsModel {

  private MetierBief bief_;

  public Hydraulique1dTableauProfilsModel() {
    super();
  }

  public void setModeleMetier(MetierBief bief) {
    bief_ = bief;
  }

  @Override
  public void setValeurs() {
	  if(bief_ == null)
		  return;
    MetierProfil[] profils = bief_.profils();
    listePts_ = new ArrayList(profils.length + getNbLignesVideFin());

    for (int i = 0; i < profils.length; i++) {
      Hydraulique1dProfilModel profilModel = new Hydraulique1dProfilModel(profils[i]);
      Hydraulique1dLigneProfilTableau lig = new Hydraulique1dLigneProfilTableau(profilModel);
      listePts_.add(lig);
    }

    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  public void setProfilsModel(Hydraulique1dProfilModel[] profilsModel) {
    listePts_ = new ArrayList(profilsModel.length + getNbLignesVideFin());

    for (int i = 0; i < profilsModel.length; i++) {
      Hydraulique1dProfilModel profilModel = profilsModel[i];
      Hydraulique1dLigneProfilTableau lig = new Hydraulique1dLigneProfilTableau(profilModel);
      listePts_.add(lig);
    }

    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  public List enleveProfilsNomVideOuAbscisseInfini(List listeProfil) {
    List listeRes = new ArrayList(listeProfil.size());
    for (Iterator iter = listeProfil.iterator(); iter.hasNext();) {
      Hydraulique1dLigneProfilTableau ligne = (Hydraulique1dLigneProfilTableau) iter.next();
      if (!(("".equals(ligne.nom())) || (ligne.getValue(0) == null))) {
        listeRes.add(ligne);
      } else {
        System.out.println("suppression de la ligne :" + ligne);
      }
    }

    return listeRes;
  }

  public MetierProfil[] getCurrentMetierProfil() {
    List listeTmp0 = getListePtsPasToutVide();
    List listeTmp = enleveProfilsNomVideOuAbscisseInfini(listeTmp0);
    Collections.sort(listeTmp);
    MetierProfil[] profilsTmp = new MetierProfil[listeTmp.size()];
    for (int i = 0; i < profilsTmp.length; i++) {
      Hydraulique1dLigneProfilTableau profTab = (Hydraulique1dLigneProfilTableau) listeTmp.get(i);
      profilsTmp[i] = new MetierProfil();
      initDProfil(profTab, profilsTmp[i]);
    }
    return profilsTmp;

  }

  @Override
  public boolean getValeurs() {
	  if(bief_ == null)
		  return false;
    List listeTmp0 = getListePtsPasToutVide();
    List listeTmp = enleveProfilsNomVideOuAbscisseInfini(listeTmp0);
    Collections.sort(listeTmp);
    MetierProfil[] profils = bief_.profils();
    boolean existeDifference = false;
    if (listeTmp.size() != profils.length) {
      existeDifference = true;
    }
    MetierProfil[] profilsTmp = new MetierProfil[listeTmp.size()];
    int tailleMin = Math.min(listeTmp.size(), profils.length);
    for (int i = 0; i < tailleMin; i++) {
      Hydraulique1dLigneProfilTableau profTab = (Hydraulique1dLigneProfilTableau) listeTmp.get(i);
      profilsTmp[i] = profils[i];
      if (!profTab.equals(profilsTmp[i])) {
        existeDifference = true;
        initDProfil(profTab, profilsTmp[i]);
      }
    }
    if (listeTmp.size() > profils.length) {
      existeDifference = true;
      MetierProfil[] nouveauxDProfils = creeProfils(listeTmp.size() - profils.length);
      int iNouveauxDProfil = 0;
      for (int i = tailleMin; i < profilsTmp.length; i++) {
        Hydraulique1dLigneProfilTableau profTab = (Hydraulique1dLigneProfilTableau) listeTmp.get(i);
        profilsTmp[i] = nouveauxDProfils[iNouveauxDProfil];
        initDProfil(profTab, profilsTmp[i]);
        iNouveauxDProfil++;
      }
    } else if (listeTmp.size() < profils.length) {
      existeDifference = true;
      MetierProfil[] profilsASupprimer = new MetierProfil[profils.length - tailleMin];
      int iProfilsASupprimer = 0;
      for (int i = tailleMin; i < profils.length; i++) {
        profilsASupprimer[iProfilsASupprimer] = profils[i];
        iProfilsASupprimer++;
      }
      supprimeProfils(profilsASupprimer);
    }
    if (existeDifference) {
      miseAJourModeleMetier(profilsTmp);
    }

    return existeDifference;
  }

  /**
   * Cree une nouvelle ligne vide. Surcharge de la classe m�re.
   *
   * @return une instance de Hydraulique1dLigneProfilTableau.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigneProfilTableau();
  }

  public void dupliquer(int[] indices) {
    for (int i = (indices.length - 1); i >= 0; i--) {
      dupliquer(indices[i]);
    }
    fireTableDataChanged();
  }

  private void dupliquer(int indice) {
    Hydraulique1dLigneProfilTableau lig = (Hydraulique1dLigneProfilTableau) listePts_.get(indice);
    if (lig.profilModel() != null) {
      Hydraulique1dProfilModel profModel = lig.profilModel();
      Hydraulique1dProfilModel profModelBis = (Hydraulique1dProfilModel) profModel.clone();
      profModelBis.nom(profModelBis.nom() + "bis");
      Hydraulique1dLigneProfilTableau ligBis = new Hydraulique1dLigneProfilTableau(profModelBis);
      listePts_.add(indice + 1, ligBis);
    }
  }

  public Hydraulique1dProfilModel[] getTabProfilModel(int[] indices) {
    ArrayList listProf = new ArrayList(indices.length);
    for (int i = 0; i < indices.length; i++) {
      Hydraulique1dLigneProfilTableau lig = (Hydraulique1dLigneProfilTableau) listePts_.get(indices[i]);
      if (!lig.isExisteNulle()) {
        listProf.add(lig.profilModel());
      }
    }
    return (Hydraulique1dProfilModel[]) listProf.toArray(new Hydraulique1dProfilModel[listProf.size()]);
  }

  public Hydraulique1dProfilModel[] getTabProfilModel() {
    ArrayList listProf = new ArrayList();
    Iterator iter = listePts_.iterator();
    while (iter.hasNext()) {
      Hydraulique1dLigneProfilTableau lig = (Hydraulique1dLigneProfilTableau) iter.next();
      if (!lig.isExisteNulle()) {
        listProf.add(lig.profilModel());
      }
    }
    return (Hydraulique1dProfilModel[]) listProf.toArray(new Hydraulique1dProfilModel[listProf.size()]);
  }

  public void editer(int[] indices) {
    for (int i = 0; i < indices.length; i++) {
      editer(indices[i]);
    }
  }

  private void editer(int indice) {
    Hydraulique1dLigneProfilTableau lig = (Hydraulique1dLigneProfilTableau) listePts_.get(indice);
    if (lig.profilModel() != null) {
      //TODO: FD utile ?
      /*Hydraulique1dProfilModel profModel = */
      lig.profilModel();
    }
  }

  /**
   * Initialise le profil m�tier � partir d'une ligne tableau.
   *
   * @param profilTab Hydraulique1dLigneProfilTableau
   * @param iprofil DProfil
   */
  protected static void initDProfil(Hydraulique1dLigneProfilTableau profilTab, MetierProfil iprofil) {
    iprofil.abscisse(profilTab.abscisse());
    iprofil.nom(profilTab.nom());
    iprofil.avecGeoreferencement(profilTab.avecGeoReferencement());
    iprofil.infosGeoReferencement(profilTab.infoGeoreferencement());
    if (profilTab.profilModel() != null) {
      iprofil.points(profilTab.profilModel().pointsNonNuls());
      iprofil.indiceLitMajGa(profilTab.profilModel().getIndiceLitMajGa());
      iprofil.indiceLitMinGa(profilTab.profilModel().getIndiceLitMinGa());
      iprofil.indiceLitMinDr(profilTab.profilModel().getIndiceLitMinDr());
      iprofil.indiceLitMajDr(profilTab.profilModel().getIndiceLitMajDr());
    }
  }

  /**
   * miseAJourModeleMetier
   */
  private void miseAJourModeleMetier(MetierProfil[] profils) {
    if (bief_ != null) {
      bief_.profils(profils);
    }
  }

  /**
   * supprimePoint
   *
   * @param iProfil DProfil
   */
  private void supprimeProfils(MetierProfil[] profilsASupprimer) {
    if (bief_ != null) {
      bief_.supprimeProfils(profilsASupprimer);
    }
  }

  /**
   * creeProfils
   *
   * @param nb Le nombre de DProfil � cr�er;
   * @return DProfil[]
   */
  private MetierProfil[] creeProfils(int nb) {
    MetierProfil[] profils = new MetierProfil[nb];
    for (int i = 0; i < profils.length; i++) {
      profils[i] = new MetierProfil();
    }
    return profils;
  }
}
