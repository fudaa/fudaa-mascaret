/*
 * @file         Hydraulique1dTableauReel.java
 * @creation     2003-11-28
 * @modification $Date: 2006-09-08 16:04:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2003 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.TableModel;

import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * Tableau contenant plusieurs colonnes de r�els �ditable.
 * @see Hydraulique1dLigneReelTableau
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.13 $ $Date: 2006-09-08 16:04:25 $ by $Author: opasteur $
 */
public class Hydraulique1dTableauReel extends Hydraulique1dTableau {
  //List listeDoubleClickListener_= new ArrayList(1);

  /**
   * Constructeur par d�faut avec Hydraulique1dTableauReelModel comme modele.
   */
  public Hydraulique1dTableauReel() {
    this(new Hydraulique1dTableauReelModel());
  }
  /**
   * Constructeur avec un modele en argument.
   * @param model Hydraulique1dTableauReelModel
   */
  public Hydraulique1dTableauReel(Hydraulique1dTableauReelModel model) {
    super(model);
  }

  /**
   * Notifie tous les �couteurs de "double click" sur une ligne.
   * la m�thode "actionDoubleClick(ActionDoubleClickEvent event)" des �couteurs est ex�cut�.
   */
  @Override
  public void fireDoubleClickLigne() {

    int[] indexes= getSelectedRows();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauReelModel) {
      Hydraulique1dTableauReelModel modele= (Hydraulique1dTableauReelModel)m;
      if (indexes.length >0) {
        Hydraulique1dLigneReelTableau ligne= (Hydraulique1dLigneReelTableau)modele.listePts_.get(indexes[0]);
        ActionDoubleClickEvent event = new ActionDoubleClickEvent(this, ligne);
        Iterator ite = listeDoubleClickListener_.iterator();
        while(ite.hasNext()) {
          ((ActionDoubleClickListener)ite.next()).actionDoubleClick(event);
        }
      }
    }
  }

  /**
   * Si aucune ligne est s�lectionn�e, cette m�thode ajoute une ligne � la fin.
   * Si des lignes sont s�lectionn�es, cette m�thode ajoute des lignes � la place des lignes s�lectionn�es.
   */
  public void ajouterLigne() {
    int[] indexes= getSelectedRows();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauReelModel) {
      Hydraulique1dTableauReelModel modele= (Hydraulique1dTableauReelModel)m;
      if (indexes.length == 0) {
        modele.ajouterLigne();
      } else {
        for (int i=0; i<indexes.length; i++) {
          modele.ajouterLigne(indexes[i]);
        }
      }
    }
  }
  /**
   * Si aucune colonne est s�lectionn�e, cette m�thode ajoute une colonne � la fin.
   * Si des colonnes sont s�lectionn�es, cette m�thode ajoute des colonnes � la place des colonnes s�lectionn�es.
   * @param nomColonne Le nom de la colonne � ajouter.
   */
  public void ajouterColonne(String nomColonne) {
    int[] indexes= getSelectedColumns();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauReelModel) {
      Hydraulique1dTableauReelModel modele= (Hydraulique1dTableauReelModel)m;
      if (indexes.length == 0) {
        modele.ajouterColonne(nomColonne);
      } else {
        for (int i=0; i<indexes.length; i++) {
          modele.ajouterColonne(nomColonne+indexes[i], indexes[i]);
        }
      }
    }
  }

  /**
   * Supprime les �ventuelles lignes s�lectionn�es.
   */
  @Override
  public void supprimeLignesSelectionnees() {
    int[] indexes= getSelectedRows();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauReelModel) {
      Hydraulique1dTableauReelModel modele= (Hydraulique1dTableauReelModel)m;
      for (int i= (indexes.length - 1); i >= 0; i--) {
        modele.supprimerLigne(indexes[i]);
      }
    }
  }

  /**
   * Supprime les �ventuelles colonnes s�lectionn�es.
   */
  public void supprimeColonnesSelectionnees() {
    int[] indexes= getSelectedColumns();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauReelModel) {
      Hydraulique1dTableauReelModel modele= (Hydraulique1dTableauReelModel)m;
      for (int i= (indexes.length - 1); i >= 0; i--) {
        modele.supprimerColonne(indexes[i]);
      }
    }
  }

  @Override
  public void cut() {
    copy();
    effaceSelectedValues();
  }
  /**
   * Efface les �ventuelles lignes s�lectionn�es.
   */
  @Override
  public void effaceLignesSelectionnees() {
    int[] indexes= getSelectedRows();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauReelModel) {
      Hydraulique1dTableauReelModel modele= (Hydraulique1dTableauReelModel)m;
      for (int i= (indexes.length - 1); i >= 0; i--) {
        modele.effacerLigne(indexes[i]);
      }
    }
  }
  public void effaceSelectedValues()
  {
    int[]   sr =getSelectedRows();
    int[]   sc =getColumnModel().getSelectedColumns();
    boolean rsa=getRowSelectionAllowed();
    boolean csa=getColumnSelectionAllowed();

    int[][] r=new int[rsa ? sr.length : getRowCount()]
                           [csa ? sc.length : getColumnCount()];

    for(int i=0;i<r.length;i++)
      for(int j=0;j<r[i].length;j++)
        getModel().setValueAt(null,rsa ? sr[i] : i, csa ? sc[j] : j);

  }
  /**
   * Efface les �ventuelles colonnes s�lectionn�es.
   */
  public void effaceColonnesSelectionnees() {
    int[] indexes= getSelectedColumns();
    TableModel m= getModel();
    if (m instanceof Hydraulique1dTableauReelModel) {
      Hydraulique1dTableauReelModel modele= (Hydraulique1dTableauReelModel)m;
      for (int i= (indexes.length - 1); i >= 0; i--) {
        modele.effacerColonne(indexes[i]);
      }
    }
  }
  public static void main(String[] arg) {
    String[] nomsColonnes = {"Nom col1","Nom col2","Nom col2"};
    Hydraulique1dTableauReelModel modelTab = new Hydraulique1dTableauReelModel(nomsColonnes,10);
    double[][] tab ={{1.1,1.2,1.3},{2.1,2.2,2.3}};
    modelTab.setTabDouble(tab);

    JFrame frame = new JFrame("Test Tableau Reel");

    tabReel = new Hydraulique1dTableauReel(modelTab);
    JButton ecritExcel = new JButton("Ecrit Excel");

    ecritExcel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        exportTableau();
      }
    });

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JPanel panel = new JPanel(new BorderLayout());

    panel.add(new JScrollPane(tabReel), BorderLayout.CENTER);
    panel.add(ecritExcel, BorderLayout.SOUTH);


    frame.getContentPane().add(panel);
    frame.pack();
    frame.setVisible(true);
  }
  private static Hydraulique1dTableauReel tabReel;
  final static void exportTableau() {
    try {
     WritableWorkbook classeur = Workbook.createWorkbook(new File("TestTableau.xls"));

     WritableSheet feuille =classeur.createSheet("Tableau",0);
     tabReel.ecritSurFeuilleExcel(feuille, 0,0);


     classeur.write();
     classeur.close();
   }
   catch (Exception ex) {
     ex.printStackTrace();
   }
  }
}
