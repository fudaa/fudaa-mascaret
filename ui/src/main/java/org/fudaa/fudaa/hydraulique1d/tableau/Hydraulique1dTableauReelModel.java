/*
 * @file         Hydraulique1dTableauReelModel.java
 * @creation     2003-11-28
 * @modification $Date: 2005-07-01 16:51:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2003 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.fudaa.ctulu.CtuluLibArray;
/**
 * Mod�le de tableau contenant plusieurs colonnes de r�els �ditable.
 * @see Hydraulique1dLigneReelTableau
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.15 $ $Date: 2005-07-01 16:51:48 $ by $Author: jm_lacombe $
 */
public class Hydraulique1dTableauReelModel extends Hydraulique1dAbstractTableauModel {

  /**
   * Indique le nombre de ligne vide rajout� � la fin.
   * Par d�faut, ce nombre est �gale � 20.
   */
  private int nbLignesVideFin_=20;
  /**
   * La liste des lignes du tableau (Hydraulique1dLigneReelTableau).
   */
  protected List listePts_;
  /**
   * Constructeur par d�faut.
   * Initialise la liste de ligne.
   */
  public Hydraulique1dTableauReelModel() {
    super();
    listeColumnNames_.add("X");
    listeColumnNames_.add("Y");
    listeColumnNames_.add("Z");
    listePts_= new ArrayList();
  }

  /**
   * Constructeur pr�cisant les noms de colonnes et le nombre de lignes vides � la fin.
   * @param columnNames le tableau des noms de colonnes.
   * @param nbLignesVideFin le nombre de lignes vides � la fin.
   */
  public Hydraulique1dTableauReelModel(String[] columnNames, int nbLignesVideFin) {
    super(columnNames);
    listePts_= new ArrayList();
    nbLignesVideFin_=nbLignesVideFin;
  }
  /**
   * @return le nombre de ligne.
   */
  @Override
  public int getRowCount() {
    return listePts_.size();
  }

  /**
   * Retourne le nombre de lignes vides � la fin du tableau.
   * @return le nombre de lignes vides.
   */
  public int getNbLignesVideFin() {
    return nbLignesVideFin_;
  }

  /**
   * Retourne la classe (Class) de type Double
   * @param col l'indice de la colonne
   * @return la classe Double
   */
  @Override
  public Class getColumnClass(int col) {
    return Double.class;
  }
  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule (Type Double ou null si cellule vide).
   */
  @Override
  public Object getValueAt(int row, int col) {
    Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)listePts_.get(row);
    return lig.getValue(col);
  }
  /**
   * Retourne la valeur d'une cellule du tableau.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return La valeur de la cellule ( Double.POSITIVE_INFINITY si cellule vide).
   */
  public double valueAt(int row, int col) {
    return Hydraulique1dLigneReelTableau.doubleValue(getValueAt(row, col));
  }
  /**
   * Retourne si la cellule est �ditable.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return Vrai.
   */
  @Override
  public boolean isCellEditable(int row, int col) {
    return true;
  }
  /**
   * Modifie la valeur d'une cellule du tableau.
   * @param value La nouvelle valeur (Double ou null).
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   */
  @Override
  public void setValueAt(Object value, int row, int col) {
    Double valeur= (Double)value;
    Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)listePts_.get(row);
    lig.setValue(col, valeur);
    fireTableCellUpdated(row,col);
  }

  /**
   * Retourne un tableau de r�els avec les 2 premi�res colonnes non vide.
   * Si les colonnes suivants sont vides, la valeur sera Double.POSITIVE_INFINITY.
   * @return Le tableau de r�els [indice colonne][indice ligne].
   */
  public double[][] getTabDouble() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
      if ((lig.X() != null) && (lig.Y() != null)) {
        listeTmp.add(lig);
      }
    }
    int nbLigne= listeTmp.size();
    int nbColonne = getColumnCount();
    double[][] res= new double[nbLigne][nbColonne];
    for (int i= 0; i < nbLigne; i++) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)listeTmp.get(i);
      for (int j = 0; j < nbColonne; j++) {
        res[i][j]= lig.value(j);
      }
    }
    return res;
  }
  public double[] getTab1DDoubleComplet() {
    List listeTmp= getListePtsComplets();
    List listeRes = new ArrayList();
    int taille= listeTmp.size();
    for (int i= 0; i < taille; i++) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)listeTmp.get(i);
      for (int j = 0; j < lig.getTaille(); j++) {
        listeRes.add(lig.getValue(j));
      }
    }
    double[] tRes = new double[listeRes.size()];
    for (int i = 0; i < tRes.length; i++) {
      tRes[i] = ((Double)listeRes.get(i)).doubleValue();
    }
    return tRes;
  }
  /**
   * Retourne un tableau de r�els des lignes completes.
   * @return Le tableau de r�els [indice ligne][indice colonne].
   */
  public double[][] getTabDoubleComplet() {
    List listeTmp= getListePtsComplets();

    int nbLigne= listeTmp.size();
    int nbColonne = getColumnCount();
    double[][] res= new double[nbLigne][nbColonne];
    for (int i= 0; i < nbLigne; i++) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)listeTmp.get(i);
      for (int j = 0; j < nbColonne; j++) {
        res[i][j]= lig.value(j);
      }
    }
    return res;
  }
  /**
   * Cree une nouvelle ligne vide.
   * Doit �tre surcharg� dans le cas o� on utilise des lignes filles
   * de de la classe Hydraulique1dLigneReelTableau.
   * @return Hydraulique1dLigneReelTableau
   */
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigneReelTableau(getColumnCount());
  }
  /**
   * V�rifie les bonnes dimension du tableau.
   * @param tableau de r�els [indice colonne][indice ligne].
   * @return boolean Vrai si OK
   */
  private boolean verifieDimTableau(double[][] tableau) {
    for (int i = 1; i < tableau.length; i++) {
      if (tableau[0].length != tableau[i].length) {
        return false;
      }
    }
    return true;
  }
  /**
   * Retourne un tableau de r�els avec les 2 premi�res colonnes non vide.
   * Si les colonnes suivants sont vides, la valeur sera Double.POSITIVE_INFINITY.
   * @param tableau de r�els [indice ligne][indice colonne].
   */
  public void setTabDouble(double[][] tableau) {
    if (!verifieDimTableau(tableau)) return;
    int nbLigne = tableau.length;

    int nbColonne = getColumnCount();
    ArrayList liste= new ArrayList(nbLigne+getNbLignesVideFin());
    for (int i = 0; i < nbLigne; i++) {
      Hydraulique1dLigneReelTableau lig = creerLigneVide();
      liste.add(lig);
      for (int j = 0; j < nbColonne; j++) {
        lig.setValue(j, tableau[i][j]);
      }
    }
    for (int i = 0; i < getNbLignesVideFin(); i++) {
      Hydraulique1dLigneReelTableau lig = creerLigneVide();
      liste.add(lig);
    }
    listePts_=liste;
    fireTableStructureChanged();
  }
  /**
   * Retourne la liste de ligne contenant aucune cellule vide.
   * @return La liste de Hydraulique1dLigneReelTableau contenant aucune cellule vide.
   */
  public List getListePtsComplets() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
      if (!lig.isExisteNulle()) {
        listeTmp.add(lig);
      }
    }
    return listeTmp;
  }
  /**
   * Retourne la liste de ligne ne contenant pas que des cellules vides.
   * @return La liste de Hydraulique1dLigneReelTableau ne contenant pas que des cellules vides.
   */
  public List getListePtsPasToutVide() {
    ArrayList listeTmp= new ArrayList(listePts_.size());
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
      if (!lig.isToutNulle()) {
        listeTmp.add(lig);
      }
    }
    return listeTmp;
  }
  /**
   * Ajoute une ligne vide la fin du tableau.
   */
  public void ajouterLigne() {
    listePts_.add(creerLigneVide());
    fireTableDataChanged();
  }
  /**
   * Ajoute une ligne vide dans le tableau.
   * @param row Indice de la ligne � ajouter.
   */
  public void ajouterLigne(int row) {
    listePts_.add(row, creerLigneVide());
    fireTableDataChanged();
  }
  /**
   * Supprime une ligne du tableau.
   * @param row Indice de la ligne � supprimer.
   */
  public void supprimerLigne(int row) {
    listePts_.remove(row);
    fireTableDataChanged();
  }
  /**
   * Efface une ligne du tableau.
   * La ligne doit alors contenir que des cellules vides.
   * @param row Indice de la ligne � effacer.
   */
  public void effacerLigne(int row) {
    ((Hydraulique1dLigneReelTableau)listePts_.get(row)).effaceLigne();
    fireTableDataChanged();
  }
  /**
   * Ajoute une colonne vide � droite du tableau.
   * @param nomColonne Le nom de la nouvelle colonne.
   */
  public void ajouterColonne(String nomColonne) {
    ajouterColonne(nomColonne, listeColumnNames_.size());
  }
  /**
   * Ajoute une colonne vide � l'indexe indiqu� du tableau.
   * @param nomColonne Le nom de la nouvelle colonne.
   * @param indexe L'indice de la colonne � rajouter.
   */
  public void ajouterColonne(String nomColonne, int indexe) {
    // ajout du nom
    listeColumnNames_.add(indexe, nomColonne);

    // ajout d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
      lig.ajoutCelluleVide(indexe);
    }
    fireTableStructureChanged();
  }
  /**
   * Supprime une colonne du tableau.
   * @param col Indice de la colonne � supprimer.
   */
  public void supprimerColonne(int col) {
    // ajout du nom
    listeColumnNames_.remove(col);

    // suppression d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
      lig.supprimeCellule(col);
    }
    fireTableStructureChanged();
  }
  /**
   * Efface une colonne du tableau.
   * La colonne doit alors contenir que des cellules vides.
   * @param col Indice de la colonne � effacer.
   */
  public void effacerColonne(int col) {
    // suppression d'une cellule sur toutes les lignes.
    Iterator ite= listePts_.iterator();
    while (ite.hasNext()) {
      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
      lig.setValue(col, null);
    }
    fireTableDataChanged();
  }

  /**
   * Trie le tableau
   */
  public void trier() {
    Collections.sort(listePts_);
    fireTableDataChanged();
  }

  public static void main(String[] arg) {
    String[] nomsColonnes = {"col1","col2"};
    Hydraulique1dTableauReelModel test = new Hydraulique1dTableauReelModel(nomsColonnes,1);
    double[][] tab ={{1.1,1.2,1.3},{2.1,2.2,2.3}};
    test.setTabDouble(tab);
    double[][] tabComplet = test.getTabDoubleComplet();
    System.out.println("egale(tab, tabComplet)="+CtuluLibArray.equals(tab, tabComplet));
  }
}
