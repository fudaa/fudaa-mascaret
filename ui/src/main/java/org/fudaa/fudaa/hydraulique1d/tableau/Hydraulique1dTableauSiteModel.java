/*
 * @file         Hydraulique1dTableauSiteModel.java
 * @creation     05/06/04
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSections;
import org.fudaa.dodico.hydraulique1d.metier.MetierParametresStockage;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierSite;

/**
 * Mod�le de tableau liste des sites : couples (indice bief, abscisse).
 * Utilis� pour �diter les sections de calcul lorsqu'elles sont d�finies
 * une � une par l'utilisateur et pour les sites de stockage des r�sultats.
 * @see Hydraulique1dLigne1EntierEtReelsTableau
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.7 $ $Date: 2007-11-20 11:43:11 $ by $Author: bmarchan $
 */
public class Hydraulique1dTableauSiteModel
    extends Hydraulique1dTableau1EntierEtReelsModel {
  private final static String[] NOMS_COL = {
      getS("n� bief"), getS("abscisse")};
  protected MetierReseau reseau_;
  private MetierDefinitionSectionsParSections defSection_;
  private MetierParametresStockage paramStockage_;

  /**
   * Constructeur par d�faut avec 2 colonnes ("n� bief" et "abscisse")
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableauSiteModel() {
    super(NOMS_COL, 20);
  }
  /**
   * Constructeur pour des classes filles avec d'autres noms.
   * et 20 lignes vides � la fin du tableau.
   * @param nomsColonnes Les noms des colonnes.
   */
  public Hydraulique1dTableauSiteModel(String[] nomsColonnes) {
    super(nomsColonnes, 20);
  }

  public void setReseau(MetierReseau reseau) {
    reseau_ = reseau;
  }

  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigneSiteTableau.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigneSiteTableau();
  }

  /**
   * Initialise le mod�le m�tier � partir du param�tre de stockage.
   * @param paramStockage DParametresStockage
   */
  public void setModelMetier(MetierParametresStockage paramStockage) {
    paramStockage_ = paramStockage;
  }

  /**
   * Initialise le mod�le m�tier � partir d'une definition de maillage section par section.
   * @param defSection MetierDefinitionSectionsParSections
   */
  public void setModelMetier(MetierDefinitionSectionsParSections defSection) {
    defSection_ = defSection;
  }

  @Override
  public void setValeurs() {
    listePts_ = new ArrayList();
    MetierSite[] sites = getSites();

    for (int i = 0; i < sites.length; i++) {
      Hydraulique1dLigneSiteTableau lig = (Hydraulique1dLigneSiteTableau)creerLigneVide();
      lig.setSite(sites[i]);
      listePts_.add(lig);
    }

    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  @Override
  public boolean getValeurs() {
    List listeTmp = getListeLignesCorrectes();
    MetierSite[] sites = getSites();
    boolean existeDifference = false;
    if (listeTmp.size() != sites.length) {
      existeDifference = true;
    }
    MetierSite[] sitesTmp = new MetierSite[listeTmp.size()];
    int tailleMin = Math.min(listeTmp.size(), sites.length);
    for (int i = 0; i < tailleMin; i++) {
      Hydraulique1dLigneSiteTableau siteTab = (Hydraulique1dLigneSiteTableau)
          listeTmp.get(i);
      sitesTmp[i] = sites[i];
      if (!siteTab.equals(sitesTmp[i])) {
        existeDifference = true;
        initDSite(siteTab, sitesTmp[i]);
      }
    }
    if (listeTmp.size() > sites.length) {
      existeDifference = true;
      MetierSite[] nouveauxDSite = creeSites(listeTmp.size() - sites.length);
      int iNouveauxDSite = 0;
      for (int i = tailleMin; i < sitesTmp.length; i++) {
        Hydraulique1dLigneSiteTableau siteTab = (Hydraulique1dLigneSiteTableau)
            listeTmp.get(i);
        sitesTmp[i] = nouveauxDSite[iNouveauxDSite];
        initDSite(siteTab, sitesTmp[i]);
        iNouveauxDSite++;
      }
    }
    else if (listeTmp.size() < sites.length) {
      existeDifference = true;
      MetierSite[] sitesASupprimer = new MetierSite[sites.length - tailleMin];
      int iSitesASupprimer = 0;
      for (int i = tailleMin; i < sites.length; i++) {
        sitesASupprimer[iSitesASupprimer] = sites[i];
        iSitesASupprimer++;
      }
      supprimeSites(sitesASupprimer);
    }
    if (existeDifference) {
      miseAJourModeleMetier(sitesTmp);
    }
    return existeDifference;
  }

  protected List getListeLignesCorrectes() {
    List listeLignesCompletes = getListePtsComplets();
    List listeLignesCorrectes = new ArrayList(listeLignesCompletes.size());
    Iterator iter = listeLignesCompletes.iterator();
    while (iter.hasNext()) {
      Hydraulique1dLigneSiteTableau ligne = (Hydraulique1dLigneSiteTableau)
          iter.next();
      int indiceBief = ligne.iBief() - 1;
      if ( (indiceBief < reseau_.biefs().length) && (ligne.iBief() > 0)) {
        if (reseau_.biefs()[indiceBief].contientAbscisse(ligne.absc())) {
          listeLignesCorrectes.add(ligne);
        }
      }
    }
    return listeLignesCorrectes;
  }

  public List getListeLignesInCorrectes() {
    List listeLigneCorrecte = getListeLignesCorrectes();
    List listeLigneInCorrectes = new ArrayList(getListePtsPasToutVide());
    listeLigneInCorrectes.removeAll(listeLigneCorrecte);
    return listeLigneInCorrectes;
  }

  /**
   * R�cup�re le tableau de MetierSite suivant que les sites m�tier sont un param�tre
   * de stockage ou une definition section par section du maillage.
   * @return MetierSite[]
   */
  private MetierSite[] getSites() {
    MetierSite[] sites = new MetierSite[0];
    if (paramStockage_ != null) {
      sites = paramStockage_.sites();
    }
    if (defSection_ != null) {
      sites = defSection_.unitaires();
    }
    return sites;
  }

  /**
   * Initialise le site m�tier � partir d'une ligne tableau.
   * @param siteTab Hydraulique1dLigneSiteTableau
   * @param isite MetierSite
   */
  protected void initDSite(Hydraulique1dLigneSiteTableau siteTab, MetierSite isite) {
    isite.abscisse(siteTab.absc());
    if (reseau_.biefs().length >= siteTab.iBief()) {
      isite.biefRattache(reseau_.biefs()[siteTab.iBief() - 1]);
    }
  }

  /**
   * miseAJourModeleMetier
   */
  public void miseAJourModeleMetier(MetierSite[] sites) {
    if (paramStockage_ != null) {
      paramStockage_.sites(sites);
    }
    if (defSection_ != null) {
      defSection_.unitaires(sites);
    }
  }

  /**
   * supprimePoint
   *
   * @param iSite MetierSite
   */
  private void supprimeSites(MetierSite[] sitesASupprimer) {
    if (paramStockage_ != null) {
      paramStockage_.supprimeSites(sitesASupprimer);
    }
    if (defSection_ != null) {
      defSection_.supprimeSections(sitesASupprimer);
    }
  }

  /**
   * creeSites
   *
   * @param nb Le nombre de MetierSite � cr�er;
   * @return MetierSite[]
   */
  private MetierSite[] creeSites(int nb) {
    MetierSite[] sites = new MetierSite[nb];
    for (int i = 0; i < sites.length; i++) {
      sites[i] = new MetierSite();
    }
    return sites;
  }

}
