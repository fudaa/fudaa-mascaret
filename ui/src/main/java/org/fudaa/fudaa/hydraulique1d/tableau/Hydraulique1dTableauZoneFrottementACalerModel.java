/*
 * @file         Hydraulique1dTableauZoneFrottementCalageModel.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;

/**
 * Mod�le de tableau des zone de frottement � caler.
 * @author Adrien Hadoux
 */
public class Hydraulique1dTableauZoneFrottementACalerModel
  extends Hydraulique1dTableauZoneModel implements ZoneFrottementCalageModelInterface{

  
  private MetierCalageAuto calageAuto_;

  /**
   * Noms des colonnes.
   * "n� bief", "abscisse d�but", "abscisse fin", "coef. mineur" et "coef. majeur".
   */
  private final static String[] NOMS_COL = {
      getS("n� bief"), getS("abscisse d�but"), getS("abscisse fin"), getS("coef. mineur"),
      getS("coef. majeur"),getS("Borne inf. coef. mineur"),getS("Borne sup. coef. mineur"),
      getS("Borne inf. coef. majeur"),getS("Borne sup. coef majeur")};
 

  /**
   * Constructeur avec 9 colonnes ("n� bief", "abscisse d�but", "abscisse fin", "coef. mineur" ou "coef. majeur" + coeff lits)
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableauZoneFrottementACalerModel() {
    super();
  
    	setColumnNames(NOMS_COL);
   
  }

  public void setCalageAuto(MetierCalageAuto _cal) {
    calageAuto_=_cal;
  }

  

  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigneZoneFrottementTableauACaler.
   */
  @Override
  public Hydraulique1dLigneZoneFrottementTableauACaler creerLigneVide() {
    return new Hydraulique1dLigneZoneFrottementTableauACaler();
  }

  /**
   * R�cup�re les donn�es de l'objet m�tier (MetierCalageAuto) et les tranferts vers le mod�le de tableau.
   */
  @Override
  public void setValeurs() {
    listePts_ = new ArrayList();

    MetierZoneFrottement[] zones = calageAuto_.zonesFrottement();
    
    for (int i = 0; i < zones.length; i++) {
      Hydraulique1dLigneZoneFrottementTableauACaler lig = (Hydraulique1dLigneZoneFrottementTableauACaler)creerLigneVide();
      lig.setMetier(zones[i]);
      listePts_.add(lig);
    }

    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  /**
   * Transferts les donn�es du tableau vers l'objet m�tier.
   * @return vrai s'il existe des diff�rences, faux sinon.
   */
  @Override
  public boolean getValeurs() {
    trier();

    List listeTmp = getListeLignesCorrectes();
    MetierZoneFrottement[] tabMetier = null;
    boolean existeDifference = false;

    MetierZoneFrottement[] zones = calageAuto_.zonesFrottement();

    if (listeTmp.size() != zones.length) {
      existeDifference = true;
    }

    MetierZoneFrottement[] zonesTmp = new MetierZoneFrottement[listeTmp.size()];

    // initialise les premi�res lignes m�tiers communes � partir des lignes de tableau
    int tailleMin = Math.min(listeTmp.size(), zones.length);
    for (int i = 0; i < tailleMin; i++) {
      Hydraulique1dLigneZoneFrottementTableauACaler zoneTab = (
          Hydraulique1dLigneZoneFrottementTableauACaler) listeTmp.get(i);
      zonesTmp[i] = zones[i];
      if (!zoneTab.equals(zonesTmp[i])) { 
        existeDifference = true;
        initLigneObjetMetier(zoneTab, zonesTmp[i]);
      }
    }

    // il existe plus de ligne dans le tableau que de lignes m�tier
    // => cr�ation de MetierZonePlanimetrage.
    if (listeTmp.size() > zones.length) {
      existeDifference = true;
      MetierZoneFrottement[] nouveauxDZone = creeZonesFrottement(listeTmp.size() -
          zones.length);
      int iNouveauxDZone = 0;
      for (int i = tailleMin; i < zonesTmp.length; i++) {
        Hydraulique1dLigneZoneFrottementTableauACaler zoneTab = (
            Hydraulique1dLigneZoneFrottementTableauACaler) listeTmp.get(i);
        zonesTmp[i] = nouveauxDZone[iNouveauxDZone];
        initLigneObjetMetier(zoneTab, zonesTmp[i]);
        iNouveauxDZone++;
      }
    }
   
    tabMetier = zonesTmp;
    if (existeDifference) {
      miseAJourModeleMetier(tabMetier);
    }
    return existeDifference;
  }

  /**
   * Initialise le planimetrage m�tier � partir d'une ligne tableau.
   * @param zoneTab Hydraulique1dLigneZoneTailleTableau
   * @param izone MetierZonePlanimetrage
   */
  private void initLigneObjetMetier(Hydraulique1dLigneZoneFrottementTableauACaler
                                    zoneTab, MetierZoneFrottement izone) {
    super.initLigneObjetMetier(zoneTab, izone);
    izone.coefMineur(zoneTab.coefMin());
    izone.coefMajeur(zoneTab.coefMaj());
    
    izone.coefLitMajBinf(zoneTab.coefLitMajBinf());
    izone.coefLitMajBsup(zoneTab.coefLitMajBsup());
    izone.coefLitMinBinf(zoneTab.coefLitMinBinf());
    izone.coefLitMinBsup(zoneTab.coefLitMinBsup());
  }

  /**
   * Mise � jour de l'objet m�tier container � partir d'un tableau d'objets m�tiers.
   * <br>Utilis� par getValeurs().
   * @param zones le tableau d'objets m�tier :
   */
  private void miseAJourModeleMetier(MetierZoneFrottement[] zones) {   
    	calageAuto_.zonesFrottement(zones);   
  }

  /**
   * supprime des zones de Frottement
   * @param zonesASupprimer MetierZoneFrottement[]
   */


  /**
   * cree des zones de frottement
   * @param nb Le nombre de MetierZoneFrottement � cr�er;
   * @return MetierDefinitionSectionsParSeriesUnitaire[]
   */
  private MetierZoneFrottement[] creeZonesFrottement(int nb) {
    MetierZoneFrottement[] zones = new MetierZoneFrottement[nb];
    for (int i = 0; i < zones.length; i++) {
      zones[i] = new MetierZoneFrottement();
    }
    return zones;
  }

 
  @Override
  public boolean isCellEditable(int row, int col) {
    return true;
  }
  

  
  public List getListePtsComplets() {
	    ArrayList listeTmp= new ArrayList(listePts_.size());
	    Iterator ite= listePts_.iterator();
	    while (ite.hasNext()) {
	      Hydraulique1dLigneReelTableau lig= (Hydraulique1dLigneReelTableau)ite.next();
	      if (!lig.isExisteNulle(4)) {
	        listeTmp.add(lig);
	      }
	    }
	    return listeTmp;
	  }
  
 
  
}
