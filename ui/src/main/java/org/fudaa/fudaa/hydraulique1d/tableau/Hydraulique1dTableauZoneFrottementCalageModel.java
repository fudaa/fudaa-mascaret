/*
 * @file         Hydraulique1dTableauZoneFrottementCalageModel.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;

/**
 * Mod�le de tableau pour les zones de frottements calage.
 * colonnes : indice bief, abscisse d�but, abscisse fin, coef. mineur et coef. majeur.
 * B.M. A mon avis, les classes m�res devraient �tre �pur�es pour etre plus g�n�ralistes
 * et ne pas implementer les m�thodes qui sont surcharg�es ici.
 *
 * @see Hydraulique1dLigne1EntierEtReelsTableau
 * @see Hydraulique1dTableauReelModel
 * @author Bertrand Marchand
 * @version $Revision: 1.4 $ $Date: 2007-11-20 11:43:11 $ by $Author: bmarchan $
 */
public class Hydraulique1dTableauZoneFrottementCalageModel
  extends Hydraulique1dTableauZoneModel implements ZoneFrottementCalageModelInterface{

  
  private MetierCalageAuto calageAuto_;

  /**
   * Noms des colonnes.
   * "n� bief", "abscisse d�but", "abscisse fin", "coef. mineur" et "coef. majeur".
   */
  private final static String[] NOMS_COL = {
      getS("n� bief"), getS("abscisse d�but"), getS("abscisse fin"), getS("coef. mineur"),
      getS("coef. majeur")};

  /**
   * Constructeur avec 9 colonnes ("n� bief", "abscisse d�but", "abscisse fin", "coef. mineur" ou "coef. majeur" + coeff lits)
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableauZoneFrottementCalageModel() {
    super();
   
    	setColumnNames(NOMS_COL);
   
  }

  public void setCalageAuto(MetierCalageAuto _cal) {
    calageAuto_=_cal;
  }

 

  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigneZoneFrottementTableau.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigneZoneFrottementTableau();
  }

  /**
   * R�cup�re les donn�es de l'objet m�tier (MetierCalageAuto) et les tranferts vers le mod�le de tableau.
   */
  @Override
  public void setValeurs() {
    listePts_ = new ArrayList();

    MetierZoneFrottement[] zones;
                             zones=calageAuto_.resultats().zonesFrottementCalees();

    for (int i = 0; i < zones.length; i++) {
      Hydraulique1dLigneZoneFrottementTableau lig = (Hydraulique1dLigneZoneFrottementTableau)creerLigneVide();
      lig.setMetier(zones[i]);
      listePts_.add(lig);
    }

    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  /**
   * Transferts les donn�es du tableau vers l'objet m�tier.
   * @return vrai s'il existe des diff�rences, faux sinon.
   */
  @Override
  public boolean getValeurs() {
    trier();

    List listeTmp = getListeLignesCorrectes();
    MetierZoneFrottement[] tabMetier = null;
    boolean existeDifference = false;

    MetierZoneFrottement[] zones  = calageAuto_.resultats().zonesFrottementCalees();

    if (listeTmp.size() != zones.length) {
      existeDifference = true;
    }

    MetierZoneFrottement[] zonesTmp = new MetierZoneFrottement[listeTmp.size()];

    // initialise les premi�res lignes m�tiers communes � partir des lignes de tableau
    int tailleMin = Math.min(listeTmp.size(), zones.length);
    for (int i = 0; i < tailleMin; i++) {
      Hydraulique1dLigneZoneFrottementTableau zoneTab = (
          Hydraulique1dLigneZoneFrottementTableau) listeTmp.get(i);
      zonesTmp[i] = zones[i];
      if (!zoneTab.equals(zonesTmp[i])) { // Forcement vrai, ce sont 2 objets de classes diff�rentes !!
        existeDifference = true;
        initLigneObjetMetier(zoneTab, zonesTmp[i]);
      }
    }

    // il existe plus de ligne dans le tableau que de lignes m�tier
    // => cr�ation de MetierZonePlanimetrage.
    if (listeTmp.size() > zones.length) {
      existeDifference = true;
      MetierZoneFrottement[] nouveauxDZone = creeZonesFrottement(listeTmp.size() -
          zones.length);
      int iNouveauxDZone = 0;
      for (int i = tailleMin; i < zonesTmp.length; i++) {
        Hydraulique1dLigneZoneFrottementTableau zoneTab = (
            Hydraulique1dLigneZoneFrottementTableau) listeTmp.get(i);
        zonesTmp[i] = nouveauxDZone[iNouveauxDZone];
        initLigneObjetMetier(zoneTab, zonesTmp[i]);
        iNouveauxDZone++;
      }
    }
    // B.M. Aucune importance, de toutes les fa�ons, on assigne la totalit� du tableau.

    // il existe moins de ligne dans le tableau que de lignes m�tier
    // => suppression de MetierZonePlanimetrage.
/*    else if (listeTmp.size() < zones.length) {
      existeDifference = true;
      MetierZoneFrottement[] zonesASupprimer = new MetierZoneFrottement[zones.length -
          tailleMin];
      int iZonesASupprimer = 0;
      for (int i = tailleMin; i < zones.length; i++) {
        zonesASupprimer[iZonesASupprimer] = zones[i];
        iZonesASupprimer++;
      }
      supprimeZonesFrottement(zonesASupprimer);
    }*/
    tabMetier = zonesTmp;
    if (existeDifference) {
      miseAJourModeleMetier(tabMetier);
    }
    return existeDifference;
  }

  /**
   * Initialise le planimetrage m�tier � partir d'une ligne tableau.
   * @param zoneTab Hydraulique1dLigneZoneTailleTableau
   * @param izone MetierZonePlanimetrage
   */
  private void initLigneObjetMetier(Hydraulique1dLigneZoneFrottementTableau
                                    zoneTab, MetierZoneFrottement izone) {
    super.initLigneObjetMetier(zoneTab, izone);
    izone.coefMineur(zoneTab.coefMin());
    izone.coefMajeur(zoneTab.coefMaj());
  }

  /**
   * Mise � jour de l'objet m�tier container � partir d'un tableau d'objets m�tiers.
   * <br>Utilis� par getValeurs().
   * @param zones le tableau d'objets m�tier :
   */
  private void miseAJourModeleMetier(MetierZoneFrottement[] zones) {
     calageAuto_.resultats().zonesFrottementCalees(zones);

  }

 

  /**
   * cree des zones de frottement
   * @param nb Le nombre de MetierZoneFrottement � cr�er;
   * @return MetierDefinitionSectionsParSeriesUnitaire[]
   */
  private MetierZoneFrottement[] creeZonesFrottement(int nb) {
    MetierZoneFrottement[] zones = new MetierZoneFrottement[nb];
    for (int i = 0; i < zones.length; i++) {
      zones[i] = new MetierZoneFrottement();
    }
    return zones;
  }

  /**
   * Retourne si la cellule est �ditable.
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   * @return Vrai si valeurs a caler, faux si valeurs cal�es.
   */
  @Override
  public boolean isCellEditable(int row, int col) {
    return false;
  }
}
