/*
 * @file         Hydraulique1dTableauZoneFrottementModel.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:43:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierZoneFrottement;

/**
 * Mod�le de tableau pour les zones de frottements.
 * colonnes : indice bief, abscisse d�but, abscisse fin, coef. mineur et coef. majeur
 *
 * @see Hydraulique1dLigne1EntierEtReelsTableau
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.7 $ $Date: 2007-11-20 11:43:10 $ by $Author: bmarchan $
 */
public class Hydraulique1dTableauZoneFrottementModel
    extends Hydraulique1dTableauZoneModel {

  /**
   * Noms des colonnes.
   * "n� bief", "abscisse d�but", "abscisse fin", "coef. mineur" et "coef. majeur".
   */
  private final static String[] NOMS_COL = {
      getS("n� bief"), getS("abscisse d�but"), getS("abscisse fin"), getS("coef. mineur"),
      getS("coef. majeur")};
  private MetierCalageAuto calage_;

  /**
   * Constructeur avec 5 colonnes ("n� bief", "abscisse d�but", "abscisse fin", "coef. mineur" ou "coef. majeur")
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableauZoneFrottementModel() {
    super();
    setColumnNames(NOMS_COL);
  }

  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigneZoneFrottementTableau.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigneZoneFrottementTableau();
  }

  /**
   * Affectation du calage automatique.
   */
  public void setCalageAuto(MetierCalageAuto _cal) {
    calage_=_cal;
  }

  /**
   * L'importation des zones depuis le calage est-il possible ?
   */
  public boolean isImportationZonesPossible() {
    return calage_==null ? false:calage_.resultats().zonesFrottementCalees().length>0;
  }

  /**
   * Importation depuis les crues du calage.
   */
  public void transfererZonesCalage() {
    listePts_.clear();

    int ibief=reseau_.biefs()[0].indice()+1;

    MetierZoneFrottement[] zones=calage_.resultats().zonesFrottementCalees();
    for (int i=0; i<zones.length; i++) {
      double absDeb=zones[i].abscisseDebut();
      double absFin=zones[i].abscisseFin();
      double cmin=zones[i].coefMineur();
      double cmaj=zones[i].coefMajeur();
      Hydraulique1dLigneZoneFrottementTableau lig = (Hydraulique1dLigneZoneFrottementTableau)creerLigneVide();
      lig.iBief(ibief);
      lig.abscDebut(absDeb);
      lig.abscFin(absFin);
      lig.coefMin(cmin);
      lig.coefMaj(cmaj);
      listePts_.add(lig);
    }
    fireTableDataChanged();
  }

  /**
   * R�cup�re les donn�es de l'objet m�tier (DReseau) et les tranferts vers le mod�le de tableau.
   */
  @Override
  public void setValeurs() {
    listePts_ = new ArrayList();
    MetierZoneFrottement[] zones = reseau_.zonesFrottement();
    for (int i = 0; i < zones.length; i++) {
      Hydraulique1dLigneZoneFrottementTableau lig = (Hydraulique1dLigneZoneFrottementTableau)creerLigneVide();
      lig.setMetier(zones[i]);
      listePts_.add(lig);
    }

    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  /**
   * Transferts les donn�es du tableau vers l'objet m�tier.
   * @return vrai s'il existe des diff�rences, faux sinon.
   */
  @Override
  public boolean getValeurs() {
    trier();
    List listeTmp = getListeLignesCorrectes();
    MetierZoneFrottement[] tabMetier = null;
    boolean existeDifference = false;

    MetierZoneFrottement[] zones = reseau_.zonesFrottement();
    if (listeTmp.size() != zones.length) {
      existeDifference = true;
    }

    MetierZoneFrottement[] zonesTmp = new MetierZoneFrottement[listeTmp.size()];

    // initialise les premi�res lignes m�tiers communes � partir des lignes de tableau
    int tailleMin = Math.min(listeTmp.size(), zones.length);
    for (int i = 0; i < tailleMin; i++) {
      Hydraulique1dLigneZoneFrottementTableau zoneTab = (
          Hydraulique1dLigneZoneFrottementTableau) listeTmp.get(i);
      zonesTmp[i] = zones[i];
      if (!zoneTab.equals(zonesTmp[i])) {
        existeDifference = true;
        initLigneObjetMetier(zoneTab, zonesTmp[i]);
      }
    }

    // il existe plus de ligne dans le tableau que de lignes m�tier
    // => cr�ation de MetierZonePlanimetrage.
    if (listeTmp.size() > zones.length) {
      existeDifference = true;
      MetierZoneFrottement[] nouveauxDZone = creeZonesFrottement(listeTmp.size() -
          zones.length);
      int iNouveauxDZone = 0;
      for (int i = tailleMin; i < zonesTmp.length; i++) {
        Hydraulique1dLigneZoneFrottementTableau zoneTab = (
            Hydraulique1dLigneZoneFrottementTableau) listeTmp.get(i);
        zonesTmp[i] = nouveauxDZone[iNouveauxDZone];
        initLigneObjetMetier(zoneTab, zonesTmp[i]);
        iNouveauxDZone++;
      }
    }

    // il existe moins de ligne dans le tableau que de lignes m�tier
    // => suppression de MetierZonePlanimetrage.
    else if (listeTmp.size() < zones.length) {
      existeDifference = true;
      MetierZoneFrottement[] zonesASupprimer = new MetierZoneFrottement[zones.length -
          tailleMin];
      int iZonesASupprimer = 0;
      for (int i = tailleMin; i < zones.length; i++) {
        zonesASupprimer[iZonesASupprimer] = zones[i];
        iZonesASupprimer++;
      }
      supprimeZonesFrottement(zonesASupprimer);
    }
    tabMetier = zonesTmp;
    if (existeDifference) {
      miseAJourModeleMetier(tabMetier);
    }
    return existeDifference;
  }

  /**
   * Initialise le planimetrage m�tier � partir d'une ligne tableau.
   * @param zoneTab Hydraulique1dLigneZoneTailleTableau
   * @param izone MetierZonePlanimetrage
   */
  private void initLigneObjetMetier(Hydraulique1dLigneZoneFrottementTableau
                                    zoneTab, MetierZoneFrottement izone) {
    super.initLigneObjetMetier(zoneTab, izone);
    izone.coefMineur(zoneTab.coefMin());
    izone.coefMajeur(zoneTab.coefMaj());
  }

  /**
   * Mise � jour de l'objet m�tier container � partir d'un tableau d'objets m�tiers.
   * <br>Utilis� par getValeurs().
   * @param zones le tableau d'objets m�tier :
   */
  private void miseAJourModeleMetier(MetierZoneFrottement[] zones) {
    for (int i = 0; i < reseau_.biefs().length; i++) {
      MetierBief b = reseau_.biefs()[i];
      ArrayList liste = new ArrayList();
      for (int j = 0; j < zones.length; j++) {
        if (b == zones[j].biefRattache()) {
          liste.add(zones[j]);
        }
      }
      MetierZoneFrottement[] zonesBief = (MetierZoneFrottement[]) liste.toArray(new
          MetierZoneFrottement[liste.size()]);
      b.zonesFrottement(zonesBief);
    }
  }

  /**
   * supprime des zones de Frottement
   * @param zonesASupprimer MetierZoneFrottement[]
   */
  private void supprimeZonesFrottement(MetierZoneFrottement[] zonesASupprimer) {
    if (reseau_ != null) {
      reseau_.supprimeZonesFrottement(zonesASupprimer);
    }
  }

  /**
   * cree des zones de frottement
   * @param nb Le nombre de MetierZoneFrottement � cr�er;
   * @return MetierDefinitionSectionsParSeriesUnitaire[]
   */
  private MetierZoneFrottement[] creeZonesFrottement(int nb) {
    MetierZoneFrottement[] zones = new MetierZoneFrottement[nb];
    for (int i = 0; i < zones.length; i++) {
      zones[i] = new MetierZoneFrottement();
    }
    return zones;
  }
}
