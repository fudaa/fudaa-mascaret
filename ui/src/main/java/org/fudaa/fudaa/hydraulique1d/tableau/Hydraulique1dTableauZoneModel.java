/*
 * @file         Hydraulique1dTableauZoneModel.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:43:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierZone;

/**
 * Mod�le de tableau des zones de biefs.
 * Utilis� pour les zones d�finissant un maillage, le planim�trage, les zones s�ches et de frottement.
 * colonnes : indice bief, abscisse d�but, abscisse fin.
 * @see Hydraulique1dLigneZoneTableau
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.7 $ $Date: 2007-11-20 11:43:09 $ by $Author: bmarchan $
 */
public abstract class Hydraulique1dTableauZoneModel
    extends Hydraulique1dTableau1EntierEtReelsModel {

  /**
   * Noms des colonnes.
   * "n� bief", "abscisse d�but", "abscisse fin".
   */
  private final static String[] NOMS_COL = {getS("n� bief"), getS("abscisse d�but"),
                                            getS("abscisse fin")};


  protected MetierReseau reseau_;

  /**
   * Constructeur avec 4 colonnes ("n� bief", "abscisse d�but", "abscisse fin")
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableauZoneModel() {
    super();
    setColumnNames(NOMS_COL);
  }

  /**
   * Initialise le r�seau m�tier.
   * Indispensable pour les 2 modes.
   * @param reseau le r�seau m�tier.
   */
  public void setReseau(MetierReseau reseau) {
    reseau_ = reseau;
  }

  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigneZoneTableau.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigneZoneTableau();
  }

  /**
   * Modifie la valeur d'une cellule du tableau.
   * Si c'est l'indice du bief qui est modifi�, les abscisses de d�but et de
   * fin peuvent �tre modifi�es.
   * @param value La nouvelle valeur (Integer, Double ou null).
   * @param row Indice de la ligne de la cellule.
   * @param col Indice de la colonne de la cellule.
   */
  @Override
  public void setValueAt(Object value, int row, int col) {
     if ((col == 0)&&(value != null)) {
      Integer valeur = (Integer)value;
      int numBief = valeur.intValue();
      Hydraulique1dLigneZoneTableau lig = (Hydraulique1dLigneZoneTableau)listePts_.get(row);
      lig.iBief(numBief);
      if (reseau_.biefs() != null) {
        if ((reseau_.biefs().length >= numBief)&&(numBief>0)) {
          if (lig.X() == null) {
            if ( ! isExisteBiefNum(numBief,lig)) {
              lig.abscDebut(reseau_.biefs()[numBief - 1].getXOrigine());
            } else {
              Double abscFinPlusGrande = rechercheAbscFinPlusGrandeBief(numBief);
              if (abscFinPlusGrande == null) {
                lig.abscDebut(reseau_.biefs()[numBief - 1].getXOrigine());
              } else {
                if (abscFinPlusGrande.doubleValue() <= reseau_.biefs()[numBief - 1].getXFin()) {
                  lig.abscDebut(abscFinPlusGrande.doubleValue());
                }
              }
            }
          }
          if (lig.Y() == null) {
            if ( ! isExisteBiefNum(numBief,lig)) {
              lig.abscFin(reseau_.biefs()[numBief - 1].getXFin());
           } else {
              Double abscFinPlusGrande = rechercheAbscFinPlusGrandeBief(numBief);

              if (abscFinPlusGrande == null) {
                lig.abscFin(reseau_.biefs()[numBief - 1].getXFin());
              } else {
                if (abscFinPlusGrande.doubleValue() < reseau_.biefs()[numBief - 1].getXFin()) {
                   lig.abscFin(reseau_.biefs()[numBief - 1].getXFin());
                }
              }
            }
          }
        }
      }
      fireTableDataChanged();
    } else {
      super.setValueAt(value, row, col);
    }
  }

  /**
   * Cherche un num�ro de bief dans les lignes du tableau.
   * @param numBief numero du bief � chercher dans les lignes du tableau.
   * @param ligne courante
   * @return boolean true si le num�ro du bief existe.
   */
  public boolean isExisteBiefNum(int numBief,Hydraulique1dLigneZoneTableau ligCourante) {
    List liste = getListePtsPasToutVide();
    liste.remove(ligCourante);
    Iterator iter = liste.iterator();
    while (iter.hasNext()) {
      Hydraulique1dLigneZoneTableau lig = (Hydraulique1dLigneZoneTableau)iter.next();
      if (lig.iBief() == numBief) return true;
    }
    return false;
  }

  /**
   * Recherche dans le tableau l'abscisse de fin la plus grande pour un bief donn�.
   * @param numBief num�ro du bief � chercher dans les lignes du tableau.
   * @return l'abscisse de fin la plus grande du bief. Peut �tre null si le bief n'existe pas ou
   * si il n'y a pas d'abscisse de fin.
   */
  public Double rechercheAbscFinPlusGrandeBief(int numBief) {
    List liste = getListePtsPasToutVide();
    Iterator iter = liste.iterator();
    Double max = null;
    while (iter.hasNext()) {
      Hydraulique1dLigneZoneTableau lig = (Hydraulique1dLigneZoneTableau)iter.next();
      if (lig.iBief() == numBief) {
        if (lig.Y() != null) {
          if (max == null) max = lig.Y();
          else {
            if (lig.abscFin() > max.doubleValue()) {
              max = lig.Y();
            }
          }
        }
      }
    }
    return max;
  }

  /**
   * R�cup�re les donn�es de l'objet m�tier et les tranferts vers le mod�le de tableau.
   */
  @Override
  public abstract void setValeurs();

  /**
   * Transferts les donn�es du tableau vers l'objet m�tier.
   * @return vrai s'il existe des diff�rences, faux sinon.
   */
  @Override
  public abstract boolean getValeurs();

  /**
   * Estime le num�ro des biefs � partir des abscisses de d�but et fin de chaque lignes.
   * @param lignes Hydraulique1dLigneZoneTableau[]
   */
  public void estimationNumeroBief(Hydraulique1dLigneZoneTableau[] lignes) {
    for (int i = 0; i < lignes.length; i++) {
      MetierBief biefDebut = reseau_.getBiefContenantAbscisse(lignes[i].abscDebut());
      MetierBief biefFin = reseau_.getBiefContenantAbscisse(lignes[i].abscFin());
      if ( (biefDebut != null) && (biefFin != null) ) {
        if (biefDebut == biefFin) {
          lignes[i].iBief(biefDebut.indice()+1);
        }
      }
    }
  }

  /**
   * Retourne la liste des lignes incorrectes pas enti�rement vides.
   * @return la liste des lignes pas enti�rement vides - les lignes correcte.
   */
  public List getListeLignesInCorrectes() {
    List listeLigneCorrecte = getListeLignesCorrectes();
    List listeLigneInCorrectes = new ArrayList(getListePtsPasToutVide());
    listeLigneInCorrectes.removeAll(listeLigneCorrecte);
    return listeLigneInCorrectes;
  }

  public String getMessageAvertissement() {
      return "";
  }

  protected List getListeLignesCorrectes() {
    List listeLignesCompletes = getListePtsComplets();
    List listeLignesCorrectes = new ArrayList(listeLignesCompletes.size());
    Iterator iter = listeLignesCompletes.iterator();
    while (iter.hasNext()) {
      Hydraulique1dLigneZoneTableau ligne = (
          Hydraulique1dLigneZoneTableau) iter.next();
      int indiceBief = ligne.iBief() - 1;
      if ( (indiceBief < reseau_.biefs().length) && (ligne.iBief() > 0)) {
        if ( (reseau_.biefs()[indiceBief].contientAbscisse(ligne.abscDebut())) &&
            (reseau_.biefs()[indiceBief].contientAbscisse(ligne.abscFin()))) {
          listeLignesCorrectes.add(ligne);
        }
      }
    }
    return listeLignesCorrectes;
  }

  /**
   * Initialise la zone m�tier � partir d'une ligne tableau.
   * @param zoneTab Hydraulique1dLigneZoneTableau
   * @param izone MetierZone
   */
  protected void initLigneObjetMetier(Hydraulique1dLigneZoneTableau zoneTab,
                        MetierZone izone) {
    izone.abscisseDebut(zoneTab.abscDebut());
    izone.abscisseFin(zoneTab.abscFin());
    if (reseau_.biefs().length >= zoneTab.iBief()) {
      izone.biefRattache(reseau_.biefs()[zoneTab.iBief() - 1]);
    }
  }

}
