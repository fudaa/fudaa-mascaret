/*
 * @file         Hydraulique1dTableauZoneSecheModel.java
 * @creation     2004-07-13
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierConditionsInitiales;
import org.fudaa.dodico.hydraulique1d.metier.MetierLigneEauInitiale;
import org.fudaa.dodico.hydraulique1d.metier.MetierZone;

/**
 * Mod�le de tableau pour les zones s�ches.
 * colonnes : indice bief, abscisse d�but, abscisse fin
 * @see Hydraulique1dLigne1EntierEtReelsTableau
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.5 $ $Date: 2007-11-20 11:43:11 $ by $Author: bmarchan $
 */
public final class Hydraulique1dTableauZoneSecheModel
    extends Hydraulique1dTableauZoneModel {

  private MetierConditionsInitiales conditionsInitiales_;
  /**
   * Constructeur avec 5 colonnes ("n� bief", "abscisse d�but", "abscisse fin")
   * et 20 lignes vides � la fin du tableau.
   */
  public Hydraulique1dTableauZoneSecheModel() {
    super();
  }

  public void setConditionsInitiales(MetierConditionsInitiales conditionsInitiales) {
    conditionsInitiales_ =conditionsInitiales;
  }

  public void estimerZonesSeches() {
    listePts_ = new ArrayList();
    MetierLigneEauInitiale ligne =  conditionsInitiales_.ligneEauInitiale();
    MetierBief[] biefs = reseau_.biefs();
    for (int i = 0; i < biefs.length; i++) {
      int numBief = i+1;
      double abscBiefAmont = biefs[i].extrAmont().profilRattache().abscisse();
      double abscBiefAval = biefs[i].extrAval().profilRattache().abscisse();
      double abscLigneAmont = ligne.amontPointsBiefNumero(numBief);
      double abscLigneAval = ligne.avalPointsBiefNumero(numBief);
      if (ligne.existePointsBiefNumero(numBief)&&(abscLigneAmont != abscLigneAval)) {

        if (abscBiefAmont < abscLigneAmont) {
          // la ligne initiale ne couvre pas toute la partie amont du bief
          Hydraulique1dLigneZoneTableau ligTab = (Hydraulique1dLigneZoneTableau)creerLigneVide();
          ligTab.iBief(numBief);
          ligTab.abscDebut(abscBiefAmont);
          ligTab.abscFin(abscLigneAmont);
          listePts_.add(ligTab);
        }
        else if (abscBiefAmont > abscLigneAmont) {
          // La ligne d'eau initiale est trop � l'amont du bief  : probl�me de coh�rence.
          System.out.println("Hydraulique1dTableauZoneSecheModel estimerZonesSeches()");
          System.out.println("La ligne d'eau initiale du bief �"+numBief+" d�passe l'amont du bief");
        }

        if (abscBiefAval > abscLigneAval) {
          // la ligne initiale ne couvre pas toute la partie aval du bief
          Hydraulique1dLigneZoneTableau ligTab = (Hydraulique1dLigneZoneTableau)creerLigneVide();
          ligTab.iBief(numBief);
          ligTab.abscDebut(abscLigneAval);
          ligTab.abscFin(abscBiefAval);
          listePts_.add(ligTab);
        }
        else if (abscBiefAval < abscLigneAval) {
          // La ligne d'eau initiale est trop � l'aval du bief : probl�me de coh�rence.
          System.out.println("Hydraulique1dTableauZoneSecheModel estimerZonesSeches()");
          System.out.println("La ligne d'eau initiale du bief �"+numBief+" d�passe l'aval du bief");
        }
      } else {
        // la ligne initiale ne couvre pas le bief
        Hydraulique1dLigneZoneTableau ligTab = (Hydraulique1dLigneZoneTableau)creerLigneVide();
        ligTab.iBief(numBief);
        ligTab.abscDebut(abscBiefAmont);
        ligTab.abscFin(abscBiefAval);
        listePts_.add(ligTab);
      }
    }
    fireTableDataChanged();
  }
  /**
   * R�cup�re les donn�es de l'objet m�tier (DReseau) et les tranferts vers le mod�le de tableau.
   */
  @Override
  public void setValeurs() {
    if (conditionsInitiales_ != null) {
      listePts_ = new ArrayList();
      MetierZone[] zones = conditionsInitiales_.zonesSeches();
      for (int i = 0; i < zones.length; i++) {
        Hydraulique1dLigneZoneTableau lig = (Hydraulique1dLigneZoneTableau)creerLigneVide();
        lig.setMetier(zones[i]);
        listePts_.add(lig);
      }

      for (int i = 0; i < getNbLignesVideFin(); i++) {
        listePts_.add(creerLigneVide());
      }
      fireTableDataChanged();
    }
  }

  /**
   * Transferts les donn�es du tableau vers l'objet m�tier.
   * @return vrai s'il existe des diff�rences, faux sinon.
   */
  @Override
  public boolean getValeurs() {
    List listeTmp = getListeLignesCorrectes();
    MetierZone[] tabMetier = null;
    boolean existeDifference = false;

    MetierZone[] zones = conditionsInitiales_.zonesSeches();
    if (listeTmp.size() != zones.length) {
      existeDifference = true;
    }

    MetierZone[] zonesTmp = new MetierZone[listeTmp.size()];

    // initialise les premi�res lignes m�tiers communes � partir des lignes de tableau
    int tailleMin = Math.min(listeTmp.size(), zones.length);
    for (int i = 0; i < tailleMin; i++) {
      Hydraulique1dLigneZoneTableau zoneTab = (Hydraulique1dLigneZoneTableau) listeTmp.get(i);
      zonesTmp[i] = zones[i];
      if (!zoneTab.equals(zonesTmp[i])) {
        existeDifference = true;
        initLigneObjetMetier(zoneTab, zonesTmp[i]);
      }
    }

    // il existe plus de ligne dans le tableau que de lignes m�tier
    // => cr�ation de MetierZonePlanimetrage.
    if (listeTmp.size() > zones.length) {
      existeDifference = true;
      MetierZone[] nouveauxDZone = creeZones(listeTmp.size() - zones.length);
      int iNouveauxDZone = 0;
      for (int i = tailleMin; i < zonesTmp.length; i++) {
        Hydraulique1dLigneZoneTableau zoneTab = (Hydraulique1dLigneZoneTableau) listeTmp.get(i);
        zonesTmp[i] = nouveauxDZone[iNouveauxDZone];
        initLigneObjetMetier(zoneTab, zonesTmp[i]);
        iNouveauxDZone++;
      }
    }

    // il existe moins de ligne dans le tableau que de lignes m�tier
    // => suppression de MetierZonePlanimetrage.
    else if (listeTmp.size() < zones.length) {
      existeDifference = true;
      MetierZone[] zonesASupprimer = new MetierZone[zones.length -
          tailleMin];
      int iZonesASupprimer = 0;
      for (int i = tailleMin; i < zones.length; i++) {
        zonesASupprimer[iZonesASupprimer] = zones[i];
        iZonesASupprimer++;
      }
      supprimeZones(zonesASupprimer);
    }
    tabMetier = zonesTmp;
    if (existeDifference) {
      miseAJourModeleMetier(tabMetier);
    }
    return existeDifference;
  }


  /**
   * Mise � jour de l'objet m�tier container � partir d'un tableau d'objets m�tiers.
   * <br>Utilis� par getValeurs().
   * @param zones le tableau d'objets m�tier :
   */
  private void miseAJourModeleMetier(MetierZone[] zones) {
    conditionsInitiales_.zonesSeches(zones);
  }

  /**
   * supprime des zones s�ches
   * @param zonesASupprimer MetierZone[]
   */
  private void supprimeZones(MetierZone[] zonesASupprimer) {
    conditionsInitiales_.supprimeZonesSeches(zonesASupprimer);
  }

  /**
   * cree des zones
   * @param nb Le nombre de MetierZone � cr�er;
   * @return MetierZone[]
   */
  private MetierZone[] creeZones(int nb) {
    MetierZone[] zones = new MetierZone[nb];
    for (int i = 0; i < zones.length; i++) {
      zones[i] = new MetierZone();
    }
    return zones;
  }
}
