/*
 * @file         Hydraulique1dTableauZoneTailleModel.java
 * @creation     2004-07-09
 * @modification $Date: 2007-11-20 11:43:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2004 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hydraulique1d.tableau;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeries;
import org.fudaa.dodico.hydraulique1d.metier.MetierDefinitionSectionsParSeriesUnitaire;
import org.fudaa.dodico.hydraulique1d.metier.MetierZone;
import org.fudaa.dodico.hydraulique1d.metier.MetierZonePlanimetrage;

import java.util.*;

/**
 * Mod�le de tableau pour :
 * <br>- la liste de sections � partir d'une carte des tailles de mailles (MODE_MAILLAGE).
 * <br>- la liste des zones de planim�trage (MODE_PLANIMETRAGE). <br><br>
 * colonnes : indice bief, abscisse d�but, abscisse fin, taille ou pas.
 * @see Hydraulique1dLigne1EntierEtReelsTableau
 * @see Hydraulique1dTableauReelModel
 * @author Jean-Marc Lacombe
 * @version $Revision: 1.8 $ $Date: 2007-11-20 11:43:11 $ by $Author: bmarchan $
 */
public class Hydraulique1dTableauZoneTailleModel
    extends Hydraulique1dTableauZoneModel {
  /**
   * Mode maillage.
   */
  public static final int MODE_MAILLAGE=0;

  /**
   * Mode planim�trage.
   */
  public static final int MODE_PLANIMETRAGE=1;

  /**
   * Mode courant d'utilisation.
   * Peut valoir MODE_MAILLAGE ou MODE_PLANIMETRAGE.
   * Par d�faut, il vaut MODE_MAILLAGE.
   */
  private int modeCourant_=MODE_MAILLAGE;

  /**
   * Noms des colonnes en mode maillage.
   * "n� bief", "abscisse d�but", "abscisse fin" et "taille (m)".
   */
  private final static String[] NOMS_COL_MAILLAGE = {
      getS("n� bief"), getS("abscisse d�but"), getS("abscisse fin"), getS("taille (m)")};


  /**
   * Noms des colonnes en mode planim�trage.
   * "n� bief", "abscisse d�but", "abscisse fin" et "pas (m)".
   */
  private final static String[] NOMS_COL_PLANIMETRAGE = {
      getS("n� bief"), getS("abscisse d�but"), getS("abscisse fin"), getS("pas (m)")};

  private MetierDefinitionSectionsParSeries defSection_;

  /**
   * Constructeur avec 4 colonnes ("n� bief", "abscisse d�but", "abscisse fin", "pas" ou "taille")
   * et 20 lignes vides � la fin du tableau.
   * @param mode le mode d'utilisation du tableau. Peut valoir MODE_MAILLAGE ou MODE_PLANIMETRAGE.
   */
  public Hydraulique1dTableauZoneTailleModel(int mode) {
    super();
    modeCourant_=mode;
    if (isModeMaillage()) {
      setColumnNames(NOMS_COL_MAILLAGE);
    } else if (isModePlanimetrage()) {
      setColumnNames(NOMS_COL_PLANIMETRAGE);
    }
  }


  /**
   * Cree une nouvelle ligne vide.
   * Surcharge de la classe m�re.
   * @return une instance de Hydraulique1dLigneZoneTailleTableau.
   */
  @Override
  public Hydraulique1dLigneReelTableau creerLigneVide() {
    return new Hydraulique1dLigneZoneTailleTableau();
  }

  /**
   * Initialise le mod�le m�tier � partir d'une definition de maillage section par s�rie.
   * Indispensable en mode MAILLAGE
   * @param defSection MetierDefinitionSectionsParSeries
   */
  public void setModelMetier(MetierDefinitionSectionsParSeries defSection) {
    defSection_ = defSection;
    setColumnNames(NOMS_COL_MAILLAGE);
  }


  /**
   * R�cup�re les donn�es de l'objet m�tier et les tranferts vers le mod�le de tableau.
   */
  @Override
  public void setValeurs() {
    listePts_ = new ArrayList();
    if (isModeMaillage()) {
      MetierDefinitionSectionsParSeriesUnitaire[] defs = getDef();
      for (int i = 0; i < defs.length; i++) {
        Hydraulique1dLigneZoneTailleTableau lig = (Hydraulique1dLigneZoneTailleTableau)creerLigneVide();
        lig.setMetier(defs[i]);
        listePts_.add(lig);
      }
    } else if (isModePlanimetrage()) {
      MetierZonePlanimetrage[] zones =  reseau_.zonesPlanimetrage();
      for (int i = 0; i < zones.length; i++) {
        Hydraulique1dLigneZoneTailleTableau lig = (Hydraulique1dLigneZoneTailleTableau)creerLigneVide();
        lig.setMetier(zones[i]);
        listePts_.add(lig);
      }
    }

    for (int i = 0; i < getNbLignesVideFin(); i++) {
      listePts_.add(creerLigneVide());
    }
    fireTableDataChanged();
  }

  /**
   * Transferts les donn�es du tableau vers l'objet m�tier.
   * @return vrai s'il existe des diff�rences, faux sinon.
   */
  @Override
  public boolean getValeurs() {
    List listeTmp = getListeLignesCorrectes();
    Object[] tabMetier = null;
    boolean existeDifference = false;

    if (isModeMaillage()) {
      MetierDefinitionSectionsParSeriesUnitaire[] defs = getDef();
      if (listeTmp.size() != defs.length) {
        existeDifference = true;
      }
      MetierDefinitionSectionsParSeriesUnitaire[] zonesTmp = new
          MetierDefinitionSectionsParSeriesUnitaire[listeTmp.size()];

      // initialise les premi�res lignes m�tiers communes � partir des lignes de tableau
      int tailleMin = Math.min(listeTmp.size(), defs.length);
      for (int i = 0; i < tailleMin; i++) {
        Hydraulique1dLigneZoneTailleTableau zoneTab = (
            Hydraulique1dLigneZoneTailleTableau) listeTmp.get(i);
        zonesTmp[i] = defs[i];
        if (!zoneTab.equals(zonesTmp[i])) {
          existeDifference = true;
          initLigneObjetMetier(zoneTab, zonesTmp[i]);
        }
      }

      // il existe plus de ligne dans le tableau que de lignes m�tier
      // => cr�ation de MetierDefinitionSectionsParSeriesUnitaire.
      if (listeTmp.size() > defs.length) {
        existeDifference = true;
        MetierDefinitionSectionsParSeriesUnitaire[] nouveauxIDef =
            creeSectionsParSeries(listeTmp.size() - defs.length);
        int iNouveauxDZone = 0;
        for (int i = tailleMin; i < zonesTmp.length; i++) {
          Hydraulique1dLigneZoneTailleTableau zoneTab = (
              Hydraulique1dLigneZoneTailleTableau) listeTmp.get(i);
          zonesTmp[i] = nouveauxIDef[iNouveauxDZone];
          initLigneObjetMetier(zoneTab, zonesTmp[i]);
          iNouveauxDZone++;
        }
      }

      // il existe moins de ligne dans le tableau que de lignes m�tier
      // => suppression de MetierDefinitionSectionsParSeriesUnitaire.
      else if (listeTmp.size() < defs.length) {
        existeDifference = true;
        MetierDefinitionSectionsParSeriesUnitaire[] zonesASupprimer = new
            MetierDefinitionSectionsParSeriesUnitaire[defs.length - tailleMin];
        int iZonesASupprimer = 0;
        for (int i = tailleMin; i < defs.length; i++) {
          zonesASupprimer[iZonesASupprimer] = defs[i];
          iZonesASupprimer++;
        }
        supprimeDefinitionSectionsSerie(zonesASupprimer);
      }
      tabMetier = zonesTmp;
    }

    // MODE PLANIMETRAGE
    else if (isModePlanimetrage()) {
      MetierZonePlanimetrage[] zones = reseau_.zonesPlanimetrage();
      if (listeTmp.size() != zones.length) {
        existeDifference = true;
      }

      MetierZonePlanimetrage[] zonesTmp = new MetierZonePlanimetrage[listeTmp.size()];

      // initialise les premi�res lignes m�tiers communes � partir des lignes de tableau
      int tailleMin = Math.min(listeTmp.size(), zones.length);
      for (int i = 0; i < tailleMin; i++) {
        Hydraulique1dLigneZoneTailleTableau zoneTab = (
            Hydraulique1dLigneZoneTailleTableau) listeTmp.get(i);
        zonesTmp[i] = zones[i];
        if (!zoneTab.equals(zonesTmp[i])) {
          existeDifference = true;
          initLigneObjetMetier(zoneTab, zonesTmp[i]);
        }
      }

      // il existe plus de ligne dans le tableau que de lignes m�tier
      // => cr�ation de MetierZonePlanimetrage.
      if (listeTmp.size() > zones.length) {
        existeDifference = true;
        MetierZonePlanimetrage[] nouveauxDZone =
            creeZonesPlanimetrage(listeTmp.size() - zones.length);
        int iNouveauxDZone = 0;
        for (int i = tailleMin; i < zonesTmp.length; i++) {
          Hydraulique1dLigneZoneTailleTableau zoneTab = (
              Hydraulique1dLigneZoneTailleTableau) listeTmp.get(i);
          zonesTmp[i] = nouveauxDZone[iNouveauxDZone];
          initLigneObjetMetier(zoneTab, zonesTmp[i]);
          iNouveauxDZone++;
        }
      }

      // il existe moins de ligne dans le tableau que de lignes m�tier
      // => suppression de MetierZonePlanimetrage.
      else if (listeTmp.size() < zones.length) {
        existeDifference = true;
        MetierZonePlanimetrage[] zonesASupprimer = new
            MetierZonePlanimetrage[zones.length - tailleMin];
        int iZonesASupprimer = 0;
        for (int i = tailleMin; i < zones.length; i++) {
          zonesASupprimer[iZonesASupprimer] = zones[i];
          iZonesASupprimer++;
        }
        supprimeZonesPlanimetrage(zonesASupprimer);
      }
      tabMetier = zonesTmp;
    }
    if (existeDifference) {
      miseAJourModeleMetier(tabMetier);
    }
    return existeDifference;
  }

  @Override
  public String getMessageAvertissement() {
      boolean existeUneAbscisseAbsenteDuProfil = false;
      if (isModePlanimetrage()) {
          List list = getListeLignesCorrectes();
          for (Iterator iter = list.iterator(); iter.hasNext(); ) {
              Hydraulique1dLigneZoneTableau ligne = (
                      Hydraulique1dLigneZoneTableau) iter.next();
              int indiceBief = ligne.iBief() - 1;
              if (!reseau_.biefs()[indiceBief].contientProfilAbscisse(ligne.
                      abscDebut())) {
                  existeUneAbscisseAbsenteDuProfil = true;
                  break;
              }
              if (!reseau_.biefs()[indiceBief].contientProfilAbscisse(ligne.
                      abscFin())) {
                  existeUneAbscisseAbsenteDuProfil = true;
                  break;
              }
          }
      }
      if (existeUneAbscisseAbsenteDuProfil) {
          String res ="Les zones des pas de planim�trage seront d�finies sur les profils\n";
          res +=      "en aval de l'abscisse indiqu�e quand celle-ci ne correspond pas �\n";
          res +=      "l'abscisse d'un profil";
          return res;
      }
      return "";
  }

  private boolean isModeMaillage() {
    return (modeCourant_ == MODE_MAILLAGE);
  }

  private boolean isModePlanimetrage() {
    return (modeCourant_ == MODE_PLANIMETRAGE);
  }

  /**
   * R�cup�re le tableau de MetierDefinitionSectionsParSeriesUnitaire
   * En mode maillage uniquement.
   * @return MetierDefinitionSectionsParSeriesUnitaire[]
   */
  private MetierDefinitionSectionsParSeriesUnitaire[] getDef() {
    MetierDefinitionSectionsParSeriesUnitaire[] def = new
        MetierDefinitionSectionsParSeriesUnitaire[0];
    if (defSection_ != null) {
      def = defSection_.unitaires();
    }
    return def;
  }

  /**
   * Initialise la d�finition de maillage m�tier � partir d'une ligne tableau.
   * @param zoneTab Hydraulique1dLigneZoneTailleTableau
   * @param idef MetierDefinitionSectionsParSeriesUnitaire
   */
  private void initLigneObjetMetier(Hydraulique1dLigneZoneTailleTableau zoneTab,
                        MetierDefinitionSectionsParSeriesUnitaire idef) {
    idef.pas(zoneTab.taille());
    MetierZone izone = idef.zone();
    izone.abscisseDebut(zoneTab.abscDebut());
    izone.abscisseFin(zoneTab.abscFin());
    if (reseau_.biefs().length >= zoneTab.iBief()) {
      izone.biefRattache(reseau_.biefs()[zoneTab.iBief() - 1]);
    }
  }

  /**
   * Initialise le planimetrage m�tier � partir d'une ligne tableau.
   * @param zoneTab Hydraulique1dLigneZoneTailleTableau
   * @param izone MetierZonePlanimetrage
   */
  private void initLigneObjetMetier(Hydraulique1dLigneZoneTailleTableau zoneTab,
                        MetierZonePlanimetrage izone) {
    izone.taillePas(zoneTab.taille());
    izone.abscisseDebut(zoneTab.abscDebut());
    izone.abscisseFin(zoneTab.abscFin());
    if (reseau_.biefs().length >= zoneTab.iBief()) {
      izone.biefRattache(reseau_.biefs()[zoneTab.iBief() - 1]);
    }
  }

  /**
   * Mis � jour de l'objet m�tier contenaire � partir d'un tableau d'objets m�tier.
   * <br>Utilis� par getValeurs().
   * @param tab le tableau d'objets m�tier :
   * <br>- MetierDefinitionSectionsParSeriesUnitaire[] en mode maillage.
   * <br>- MetierZonePlanimetrage[] en mode planim�trage.
   */
  private void miseAJourModeleMetier(Object[] tab) {
    if (isModeMaillage()) {
      defSection_.unitaires((MetierDefinitionSectionsParSeriesUnitaire[])tab);
    }

    if (isModePlanimetrage()) {
      MetierZonePlanimetrage[] zones = (MetierZonePlanimetrage[])tab;
      for (int i = 0; i < reseau_.biefs().length; i++) {
        MetierBief b = reseau_.biefs()[i];
        ArrayList liste = new ArrayList();
        for (int j = 0; j < zones.length; j++) {
          if (b == zones[j].biefRattache()) {
            liste.add(zones[j]);
          }
        }
        MetierZonePlanimetrage[] zonesBief = (MetierZonePlanimetrage[])liste.toArray(new MetierZonePlanimetrage[liste.size()]);
        b.zonesPlanimetrage(zonesBief);
      }
    }
  }

  /**
   * supprimeDefinitionSectionsSerie
   *
   * @param zonesASupprimer MetierDefinitionSectionsParSeriesUnitaire[]
   */
  private void supprimeDefinitionSectionsSerie(MetierDefinitionSectionsParSeriesUnitaire[]
                             zonesASupprimer) {
    if (defSection_ != null) {
      defSection_.supprimeSeries(zonesASupprimer);
    }
  }

  /**
   * supprimeZonesPlanimetrage
   *
   * @param zonesASupprimer MetierZonePlanimetrage[]
   */
  private void supprimeZonesPlanimetrage(MetierZonePlanimetrage[] zonesASupprimer) {
    if (reseau_ != null) {
      reseau_.supprimeZonesPlanimetrage(zonesASupprimer);
    }
  }

  /**
   * cree SectionsParSeries
   *
   * @param nb Le nombre de MetierDefinitionSectionsParSeriesUnitaire � cr�er;
   * @return MetierDefinitionSectionsParSeriesUnitaire[]
   */
  private MetierDefinitionSectionsParSeriesUnitaire[] creeSectionsParSeries(int nb) {
    MetierDefinitionSectionsParSeriesUnitaire[] def = new
        MetierDefinitionSectionsParSeriesUnitaire[nb];
    for (int i = 0; i < def.length; i++) {
      def[i] = new MetierDefinitionSectionsParSeriesUnitaire();
    }
    return def;
  }
  /**
   * cree creeZonesPlanimetrage
   *
   * @param nb Le nombre de MetierDefinitionSectionsParSeriesUnitaire � cr�er;
   * @return MetierDefinitionSectionsParSeriesUnitaire[]
   */
  private MetierZonePlanimetrage[] creeZonesPlanimetrage(int nb) {
    MetierZonePlanimetrage[] zones = new MetierZonePlanimetrage[nb];
    for (int i = 0; i < zones.length; i++) {
      zones[i] = new MetierZonePlanimetrage();
    }
    return zones;
  }
}
