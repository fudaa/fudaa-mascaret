package org.fudaa.fudaa.hydraulique1d.tableau;

import org.fudaa.dodico.hydraulique1d.metier.calageauto.MetierCalageAuto;

/**
 * Interface commune aux modele de zone frottements � caler ou cal�es. 
 * @author Adrien
 *
 */
public interface ZoneFrottementCalageModelInterface {

	public void setCalageAuto(MetierCalageAuto _cal);
	
}
