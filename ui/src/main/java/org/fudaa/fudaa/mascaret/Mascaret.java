/*
 * @file         Mascaret.java
 * @creation     2000-07-04
 * @modification $Date: 2008-02-29 16:47:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.mascaret;

import com.memoire.bu.BuResource;
import javax.swing.UIManager;
import org.fudaa.fudaa.commun.impl.Fudaa;

/**
 * Cette classe contient la fonction Main() de Fudaa-Mascaret.
 *
 * @version $Revision: 1.13 $ $Date: 2008-02-29 16:47:09 $ by $Author: opasteur $
 * @author Jean-Marc Lacombe
 */
public class Mascaret {

  public static void main(String[] args) {
    try {
      //pour utiliser les icones crystal.
      //a voir si cela convient.
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    BuResource.BU.setIconFamily("crystal");
    Fudaa f = new Fudaa();
    f.setApplyLookAndFeel(false);
    f.setSplashTextVisible(false);
    f.launch(args, MascaretImplementation.informationsSoftware(), true);

    f.startApp(new MascaretImplementation(), MascaretApplication.class);

  }
}
