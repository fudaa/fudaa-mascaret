/*
 * @file         MascaretApplication.java
 * @creation     2000-07-04
 * @modification $Date: 2006-09-12 08:35:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.mascaret;

import java.awt.Dimension;
import java.awt.Point;

import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;

import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuRegistry;

/**
 * @version $Revision: 1.7 $ $Date: 2006-09-12 08:35:33 $ by $Author: opasteur $
 * @author Jean-Marc Lacombe
 */
public class MascaretApplication extends Hydraulique1dBaseApplication {

  public MascaretApplication() {
    super();

  }

  @Override
  public void exit() {
    if (confirmExit()) {
      if (BuPreferences.BU.getIntegerProperty("window.size") == 2) {
        Point p = getLocation();
        Dimension d = getSize();
        BuPreferences.BU.putIntegerProperty("window.x", p.x);
        BuPreferences.BU.putIntegerProperty("window.y", p.y);
        BuPreferences.BU.putIntegerProperty("window.w", d.width);
        BuPreferences.BU.putIntegerProperty("window.h", d.height);
      }
      BuPreferences.BU.putIntegerProperty("leftcolumn.width", getMainPanel().getLeftColumn().getWidth());
      BuPreferences.BU.putIntegerProperty("rightcolumn.width", getMainPanel().getRightColumn().getWidth());
      BuPreferences.BU.writeIniFile();

      //if(DEBUG)
      //FuLog.debug("BAN: try to unregister with "+BuRegistry.getInstance());
      BuRegistry.unregister(this);

      //if(FuLib.isJDistroRunning())
      //System.exit(0);
      MascaretImplementation impl = (MascaretImplementation) getImplementation();
      impl.fermerConnexions();
    }
    //else if(DEBUG) FuLog.debug("BAN: exit *NOT* confirmed");
  }

}
