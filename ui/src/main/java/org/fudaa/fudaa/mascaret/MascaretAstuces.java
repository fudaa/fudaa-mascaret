package org.fudaa.fudaa.mascaret;

import com.memoire.bu.BuPreferences;

import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;

/**
 * Classe pour les astuces du logiciel Fudaa-Mascaret.
 *
 * @version      $Revision: 1.5 $ $Date: 2007-01-19 13:14:12 $ by $Author: deniger $
 * @author       Olivier Pasteur
 */
public class MascaretAstuces extends FudaaAstucesAbstract {

    /**
  * renvoie un nouvel objet MascaretAstuces.
  */
 public static MascaretAstuces MASCARET= new MascaretAstuces();
 /**
  * renvoie un nouvel objet FudaaAstuces (parent de
  * OscarAstuces).
  * @return une instance d'objet <code>FudaaAstuces</code>
  */
  @Override
 protected FudaaAstucesAbstract getParent() {
   FudaaAstuces _fa= FudaaAstuces.FUDAA;
   return _fa;
 }
 /**
  * renvoie un nouvel objet MascaretPreferences.
  * @return une instance d'objet <code>MascaretPreferences</code>
  */
  @Override
 protected BuPreferences getPrefs() {
   BuPreferences bp= MascaretPreferences.MASCARET;
   return bp;
 }



   public static void main(String argv[]) { for(int i = 0; i < 20; i++) {
           System.out.println("num "+i); System.out.println(hasard(i)); }
   }

}
