/*
 * @file         MascaretImplementation.java
 * @creation     2000-07-04
 * @modification $Date: 2008-03-07 09:47:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.mascaret;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileFilter;

import org.fudaa.dodico.boony.BoonyLib;
import org.fudaa.dodico.corba.calcul.SProgression;
import org.fudaa.dodico.corba.mascaret.ICalculMascaret;
import org.fudaa.dodico.corba.mascaret.ICalculMascaretHelper;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.hydraulique1d.metier.EnumMetierRegime;
import org.fudaa.dodico.hydraulique1d.metier.MetierBief;
import org.fudaa.dodico.hydraulique1d.metier.MetierCalculHydraulique1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierCasier;
import org.fudaa.dodico.hydraulique1d.metier.MetierDescriptionVariable;
import org.fudaa.dodico.hydraulique1d.metier.MetierEtude1d;
import org.fudaa.dodico.hydraulique1d.metier.MetierReseau;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsGeneraux;
import org.fudaa.dodico.hydraulique1d.metier.MetierResultatsTemporelSpatial;
import org.fudaa.dodico.hydraulique1d.metier.casier.MetierGeometrieCasier;
import org.fudaa.dodico.hydraulique1d.metier.evenement.H1dObjetEvent;
import org.fudaa.dodico.hydraulique1d.metier.evenement.Notifieur;
import org.fudaa.dodico.hydraulique1d.metier.sediment.MetierCalculSediment;
import org.fudaa.dodico.mascaret.DCalculMascaret;
import org.fudaa.dodico.mascaret.DResultatsMascaret;
import org.fudaa.dodico.mascaret.FichierMascaretException;
import org.fudaa.dodico.objet.DodicoBoonyLibImpl;
import org.fudaa.ebli.impression.EbliFillePrevisualisation;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaTee;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.commun.projet.FudaaParamChangeLog;
import org.fudaa.fudaa.commun.projet.FudaaParamEventView;
import org.fudaa.fudaa.commun.projet.FudaaProjetInformationsFrame;
import org.fudaa.fudaa.hydraulique1d.CGlobal;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseApplication;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dBaseImplementation;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExportPanneau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dExportXMLPanneau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImport;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImportCasPanneau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImportXMLPanneau;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dProjet;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dResource;
import org.fudaa.dodico.hydraulique1d.conv.CConversionHydraulique1d;
import org.fudaa.dodico.hydraulique1d.conv.ConvH1D_Masc;
import org.fudaa.dodico.hydraulique1d.conv.ConvMasc_H1D;
import org.fudaa.fudaa.hydraulique1d.editor.BiefEditor;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHMRepository;
import org.fudaa.fudaa.hydraulique1d.ihmhelper.Hydraulique1dIHM_Bief;
import org.fudaa.fudaa.hydraulique1d.reseau.Hydraulique1dReseauFrame;
import org.fudaa.fudaa.hydraulique1d.reseau.Hydraulique1dReseauGridAdapter;
import org.fudaa.fudaa.hydraulique1d.reseau.Hydraulique1dReseauMouseAdapter;
import org.fudaa.fudaa.hydraulique1d.reseau.MascaretGridInteractive;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuApplication;
import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuBrowserPreferencesPanel;
import com.memoire.bu.BuColumn;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuMenuRecentFiles;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuPrinter;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTaskOperation;
import com.memoire.bu.BuTaskView;
import com.memoire.bu.BuToolBar;
import com.memoire.bu.BuUserPreferencesPanel;
import com.memoire.dja.DjaFrame;
import com.memoire.dja.DjaGridInteractive;
import com.memoire.fu.FuLib;
import com.memoire.bu.BuLib;
import com.memoire.fu.FuLog;

import java.awt.EventQueue;

import javax.swing.JPanel;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.fudaa.hydraulique1d.Hydraulique1dImportXCasPanneau;
import org.fudaa.fudaa.mascaret.factory.OsFactory;

/**
 * @author Jean-Marc Lacombe
 */
final public class MascaretImplementation extends Hydraulique1dBaseImplementation {

  public static ICalculMascaret SERVEUR_MASCARET = null;
  public static IConnexion CONNEXION_MASCARET = null;
//  public static IConnexion CONNEXION_HYDRO1D= null;
  public final static BuInformationsSoftware isApp_ = new BuInformationsSoftware();
  public final static BuInformationsDocument idApp_ = new BuInformationsDocument();
  //public static IPersonne PERSONNE= null;
  private static MetierCalculHydraulique1d calcul_ = null;
  private static CConversionHydraulique1d HYDRO_KONVERT = null;
  private static boolean DEMO_VERSION = false;
  private final static int RECENT_COUNT = 10;
  // donnees membres privees
//  private DObjetEventListenerSupport evtSupport_;
  private MetierEtude1d ietude1d_;
  private MetierResultatsTemporelSpatial ires_;
  private BuAssistant assistant_;
//  protected BuHelpFrame aide_;
  private Hydraulique1dProjet projet_;
  private MascaretGridInteractive grille_;
  private FudaaProjetInformationsFrame fProprietes_;
  private Hydraulique1dIHMRepository ihmP_;
  private boolean enregistrer_;
  private FudaaParamEventView msgView_;
  private FileFilter fileFilter_;
  protected EbliFillePrevisualisation previsuFille_;
  /**
   * Un boolean pour signifier que le calcul est en cours.
   */
  private boolean bcalcul_ = false;
  //private ICalculMascaret calculEnCours = null;

  static {
    isApp_.name = "Fudaa-Mascaret";
    isApp_.version = "3.6";
    isApp_.date = "July 2017";
    isApp_.rights = MascaretResource.MASCARET.getString("Tous droits r�serv�s") + ". EDF-CEREMA (c) 2001-2015";
//    isApp_.contact = "http://www.openmascaret.org/index.php?option=com_kunena&view=category&Itemid=177&layout=list";
    isApp_.contact = "ot-consultancy@opentelemac.org";
    isApp_.license = MascaretResource.MASCARET.getString("licences GPL pour Fudaa-Mascaret et le noyau de calcul");
    isApp_.languages = "fr,en";
    isApp_.http = "http://www.openmascaret.org/";
    isApp_.update = "";
    isApp_.man = FudaaLib.LOCAL_MAN;
    isApp_.logo = MascaretResource.MASCARET.getIcon("fudaamascaretlogo");
    isApp_.banner = MascaretResource.MASCARET.getIcon("fudaamascaretbanner");
    final String author = "EDF R&D " + MascaretResource.MASCARET.getString("et") + " CEREMA";
    isApp_.authors = new String[]{author};
    isApp_.contributors = isApp_.authors;
    isApp_.documentors = isApp_.authors;
    isApp_.testers = isApp_.authors;
    isApp_.specifiers = new String[]{"EDF R&D"};
    isApp_.libraries = null;
    isApp_.thanks = new String[]{"Fudaa team"};
    idApp_.name = "Fudaa-Mascaret";
    idApp_.version = "3.6";
    idApp_.organization = "EDF-CEREMA";
    idApp_.author = author;
    idApp_.contact = "http://www.openmascaret.org/";
    idApp_.date = FuLib.date();
    idApp_.logo = MascaretResource.MASCARET.getIcon("fudaamascaretlogo");

    isApp_.citation = "Jean-Marc Lacombe";
    BuPrinter.INFO_LOG = isApp_;
    BuPrinter.INFO_DOC = idApp_;
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return isApp_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isApp_;
  }
  // Constructeur

  public MascaretImplementation() {
    super();

    checkOs64bits();
  }

  private void checkOs64bits() {

    if (!OsFactory.isOs64bits()) {
      new BuDialogMessage(
              getApp(),
              getInformationsSoftware(),
              Hydraulique1dResource.HYDRAULIQUE1D.getString("Attention, vous devez disposer d'un syst�me d'exploitation 64 bits")).
              activate();
      this.fermer();
    }

  }

  @Override
  public void init() {
    super.init();

    if (BoonyLib.getBoonyImpl() == null) {
      BoonyLib.setBoonyImpl(new DodicoBoonyLibImpl());
    }
    try {
      if ((BuPreferences.BU.getStringProperty("button.icon") == null)
              || (BuPreferences.BU.getStringProperty("button.icon").isEmpty())) {
        BuPreferences.BU.putStringProperty("button.icon", "true");
      }
      if ((BuPreferences.BU.getStringProperty("button.text") == null)
              || (BuPreferences.BU.getStringProperty("button.text").isEmpty())) {
        BuPreferences.BU.putStringProperty("button.text", "false");
        //ts_=null;
      }
      BuInformationsSoftware infSoft = getInformationsSoftware();
      getApp().setTitle("Fudaa-Mascaret");
      BuMenuBar mb = BuMenuBar.buildBasicMenuBar();
      getApp().setMainMenuBar(mb);
      mb.addActionListener(this);
      mb.addMenu(buildHydrauliqueMenu());
      if (CGlobal.AVEC_QUALITE_DEAU) {
        mb.addMenu(buildQualiteDEauMenu());
      }
      mb.addMenu(buildSedimentMenu());
      mb.addMenu(buildResultatMenu());
      if (CGlobal.AVEC_CALAGE_AUTO) {
        mb.addMenu(buildCalageMenu());
      }
      mb.addMenu(buildSyntheseMenu());

      ((BuMenu) mb.getMenu("IMPORTER")).addMenuItem(_("G�om�trie casiers"), "IMPORT_GEOMETRIE_CASIERS", true);
      ((BuMenu) mb.getMenu("IMPORTER")).addMenuItem(_("G�om�trie biefs"), "IMPORT_GEOMETRIE_BIEFS", true);
      ((BuMenu) mb.getMenu("IMPORTER")).addMenuItem(_("Mascaret (cas et geo)"), "IMPORTMASCARET", true);
      ((BuMenu) mb.getMenu("IMPORTER")).addMenuItem(_("Mascaret XML (.xcas)"), "IMPORTXCAS", true);
      ((BuMenu) mb.getMenu("IMPORTER")).addMenuItem(_(".masc (2 XML)"), "IMPORTXML", true);

      buildExporterMenu();
      JMenuItem code1dItem = new JMenuItem(_("Noyau de calcul") + "...");
      code1dItem.setActionCommand("ABOUTCODE1D");
      code1dItem.addActionListener(this);
      ((BuMenu) mb.getMenu("MENU_AIDE")).insert(code1dItem, 8);

      // Toolbar
      BuToolBar tb = BuToolBar.buildBasicToolBar();
      tb.addSeparator();
      tb.addToolButton(_("Connecter"), "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      tb.addToolButton(_("Calculer"), "CALCULER", true);
      if (CGlobal.AVEC_CALAGE_AUTO) {
        tb.addToolButton(_("Calage"), _("Lancement d'un calcul de calage"), "CALCUL_CALAGE", Hydraulique1dResource.HYDRAULIQUE1D.getIcon(
                "hydraulique1dcalage"), false);
      }
      tb.addToolButton(_("Arr�ter"), "ARRETER", BuResource.BU.getIcon("crystal22_arreter.png"), false);
      tb.addActionListener(this);
      setMainToolBar(tb);

      // Suppression d'actions par d�faut.
      removeAction("RECHERCHER");
      removeAction("REMPLACER");
      removeAction("DUPLIQUER");
      removeAction("CREER");
      //GENESIS: d�sactiver l'aide temporairement car plus d'actualit�.
      removeAction("INDEX_ALPHA");
      removeAction("INDEX_THEMA");
      removeAction("AIDE_INDEX");
      removeAction("POINTEURAIDE");
      //fred inutile fait par FudaaCommonImplementation
      ((BuMenu) mb.getMenu("MENU_EDITION")).addMenuItem(
              "Console",
              "CONSOLE",
              FudaaTee.isCreated());

      getApp().setEnabledForAction("PREFERENCE", true);
      getApp().setEnabledForAction("CREER", true);
      getApp().setEnabledForAction("OUVRIR", true);
      getApp().setEnabledForAction("IMPORTER", true);
      getApp().setEnabledForAction("PROPRIETE", false);
      getApp().setEnabledForAction("MASCARET", false);
      getApp().setEnabledForAction("CALCULER", false);
      getApp().setEnabledForAction("ARRETER", false);
      getApp().setEnabledForAction("QUITTER", true);
      getApp().setEnabledForAction("ARRETER", false);
      getApp().setEnabledForAction("ENREGISTRER", enregistrer_);
      getApp().setEnabledForAction("ENREGISTRERSOUS", false);
      getApp().setEnabledForAction("EXPORTER", false);
      getApp().removeAction("VISIBLE_LEFTCOLUMN");
      setEnableMenu();
      assistant_ = new BuAssistant();
      //assistant_.setLog(false);
      MascaretResource.ASSISTANT = assistant_;
      Hydraulique1dResource.setAssistant(assistant_);
      BuMainPanel mp = getApp().getMainPanel();
      //BuColumn lc= mp.getLeftColumn();
      BuColumn rc = mp.getRightColumn();
      rc.addToggledComponent(_("Assistant"), "ASSISTANT", assistant_, this);
      BuTaskView taches_ = new BuTaskView();
      BuScrollPane sp = new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      rc.addToggledComponent(_("T�ches"), "TACHE", sp, this);
      getMainPanel().setTaskView(taches_);
      msgView_ = new FudaaParamEventView();
      BuScrollPane sp2 = new BuScrollPane(msgView_);
      sp2.setPreferredSize(new Dimension(150, 80));
      rc.addToggledComponent("Messages", "MESSAGE", sp2, this);

//      rl.setPreferredSize(new Dimension(120, 120));
//      rl.setMinimumSize(new Dimension(120, 120));
      setMessageView(msgView_);
      FudaaParamChangeLog.CHANGE_LOG.setApplication(
              (BuApplication) Hydraulique1dBaseApplication.FRAME,
              msgView_);
      BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(MascaretPreferences.MASCARET);
        mr.setResource(MascaretResource.MASCARET);
      }
      mp.setLogo(infSoft.logo);
    } catch (Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }

  @Override
  public void start() {
    super.start();
    BuPreferences.BU.applyOn(getApp());
    projet_ = Hydraulique1dProjet.getInstance();
    projet_.setCommonImplementation(this);
    assistant_.changeAttitude(
            BuAssistant.PAROLE,
            _("Bienvenue dans ") + isApp_.name + " !\n");
    final BuMainPanel mp = getMainPanel();
    BuInformationsSoftware infSoft = getInformationsSoftware();
    mp.setLogo(infSoft.logo);
    BuMenuBar mb = getMainMenuBar();
    mb.removeActionListener(this);
    assistant_.addEmitters((Container) getApp());
    mb.addActionListener(this);
    fProprietes_ = new FudaaProjetInformationsFrame(this);
    ihmP_ = Hydraulique1dIHMRepository.getInstance();
    projet_.setIhmRepository(ihmP_);

    final BuColumn rl = mp.getLeftColumn();
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        ensureRigthColumnIsVisible(rl);
      }
    });

    //Genesis: autorisation de la version 1.7
    String versionJava = System.getProperty("java.version").substring(0, 3);
    if (versionJava.equalsIgnoreCase("1.7") || versionJava.equalsIgnoreCase("1.8")) {
      FuLog.debug(_("version de java correcte"));
    } else {
      error(_("Version de Java"), _("La version de java est incorrecte") + " "
              + System.getProperty("java.version") + "\n" + _("Une version 1.7 � 1.8 est n�cessaire"));
      System.exit(0);
    }
  }

  public void ensureRigthColumnIsVisible(BuColumn rl) {
    rl.setVisible(true);
    int h = (int) rl.getPreferredSize().getHeight();
    rl.setMinimumSize(new Dimension(LEFT_COLUMN_MIN_WIDTH, h));
    if (rl.getWidth() < LEFT_COLUMN_MIN_WIDTH) {
      rl.setPreferredSize(new Dimension(LEFT_COLUMN_MIN_WIDTH, h));
      rl.setSize(new Dimension(LEFT_COLUMN_MIN_WIDTH, h));
      rl.revalidate();
      rl.getParent().revalidate();
    }
    getMainPanel().updateSplits();
    getMainPanel().updateDisplay();
  }
  protected static final int LEFT_COLUMN_MIN_WIDTH = 155;

  @Override
  protected String _(String _s) {
    return MascaretResource.MASCARET.getString(_s);
  }

  public void setMessageView(FudaaParamEventView v) {
    msgView_ = v;
  }

  public FudaaParamEventView getMessageView() {
    return msgView_;
  }
  // Actions

  @Override
  public void actionPerformed(ActionEvent _evt) {
    try {
      //BuInformationsSoftware il= getInformationsSoftware();
      //JComponent source= (JComponent)_evt.getSource();
      String action = _evt.getActionCommand();
      String arg = "";
      int i = action.indexOf('(');
      if (i >= 0) {
        arg = action.substring(i + 1, action.length() - 1);
        action = action.substring(0, i);
      }

      if (action.equals("ABOUTCODE1D")) {
        String avecsansQE = _("avec qualit� d'eau du");
        if (!CGlobal.AVEC_QUALITE_DEAU) {
          avecsansQE = _("sans qualit� d'eau du");
        }
        new BuDialogMessage(
                getApp(),
                getInformationsSoftware(),
                _("Noyaux de calcul") + ":\n  8.1.4 " + avecsansQE + " 18/07/2017\n" + _("Auteur") + ":\n  EDF - CEREMA - ARTELIA \n" + _("Contact") + ":\n  ot-consultancy@opentelemac.org").
                activate();
      } else if (action.equals("CREER")) {
        creer();
      } else if (action.equals("OUVRIR")) {
        ouvrir(null);
      } else if (action.equals("REOUVRIR")) {
        ouvrir(arg);
      } else if (action.equals("ENREGISTRER")) {
        enregistrer();
      } else if (action.equals("ENREGISTRERSOUS")) {
        enregistrerSous();
      } else if (action.equals("FERMER")) {
        fermer();

      } else if (action.equals("PROPRIETE")) {
        if (fProprietes_.getDesktopPane() != getMainPanel().getDesktop()) {
          addInternalFrame(fProprietes_);
        } else {
          activateInternalFrame(fProprietes_);
        }
      } else if (action.equals("EXPORTMASCARET")) {
        exporterMascaret();
      } else if (action.equals("EXPORTXML")) {
        exporterXML();
      } else if (action.equals("IMPORTMASCARET")) {
        importerMascaret();
      } else if (action.equals("IMPORTXCAS")) {
        importerXCas();
      } else if (action.equals("IMPORTXML")) {
        importerXML();
      } else if (action.equals("EXPORT_IMAGES")) {
        exportImage();
      } else if (action.equals("IMPORT_GEOMETRIE_CASIERS")) {
        importerGeoCasiers();
      } else if (action.equals("IMPORT_GEOMETRIE_BIEFS")) {
        importerGeoBiefs();
      } else if (action.equals("CALCULER")) {
        calculer(false);
      } else if (action.equals("ARRETER")) {
        arreter();
      } else if (action.equals("NOYAU")) {
        noyau();
      } else if (action.equals("RESEAU")) {
        reseau();
      } else if (action.equals("LOIS")) {
        lois();
      } else if (action.equals("MAILLAGE")) {
        maillage();
      } else if (action.equals("PROFILS_EDIT")) {
        editProfils();
      } else if (action.equals("COND_INITIALES")) {
        condInitiales();
      } else if (action.equals("LIGNE_INITIALE")) {
        ligneInitiale();
      } else if (action.equals("PARAM_TEMPOREL")) {
        paramTemporel();
      } else if (action.equals("LAISSES_DE_CRUES")) {
        laissesCrues();
      } else if (action.equals("ZONES_DE_FROTTEMENT")) {
        zonesFrottement();
      } else if (action.equals("ZONES_SECHES")) {
        zonesSeches();
      } else if (action.equals("PARAMETRES")) {
        parametres();
      } else if (action.equals("FICHIER_REPRISE")) {
        paramReprise();
      } else if (action.equals("PARAM_RESULTAT")) {
        paramResultat();
      } else if (action.equals("VARIABLES_RESULTAT")) {
        variableResultat();
      } else if (action.equals("EDITION_SITE")) {
        sitesStockage();
      } else if (action.equals("RESULTATS_GENERAUX")) {
        resultatsGeneraux();
      } else if (action.equals("GRAPHES_RESULTATS")) {
        graphesResultats(null);
      } else if (action.equals("VISU_COND_INIT")) {
        visuConditionsInitiales();
      } else if (action.equals("TABLEAUX")) {
        tableaux();
      } else if (action.equals("FROT_INIT_CALAGE")) {
        zonesFrottementACaler();
      } else if (action.equals("CRUES_CALAGE")) {
        cruesCalage();
      } else if (action.equals("PARAM_CALAGE")) {
        parametresCalage();
      } else if (action.equals("RESULTATS_CALAGE")) {
        listingsCalage();
      } else if (action.equals("GRAPHES_CALAGE")) {
        graphesResultats(ietude1d_.calageAuto().resultats().resultats(),true);
      } else if (action.equals("FROT_CALAGE")) {
        zonesFrottementCalees();
      } else if (action.equals("CALCUL_CALAGE")) {
        calculer(true);
      } else if (action.equals("PARAM_SEDIMENT")) {
        paramsSediment();
      } else if (action.equals("MODELEQUALITEDEAU")) {
        modeleQualiteDEau();
      } else if (action.equals("LOIS_POUR_QUALITEDEAU")) {
        loiPourQualiteDEau();
      } else if (action.equals("CONCENT_INIT")) {
        concentrationsInitiales();
      } else if (action.equals("PARAMETRES_GENERAUX_QUALITEDEAU")) {
        paramsGenerauxQualiteDEau();
      } else if (action.equals("PARAM_RESULTAT_POUR_QUALITEDEAU")) {
        paramsGResultatPourQualiteDEau();
      } else if (action.equals("ASSISTANT")
              || action.equals("TACHE")
              || action.equals("MESSAGE")) {
        BuColumn rc = getMainPanel().getRightColumn();
        rc.toggleComponent(action);
        setCheckedForAction(action, rc.isToggleComponentVisible(action));
      } else {
        super.actionPerformed(_evt);
      }
    } catch (Throwable t) {
      t.printStackTrace();
      if (t.getMessage() != null) {
        new BuDialogError(getApp(), getInformationsSoftware(), t.getMessage())
                .activate();
      }
    }
  }

 

public void oprCalculer() {
    try {
      etat = AUTORISE_CALCULER;
      setEnableMenu();
      if (!isConnected()) {
        new BuDialogError(
                getApp(),
                isApp_,
                _("vous n'etes pas connect� � un serveur MASCARET !"))
                .activate();
        return;
      }

      if (ietude1d_.resultatsGeneraux() != null) {
        MetierResultatsGeneraux ires = ietude1d_.resultatsGeneraux();
        ires.dispose();
        System.gc();
      }

      bcalcul_ = true;
      setEnableMenu();

      //BuMainPanel mp= getMainPanel();
      System.err.println(_("Transmission des parametres") + "...");
      HYDRO_KONVERT.convertirParametres(false, true);
      System.err.println(_("Execution du calcul") + "...");

      ICalculMascaret calculEnCours = (ICalculMascaret) HYDRO_KONVERT.calculHydraulique1d.calculCode();
      calculEnCours.setNoyau(ietude1d_.paramGeneraux().noyauV5P2());
      calculEnCours.calcul(CONNEXION_MASCARET);
      String avertissement = SERVEUR_MASCARET.getAvertissements();
      //System.err.println("AVERTISSEMENT "+avertissement);
      HYDRO_KONVERT.convertirResultats(avertissement);

      if (ietude1d_.resultatsGeneraux() != null && ietude1d_.resultatsGeneraux().hasResultatsTemporelSpatial()) {
        // On ajoute les nouveaux r�sultats s�dimentaires
        if (ietude1d_.sediment().parametres().isActif()) {
          MetierCalculSediment calSed = new MetierCalculSediment(ietude1d_.resultatsGeneraux().resultatsTemporelSpatial(), ietude1d_.sediment().
                  parametres());
          calSed.computeResultsForVariables();
        }
      }

      if (ietude1d_.resultatsGeneraux() != null) {
        MetierResultatsGeneraux resGen = ietude1d_.resultatsGeneraux();
        if ((resGen.messagesEcran() != null)
                && (resGen.messagesEcran().length != 0)) {
          setEnableMenu(AUTORISE_RESULTATS_GENERAUX);
        } else if ((resGen.listing() != null) && (resGen.listing().length != 0)) {
          setEnableMenu(AUTORISE_RESULTATS_GENERAUX);
//        } else if ((resGen.listingDamocles() != null)
//                && (resGen.listingDamocles().length != 0)) {
//          setEnableMenu(AUTORISE_RESULTATS_GENERAUX);
        }
      }
      if ((ietude1d_.resultatsGeneraux().hasResultatsTemporelSpatial())
              && (ietude1d_
              .resultatsGeneraux()
              .resultatsTemporelSpatial()
              .descriptionVariables()
              != null)
              && (ietude1d_
              .resultatsGeneraux()
              .resultatsTemporelSpatial()
              .descriptionVariables().length
              != 0)) {
        setEnableMenu(AUTORISE_GRAPHES_RESULTATS);
      }

    } catch (Throwable u) {
      new BuDialogError(getApp(), isApp_, u.getLocalizedMessage()).activate();
      u.printStackTrace();
      return;
    } finally {
      bcalcul_ = false;
      setEnableMenu();
    }
  }

  /**
   * Op�ration de calcul pour le calage
   */
  public void oprCalculerCalage() {
    try {
      bcalcul_ = true;
      setEnableMenu();
//      setEnabledForAction("CALCUL_CALAGE", false);
//      setEnabledForAction("CALCULER", false);

      if (!isConnected()) {
        new BuDialogError(
                getApp(),
                isApp_,
                _("vous n'etes pas connect� � un serveur MASCARET !"))
                .activate();
        return;
      }

      if (ietude1d_.calageAuto().resultats() != null) {
        ietude1d_.calageAuto().resultats().dispose();
        ietude1d_.calageAuto().resultats(null);
        System.gc();
      }

      System.err.println(_("Transmission des parametres..."));
      HYDRO_KONVERT.convertirParametres(true, true);
      System.err.println(_("Execution du calcul..."));

      ICalculMascaret calculEnCours = (ICalculMascaret) HYDRO_KONVERT.calculHydraulique1d.calculCode();
      //Calage Auto � partir en 6.1
      calculEnCours.setNoyau(false);
      calculEnCours.calcul(CONNEXION_MASCARET);

      //HYDRO_KONVERT.calculHydraulique1d.calculCode().calcul(CONNEXION_MASCARET);
      HYDRO_KONVERT.convertirResultatsPourCalageAuto();

      String avertissement = SERVEUR_MASCARET.getAvertissements();
      HYDRO_KONVERT.convertirResultats(avertissement);
      if (SERVEUR_MASCARET.IsAfficherResultats()) {
        resultatsGeneraux();
      }
    } catch (Throwable u) {
      new BuDialogError(getApp(), isApp_, u.getLocalizedMessage()).activate();
      u.printStackTrace();
      return;
    } finally {
      bcalcul_ = false;
      setEnableMenu();
    }
  }

  // ObjetEventListener
  @Override
  public void objetCree(H1dObjetEvent e) {
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    if (!enregistrer_) {
      enregistrer_ = true;
      setEnabledForAction("ENREGISTRER", enregistrer_);
    }
    setEnableMenu();
  }

  @Override
  public void objetSupprime(H1dObjetEvent e) {
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    if (!enregistrer_) {
      enregistrer_ = true;
      setEnabledForAction("ENREGISTRER", enregistrer_);
    }
    setEnableMenu();
  }

  @Override
  public void objetModifie(H1dObjetEvent e) {
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(
            e.getMessage() + " (" + e.getChamp() + ")");
    if (!enregistrer_) {
      enregistrer_ = true;
      setEnabledForAction("ENREGISTRER", enregistrer_);
    }
    setEnableMenu();
  }

  void fermerConnexions() {
    super.closeConnexions();
  }

  @Override
  public void exit() {
    fermer();
    getApp().exit();

//    super.exit();
  }

  @Override
  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * @return Renvoie les preferences de l'application
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  @Override
  public BuPreferences getApplicationPreferences() {
    return MascaretPreferences.MASCARET;
  }

  /**
   * Utilise pour les connexions : si l'utilisateur veut se reconnecter il faut fermer tous les projets.
   *
   * @return true car le projet est Dodico 2
   * @see org.fudaa.fudaa.commun.impl.FudaaImplementation#isDodico2Project()
   */
  @Override
  public boolean isDodico2Project() {
    return true;
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaImplementation#clearVariables()
   */
  @Override
  protected void clearVariables() {
    fermer();
    CONNEXION_MASCARET = null;
//    CONNEXION_HYDRO1D=null;
    SERVEUR_MASCARET = null;
    calcul_ = null;
  }

  /**
   * @return Les taches de connexions.
   * @see org.fudaa.fudaa.commun.impl.FudaaImplementation#getTacheConnexionMap()
   */
  @Override
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_MASCARET, CONNEXION_MASCARET);
//    FudaaDodicoTacheConnexion c1=new FudaaDodicoTacheConnexion(calcul_,CONNEXION_HYDRO1D);
//    return new FudaaDodicoTacheConnexion[]{c,c1};
    return new FudaaDodicoTacheConnexion[]{c};
  }

  /**
   * @return Les classes des taches delegu�es.
   * @see org.fudaa.fudaa.commun.impl.FudaaImplementation#getTacheDelegateClass()
   */
  @Override
  protected Class[] getTacheDelegateClass() {
    return new Class[]{DCalculMascaret.class};
  }

  /**
   * Initialise les connexions.
   *
   * @param _r La Map des Taches de connexions identifi�s par leur classe de tache d�l�gu�e.
   * @see org.fudaa.fudaa.commun.impl.FudaaImplementation#initConnexions(java.util.Map)
   */
  @Override
  protected void initConnexions(Map _r) {
    FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculMascaret.class);
    CONNEXION_MASCARET = c.getConnexion();
    SERVEUR_MASCARET = ICalculMascaretHelper.narrow(c.getTache());
//  c=(FudaaDodicoTacheConnexion)_r.get(MetierCalculHydraulique1d.class);
//  CONNEXION_HYDRO1D=c.getConnexion();
//  calcul_=DCalculHydraulique1dHelper.narrow(c.getTache());
    calcul_ = new MetierCalculHydraulique1d();
    calcul_.calculCode(SERVEUR_MASCARET);
    HYDRO_KONVERT
            = new CConversionHydraulique1d(calcul_, CONNEXION_MASCARET);
    /*  if (evtSupport_ != null) {
     UsineLib.findUsine().removeObjetEventListener(
     (DHydraulique1dEventListenerSupport)evtSupport_.tie());
     }
     evtSupport_= DObjetEventListenerSupport.createEventSupport();
     evtSupport_.clientListener(this);
     UsineLib.findUsine().addObjetEventListener(
     (DHydraulique1dEventListenerSupport)evtSupport_.tie());*/
    Notifieur.getNotifieur().addObjetEventListener(this);
  }

  /**
   * Ajoute les propres panneaux de pref.
   *
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#buildPreferences()
   */
  @Override
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
//    _prefs.add(new BuLookPreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
  }
  // Menu Mascaret

  private BuMenu buildHydrauliqueMenu() {
    BuMenu r = new BuMenu(_("Hydraulique"), "HYDRAULIQUE");
    r.addSeparator(_("Param�tres"));
    r.addMenuItem(_("Noyau Calcul"), "NOYAU", false);
    r.addMenuItem(_("Edition du r�seau hydraulique"), "RESEAU", false);
    r.addMenuItem(_("Profils"), "PROFILS_EDIT", false);    
    r.addMenuItem(_("Catalogue des lois"), "LOIS", false);
    r.addMenuItem(_("Maillage"), "MAILLAGE", false);
     r.addMenuItem(_("Conditions Initiales"), "COND_INITIALES", false);
    r.addMenuItem(_("Param�tres Temporels"), "PARAM_TEMPOREL", false);
    r.addMenuItem(_("Param�tres G�n�raux"), "PARAMETRES", false);
    r.addMenuItem(_("Param�tres R�sultats"), "PARAM_RESULTAT", false);
    //r.addSeparator("R�sultats");
    //r.addMenuItem("R�sultats g�n�raux", "RESULTATS_GENERAUX", false);
    //r.addMenuItem("Graphes", "GRAPHES_RESULTATS", false);
    return r;
  }

  // Menu Qualit� d'eau
  private BuMenu buildQualiteDEauMenu() {
    BuMenu r = new BuMenu(_("Qualit� d'eau"), "QUALITEDEAU", true);
    r.addSeparator(_("Param�tres"));
    r.addMenuItem(_("Mod�le de qualit� d'eau"), "MODELEQUALITEDEAU", true);
    r.addMenuItem(_("Catalogue des lois"), "LOIS_POUR_QUALITEDEAU", false);
    r.addMenuItem(_("Concentrations Initiales"), "CONCENT_INIT", false);
    r.addMenuItem(_("Param�tres G�n�raux Qualit� d'Eau"), "PARAMETRES_GENERAUX_QUALITEDEAU", false);
    r.addMenuItem(_("Param�tres R�sultats"), "PARAM_RESULTAT_POUR_QUALITEDEAU", false);
    return r;
  }

  // Menu Resultat
  private BuMenu buildResultatMenu() {
    BuMenu r = new BuMenu(_("R�sultats"), "RESULTATS", true);
    r.addSeparator(_("R�sultats"));
    r.addMenuItem(_("R�sultats g�n�raux"), "RESULTATS_GENERAUX", false);
    r.addMenuItem(_("Graphes"), "GRAPHES_RESULTATS", false);

    return r;
  }

  // Menu Calage
  private BuMenu buildCalageMenu() {
    BuMenu r = new BuMenu(_("Calage"), "CALAGE", true);
//    r.setEnabled(false);
//    r.setToolTipText("Actif si 1 seul bief");
    r.addSeparator(_("Param�tres"));
    r.addMenuItem(_("Zones de frottement"), "FROT_INIT_CALAGE", false);
    r.addMenuItem(_("Donn�es de crues"), "CRUES_CALAGE", false);
    r.addMenuItem(_("Param�tres du calage"), "PARAM_CALAGE", false);
    r.addSeparator(_("R�sultats"));
    r.addMenuItem(_("R�sultats g�n�raux"), "RESULTATS_CALAGE", false);
    r.addMenuItem(_("Graphes"), "GRAPHES_CALAGE", false);
    r.addMenuItem(_("Zones de frottement cal�es"), "FROT_CALAGE", false);
    return r;
  }

  /**
   * @return Le menu sediments
   */
  private BuMenu buildSedimentMenu() {
    BuMenu r = new BuMenu(_("S�diment"), "SEDIMENT");
    r.addMenuItem(_("Param�tres de s�dimentologie"), "PARAM_SEDIMENT");

    return r;
  }

  // Menu Synthese
  private BuMenu buildSyntheseMenu() {
    BuMenu r = new BuMenu(_("Synth�se"), "SYNTHESE");
    r.addMenuItem(_("Tableaux"), "TABLEAUX", true);
    return r;
  }

  /**
   * Methode appelee une seule fois pour construire le menu d'export.
   */
  private void buildExporterMenu() {
    BuMenu m = (BuMenu) getMainMenuBar().getMenu("EXPORTER");
    m.addMenuItem("Mascaret", "EXPORTMASCARET", false);
    m.addMenuItem("XML", "EXPORTXML", false);
    addExportImageMenuItem(m);
  }

  private void addExportImageMenuItem(BuMenu _m) {
    _m.addMenuItem(_("Image"), "EXPORT_IMAGES", this);
  }

  /**
   * Arret du calcul. Le calcul peut �tre pour la simulation ou le calage.
   */
  private void arreter() {

    JCheckBox cbResultats = new JCheckBox(_("R�cup�rer des r�sultats partiels ?"));
    Object[] o = {_("Votre calcul va etre arr�t�") + ".\n" + _("Souhaitez-vous confirmer l'arr�t du calcul ?") + "\n", cbResultats};
    int res = JOptionPane.showConfirmDialog(null, o, _("Arret du calcul"), JOptionPane.YES_NO_OPTION);
    if (res == JOptionPane.YES_OPTION) {
      SERVEUR_MASCARET.ArreterCalcul(true, cbResultats.isSelected());
    } else {
      SERVEUR_MASCARET.ArreterCalcul(false, false);
    }
  }

  /**
   * Lancement du calcul. Le calcul peut �tre pour la simulation ou le calage.
   *
   * @return _calage true : Calcul pour le calage.
   */
  private void calculer(boolean _calage) {
    if (DEMO_VERSION) { // Pas de lancement si version d�mo.
      new BuDialogError(
              getApp(),
              getInformationsSoftware(),
              "Ceci est une version de d�monstration,\n"
              + "vous ne pouvez pas lancer de calcul")
              .activate();
      return;
    }

    // Controle que toutes les variables hydrauliques sont bien pr�sentes pour le calcul s�dimentaire.
    if (ietude1d_.sediment().parametres().isActif()) {
      MetierDescriptionVariable[] missVars = ietude1d_.sediment().parametres().isVariablesManquantes(ietude1d_.paramResultats().variables());
      if (missVars.length != 0) {
        StringBuilder sb = new StringBuilder();
        for (MetierDescriptionVariable var : missVars) {
          sb.append("\n- ").append(var.description());
        }
        if (JOptionPane.showConfirmDialog(getApp().getFrame(), Hydraulique1dResource.getS(
                "Les r�sultats hydrauliques suivants sont n�cessaires pour\nle calcul des r�sultats s�dimentologiques choisis:{0}\n\n"
                + "Certains r�sultats s�dimentologiques ne seront pas calcul�s.\nVoulez-vous poursuivre ?", sb.toString()),
                Hydraulique1dResource.getS("Attention"), JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) {
          return;
        }
      }
    }

    JInternalFrame[] internFrames = getAllInternalFrames();
    for (int i = 0; i < internFrames.length; i++) {
      if (internFrames[i] instanceof Hydraulique1dReseauFrame) {
        Hydraulique1dReseauFrame djaFrame
                = (Hydraulique1dReseauFrame) internFrames[i];
        if (((DjaGridInteractive) djaFrame.getGrid()).isInteractive()) {
          djaFrame.actionPerformed(
                  new ActionEvent(
                          this,
                          ActionEvent.ACTION_PERFORMED,
                          "DJA_TOGGLE_INTERACTIVE"));
        }
        projet_.setDjaFrame(djaFrame);
      }
    }

    final BuTaskOperation op
            = new BuTaskOperation(this, _("Calcul"), _calage ? "oprCalculerCalage" : "oprCalculer");
    op.start();
    final BuMainPanel mp = getMainPanel();
    mp.setProgression(0);
    mp.setMessage(_("Ecriture des fichiers param�tres"));
    SERVEUR_MASCARET.setNoyau(ietude1d_.paramGeneraux().noyauV5P2());
    new Thread() {
      @Override
      public void run() {
        SProgression msg;
        if (op.isAlive()) {
          msg = SERVEUR_MASCARET.progression();
          while (msg == null) {
            msg = SERVEUR_MASCARET.progression();
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
          }
        }
        while (op.isAlive()) {
          msg = SERVEUR_MASCARET.progression();
          if (msg == null) {
            mp.setProgression(100);
            mp.setMessage(_("Op�ration temin�e"));
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            break;
          }
          mp.setMessage(msg.operation);
          mp.setProgression(msg.pourcentage);
          try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
          }
        }
        mp.setProgression(0);
        mp.setMessage("");

        try {
          if (SERVEUR_MASCARET.IsAfficherResultats()) {
            BuLib.invokeLater(new Runnable() {
              @Override
              public void run() {
                resultatsGeneraux();
              }
            });
          }
        } catch (RuntimeException ex) {
          System.err.println(_("impossible d'afficher les r�sultats"));
          ex.printStackTrace();
        }

      }
    }
            .start();
  }

  private void exporterMascaret() {
    Hydraulique1dExportPanneau p
            = new Hydraulique1dExportPanneau(Hydraulique1dBaseApplication.FRAME);
    p.setVisible(true);
    if (p.valeurRetour() != Hydraulique1dExportPanneau.OK) {
      return;
    }
    String[] choix = p.getChoix();
    if (choix == null) {
      return;
    }
    File dir = p.getFichier();
    if (dir == null) {
      return;
    }
    String nom = dir.getAbsolutePath();
    for (int i = 0; i < choix.length; i++) {
      if ("GEO_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportProfils_MASCARET(
                new File(nom + ".geo"),
                ietude1d_.reseau().biefs());
      } else if ("GEO_CASIER".equals(choix[i])) {
        Hydraulique1dExport.exportGeometrie_CASIER(
                new File(nom + ".casier"),
                ietude1d_.reseau().casiers());
      } else if ("CAS_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportCas_MASCARET(
                false,
                CtuluLibFile.appendExtensionIfNeeded(new File(nom), "cas"),
                ietude1d_);
      } else if ("XCAS_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportXCas_MASCARET(
                false,
                CtuluLibFile.appendExtensionIfNeeded(new File(nom), "xcas"),
                ietude1d_);
      } else if ("LOIS_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportLoisHydro_MASCARET(
                nom,
                ietude1d_);
      } else if ("REP_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportReprise_MASCARET(
                new File(nom + "_lec.rep"),
                ietude1d_.donneesHydro().conditionsInitiales().
                paramsReprise());
      } else if ("LIST_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportListing(
                new File(nom + ".lis"),
                ietude1d_.resultatsGeneraux().listing());
      } else if ("REP_ECRI_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportReprise_MASCARET(
                new File(nom + "_ecr.rep"),
                ietude1d_.resultatsGeneraux().resultatReprise());
      } else if ("LIG_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportLigneDEauInitiale(
                new File(nom + ".lig"),
                ietude1d_.donneesHydro().conditionsInitiales().
                ligneEauInitiale(),
                ietude1d_.reseau(),
                ietude1d_.paramResultats().decalage(),
                ietude1d_.paramGeneraux().profilsAbscAbsolu(), false); //false indique que l'export doit etre de type mascaret pas de conversion si on est en relatif
      } else if ("LIG_RUB".equals(choix[i])) {
        Hydraulique1dExport.exportLigneDEauInitiale(
                new File(nom + "_rub.lig"),
                ietude1d_.donneesHydro().conditionsInitiales().
                ligneEauInitiale(),
                ietude1d_.reseau(),
                ietude1d_.paramResultats().decalage(),
                ietude1d_.paramGeneraux().profilsAbscAbsolu(), true); //true indique que l'export doit etre de type rubens, il y aura une conversion si on est en relatif

      } else if ("RESU_MASCA".equals(choix[i])) {
        Hydraulique1dExport.exportRubens(
                new File(nom + ".rub"),
                ietude1d_.resultatsGeneraux().resultatsTemporelSpatial());
      } else if ("RESU_OPTYCA".equals(choix[i])) {
        Hydraulique1dExport.exportOpthyca(new File(nom + ".opt"),
                ietude1d_.resultatsGeneraux().
                resultatsTemporelSpatial());
      } else if ("RESU_CASIER_OPTYCA".equals(choix[i])) {
        Hydraulique1dExport.exportOpthycaCasier(
                new File(nom + ".cas_opt"),
                ietude1d_);
      } else if ("RESU_CASIER_RUBENS".equals(choix[i])) {
        Hydraulique1dExport.exportRubens(
                new File(nom + ".cas_rub"),
                ietude1d_.resultatsGeneraux().resultatsTemporelCasier());
      } else if ("RESU_CASIER_LISTING".equals(choix[i])) {
        Hydraulique1dExport.exportListing(
                new File(nom + ".cas_lis"),
                ietude1d_.resultatsGeneraux().listingCasier());
      } else if ("RESU_LIAISON_OPTYCA".equals(choix[i])) {
        Hydraulique1dExport.exportOpthycaLiaison(
                new File(nom + ".liai_opt"),
                ietude1d_);
      } else if ("RESU_LIAISON_RUBENS".equals(choix[i])) {
        Hydraulique1dExport.exportRubens(
                new File(nom + ".liai_rub"),
                ietude1d_.resultatsGeneraux().resultatsTemporelLiaison());
      } else if ("RESU_LIAISON_LISTING".equals(choix[i])) {
        Hydraulique1dExport.exportListing(
                new File(nom + ".liai_lis"),
                ietude1d_.resultatsGeneraux().listingCasier());
      } else if ("CAS_CALAGE".equals(choix[i])) {
        Hydraulique1dExport.exportCas_MASCARET(
                true,
                new File(nom + ".cal_cas"),
                ietude1d_);
      } else if ("XCAS_CALAGE".equals(choix[i])) {
        Hydraulique1dExport.exportXCas_MASCARET(
                true,
                new File(nom + "_cal.xcas"),
                ietude1d_);
      } else if ("RESU_CALAGE_OPTHYCA".equals(choix[i])) {
        Hydraulique1dExport.exportResuCalageAutoOpthyca(
                new File(nom + ".cal_opt"),
                ietude1d_);
      } else if ("RESU_CALAGE_RUBENS".equals(choix[i])) {
        Hydraulique1dExport.exportResuCalageAutoRubens(
                new File(nom + ".cal_rub"),
                ietude1d_);
      } else if ("LIST_CALAGE".equals(choix[i])) {
        Hydraulique1dExport.exportListing(
                new File(nom + ".cal_lis"),
                ietude1d_.calageAuto().resultats().listingCalage());
      } else if ("LOI_TRACER".equals(choix[i])) {
        Hydraulique1dExport.exportLoisTracer_MASCARET(
                nom,
                ietude1d_.donneesHydro().getLoisTracer(), ietude1d_.qualiteDEau().parametresModeleQualiteEau().vvNomTracer());
      } else if ("PARAM_PHY".equals(choix[i])) {
        Hydraulique1dExport.exportParamsPhysiquesQE(
                new File(nom + ".phy"),
                ietude1d_.qualiteDEau().parametresGenerauxQualiteDEau().paramsPhysTracer());

      } else if ("CONCS_INITS".equals(choix[i])) {
        MetierResultatsTemporelSpatial ires = ConvH1D_Masc.
                convertitDConcentrationInitiale_IResultatTemporelSpatial(
                        ietude1d_.qualiteDEau().concentrationsInitiales(),
                        ietude1d_.qualiteDEau().parametresModeleQualiteEau().
                        vvNomTracer());
        Hydraulique1dExport.exportOpthyca(new File(nom + ".conc"), ires);

      } else if ("METEO".equals(choix[i])) {
        Hydraulique1dExport.exportListing(
                new File(nom + ".met"),
                ietude1d_.qualiteDEau().parametresGenerauxQualiteDEau().
                paramMeteoTracer());
      } else if ("LIST_TRACER".equals(choix[i])) {
        Hydraulique1dExport.exportListing(
                new File(nom + ".tra_lis"),
                ietude1d_.resultatsGeneraux().listingTracer());

      } else if ("RESU_TRACER_OPTYCA".equals(choix[i])) {
        Hydraulique1dExport.exportOpthyca(new File(nom + ".tra_opt"),
                ietude1d_.resultatsGeneraux().
                resultatsTemporelTracer());
      } else if ("RESU_TRACER_RUBENS".equals(choix[i])) {
        Hydraulique1dExport.exportRubens(new File(nom + ".tra_rub"),
                ietude1d_.resultatsGeneraux().
                resultatsTemporelTracer());

      } else if ("RESU_TRACER_ET_MASCARET_OPTYCA".equals(choix[i])) {
        try {
          Hydraulique1dExport.exportOpthyca(new File(nom + "_TraEtMas.opt"),
                  MetierResultatsTemporelSpatial.fusionnerDResultatsTemporelSpatial(
                          ietude1d_.resultatsGeneraux().
                          resultatsTemporelSpatial(), ietude1d_.resultatsGeneraux().
                          resultatsTemporelTracer()));
        } catch (Exception exception) {
          String logMsg
                  = _("Attention, la fusion des r�sultats Mascaret et Tracer a �chou�")
                  + exception.getLocalizedMessage();
          new BuDialogMessage(
                  (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                  ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                  .getInformationsSoftware(),
                  logMsg)
                  .activate();
          System.err.println("$$$ " + exception);
          exception.printStackTrace();
        }
      } else if ("RESU_TRACER_ET_MASCARET_RUBENS".equals(choix[i])) {

        try {
          Hydraulique1dExport.exportRubens(new File(nom + "_TraEtMas.rub"),
                  MetierResultatsTemporelSpatial.fusionnerDResultatsTemporelSpatial(
                          ietude1d_.resultatsGeneraux().
                          resultatsTemporelSpatial(), ietude1d_.resultatsGeneraux().
                          resultatsTemporelTracer()));
        } catch (Exception exception) {
          String logMsg
                  = _("Attention, la fusion des r�sultats Mascaret et Tracer a �chou�")
                  + exception.getLocalizedMessage();
          new BuDialogMessage(
                  (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                  ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                  .getInformationsSoftware(),
                  logMsg)
                  .activate();
          System.err.println("$$$ " + exception);
          exception.printStackTrace();
        }
      }
    }
  }

  private void exporterXML() throws IOException {
    Hydraulique1dExportXMLPanneau p
            = new Hydraulique1dExportXMLPanneau(Hydraulique1dBaseApplication.FRAME);
    p.setVisible(true);
    if (p.valeurRetour() != Hydraulique1dExportXMLPanneau.OK) {
      return;
    }
    String[] choix = p.getChoix();
    if (choix == null) {
      return;
    }
    File dir = p.getFichier();
    if (dir == null) {
      return;
    }
    String nom = dir.getAbsolutePath();
    for (int i = 0; i < choix.length; i++) {
      if ("ETUDE1D".equals(choix[i])) {
        projet_.setEtude1d(ietude1d_);
        projet_.exportXMLEtude1d(new File(nom + "_etude1d.xml"));
      } else if ("RESEAU".equals(choix[i])) {
        JInternalFrame[] internFrames = getAllInternalFrames();
        for (i = 0; i < internFrames.length; i++) {
          if (internFrames[i] instanceof Hydraulique1dReseauFrame) {
            Hydraulique1dReseauFrame djaFrame
                    = (Hydraulique1dReseauFrame) internFrames[i];
            if (((DjaGridInteractive) djaFrame.getGrid()).isInteractive()) {
              djaFrame.actionPerformed(
                      new ActionEvent(
                              this,
                              ActionEvent.ACTION_PERFORMED,
                              "DJA_TOGGLE_INTERACTIVE"));
            }
            projet_.setDjaFrame(djaFrame);
          }
        }
        projet_.exportXMLReseau(new File(nom + "_reseau.xml"));
      }
    }
  }

  private void importerXCas() {
    fermer();
    Hydraulique1dImportXCasPanneau p = new Hydraulique1dImportXCasPanneau(Hydraulique1dBaseApplication.FRAME);
    p.setVisible(true);
    if (p.getReponse() != Hydraulique1dImportCasPanneau.OK) {
      return;
    }

    // Le fichier cas
    File fxcas = p.getFichierXCas();
    // Le fichier geo
    File fgeo = p.getFichierGeo();
    // Lecture du fichier Mascaret et transfert dans l'objet metier.

    Notifieur.getNotifieur().setEventMuet(true);
    resetCurrentProject();
    boolean ok = Hydraulique1dImport.importXCasMascaret(fxcas, fgeo, ietude1d_, false);
    Notifieur.getNotifieur().setEventMuet(false);
    if (!ok) {
      fermer();
      return;
    }
    projectLoadedAfterImport();
  }

  /**
   * Import d'un fichier .cas+.geo mascaret
   *
   * @throws IOException
   */
  private void importerMascaret() throws IOException {
    fermer();
    Hydraulique1dImportCasPanneau p = new Hydraulique1dImportCasPanneau(Hydraulique1dBaseApplication.FRAME);
    p.setVisible(true);
    if (p.getReponse() != Hydraulique1dImportCasPanneau.OK) {
      return;
    }

    // Le fichier cas
    File fcas = p.getFichierCas();
    // Le fichier geo
    File fgeo = p.getFichierGeo();
    // Le fichier dico.
    File fdico;
    if (p.isDico52()) {
      SERVEUR_MASCARET.setNoyau(true);
      fdico = new File(SERVEUR_MASCARET.getCheminNoyau(), "dico.txt");
    } else {
      SERVEUR_MASCARET.setNoyau(false);
      fdico = new File(SERVEUR_MASCARET.getCheminNoyau(), "dico.txt");
    }

    if (!fdico.exists()) {
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              _("Le fichier dictionnaire n'existe pas !"))
              .activate();
      return;
    }

    // Lecture du fichier Mascaret et transfert dans l'objet metier.
    Notifieur.getNotifieur().setEventMuet(true);
    resetCurrentProject();
    boolean ok = Hydraulique1dImport.importMascaret(fcas, fgeo, fdico, ietude1d_, p.isDico52());
    Notifieur.getNotifieur().setEventMuet(false);
    if (!ok) {
      fermer();
      return;
    }
    projectLoadedAfterImport();
  }

  private void importerXML() throws IOException {
    fermer();
    if (ietude1d_ == null) {
      ietude1d_ = new MetierEtude1d();
    }
    projet_.setEtude1d(ietude1d_);
    projet_.setIhmRepository(ihmP_);
    Hydraulique1dImportXMLPanneau p
            = new Hydraulique1dImportXMLPanneau(Hydraulique1dBaseApplication.FRAME);
    p.setVisible(true);
    if (p.valeurRetour() != Hydraulique1dImportXMLPanneau.OK) {
      return;
    }
    File fichierEtude = p.getFichierEtude();
    File fichierReseau = p.getFichierReseau();
    if (fichierEtude == null) {
      return;
    }
    projet_.importXML(fichierEtude, fichierReseau);
    ietude1d_ = projet_.getEtude1d();
    if (ietude1d_ != null) {
      calcul_.etude(ietude1d_);
      ihmP_.setEtude(ietude1d_);
    }
    etat = ietude1d_.etatMenu();
    setEnableMenu();
    Hydraulique1dReseauFrame f = (Hydraulique1dReseauFrame) projet_.getDjaFrame();
    if (f != null) {
      grille_ = (MascaretGridInteractive) f.getGrid();
      ajouteFenetre(f);
    }
    if (!verifieContraintes()) {
      fermer();
      return;
    }
    new BuDialogMessage(getApp(), isApp_, "Etude charg�e").activate();
    setEnabledForAction("EXPORTMASCARET", true);
    setEnabledForAction("EXPORTXML", true);
    setEnabledForAction("IMPORTXML", true);
    setEnabledForAction("EXPORTER", true);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("MASCARET", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("ARRETER", false);
    updateTitle(fichierEtude);
  }

  private void importerGeoCasiers() {
    try {
      MetierCasier[] icasiers = ietude1d_.reseau().casiers();
      if ((icasiers == null) || (icasiers.length == 0)) {
        String logMsg
                = _("Attention, il est n�cessaire de cr�er des casiers avant de r�aliser cette op�ration");
        new BuDialogMessage(
                (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                .getInformationsSoftware(),
                logMsg)
                .activate();
        return;
      }
      String[] extensionGeometrie = {"casier", "geo"};
      String[] descriptionGeometrie = {_("Fichier geom�trie des casiers"), _("Fichier geom�trie des casiers")};
      File fichier = Hydraulique1dImport.chooseFile(extensionGeometrie, descriptionGeometrie);

      if (fichier != null) {
        MetierGeometrieCasier[] tabGeo
                = Hydraulique1dImport.geometrieCasier(fichier, false);
        if (tabGeo != null) {
          if (icasiers.length != tabGeo.length) {
            String logMsg
                    = _("Attention, le nombre de casiers existant") + " (" + icasiers.length + ")";
            logMsg += "\n " + _("est diff�rent du nombre de casiers import�") + " ("
                    + tabGeo.length
                    + ")";
            logMsg += "\n \n " + _("Importation effectu� jusqu'au casier n�")
                    + Math.min(icasiers.length, tabGeo.length);
            new BuDialogMessage(
                    (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                    ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                    .getInformationsSoftware(),
                    logMsg)
                    .activate();
          }
          for (int i = 0; (i < icasiers.length) && (i < tabGeo.length); i++) {
            icasiers[i].geometrie(tabGeo[i]);
          }
        }
      }
    } catch (NullPointerException nulEx) {
      String logMsg
              = _("Attention, il est n�cessaire de cr�er des casiers avant de r�aliser cette op�ration");
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              logMsg)
              .activate();
    } catch (Exception exception) {
      String logMsg
              = _("Attention, probl�me inconnu") + ": " + exception.getLocalizedMessage();
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              logMsg)
              .activate();
      System.err.println("$$$ " + exception);
      exception.printStackTrace();
    }
  }

  private void importerGeoBiefs() {
    try {
      MetierBief[] ibiefs = ietude1d_.reseau().biefs();
      if ((ibiefs == null) || (ibiefs.length == 0)) {
        String logMsg
                = _("Attention, il est n�cessaire de cr�er des biefs avant de r�aliser cette op�ration");
        new BuDialogMessage(
                (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
                ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
                .getInformationsSoftware(),
                logMsg)
                .activate();
        return;
      }
      String[] extensionGeometrie = {"pro", "geo", "georef"};
      String[] descriptionGeometrie = {_("Fichier geom�trie des profils de tous les biefs"), _("Fichier geom�trie des profils de tous les biefs")};
      File fichier = Hydraulique1dImport.chooseFile(extensionGeometrie, descriptionGeometrie);
      if (fichier != null) {
        Hydraulique1dImport.importProfilsPRO_MASCARET(fichier, ibiefs);
      }
    } catch (NullPointerException nulEx) {
      String logMsg
              = _("Attention, il est n�cessaire de cr�er des biefs avant de r�aliser cette op�ration");
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              logMsg)
              .activate();
    } catch (Exception exception) {
      String logMsg
              = _("Attention, probl�me inconnu") + ": " + exception.getLocalizedMessage();
      new BuDialogMessage(
              (BuCommonInterface) Hydraulique1dBaseApplication.FRAME,
              ((BuCommonInterface) Hydraulique1dBaseApplication.FRAME)
              .getInformationsSoftware(),
              logMsg)
              .activate();
      System.err.println("$$$ " + exception);
      exception.printStackTrace();
    }
  }

  @Override
  public void license() {

    InputStream s = MascaretResource.MASCARET.getStream(("Licence.txt"));
    int c;
    StringBuilder buff = new StringBuilder();
    try {
      while ((c = s.read()) != -1) {
        buff.append((char) c);
      }
      s.close();

    } catch (IOException e) {
      System.err.println(_("Probleme sur la lecture du fichier de licence !!!"));
      e.printStackTrace();
    }

    BuInformationsSoftware il = getInformationsSoftware();
    JTextPane texteLicense = new JTextPane();
    texteLicense.setEditable(false);
    texteLicense.setText(buff.toString());
    JScrollPane scroll = new JScrollPane(texteLicense);
    BuDialogMessage bd = new BuDialogMessage(getApp(), il, scroll);
    bd.activate();
  }

  private void creer() {
    fermer();
    Notifieur.getNotifieur().setEventMuet(true);
    projet_ = Hydraulique1dProjet.getInstance();
    projet_.setCommonImplementation(this);
    ietude1d_ = new MetierEtude1d();
    projet_.setEtude1d(ietude1d_);
    calcul_.etude(ietude1d_);
    ihmP_.setEtude(ietude1d_);
    if (!CGlobal.AVEC_QUALITE_DEAU) {
      ietude1d_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs(false);
    }
    setEnableMenu(AUTORISE_NOYAU);
    setEnabledForAction("EXPORTMASCARET", true);
    setEnabledForAction("EXPORTXML", true);
    setEnabledForAction("IMPORTXML", true);
    setEnabledForAction("EXPORTER", true);
    setEnabledForAction("IMPORTER", true);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("MASCARET", true);
    setEnabledForAction("FERMER", true);
    Notifieur.getNotifieur().setEventMuet(false);
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#cmdOuvrirFile(java.io.File)
   */
  @Override
  public void cmdOuvrirFile(File _f) {
    ouvrir(_f.getAbsolutePath());
  }

  private void ouvrir(String arg) {
    long t0 = System.currentTimeMillis();
    File fichier;
    if (arg == null) {
      FileFilter[] fileFilters = new FileFilter[3];
      String[] rubFilters = {"rub", "cas_rub", "liai_rub", "tra_rub"};
      String[] optFilters = {"opt", "cas_opt", "liai_opt", "cal_opt", "tra_opt"};
      fileFilters[0] = new BuFileFilter(rubFilters, _("R�sultat calcul au format rubens"));
      fileFilters[1] = new BuFileFilter(optFilters, _("R�sultat calcul au format optyca"));
      fileFilters[2] = new BuFileFilter("masc", _("Etude Fudaa-Mascaret"));
      // B.M. Visiblement, le tableau fileFilters est modifi� par ouvrirFileChooser(). On en cr�e 1 temporaire.
      fichier = FudaaGuiLib.ouvrirFileChooser(_("Ouvrir"), new FileFilter[]{fileFilters[0], fileFilters[1], fileFilters[2]}, getFrame(), false,
              (String) null);
      setFileFilter(fileFilters[0]);
      if (fichier == null) {
        return;
      }
      if (!fichier.canRead()) {
        error(_("Erreur d'ouverture de Fichier"), _("Impossible de lire le fichier ") + fichier.getPath(), false);
        /*        new BuDialogMessage(
         (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
         ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
         .getInformationsSoftware(),"Impossible de lire le fichier "+fichier.getPath()).activate();*/

        return;
      }
      fermer();
      // fichier rubens
      if (fileFilters[0].accept(fichier)) {
        Notifieur.getNotifieur().setEventMuet(true);
        try {
          ires_ = ConvMasc_H1D.
                  convertirResultatsTemporelSpatialMasToH1d(
                          DResultatsMascaret.litResultatsTemporelSpatial(fichier, 1E20,
                                  DResultatsMascaret.RUBENS));
          setEnabledForAction("ENREGISTRERSOUS", true);
          graphesResultats(ires_);
        } catch (FichierMascaretException ex) {
          System.out.println(_("Probl�me lecture Rubens ") + ex.getLocalizedMessage());
          ex.printStackTrace();
          String message = _("Probl�me lecture Rubens") + " \n" + ex.getLocalizedMessage();
          if (ex.getLigne() != null) {
            message += "\n" + _("A la ligne") + " : " + ex.getLigne();
          }
          if (ex.getNumeroLigne() != -1) {
            message += "\n" + _("Au num�ro de ligne") + " : " + ex.getNumeroLigne();
          }
          error(_("Erreur d'ouverture de Fichier"), message, false);
          /*          new BuDialogMessage(
           (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
           ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
           .getInformationsSoftware(),message).activate();*/

        }
        Notifieur.getNotifieur().setEventMuet(false);
        return;
      }
      // fichier optyca
      if (fileFilters[1].accept(fichier)) {
        Notifieur.getNotifieur().setEventMuet(true);
        try {
          ires_ = ConvMasc_H1D.
                  convertirResultatsTemporelSpatialMasToH1d(
                          DResultatsMascaret.litResultatsTemporelSpatial(fichier, 1E20,
                                  DResultatsMascaret.OPTYCA));
          setEnabledForAction("ENREGISTRERSOUS", true);
          ires_.setTemporel(!(ires_.resultatsCalageAuto()));
          graphesResultats(ires_);
        } catch (FichierMascaretException ex) {
          System.out.println(_("Probl�me lecture Opthyca ") + ex.getLocalizedMessage());
          ex.printStackTrace();
          String message = _("Probl�me lecture Opthyca ") + "\n" + ex.getLocalizedMessage();
          if (ex.getLigne() != null) {
            message += "\n" + _("A la ligne") + " : " + ex.getLigne();
          }
          if (ex.getNumeroLigne() != -1) {
            message += "\n" + _("Au num�ro de ligne") + " : " + ex.getNumeroLigne();
          }
          error(_("Erreur d'ouverture de Fichier"), message, false);
          /*          new BuDialogMessage(
           (BuCommonInterface)Hydraulique1dBaseApplication.FRAME,
           ((BuCommonInterface)Hydraulique1dBaseApplication.FRAME)
           .getInformationsSoftware(),message).activate();*/
        }
        Notifieur.getNotifieur().setEventMuet(false);
        return;
      }
      getMainMenuBar().addRecentFile(fichier.getAbsolutePath(), "mascaret");
      MascaretPreferences.MASCARET.writeIniFile();

    } else {
      fermer();
      fichier = new File(arg);
    }
    ires_ = null;
    //NOTE: pour eviter d'envoyer des evt
    Notifieur.getNotifieur().setEventMuet(true);
    MetierEtude1d ietude1dTmp = new MetierEtude1d();
    projet_.setEtude1d(ietude1dTmp);
    projet_.setIhmRepository(ihmP_);
    if (!projet_.ouvrir(fichier)) {
      projet_.setEtude1d(ietude1d_);
      return;
    }
    ietude1d_ = projet_.getEtude1d();
    if (ietude1d_ != null) {
      ietude1d_.updateListener();
      calcul_.etude(ietude1d_);
      ihmP_.setEtude(ietude1d_);
    }
    etat = ietude1d_.etatMenu();
    setEnableMenu();
    Hydraulique1dReseauFrame f = (Hydraulique1dReseauFrame) projet_.getDjaFrame();
    if (f != null) {
      grille_ = (MascaretGridInteractive) f.getGrid();
      installContextHelp(f.getRootPane(), "mascaret/description_reseau.html");
      ajouteFenetre(f);
    }
    if (!verifieContraintes()) {
      fermer();
      return;
    }
    long DT = System.currentTimeMillis() - t0;
    System.out.println(_("Dur�e lecture") + " : " + ((double) DT) / 1000 + " sec");
    new BuDialogMessage(getApp(), isApp_, _("Etude charg�e")).activate();
    setEnabledForAction("EXPORTMASCARET", true);
    setEnabledForAction("EXPORTXML", true);
    setEnabledForAction("IMPORTXML", true);
    setEnabledForAction("EXPORTER", true);
    setEnabledForAction("IMPORTER", true);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("MASCARET", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("ARRETER", false);
    updateTitle(projet_.getFile());
    Notifieur.getNotifieur().setEventMuet(false);
  }

  private FileFilter getFileFilter() {
    return fileFilter_;
  }

  private void setFileFilter(FileFilter _fileFilter) {
    fileFilter_ = _fileFilter;
  }

  private void fermer() {
    //System.err.println("MascaretImplementation debut fermer()");
    if (FudaaParamChangeLog.CHANGE_LOG.isDirty()) {
      int res
              = new BuDialogConfirmation(
                      getApp(),
                      isApp_,
                      _("Votre projet va etre ferm�") + ".\n"
                      + _("Voulez-vous l'enregistrer avant ?") + "\n")
              .activate();
      if (res == JOptionPane.YES_OPTION) {
        enregistrer();
      }
    }
    if (ihmP_ != null) {
      ihmP_.fermer();
    }
    JInternalFrame[] internFrames = getAllInternalFrames();
    for (int i = 0; i < internFrames.length; i++) {
      if (internFrames[i] != null) {
        internFrames[i].dispose();
      }
    }
    if (ihmP_ != null) {
      ihmP_.init();
    }
    internFrames = getAllInternalFrames();
    for (int i = 0; i < internFrames.length; i++) {
      if (internFrames[i] instanceof Hydraulique1dReseauFrame) {
        internFrames[i] = null;
      }
    }
    if (ietude1d_ != null) {
      ietude1d_.dispose();
      ietude1d_ = null;
    }
    grille_ = null;
    System.gc();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("EXPORTMASCARET", false);
    setEnabledForAction("EXPORTXML", false);
    setEnabledForAction("EXPORTER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    etat = Hydraulique1dBaseImplementation.AUTORISE_NOYAU;
    setEnableMenu();
    getApp().setTitle("Fudaa-Mascaret " + getInformationsSoftware().version);
    //System.err.println("MascaretImplementation fin fermer()");
  }

  private void enregistrer() {
    File fichier = projet_.getFile();
    if (fichier == null) {
      enregistrerSous();
    } else {
      enregistreFichierMasc(fichier);
    }
  }

  private void enregistrerSous() {
    List<FileFilter> listeFileFilter = new ArrayList<FileFilter>();
    if (ietude1d_ != null) {
      if (ietude1d_.resultatsGeneraux() != null) {

        if (ietude1d_.resultatsGeneraux().resultatsTemporelSpatial() != null) {
          if (ietude1d_.resultatsGeneraux().resultatsTemporelSpatial().valPas() != null) {
            if (ietude1d_.resultatsGeneraux().resultatsTemporelSpatial().
                    valPas().length > 0) {
              listeFileFilter.add(new BuFileFilter("rub",
                      _("R�sultat calcul au format rubens")));
              listeFileFilter.add(new BuFileFilter("opt",
                      _("R�sultat calcul au format optyca")));
            }
          }
        }

        try {
          if (ietude1d_.resultatsGeneraux().resultatsTemporelCasier() != null) {
            if (ietude1d_.resultatsGeneraux().resultatsTemporelCasier().
                    valPas() != null) {
              if (ietude1d_.resultatsGeneraux().resultatsTemporelCasier().
                      valPas().length > 0) {
                listeFileFilter.add(new BuFileFilter("cas_rub",
                        _("R�sultat calcul casier au format rubens")));
                listeFileFilter.add(new BuFileFilter("cas_opt",
                        _("R�sultat calcul casier au format optyca")));
              }
            }
          }
        } catch (NullPointerException ex1) {
          // pas de resultatsTemporelCasier
        }

        try {
          if (ietude1d_.resultatsGeneraux().resultatsTemporelLiaison() != null) {
            if (ietude1d_.resultatsGeneraux().resultatsTemporelLiaison().
                    valPas() != null) {
              if (ietude1d_.resultatsGeneraux().resultatsTemporelLiaison().
                      valPas().length > 0) {
                listeFileFilter.add(new BuFileFilter("liai_rub",
                        _("R�sultat calcul liaison au format rubens")));
                listeFileFilter.add(new BuFileFilter("liai_opt",
                        _("R�sultat calcul liaison au format optyca")));
              }
            }
          }
        } catch (NullPointerException ex1) {
          // pas de resultatsTemporelLiaison
        }

        try {
          if (ietude1d_.resultatsGeneraux().resultatsTemporelTracer() != null) {
            if (ietude1d_.resultatsGeneraux().resultatsTemporelTracer().
                    valPas() != null) {
              if (ietude1d_.resultatsGeneraux().resultatsTemporelTracer().
                      valPas().length > 0) {
                listeFileFilter.add(new BuFileFilter("tra_rub",
                        _("R�sultat calcul qualit� d'eau au format rubens")));
                listeFileFilter.add(new BuFileFilter("tra_opt",
                        _("R�sultat calcul de qualit� d'eau au format optyca")));
              }
            }
          }
        } catch (NullPointerException ex1) {
          // pas de resultatsTemporelTracer
        }

        listeFileFilter.add(new BuFileFilter("masc", _("Etude Fudaa-Mascaret")));
      }
    } else if (ires_ != null) {
      listeFileFilter.add(new BuFileFilter("rub",
              _("R�sultat calcul au format rubens")));
      listeFileFilter.add(new BuFileFilter("opt",
              _("R�sultat calcul au format optyca")));

    }
    FileFilter[] fileFilters = listeFileFilter.toArray(new FileFilter[listeFileFilter.size()]);
    File fichier = FudaaGuiLib.ouvrirFileChooser(_("Enregister Sous"), fileFilters, getFrame(), true);
    setFileFilter(fileFilters[0]);

    if (fichier == null) {
      return;
    }

    MetierResultatsTemporelSpatial ires = null;
    int format = DResultatsMascaret.RUBENS;
    String ext = "masc";
    if (ires_ != null) {
      ext = "rub";
    }
    String filterDescription = getFileFilter().getDescription();
    // fichier r�sultat hydraulique rubens
    if ((_("R�sultat calcul au format rubens") + " (*.rub)").equals(filterDescription)) {
      if (ires_ != null) {
        ires = ires_;
      } else {
        ires = ietude1d_.resultatsGeneraux().resultatsTemporelSpatial();
      }
      format = DResultatsMascaret.RUBENS;
      ext = "rub";
    }
    // fichier r�sultat hydraulique optyca
    if ((_("R�sultat calcul au format optyca") + " (*.opt)").equals(filterDescription)) {
      if (ires_ != null) {
        ires = ires_;
      } else {
        ires = ietude1d_.resultatsGeneraux().resultatsTemporelSpatial();
      }
      format = DResultatsMascaret.OPTYCA;
      ext = "opt";
    }
    // fichier r�sultat casier rubens
    if ((_("R�sultat calcul casier au format rubens") + " (*.cas_rub)").equals(filterDescription)) {
      ires = ietude1d_.resultatsGeneraux().resultatsTemporelCasier();
      format = DResultatsMascaret.RUBENS;
      ext = "cas_rub";
    }
    // fichier r�sultat casier optyca
    if ((_("R�sultat calcul casier au format optyca)") + " (*.cas_opt)").equals(filterDescription)) {
      ires = ietude1d_.resultatsGeneraux().resultatsTemporelCasier();
      format = DResultatsMascaret.OPTYCA;
      ext = "cas_opt";
    }
    // fichier r�sultat liaison rubens
    if ((_("R�sultat calcul liaison au format rubens") + " (*.liai_rub)").equals(filterDescription)) {
      ires = ietude1d_.resultatsGeneraux().resultatsTemporelLiaison();
      format = DResultatsMascaret.RUBENS;
      ext = "liai_rub";
    }
    // fichier r�sultat liaison optyca
    if ((_("R�sultat calcul liaison au format optyca") + " (*.liai_opt)").equals(filterDescription)) {
      ires = ietude1d_.resultatsGeneraux().resultatsTemporelLiaison();
      format = DResultatsMascaret.OPTYCA;
      ext = "liai_opt";
    }
    // fichier r�sultat qualit� d'eau rubens
    if ((_("R�sultat calcul de qualit� d'eau au format rubens") + " (*.tra_rub)").equals(filterDescription)) {
      ires = ietude1d_.resultatsGeneraux().resultatsTemporelTracer();
      format = DResultatsMascaret.RUBENS;
      ext = "tra_rub";
    }
    // fichier r�sultat qualit� d'eau optyca
    if ((_("R�sultat calcul qualit� d'eau au format optyca") + " (*.tra_opt)").equals(filterDescription)) {
      ires = ietude1d_.resultatsGeneraux().resultatsTemporelTracer();
      format = DResultatsMascaret.OPTYCA;
      ext = "tra_opt";
    }

    // ajoute �ventuelement l'extension � la fin fu fichier
    String filename = fichier.getPath();
    int indexSlash = filename.lastIndexOf(java.io.File.separatorChar);
    if (indexSlash > 0) {
      String nomFicSeul = fichier.getPath().substring(indexSlash + 1);
      if (!nomFicSeul.endsWith("." + ext)) {
        filename += "." + ext;
      }
      fichier = new File(filename);
    }

    if (ires != null) {
      try {
        DResultatsMascaret.ecritResultats(format, fichier,
                ConvH1D_Masc.convertirResultatsTemporelSpatialH1dToMas(ires));
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      return;
    }
    // fichier mascaret par d�faut
    enregistreFichierMasc(fichier);
  }

  private void enregistreFichierMasc(File fichier) {
    if (getCurrentInternalFrame() instanceof DjaFrame) {
      Hydraulique1dReseauFrame djaFrame
              = (Hydraulique1dReseauFrame) getCurrentInternalFrame();
      if (((DjaGridInteractive) djaFrame.getGrid()).isInteractive()) {
        try {
          djaFrame.actionPerformed(
                  new ActionEvent(
                          this,
                          ActionEvent.ACTION_PERFORMED,
                          "DJA_TOGGLE_INTERACTIVE"));
        } catch (RuntimeException ex) {
          return;
        }
      }
      projet_.setDjaFrame(djaFrame);
    }
    long t0 = System.currentTimeMillis();
    ietude1d_.etatMenu(etat);
    projet_.setEtude1d(ietude1d_);
    try {
      projet_.enregistreSous(fichier);
    } catch (IOException ex1) {
      ex1.printStackTrace();
      new BuDialogError(getApp(),
              getInformationsSoftware(),
              _("Erreur d'entr�e-sortie, enregistrement non effectu�") + "\n"
              + ex1.getLocalizedMessage())
              .activate();
    } catch (OutOfMemoryError ex3) {
      ex3.printStackTrace();
      new BuDialogError(getApp(),
              getInformationsSoftware(),
              _("M�moire insuffisante, enregistrement non effectu�"))
              .activate();
    } catch (Throwable ex2) {
      ex2.printStackTrace();
      new BuDialogError(getApp(),
              getInformationsSoftware(),
              _("Erreur impossible � r�cup�rer, enregistrement non effectu�") + "\n "
              + ex2.getLocalizedMessage()
              + "\n" + _("Pr�venir la maintenance de Fudaa-Mascaret"))
              .activate();
    }
    long DT = System.currentTimeMillis() - t0;
    System.out.println("Dur�e enregistrement : " + ((double) DT) / 1000 + " sec");
    boolean found = false;
    String r = fichier.getAbsolutePath();

    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r
              .equals(
                      MascaretPreferences.MASCARET.getStringProperty(
                              "file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, "mascaret");
      MascaretPreferences.MASCARET.writeIniFile();
    }
    updateTitle(fichier);
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    enregistrer_ = false;
    setEnabledForAction("ENREGISTRER", enregistrer_);
  }

  private void updateTitle(File fichier) {
    String name = fichier.getAbsolutePath();
    int pos = name.lastIndexOf(System.getProperty("file.separator"));
    if (pos != -1) {
      name = name.substring(pos + 1);
    }
    setTitle("Fudaa-Mascaret " + getInformationsSoftware().version + " - " + name);
  }

  private boolean verifieContraintes() {
    return true;
  }

  public void ajouteFenetre(JInternalFrame _f) {
    removeInternalFrame(_f);
    addInternalFrame(_f);
  }

  private void noyau() {
    if (ietude1d_ == null) {
      creer();
    }
    ihmP_.NOYAU().editer();
    setEnableMenu(AUTORISE_RESEAU);
  }
  // ajout de la fenetre qui contient le r�seau

  private void reseau() {
    JInternalFrame[] internFrames = getAllInternalFrames();
    for (int i = 0; i < internFrames.length; i++) {
      if (internFrames[i] instanceof Hydraulique1dReseauFrame) {
        internFrames[i].show();
        return;
      }
    }
    if (grille_ == null) {
      grille_ = new MascaretGridInteractive();
      grille_.setInteractive(true);
    }
    Notifieur.getNotifieur().setEventMuet(true);
    MetierReseau reseau = new MetierReseau();
    Notifieur.getNotifieur().setEventMuet(false);
    ietude1d_.reseau(reseau);
    Hydraulique1dReseauFrame f
            = new Hydraulique1dReseauFrame(this, Hydraulique1dResource.HYDRAULIQUE1D.getString("RESEAU"), grille_, ietude1d_, ihmP_);
    installContextHelp(f.getRootPane(), "mascaret/description_reseau.html");
    f.setLocation(155 + 25, -15 + 25);
    ajouteFenetre(f);
    Hydraulique1dReseauMouseAdapter mouseAdapter
            = new Hydraulique1dReseauMouseAdapter(f,ietude1d_);
    grille_.addMouseListener(mouseAdapter);
    grille_.addMouseMotionListener(mouseAdapter);
    grille_.addGridListener(
            new Hydraulique1dReseauGridAdapter(ietude1d_.reseau()));
    setEnableMenu(AUTORISE_PLANIMETRAGE);
    projet_.setDjaFrame(f);
  }

  private void lois() {
    ihmP_.LIBRARY_LOI().setQualiteDEau(false);
    ihmP_.LIBRARY_LOI().editer();
    setEnableMenu(AUTORISE_MAILLAGE);
  }

  private void maillage() {
    ihmP_.MAILLAGE2().editer();
    setEnableMenu(AUTORISE_COND_INITIALES);
  }

  /*private void planimetrage() {
    ihmP_.PLANIMETRAGE2().editer();
    setEnableMenu(AUTORISE_COND_INITIALES);
  }*/

  private void condInitiales() {
    ihmP_.CONDITION_INITIALE().editer();
    setEnableMenu(AUTORISE_PARAM_TEMPOREL);
  }

  private void ligneInitiale() {
    ihmP_.LIGNE_INITIALE().editer();
  }

  private void paramTemporel() {
    ihmP_.PARAM_TEMPOREL().editer();
    setEnableMenu(AUTORISE_PARAMETRES);
  }

  private void parametres() {
    ihmP_.PARAM_GENERAUX().editer();
    setEnableMenu(AUTORISE_PARAM_RESULTAT);
  }

  private void laissesCrues() {
    ihmP_.LAISSE().editer();
  }

  private void zonesFrottement() {
    ihmP_.ZONES_FROTTEMENT().editer();
    //-- set param zone de frottement.
    ihmP_.PARAM_GENERAUX().getEdit_().displayFrottementGeneraux(ihmP_.ZONES_FROTTEMENT());
  }

  private void zonesSeches() {
    ihmP_.ZONES_SECHES().editer();
  }

  private void sitesStockage() {
    ihmP_.SITES_STOCKAGE().editer();
  }

  private void paramResultat() {
    ihmP_.PARAM_RESULTAT().editer();
    setEnableMenu(AUTORISE_CALCULER);
  }

  private void paramReprise() {
    ihmP_.PARAM_REPRISE().editer();
  }

  private void variableResultat() {
    ihmP_.VARIABLE_RESULTAT().editer();
  }

  private void resultatsGeneraux() {
    ihmP_.RESULTATS_GENERAUX().editer();
  }

  private void graphesResultats(MetierResultatsTemporelSpatial res) {
	  graphesResultats(res,false);
	  }
  
  private void graphesResultats(MetierResultatsTemporelSpatial res, boolean resultCalage) {
    ihmP_.GRAPHES_RESULTATS().editer(res,resultCalage, ietude1d_);
  }

  private void visuConditionsInitiales() {
    ihmP_.VISU_INITIALE().editer();
  }

  private void tableaux() {
    if (ihmP_ == null) {
      ihmP_ = Hydraulique1dIHMRepository.getInstance();
    }
    ihmP_.TABLEAUX().editer();
  }

  private void parametresCalage() {
    Hydraulique1dIHMRepository.getInstance().PARAM_CALAGE().editer();
  }

  private void zonesFrottementACaler() {
    Hydraulique1dIHMRepository.getInstance().ZONES_FROTTEMENT_A_CALER().editer();
  }

  private void zonesFrottementCalees() {
    Hydraulique1dIHMRepository.getInstance().ZONES_FROTTEMENT_CALEES().editer();
  }

  private void cruesCalage() {
    Hydraulique1dIHMRepository.getInstance().CRUES_CALAGE().editer();
  }

  private void listingsCalage() {
    Hydraulique1dIHMRepository.getInstance().LISTINGS_CALAGE().editer();
  }

  private void modeleQualiteDEau() {
    if (etat < AUTORISE_PARAM_RESULTAT) {
      error(_("Qualit� d'eau"), _("Pour acceder au mod�le de qualit� d'eau, il faut d'abord terminer le mod�le hydraulique !"), false);

    } else {
      Hydraulique1dIHMRepository.getInstance().MODELE_QUALITE_EAU().editer();
    }
  }

  private void paramsSediment() {
    Hydraulique1dIHMRepository.getInstance().PARAMS_SEDIMENT().editer();
  }

  private void loiPourQualiteDEau() {
    ihmP_.LIBRARY_LOI().setQualiteDEau(true);
    ihmP_.LIBRARY_LOI().editer();
  }

  private void concentrationsInitiales() {
    ihmP_.CONCENTRATIONS_INITIALES().editer();
  }

  private void paramsGenerauxQualiteDEau() {
    ihmP_.PARAMS_GENERAUX_TRACER().editer();
  }

  private void paramsGResultatPourQualiteDEau() {
    ihmP_.PARAM_RESULTAT_QUALITE_EAU().editer();
  }

  @Override
  public void setEnableMenu(int _etat) {
    if (_etat > etat) {
      etat = _etat;
    }
    setEnableMenu();
  }

  private void setEnableMenu() {
    boolean bnoyauPerm = ietude1d_ != null && ietude1d_.paramGeneraux().regime().value() == EnumMetierRegime._FLUVIAL_PERMANENT;
    // Les conditions initiales ne sont pas utilisees si noyau permanent.
    if (etat == AUTORISE_COND_INITIALES && bnoyauPerm) {
      etat = AUTORISE_PARAM_TEMPOREL;
    }

    if (etat >= AUTORISE_NOYAU) {
      setEnabledForAction("MASCARET", true);
      setEnabledForAction("NOYAU", true);
      setEnabledForAction("IMPORTXML", true);
      setEnabledForAction("EXPORTMASCARET", true);
      setEnabledForAction("EXPORTXML", true);
    } else {
      setEnabledForAction("MASCARET", false);
      setEnabledForAction("NOYAU", false);
      setEnabledForAction("EXPORTMASCARET", false);
      setEnabledForAction("EXPORTXML", false);
    }
    setEnabledForAction("SEDIMENT", etat >= AUTORISE_RESEAU);
    if (etat >= AUTORISE_RESEAU) {
      setEnabledForAction("RESEAU", true);
    } else {
      setEnabledForAction("RESEAU", false);
    }
    if (etat >= AUTORISE_LOIS) {
      setEnabledForAction("LOIS", true);
      
    } else {
      setEnabledForAction("LOIS", false);
    
    }
    if (etat >= AUTORISE_MAILLAGE) {
      setEnabledForAction("MAILLAGE", true);
    } else {
      setEnabledForAction("MAILLAGE", false);
    }
    if (etat >= AUTORISE_PLANIMETRAGE) {
      //setEnabledForAction("PLANIMETRAGE", true);
      setEnabledForAction("PROFILS_EDIT", true);
    } else {
      //setEnabledForAction("PLANIMETRAGE", false);
      setEnabledForAction("PROFILS_EDIT", false);
    }
    if (etat >= AUTORISE_COND_INITIALES && !bnoyauPerm) {
      setEnabledForAction("COND_INITIALES", true);
    } else {

      setEnabledForAction("COND_INITIALES", false);
    }
    if (etat >= AUTORISE_PARAM_TEMPOREL) {
      setEnabledForAction("PARAM_TEMPOREL", true);
    } else {
      setEnabledForAction("PARAM_TEMPOREL", false);
    }
    if (etat >= AUTORISE_PARAMETRES) {
      setEnabledForAction("PARAMETRES", true);
    } else {
      setEnabledForAction("PARAMETRES", false);
    }
    if (etat >= AUTORISE_PARAM_RESULTAT) {
      setEnabledForAction("PARAM_RESULTAT", true);
    } else {
      setEnabledForAction("PARAM_RESULTAT", false);
    }
    if (etat >= AUTORISE_CALCULER && !bcalcul_) {
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("ARRETER", false);
    } else {
      setEnabledForAction("CALCULER", false);
      if (bcalcul_) {
        setEnabledForAction("ARRETER", true);
      }
    }

    if (etat >= AUTORISE_RESULTATS_GENERAUX) {
      setEnabledForAction("RESULTATS", true);
      setEnabledForAction("RESULTATS_GENERAUX", true);
    } else {
      setEnabledForAction("RESULTATS", false);
      setEnabledForAction("RESULTATS_GENERAUX", false);
    }
    if (etat >= AUTORISE_GRAPHES_RESULTATS) {
      setEnabledForAction("GRAPHES_RESULTATS", true);
    } else {
      setEnabledForAction("GRAPHES_RESULTATS", false);
    }
    setEnableMenuQualiteDEau();
    setEnableMenuCalage();
  }

  /**
   * Controle sur l'activit� des items du menu calage.
   */
  private void setEnableMenuCalage() {
    boolean b1bief = false;
    boolean bnoyauPerm = false;
    boolean bnoyauVersion61 = false;
    boolean bzonesFrot = false;
    boolean bcrues = false;
    boolean bfrot = false;
    boolean bgraph = false;
    boolean blisting = false;

    if (ietude1d_ != null) {
      bnoyauPerm = (ietude1d_.paramGeneraux().regime().value() == EnumMetierRegime._FLUVIAL_PERMANENT);
      bnoyauVersion61 = !ietude1d_.paramGeneraux().noyauV5P2();
      if (ietude1d_.reseau() != null) {
        if (ietude1d_.reseau().biefs() != null) {
          b1bief = (ietude1d_.reseau().biefs().length == 1);
        }
      }
      if (ietude1d_.calageAuto() != null) {
        if (ietude1d_.calageAuto().zonesFrottement() != null) {
          bzonesFrot = (ietude1d_.calageAuto().zonesFrottement().length > 0);
        }
        if (ietude1d_.calageAuto().crues() != null) {
          bcrues = (ietude1d_.calageAuto().crues().length > 0);
        }
        if (ietude1d_.calageAuto().resultats() != null) {
          blisting
                  = (ietude1d_.calageAuto().resultats().messagesEcran() != null
                  && ietude1d_.calageAuto().resultats().messagesEcran().length > 0)
                  || (ietude1d_.calageAuto().resultats().messagesEcranErreur() != null
                  && ietude1d_.calageAuto().resultats().messagesEcranErreur().length > 0)
                  || (ietude1d_.calageAuto().resultats().listingCalage() != null
                  && ietude1d_.calageAuto().resultats().listingCalage().length > 0)
                  //                  || (ietude1d_.calageAuto().resultats().listingDamocles() != null
                  //                  && ietude1d_.calageAuto().resultats().listingDamocles().length > 0)
                  || (ietude1d_.calageAuto().resultats().listingMascaret() != null
                  && ietude1d_.calageAuto().resultats().listingMascaret().length > 0);

          if (ietude1d_.calageAuto().resultats().resultats() != null) {
            bfrot = (ietude1d_.calageAuto().resultats().resultats().valPas().length > 0);
            bgraph = bfrot;
          }
        }
      }
    }

    setEnabledForAction("CALAGE", bnoyauVersion61 & b1bief & bnoyauPerm);
    setEnabledForAction("FROT_INIT_CALAGE", bnoyauVersion61 & b1bief);
    setEnabledForAction("CRUES_CALAGE", bnoyauVersion61 & b1bief);
    setEnabledForAction("PARAM_CALAGE", bnoyauVersion61 & b1bief);
    setEnabledForAction("CALCUL_CALAGE", bnoyauVersion61 & (etat >= AUTORISE_CALCULER) & b1bief & bnoyauPerm & bzonesFrot & bcrues & !bcalcul_);
    setEnabledForAction("RESULTATS_CALAGE", bnoyauVersion61 & b1bief & bzonesFrot & bcrues & blisting);
    setEnabledForAction("GRAPHES_CALAGE", bnoyauVersion61 & b1bief & bzonesFrot & bcrues & bgraph);
    setEnabledForAction("FROT_CALAGE", bnoyauVersion61 & b1bief & bzonesFrot & bcrues & bfrot);
  }

  /**
   * Controle sur l'activit� des items du menu qualite d'eau.
   */
  @Override
  public void setEnableMenuQualiteDEau() {
//    boolean bnoyauNonTrans=false;
    boolean bnoyauVersion71 = false;
    //boolean bpresenceCasier=false;
    boolean bpresenceQualiteDEau = false;
    boolean betatSupParamsResulat = false;

    if (ietude1d_ != null) {
//      bnoyauNonTrans=!(ietude1d_.paramGeneraux().regime().value() == EnumMetierRegime._TRANSCRITIQUE);
      bnoyauVersion71 = !ietude1d_.paramGeneraux().noyauV5P2();

      if (ietude1d_.qualiteDEau() != null) {
        if (ietude1d_.qualiteDEau().parametresModeleQualiteEau() != null) {
          bpresenceQualiteDEau = (ietude1d_.qualiteDEau().parametresModeleQualiteEau().presenceTraceurs());
        }
      }

      if (ietude1d_.paramResultats() != null) {
        betatSupParamsResulat = etat >= AUTORISE_PARAM_RESULTAT;
      }
    }

    setEnabledForAction("QUALITEDEAU", bnoyauVersion71);
    setEnabledForAction("MODELEQUALITEDEAU", bnoyauVersion71);
    setEnabledForAction("LOIS_POUR_QUALITEDEAU", bnoyauVersion71 & betatSupParamsResulat & bpresenceQualiteDEau);
    setEnabledForAction("PARAMETRES_GENERAUX_QUALITEDEAU", betatSupParamsResulat & bpresenceQualiteDEau);
    setEnabledForAction("CONCENT_INIT", bnoyauVersion71 & betatSupParamsResulat & bpresenceQualiteDEau);
    setEnabledForAction("PARAM_RESULTAT_POUR_QUALITEDEAU", bnoyauVersion71 & betatSupParamsResulat & bpresenceQualiteDEau);

  }

///On surcharge displayHelp pour pouvoir modifier le nom de l'application,
//le nom utilis� �tant celui fournit dans isApp_.name
  @Override
  public void displayHelp(final String _url) {
    super.displayHelp(FuLib.replace(_url, "fudaa-mascaret", "src\\mascaret"));
  }

  @Override
  public void contextHelp(String _url) {
    System.err.println("MascaretImplementation  contextHelp called" + _url);

    try {

      if (_url == null || _url.length() == 0) {
        _url = "mascaret/index.html";
      }

      _url = "aide\\src\\" + _url;
      System.err.println(_url);
      displayURL(_url);

    } catch (Exception ex) {

      ex.printStackTrace();
    }
  }
  /*  private void setEtat(int etat_) {
   etat= etat_;
   }*/

  /**
   *
   */
  @Override
  public FudaaAstucesAbstract getAstuces() {
    return MascaretAstuces.MASCARET;
  }

  private void projectLoadedAfterImport() {
    FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
    enregistrer_ = true;
    // Construction du reseau et de la fenetre reseau.

    if (grille_ == null) {
      grille_ = new MascaretGridInteractive();
      grille_.setInteractive(false);
    }
    Hydraulique1dReseauFrame f = new Hydraulique1dReseauFrame(this, Hydraulique1dResource.HYDRAULIQUE1D.getString("RESEAU"), grille_, ietude1d_, ihmP_);
    f.initFromEtude(ietude1d_);
    installContextHelp(f.getRootPane(), "mascaret/description_reseau.html");
    f.setLocation(155 + 25, -15 + 25);
    ajouteFenetre(f);
    Hydraulique1dReseauMouseAdapter mouseAdapter = new Hydraulique1dReseauMouseAdapter(f,ietude1d_);
    grille_.addMouseListener(mouseAdapter);
    grille_.addMouseMotionListener(mouseAdapter);
    grille_.addGridListener(new Hydraulique1dReseauGridAdapter(ietude1d_.reseau()));
    f.repositionner();
    projet_.setDjaFrame(f);

    // On ne sait pas ce qui a �t� fait... => On autorise Calculer.
    setEnableMenu(AUTORISE_CALCULER);

    setEnabledForAction("EXPORTMASCARET", true);
    setEnabledForAction("EXPORTXML", true);
    setEnabledForAction("IMPORTXML", true);
    setEnabledForAction("EXPORTER", true);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("ENREGISTRER", true);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("MASCARET", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("ARRETER", false);
  }

  private void editProfils(){
	  /*BiefEditor editor = new BiefEditor(ietude1d_.reseau().biefs());
	  this.addInternalFrame(editor);
	  */
	  Hydraulique1dIHM_Bief ihmBief
      = Hydraulique1dIHMRepository.getInstance().BIEF();
		if(this.ietude1d_.reseau() != null && this.ietude1d_.reseau().biefs() != null && this.ietude1d_.reseau().biefs().length>0)
			ihmBief.setBief(this.ietude1d_.reseau().biefs()[0]);
		ihmBief.editer();
		ihmBief.setEtude(ietude1d_);
		setEnableMenu(AUTORISE_LOIS);
  }
  
  private void resetCurrentProject() {
    if (ietude1d_ == null) {
      ietude1d_ = new MetierEtude1d();
    }
    calcul_.etude(ietude1d_);
    ihmP_.setEtude(ietude1d_);
    projet_.setEtude1d(ietude1d_);
    projet_.setIhmRepository(ihmP_);
  }
}
