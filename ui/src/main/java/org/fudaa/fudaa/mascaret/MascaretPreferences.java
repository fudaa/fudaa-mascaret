/*
 * @file         MascaretPreferences.java
 * @creation     2000-11-06
 * @modification $Date: 2006-09-12 08:35:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.mascaret;
import org.fudaa.fudaa.commun.FudaaPreferences;
/**
 * Preferences pour Mascaret.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-12 08:35:33 $ by $Author: opasteur $
 * @author       Jean-Marc Lacombe
 */
public class MascaretPreferences extends FudaaPreferences {
  public final static MascaretPreferences MASCARET= new MascaretPreferences();
  @Override
  public void applyOn(Object _o) {
    if (!(_o instanceof MascaretImplementation))
      throw new RuntimeException("" + _o + " is not a MascaretImplementation.");
  }
}
