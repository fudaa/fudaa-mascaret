/*
 * @file         MascaretResource.java
 * @creation     1999-01-17
 * @modification $Date: 2006-09-12 08:35:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.mascaret;
import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuResource;
import org.fudaa.fudaa.ressource.FudaaResource;
/**
 * @version      $Revision: 1.5 $ $Date: 2006-09-12 08:35:33 $ by $Author: opasteur $
 * @author       Axel von Arnim, Mickael Rubens
 */
public class MascaretResource extends FudaaResource {
  public final static MascaretResource MASCARET= new MascaretResource(FudaaResource.FUDAA);
  public static BuAssistant ASSISTANT;
  private MascaretResource(final BuResource _parent) {
    super(_parent);
  }

}
