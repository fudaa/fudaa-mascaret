package org.fudaa.fudaa.mascaret.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OsFactory {


	public static String OS = System.getProperty("os.name").toLowerCase();




	/**
	 * Method that checks if os is 64 bits.
	 * chek fo an os.
	 */
	public static  boolean isOs64bits() {
		String os = System.getProperty("os.name").toLowerCase();
		System.out.println("os = "+os);
		if (isWindows()) {
			return isWindow64bits();
		} else if (isMac()) {
			return isMac64bits();
		} else if (isUnix()) {
			return isUnix64bits();
		} else if (isSolaris()) {
			return isSolaris64bits();
		} else {
			return isJVM64bits();
		} 

	}
	
	
	private static boolean isJVM64bits() {
		return System.getenv("os.arch").contains("64");
	}

	private static  boolean isWindow64bits() {
		return  (System.getenv("ProgramFiles(x86)") != null);
	}


	private static  boolean isUnix64bits() {
		String result= getResultOFCmd("uname -m");
		return (result == null?isJVM64bits(): result.contains("64"));

	}

	private static  boolean isMac64bits() {
		String result= getResultOFCmd("uname -a");
		return (result == null?isJVM64bits():result.toLowerCase().contains("x86_64"));
	}

	private static  boolean isSolaris64bits() {
		String result= getResultOFCmd("isainfo -v");
		return (result == null?isJVM64bits(): result.contains("64"));

	}



	public static boolean isWindows() {

		return (OS.toLowerCase().indexOf("win") >= 0);

	}

	public static boolean isMac() {

		return (OS.toLowerCase().indexOf("mac") >= 0);

	}

	public static boolean isUnix() {

		return (OS.toLowerCase().indexOf("nix") >= 0 || OS.toLowerCase().indexOf("nux") >= 0 || OS.toLowerCase().indexOf("aix") > 0 );

	}

	public static boolean isSolaris() {

		return (OS.toLowerCase().indexOf("sunos") >= 0);

	}


	private static String getResultOFCmd(String cmd) {
		String result="", line;
		Process process;
		try {
			process = Runtime.getRuntime().exec(cmd);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			while ((line = stdInput.readLine()) != null) {
				result = result + line;
			}


		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}    

		return result;
	}


	public static void main(String args[]) {
		System.out.println("your system is 64? -> " + OsFactory.isOs64bits());	
		System.out.println("os arch " + System.getProperty("os.arch"));
		System.out.println(System.getProperty("user.home"));
	}


}
