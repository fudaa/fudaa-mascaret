package org.fudaa.fudaa.hydraulique1d.legende;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.text.DecimalFormat;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LegendeTest {
	protected static Color[] DEFAULT_COLOR_RANGE = new Color[]{ new Color(31,120,188).darker(), Color.GREEN, new Color(251,208,151).darker().darker(),Color.RED.brighter().brighter().brighter().brighter()};
	protected Color[] colorConcentrations ;
	protected static int DEFAULT_NB_CONCENTRATION = 10;
	protected int numberOFDifferentsConcentrations=DEFAULT_NB_CONCENTRATION;
	protected double[] currentConcentrations;
	protected double minC=0 , maxC=25;  
	
	public  JComponent buildLegende() {
		
		JPanel legende = new JPanel(new GridLayout(numberOFDifferentsConcentrations,2,0,0)){
			
		public void paintComponent(Graphics g) {
	        super.paintComponent(g);
      
	        
	        DecimalFormat legendFormatter = new DecimalFormat("0.0000");
			double var =  (maxC - minC)/colorConcentrations.length;	
			int x=10,y=10, width = 32, height=32;
			for(int i=numberOFDifferentsConcentrations-1;i>=0;i--) {
				g.setColor(colorConcentrations[i]);
				g.fillRect(x, y, width, height);
				g.setColor(Color.black);
				g.drawLine(x+26, y+32, x+31, y+32);
				g.drawLine(11, y+32, 16, y+32);
				//if(y !=10) {
					g.drawLine(x+26, y, x+31, y);
					g.drawLine(11, y, 16, y);
			//	}
				g.drawString(legendFormatter.format(minC + (i)*var), x +35, y+35);
				if(i == (numberOFDifferentsConcentrations-1)) {
					g.drawString(legendFormatter.format(minC + (i+1)*var), x +35, y+5);					
				}
				y =y +32;
			}
	        
	    }
		};
		legende.setBackground(Color.white);
		/*
		JPanel content;
		JLabel colorLabel, textLabel;
		DecimalFormat legendFormatter = new DecimalFormat("0.0000");
		double var =  (maxC - minC)/colorConcentrations.length;	
		for(int i=numberOFDifferentsConcentrations-1;i>=0;i--) {
			content = new JPanel(new FlowLayout(FlowLayout.LEFT));
			content.setBackground(Color.white);
			colorLabel = new JLabel("  ");
			colorLabel.setOpaque(true);
			colorLabel.setBackground(colorConcentrations[i]);
			colorLabel.setPreferredSize(new Dimension(16,16));
			content.add(colorLabel);
			textLabel= new JLabel( legendFormatter.format(minC + (i)*var) );
			content.add(textLabel);
			legende.add(content);
		}
		*/
		legende.setPreferredSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		legende.setSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		legende.setMaximumSize(new Dimension(160,35*numberOFDifferentsConcentrations));
		
		return legende;
	}
	
	public void calculate() {
		colorConcentrations = new Color[numberOFDifferentsConcentrations];
		//colorConcentrations[0] = new Color(132,166,188);
		colorConcentrations[0] = DEFAULT_COLOR_RANGE[0];
		int cptColor =0;
		for(int i=1;i<numberOFDifferentsConcentrations;i++) {
			if(i%3 ==0 && cptColor < DEFAULT_COLOR_RANGE.length-1){
				cptColor++;
				colorConcentrations[i] =DEFAULT_COLOR_RANGE[cptColor];
			}else
				if(i<3 || (i>=6 &&i<9) )
				colorConcentrations[i] =colorConcentrations[i-1].brighter();
				else
					colorConcentrations[i] =colorConcentrations[i-1].darker();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LegendeTest test = new LegendeTest();
		test.calculate();
		JComponent jc= test.buildLegende();
        JFrame frame = new JFrame();
        frame.setPreferredSize(new Dimension(150,300));
        frame.getContentPane().add(jc);
        frame.setVisible(true);
        
        
	}

}
